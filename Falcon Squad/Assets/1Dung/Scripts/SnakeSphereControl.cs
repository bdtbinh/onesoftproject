﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;
using DG.Tweening;

public class SnakeSphereControl : MonoBehaviour
{
    [Header("====== TAILS MOVE CONFIG ======")]
    public List<GameObject> tails = new List<GameObject>();//Object các đuôi
    [Tooltip("Khoảng cách giữa các đuôi")]
    public float minDistance = 0.25f;//Khoảng cách giữa các đuôi
    [Tooltip("Tốc độ di chuyển của Head")]
    public float speed = 1;//Tốc độ di chuyển của Head
    [Tooltip("Tốc độ quay của đuôi")]
    public float rotationSpeed = 50;//Tốc độ quay của đuôi
    [Tooltip("Khoảng cách ngắn nhất giữa các đuôi")]
    public float spaceTail;//Khoảng cách ngắn nhất giữa các đuôi
    private int numElementLife;//Số lượng thân còn sống
    [Tooltip("Các điểm nổ cuối cùng")]
    //public Transform[] PosExploisions;//Các điểm nổ cuối cùng
    private EnemyAction2017 snakeSphere;
    [HideInInspector] public bool headIsPauseMove;

    private CheckEnemyInScene enemyInScene;

    Tweener tweener;
    PathManager path;
    float moveSpeed;


    private void Start()
    {
        path = snakeSphere.M_SplineMove.pathContainer;
    }
    private void MoveHead()
    {
        snakeSphere.M_SplineMove.SetPath(path);
        StartCoroutine(SetupMoveForTail(path));

    }

    IEnumerator SetupMoveForTail(PathManager path)
    {
        for (int i = 1; i < tails.Count; i++)
        {
            yield return new WaitForEndOfFrame();
            //tails[i].GetComponent<TailController>().splineMove.SetPath(path);
        }
    }

    public void MoveTail()
    {
        float curSpeed = speed;
        for (int i = 1; i < tails.Count; i++)
        {
            Transform curBodyPart = tails[i].transform;
            Transform prevBodyPart = tails[i - 1].transform;

            float dis = Vector3.Distance(prevBodyPart.position, curBodyPart.position);

            float _spaceTail = spaceTail;
            //float _spaceTailShadow = spaceTail;
            Vector3 newPos = prevBodyPart.position;
            newPos.z = tails[0].transform.position.z;

            float T = Time.deltaTime * dis / minDistance * curSpeed;

            if (i == 1)
            {
                _spaceTail += 0.2f;
                //_spaceTailShadow += 0.2f;
            }

            if (headIsPauseMove)
            {

                if (i == 1)
                {
                    if (dis >= _spaceTail + 2.5f)
                    {
                        curBodyPart.position = Vector3.Slerp(curBodyPart.position, newPos, T * 1.2f);
                    }
                }
                else
                {
                    if (dis >= _spaceTail)
                        curBodyPart.position = Vector3.Slerp(curBodyPart.position, newPos, T * 1.2f);
                }
            }
            else
            {
                if (i == 1)
                {
                    if (dis >= 1.2f)
                        curBodyPart.position = Vector3.Slerp(curBodyPart.position, newPos, T * 1.2f);
                }
                else
                {
                    if (dis >= 1.1f)
                        curBodyPart.position = Vector3.Slerp(curBodyPart.position, newPos, T * 1.2f);
                }
                //if (dis >= 1.1f)
                //    curBodyPart.position = Vector3.Slerp(curBodyPart.position, newPos, T * 1.2f);

            }
            curBodyPart.rotation = Quaternion.Slerp(curBodyPart.rotation, prevBodyPart.rotation, rotationSpeed * Time.deltaTime);
        }
    }
}
