﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class ButterflyBombController : MonoBehaviour
{

    Transform transCache, transParentCache;
    Transform playerTrans;
    EnemyAction2017 enemyActionIns;

    public float radiusBomb;
    public Animator animEnemy;
    public float timeLockPlayer=2.5f;
    public float normalSpeed = 4f;

    private void OnEnable()
    {
        playerTrans = GameObject.FindGameObjectWithTag("player").transform;
    }

    void Start()
    {
        transCache = transform;
        transParentCache = transCache.parent;
        enemyActionIns = GetComponent<EnemyAction2017>();
    }

    private void OnDisable()
    {
        startFindPlayer = false;
        isLookAt = false;
        speedAdd = 1f;
    }


    public void StartFindPlayerAction()
    {
        transParentCache = transCache.parent;
        enemyActionIns.M_SplineMove.Stop();
        isLookAt = true;
        this.Delay(1.5f, () =>
        {
            startFindPlayer = true;
        });

    }

    private void FixedUpdate()
    {
        if (isLookAt)
        {
            Quaternion posFollow = Quaternion.LookRotation(-(transCache.localPosition + transParentCache.localPosition) + playerTrans.localPosition, transCache.TransformDirection(Vector3.forward));
            transCache.rotation = new Quaternion(0, 0, posFollow.z, posFollow.w);
        }

        if (startFindPlayer)
        {
            FollowTarget(playerTrans, 20f, normalSpeed);
        }
    }

    bool isLookAt = false;
    float speedAdd = 1f;
    bool startFindPlayer = false;
    private void FollowTarget(Transform target, float distance, float normalSpeed)
    {       
        if (Vector2.Distance(target.position, transCache.position) < distance && !AllPlayerManager.Instance.playerCampaign.Aircraft.aircraftIsReloading)
        {
            speedAdd += 1.2f * Time.deltaTime; //Tốc độ tăng đần theo thời gian
            transCache.position = Vector3.MoveTowards(transCache.position, target.position, Time.deltaTime * normalSpeed * 1f * speedAdd);
            //
            Quaternion posFollow = Quaternion.LookRotation(-(transCache.localPosition + transParentCache.localPosition) + target.localPosition, transCache.TransformDirection(Vector3.forward));
            transCache.rotation = new Quaternion(0, 0, posFollow.z, posFollow.w);

            if (Vector2.Distance(target.position, transCache.position) < 2.5f)
            {
                startFindPlayer = false;
                AttackStep2();
            }
        }
        else
        {
            transCache.position -= transCache.up * Time.deltaTime * normalSpeed;
            speedAdd = 1f;
        }

    }
  
    public void AttackStep2()
    {

        Debug.LogError("NO");

        Collider2D[] col = Physics2D.OverlapCircleAll(transCache.position, radiusBomb);

        foreach (var item in col)
        {
            if (item.gameObject.CompareTag("player"))
            {
                item.GetComponent<Aircraft>().playerControllerFalconScript.PlayerDie();
            }
        }

        this.Delay(0.2f, () => { enemyActionIns.Die(); });
    }
}
