﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationBullet : MonoBehaviour
{

    private Quaternion startTranform;
    public Transform bulletChild;
    private void OnEnable()
    {
        startTranform = gameObject.transform.rotation;
        if (bulletChild == null)
        {
            bulletChild = gameObject.transform.GetChild(0);
        }
    }
    private void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            bulletChild.rotation = new Quaternion(startTranform.x, startTranform.y, -startTranform.z, startTranform.w);
        }
        //bulletChild.rotation = startTranform;
    }
}
