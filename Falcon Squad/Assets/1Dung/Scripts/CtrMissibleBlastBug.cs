﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CtrMissibleBlastBug : UbhBaseShot
{
    void Start()
    {

    }

    public override void Shot()
    {
        var bullet = GetBullet(transform.position);

        if (bullet != null)
        {
            bullet.transform.rotation = transform.rotation;
            BulletBlastBugControll com = bullet.GetComponent<BulletBlastBugControll>();
            com.FindPlayer();
        }
    }
}
