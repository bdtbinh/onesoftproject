﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using DG.Tweening;
using DG.Tweening.Core;
using SkyGameKit;
using TCore;

public class MeteorCollision : EnemyCollisionBase
{
    public EnemyAction2017 meteorObj;
    private CheckEnemyInScene checkEnemyInScene;
    //
    SpriteRenderer spr;

    Transform transCache, transParent;

    void Awake()
    {
        meteorObj = GetComponent<EnemyAction2017>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();
        spr = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        transCache = transform;
        transParent = transform.parent;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            meteorObj.Die(EnemyKilledBy.DeadZone);
        }
    }

    //--------------
    public override void TakeDamage(int damage, bool fromServer = false)
    {
        if (meteorObj == null)
        {
            meteorObj = GetComponent<EnemyAction2017>();
        }
        meteorObj.currentHP -= damage;
        if (meteorObj.enemyHpBarIns != null)
        {
            meteorObj.enemyHpBarIns.ChangeHPBar(meteorObj.currentHP, meteorObj.maxHP);
        }
        if (meteorObj.currentHP <= 0)
        {
            if (!meteorObj.fxDie)
            {
                meteorObj.fxDie = true;
                meteorObj.Shoot();

                Debug.Log("meteorShotWhenDie");

                this.Delay(1f, () =>
                {
                    if (!fromServer)
                    {
                        meteorObj.Die(EnemyKilledBy.Player);
                    }
                    else
                    {
                        meteorObj.Die(EnemyKilledBy.Server);
                    }
                },true);


                Pool_EffectDie();
            }
        }
        else
        {
            PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
        }
        base.TakeDamage(damage, fromServer);
    }



    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            if (checkEnemyInScene.enemyInScreen)
            {
                TakeDamage(laserPower);
            }
        }
    }

    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.enemyDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
