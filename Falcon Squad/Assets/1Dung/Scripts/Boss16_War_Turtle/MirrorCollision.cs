﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;

public class MirrorCollision : MonoBehaviour
{

    public MirrorElementControll mirrorObj;
    public float index;
    public void Pool_EffectTriggerBullet(Transform bulletTrans)
    {
        if (EffectList.Instance.enemyTriggerBullet != null && bulletTrans.gameObject.activeInHierarchy)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyTriggerBullet, bulletTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
    public void OnTriggerEnter2D(Collider2D col)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            var bullet = col.GetComponent<BulletPlayerValue>();
            switch (col.tag)
            {
                case "ZoneCheckEnemyInScene":
                    Debug.LogError("inScene");
                    break;
                case "PlayerBullet":
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    mirrorObj.effect.Play();
                    mirrorObj.Count += index;

                    break;
                case "PlayerBulletBurstShot":
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    mirrorObj.effect.Play();
                    mirrorObj.Count += index;
                    break;
                case "PlayerBulletStarbomb":
                    var bulletStarbomb = col.GetComponent<BulletActiveSkillPlane8>();
                    bulletStarbomb.StartExplosive();
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    mirrorObj.effect.Play();
                    mirrorObj.Count += index;
                    break;
                case "PlayerBulletHoming":

                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    mirrorObj.effect.Play();
                    mirrorObj.Count += index;
                    break;
                case "PlayerBulletWindSlash":
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    mirrorObj.Count += index;
                    break;
                case "Wing2SmallBird":
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    mirrorObj.effect.Play();
                    mirrorObj.Count += index;
                    break;

                case "PlayerMissile":
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    mirrorObj.effect.Play();
                    mirrorObj.Count += index;
                    break;
            }
        }
      

    }
}
