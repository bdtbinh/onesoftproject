﻿using PathologicalGames;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class BossWarTurtleCollision : EnemyCollisionBase
{
    private BossWarTurtleAction bossObj;
    private CheckEnemyInScene checkEnemyInScene;
    Transform transCache, transParent;

    void Start()
    {
        transCache = transform;
        transParent = transCache.parent;

        bossObj = GetComponent<BossWarTurtleAction>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            bossObj.Die(EnemyKilledBy.DeadZone);
        }
    }


    public override void TakeDamage(int damage, bool fromServer = false)
    {
        bossObj.currentHP -= damage;

        //Debug.LogError(boss11Obj.phase != boss11Obj.GetNumberPhase());
        if (bossObj.CheckChangePhase())
        {
            bossObj.StartBossAction();
        }

        PanelBossHealbar.Instance.ChangeBossHealbar(bossObj.currentHP, bossObj.maxHP);
        if (bossObj.currentHP <= 0)
        {
            PanelBossHealbar.Instance.HideBossHealbar();
            bossObj.GetComponent<CircleCollider2D>().enabled = false;
            if (bossObj.mirrorController != null)
            {
                bossObj.mirrorController.gameObject.SetActive(false);
            }
            if (bossObj.mirrorControllerOverride != null)
            {
                bossObj.mirrorControllerOverride.gameObject.SetActive(false);
            }
            bossObj.M_SplineMove.Stop();
            Pool_EffectDie();
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign || GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
            {
                MainScene.Instance.bossDead = true;
            }
            this.Delay(1.8f, () =>
            {
                bossObj.GetComponent<CircleCollider2D>().enabled = true;
                if (!fromServer)
                {
                    bossObj.Die(EnemyKilledBy.Player);
                }
                else
                {
                    bossObj.Die(EnemyKilledBy.Server);
                }
            });
        }
        else
        {
            //PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
        }
    }



    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {

        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            try
            {
                if (checkEnemyInScene.enemyInScreen)
                {
                    TakeDamage(laserPower);
                }
            }
            catch (System.NullReferenceException)
            {
                Debug.Log("Null LaserOnOnLaserHitTriggeredEnemy boss2");
                throw;
            }

        }
    }


    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.bossDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.bossDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
