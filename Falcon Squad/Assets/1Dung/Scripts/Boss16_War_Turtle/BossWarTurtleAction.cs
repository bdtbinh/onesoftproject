﻿using System.Collections;
using UnityEngine;
using SkyGameKit;
using DG.Tweening;


public class BossWarTurtleAction : BossAction
{
    public PhaseData[] phaseData;
    //public GameObject bulletInOverride;
    [HideInInspector]
    public int phase = 0;
    Transform player;
    PlayerInit playerInit;
    public BossWarTurtleAnimChange animHead, animGun_S_Left, animGun_S_Right, animGun_Hand_Left, animGun_Hand_Right, animGun_Back;
    public MirrorController mirrorController;
    public MirrorController mirrorControllerOverride;
    public IceSpiritController icyController;
    //public MirrorPosController[] mirrorPos;

    public ParticleSystem iceStorm, iceSprit_Atk;
    public ParticleSystem effectOverride;
    public ParticleSystem effectLaserBeam;
    public ParticleSystem effectRage;
    public GameObject effectSign;
    //public VortexLaser[] vortexLaser;
    //[HideInInspector]
    Collider2D collider;
    Vector3[] listEndPos = { new Vector3(3, 0.5f, 0), new Vector3(-3, 0.5f, 0) };
    private void Start()
    {
        collider = GetComponent<Collider2D>();
        player = GameObject.FindGameObjectWithTag("player").transform;
        playerInit = player.parent.gameObject.GetComponent<PlayerInit>();
    }

    //------------------------------------------GET INIT DATA----------------------------------------//
    /// <summary>
    /// Lấy ra Phase hiện tại
    /// </summary>
    /// <returns></returns>
    /// 
    public int GetNumberPhase()
    {
        for (int i = phaseData.Length - 1; i > 0; i--)
        {
            if (currentHP < maxHP * phaseData[i].hpPercent / 100)
            {
                phase = i;
                break;
            }
        }
        return phase;
    }

    /// <summary>
    /// Lấy ra action override hiện tại
    /// </summary>
    /// <param name="actionName"></param>
    /// <returns></returns>
    public ActionInOverride GetActionInOverride(string actionName)
    {
        int temp = GetNumberPhase();
        int x = 0;
        for (int i = 0; i < phaseData[temp].listactionOverride.actionInOverride.Length; i++)
        {
            if (phaseData[temp].listactionOverride.actionInOverride[i].actionOverrideName.Equals(actionName))
            {
                x = i;
            }
        }
        return phaseData[temp].listactionOverride.actionInOverride[x];
    }

    /// <summary>
    /// Lấy ra action normal hiện tại
    /// </summary>
    /// <param name="actionName"></param>
    /// <returns></returns>
    public ActionInPhase GetActionInPhase(string actionName)
    {
        int temp = GetNumberPhase();
        int x = 0;
        for (int i = 0; i < phaseData[phase].listActionInPhase.Length; i++)
        {
            if (phaseData[phase].listActionInPhase[i].actionName.Equals(actionName))
            {
                x = i;
            }
        }
        return phaseData[phase].listActionInPhase[x];
    }

    //---------------------------------------START ACTION-------------------------------------------//
    public void StartBossAction()
    {
        StopAllCoroutines();
        //mirrorController.HideAllMirror();
        int temp = GetNumberPhase();
        if (phaseData[temp].listactionOverride.actionInOverride.Length == 0)
        {
            StartCoroutine(StartNormalAction());
        }
        else
        {
            StartCoroutine(StartOverrideAction());
        }

    }
    public IEnumerator StartNormalAction()
    {
        dem = 0;
        isOverride = false;
        if (mirrorController != null)
        {
            mirrorController.HideAllMirror();
        }
        if (mirrorControllerOverride != null)
        {
            mirrorControllerOverride.HideAllMirror();
        }
        int temp = GetNumberPhase();
        for (int i = 0; i < phaseData[temp].listActionInPhase.Length; i++)
        {
            if (phaseData[temp].listActionInPhase[i].isLoop == false)
            {
                phaseData[temp].listActionInPhase[i].action.Invoke();
            }
            else
            {
                StopCoroutine(StartLoopAction(phaseData[temp].listActionInPhase[i].actionName, phaseData[temp].listActionInPhase[i].timeLoop, temp));
                LoopAction(phaseData[temp].listActionInPhase[i].actionName, phaseData[temp].listActionInPhase[i].timeLoop, temp);
            }
            yield return new WaitForSeconds(phaseData[temp].listActionInPhase[i].timeNextAction);

        }
    }

    public IEnumerator StartOverrideAction()
    {
        if (mirrorController != null)
        {
            mirrorController.HideAllMirror();
        }
        if (mirrorControllerOverride != null)
        {
            mirrorControllerOverride.HideAllMirror();
        }
        iceSprit_Atk.Stop();
        iceSprit_Atk.gameObject.SetActive(false);
        animHead.ChangeAnimStep3();

        int temp = GetNumberPhase();

        for (int i = 0; i < phaseData[temp].listactionOverride.actionInOverride.Length; i++)
        {
            phaseData[temp].listactionOverride.actionInOverride[i].action.Invoke();
            yield return new WaitForSeconds(phaseData[temp].listactionOverride.actionInOverride[i].timeNextAction);
        }
    }

    //-------------------------------------LOOP ACTION----------------------------------------------//
    public IEnumerator StartLoopAction(string actionName, float timeLoop, int phase)
    {
        if (gameObject.activeInHierarchy)
        {
            yield return new WaitForSeconds(timeLoop);
            LoopAction(actionName, timeLoop, phase);
        }
        else
        {
            StopAllCoroutines();
        }
    }

    public void LoopAction(string actionName, float timeLoop, int phase)
    {
        ActionInPhase actionInPhase = GetActionInPhase(actionName);

        actionInPhase.action.Invoke();

        StartCoroutine(StartLoopAction(actionName, timeLoop, phase));
    }

    //------------------------------------SHOOT GUN IN ACTION---------------------------------------//
    public void ShootGunInAction(string actionName)
    {
        ActionInPhase actionInPhase = GetActionInPhase(actionName);
        if (actionInPhase.bulletData == null)
        {
            return;
        }
        else
        {
            BulletData[] bulletData = actionInPhase.bulletData;
            StartCoroutine(ShootBullet(bulletData));
        }
    }

    public IEnumerator ShootGunInOverrideAction(BulletData[] bulletData)
    {
        if (bulletData != null)
        {
            foreach (BulletData item in bulletData)
            {
                item.bulletInfo.StartShotRoutine();
                yield return new WaitForSeconds(item.timeNextShoot);
            }
        }
    }

    public IEnumerator ShootBullet(BulletData[] bulletData)
    {
        if (bulletData != null)
        {
            foreach (var item in bulletData)
            {
                item.bulletInfo.StartShotRoutine();
                yield return new WaitForSeconds(item.timeNextShoot);
            }
        }
    }

    //-----------------------------------CHECK CHANGE PHASE-----------------------------------------//
    public bool CheckChangePhase()
    {
        if (phase != GetNumberPhase())
        {
            phase = GetNumberPhase();
            return true;
        }
        else return false;
    }

    //-----------------------------------NORMAL ACTION----------------------------------------------//
    public void IcySpirit(int number)
    {
        //int phase = GetNumberPhase();
        //animHead.ChangAnimStep1();
        //iceSprit_Atk.gameObject.SetActive(true);
        //iceSprit_Atk.Play();
        //this.Delay(1.5f, () =>
        //{
        //    animHead.ChangeAnimStep3();
        //    iceSprit_Atk.Stop();
        //    iceSprit_Atk.gameObject.SetActive(false);
        //    for (int i = 0; i < number; i++)
        //    {
        //        Vector3 screenPosition = MainScene.Instance.cameraTk2d.ScreenToWorldPoint(new Vector3(UnityEngine.Random.Range(Screen.width / 7, 6 * Screen.width / 7),
        //            UnityEngine.Random.Range(Screen.height / 15, Screen.height / 3), MainScene.Instance.cameraTk2d.farClipPlane / 2));
        //        //HelperDung.StartAction(() => { icyController.ActiveBullet(phase, i, screenPosition); }, 1f);
        //        icyController.ActiveBullet(phase, i, screenPosition);
        //    }

        //}, true);

        StartCoroutine(StartIcySpririt(number));

    }

    public IEnumerator StartIcySpririt(int number)
    {
        int phase = GetNumberPhase();
        animHead.ChangAnimStep1();
        iceSprit_Atk.gameObject.SetActive(true);
        iceSprit_Atk.Play();

        StartCoroutine(ShowIceSpririt(number));
        yield return new WaitForSeconds(1.5f);

        animHead.ChangeAnimStep3();
        iceSprit_Atk.Stop();
        iceSprit_Atk.gameObject.SetActive(false);

    }

    public IEnumerator ShowIceSpririt(int number)
    {
        for (int i = 0; i < number; i++)
        {
            Vector3 screenPosition = MainScene.Instance.cameraTk2d.ScreenToWorldPoint(new Vector3(UnityEngine.Random.Range(Screen.width / 7, 6 * Screen.width / 7),
                UnityEngine.Random.Range(Screen.height / 15, Screen.height / 3), MainScene.Instance.cameraTk2d.farClipPlane / 2));
            //HelperDung.StartAction(() => { icyController.ActiveBullet(phase, i, screenPosition); }, 1f);
            yield return new WaitForSeconds(0.5f);
            icyController.ActiveBullet(phase, i, screenPosition);
        }
    }
    public void ReflectMirror(int number)
    {
        //collider.enabled = false;
        animGun_S_Left.ChangAnimStep1();
        animGun_S_Right.ChangAnimStep1();
        this.Delay(0.5f, () =>
        {
            // for (int i = 0; i < number; i++)
            //{
            //mirrorPos[i].gameObject.SetActive(true);
            //DOTween.Sequence().Append(mirrorPos[i].transform.DOMove(listEndPos[i], 1).OnComplete(() =>
            //mirrorPos[i].gameObject.SetActive(false);

            if (mirrorController != null)
            {
                mirrorController.ShowMirror(number);
            }
            //}
        });
        animGun_S_Left.ChangeAnimStep3();
        animGun_S_Right.ChangeAnimStep3();
    }

    public void VortexLaser(int number)
    {
        //isShootLaser = true;
        //Debug.LogError("VortexLaser");
        //M_SplineMove.Pause();

        animGun_Hand_Left.ChangAnimStep1();
        animGun_Hand_Right.ChangAnimStep1();
        ShootGunInAction("VortexLaser");
        this.Delay(0.5f, () =>
         {
             animGun_Hand_Left.ChangeAnimStep3();
             animGun_Hand_Right.ChangeAnimStep3();
         });

    }
    public void SuperIceBeam()
    {
        animGun_Back.ChangAnimStep1();
        effectLaserBeam.Play();
        ActionInPhase actionInPhase = GetActionInPhase("SuperIceBeam");

        UbhShotCtrl iceBeam = actionInPhase.bulletData[0].bulletInfo.gameObject.GetComponent<UbhShotCtrl>();

        Vector3 playerPosition = player.position;
        Vector3 direction = player.position - actionInPhase.bulletData[0].bulletInfo.transform.position;

        isFollowPlayer = true;
        effectSign.SetActive(true);
        //HelperDung.LookAtToDirectionClamp(direction, actionInPhase.bulletData[0].bulletInfo.transform.parent.gameObject, 40, 2f);

        this.Delay(3f, () =>
        {
            effectSign.SetActive(false);
            iceBeam.gameObject.SetActive(true);
            StartCoroutine(EndIceBeam(iceBeam.gameObject, actionInPhase.bulletData[0].timeNextShoot));
        }, true);


    }

    public IEnumerator EndIceBeam(GameObject obj, float time)
    {
        yield return new WaitForSeconds(time);
        animGun_Back.ChangeAnimStep3();
        //HelperDung.LookAtToDirection(Vector3.zero, obj, 2);
        effectLaserBeam.Stop();
        obj.SetActive(false);
        isFollowPlayer = false;
    }
    //-----------------------------------OVERRIDE ACTION--------------------------------------------//
    [HideInInspector]
    public int dem;

    Vector3 bossPosition;
    [HideInInspector]
    public bool isOverride;

    public void Override(int numer)
    {
        collider.enabled = false;
        isOverride = true;
        bossPosition = transform.position;
        M_SplineMove.Pause();

        ActionInOverride actionInOverride = GetActionInOverride("Override" + dem.ToString());

        DOTween.Sequence().Append(transform.DOMove(new Vector3(0, 4.13f, 0), actionInOverride.moveDuration).SetEase(actionInOverride.ease))
            .OnComplete(() =>
            {
                if (dem == 0)
                {
                    effectOverride.gameObject.SetActive(true);
                    effectOverride.Play();
                    effectRage.gameObject.SetActive(true);
                    effectRage.Play();
                    animHead.ChangAnimStep1();

                    this.Delay(2.5f, () =>
                    {
                        animHead.ChangeAnimStep3();
                        if (!iceStorm.gameObject.activeInHierarchy)
                        {
                            iceStorm.gameObject.SetActive(true);
                            iceStorm.loop = true;
                            for (int i = 0; i < 4; i++)
                            {
                                iceStorm.gameObject.transform.GetChild(i).GetComponent<ParticleSystem>().loop = true;
                            }
                            iceStorm.Play();
                        }
                        if (gameObject.activeInHierarchy)
                        {
                            //isOverride = true;
                            StartCoroutine(ShowMirrorInOverride(3));
                        }
                        this.Delay(1f, () =>
                        {
                            ShootLaserInOverride(actionInOverride, numer);
                        }, true);
                    });
                }
                else
                {
                    if (!iceStorm.gameObject.activeInHierarchy)
                    {
                        iceStorm.gameObject.SetActive(true);
                        iceStorm.loop = true;
                        for (int i = 0; i < 4; i++)
                        {
                            iceStorm.gameObject.transform.GetChild(i).GetComponent<ParticleSystem>().loop = true;
                        }
                        iceStorm.Play();
                    }
                    if (gameObject.activeInHierarchy)
                    {
                        //isOverride = true;
                        StartCoroutine(ShowMirrorInOverride(3));
                    }
                    this.Delay(1f, () =>
                    {
                        ShootLaserInOverride(actionInOverride, numer);
                    }, true);
                }

            });
    }

    public IEnumerator ShowMirrorInOverride(int number)
    {
        yield return null;
        if (mirrorControllerOverride != null)
        {
            mirrorControllerOverride.ShowMirror(number);
        }
    }

    public void ShootLaserInOverride(ActionInOverride actionInOverride, int number)
    {
        StartCoroutine(StartShootLaserInOverride(actionInOverride, number));
    }

    public IEnumerator StartShootLaserInOverride(ActionInOverride actionInOverride, int number)
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("player").transform;
        }
        effectLaserBeam.Play();
        Vector3 playerPosition = player.position;
        Vector3 direction = player.position - actionInOverride.bulletData[0].bulletInfo.transform.position;

        isFollowPlayer = true;
        effectSign.SetActive(true);
        //SuperIceBeamController superIceBeamController = actionInOverride.bulletData[0].bulletInfo.GetComponent<SuperIceBeamController>();
        //superIceBeamController.ShootLaserStep1();

        animGun_Back.ChangAnimStep1();
        GameObject iceBeam = actionInOverride.bulletData[0].bulletInfo.gameObject;
        this.Delay(3f, () =>
        {
            effectSign.SetActive(false);
            iceBeam.gameObject.SetActive(true);
        }, true);
        StartCoroutine(EndIceBeam(iceBeam.gameObject, actionInOverride.bulletData[0].timeNextShoot));

        yield return new WaitForSeconds(0.3f);
        StartCoroutine(ShootBulletInOverride(number));

        yield return new WaitForSeconds(actionInOverride.bulletData[0].timeNextShoot / 2);

        //isFollowPlayer = true;
        // Quay laser theo huong player
        //FollowPlayer();
        //Vector3 playerPosition = player.position;
        //Vector3 direction = player.position - actionInOverride.bulletData[0].bulletInfo.transform.position;
        //HelperDung.LookAtToDirection(direction, actionInOverride.bulletData[0].bulletInfo.gameObject, 20f);
        //yield return new WaitForSeconds(1.75f);
        //isFollowPlayer = false;

        dem++;
        CheckEndOverride(dem);
    }

    bool isFollowPlayer;

    public void FollowPlayer()
    {
        if (isFollowPlayer)
        {
            if (player == null)
            {
                player = GameObject.FindGameObjectWithTag("player").transform;
            }
            Vector3 playerPosition = player.position;
            Vector3 direction = player.position - animGun_Back.gameObject.transform.position;
            HelperDung.LookAtToDirectionClamp(direction, animGun_Back.gameObject, 40f, 0.4f);
        }
        else
        {
            animGun_Back.gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
        }
        if (isOverride)
        {
            if (player == null || playerInit == null)
            {
                player = GameObject.FindGameObjectWithTag("player").transform;
                playerInit = player.parent.gameObject.GetComponent<PlayerInit>();
            }
            if (player.position.y > 2f && playerInit.Aircraft.aircraftIsReloading == false)
            {
                player.parent.gameObject.GetComponent<PlayerController>().PlayerDie();
            }
        }
    }
    private void FixedUpdate()
    {
        FollowPlayer();
    }

    public IEnumerator ShootBulletInOverride(int number)
    {
        for (int i = 0; i < 2; i++)
        {
            yield return new WaitForSeconds(2f);
            if (player == null)
            {
                player = GameObject.FindGameObjectWithTag("player").transform;
            }
            Vector3 screenPosition = MainScene.Instance.cameraTk2d.ScreenToWorldPoint(new Vector3(UnityEngine.Random.Range(Screen.width / 7, 6 * Screen.width / 7),
                UnityEngine.Random.Range(Screen.height / 15, Screen.height / 3), MainScene.Instance.cameraTk2d.farClipPlane / 2));
            icyController.ActiveBulletInOverride(i, screenPosition);
        }
    }

    public void CheckEndOverride(int count)
    {
        //Debug.LogError(count + "/" + phaseData[GetNumberPhase()].listactionOverride.actionInOverride.Length);
        //Debug.LogError(count == phaseData[GetNumberPhase()].listactionOverride.actionInOverride.Length);
        if (count == phaseData[GetNumberPhase()].listactionOverride.actionInOverride.Length)
        {
            collider.enabled = true;
            iceStorm.loop = false;
            for (int i = 0; i < 4; i++)
            {
                iceStorm.gameObject.transform.GetChild(i).GetComponent<ParticleSystem>().loop = false;
            }
            iceStorm.Play();
            DOTween.Sequence().Append(transform.DOMove(new Vector3(0, 4.13f, 0), 1f)).OnComplete(() =>
            {
                this.Delay(1f, () =>
                {
                    SetNextPath();
                    iceStorm.gameObject.SetActive(false);

                    //collider.enabled = true;
                    StartCoroutine(StartNormalAction());
                });

            });
        }
        else
        {
            return;
        }
    }

    //---------------------------------------------------------------------------------------------//
    public override void Restart()
    {
        base.Restart();
    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);
        dem = 0;
    }

}
