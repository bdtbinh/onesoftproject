﻿using SWS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using PathologicalGames;
using System;
namespace SkyGameKit
{

    //public class BasePhase
    //{
    //    public MiniPhase miniPhase;
    //}
    [Serializable]
    public class ActionPropertiesChange
    {
        [Space(50)]
        [HideInInspector]
        public string[] listAcitonName;
        [ValueDropdown("listAcitonName")]
        public string actionName;       

        [Header("Normal properties")]
        public BulletInfo[] bulletInfoData;
    }

    [Serializable]
    public class BulletInfo
    {
        public UbhShotCtrl bulletInfo;
        [Title("Thời gian chờ bắn giữa các phase")]
        public float timeDelaySkill;

        [Title("Chỉ dùng cho Skill2.Thời gian chờ bắn giữa các điểm tụ")]
        public float timeDelayShoot;
    }


    [Serializable]
    public class PhaseElement
    {
        public float hpPercent;
        public ActionPropertiesChange[] listActionPropertiesChange;
    }

    [Serializable]
    public class MiniPhase
    {
        public GameObject enemyPrefab;
        public PhaseElement[] listPhaseElement;
        [Button, GUIColor(0, 1, 0, 1)]
        public virtual void LoadAction()
        {
            string[] _listAcitonName = enemyPrefab.GetComponent<BaseEnemy>().GetListActionName();
            foreach (var itemListElement in listPhaseElement)
            {

                foreach (var item in itemListElement.listActionPropertiesChange)
                {
                    item.listAcitonName = _listAcitonName;
                }
            }
        }
    }
}