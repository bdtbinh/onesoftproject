﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using SWS;
using PathologicalGames;

public class RandomMove : MonoBehaviour
{
    public static List<int> checkRandom = new List<int>();

    public List<PathManager> listPathRandom;
    bool isMovePath, check;

    public GameObject player;

    //Vector3 point;
    Quaternion startRotate;

    private SplineMoveBullet _splineMove;
    UbhBullet bullet;
   
    public void Start()
    {
        
    }
    private void OnEnable()
    {
        _splineMove = GetComponent<SplineMoveBullet>();
        startRotate = transform.rotation;
        bullet = gameObject.GetComponentInChildren<UbhBullet>();

        SetNextPath();
    }

    private void OnDisable()
    {
        isMovePath = false;
        check = false;
    }
    public PathManager nextPath;
    public void SetNextPath()
    {
        //_splineMove.Stop();
        if (_splineMove == null)
        {
            return;
        }
        _splineMove.moveToPath = true;
        int temp = Random.Range(0, listPathRandom.Count);
        checkRandom.Add(temp);
        if (!checkRandom.Contains(temp))
        {
            nextPath = listPathRandom[temp];
        }
        else
        {
            if (temp != 0)
            {
                temp = Random.Range(0, temp);
            }
            else
            {
                temp = Random.Range(temp + 1, listPathRandom.Count);
            }
            nextPath = listPathRandom[temp];
        }
        checkRandom.Clear();
        _splineMove.pathContainer = nextPath;
        _splineMove.StartMove();
    }

    private void FixedUpdate()
    {
        gameObject.transform.GetChild(0).gameObject.transform.rotation = new Quaternion(startRotate.x, startRotate.y, -startRotate.z, startRotate.w);
        if (MainScene.Instance.bossDead == true && GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            UbhObjectPool.instance.ReleaseBullet(bullet);
        }
    }
}
