﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss11ChangeAnimation : MonoBehaviour
{
     Animator anim;
    //public BossActionChange bossObj;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    public void EndAnimation()
    {
        anim.SetBool("attack1", false);        
    }

    public void ChangeAnimStep2()
    {
        anim.SetTrigger("isAttack2");
    }

    public void ChangeAtkToIdle()
    {
        anim.SetTrigger("isIdle");
    }
    public void ChangeOutScene()
    {
        anim.SetTrigger("isOutScene");
    }
    public void ChangeDashUp()
    {
        anim.SetTrigger("isDashUp");
    }

    //public void BossAction()
    //{
    //    bossObj.GoOutScene();
    //}

}
