﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;

public class Boss11Collision : EnemyCollisionBase
{
    private Boss11Action boss11Obj;
    private CheckEnemyInScene checkEnemyInScene;
    SpriteRenderer spr;
    Transform transCache, transParent;

    void Awake()
    {
        transCache = transform;
        transParent = transCache.parent;

        boss11Obj = GetComponent<Boss11Action>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();
        spr = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            boss11Obj.Die(EnemyKilledBy.DeadZone);
        }
    }


    public override void TakeDamage(int damage, bool fromServer = false)
    {
        if (boss11Obj == null)
        {
            boss11Obj = GetComponent<Boss11Action>();
        }
        boss11Obj.currentHP -= damage;
        PanelBossHealbar.Instance.ChangeBossHealbar(boss11Obj.currentHP, boss11Obj.maxHP);
        if (boss11Obj.currentHP <= 0)
        {
            if (!boss11Obj.bossFx)
            {
                boss11Obj.bossFx = true;
                PanelBossHealbar.Instance.HideBossHealbar();
                boss11Obj.GetComponent<CircleCollider2D>().enabled = false;
                boss11Obj.M_SplineMove.Stop();
                Pool_EffectDie();
                if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign || GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
                {
                    MainScene.Instance.bossDead = true;
                }
                this.Delay(1.8f, () =>
                {
                    boss11Obj.GetComponent<CircleCollider2D>().enabled = true;
                    if (!fromServer)
                    {
                        boss11Obj.Die(EnemyKilledBy.Player);
                    }
                    else
                    {
                        boss11Obj.Die(EnemyKilledBy.Server);
                    }
                });
            }
        }
        else
        {
            PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
        }
        base.TakeDamage(damage, fromServer);
    }



    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {

        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            try
            {
                if (checkEnemyInScene.enemyInScreen)
                {
                    TakeDamage(laserPower);
                }
            }
            catch (System.NullReferenceException)
            {
                Debug.Log("Null LaserOnOnLaserHitTriggeredEnemy boss2");
                throw;
            }

        }
    }


    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.bossDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.bossDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

}
