﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;

public class Boss11Action : BossAction
{
    public Boss11GunMain boss11GunMainWeapon;

    //public List<Phase> listPhase;
    public MiniPhase miniPhase;
    //bool isShootTarget;

    public Animator animBoss;
    
    //-------------------------------ShootLoopSkill1--------------------------------------------//

    IEnumerator ShootMainWeaponLoop1(int timeDelay)
    {

        yield return new WaitForSeconds(timeDelay);
        if (gameObject.activeInHierarchy)
        {
            ShootSkill1(timeDelay);
        }
        else
        {
            StopAllCoroutines();

        }

    }
    //------------------------------------ShootLoopSkill2----------------------------------------------//

    IEnumerator ShootMainWeaponLoop2(int timeDelay)
    {

        yield return new WaitForSeconds(timeDelay);
        if (gameObject.activeInHierarchy)
        {
            ShootSkill2(timeDelay);
        }
        else
        {
            StopAllCoroutines();
        }

    }
    //------------------------------------ShootLoopSkill3----------------------------------------------//

    IEnumerator ShootMainWeaponLoop3(int timeDelay)
    {

        yield return new WaitForSeconds(timeDelay);
        if (gameObject.activeInHierarchy)
        {
            ShootSkill3(timeDelay);
        }
        else
        {
            StopAllCoroutines();

        }

    }
    //------------------------------------ShootLoopSkill4----------------------------------------------//
  
    IEnumerator ShootMainWeaponLoop4(int timeDelay)
    {

        yield return new WaitForSeconds(timeDelay);
        if (gameObject.activeInHierarchy)
        {
            ShootSkill4(timeDelay);
        }
        else
        {
            StopAllCoroutines();
        }
    }


    int phase = 0;
    /// <summary>
    /// Lấy ra Phase hiện tại
    /// </summary>
    /// <returns></returns>
    public int GetNumberPhase()
    {
        for (int i = miniPhase.listPhaseElement.Length - 1; i > 0; i--)
        {
            //Debug.Log(listPhase[i].hp);
            if (currentHP < maxHP * miniPhase.listPhaseElement[i].hpPercent / 100)
            {
                phase = i;
                break;
            }
        }
        //Debug.Log(phase);
        return phase;
    }

    public void ShootSkill1(int timeDelayShoot1)
    {
        int temp = GetNumberPhase();

        for (int j = 0; j < miniPhase.listPhaseElement[temp].listActionPropertiesChange.Length; j++)
        {
            if (miniPhase.listPhaseElement[temp].listActionPropertiesChange[j].actionName.Equals("ShootSkill1"))
            {              
                boss11GunMainWeapon.ShootMainWeaponStep1(miniPhase.listPhaseElement[temp].listActionPropertiesChange[j]);
                
            }
            //else
            //{
            //    boss11GunMainWeapon.ShootMainWeaponStep1(null);
            //}
        }
        StartCoroutine("ShootMainWeaponLoop1", timeDelayShoot1);
    }

    public void ShootSkill2(int timeDelayShoot2)
    {

        int temp = GetNumberPhase();
        for (int j = 0; j < miniPhase.listPhaseElement[temp].listActionPropertiesChange.Length; j++)
        {
            if (miniPhase.listPhaseElement[temp].listActionPropertiesChange[j].actionName.Equals("ShootSkill2"))
            {
                //Debug.Log("ShootSkill2");
                boss11GunMainWeapon.ShootMainWeapon2(miniPhase.listPhaseElement[temp].listActionPropertiesChange[j]);
            }
            //else
            //{
            //    boss11GunMainWeapon.ShootMainWeapon2();
            //}
        }
        StartCoroutine("ShootMainWeaponLoop2", timeDelayShoot2);
    }

    public void ShootSkill3(int timeDelayShoot3)
    {

        int temp = GetNumberPhase();

        for (int j = 0; j < miniPhase.listPhaseElement[temp].listActionPropertiesChange.Length; j++)
        {
            if (miniPhase.listPhaseElement[temp].listActionPropertiesChange[j].actionName.Equals("ShootSkill3"))
            {               
                boss11GunMainWeapon.ShootMainWeaponStep3(miniPhase.listPhaseElement[temp].listActionPropertiesChange[j]);
            }
            //else
            //{
            //    boss11GunMainWeapon.ShootMainWeaponStep3(null);
            //}
        }
        StartCoroutine("ShootMainWeaponLoop3", timeDelayShoot3);
    }

    public void ShootSkill4(int timeDelayShoot4)
    {

        int temp = GetNumberPhase();

        for (int j = 0; j < miniPhase.listPhaseElement[temp].listActionPropertiesChange.Length; j++)
        {
            if (miniPhase.listPhaseElement[temp].listActionPropertiesChange[j].actionName.Equals("ShootSkill4"))
            {
                
                boss11GunMainWeapon.ShootMainWeaponStep4(miniPhase.listPhaseElement[temp].listActionPropertiesChange[j]);
            }
            //else
            //{
            //    boss11GunMainWeapon.ShootMainWeaponStep4(null);
            //}
        }

        StartCoroutine("ShootMainWeaponLoop4", timeDelayShoot4);
    }

    public void AnimAttack1()
    {
        animBoss.SetBool("attack1", true);
    }
    public override void Restart()
    {
        base.Restart();
    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);

    }
}


