﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss11GunMain : MonoBehaviour
{
    //public ParticleSystem effectStepGun1;
    public ParticleSystem[] effectStepGun2;
    public ParticleSystem effectStepGun3;

    public Boss11Action bossActionObj;


    [SerializeField]
    public BulletInfo[] gunCtrlListPhase1;
    public BulletInfo[] gunCtrlListPhase2;
    public BulletInfo[] gunCtrlListPhase3;
    public BulletInfo[] gunCtrlListPhase4;

    //UbhShotCtrl gunCtrlListUse1;
    UbhShotCtrl[] gunCtrlListUse;

    //[Header("Prefab Shoot Info")]
    //public UbhShotCtrl[] shotInfoSkill1;
    //public UbhShotCtrl[] ShotInfoSkill2;
    //public UbhShotCtrl[] ShotInfoSkill3;
    //public UbhShotCtrl[] ShotInfoSkill4;
    //private void OnDisable()
    //{
    //    //effectStepGun1.gameObject.SetActive(false);
    //}

    public void ShootMainWeaponStep1(ActionPropertiesChange actionPropertiesChange = null)
    {
        bossActionObj.AnimAttack1();
        if (gameObject.activeInHierarchy)
        {

            gunCtrlListPhase1 = actionPropertiesChange.bulletInfoData;
            //gunCtrlListPhase1 = gunCtrlListUse;

            foreach (var item in gunCtrlListPhase1)
            {
                this.Delay(item.timeDelaySkill, () => { item.bulletInfo.StartShotRoutine(); });
            }
        }
        //EndEffect();
    }
    public void ShootMainWeapon2(ActionPropertiesChange actionPropertiesChange = null)
    {
        StartCoroutine(ShootMainWeaponStep2(actionPropertiesChange));
    }
    public IEnumerator ShootMainWeaponStep2(ActionPropertiesChange actionPropertiesChange = null)
    {
        //bossActionObj.M_SplineMove.Pause();
        if (gameObject.activeInHierarchy)
        {
            gunCtrlListPhase2 = actionPropertiesChange.bulletInfoData;
            for(int i=0; i < effectStepGun2.Length; i++)
            {
                yield return new WaitForSeconds(0.15f);
                effectStepGun2[i].gameObject.SetActive(true);
                effectStepGun2[i].Play();
            }
            //gunCtrlListPhase2 = gunCtrlListUse;
            for (int i = 0; i < gunCtrlListPhase2.Length; i++)
            {
                UbhShotCtrl[] gunCtrl2 = new UbhShotCtrl[gunCtrlListPhase2[i].bulletInfo.gameObject.transform.childCount];
                for (int j = 0; j < gunCtrlListPhase2[i].bulletInfo.gameObject.transform.childCount; j++)
                {

                    gunCtrl2[j] = gunCtrlListPhase2[i].bulletInfo.gameObject.transform.GetChild(j).GetComponent<UbhShotCtrl>();

                    yield return new WaitForSeconds(gunCtrlListPhase2[i].timeDelayShoot);                  
                    gunCtrl2[j].StartShotRoutine();
                }

                yield return new WaitForSeconds(actionPropertiesChange.bulletInfoData[i].timeDelaySkill);

            }
        }
        EndEffectGun2();
        //ResumePath();

    }
    public void ShootMainWeaponStep3(ActionPropertiesChange actionPropertiesChange = null)
    {
        //bossActionObj.M_SplineMove.Pause();
        effectStepGun3.gameObject.SetActive(true);
        effectStepGun3.Play();

        if (gameObject.activeInHierarchy)
        {
            gunCtrlListPhase3 = actionPropertiesChange.bulletInfoData;
            //gunCtrlListPhase3 = gunCtrlListUse;
            if (actionPropertiesChange != null)
            {
                gunCtrlListPhase3 = actionPropertiesChange.bulletInfoData;
            }
            foreach (var gun in gunCtrlListPhase3)
            {
                this.Delay(gun.timeDelaySkill, () => { gun.bulletInfo.StartShotRoutine(); });

            }
        }
        this.Delay(1f, () =>
        {
            EndEffect();

            //ResumePath();
        });

    }
    public void ShootMainWeaponStep4(ActionPropertiesChange actionPropertiesChange = null)
    {
        bossActionObj.AnimAttack1();
        this.Delay(0.2f, () =>
        {
            if (gameObject.activeInHierarchy)
            {
                gunCtrlListPhase4 = actionPropertiesChange.bulletInfoData;
                //gunCtrlListPhase4 = gunCtrlListUse;
                if (actionPropertiesChange != null)
                {
                    gunCtrlListPhase4 = actionPropertiesChange.bulletInfoData;
                }
                foreach (var gun in gunCtrlListPhase4)
                {
                    this.Delay(gun.timeDelaySkill, () => { gun.bulletInfo.StartShotRoutine(); });

                }
            }
        });
    }

    //public void ResumePath()
    //{
    //    bossActionObj.M_SplineMove.Resume();
    //}
    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        foreach (var gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }

    }
    public void EndEffect()
    {
        effectStepGun3.gameObject.SetActive(false);
        effectStepGun3.Stop();
    }
    public void EndEffectGun2()
    {
        for (int i = 0; i < effectStepGun2.Length; i++)
        {
            effectStepGun2[i].gameObject.SetActive(false);
            effectStepGun2[i].Stop();
        }
    }
}
