﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class BulletBoss11OverrideController : MonoBehaviour
{
    public UbhShotCtrl[] gun;
    UbhBullet bullet;
    public ParticleSystem effect;

    private void OnEnable()
    {
        bullet = GetComponent<UbhBullet>();
        effect.gameObject.SetActive(true);
        effect.Play();
        ShootGunStep1();
    }

    public void ShootGunStep1()
    {
        this.Delay(1.5f, () => { ShootGunStep2(); });
    }
    public void ShootGunStep2()
    {
        if (gameObject.activeInHierarchy)
        {
            gun[0].StartShotRoutine();
        }

        this.Delay(0.1f, () => {
            ReleaseBullet();
        });
    }

    public void ReleaseBullet()
    {
        UbhObjectPool.instance.ReleaseBullet(bullet);
    }
}
