﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
using SkyGameKit;
using UnityEngine.Events;
using DG.Tweening;

[Serializable]
public class BulletData
{
    public UbhShotCtrl bulletInfo;
    [Title("Thời gian chờ bắn giữa các loại đạn")]
    public float timeNextShoot;
}


[Serializable]
public class ActionInPhase
{
    [GUIColor(255 , 0 , 0)]
    [Title("Điểm bắt đầu action mới", bold: true, horizontalLine: true)]
    public string actionName = "";
    public UnityEvent action;
    [Title("Thời gian chờ thực hiện action tiếp theo ")]
    public float timeNextAction;
    
    [Title("Dữ liệu loại đạn cần bắn ")]
    public BulletData[] bulletData;

    [Title("Loop lại action ?")]
    public bool isLoop;

    [ShowIf("isLoop")]
    [Title("Thời gian loop lại action")]
    public float timeLoop;

}

[Serializable]
public class PhaseData
{   
    [GUIColor(255 , 255 , 0)]
    [Title("Điểm bắt đầu phase mới", bold: true, horizontalLine: true)]
    public float hpPercent;
    //[GUIColor(124, 252, 0)]
    public Override listactionOverride;

    public ActionInPhase[] listActionInPhase;
    
}

[Serializable]
public class ActionInOverride
{
    [GUIColor(255, 0, 0)]
    public string actionOverrideName;
    public UnityEvent action;
    public BulletData[] bulletData;
    public Ease ease = Ease.Linear;
    public float moveDuration;
    public float timeNextAction;
}

[Serializable]
public class Override
{
    public ActionInOverride[] actionInOverride;
    public float timeValueOverride;
}
public class PhaseBaseChange : MonoBehaviour
{
    
}
