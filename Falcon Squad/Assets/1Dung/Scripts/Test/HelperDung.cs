﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

public static class HelperDung
{
    public static List<object> DisruptiveList(List<object> array)
    {
        for (int i = 0; i < array.Count; i++)
        {
            //int r1 = System.Random.Next
            int r1 = UnityEngine.Random.Range(0, array.Count);
            int r2 = UnityEngine.Random.Range(0, array.Count);

            object temp = array[r1];
            array[r1] = array[r2];
            array[r2] = temp;
        }

        return array;
    }

    public static List<T> DisruptiveList<T>(List<T> array) where T : MonoBehaviour
    {
        for (int i = 0; i < array.Count; i++)
        {
            //int r1 = System.Random.Next
            int r1 = UnityEngine.Random.Range(0, array.Count);
            int r2 = UnityEngine.Random.Range(0, array.Count);

            T temp = array[r1];
            array[r1] = array[r2];
            array[r2] = temp;
        }

        return array;
    }

    /// <summary>
    /// Gọi một Hàm chạy liên tục
    /// </summary>
    /// <param name="action"></param>
    /// <param name="conditionStop"></param>
    /// <returns></returns>
    public static IEnumerator StartThread(UnityAction action, System.Func<bool> conditionStop,
        UnityAction actionStop = null, float time = 0.01f)
    {
        bool isStop = false;

        while (isStop == false)
        {
            yield return new WaitForSecondsRealtime(time);
            // Debug.Log("conditionStop " + conditionStop());
            if (conditionStop() == true)
            {
                isStop = true;
                if (actionStop != null)
                    actionStop();
            }
            else
            {
                action();
            }
        }
    }

    /// <summary>
    /// Gọi một Hàm chạy liên tục
    /// </summary>
    /// <param name="action"></param>
    /// <param name="conditionStop"></param>
    /// <returns></returns>
    public static IEnumerator StartThread(UnityAction action, float timeToStop, UnityAction actionStop = null)
    {
        bool isStop = false;

        float timer = 0;

        while (isStop == false)
        {
            yield return new WaitForSecondsRealtime(0.02f);
            timer += 0.02f;
            //Debug.Log(timer);
            if (timer >= timeToStop - 1)
            {
                isStop = true;
                if (actionStop != null)
                    actionStop();
            }
            else
                action();
        }
    }

    /// <summary>
    /// Tạo một hàm chạy sau một khoảng thời gian
    /// </summary>
    /// <param name="action"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    public static IEnumerator StartAction(UnityAction action, float time)
    {
        yield return new WaitForSecondsRealtime(time);
        action();
    }

    public static void StartActionNotUseCorutines(UnityAction action, float time)
    {
        Observable.Timer(TimeSpan.FromSeconds(time)).Subscribe(l => { action(); });
    }

    /// <summary>
    /// Tạo một hàm chạy khi một điều kiện thỏa mãn
    /// </summary>
    /// <param name="action"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    public static IEnumerator StartAction(UnityAction action, System.Func<bool> condition)
    {
        yield return new WaitUntil(condition);
        action();
    }

    /// <summary>
    /// Lấy một điểm cách 1 điểm cho trước và theo 1 hướng
    /// </summary>
    /// <param name="distance"></param>
    /// <param name="direction"></param>
    /// <param name="fromPoint"></param>
    /// <returns></returns>
    public static Vector3 GetPointDistanceFromObject(float distance, Vector3 direction, Vector3 fromPoint)
    {
        distance -= 1;
        //if (distance < 0)
        //    distance = 0;

        Vector3 finalDirection = direction + direction.normalized * distance;
        Vector3 targetPosition = fromPoint + finalDirection;

        return targetPosition;
    }


    /// <summary>
    /// Lấy ra một Vector hợp với VectorP một góc angle. Điểm đầu của Vector là PositionStart
    /// </summary>
    /// <param name="vectorP"></param>
    /// <param name="angle"></param>
    /// <param name="positionStart"></param>
    /// <returns></returns>
    public static Vector3 GetDirectionFromAngle(Vector3 vectorP, float angle, Vector3 positionStart)
    {
        if (angle == 90)
        {
            return new Vector3(vectorP.y, -vectorP.x).normalized;
        }
        else if (angle == 0)
        {
            return vectorP;
        }
        else if (angle == 180)
        {
            return -vectorP;
        }
        else if (angle == 360)
        {
            return new Vector3(-vectorP.y, vectorP.x).normalized;
        }

        if (angle > 360)
            angle -= 360;
        if (angle < 0)
            angle *= -1;

        float radiaAngle = (angle * Mathf.PI) / 180;
        float tanAngle = Mathf.Tan(radiaAngle);
        Vector2 u = new Vector2(vectorP.y, -vectorP.x);

        Vector3 B = GetPointDistanceFromObject(1, vectorP, positionStart);
        //Debug.Log("Distance "+ Vector3.Distance(positionStart, B));

        Vector3 result = GetPointDistanceFromObject(tanAngle, u, B);

        return (result - positionStart).normalized;
    }

    /// <summary>
    /// Lấy ra một Vector hợp với VectorP một góc angle. Điểm đầu của Vector là PositionStart
    /// </summary>
    /// <param name="vectorP"></param>
    /// <param name="angle"></param>
    /// <param name="positionStart"></param>
    /// <returns></returns>
    public static Vector3 GetDirectionFromAngle_2(Vector3 vectorP, float angle, Vector3 positionStart)
    {
        if (angle == 90)
        {
            return new Vector3(vectorP.y, -vectorP.x).normalized;
        }
        else if (angle == 0)
        {
            return vectorP;
        }
        else if (angle == 180)
        {
            return -vectorP;
        }
        else if (angle == 360)
        {
            return new Vector3(-vectorP.y, vectorP.x).normalized;
        }

        if (angle > 360)
            angle -= 360;
        if (angle < 0)
            angle *= -1;

        if (angle > 90 && angle < 270)
            vectorP = -vectorP;

        float radiaAngle = (angle * Mathf.PI) / 180;
        float tanAngle = Mathf.Tan(radiaAngle);
        Vector2 u = new Vector2(vectorP.y, -vectorP.x);

        Vector3 B = GetPointDistanceFromObject(1, vectorP, positionStart);
        //Debug.Log("Distance "+ Vector3.Distance(positionStart, B));

        Vector3 result = GetPointDistanceFromObject(tanAngle, u, B);

        return (result - positionStart).normalized;
    }

    public static IEnumerator DOLocalRotateQuaternion(Quaternion endValue, float speedRotate, GameObject objLookAt,
        UnityAction actionComplete)
    {
        bool isStop = false;

        while (isStop == false)
        {
            yield return new WaitForSecondsRealtime(Time.deltaTime);
            //Debug.Log(objLookAt.name + " "   + Quaternion.Angle(objLookAt.transform.localRotation, endValue));
            if (Quaternion.Angle(objLookAt.transform.localRotation, endValue) <= 8f)
            {
                isStop = true;
                break;
            }
            else
            {
                objLookAt.transform.localRotation = Quaternion.Slerp(objLookAt.transform.localRotation, endValue,
                    Time.deltaTime * speedRotate);
            }
        }

        actionComplete();
    }

    public static void LookAtToDirectionClamp(Vector3 diretion, GameObject objLookAt,float clampValue, float speedLockAt = 500)
    {
        float xTarget = diretion.x;
        float yTarget = diretion.y;
        float angle = Mathf.Atan2(yTarget, xTarget) * Mathf.Rad2Deg + 90;

        angle = Mathf.Clamp(angle, -clampValue, clampValue);

        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

        objLookAt.transform.rotation = Quaternion.Slerp(objLookAt.transform.rotation, q, Time.deltaTime * speedLockAt);
    }
 
    public static void LookAtToDirection(Vector3 diretion, GameObject objLookAt, float speedLockAt = 500)
    {
        float xTarget = diretion.x;
        float yTarget = diretion.y;
        float angle = Mathf.Atan2(yTarget, xTarget) * Mathf.Rad2Deg + 90;
     
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

        objLookAt.transform.rotation = Quaternion.Slerp(objLookAt.transform.rotation, q, Time.deltaTime * speedLockAt);
    }
    public static void LookAtToDirectionAngleDetal(Vector3 diretion, GameObject objLookAt, float AngleDetal, float speedLockAt = 500)
    {
        float xTarget = diretion.x;
        float yTarget = diretion.y;
        float angle = Mathf.Atan2(yTarget, xTarget) * Mathf.Rad2Deg + 90 + AngleDetal;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        objLookAt.transform.rotation = Quaternion.Slerp(objLookAt.transform.rotation, q, Time.deltaTime * speedLockAt);
    }

    public static void LookAtToPosition(Vector3 position, GameObject objLookAt, float speedRotate = 500)
    {
        float xTarget = position.x - objLookAt.transform.position.x;
        float yTarget = position.y - objLookAt.transform.position.y;
        float angle = Mathf.Atan2(yTarget, xTarget) * Mathf.Rad2Deg + 90;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        objLookAt.transform.rotation = Quaternion.Slerp(objLookAt.transform.rotation, q, Time.deltaTime * speedRotate);
    }

    public static void LookAtToPositionAngleDetal(Vector3 position, GameObject objLookAt, float AngleDetal, float speedRotate = 500)
    {
        float xTarget = position.x - objLookAt.transform.position.x;
        float yTarget = position.y - objLookAt.transform.position.y;
        float angle = Mathf.Atan2(yTarget, xTarget) * Mathf.Rad2Deg + 90 + AngleDetal;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        objLookAt.transform.rotation = Quaternion.Slerp(objLookAt.transform.rotation, q, Time.deltaTime * speedRotate);
    }



    //public static List<HPInPattern> SelectionSortPattern(List<HPInPattern> A)
    //{
    //    int i, j, min_idx;
    //    int n = A.Count;

    //    for (i = 0; i < n - 1; i++)
    //    {
    //        // Tìm phần tử nhỏ nhất trong mảng
    //        min_idx = i;
    //        for (j = i + 1; j < n; j++)
    //            if ((int)A[j].typePattern < (int)A[min_idx].typePattern)
    //                min_idx = j;

    //        // Đổi chỗ phần tử nhỏ nhất trong mảng
    //        HPInPattern temp = A[min_idx];
    //        A[min_idx] = A[i];
    //        A[i] = temp;
    //    }

    //    return A;
    //}
}
