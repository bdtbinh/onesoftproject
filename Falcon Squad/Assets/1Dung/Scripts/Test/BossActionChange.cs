﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using UniRx;
using System;
using SWS;
using DG.Tweening;
using UnityEngine.Events;

public class BossActionChange : BossAction
{
    public override void Restart()
    {
        base.Restart();
    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);

    }

    public PhaseData[] phaseData;
    LineRenderer line;
    [HideInInspector]
    public int phase = 0;
    Transform player;

    public GameObject arrowPrefab;
    public ArrowBullet arrowBullet;

    public ParticleSystem rageParticle;
    public ParticleSystem overLoadParticle;

    public Animator anim;

    private CircleCollider2D collider;
    private Vector3 startArrow;
    private void Start()
    {
        dem = 0;
        startArrow = new Vector3(0, 12, 0);
        collider = GetComponent<CircleCollider2D>();
        arrowBullet = Instantiate(arrowPrefab, startArrow, Quaternion.identity).GetComponent<ArrowBullet>();

        line = GetComponentInParent<LineRenderer>();
        player = GameObject.FindGameObjectWithTag("player").transform;
    }

    /// <summary>
    /// Lấy ra Phase hiện tại
    /// </summary>
    /// <returns></returns>
    /// 
    public int GetNumberPhase()
    {
        for (int i = phaseData.Length - 1; i > 0; i--)
        {
            if (currentHP < maxHP * phaseData[i].hpPercent / 100)
            {
                phase = i;
                break;
            }
        }
        return phase;
    }

    /// <summary>
    /// Lấy ra action override hiện tại
    /// </summary>
    /// <param name="actionName"></param>
    /// <returns></returns>
    public ActionInOverride GetActionInOverride(string actionName)
    {
        int temp = GetNumberPhase();
        int x = 0;
        for (int i = 0; i < phaseData[temp].listactionOverride.actionInOverride.Length; i++)
        {
            if (phaseData[temp].listactionOverride.actionInOverride[i].actionOverrideName.Equals(actionName))
            {
                x = i;
            }
        }
        return phaseData[temp].listactionOverride.actionInOverride[x];
    }

    public ActionInPhase GetActionInPhase(string actionName)
    {
        int temp = GetNumberPhase();
        int x = 0;
        for (int i = 0; i < phaseData[phase].listActionInPhase.Length; i++)
        {
            if (phaseData[phase].listActionInPhase[i].actionName.Equals(actionName))
            {
                x = i;
            }
        }
        return phaseData[phase].listActionInPhase[x];
    }


    bool isActionDone;
    float timer;
    bool isActionRunning;

    private void Update()
    {
        if (isActionRunning)
        {
            timer -= Time.deltaTime;
        }
    }

    public void StartBossAction()
    {
        foreach (ParticleSystem item in effectSkill2)
        {
            item.Stop();
            item.gameObject.SetActive(false);
        }
        foreach (ParticleSystem item in effGun2Phase2)
        {
            item.Stop();
            item.gameObject.SetActive(false);
        }

        StopAllCoroutines();

        int temp = GetNumberPhase();
        if (phaseData[temp].listactionOverride.actionInOverride.Length == 0)
        {
            StartCoroutine(StartNormalAction());
        }
        else
        {
            //timer = phaseData[temp].listactionOverride.timeValueOverride;
            //isActionRunning = true;
            StartCoroutine(StartOverrideAction());
        }

    }
    public IEnumerator StartNormalAction()
    {
        dem = 0;

        //StopCoroutine(StartOverrideAction());
        int temp = GetNumberPhase();
        yield return new WaitForSeconds(1.5f);
        for (int i = 0; i < phaseData[temp].listActionInPhase.Length; i++)
        {
            if (phaseData[temp].listActionInPhase[i].isLoop == false)
            {
                phaseData[temp].listActionInPhase[i].action.Invoke();
            }
            else
            {
                StopCoroutine(StartLoopAction(phaseData[temp].listActionInPhase[i].actionName, phaseData[temp].listActionInPhase[i].timeLoop, temp));
                LoopAction(phaseData[temp].listActionInPhase[i].actionName, phaseData[temp].listActionInPhase[i].timeLoop, temp);
            }
            yield return new WaitForSeconds(phaseData[temp].listActionInPhase[i].timeNextAction);

        }
    }

    public IEnumerator StartOverrideAction()
    {
        dem = 0;
        collider.enabled = false;
        //StopCoroutine(StartNormalAction());
        int temp = GetNumberPhase();
        for (int i = 0; i < phaseData[temp].listactionOverride.actionInOverride.Length; i++)
        {
            phaseData[temp].listactionOverride.actionInOverride[i].action.Invoke();
            yield return new WaitForSeconds(phaseData[temp].listactionOverride.actionInOverride[i].timeNextAction);
        }
    }

    //----------------------------------------NORMAL ACTION-------------------------------------------//
    public void ShootSkill1(string actionName)
    {
        anim.SetTrigger("isAttack1");

        ShootGunInAction(actionName, GetNumberPhase());
    }

    public ParticleSystem[] effectSkill2;
    public IEnumerator StartShootSkill2Phase1(string actionName)
    {
        ActionInPhase actionInPhase = GetActionInPhase(actionName);

        if (actionInPhase.bulletData != null)
        {
            foreach (ParticleSystem item in effectSkill2)
            {
                item.gameObject.SetActive(true);
                item.Play();
                yield return new WaitForSeconds(0.2f);
            }
            BulletData[] bulletdata = actionInPhase.bulletData;
            foreach (var item in bulletdata)
            {
                UbhShotCtrl[] bullet = new UbhShotCtrl[item.bulletInfo.gameObject.transform.childCount];
                for (int j = 0; j < bullet.Length; j++)
                {
                    bullet[j] = item.bulletInfo.gameObject.transform.GetChild(j).GetComponent<UbhShotCtrl>();
                    bullet[j].StartShotRoutine();
                    yield return new WaitForSeconds(0.2f);
                }
            }
        }
        foreach (ParticleSystem item in effectSkill2)
        {
            item.Stop();
            item.gameObject.SetActive(false);
        }

    }
    public void ShootSkill2Phase1(string actionName)
    {
        StartCoroutine(StartShootSkill2Phase1(actionName));
    }

    public ParticleSystem[] effGun2Phase2;

    public IEnumerator StartShootSkill2Phase2(string actionName)
    {
        ActionInPhase actionInPhase = GetActionInPhase(actionName);

        if (actionInPhase.bulletData != null)
        {
            foreach (ParticleSystem item in effGun2Phase2)
            {
                item.gameObject.SetActive(true);
                item.Play();
                yield return new WaitForSeconds(0.1f);
            }

            BulletData[] bulletdata = actionInPhase.bulletData;
            foreach (var item in bulletdata)
            {
                UbhShotCtrl[] bullet = new UbhShotCtrl[item.bulletInfo.gameObject.transform.childCount];
                for (int j = 0; j < bullet.Length; j++)
                {
                    bullet[j] = item.bulletInfo.gameObject.transform.GetChild(j).GetComponent<UbhShotCtrl>();
                    bullet[j].StartShotRoutine();
                }
            }
            foreach (ParticleSystem item in effGun2Phase2)
            {
                item.Stop();
                item.gameObject.SetActive(false);
            }
        }
    }

    public void ShootSkill2Phase2(string actionName)
    {
        StartCoroutine(StartShootSkill2Phase2(actionName));
    }

    public void ShootSkill3(string actionName)
    {
        //Debug.LogError("Action2");
        ShootGunInAction(actionName, GetNumberPhase());

    }

    public void ShootSkill4(string actionName)
    {
        anim.SetTrigger("isAttack1");
        ShootGunInAction(actionName, GetNumberPhase());

    }

    //-------------------------------------LOOP ACTION----------------------------------------------//
    public IEnumerator StartLoopAction(string actionName, float timeLoop, int phase)
    {
        if (gameObject.activeInHierarchy)
        {
            yield return new WaitForSeconds(timeLoop);
            LoopAction(actionName, timeLoop, phase);
        }
        else
        {
            StopAllCoroutines();
        }
    }

    public void LoopAction(string actionName, float timeLoop, int phase)
    {
        ActionInPhase actionInPhase = GetActionInPhase(actionName);

        actionInPhase.action.Invoke();

        StartCoroutine(StartLoopAction(actionName, timeLoop, phase));
    }

    //------------------------------------SHOOT GUN IN ACTION---------------------------------------//
    public void ShootGunInAction(string actionName, int phase)
    {
        ActionInPhase actionInPhase = GetActionInPhase(actionName);
        if (actionInPhase.bulletData == null)
        {
            return;
        }
        else
        {
            BulletData[] bulletData = actionInPhase.bulletData;
            StartCoroutine(ShootBullet(bulletData));
        }
    }

    public IEnumerator ShootGunInOverrideAction(BulletData[] bulletData)
    {
        if (bulletData != null)
        {
            foreach (BulletData item in bulletData)
            {
                Debug.LogError(item.bulletInfo.name);
                item.bulletInfo.StartShotRoutine();
                yield return new WaitForSeconds(item.timeNextShoot);
            }
        }

        //if (bulletData != null)
        //{
        //    foreach(var item in bulletData)
        //    {
        //        this.Delay(item.timeNextShoot, () => {
        //            item.bulletInfo.StartShotRoutine();
        //        });
        //    }
        //}
    }

    public IEnumerator ShootBullet(BulletData[] bulletData)
    {
        if (bulletData != null)
        {
            foreach (var item in bulletData)
            {
                item.bulletInfo.StartShotRoutine();
                yield return new WaitForSeconds(item.timeNextShoot);
            }
        }
    }

    //-----------------------------------CHECK CHANGE PHASE-----------------------------------------//
    public bool CheckChangePhase()
    {
        if (phase != GetNumberPhase())
        {
            phase = GetNumberPhase();
            return true;
        }
        else return false;
    }

    //----------------------------------OVERRIDE ACTION--------------------------------------------//  
    public int dem;
    //Vector3 bossPosition;
    public void OverrideSkill1(string actionName)
    {

        ActionInOverride actionInOverride;
        actionInOverride = GetActionInOverride(actionName);

        M_SplineMove.Pause();
        //bossPosition = gameObject.transform.position;
        DOTween.Sequence().Append(transform.DOMove(new Vector3(0, 4, 0), actionInOverride.moveDuration)).SetEase(actionInOverride.ease).OnComplete(() =>
          {
              dem++;
              anim.SetTrigger("isRage");
              rageParticle.gameObject.SetActive(true);
              rageParticle.Play();
              overLoadParticle.gameObject.SetActive(true);
              overLoadParticle.Play();
          });

    }

    public void OverrideSkill2(string actionName)
    {
        StartCoroutine(StartOverrideSkill2(actionName));
    }
    public IEnumerator StartOverrideSkill2(string actionName)
    {

        ActionInOverride actionInOverride;
        actionInOverride = GetActionInOverride(actionName);
        rageParticle.Stop();
        rageParticle.gameObject.SetActive(false);
        overLoadParticle.Stop();
        overLoadParticle.gameObject.SetActive(false);

        //anim.SetTrigger("isOutScene");
        yield return new WaitForSeconds(0.5f);
        DOTween.Sequence().Append(transform.DOMove(new Vector3(0, 12f, 0), 0f)).SetEase(actionInOverride.ease).OnComplete(() =>
        {
            anim.SetTrigger("isIdle");

        });
        yield return new WaitForSeconds(1f);
        arrowBullet.StartAttackFollowPlayer(actionInOverride.moveDuration);

    }

    public void CheckEndOverride(int count)
    {
        if (count == phaseData[GetNumberPhase()].listactionOverride.actionInOverride.Length)
        {
            this.Delay(1.5f, () => {
                arrowBullet.ActionEnd();
            });
        }

        else if (count == phaseData[GetNumberPhase()].listactionOverride.actionInOverride.Length + 1)
        {
            DOTween.Sequence().Append(transform.DOMove(new Vector3(0, 4, 0), 1f)).OnComplete(() =>
            {
                SetNextPath();
                collider.enabled = true;

                StartCoroutine(StartNormalAction());
                arrowBullet.TurnBackPosition(startArrow);
            });


        }

    }
}
