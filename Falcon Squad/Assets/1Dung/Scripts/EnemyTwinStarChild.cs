﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using Sirenix.OdinInspector;
using PathologicalGames;
using Assets.MuscleDrop.Scripts.Utilities.ExtensionMethods;
using TCore;
using SWS;

public class EnemyTwinStarChild : EnemyCollisionBase
{
    public string typeEnemyChild;
    public float speedMovement;
    public float timeMovement;
    public UbhShotCtrl listGun;

    private splineMove splineMoveObj;
    public PathManager path;
    private float speedStart;
    //private EnemyAction2017 enemyAction;
    public int hpTwinStarChild;
    [DisplayAsString]
    public int curHpTwinStarChild;

    SpriteRenderer spr;
    public EnemyHPbar enemyHpBarIns;
    void Start()
    {

        if (typeEnemyChild == "left")
        {
            transform.localPosition = new Vector3(MainScene.Instance.posAnchorLeft.localPosition.x, transform.localPosition.y, 0f);
        }
        else if(typeEnemyChild == "right")
        {
            //Debug.LogError("tranform : " +gameObject.name + gameObject.transform.localPosition);
            transform.localPosition = new Vector3(MainScene.Instance.posAnchorRight.localPosition.x, transform.localPosition.y, 0f);
        }

        spr = GetComponent<SpriteRenderer>();
        //
        //enemyAction = GetComponentInParent<EnemyAction2017>();

        splineMoveObj = gameObject.GetComponent<splineMove>();

        splineMoveObj.SetPath(path);
        speedStart = splineMoveObj.speed;
    }

    //private void OnTriggerEnter2D(Collider2D col)
    //{
    //    base.OnTriggerEnter2D(col);
    //    if (col.tag == Const.deadZone)
    //    {
    //        if (typeEnemyChild == "left")
    //        {
    //            enemyAction.Die(EnemyKilledBy.DeadZone);
    //        }
    //    }
    //}


    //public override void TakeDamage(int damage)
    //{
    //    curHpTwinStarChild -= damage;
    //    if (enemyHpBarIns != null)
    //    {
    //        enemyHpBarIns.ChangeHPBar(curHpTwinStarChild, hpTwinStarChild);
    //    }
    //    if (curHpTwinStarChild <= 0)
    //    {
    //        Pool_EffectDie();
    //        enemyAction.Die(EnemyKilledBy.Player);
    //    }
    //    else
    //    {
    //        Pool_EffectTriggerBullet(transform);
    //        PoolExplosiveSmall(spr);
    //        SoundManager.SoundBullet_Enemy();
    //    }
    //}


    //public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    //{

    //    if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
    //    {
    //        TakeDamage(laserPower);
    //    }
    //}


    ////---------
    //public override void Pool_EffectDie()
    //{
    //    base.Pool_EffectDie();
    //    if (EffectList.Instance.enemyDie != null)
    //    {
    //        Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyDie, transform.position, Quaternion.identity);
    //        effectIns.GetComponent<ParticleSystem>().Play(true);
    //    }
    //}



    //---------------------------------------------MOVE AND CHANGE SPEED----------------------------------------------

    bool enemyInScreen;
    private void OnBecameVisible()
    {
        enemyInScreen = true;
        //Debug.LogError("vao");
    }

    private void OnBecameInvisible()
    {
        enemyInScreen = false;
        //Debug.LogError("ra");
    }
    public IEnumerator ChangeSpeedChild(float newSpeed, float timeEnd)
    {

        //Debug.LogError(gameObject.name + " : " + newSpeed + " : " + timeEnd);
        if (newSpeed == null || newSpeed == 0)
        {
            Debug.LogError("Chua truyen` newSpeed vao` turn manager");
        }
        else
        {
            splineMoveObj.ChangeSpeed(newSpeed);
            splineMoveObj.Resume();
        }

        yield return new WaitForSeconds(timeEnd);
        {
            splineMoveObj.Pause();
            //splineMoveObj.speed = speedStart;
            //splineMoveObj.ChangeSpeed(speedStart);
           
        }
        if (listGun != null && enemyInScreen)
        {
            listGun.StartShotRoutine();
        }

    }

    public void ChangeSpeed()
    {
        StartCoroutine(ChangeSpeedChild(speedMovement, timeMovement));
    }

    //-----------------------------------------------------------------------------------------------------//
    bool isPauseAndShoot;
    public void PauseAndShoot()
    {
        //if (!isPauseAndShoot)
        //{
        //    isPauseAndShoot = true;
        //    splineMoveObj.Pause();
        //}


    }
}
