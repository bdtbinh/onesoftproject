﻿using System.Collections;
using UnityEngine;
using PathologicalGames;
using SWS;
using SkyGameKit;
using TCore;


public class MeteorController : EnemyData
{   
    EnemyChangeAnimation enemyChangeAnimation;
    public UbhShotCtrl[] gunCtrlList;
    public EnemyHPbar enemyHpBarIns;
    EnemyDropItem enemyDropItem;
    public Animator anim;
    float speedStart;

    [HideInInspector]
    public bool enemyIsInScene;

    //
    Transform tran_cache, tranParent_Cache;
    float posTop, posBottom, posLeft, posRight;
    void Awake()
    {
        speedStart = M_SplineMove.speed;
        enemyChangeAnimation = GetComponentInChildren<EnemyChangeAnimation>();
        enemyDropItem = GetComponent<EnemyDropItem>();       
    }
    void Start()
    {
        ChangeDataByDifficult();

        posTop = MainScene.Instance.posAnchorTop.localPosition.y;
        posBottom = MainScene.Instance.posAnchorBottom.localPosition.y;
        posLeft = MainScene.Instance.posAnchorLeft.localPosition.x - 0.1f;
        posRight = MainScene.Instance.posAnchorRight.localPosition.x + 0.1f;


        if (enemyHpBarIns != null)
        {
            enemyHpBarIns.baseEnemyTransIns = transform;
        }

    }
    bool enemyInScreen;
    private void OnBecameVisible()
    {
        enemyInScreen = true;
        //Debug.LogError("vao");
    }

    private void OnBecameInvisible()
    {
        enemyInScreen = false;
        //Debug.LogError("ra");
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    float valueAddHp;
    void ChangeDataByDifficult()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            switch (CacheGame.GetDifficultCampaign())
            {
                case "Normal":
                    valueAddHp = DataRemoteEnemy.Instance.HP_Normal;
                    break;
                case "Hard":
                    valueAddHp = DataRemoteEnemy.Instance.HP_Hard;
                    break;
                case "Hell":
                    valueAddHp = DataRemoteEnemy.Instance.HP_Hell;
                    break;
                default:
                    break;
            }

            maxHP = (int)(maxHP * valueAddHp);

            double coeff = CacheGame.GetValueLevelModifiServer(CacheGame.GetCurrentLevel());

            maxHP = (int)(maxHP * coeff);
            currentHP = maxHP;
        }

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
        {

            valueAddHp = DataRemoteEnemy.Instance.HP_Noel;
            maxHP = (int)(maxHP * valueAddHp);
            currentHP = maxHP;
        }
    }
   
    public void Shoot()
    {
        anim.SetTrigger("isAttack1");
        if (enemyInScreen)
        {
            if (gunCtrlList != null)
            {
                foreach(var item in gunCtrlList)
                {
                    item.StartShotRoutine();
                }
            }
        }
    }

    public void Change_Atk_To_Idle()
    {
        anim.SetTrigger("isIdle");
    }
    public void Pause()
    {
        M_SplineMove.Pause();
        if (gameObject.activeInHierarchy)
        {
            if (timeActionPause == 0 && timeActionPause == null)
            {
                Debug.LogError("Chua truyen` timeActionPause vao` turn manager");
            }
            else
                StartCoroutine("Resume", timeActionPause);
        }
    }


    IEnumerator Resume(float timeDelay)
    {
        yield return new WaitForSeconds(timeDelay);
        if (gameObject.activeInHierarchy)
        {
            M_SplineMove.Resume();
        }
        else
        {
            StopAllCoroutines();
        }
    }
    public void PauseAndShoot()
    {
        M_SplineMove.Pause();
        enemyChangeAnimation.ChangeAnim_Atk_Step1();
    }
    bool isShootLoop;

    public void ShootLoop(float timeDelay)
    {
        if (!isShootLoop)
        {
            tran_cache = transform;
            tranParent_Cache = tran_cache.parent;
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine("StartShootLoop", timeDelay);
            }

            isShootLoop = true;
        }
    }

    IEnumerator StartShootLoop(float timeDelay)
    {
        enemyChangeAnimation.ChangeAnim_Atk_Step1();
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (enemyInScreen)
            {
                if (gameObject.activeInHierarchy)
                {               

                    enemyChangeAnimation.ChangeAnim_Atk_Step1();
                }
                else
                {
                    enemyChangeAnimation.StopAllCoroutines();
                    StopAllCoroutines();
                    break;
                }
            }
            else
            {
               //Debug.LogError ("CheckEnemyInScene: " + CheckEnemyInScene ());
            }

        }
    }
    bool pathLoopChanged;
    //	protected Transform newPathLoopTransform;

    public void ChangeToPathLoop()
    {


        if (!pathLoopChanged)
        {
            if (newPath_Loop.name.Length < 1)
            {
                Debug.LogError("Da dien` new path vao turn chua???");
            }

            M_SplineMove.Stop();
            //
            if (PoolManager.Pools[Const.pathPool].IsSpawned(M_SplineMove.pathContainer.transform))
            {
                PoolManager.Pools[Const.pathPool].Despawn(M_SplineMove.pathContainer.transform);
            }
            //
            M_SplineMove.moveToPath = true;
            M_SplineMove.loopType = splineMove.LoopType.loop;

            newPathTransform = PoolManager.Pools[Const.pathPool].Spawn(
                newPath_Loop.transform,
                posStartPathLoop,
                newPath_Loop.transform.rotation
            );


            var bezier = newPathTransform.GetComponent<BezierPathManager>();
            if (bezier != null)
            {
                bezier.CalculatePath();
                M_SplineMove.SetPath(bezier);
            }
            else
            {
                var newPathManager = newPathTransform.GetComponent<PathManager>();
                M_SplineMove.SetPath(newPathManager);
            }

            pathLoopChanged = true;
        }

    }
    //------------------------------ ChangeSpeed-----------------
    bool speedChanged;

    public void ChangeSpeed()
    {
        if (!speedChanged)
        {
            if (newSpeed == null || newSpeed == 0)
            {
                Debug.LogError("Chua truyen` newSpeed vao` turn manager");
            }
            else
            {
                M_SplineMove.ChangeSpeed(newSpeed);
                speedChanged = true;
            }
        }
    }

    public void DropItem()
    {
        enemyDropItem.Drop();
    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {

        if (PoolManager.Pools[Const.enemyPoolName].IsSpawned(transform))
        {
            if (type == EnemyKilledBy.Player)
            {
                PanelUITop.Instance.SetTextScore(score);

                SoundManager.SoundEnemyDie();
                MainScene.Instance.numEnemyKilled++;
                //CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString()) + 1);
                ////daily
                //CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString()) + 1);
                //CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString()) + 1);

                if (ID == "Enemy3305_Nhangnangluong")
                {
                    if (GetComponentInChildren<Enemy305NhangnangluongFX>() != null)
                    {
                        GetComponentInChildren<Enemy305NhangnangluongFX>().SetupWhenEnemyDie();
                    }
                }
                if (ID == "EnemyLaserRoundter")
                {
                    if (GetComponentInChildren<EnemyLaserRoundter>() != null)
                    {
                        GetComponentInChildren<EnemyLaserRoundter>().StopShootLaserWhenDie();
                    }
                }

            }
            if (gameObject.tag != "Hazzard" && !MainScene.Instance.gameFinished)
            {
                MainScene.Instance.totalEnemyCanDie++;
            }
        }



        base.Die(type);


        enemyChangeAnimation = GetComponentInChildren<EnemyChangeAnimation>();
        if (enemyChangeAnimation != null)
        {
            enemyChangeAnimation.StopAllCoroutines();
        }

        syncAnimation = false;
        speedChanged = false;
      
        pathLoopChanged = false;
        isShootLoop = false;
       
        //
        M_SplineMove.moveToPath = false;
        M_SplineMove.loopType = splineMove.LoopType.none;
        //

        M_SplineMove.ChangeSpeed(speedStart);
        StopAllCoroutines();

    }
}
