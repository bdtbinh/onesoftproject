﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using SkyGameKit;


public class BulletBlastBugControll : MonoBehaviour
{
    public float timeExplosive;
    public float distance = 1.5f;
    public float timeFollow = 1f;
    public float initialVelocity;
    public float acceleration;
    public float speedRotation;

    public GameObject particleExplosive;

    private float _currentVelocity;

    public float CurrentVelocity
    {
        get
        {
            return _currentVelocity;
        }
        set
        {
            _currentVelocity = value;
        }
    }
    private GameObject playerTarget;
    private Vector3 direction = Vector3.right;
    private Transform m_target;


    private Vector3 diff;
    private Vector3 axis = Vector3.forward;

    private CompositeDisposable disUpdate = new CompositeDisposable();

    private bool nearTarget = false;
    private bool isExplosive = false;
    public void FindPlayer()
    {
        CurrentVelocity = initialVelocity;
        m_target = null;
        playerTarget = GameObject.FindGameObjectWithTag("player");

        if (playerTarget != null)
        {
            m_target = playerTarget.transform;
        }
        Observable.EveryUpdate().Subscribe(_ =>
        {
            if (m_target == null)
            {
                CurrentVelocity += acceleration * Time.deltaTime;
                transform.Translate(direction * CurrentVelocity * Time.deltaTime);
            }
            else
            {
                if (m_target.gameObject.activeInHierarchy && m_target.parent.gameObject.activeInHierarchy && !nearTarget)
                {
                    //Target active in scene
                    diff = m_target.position - transform.position;
                    float angle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
                    Quaternion q = Quaternion.AngleAxis(angle, axis);
                    transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speedRotation);

                    CurrentVelocity += acceleration * Time.deltaTime;
                    transform.Translate(direction * CurrentVelocity * Time.deltaTime);
                }
               
            }
            if (gameObject.transform.position.y < -2.5f)
            {
                nearTarget = true;
            }
            if (nearTarget && !isExplosive)
            {
                isExplosive = true;
                // Dien anim. Xong anim thi no va bien mat
                //Debug.LogError("Dien anim. Xong anim thi no va bien mat");

                Transform explosionTransform = Fu.SpawnExplosion(particleExplosive, transform.position, Quaternion.identity);
                if (explosionTransform != null)
                {
                    SgkExplosion explosion = explosionTransform.GetComponent<SgkExplosion>();
                    if (explosion != null)
                    {
                        //explosion.target = gameObject.transform;
                        
                    }
                }

                this.Delay(timeExplosive, () =>
                {
                    UbhObjectPool.instance.ReleaseBullet(bullet);
                });
            }
        }).AddTo(disUpdate);
        
    }
    UbhBullet bullet;
    private float currentVelocityStart;
    private void OnEnable()
    {
        bullet = gameObject.GetComponent<UbhBullet>();
        
    }
    private void OnDisable()
    {
        nearTarget = false;
        isExplosive = false;
        disUpdate.Clear();
    }
}
