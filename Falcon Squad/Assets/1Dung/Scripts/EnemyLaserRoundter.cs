﻿using System.Collections;
using System.Collections.Generic;
using TwoDLaserPack;
using UnityEngine;
using UnityEngine.SceneManagement;
using SkyGameKit;

public class EnemyLaserRoundter : MonoBehaviour
{
    public ParticleSystem effectLaser;
    public EnemyAction2017 enemyAction;
    //public GameObject laserOn;
    public float timeLaser = 2.5f;
    public float speedRotation = 35;
    public float timeWarning = 1.75f;
    Animator animLaser;
    //int leftRightLaser;
    public SpriteBasedLaser[] spriteBasedLaserList;
    public GameObject[] blueLaserArc;

    //public TweenRotation tweenRotLaser;
    //Vector3 laserToLeft, laserToRight;

    void Start()
    {
        animLaser = GetComponent<Animator>();

        foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
        {
            spriteBasedLaserScript.OnLaserHitTriggered += LaserOnOnLaserHitTriggered;
            spriteBasedLaserScript.SetLaserState(false);
        }

       
        //laserToLeft = new Vector3(0f, 0f, 90f);
        //laserToRight = new Vector3(0f, 0f, -90f);
    }



    IEnumerator StartShootLaser(float timeLoop)
    {

        while (true)
        {
            yield return new WaitForSeconds(timeLoop);
            if (gameObject.activeInHierarchy)
            {
                ShootLaserStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    bool isShotLaserLoop;
    public void ShootLaserLoop(float timeLoop)
    {
        ShootLaserStep1();
        if (!isShotLaserLoop)
        {
            isShotLaserLoop = true;
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine(StartShootLaser(timeLoop));
            }
        }
    }

    public void ShootLaserStep1()
    {

        //tweenRotUbhShot.enabled = true;
        //tweenRotUbhShot.ResetToBeginning ();

        //Debug.LogError("ShootLaserStep1 ");
        //animLaser.SetTrigger("isLaserAttack2");
        effectLaser.gameObject.SetActive(true);
        effectLaser.Play();
        //laserOn.SetActive(true);
        this.Delay(1f, () => {
            for (int i = 0; i < blueLaserArc.Length; i++)
            {
                blueLaserArc[i].SetActive(true);
            }
            StartCoroutine("ShootLaserStep2");
        });
       
    }

    IEnumerator ShootLaserStep2()
    {
        yield return new WaitForSeconds(timeWarning);
        for (int i = 0; i < blueLaserArc.Length; i++)
        {
            blueLaserArc[i].SetActive(false);
        }
        if (gameObject.activeInHierarchy)
        {
            effectLaser.Stop();
            effectLaser.gameObject.SetActive(false);
            //enemyAction.M_SplineMove.Pause();
            foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
            {
                spriteBasedLaserScript.SetLaserState(true);
            }
        }
        else
        {
            StopAllCoroutines();
        }

        this.Delay(timeLaser, () => { FinishTweenShootLaser(); });
    }

    public void FinishTweenShootLaser()
    {

        StartCoroutine("StopShootLaser");
    }

    IEnumerator StopShootLaser()
    {
        yield return new WaitForSeconds(0.1f);

        if (gameObject.activeInHierarchy)
        {
            foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
            {
                spriteBasedLaserScript.SetLaserState(false);
            }
            //laserOn.SetActive(false);

            //enemyAction.M_SplineMove.Resume();
        }
        else
        {
            StopAllCoroutines();
        }
    }

    private void FixedUpdate()
    {
        if (gameObject.activeInHierarchy)
        {
            gameObject.transform.eulerAngles += new Vector3(0, 0, 2) * speedRotation * Time.fixedDeltaTime;
        }
    }

    public void StopShootLaserWhenDie()
    {
        foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
        {
            spriteBasedLaserScript.SetLaserState(false);
        }
        isShotLaserLoop = false;
        StopAllCoroutines();
    }


    private void LaserOnOnLaserHitTriggered(RaycastHit2D hitInfo)
    {
        //		Debug.LogError ("LaserOnOnLaserHitTriggered Player");

        if (hitInfo.collider.tag == "player" && !MainScene.Instance.gameFinished && !hitInfo.collider.GetComponent<Aircraft>().aircraftIsReloading && !hitInfo.collider.GetComponent<Aircraft>().aircraftUsingActiveShield && !MainScene.Instance.gameStopping && !MainScene.Instance.bossDead)
        {
            //hitInfo.collider.GetComponent<PlayerCollision> ().Pool_EffectPlayerDie ();
            //hitInfo.collider.GetComponent<PlayerController> ().PlayerDie ();
            if (SceneManager.GetActiveScene().name == "LevelTutorial")
            {
                hitInfo.collider.GetComponent<Aircraft>().playerInitScript.ChangeNumberLife(-9999);
            }
            hitInfo.collider.GetComponent<Aircraft>().playerControllerFalconScript.PlayerDie();
        }
    }

}
