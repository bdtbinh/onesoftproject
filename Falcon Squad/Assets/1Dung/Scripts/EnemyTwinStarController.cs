﻿using System.Collections;
using UnityEngine;


public class EnemyTwinStarController : EnemyData
{
    public EnemyTwinStarChild[] enemyTwinStarChild;
    EnemyDropItem enemyDropItem;

    private EnemyTwinStarChild enemyChild;

    private void Start()
    {
        enemyChild = enemyTwinStarChild[0];
        enemyDropItem = GetComponent<EnemyDropItem>();
    }
    int temp;
    float time = -1;
    bool isChangeSpeedAndShootLoop;
    public void ChangeSpeedAndMoveLoop(float timeLoop)
    {
        if (!isChangeSpeedAndShootLoop)
        {
            isChangeSpeedAndShootLoop = true;
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine(StartChangeSpeedAndLoop(timeLoop));
            }
        }
    }
    public IEnumerator StartChangeSpeedAndLoop(float timeLoop)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeLoop);
            if (gameObject.activeInHierarchy)
            {
                temp++;
                if (temp % 2 == 0)
                {
                    enemyChild = enemyTwinStarChild[0];
                }
                else
                {
                    enemyChild = enemyTwinStarChild[1];
                }

                enemyChild.ChangeSpeed();
                //Debug.LogError("Change Speed In Parent");
            }
            else
            {
                StopAllCoroutines();
            }
        }
    }
    public void DropItem()
    {
        enemyDropItem.Drop();
    }

}
