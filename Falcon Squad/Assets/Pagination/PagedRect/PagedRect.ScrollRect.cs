﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UI.Pagination
{
    public partial class PagedRect
    {
        private Coroutine scrollCoroutine = null;
        public void CenterScrollRectOnCurrentPage(bool initial = false)
        {
            if (NumberOfPages == 0) return;

            ScrollRect.ResetDragOffset = true;

            if (Application.isPlaying && !initial)
            {
                if (scrollCoroutine != null) StopCoroutine(scrollCoroutine);
                scrollCoroutine = StartCoroutine(ScrollToDesiredPosition());
            }
            else
            {
                SetScrollRectPosition();
            }
        }

        protected void SetScrollRectPosition()
        {
            if (ShowPagePreviews) HandlePagePreviewPreferredSizes();

            float offset = NumberOfPages > 0 ? GetDesiredScrollRectOffset() : 0f;

            if (ScrollRect.horizontal)
            {
                ScrollRect.content.anchoredPosition = new Vector2(offset, 0);
            }
            else
            {
                ScrollRect.content.anchoredPosition = new Vector2(0, offset);
            }
        }

        protected IEnumerator ScrollToDesiredPosition()
        {
            float percentageComplete = 0f;

            if (ShowPagePreviews) HandlePagePreviewScaling();

            float offset = GetDesiredScrollRectOffset();

            // positioning
            scrollRectAnimation_DesiredPosition = Vector2.zero;
            scrollRectAnimation_InitialPosition = ScrollRect.content.anchoredPosition;

            if (ScrollRect.horizontal)
            {
                scrollRectAnimation_DesiredPosition.x = offset;
                scrollRectAnimation_InitialPosition.y = 0;
            }
            else
            {
                scrollRectAnimation_DesiredPosition.y = offset;
                scrollRectAnimation_InitialPosition.x = 0;
            }

            float timeStartedMoving = Time.time;
            while (percentageComplete < 1f)
            {
                float timeSinceStarted = Time.time - timeStartedMoving;
                percentageComplete = timeSinceStarted / (0.25f / AnimationSpeed);

                ScrollRect.content.anchoredPosition = Vector2.Lerp(scrollRectAnimation_InitialPosition, scrollRectAnimation_DesiredPosition, percentageComplete);                

                yield return null;
            }

            ScrollRect.content.anchoredPosition = scrollRectAnimation_DesiredPosition;
        }

        protected int GetClosestPageNumberToScrollRectCenter()
        {
            return GetPageDistancesFromScrollRectCenter().OrderBy(d => d.Value).FirstOrDefault().Key;
        }

        protected Dictionary<int, float> GetPageDistancesFromScrollRectCenter()
        {
            var scrollRectRectTransform = (RectTransform)ScrollRect.transform;
            var centerOfScrollRect = (Vector2)(scrollRectRectTransform.position);

            Dictionary<int, float> pageDistances = new Dictionary<int, float>();
            var pageContainer = Viewport.transform as RectTransform;

            var childCount = pageContainer.childCount;

            // this is an attempt to ensure that pageDistances is always in the correct order
            for (var x = 0; x < childCount; x++)
            {
                var transform = pageContainer.GetChild(x);
                if (!transform.gameObject.activeInHierarchy) continue;

                var page = transform.GetComponent<Page>();
                if (page == null) continue;

                float distanceToCenterOfScrollRect = 0f;
                var rectTransform = transform as RectTransform;

                if (ScrollRect.horizontal)
                {
                    centerOfScrollRect = new Vector2(centerOfScrollRect.x, 0);
                    var pagePosition = new Vector2(rectTransform.position.x, 0);
                    if (rectTransform.pivot.x == 0)
                    {
                        pagePosition.x += rectTransform.rect.width / 2f;
                    }
                    else if(rectTransform.pivot.x == 1)
                    {
                        pagePosition.x -= rectTransform.rect.width / 2f;
                    }

                    distanceToCenterOfScrollRect = Vector2.Distance(centerOfScrollRect, pagePosition);
                }
                else
                {
                    centerOfScrollRect = new Vector2(0, centerOfScrollRect.y);
                    var pagePosition = new Vector2(0, rectTransform.position.y);

                    if (rectTransform.pivot.y == 0)
                    {
                        pagePosition.y += rectTransform.rect.height / 2f;
                    }
                    else if (rectTransform.pivot.y == 1)
                    {
                        pagePosition.y -= rectTransform.rect.height / 2f;
                    }

                    distanceToCenterOfScrollRect = Vector2.Distance(centerOfScrollRect, pagePosition);
                }                

                pageDistances.Add(page.PageNumber, distanceToCenterOfScrollRect);
            }

            return pageDistances;
        }

        protected float GetDesiredScrollRectOffset()
        {
            return GetPageOffset(GetCurrentPage());
            /*if (ShowPagePreviews) return GetDesiredScrollRectOffset_PagePreviews();

            float offset = 0;
            var pagesBeforeCurrent = GetPagePosition(CurrentPage) - 1;
            var pageSize = sizingTransform.rect;

            if (ScrollRect.horizontal)
            {
                offset -= (pageSize.width + SpaceBetweenPages) * pagesBeforeCurrent;
            }
            else
            {
                offset += (pageSize.height + SpaceBetweenPages) * pagesBeforeCurrent;
            }

            return offset;*/
        }

        public float GetPageOffset(Page page)
        {
            if (ShowPagePreviews) return GetPageOffset_PagePreviews(page);

            float offset = 0;
            var pagesBeforeDesiredPage = GetPagePosition(page.PageNumber) - 1;
            var pageSize = sizingTransform.rect;

            if (ScrollRect.horizontal)
            {
                offset -= (pageSize.width + SpaceBetweenPages) * pagesBeforeDesiredPage;
            }
            else
            {
                offset += (pageSize.height + SpaceBetweenPages) * pagesBeforeDesiredPage;
            }

            return offset;
        }
    }
}
