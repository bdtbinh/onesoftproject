using System;
using System.Collections.Generic;

/// <summary>
/// 不要手动更改，由ConfigEditor自动生成的配置文件（模板为GetterTemplete）
/// </summary>
public class DailyLoginConfig
{
	//属性
	public string NAME_ID;//rương theo ngày
	public string typeGiftList;//list type gift
	public float numGiftList;//list num gift
	

	#region 静态方法
	public static Dictionary<string, DailyLoginConfig> Map;

	public static DailyLoginConfig Get(string NAME_ID)
	{
		return Map[NAME_ID];
	}
	public static void Parse(string cfgStr)
	{
		Map = new Dictionary<string, DailyLoginConfig>();
		string[][] configArray = ConfigUtils.ParseConfig(cfgStr);
		int len = configArray.Length;
        
		for(int i = 3;i<len;i++)
		{
			string[] args = configArray[i];
			DailyLoginConfig cfg = new DailyLoginConfig();
			
			cfg.NAME_ID = Convert.ToString(args[0]);
			cfg.typeGiftList = Convert.ToString(args[1]);
			cfg.numGiftList = float.Parse(args[2]);
			
			
			Map[cfg.NAME_ID] = cfg;
		}
	}
	#endregion
}
