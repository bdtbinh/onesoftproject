using System;
using System.Collections.Generic;

/// <summary>
/// 不要手动更改，由ConfigEditor自动生成的配置文件（模板为GetterTemplete）
/// </summary>
public class ShopUpgradeConfig
{
	//属性
	public string NAME_ID;//key map string
	public uint totalLevel;//tong level
	public float[] list_PriceValue;//list Level
	

	#region 静态方法
	public static Dictionary<string, ShopUpgradeConfig> Map;

	public static ShopUpgradeConfig Get(string NAME_ID)
	{
		return Map[NAME_ID];
	}
	public static void Parse(string cfgStr)
	{
		Map = new Dictionary<string, ShopUpgradeConfig>();
		string[][] configArray = ConfigUtils.ParseConfig(cfgStr);
		int len = configArray.Length;
        
		for(int i = 3;i<len;i++)
		{
			string[] args = configArray[i];
			ShopUpgradeConfig cfg = new ShopUpgradeConfig();
			
			cfg.NAME_ID = Convert.ToString(args[0]);
			cfg.totalLevel = uint.Parse(args[1]);
			cfg.list_PriceValue = ConfigUtils.ParseArray<float>(args[2], float.Parse);
			
			
			Map[cfg.NAME_ID] = cfg;
		}
	}
	#endregion
}
