using System;
using System.Collections.Generic;

/// <summary>
/// 不要手动更改，由ConfigEditor自动生成的配置文件（模板为GetterTemplete）
/// </summary>
public class LuckyWheelConfig
{
	//属性
	public string NAME_ID;//SLOT_NO
	public string nameItem;//loại item
	public int numItem;//so luong
	public int rate;//tong 100
	

	#region 静态方法
	public static Dictionary<string, LuckyWheelConfig> Map;

	public static LuckyWheelConfig Get(string NAME_ID)
	{
		return Map[NAME_ID];
	}
	public static void Parse(string cfgStr)
	{
		Map = new Dictionary<string, LuckyWheelConfig>();
		string[][] configArray = ConfigUtils.ParseConfig(cfgStr);
		int len = configArray.Length;
        
		for(int i = 3;i<len;i++)
		{
			string[] args = configArray[i];
			LuckyWheelConfig cfg = new LuckyWheelConfig();
			
			cfg.NAME_ID = Convert.ToString(args[0]);
			cfg.nameItem = Convert.ToString(args[1]);
			cfg.numItem = int.Parse(args[2]);
			cfg.rate = int.Parse(args[3]);
			
			
			Map[cfg.NAME_ID] = cfg;
		}
	}
	#endregion
}
