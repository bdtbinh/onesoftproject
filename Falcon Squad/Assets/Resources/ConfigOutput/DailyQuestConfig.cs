using System;
using System.Collections.Generic;

/// <summary>
/// 不要手动更改，由ConfigEditor自动生成的配置文件（模板为GetterTemplete）
/// </summary>
public class DailyQuestConfig
{
	//属性
	public string NAME_ID;//key map string
	public string typeQuest;//type Quest
	public uint levelQuest;//level Quest
	public uint valueQuest;//list level
	

	#region 静态方法
	public static Dictionary<string, DailyQuestConfig> Map;

	public static DailyQuestConfig Get(string NAME_ID)
	{
		return Map[NAME_ID];
	}
	public static void Parse(string cfgStr)
	{
		Map = new Dictionary<string, DailyQuestConfig>();
		string[][] configArray = ConfigUtils.ParseConfig(cfgStr);
		int len = configArray.Length;
        
		for(int i = 3;i<len;i++)
		{
			string[] args = configArray[i];
			DailyQuestConfig cfg = new DailyQuestConfig();
			
			cfg.NAME_ID = Convert.ToString(args[0]);
			cfg.typeQuest = Convert.ToString(args[1]);
			cfg.levelQuest = uint.Parse(args[2]);
			cfg.valueQuest = uint.Parse(args[3]);
			
			
			Map[cfg.NAME_ID] = cfg;
		}
	}
	#endregion
}
