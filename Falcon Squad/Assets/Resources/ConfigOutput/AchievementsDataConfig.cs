using System;
using System.Collections.Generic;

/// <summary>
/// 不要手动更改，由ConfigEditor自动生成的配置文件（模板为GetterTemplete）
/// </summary>
public class AchievementsDataConfig
{
	//属性
	public string NAME_ID;//key map string
	public string[] list_Value;//list level
	

	#region 静态方法
	public static Dictionary<string, AchievementsDataConfig> Map;

	public static AchievementsDataConfig Get(string NAME_ID)
	{
		return Map[NAME_ID];
	}
	public static void Parse(string cfgStr)
	{
		Map = new Dictionary<string, AchievementsDataConfig>();
		string[][] configArray = ConfigUtils.ParseConfig(cfgStr);
		int len = configArray.Length;
        
		for(int i = 3;i<len;i++)
		{
			string[] args = configArray[i];
			AchievementsDataConfig cfg = new AchievementsDataConfig();
			
			cfg.NAME_ID = Convert.ToString(args[0]);
			cfg.list_Value = ConfigUtils.ParseArray<string>(args[1], Convert.ToString);
			
			
			Map[cfg.NAME_ID] = cfg;
		}
	}
	#endregion
}
