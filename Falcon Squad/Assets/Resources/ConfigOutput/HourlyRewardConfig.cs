using System;
using System.Collections.Generic;

/// <summary>
/// 不要手动更改，由ConfigEditor自动生成的配置文件（模板为GetterTemplete）
/// </summary>
public class HourlyRewardConfig
{
	//属性
	public string NAME_ID;//Gold nhan theo list
	public float numGift;//list num gift
	

	#region 静态方法
	public static Dictionary<string, HourlyRewardConfig> Map;

	public static HourlyRewardConfig Get(string NAME_ID)
	{
		return Map[NAME_ID];
	}
	public static void Parse(string cfgStr)
	{
		Map = new Dictionary<string, HourlyRewardConfig>();
		string[][] configArray = ConfigUtils.ParseConfig(cfgStr);
		int len = configArray.Length;
        
		for(int i = 3;i<len;i++)
		{
			string[] args = configArray[i];
			HourlyRewardConfig cfg = new HourlyRewardConfig();
			
			cfg.NAME_ID = Convert.ToString(args[0]);
			cfg.numGift = float.Parse(args[1]);
			
			
			Map[cfg.NAME_ID] = cfg;
		}
	}
	#endregion
}
