using System;
using System.Collections.Generic;

/// <summary>
/// 不要手动更改，由ConfigEditor自动生成的配置文件（模板为GetterTemplete）
/// </summary>
public class DailyQuestGiftConfig
{
	//属性
	public string NAME_ID;//rương theo mốc
	public string[] typeGiftList;//list type gift
	public float[] numGiftList;//list num gift
	

	#region 静态方法
	public static Dictionary<string, DailyQuestGiftConfig> Map;

	public static DailyQuestGiftConfig Get(string NAME_ID)
	{
		return Map[NAME_ID];
	}
	public static void Parse(string cfgStr)
	{
		Map = new Dictionary<string, DailyQuestGiftConfig>();
		string[][] configArray = ConfigUtils.ParseConfig(cfgStr);
		int len = configArray.Length;
        
		for(int i = 3;i<len;i++)
		{
			string[] args = configArray[i];
			DailyQuestGiftConfig cfg = new DailyQuestGiftConfig();
			
			cfg.NAME_ID = Convert.ToString(args[0]);
			cfg.typeGiftList = ConfigUtils.ParseArray<string>(args[1], Convert.ToString);
			cfg.numGiftList = ConfigUtils.ParseArray<float>(args[2], float.Parse);
			
			
			Map[cfg.NAME_ID] = cfg;
		}
	}
	#endregion
}
