using UnityEngine;
/// <summary>
/// 不要手动更改，由ConfigEditor自动生成的配置文件（模板为LoaderTemplete）
/// </summary>
public class ConfigLoader
{
	public static void Load()
	{
        //各配置加载自动生成在此行下面
		int startIndex = ConfigManagerSettings.FilesURL.LastIndexOf("Resources/") + 10;
		int length = ConfigManagerSettings.FilesURL.Length - startIndex;
		string relativeURL = ConfigManagerSettings.FilesURL.Substring(startIndex, length);

		string AchievementsDataText = Resources.Load<TextAsset>(relativeURL + "/" + "AchievementsData").text;
		AchievementsDataConfig.Parse(AchievementsDataText);

		string DailyLoginText = Resources.Load<TextAsset>(relativeURL + "/" + "DailyLogin").text;
		DailyLoginConfig.Parse(DailyLoginText);

		string DailyQuestText = Resources.Load<TextAsset>(relativeURL + "/" + "DailyQuest").text;
		DailyQuestConfig.Parse(DailyQuestText);

		string DailyQuestGiftText = Resources.Load<TextAsset>(relativeURL + "/" + "DailyQuestGift").text;
		DailyQuestGiftConfig.Parse(DailyQuestGiftText);

		string EquipText = Resources.Load<TextAsset>(relativeURL + "/" + "Equip").text;
		EquipConfig.Parse(EquipText);

		string HourlyRewardText = Resources.Load<TextAsset>(relativeURL + "/" + "HourlyReward").text;
		HourlyRewardConfig.Parse(HourlyRewardText);

		string LuckyWheelText = Resources.Load<TextAsset>(relativeURL + "/" + "LuckyWheel").text;
		LuckyWheelConfig.Parse(LuckyWheelText);

		string MonsterText = Resources.Load<TextAsset>(relativeURL + "/" + "Monster").text;
		MonsterConfig.Parse(MonsterText);

		string ShopUpgradeText = Resources.Load<TextAsset>(relativeURL + "/" + "ShopUpgrade").text;
		ShopUpgradeConfig.Parse(ShopUpgradeText);

		string TestTypesText = Resources.Load<TextAsset>(relativeURL + "/" + "TestTypes").text;
		TestTypesConfig.Parse(TestTypesText);


	}
}