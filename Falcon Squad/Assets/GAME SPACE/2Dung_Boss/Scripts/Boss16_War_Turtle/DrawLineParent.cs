﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLineParent : MonoBehaviour {

    public DrawLine[] drawLines;
    public Transform vortexParent;
     Transform player;
    private Transform bossObj;
    private void Start()
    {
        bossObj = GameObject.FindGameObjectWithTag("Boss7").transform;
        player = GameObject.FindGameObjectWithTag("player").transform;

        transform.position = new Vector3(bossObj.position.x + vortexParent.position.x, player.position.y);
    }
    public void StartSraw()
    {


        for(int i = 0; i < drawLines.Length; i++)
        {
            drawLines[i].StartDrawLine();
        }
    }
}
