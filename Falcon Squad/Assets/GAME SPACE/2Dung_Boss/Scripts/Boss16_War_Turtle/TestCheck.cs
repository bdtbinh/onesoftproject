﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using DG.Tweening;
public class TestCheck : MonoBehaviour
{  
    public LineRenderer line;

    private Vector3 startPos;    // Start position of line
    private Vector3 endPos;    // End position of line
    private float distance;

    private float Y = 0;
    bool isStart;

    Transform player;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("player").transform;
        startPos = transform.position;
        endPos = new Vector3(startPos.x, player.position.y);
        distance = Vector3.Distance(startPos, endPos);
        CreateLine();
        this.Delay(1f, () => {
            StartDraw();
        });
        
    }
    public DrawLine[] drawLine;
    public void StartDraw()
    {
        line.SetPosition(0, startPos);
        Y = startPos.y;
        //StartCoroutine(DrawLine())
        DOTween.Sequence().Append(DOTween.To(() => Y, x => Y = x, endPos.y, Time.deltaTime).OnStepComplete(() =>
        {
            line.SetPosition(1, new Vector3( endPos.x ,Y));
            AddColliderToLine(startPos , new Vector3(endPos.x,Y));
            this.Delay(1f, () => { line.SetPosition(0, new Vector3(endPos.x, Y)); });
        }).OnComplete(()=> 
        {
            for(int i = 0; i < drawLine.Length; i++)
            {
                drawLine[i].StartDrawLine();
            }
        }));
    }

    //IEnumerator DrawLine(float time)
    //{
    //    yield return new WaitForSeconds(time);
    //}
    void Update()
    {

    }
    // Following method creates line runtime using Line Renderer component
    private void CreateLine()
    {
        line.SetVertexCount(2);
        line.SetWidth(1f, 1f);      
        line.useWorldSpace = true;
    }
    // Following method adds collider to created line
    private void AddColliderToLine(Vector3 startPos, Vector3 endPos)
    {
        BoxCollider2D col = new GameObject("Collider").AddComponent<BoxCollider2D>();
        col.transform.parent = line.transform; // Collider is added as child object of line
        float lineLength = Vector3.Distance(startPos, endPos); // length of line
        col.size = new Vector3(lineLength, 0.1f, 1f); // size of collider is set where X is length of line, Y is width of line, Z will be set as per requirement
        Vector3 midPoint = (startPos + endPos) / 2;
        col.transform.position = midPoint; // setting position of collider object
        // Following lines calculate the angle between startPos and endPos
        float angle = (Mathf.Abs(startPos.y - endPos.y) / Mathf.Abs(startPos.x - endPos.x));
        if ((startPos.y < endPos.y && startPos.x > endPos.x) || (endPos.y < startPos.y && endPos.x > startPos.x))
        {
            angle *= -1;
        }
        angle = Mathf.Rad2Deg * Mathf.Atan(angle);
        col.transform.Rotate(0, 0, angle);

    }
}

