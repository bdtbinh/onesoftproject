﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;
public class MirrorPosController : MonoBehaviour
{
    splineMove splineMove;
    //Vector3 startPos=Vector3.zero;

    private void OnEnable()
    {
        if (splineMove == null)
        {
            splineMove = GetComponent<splineMove>();
            //.pathContainer = 
        }
        //if (startPos == Vector3.zero)
        //{
        //    startPos = transform.localPosition;
        //}
        Move();
    }

    public void Move()
    {
        splineMove.StartMove();
    }

    public void HideGameObj()
    {
        gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        splineMove.Pause();
        transform.localPosition = Vector3.zero;
    }
}
