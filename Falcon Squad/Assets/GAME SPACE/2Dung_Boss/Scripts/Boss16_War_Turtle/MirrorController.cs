﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using SWS;
using DG.Tweening;
public class MirrorController : MonoBehaviour
{
    //public BossWarTurtleAction bossObj;
    public MirrorElementControll[] mirrorElement;
    public float timeHideMirror;

    //public splineMove[] splineMoveMirrorElement = new splineMove[3];
    //Vector3[] arrposition = { new Vector3(-2.11f, -1.79f, 0f), new Vector3(2.36f, -1.8f, 0), new Vector3(0, -2.57f, 0) };

    [HideInInspector]
    public bool isShow;
    public void ShowMirror(int number)
    {
        StartCoroutine(StartShowMirror(number));
    }

    public IEnumerator StartShowMirror(int number)
    {
        isShow = true;
        yield return null;
        for (int i = 0; i < number; i++)
        {
            mirrorElement[i].gameObject.SetActive(true);
        }
    }
    public void HideAllMirror()
    {
        isShow = false;
        for (int i = 0; i < 3; i++)
        {
            mirrorElement[i].StartHideMirrorOverride();
        }

    }
}
