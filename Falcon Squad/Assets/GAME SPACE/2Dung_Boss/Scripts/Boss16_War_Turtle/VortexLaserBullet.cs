﻿using DG.Tweening;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VortexLaserBullet : MonoBehaviour
{
    public LineRenderer line;
    public Transform player;

    private Vector3 startPos;    // Start position of line
    private Vector3 endPos;    // End position of line
    private float distance;

    public Transform lineParent;
    //public ParticleSystem effect;
    public DrawLine[] drawLines;

    List<Vector3> arrPoint = new List<Vector3>();
    EdgeCollider2D collider;

    
    private void OnEnable()
    {
        player = GameObject.FindGameObjectWithTag("player").transform;
        collider = GetComponent<EdgeCollider2D>();
        collider.enabled = true;
        StartCoroutine(Draw());
    }

    private void OnDisable()
    {

        endPos = Vector3.zero;
        arrPoint.Clear();
        player = null;
    }
    //public IEnumerator StartDraw(float time)
    //{
    //    yield return new WaitForSeconds(time);
    //    Draw();
    //}
    public IEnumerator Draw()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("player").transform;
        }

        for (int i = 0; i < 16; i++)
        {
            arrPoint.Add(Vector3.zero);
        }
        line.SetPositions(arrPoint.ToArray());
        line.startWidth = 1.2f;
        line.endWidth = 1.2f;
        collider.edgeRadius = 0.2f;
        startPos = Vector3.zero;
        Vector3 playerCache = player.transform.position;

        if (playerCache.y > 2)
        {
            playerCache.y = -4f;
        }

        endPos = new Vector3(startPos.x, playerCache.y);
        //endPos = new Vector3(startPos.x, (playerCache.y));

        //lineParent.position = new Vector3(transform.position.x, endPos.y, 0);
        lineParent.position = new Vector3(transform.position.x, endPos.y + transform.position.y, 0);
        //effect.gameObject.transform.position = lineParent.position;

        float pointX = (startPos.x - endPos.x) / 16;
        float pointY = (startPos.y - endPos.y) / 16;


        for (int i = 1; i < 16; i++)
        {
            arrPoint[i] = new Vector3((startPos.x + pointX * i), (startPos.y + pointY * i));
        }

        
        line.SetVertexCount(1);
        line.SetPosition(0, new Vector3(arrPoint[0].x, 0, 0));

        for (int i = 1; i < arrPoint.Count-1; i++)
        {
            yield return new WaitForSeconds(0.04f);

            line.SetVertexCount(i + 1);
            line.SetPosition(i, arrPoint[i]);
            if (i == 4)
            {
                StartCoroutine(HideLine());
            }
        }
        //effect.Play();
        for (int i = 0; i < drawLines.Length; i++)
        {
            drawLines[i].StartDrawLine();
        }

        collider.points = ConvertArray(arrPoint.ToArray());
    }

    public IEnumerator HideLine()
    {
        List<Vector3> arr = new List<Vector3>();
        arr = arrPoint;
        //yield return new WaitForSeconds(0.1f);
        while (arrPoint.Count > 0)
        {
            yield return new WaitForSeconds(0.04f);
            if (arr.Count == 0)
            {
                break;
            }
            else
            {
                arr.RemoveAt(0);
                line.SetVertexCount(arr.Count);
                line.SetPositions(arr.ToArray());
                collider.points = ConvertArray(arr.ToArray());
            }
        }
        collider.enabled = false;    
    }

    Vector2[] ConvertArray(Vector3[] v3)
    {
        Vector2[] v2 = new Vector2[v3.Length];
        for (int i = 0; i < v3.Length; i++)
        {
            Vector3 tempV3 = v3[i];
            v2[i] = new Vector2(tempV3.x, tempV3.y);
        }
        return v2;
    }
}
