﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class BulletInOverrideController : MonoBehaviour
{

    //public UbhShotCtrl gun;
    Vector3 startPosition;
    public float timeNextStep = 0.75f;
    public ParticleSystem effect;
    public CircleCollider2D child;
    private void OnEnable()
    {
        child.enabled = false;
        effect.gameObject.SetActive(true);
        effect.Play();
        StartCoroutine(StartShoot(timeNextStep));
    }
    IEnumerator StartShoot(float time)
    {
        yield return new WaitForSeconds(time);
        Shoot();
    }

    public void Shoot()
    {
        child.enabled = true;
        this.Delay(1f, () => 
        {
            HideBullet();
        });
    }

    public void HideBullet()
    {
        this.gameObject.SetActive(false);
    }

}
