﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class IceSpiritController : MonoBehaviour
{
    public GameObject[] icySpiritBullet;
    public GameObject bulletInOverridePrefab;
    public GameObject mirrorParentOverridePrefab;
    public GameObject mirrorParentPrefab;
    public BossWarTurtleAction bossObj;
    //public Vector3[] positionIce;

    GameObject[,] arrIcySpiritBullet = new GameObject[3, 5];
    GameObject[] arrIcySpiritBullet_phase2 = new GameObject[5];
    GameObject[] arrIcySpiritBullet_phase3 = new GameObject[5];
    GameObject[] arrBulletInOverride = new GameObject[5];

    GameObject mirrorParent;

    public Vector3[] listPositionIce;
    private void Start()
    {
        for (int j = 0; j < 3; j++)
        {
            for (int i = 0; i < 5; i++)
            {
                arrIcySpiritBullet[j, i] = Instantiate(icySpiritBullet[j], transform.position, Quaternion.identity) as GameObject;
                arrIcySpiritBullet[j, i].SetActive(false);
            }
        }

        for (int i = 0; i < 5; i++)
        {
            arrBulletInOverride[i] = Instantiate(bulletInOverridePrefab, transform.position, Quaternion.identity) as GameObject;
        }
        bossObj.mirrorControllerOverride = Instantiate(mirrorParentOverridePrefab, new Vector3(0, 5, 0), Quaternion.identity).GetComponent<MirrorController>();
        bossObj.mirrorController = Instantiate(mirrorParentPrefab, new Vector3(0, 5f, 0), Quaternion.identity).GetComponent<MirrorController>();
    }
    public void ActiveBullet(int phase, int number, Vector3 position)
    {
        if (listPositionIce.Length == 0)
        {
            arrIcySpiritBullet[phase, number].transform.position = position;
        }
        else
        {
            arrIcySpiritBullet[phase, number].transform.position = listPositionIce[Random.Range(0, listPositionIce.Length)];
        }
        arrIcySpiritBullet[phase, number].SetActive(true);
    }

    public void ActiveBulletInOverride(int number, Vector3 position)
    {
        arrBulletInOverride[number].transform.position = position + new Vector3(Random.Range(0, 1), Random.Range(0, 1));
        arrBulletInOverride[number].SetActive(true);
    }

}
