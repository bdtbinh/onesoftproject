﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class IceSpiritBullet : MonoBehaviour
{
    public UbhShotCtrl gun;
    Vector3 startPosition;
    public ParticleSystem effect;
    public GameObject child;
    Transform player;
    EnemyAction2017 enemyIns;
    public ParticleSystem effectShoot;
    public float timeHide;
    public float timeDelayShoot;
    //SpriteRenderer spriteRenderer;
    private void OnEnable()
    {
        //if (spriteRenderer == null)
        //{
        //    spriteRenderer = child.GetComponent<SpriteRenderer>();
        //}
        if (enemyIns == null)
        {
            enemyIns = GetComponent<EnemyAction2017>();
        }
        //enemyIns.maxHP = 2500;
        enemyIns.currentHP = enemyIns.maxHP;
        effect.gameObject.SetActive(true);
        
        //spriteRenderer.color = new Color32(255, 255, 255, 0);
        //temp = 0;
        //StartCoroutine(AddAnphal());
        StartCoroutine(StartShoot(2.8f));
        this.Delay(timeHide, () => { HideBullet(); });
    }

    IEnumerator StartShoot(float time)
    {
        effectShoot.Play();
        yield return new WaitForSeconds(timeDelayShoot);
        Shoot();
    }

    public void Shoot()
    {
        if (gun != null)
        {
            gun.StartShotRoutine();
        }
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(StartShoot(timeDelayShoot));
        }
        else
        {
            StopCoroutine(StartShoot(timeDelayShoot));
        }
    }


    public void HideBullet()
    {
        this.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("player").transform;
        }
        if (child.activeInHierarchy)
        {
            Vector3 direction = player.transform.position - child.transform.position;
            HelperDung.LookAtToDirection(direction, child, 1000f);
        }
        if (enemyIns.currentHP <= 0)
        {
            gameObject.SetActive(false);
        }

    }
}
