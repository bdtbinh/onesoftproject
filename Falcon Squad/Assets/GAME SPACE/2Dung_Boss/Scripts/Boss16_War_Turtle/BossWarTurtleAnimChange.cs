﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWarTurtleAnimChange : MonoBehaviour
{
    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    public void ChangAnimStep1()
    {
        anim.SetTrigger("isAttack1");
    }
    public void ChangeAnimStep2()
    {
        anim.SetTrigger("isAttack2");
    }
    public void ChangeAnimStep3()
    {
        anim.SetTrigger("isAttack3");
    }
    public void ChangeAnimToIdle()
    {
        anim.SetTrigger("isIdle");
    }
}
