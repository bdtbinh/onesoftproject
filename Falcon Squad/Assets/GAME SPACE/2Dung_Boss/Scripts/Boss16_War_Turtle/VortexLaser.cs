﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using TwoDLaserPack;
using UnityEngine.SceneManagement;

public class VortexLaser : MonoBehaviour
{
    //public ParticleSystem effectLaser;
    //public GameObject laserOn;
    public float timeLaser = 1.5f;
    public float speedRotation = 35;
    //Animator animLaser;
    //int leftRightLaser;
    public SpriteBasedLaser[] spriteBasedLaserList;
    public GameObject[] blueLaserArc;  

    void Start()
    {
        drawLine = Instantiate(drawLinePrefabs,new Vector3(0,12,0),Quaternion.identity).GetComponent<DrawLineParent>();
        
        foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
        {
            spriteBasedLaserScript.OnLaserHitTriggered += LaserOnOnLaserHitTriggered;
            spriteBasedLaserScript.SetLaserState(false);
        }
    }
    public void ShootLaserStep1()
    {

        //		tweenRotUbhShot.enabled = true;
        //		tweenRotUbhShot.ResetToBeginning ();

        //Debug.LogError("ShootLaserStep1 ");
        //animLaser.SetTrigger("isLaserAttack2");
        //effectLaser.gameObject.SetActive(true);
        //effectLaser.Play();
        //laserOn.SetActive(true);

        for (int i = 0; i < blueLaserArc.Length; i++)
        {
            blueLaserArc[i].SetActive(true);
        }
        StartCoroutine("ShootLaserStep2");
    }

    IEnumerator ShootLaserStep2()
    {
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < blueLaserArc.Length; i++)
        {
            blueLaserArc[i].SetActive(false);
        }
        if (gameObject.activeInHierarchy)
        {

            //effectLaser.Stop();
            //effectLaser.gameObject.SetActive(false);
            
            foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
            {
                spriteBasedLaserScript.SetLaserState(true);
            }
        }
        else
        {
            StopAllCoroutines();
        }

        this.Delay(timeLaser, () => { FinishTweenShootLaser(); });
    }

    public void FinishTweenShootLaser()
    {        
        StartCoroutine("StopShootLaser");
    }

    IEnumerator StopShootLaser()
    {
        yield return new WaitForSeconds(0.1f);

        if (gameObject.activeInHierarchy)
        {
            foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
            {
                spriteBasedLaserScript.SetLaserState(false);
            }
            //laserOn.SetActive(false);
            drawLine.vortexParent = gameObject.transform;
            drawLine.StartSraw();
            //M_SplineMove.Resume();
        }
        else
        {
            StopAllCoroutines();
        }
    }
    public void StopShootLaserWhenDie()
    {
        foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
        {
            spriteBasedLaserScript.SetLaserState(false);
        }
       
        StopAllCoroutines();
    }


    private void LaserOnOnLaserHitTriggered(RaycastHit2D hitInfo)
    {
        //		Debug.LogError ("LaserOnOnLaserHitTriggered Player");

        if (hitInfo.collider.tag == "player" && !MainScene.Instance.gameFinished && !hitInfo.collider.GetComponent<Aircraft>().aircraftIsReloading && !hitInfo.collider.GetComponent<Aircraft>().aircraftUsingActiveShield && !MainScene.Instance.gameStopping && !MainScene.Instance.bossDead)
        {
            //hitInfo.collider.GetComponent<PlayerCollision> ().Pool_EffectPlayerDie ();
            //hitInfo.collider.GetComponent<PlayerController> ().PlayerDie ();
            if (SceneManager.GetActiveScene().name == "LevelTutorial")
            {
                hitInfo.collider.GetComponent<Aircraft>().playerInitScript.ChangeNumberLife(-9999);
            }
            hitInfo.collider.GetComponent<Aircraft>().playerControllerFalconScript.PlayerDie();
        }
    }

    public GameObject drawLinePrefabs;
    DrawLineParent drawLine;

}
