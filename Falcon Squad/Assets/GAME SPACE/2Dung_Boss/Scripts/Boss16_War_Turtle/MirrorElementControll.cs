﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;
using SkyGameKit;
using DG.Tweening;

public class MirrorElementControll : MonoBehaviour
{
    Vector3 startPosition;
    private float count;

    public UbhShotCtrl gun;
    public BossWarTurtleAction bossObj;
    public MirrorController mirrorController;
    public splineMove splineMove;
    public ParticleSystem effect;

    public float Count
    {
        get
        {
            return count;
        }

        set
        {
            count = value;

            if (count > 40 && count < 70)
            {
                anim.SetTrigger("isYellow");
            }
            else if (count > 70 && count < 100)
            {
                anim.SetTrigger("isRed");
            }
            else if (count > 100)
            {
                //Debug.LogError("isShoot");
                anim.SetTrigger("isShoot");
            }
            else if (count < 40)
            {
                anim.SetTrigger("isBlue");
            }
        }
    }
    Animator anim;
    bool isFirst;
    private void Start()
    {
        startPosition = gameObject.transform.localPosition;

    }
    private void OnEnable()
    {

        if (splineMove == null)
        {
            splineMove = GetComponent<splineMove>();
        }
        if (anim == null)
        {
            anim = GetComponent<Animator>();
        }
        if (bossObj == null)
        {
            bossObj = GameObject.FindGameObjectWithTag("Boss7").GetComponent<BossWarTurtleAction>();
        }
        Count = 0;
        if (bossObj.isOverride)
        {
            splineMove.Pause();
        }
        else
        {
            splineMove.onStart = true;
            splineMove.moveToPath = true;
            splineMove.Resume();
            splineMove.StartMove();
            StartHideMirror();
        }
        anim.Play("Mirror_Point");
        this.Delay(0.5f, () =>
        {
            anim.SetTrigger("isPos");           
        });
    }

    public void Mirror_Step1()
    {

        anim.SetTrigger("isBlue");
        mirrorController.isShow = true;
    }
    public void Shoot()
    {
        if (gameObject.activeInHierarchy && gun != null)
        {
            gun.StartShotRoutine();
        }
        Count = 0;

    }
    public IEnumerator HideMirror(float time = 0)
    {
        yield return new WaitForSeconds(time);
        if (gameObject.activeInHierarchy)
        {
            anim.SetTrigger("isDeActive");
        }
    }

    public void StartHideMirror()
    {
        StartCoroutine(HideMirror(mirrorController.timeHideMirror));
    }

    public void StartHideMirrorOverride()
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(HideMirror(0));
        }
    }

    public void DeActiveShield()
    {
        mirrorController.isShow = false;
        splineMove.Pause();
        gameObject.transform.localPosition = startPosition;
        gameObject.SetActive(false);
        //Color color = gameObject.GetComponent<SpriteRenderer>().color;
        //DOTween.Sequence().Append(gameObject.transform.DOMove(new Vector3(0, 3, 0), 1.5f).OnPlay(() =>
        //   {
        //       color = new Color32(255, 255, 255, 0);
        //   }).OnComplete(() =>
        //   {
        //       gameObject.transform.localPosition = startPosition;
        //       color = new Color32(255, 255, 255, 255);
        //       gameObject.SetActive(false);
        //   }));

    }
    public void ChangeAnimToBlue()
    {
        anim.SetTrigger("isBlue");
    }

}
