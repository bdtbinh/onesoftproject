﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using PathologicalGames;

public class DrawLine : MonoBehaviour
{
    public Transform[] controlPoints;
    public LineRenderer lineRenderer;
    //public ParticleSystem effect;

    private int curveCount = 0;
    private int layerOrder = 0;
    private const int SEGMENT_COUNT = 50;
    public Transform parent;
    public EdgeCollider2D edgeCollider;

    void Start()
    {
        if (!lineRenderer)
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
        lineRenderer.sortingLayerID = layerOrder;
        curveCount = (int)controlPoints.Length / 3;

        //this.Delay(1.5f, () => { StartDrawLine(); });
    }

    public void StartDrawLine()
    {
        edgeCollider.enabled = true;
        StartCoroutine(DrawCurve());
    }

    List<Vector3> points = new List<Vector3>();
    IEnumerator DrawCurve()
    {
        //edgeCollider.points = new Vector2[SEGMENT_COUNT];
        //effect.gameObject.SetActive(true);
        //effect.Play();
        List<Vector3> arrpoint = new List<Vector3>();
        points.Add(Vector3.zero);
        lineRenderer.SetVertexCount(1);
        lineRenderer.SetPosition(0, points[0]);
        edgeCollider.edgeRadius = 0.2f;

        for (int j = 0; j < curveCount; j++)
        {
            for (int i = 2; i <= SEGMENT_COUNT; i++)
            {
                float t = i / (float)SEGMENT_COUNT;
                //float t = i / i * (float)SEGMENT_COUNT;
                int nodeIndex = j * 3;
                Vector3 pixel = CalculateCubicBezierPoint(t, controlPoints[nodeIndex].position, controlPoints[nodeIndex + 1].position, controlPoints[nodeIndex + 2].position, controlPoints[nodeIndex + 3].position);
                pixel = pixel - parent.transform.position;
                //yield return new WaitForSeconds(0.005f);
                //lineRenderer.SetVertexCount((j * SEGMENT_COUNT) + i);
                //lineRenderer.SetPosition((j * SEGMENT_COUNT) + (i - 1), pixel);
                points.Add(pixel);
            }
        }

        for (int i = 1; i < points.Count - 1; i++)
        {
            yield return new WaitForSeconds(0.04f);
            lineRenderer.SetVertexCount(i + 1);
            lineRenderer.SetPosition(i, points[i]);
            //effect.gameObject.transform.localPosition = points[i];
            arrpoint.Add(points[i]);

            //HelperDung.LookAtToDirection(points[i+1], effect.gameObject, 1000);

            edgeCollider.points = ConvertArray(arrpoint.ToArray());

            if (i == points.Count - 2)
            {
                StartCoroutine(HideLine());
                //effect.transform.rotation = new Quaternion(0, 0, 0, 0);
                //effect.gameObject.SetActive(false);
            }

        }
    }
    public IEnumerator HideLine()
    {
        List<Vector3> arr = new List<Vector3>();
        arr = points;
        //yield return new WaitForSeconds(0.1f);
        while (points.Count > 0)
        {
            yield return new WaitForSeconds(0.005f);
            if (arr.Count == 0)
            {
                break;
            }
            else
            {
                arr.RemoveAt(0);
                lineRenderer.SetVertexCount(arr.Count);
                lineRenderer.SetPositions(arr.ToArray());
                edgeCollider.points = ConvertArray(arr.ToArray());
            }
        }
        //effect.Stop();       
        edgeCollider.enabled = false;
        Destroy(gameObject.transform.parent.parent.gameObject);
    }
    Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
    }
    Vector2 ConvertVector(Vector3 v3)
    {
        Vector2 v2 = new Vector2();

        Vector3 tempV3 = v3;
        v2 = new Vector2(tempV3.x, tempV3.y);
        return v2;
    }
    Vector2[] ConvertArray(Vector3[] v3)
    {
        Vector2[] v2 = new Vector2[v3.Length];
        for (int i = 0; i < v3.Length; i++)
        {
            Vector3 tempV3 = v3[i];
            v2[i] = new Vector2(tempV3.x, tempV3.y);
        }
        return v2;
    }


}
