﻿using PathologicalGames;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class BossEventSummerShieldCoreCollision : MonoBehaviour
{
    public  void  OnTriggerEnter2D(Collider2D col)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            switch (col.tag)
            {
                case "ZoneCheckEnemyInScene":
                    Debug.LogError("inScene");
                    break;
                case "PlayerBullet":

                    var bullet = col.GetComponent<BulletPlayerValue>();
                    Pool_EffectTriggerBullet(col.transform);
                    bullet.Despawn();


                    break;
                case "PlayerBulletBurstShot":

                    var bulletBurstShot = col.GetComponent<BulletPlayerValue>();
                    Pool_EffectTriggerBurstShot(col.transform);
                    bulletBurstShot.Despawn();


                    break;
                case "PlayerBulletStarbomb":


                    var bulletStarbomb = col.GetComponent<BulletActiveSkillPlane8>();

                    bulletStarbomb.StartExplosive();



                    break;
                case "PlayerBulletHoming":

                    var bulletHoming = col.GetComponent<BulletPlayerValue>();

                    Pool_EffectTriggerBullet(col.transform);
                    bulletHoming.Despawn();



                    break;
                case "PlayerBulletWindSlash":

                    var bulletWindSlash = col.GetComponent<BulletPlayerValue>();

                    Pool_EffectTriggerBullet(col.transform);
                    bulletWindSlash.Despawn();

                    break;
                case "Wing2SmallBird":

                    var bulletWing2Smailbird = col.GetComponent<Wing2SmallBird>();
                    Pool_EffectTriggerBullet(col.transform);                    
                    //bulletWing2Smailbird.Despawn();

                    break;
                case "PlayerMissile":
                    Debug.Log("enemy Trigger Player");
                    if (PoolManager.Pools["BulletPlayerPool"].IsSpawned(col.transform))
                    {
                        PoolManager.Pools["BulletPlayerPool"].Despawn(col.transform);
                    }
                    break;
            }
        }
    }
    public  void Pool_EffectTriggerBullet(Transform bulletTrans)
    {
        if (EffectList.Instance.enemyTriggerBullet != null && bulletTrans.gameObject.activeInHierarchy)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyTriggerBullet, bulletTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
    public  void Pool_EffectTriggerBurstShot(Transform bulletTrans)
    {
        if (EffectList.Instance.enemyTriggerBulletBurstShot != null && bulletTrans.gameObject.activeInHierarchy)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyTriggerBulletBurstShot, bulletTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
