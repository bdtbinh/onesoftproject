﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class BossEventSummerWeaponLeftRight : MonoBehaviour
{

    Animator anim;

    public UbhShotCtrl[] gunCtrlList;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void ShootMainWeaponStep1()
    {
        anim.SetTrigger("isAttack1");
        this.Delay(0.3f, () => { ShootMainWeaponStep2(); });
    }

    public void ShootMainWeaponStep2()
    {
        anim.SetTrigger("isAttack2");

    }

    public void ShootMainWeaponStep3()
    {
        anim.SetTrigger("isAttack3");


        if (gameObject.activeInHierarchy)
        {
            this.Delay(0.5f, () =>
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StartShotRoutine();
                }
            });
        }

    }


    public void ChangeAnim_Atk_To_Idle()
    {
        anim.SetTrigger("isIdle");
    }
    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        foreach (UbhShotCtrl gun in gunCtrlList)
        {
            gun.StopShotRoutine();
        }

    }
}
