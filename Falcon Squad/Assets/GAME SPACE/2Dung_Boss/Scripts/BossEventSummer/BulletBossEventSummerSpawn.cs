﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class BulletBossEventSummerSpawn : MonoBehaviour
{

    Transform transCache, transParentCache;
    Transform playerTrans;
    //EnemyAction2017 enemyActionIns;

    //public Animator animEnemy;
    bool startFindPlayer = false;

    public float normalSpeed = 2.5f;


    void Start()
    {
        transCache = transform;
        transParentCache = transCache.parent;
        playerTrans = AllPlayerManager.Instance.playerCampaign.Aircraft.transform;
        //enemyActionIns = GetComponent<EnemyAction2017>();
    }


    void Update()
    {
        if (playerTrans != null)
        {
            if (startFindPlayer)
            {
                FollowTarget(playerTrans.gameObject, 8f, normalSpeed);
            }
        }
        else
        {
            transCache.position += transCache.up * Time.deltaTime * normalSpeed;
            if (playerInZone)
            {
                speedAdd = 1f;
                //animEnemy.SetTrigger("isIdle");
                playerInZone = false;
            }
        }
    }

    private void OnEnable()
    {
        this.Delay(1f, () =>
        {
            if (playerTrans == null)
            {
                playerTrans = AllPlayerManager.Instance.playerCampaign.Aircraft.transform;
            }
            StartFindPlayerAction();
        });
    }

    private void OnDisable()
    {
        startFindPlayer = false;
        playerInZone = false;
        speedAdd = 1f;
    }


    void StartFindPlayerAction()
    {
        transParentCache = transCache.parent;
        //enemyActionIns.M_SplineMove.Stop();

        startFindPlayer = true;

    }


    bool playerInZone = false;
    float speedAdd = 1f;
    private void FollowTarget(GameObject target, float distance, float normalSpeed)
    {
        if (Vector2.Distance(target.transform.position, transCache.position) < distance && !AllPlayerManager.Instance.playerCampaign.Aircraft.aircraftIsReloading)
        {
            speedAdd += 1.2f * Time.deltaTime; //Tốc độ tăng đần theo thời gian
            transCache.position = Vector3.MoveTowards(transCache.position, target.transform.position, Time.deltaTime * normalSpeed * 1f * speedAdd);
            //
            Quaternion posFollow = Quaternion.LookRotation(-(transCache.localPosition + transParentCache.localPosition) + target.transform.localPosition, transCache.TransformDirection(Vector3.forward));
            transCache.rotation = new Quaternion(0, 0, posFollow.z, posFollow.w);

            if (!playerInZone)
            {
                //animEnemy.SetTrigger("isAttack1");
                playerInZone = true;
            }
        }
        else
        {
            transCache.position -= transCache.up * Time.deltaTime * normalSpeed;

            if (playerInZone)
            {
                speedAdd = 1f;
                //animEnemy.SetTrigger("isIdle");
                playerInZone = false;
            }
        }
    }
}
