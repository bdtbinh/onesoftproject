﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEventSummerSheildController : MonoBehaviour
{

    //public BossEventSummerAction bossEventSummerAction;
    public BossEventSummerShieldCore[] shieldCore;
    public BossEventSummerAction bossAction;
    public ParticleSystem[] particleSmoke;

    List<BossEventSummerShieldCore> listShieldCore = new List<BossEventSummerShieldCore>();
    int count = 0;

    public void ChangeShieldStep1()
    {
        int temp;
        int shieldCoreLife = 0;
        for (int i = 0; i < shieldCore.Length; i++)
        {
            if (shieldCore[i].gameObject.activeInHierarchy)
            {
                shieldCoreLife++;
            }
        }
        //Debug.LogError(shieldCoreLife);
        if (shieldCoreLife == 0)
        {
            bossAction.time = 0;
            ShowParticle();
            bossAction.ChangeSpeed();
            bossAction.GoOutMap();
        }
        else if (shieldCoreLife == 1)
        {
            foreach (var item in shieldCore)
            {
                if (item.gameObject.activeInHierarchy)
                {
                    item.HideShield();
                }
            }
        }
        else if (shieldCoreLife == 2)
        {
            count++;
            listShieldCore.Clear();
            foreach (var item in shieldCore)
            {
                if (item.gameObject.activeInHierarchy)
                {
                    item.HideShield();
                    listShieldCore.Add(item);
                }
            }

            listShieldCore[count % 2].ActiveShield();
        }
        else if (shieldCoreLife == 3)
        {
            temp = Random.Range(0, 3);

            for (int i = 0; i < shieldCore.Length; i++)
            {
                if (i == temp)
                {
                    shieldCore[i].HideShield();
                }
                else
                {
                    shieldCore[i].ActiveShield();
                }
            }
        }
    }

    public void ShowParticle()
    {
        for(int i=0; i < particleSmoke.Length; i++)
        {
            particleSmoke[i].gameObject.SetActive(true);
            particleSmoke[i].Play();
        }
    }

    public void HideParticle()
    {
        for (int i = 0; i < particleSmoke.Length; i++)
        {
            particleSmoke[i].Stop();
            particleSmoke[i].gameObject.SetActive(false);           
        }
    }

}
