﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class BossEventSummerWeaponCenter : MonoBehaviour
{

    public Animator animSpawnLeft, animSpawnRight;

    public UbhShotCtrl[] gunCtrlMixed;
    public UbhShotCtrl[] gunCtrlSpawnEnemy;
    public ParticleSystem effectGunMixed;


    void Awake()
    {
        //anim = GetComponent<Animator>();
    }

    //----------------------------------SHOT WEAPON MIXED---------------------------------//
    public void ShootWeaponMixedStep1()
    {

        effectGunMixed.gameObject.SetActive(true);
        effectGunMixed.Play();
       

        ShootWeaponMixedStep2();

        //anim.SetTrigger("isAttack1");
        //effectStep1Gun.gameObject.SetActive(true);
        //effectStep1Gun.Play(true);
    }

    public void ShootWeaponMixedStep2()
    {
        //Debug.LogError("ShootMainWeaponStep2");
        //anim.SetTrigger("isAttack2");
        this.Delay(1.5f, () =>
        {
            effectGunMixed.Stop();
            effectGunMixed.gameObject.SetActive(false);
            ShootWeaponMixedStep3();

        });
    }

    public void ShootWeaponMixedStep3()
    {
        //anim.SetTrigger("isAttack3");
        if (gameObject.activeInHierarchy)
        {
            this.Delay(0.2f, () =>
            {
                foreach (UbhShotCtrl gun in gunCtrlMixed)
            {
                gun.StartShotRoutine();
                }
            });
        }
    }


    //------------------------------------SHOOT WEAPON SPAWN ENEMY------------------------------------------//

    public void ShootWeaponSpawnEnemyStep1()
    {

       
        animSpawnLeft.SetTrigger("isAttack1");
        animSpawnRight.SetTrigger("isAttack1");
        //effectStep1Gun.gameObject.SetActive(true);
        //effectStep1Gun.Play(true);
        this.Delay(0.1f, () => { ShootWeaponSpawnEnemyStep2(); });
    }

    public void ShootWeaponSpawnEnemyStep2()
    {
        animSpawnLeft.SetTrigger("isAttack2");
        animSpawnRight.SetTrigger("isAttack2");
        ShootWeaponSpawnEnemyStep3();
        //if (gameObject.activeInHierarchy)
        //{
        //    foreach (UbhShotCtrl gun in gunCtrlList)
        //    {
        //        gun.StartShotRoutine();
        //    }
        //}
    }

    public void ShootWeaponSpawnEnemyStep3()
    {
        animSpawnLeft.SetTrigger("isAttack3");
        animSpawnRight.SetTrigger("isAttack3");
        //anim.SetTrigger("isAttack3");
        this.Delay(1f, () => {
            if (gameObject.activeInHierarchy)
            {
                foreach (UbhShotCtrl gun in gunCtrlSpawnEnemy)
                {
                    gun.StartShotRoutine();
                }
            }
        });
       

       this.Delay(0.1f, () => { ChangeAnim_Atk_To_Idle(); });
    }

    //--------------------------------------------------------------------------------------------------//
    public void ChangeAnim_Atk_To_Idle()
    {
        animSpawnLeft.SetTrigger("isIdle");
        animSpawnRight.SetTrigger("isIdle");
    }
    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        foreach (UbhShotCtrl gun in gunCtrlMixed)
        {
            gun.StopShotRoutine();
        }
        foreach (UbhShotCtrl gun in gunCtrlSpawnEnemy)
        {
            gun.StopShotRoutine();
        }


    }
}
