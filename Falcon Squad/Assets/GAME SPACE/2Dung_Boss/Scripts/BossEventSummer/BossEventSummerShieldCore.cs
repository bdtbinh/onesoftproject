﻿using PathologicalGames;
using Sirenix.OdinInspector;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class BossEventSummerShieldCore : EnemyCollisionBase
{
    //public BossEventSummerSheildController bossEventSummerSheildController;

    public int hpShieldCore;
    [DisplayAsString]
    public int curHpShieldCore;

    CircleCollider2D boxCollider;
    Quaternion offsetEffect;
    public EnemyHPbar enemyHpBarIns;
    public SpriteRenderer shield;
    public bool isShieldCoreDie = false;
    private EnemyDropItem enemyDropItem;
    private Animator anim;
    public BossEventSummerSheildController bossEventSummerSheildController;
    void Start()
    {
        anim = GetComponent<Animator>();
        enemyDropItem = GetComponent<EnemyDropItem>();
        boxCollider = GetComponent<CircleCollider2D>();
    }

    private void OnEnable()
    {
        ResetData();
    }

    public void ResetData()
    {
        curHpShieldCore = hpShieldCore;
    }
    public void ActiveShield()
    {
        shield.gameObject.SetActive(true);
        if (boxCollider != null)
        {
            boxCollider.enabled = false;
        }
    }

    public void HideShield()
    {
        shield.gameObject.SetActive(false);
        if (boxCollider != null)
        {
            boxCollider.enabled = true;
        }
    }

    public override void TakeDamage(int damage, bool fromServer = false)
    {
        anim.SetTrigger("isDamage");
        curHpShieldCore -= damage;

        if (curHpShieldCore <= 0)
        {
            DropItem();
            Pool_EffectDie();
            isShieldCoreDie = true;

            gameObject.SetActive(false);
            boxCollider.enabled = false;

            bossEventSummerSheildController.ChangeShieldStep1();
        }
        else
        {
            Pool_EffectTriggerBullet(transform);
            SoundManager.SoundBullet_Enemy();
            if (enemyHpBarIns != null)
            {
                enemyHpBarIns.ChangeHPBar(curHpShieldCore, hpShieldCore);
            }
        }
        this.Delay(0.2f, () => { anim.SetTrigger("unDamage"); });
    }

    private void Update()
    {
        //offsetEffect = bossEventSummerSheildController.gameObject.transform.rotation;
        //gameObject.transform.rotation = offsetEffect;
    }
    public void DropItem()
    {
        enemyDropItem.Drop();
    }

    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        //Debug.LogError("laserPower:" + laserPower + "_nameObj:" + gameObject.name);
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            TakeDamage(laserPower);
        }
    }
    public override void Pool_EffectDie()
    {
        if (EffectList.Instance.enemyDie != null)
        {


            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
