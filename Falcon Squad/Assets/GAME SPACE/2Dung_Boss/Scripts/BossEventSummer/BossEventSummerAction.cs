﻿using Sirenix.OdinInspector;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class BossEventSummerAction : BossAction
{
    //[DisplayAsString]
    //public bool boss2Pausing;

    public BossEventSummerWeaponLeftRight bossEventSummerWeaponLeft, bossEventSummerWeaponRight;
    public BossEventSummerWeaponCenter bossEventSummerWeaponCenter;
    public BossEventSummerSheildController bossEventShieldController;

    public PathManager pathGoOutMap;
   
    private void OnEnable()
    {
        bossEventShieldController.ChangeShieldStep1();
        time = 60;      
        StartCoroutine(CountDownTime());
    }
    [HideInInspector]
    public int time;
    public IEnumerator CountDownTime()
    {
        //if (gameObject.activeInHierarchy)
        //{
        //    PanelBossHealbar.Instance.lTimerBossEvent.text = I2.Loc.ScriptLocalization.boss_time.Replace("%{time}", time.ToString());
        //}
        
        while (time > 0)
        {

            yield return new WaitForSeconds(1f);
            time -= 1;
            PanelBossHealbar.Instance.lTimerBossEvent.gameObject.SetActive(true);

            PanelBossHealbar.Instance.lTimerBossEvent.text = I2.Loc.ScriptLocalization.boss_time.Replace("%{time}", time.ToString());
        }
        PanelBossHealbar.Instance.lTimerBossEvent.gameObject.SetActive(false);
        StopCoroutine(CountDownTime());

    }

    //------------------------------ ChangeSpeed-----------------
    bool speedChanged;

    public void ChangeSpeed()
    {
        if (!speedChanged)
        {
            if (newSpeed == null || newSpeed == 0)
            {
                Debug.LogError("Chua truyen` newSpeed vao` turn manager");
            }
            else
            {
                M_SplineMove.ChangeSpeed(newSpeed);
                speedChanged = true;
            }
        }
    }

    //	-------------------------------------------- SHOOT WEAPON LEFT RIGHT--------------------------------------------------------------//

    bool isShootWeaponLeftRight;

    public void ShootWeaponLeftRightLoop(float timeDelay)
    {
        if (!isShootWeaponLeftRight)
        {
            isShootWeaponLeftRight = true;
            
            StartCoroutine(StartShootWeaponLeftRightLoop(timeDelay));
        }
    }

    IEnumerator StartShootWeaponLeftRightLoop(float timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                bossEventSummerWeaponLeft.ShootMainWeaponStep1();
                bossEventSummerWeaponRight.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }


    //	-----------------------------------ShootMainWeaponCenterLoop - Weapon Mixed-------------------------------------------------------------//

    bool isShootWeaponMixed;

    public void ShootWeaponMixedLoop(float timeLoop)
    {
        if (!isShootWeaponMixed)
        {
            isShootWeaponMixed = true;
            
            StartCoroutine(StartShootWeaponMixedLoop(timeLoop));
        }

    }

    IEnumerator StartShootWeaponMixedLoop(float timeLoop)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeLoop);
            if (gameObject.activeInHierarchy)
            {
                bossEventSummerWeaponCenter.ShootWeaponMixedStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    //---------------------------------- ShootMainWeaponCenterLoop - Weapon Spawn Enemy-----------------------------------------//

    bool isShootWeaponSpawnEnemy;

    public void ShootWeaponSpawnEnemyLoop(float timeLoop)
    {
        if (!isShootWeaponSpawnEnemy)
        {
            isShootWeaponSpawnEnemy = true;
           
            StartCoroutine(StartShootWeaponSpawnEnemyLoop(timeLoop));
        }

    }

    IEnumerator StartShootWeaponSpawnEnemyLoop(float timeLoop)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeLoop);
            if (gameObject.activeInHierarchy)
            {
                bossEventSummerWeaponCenter.ShootWeaponSpawnEnemyStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }
    //-------------------------------------- CHANGE SHIELD GOID LOOP -------------------------------------//

    bool isChangeShieldGoldLoop;
    public void ChangeShieldGoldLoop(float timeLoop)
    {
        if (!isChangeShieldGoldLoop)
        {
            isChangeShieldGoldLoop = true;
            StartCoroutine(StartChangeShieldGold(timeLoop));
        }
    }

    IEnumerator StartChangeShieldGold(float timeLoop)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeLoop);
            if (gameObject.activeInHierarchy)
            {
                bossEventShieldController.ChangeShieldStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }

    
    public void GoOutMap()
    {
        bossEventShieldController.ShowParticle();
        M_SplineMove.Stop();
        M_SplineMove.moveToPath = true;
        PathManager nextPath = pathGoOutMap;
        M_SplineMove.SetPath(nextPath);
    }

    //IEnumerator StartGooutMap(float time)


    public override void Restart()
    {
        if (maxHP > 0)
        {
            currentHP = maxHP;
        }
        else
        {
            Debug.LogError("Máu enemy <= 0, tự gán = 5");
            currentHP = 5;
        }
        isRemoved = false;
        LevelManager.aliveEnemy.Add(this);
        LevelManager.OnEnemySpawned(ID);
        forceRemoveEvent.RemoveAllListeners();
        deadEvent.RemoveAllListeners();
        M_SplineMove.events.Clear();
    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);

        bossEventShieldController.HideParticle();
        isShootWeaponLeftRight = false;
        isShootWeaponMixed = false;
        isShootWeaponSpawnEnemy = false;

    }

}
