﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;
public class BossEventSummerCollision : EnemyCollisionBase
{

    private BossEventSummerAction bossEventSummerObj;
    private CheckEnemyInScene checkEnemyInScene;
    SpriteRenderer spr;
    Transform transCache, transParent;

    void Start()
    {
        transCache = transform;
        transParent = transCache.parent;

        bossEventSummerObj = GetComponent<BossEventSummerAction>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();
        spr = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            bossEventSummerObj.Die(EnemyKilledBy.DeadZone);
        }
    }


    //public override void TakeDamage(int damage)
    //{
    //    bossEventSummerObj.currentHP -= damage;
    //    PanelBossHealbar.Instance.ChangeBossHealbar(bossEventSummerObj.currentHP);
    //    if (bossEventSummerObj.currentHP <= 0)
    //    {
    //        PanelBossHealbar.Instance.HideBossHealbar();
    //        bossEventSummerObj.GetComponent<CircleCollider2D>().enabled = false;
    //        bossEventSummerObj.M_SplineMove.Stop();
    //        Pool_EffectDie();
    //        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.EndLess && GameContext.modeGamePlay != GameContext.ModeGamePlay.Events && GameContext.modeGamePlay != GameContext.ModeGamePlay.Tournament)
    //        {
    //            MainScene.Instance.bossDead = true;
    //        }
    //        this.Delay(1.8f, () =>
    //        {
    //            bossEventSummerObj.GetComponent<CircleCollider2D>().enabled = true;
    //            bossEventSummerObj.Die(EnemyKilledBy.Player);
    //        });
    //    }
    //    else
    //    {
    //        PoolExplosiveSmall(spr);
    //        SoundManager.SoundBullet_Enemy();
    //    }
    //}



    //public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    //{

    //    if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
    //    {
    //        try
    //        {
    //            if (checkEnemyInScene.enemyInScreen)
    //            {
    //                TakeDamage(laserPower);
    //            }
    //        }
    //        catch (System.NullReferenceException)
    //        {
    //            Debug.Log("Null LaserOnOnLaserHitTriggeredEnemy boss2");
    //            throw;
    //        }

    //    }
    //}


    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.bossDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.bossDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
