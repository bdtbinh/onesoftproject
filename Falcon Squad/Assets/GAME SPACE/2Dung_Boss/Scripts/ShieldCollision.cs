﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;
using DG.Tweening;


public class ShieldCollision : MonoBehaviour
{

    private float count;
    public CounterBugShieldControl enemyObj;
    public Animator anim;
    public ParticleSystem effect;

    private void OnEnable()
    {
        if (anim == null)
        {
            anim = GetComponent<Animator>();
        }
        anim.Play("ShieldCounterBug_Active");
    } 

    public float Count
    {
        get
        {
            return count;
        }

        set
        {
            count = value;

            if (count > 40 && count < 70)
            {
                anim.SetTrigger("isYellow");
            }
            else if (count > 70 && count < 100)
            {
                anim.SetTrigger("isRed");
            }
            else if (count > 100)
            {               

                anim.SetTrigger("isShoot");                       
            }
            else if(count < 40)
            {
                anim.SetTrigger("isBlue");
            }
        }
    }

    public void ShieldShoot()
    {

        enemyObj.Shoot();
        
    }
    public void ChangeAnimBlue()
    {
        anim.SetTrigger("isBlue");
        Count = 0;
    }

    public void DeActiveShield()
    {
        gameObject.SetActive(false);
        enemyObj.isShield = false;
        enemyObj.animEnemy.SetTrigger("isIdle");
    }

    bool isShoot;
    public float time;
    public float index;
    public float hitIndex = 1.5f;
    private void Update()
    {
        if (isShoot)
        {
            time -= Time.deltaTime;
            if (time < 0)
            {
                time = 1f;
                isShoot = false;
            }
        }
        else
        {
            time = 1;
            if (Count >= 2)
            {
                time -= Time.deltaTime;
                if (time < 0)
                {
                    time = 1f;
                    Count -= index;
                }
            }

        }

    }
    public void Pool_EffectTriggerBullet(Transform bulletTrans)
    {
        if (EffectList.Instance.enemyTriggerBullet != null && bulletTrans.gameObject.activeInHierarchy)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyTriggerBullet, bulletTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
    public void OnTriggerEnter2D(Collider2D col)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            var bullet = col.GetComponent<BulletPlayerValue>();
            isShoot = true;
            switch (col.tag)
            {

                case "ZoneCheckEnemyInScene":
                    Debug.LogError("inScene");
                    break;
                case "PlayerBullet":
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    effect.Play();
                    Count+=hitIndex;

                    break;
                case "PlayerBulletBurstShot":
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    effect.Play();

                    Count += hitIndex;

                    break;
                case "PlayerBulletStarbomb":
                    var bulletStarbomb = col.GetComponent<BulletActiveSkillPlane8>();
                    bulletStarbomb.StartExplosive();
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    effect.Play();

                    Count += hitIndex;

                    break;
                case "PlayerBulletHoming":

                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    break;
                case "PlayerBulletWindSlash":
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    effect.Play();

                    Count += hitIndex;

                    break;
                case "Wing2SmallBird":
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    effect.Play();

                    Count += hitIndex;

                    break;

                case "PlayerMissile":
                    Pool_EffectTriggerBullet(col.transform);
                    SoundManager.SoundBullet_Enemy();
                    bullet.Despawn();
                    effect.Play();

                    Count += hitIndex;

                    break;
            }

            //gameObject.
        }

    }
}
