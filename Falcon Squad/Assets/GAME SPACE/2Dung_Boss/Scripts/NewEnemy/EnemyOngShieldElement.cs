﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using SkyGameKit;
using PathologicalGames;
using TCore;

public class EnemyOngShieldElement : EnemyCollisionBase
{
    public EnemyOngShieldController shieldController;

    public int hpShield;

    [DisplayAsString]
    public int currentHP;
    [HideInInspector]
    public SpriteRenderer spr;
    CircleCollider2D boxCollider;
    Vector3 offsetEffect;
    public EnemyHPbar enemyHpBarIns;

    public bool isShieldCoreDie = false;
    public Sprite sShieldCoreShow, sShieldCoreHide;


    private void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<CircleCollider2D>();
        //
        RestartData();
        offsetEffect = new Vector3(0f, 0f, 0f);
       
    }

    public void RestartData()
    {
        currentHP = hpShield;
        isShieldCoreDie = false;
        spr.sprite = sShieldCoreShow;
        spr.enabled = true;
        boxCollider.enabled = true;
    }
    public override void TakeDamage(int damage, bool fromServer = false)
    {
        currentHP -= damage;

        if (currentHP <= 0)
        {
            Pool_EffectDie();
            spr.sprite = sShieldCoreHide;
            boxCollider.enabled = false;
            isShieldCoreDie = true;
            shieldController.HideShield();
        }
        else
        {
            Pool_EffectTriggerBullet(transform);
            PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
            if (enemyHpBarIns != null)
            {
                enemyHpBarIns.ChangeHPBar(currentHP, hpShield);
            }
        }

        base.TakeDamage(damage, fromServer);
    }
    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        //Debug.LogError("laserPower:" + laserPower + "_nameObj:" + gameObject.name);
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            TakeDamage(laserPower);
        }
    }



    //---------
    public override void Pool_EffectDie()
    {
        if (EffectList.Instance.enemyDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyDie, transform.position + offsetEffect, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
