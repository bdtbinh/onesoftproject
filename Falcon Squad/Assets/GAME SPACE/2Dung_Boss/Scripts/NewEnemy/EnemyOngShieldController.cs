﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class EnemyOngShieldController : MonoBehaviour
{
    public EnemyAction2017 enemyOngObj;
    public EnemyOngShieldElement shieldElement1, shieldElement2;
    public Animator anim;
    public float timeStun;
    public float timeShootLoop;

    [HideInInspector]
    public bool isStun;

    private CircleCollider2D colliderShield;
    private SpriteRenderer spr;


    private void Start()
    {
        colliderShield = GetComponent<CircleCollider2D>();
        spr = GetComponent<SpriteRenderer>();
    }

    public void HideShield()
    {
        if (shieldElement1.isShieldCoreDie && shieldElement2.isShieldCoreDie)
        {
            StopAllCoroutines();
            isStun = true;
            enemyOngObj.M_SplineMove.Pause();
            anim.SetTrigger("isStun");
            colliderShield.enabled = false;
            spr.enabled = false;
            shieldElement1.spr.enabled = false;
            shieldElement2.spr.enabled = false;
            this.Delay(timeStun, () =>
             {
                 isStun = false;
                 anim.SetTrigger("isIdle");
                 colliderShield.enabled = true;
                 spr.enabled = true;
                 enemyOngObj.M_SplineMove.Resume();

                 shieldElement1.RestartData();
                 shieldElement2.RestartData();

                 this.Delay(timeStun + 0.5f, () => { ShootLoop(); });
             });
        }
    }

    public void ShootLoop()
    {
        if (!isStun)
        {
            enemyOngObj.Shoot();
            StartCoroutine(StartShootLoop());
        }            
    }

    public IEnumerator StartShootLoop()
    {
        yield return new WaitForSeconds(timeShootLoop);

        ShootLoop();

    }
   
}
