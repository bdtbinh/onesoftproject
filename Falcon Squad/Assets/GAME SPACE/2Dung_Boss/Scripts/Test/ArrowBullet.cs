﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class ArrowBullet : MonoBehaviour
{
    public UbhShotCtrl gun;
    public UbhShotCtrl gunEndAction;
    public float timeActive;

    [HideInInspector]
    public int dem;

    private Vector3 positionAppear, playerPosition;
    private Vector3[] arrPoint = new Vector3[15];
    //private float moveDuration = 3;
    private Transform player;
    private Vector3[] listRandomPosition;
    private Vector3 startScale;
    public ParticleSystem effect;
    //public TrailRenderer trail;
    LineRenderer line;
    BossActionChange bossObj;
    private void Start()
    {
        bossObj = GameObject.FindGameObjectWithTag("Boss11").GetComponent<BossActionChange>();
        startScale = transform.localScale;
        player = GameObject.FindGameObjectWithTag("player").transform;
        listRandomPosition = new Vector3[]
           {
            new Vector3(UnityEngine.Random.Range(-3.5f, 0f), 5), new Vector3(-3.5f,UnityEngine.Random.Range(2f, 5)),
            new Vector3(3.5f, UnityEngine.Random.Range(2f, 5f)), new Vector3(UnityEngine.Random.Range(0, 3.5f), 5),
            new Vector3(UnityEngine.Random.Range(-3.5f, 0f), -5), new Vector3(-3.5f, UnityEngine.Random.Range(-5f, -2f)),
            new Vector3(UnityEngine.Random.Range(0, 3.5f), -5), new Vector3(3.5f, UnityEngine.Random.Range(-5f, -2f))
           };
        line = GetComponent<LineRenderer>();
        //this.Delay(2f, () => { StartAttackFollowPlayer(); });
    }
    //public void StartAttackFlowPlayer(float moveDuration)
    //{
    //    StartCoroutine(AttackFlowPlayer(moveDuration));
    //}
    public void StartAttackFollowPlayer(float moveDuration)
    {
        StartCoroutine(AttackFlowPlayer(moveDuration));
        //StartCoroutine(Loop(5));
    }
    public IEnumerator AttackFlowPlayer(float moveDuration)
    {
        effect.Play();
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("player").transform;
        }
        playerPosition = player.position;
        for (int i = 0; i < 15; i++)
        {
            arrPoint[i] = Vector3.zero;
        }
        line.SetPositions(arrPoint);
        line.startWidth = 0.5f;
        line.endWidth = 0.5f;
        if (playerPosition.x > 0)
        {
            if (playerPosition.y > 0)
            {
                positionAppear = listRandomPosition[UnityEngine.Random.Range(4, 6)];
            }
            else
            {
                positionAppear = listRandomPosition[UnityEngine.Random.Range(0, 2)];
            }

        }
        else
        {
            if (playerPosition.y > 0)
            {
                positionAppear = listRandomPosition[UnityEngine.Random.Range(6, 8)];
            }
            else
            {
                positionAppear = listRandomPosition[UnityEngine.Random.Range(2, 4)];
            }

        }

        float pointX = (positionAppear.x - playerPosition.x) / 5;
        float pointY = (positionAppear.y - playerPosition.y) / 5;


        for (int i = 0; i < 15; i++)
        {
            yield return new WaitForSeconds(0.01f);
            arrPoint[i] = new Vector3((positionAppear.x - pointX * (i - 2)), (positionAppear.y - pointY * (i - 2)));
            line.SetVertexCount(i + 1);
            line.SetPosition(i, arrPoint[i]);
        }
        //line.SetVertexCount(15);
        //line.SetPositions(arrPoint);
        StartCoroutine(ScaleLine(0.5f, line, moveDuration));
        this.Delay(0.7f, () =>
        {

            transform.position = arrPoint[0];

            HelperDung.LookAtToDirection(arrPoint[arrPoint.Length - 1] - positionAppear, gameObject, 1000);

            //trail.Clear();
            //trail.gameObject.SetActive(true);
            transform.localScale = startScale;

            DOTween.Sequence()
                .Append(transform.DOPath(arrPoint, moveDuration).OnWaypointChange((int x) =>
                {
                    if (x > 1)
                    {
                        gun.StartShotRoutine();
                    }
                }))
                .OnComplete(() =>
                {
                    //effect.Stop();
                    //trail.Clear();
                    //trail.gameObject.SetActive(false);
                    gameObject.transform.localScale = Vector3.zero;
                    bossObj.dem++;
                    bossObj.CheckEndOverride(bossObj.dem);
                });

        });
    }



    public void TurnBackPosition(Vector3 position)
    {
        effect.Stop();
        //trail.gameObject.SetActive(false);
        gameObject.transform.position = position;
    }

    public IEnumerator ScaleLine(float time, LineRenderer line, float moveDuration = 2)
    {
        while (time > 0)
        {
            yield return new WaitForSeconds(0.02f);
            time -= 0.02f;
            line.startWidth = time;
            line.endWidth = time;
        }
        //line.startWidth = 0;
        //line.endWidth = 0;
    }

    public void ActionEnd()
    {
        StartCoroutine(ScaleLine1(0.5f, line));
    }
    public IEnumerator ScaleLine1(float time, LineRenderer line)
    {
        effect.Play();
        line.startWidth = time;
        line.endWidth = time;
        Vector3 start = new Vector3(0, -10, 0);
        Vector3 end = new Vector3(0, 10, 0);
        line.SetVertexCount(3);
        Vector3[] listpoint = { start, (start + end) / 2, end };
        for (int i = 0; i < listpoint.Length; i++)
        {
            yield return new WaitForSeconds(0.02f);
            line.SetVertexCount(i + 1);
            line.SetPosition(i, listpoint[i]);
        }

        //line.SetPositions(listpoint);
        while (time > 0)
        {
            yield return new WaitForSeconds(0.02f);
            time -= 0.02f;
            line.startWidth = time;
            line.endWidth = time;
        }

        //line.startWidth = 0;
        //line.endWidth = 0;
        transform.position = start;

        HelperDung.LookAtToDirection(end - start, gameObject, 100);
        //yield return null;
        //trail.Clear();
        //trail.gameObject.SetActive(true);
        transform.localScale = startScale;

        DOTween.Sequence()
            .Append(transform.DOPath(listpoint, 1f).OnWaypointChange((int x) =>
            {
                if (x == 1)
                {
                    gunEndAction.StartShotRoutine();
                }
            }))
            .OnComplete(() =>
            {
                effect.Stop();
                //trail.Clear();
                //trail.gameObject.SetActive(false);
                gameObject.transform.localScale = Vector3.zero;

                StartCoroutine(ResumeActionBoss(timeActive));
            });
    }

    public IEnumerator ResumeActionBoss(float time)
    {
        yield return new WaitForSeconds(time);
        bossObj.dem++;
        bossObj.CheckEndOverride(bossObj.dem);
    }
}
