﻿using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBoss11EndOverride : MonoBehaviour
{
    public UbhShotCtrl[] gun;
    UbhBullet bullet;
    public ParticleSystem effect;

    public float timeStep1 = 2f;
    public float timeDelay = 0.75f;
    private void OnEnable()
    {
        bullet = GetComponent<UbhBullet>();
        effect.gameObject.SetActive(true);
        effect.Play();
        ShootGunStep1();

    }

    public void ShootGunStep1()
    {
        this.Delay(1.2f, () => { ShootGunStep2(); });
    }
    public void ShootGunStep2()
    {
        if (gameObject.activeInHierarchy)
        {
            gun[0].StartShotRoutine();
        }

        this.Delay(timeStep1, () => {
            gun[0].StopShotRoutine();
            this.Delay(timeDelay, () => { gun[1].StartShotRoutine(); });
        });
    }

    public void ReleaseBullet()
    {
        UbhObjectPool.instance.ReleaseBullet(bullet);
    }
}
