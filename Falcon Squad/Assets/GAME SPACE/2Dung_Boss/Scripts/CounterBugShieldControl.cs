﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using DG.Tweening;
public class CounterBugShieldControl : EnemyCollisionBase
{
    public float timeActiveShield;
    public float timeShieldLoop;
    public ShieldCollision shield;
    public Animator animEnemy;
    public Animator animShield;
    public EnemyAction2017 enemy;
    public bool isShield;

    public UbhShotCtrl[] listGun;

    bool isLoopActiveShiled;

    private void OnDisable()
    {
        isShield = false;
        isLoopActiveShiled = false;
        StopCoroutine(ActiveShield());
        StopCoroutine(ShieldStep2());
    }

    public void ActiveShieldLoop()
    {

        if (!isLoopActiveShiled)
        {
            animEnemy.SetTrigger("isShield");
            shield.gameObject.SetActive(true);
            animShield.Play("ShieldCounterBug");
            isShield = true;
            StartCoroutine(ShieldStep2());
            isLoopActiveShiled = true;

        }
        StartCoroutine(ActiveShield(timeShieldLoop));
    }


    IEnumerator ActiveShield(float timeLoop = 0)
    {

        while (true)
        {
            yield return new WaitForSeconds(timeLoop);
            animEnemy.SetTrigger("isShield");
            shield.gameObject.SetActive(true);
            isShield = true;

            StartCoroutine(ShieldStep2());
        }

    }

    public IEnumerator ShieldStep2()
    {
        yield return new WaitForSeconds(timeActiveShield);
        animShield.SetTrigger("isDeActive");

    }


    public void Shoot()
    {
        if (gameObject.activeInHierarchy)
        {
            if (listGun != null)
            {
                foreach (var item in listGun)
                {
                    item.StartShotRoutine();
                }
            }
        }

    }
    public override void OnTriggerEnter2D(Collider2D col)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            var bullet = col.GetComponent<BulletPlayerValue>();
            switch (col.tag)
            {

                case "ZoneCheckEnemyInScene":
                    Debug.LogError("inScene");
                    break;
                case "PlayerBullet":
                    Pool_EffectTriggerBullet(col.transform);
                    if (!isShield)
                    {
                        TakeDamage(bullet.power);
                    }
                    bullet.Despawn();
                    break;
                case "PlayerBulletBurstShot":
                    Pool_EffectTriggerBurstShot(col.transform);
                    if (!isShield)
                    {
                        TakeDamage(bullet.power);
                    }
                    bullet.Despawn();
                    break;
                case "PlayerBulletStarbomb":
                    var bulletStarbomb = col.GetComponent<BulletActiveSkillPlane8>();
                    if (!isShield)
                    {
                        TakeDamage(bullet.power);
                    }
                    bulletStarbomb.StartExplosive();
                    break;
                case "PlayerBulletHoming":
                    if (!isShield)
                    {
                        TakeDamage(bullet.power);
                    }
                    Pool_EffectTriggerBullet(col.transform);
                    bullet.Despawn();
                    break;
                case "PlayerBulletWindSlash":
                    if (!isShield)
                    {
                        TakeDamage(bullet.power);
                    }
                    Pool_EffectTriggerBullet(col.transform);
                    break;
                case "Wing2SmallBird":
                    var bullet1 = col.GetComponent<Wing2SmallBird>();
                    Pool_EffectTriggerBullet(col.transform);
                    Debug.LogError("enemy Trigger Wing2SmallBird: " + bullet.power);
                    if (!isShield)
                    {
                        TakeDamage(bullet1.power);
                    }
                    break;
                case "PlayerMissile":
                    Debug.Log("enemy Trigger Player");
                    if (PoolManager.Pools["BulletPlayerPool"].IsSpawned(col.transform))
                    {
                        PoolManager.Pools["BulletPlayerPool"].Despawn(col.transform);
                    }
                    break;
            }
        }

    }

    public void Change_Atk_To_Idle()
    {
        animEnemy.SetTrigger("isIdle");
    }
}
