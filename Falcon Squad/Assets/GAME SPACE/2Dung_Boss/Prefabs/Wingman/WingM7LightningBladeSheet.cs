﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class WingM7LightningBladeSheet : MonoBehaviour {

    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;


    private static Dictionary<int, WingM7LightningBladeSheet> dictionary = new Dictionary<int, WingM7LightningBladeSheet>();

    /// <summary>
    /// 通过level获取WingM7LightningBladeSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingM7LightningBladeSheet的实例</returns>
    public static WingM7LightningBladeSheet Get(int level)
    {
        return dictionary[level];
    }

    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingM7LightningBladeSheet> GetDictionary()
    {
        return dictionary;
    }
}
