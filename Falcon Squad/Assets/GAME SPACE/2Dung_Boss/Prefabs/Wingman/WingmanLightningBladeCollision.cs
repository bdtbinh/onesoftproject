﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingmanLightningBladeCollision : MonoBehaviour
{
    public float power;
    private void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "EnemyBullet":
                Debug.Log("Wing2SmallBird Trigger EnemyBullet");
                UbhBullet bullet = col.transform.parent.GetComponent<UbhBullet>();
                if (bullet != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(bullet);
                }
                break;
            case "BulletEnemyDestroyByWing":
                Debug.Log("Wing2SmallBird Trigger BulletEnemyDestroyByWing");
                UbhBullet bullet2 = col.transform.parent.GetComponent<UbhBullet>();
                if (bullet2 != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(bullet2);
                }
                break;
            default:
                break;
        }
    }

}
