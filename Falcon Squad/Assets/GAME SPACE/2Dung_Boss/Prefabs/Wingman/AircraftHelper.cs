﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AircraftHelper : MonoBehaviour
{
    public float time = 4f;
    private float timeCache;
    private UISprite uISprite;
    private void Start()
    {
        uISprite = GetComponent<UISprite>();
        uISprite.fillAmount = 0f;
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "player")
        {
            timeCache += Time.deltaTime;
            uISprite.fillAmount = timeCache / time;
            if (uISprite.fillAmount >= 1)
            {
                timeCache = 0;
                Debug.LogError("Done");
                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "player")
        {
            Debug.LogError("Exit");
            timeCache = 0;
            uISprite.fillAmount = 0f;
        }
    }


}
