﻿using Sirenix.OdinInspector;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wingman7LightningBlade : MonoBehaviour
{

    //[Title("Data riêng", bold: true, horizontalLine: true)]

    //public GameObject[] listFxAnimDrone;
    //public GameObject[] listFxWingman;
    //public Transform lightBlade;
    //private GameObject fxAmingUse;
    //private GameObject fxWingmanUse;
    //private Vector2[] normalSize = { new Vector2(0.4f,0), new Vector2(0.6f,0.3f), new Vector2(0.3f,1.5f), new Vector2(0,5),
    //                                 new Vector2(-0.3f,1.5f), new Vector2(-0.6f,0.3f), new Vector2(-0.4f,0) };
    //private Vector2[] bigSize = {  new Vector2(0.4f,0), new Vector2(0.6f,0.3f), new Vector2(0.3f,1.5f), new Vector2(0,5),
    //                                 new Vector2(-0.3f,1.5f), new Vector2(-0.6f,0.3f), new Vector2(-0.4f,0) };
    //public PolygonCollider2D colliderInChid;
    //public WingmanLightningBladeCollision WingmanLightningBladeCollision;

    //public float timeActiveBlade;
    //public float timeHideBlade;

    //public void Start()
    //{
    //    base.Start();
    //    //colliderInChid = GetComponentInChildren<PolygonCollider2D>();

    //    InitDataByRank();

    //    for (int i = 0; i < listMainWeaponUbhSetData.Length; i++)
    //    {
    //        listMainWeaponUbhSetData[i].playerInitScript = playerInitScript;
    //        listMainWeaponUbhSetData[i].typePlaneShotBulletHell = TypeWingmanShotBulletHell;
    //        listMainWeaponUbhSetData[i].typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.WingmanMainPower;
    //    }

    //    WingmanLightningBladeCollision.power = mainWeaponPower;

    //}

    //void InitDataByRank()
    //{
    //    if (fxAmingUse == null)
    //    {
    //        fxAmingUse = Instantiate(listFxAnimDrone[((int)Rank) - 1], lightBlade);
    //        fxAmingUse.transform.localPosition = Vector3.zero;
    //        fxAmingUse.transform.localScale = new Vector3(0.69f, 0.69f, 0.69f);
    //    }
    //    if (fxWingmanUse == null)
    //    {
    //        fxWingmanUse = Instantiate(listFxWingman[((int)Rank) - 1], transform);
    //        fxWingmanUse.transform.localPosition = Vector3.zero;
    //        fxWingmanUse.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
    //    }

    //    colliderInChid.points = normalSize;

    //    if (HasSkillRank(Rank.C))
    //    {
    //        Debug.LogError("Rank C ChangePercentPowerForDrone_OverChange");
    //        ChangePercentPowerForDrone_OverChange(0.2f, 5);
    //    }
    //    if (HasSkillRank(Rank.B))
    //    {
    //        Debug.LogError("Rank B ChangePowerDrone");
    //        SupportFire_ChangePowerDrone(0.08f);
    //    }
    //    if (HasSkillRank(Rank.A))
    //    {
    //        Debug.LogError("Rank A ChangePowerSkillAirCraft");
    //        HiddenPower_ChangePowerSkillAircraft(0.1f);
    //    }
    //    if (HasSkillRank(Rank.S))
    //    {
    //        Debug.LogError("Rank S");
    //        colliderInChid.points = bigSize;
    //    }
    //    if (HasSkillRank(Rank.SS))
    //    {
    //        Debug.LogError("Rank SS ChangePowerSkillAirCraft");
    //        HiddenPower_ChangePowerSkillAircraft(0.1f);
    //    }
    //    if (HasSkillRank(Rank.SSS))
    //    {
    //        Debug.LogError("Rank SSS ChangeAttackSpeedMainGunAirCraft");
    //        ChangeAttackSpeedMainGunAirCraft(0.2f, 5);
    //    }
    //}

    //public IEnumerator ActiveBladeLoop()
    //{
    //    lightBlade.gameObject.SetActive(true);
    //    yield return new WaitForSeconds(timeActiveBlade);
    //    lightBlade.gameObject.SetActive(false);
    //    StartCoroutine(StartActiveLightBlade());
    //}

    //public IEnumerator StartActiveLightBlade()
    //{
    //    yield return new WaitForSeconds(timeHideBlade);
    //    StartCoroutine(ActiveBladeLoop());
    //}
    //protected override void LoadMainWeaponPowerData()
    //{
    //    if (mainWeaponPowerLevel >= WingM3LazerSheet.GetDictionary().Count)
    //    {
    //        mainWeaponPowerLevel = WingM3LazerSheet.GetDictionary().Count - 1;
    //    }
    //    mainWeaponPower = WingM3LazerSheet.Get(mainWeaponPowerLevel).mainweapon_power;
    //    //GetSpriteMainWeaponUse();
    //}


    //public override void ReloadData()
    //{

    //    base.Start();

    //    InitDataByRank();

    //    for (int i = 0; i < listMainWeaponUbhSetData.Length; i++)
    //    {
    //        listMainWeaponUbhSetData[i].playerInitScript = playerInitScript;
    //        listMainWeaponUbhSetData[i].typePlaneShotBulletHell = TypeWingmanShotBulletHell;
    //        listMainWeaponUbhSetData[i].typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.WingmanMainPower;
    //    }

    //    WingmanLightningBladeCollision.power = mainWeaponPower;
    //}
    //public override void ShootMainWeapon()
    //{
    //    StopAllCoroutines();
    //    StartCoroutine(ActiveBladeLoop());
    //}
    //public override void StopShootMainWeapon()
    //{
    //    StopAllCoroutines();
    //}
}
