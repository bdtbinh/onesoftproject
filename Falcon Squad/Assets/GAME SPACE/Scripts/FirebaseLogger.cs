﻿//using System.Collections.Generic;
//using Firebase.Analytics;
//using UnityEngine;

//public class FirebaseLogger
//{
//    public static string EVENT_USER_BUY_IAP = "buy_iap_event";
//    public static string EVENT_BUTTON_CLICK = "button_click_event";
//    public static string EVENT_COIN_CHANGE = "coin_change_event";
//    public static string EVENT_UPGRADE_ACTION = "upgrade_event";
//    public static string EVENT_SHOW_VIDEO = "video_show_event";
//    public static string EVENT_COMPLETE_VIDEO = "video_complete_event";
//    public static string EVENT_CANCEL_VIDEO = "video_cancel_event";


//    public static string PROPERTY_CURRENT_USER_STATUS = "PropertyCurrentUserStatus";

//    public static string PROPERTY_MAX_LEVEL_STATUS = "PropertyMaxLevelStatus";
//    // Các key của event dành cho log dữ liệu LEVEL
//    public static string EVENT_LEVEL_START = "level_start_event";
//    public static string EVENT_LEVEL_COMPLETE = "level_complete_event" ;
//    public static string EVENT_LEVEL_FAIL = "level_fail_event";

//    // game mode: easy = 1, medium = 2, hard = 3
//    public static string EVENT_LEVEL_GAME_MODE = "level_game_mode_event";

//    public static string EVENT_SHOW_FULLSCREEN_ADS = "fullscreen_show_event";

//    public static string LEVEL_PREFIX = "level_";

//    static Dictionary<string, float> timeLog = new Dictionary<string, float>();

//    // Các key cho parameter
//    public static string PARAM_IAP_PACK_NAME = "iap_pack_name_param";
//    public static string PARAM_IAP_PACK_VALUE = "iap_pack_value_param";

//    public static string PARAM_SCREEN_NAME = "screen_name_param";
//    public static string PARAM_LEVEL_PLAY_TIME = "level_play_time_param";
//    public static string PARAM_LEVEL_FAIL_POSITION = "level_fail_position_param";

//    public static string PARAM_ADS_TYPE = "ads_type_param";
//    public static string PARAM_VIDEO_TYPE = "video_type_param";
//    public static string PARAM_VIEW_VIDEO_REASON = "view_video_for_reason_param";
//    public static string PARAM_BUTTON_NAME = "button_name_param";
//    public static string PARAM_COIN_NAME = "coin_name_param";
//    public static string PARAM_COIN_CHANGE_AMOUNT = "coin_change_amount_param";
//    public static string PARAM_COIN_TOTAL_AFTER_CHANGE = "coin_total_after_change_param";
//    public static string PARAM_COIN_CHANGE_TYPE = "coin_change_type_param";
//    public static string PARAM_PLAYER_ID = "player_id_param";
//    public static string PARAM_UPGRADE_NAME = "upgrade_name_param";
//    public static string PARAM_UPGRADE_VALUE = "upgrade_value_param";


//    // PARAM VALUE dành cho coin change type
//    public static string COIN_CHANGE_TYPE_VIEW_VIDEO = "view_video_type";
//    public static string COIN_CHANGE_TYPE_BUY_IAP = "buy_iap_type";
//    public static string COIN_CHANGE_TYPE_FIRST_TIME = "first_time_type";

//    public static string COIN_CHANGE_TYPE_ACHIEVEMENT = "achievement_type";

//    public static string COIN_CHANGE_TYPE_DAILY_LOGIN = "daily_login_type";


//    /// <summary>
//    /// Hàm này dùng để log thông tin khi bắt đầu level
//    /// </summary>
//    /// <param name="level">Điền vào level hiện tại (int), ví dụ 15, class sẽ tự động convert thành chuỗi (string) dạng 0015 trước khi log lên Firebase </param>
//    public static void LogLevelStart(int level, int game_mode = 0)
//    {
//        var level_name = FormatLevelName(level);
//        float startTime = Time.realtimeSinceStartup;

//        // Remove before insert new one
//        timeLog.Remove(level_name);
//        timeLog.Add(level_name, startTime);

//        FirebaseAnalytics.LogEvent(
//            EVENT_LEVEL_START,
//            new Parameter(FirebaseAnalytics.ParameterLevel, level_name),
//            new Parameter(EVENT_LEVEL_GAME_MODE, game_mode)
//        );
//        Firebase.Analytics.FirebaseAnalytics.SetUserProperty(PROPERTY_CURRENT_USER_STATUS, "START_LEVEL_" + level_name);
//        Firebase.Analytics.FirebaseAnalytics.SetUserProperty(PROPERTY_MAX_LEVEL_STATUS, "MAX_LEVEL_" + CacheGame.GetMaxLevel3Difficult());
//    }
//    /// <summary>
//    /// Log Khi user hoàn thành level
//    /// </summary>
//    /// <param name="level">Điền vào level hiện tại (int), ví dụ 15, class sẽ tự động convert thành chuỗi (string) dạng 0015 trước khi log lên Firebase </param>
//    public static void LogLevelComplete(int level, int game_mode = 0)
//    {
//        var level_name = FormatLevelName(level);
//        LogLevelStatus(level, EVENT_LEVEL_COMPLETE,null, game_mode);
//        FirebaseAnalytics.LogEvent(
//            FirebaseAnalytics.EventLevelUp,
//            new Parameter(FirebaseAnalytics.ParameterLevel, level)
//        );
//        Firebase.Analytics.FirebaseAnalytics.SetUserProperty(PROPERTY_CURRENT_USER_STATUS, "COMPLETE_LEVEL_" + level_name);
//        Firebase.Analytics.FirebaseAnalytics.SetUserProperty(PROPERTY_MAX_LEVEL_STATUS, "MAX_LEVEL_" + CacheGame.GetMaxLevel3Difficult());
//    }

//    /// <summary>
//    /// Log khi user không qua nổi level
//    /// </summary>
//    /// <param name="level">Điền vào level hiện tại (int), ví dụ 15, class sẽ tự động convert thành chuỗi (string) dạng 0015 trước khi log lên Firebase </param>
//    /// <param name="currentPosition">Vị trí trong map hoặc trạng thái map hoặc một thông tin gì đó bạn muốn phân tích khi user không qua nổi</param>
//   public static void LogLevelFail(int level, string currentPosition, int game_mode = 0)
//    {
//        var level_name = FormatLevelName(level);
//        LogLevelStatus(level, EVENT_LEVEL_FAIL, currentPosition, game_mode);
//        Firebase.Analytics.FirebaseAnalytics.SetUserProperty(PROPERTY_CURRENT_USER_STATUS, "FAIL_LEVEL_" +level_name);
//        Firebase.Analytics.FirebaseAnalytics.SetUserProperty(PROPERTY_MAX_LEVEL_STATUS, "MAX_LEVEL_" + CacheGame.GetMaxLevel3Difficult());
//    }

//    /// <summary>
//    /// Bắt đầu show video cho người chơi
//    /// </summary>
//    /// <param name="level"> Show tại level nào, nếu không có thông tin level có thể điền 0</param>
//    /// <param name="video_type">Loại video, admob, unity, heyzap, ...</param>
//    /// <param name="screen_name"> Show tại màn hình nào, "in_game", "menu", "game_over", "game_victory" ... </param>
//    /// <param name="view_for_reason">Xem video vì lý do gì, "add_100_coin", "x2_coin", "more_live", ...</param>
//    public static void LogVideoShowStart(int level, string video_type, string screen_name, string view_for_reason)
//    {
//        LogVideoStatus(EVENT_SHOW_VIDEO, level, video_type, screen_name, view_for_reason);
//    }

//    /// <summary>
//    /// Khi người dùng hoàn thành video, (Được tặng thuưởng hoặc được thêm mạng)
//    /// </summary>
//    /// <param name="level"> Show tại level nào, nếu không có thông tin level có thể điền 0</param>
//    /// <param name="video_type">Loại video, admob, unity, heyzap, ...</param>
//    /// <param name="screen_name"> Show tại màn hình nào, "in_game", "menu", "game_over", "game_victory" ... </param>
//    /// <param name="view_for_reason">Xem video vì lý do gì, "add_100_coin", "x2_coin", "more_live", ...</param>
//    public static void LogVideoComplete(int level, string video_type, string screen_name, string view_for_reason)
//    {
//        LogVideoStatus(EVENT_COMPLETE_VIDEO, level, video_type, screen_name, view_for_reason);
//    }

//    /// <summary>
//    /// Khi người dùng đang xem nhưng quyết định cancel, bấm back hoặc bấm nút close trên video giữa chừng
//    /// </summary>
//    /// <param name="level"> Show tại level nào, nếu không có thông tin level có thể điền 0</param>
//    /// <param name="video_type">Loại video, admob, unity, heyzap, ...</param>
//    /// <param name="screen_name"> Show tại màn hình nào, "in_game", "menu", "game_over", "game_victory" ... </param>
//    /// <param name="view_for_reason">Xem video vì lý do gì, "add_100_coin", "x2_coin", "more_live", ...</param>
//    public static void LogVideoCancel(int level, string video_type, string screen_name, string view_for_reason)
//    {
//        LogVideoStatus(EVENT_CANCEL_VIDEO, level, video_type, screen_name, view_for_reason);
//    }
//    /// <summary>
//    /// Log thông tin bấm nút tại screen nào
//    /// </summary>
//    /// <param name="screen_name">"in_game", "menu", "game_over", "game_victory", "setting" ...</param>
//    /// <param name="button_name">Tên button cần log, "Pause", "Start", "Level Selector", ...</param>
//    public static void LogButtonClick(string screen_name, string button_name)
//    {
//        FirebaseAnalytics.LogEvent(
//        EVENT_BUTTON_CLICK,
//        new Parameter(PARAM_SCREEN_NAME, screen_name),
//        new Parameter(PARAM_BUTTON_NAME, button_name)
//        );
//    }
//    /// <summary>
//    /// Log sự thay đổi của đơn vị tiền tệ trong game, chỉ log khi có giao dịch cộng hoặc trừ coin, ví dụ sau khi xem xong video, user được cộng 100 coin,
//    /// sau khi nâng cấp, user bị giảm 125 coin, user được tặng 1000 coin khi bắt đầu cài game, ....
//    /// </summary>
//    /// <param name="coin_name">Tên coin trong game: coin, gem, crytal ...</param>
//    /// <param name="coin_change_amount">Giá trị coin thay đổi, -100, + 1000 ... </param>
//    /// <param name="coin_total_after_change">Giá trị coin user sở hữu sau khi thay đổi </param>
//    /// <param name="coin_change_type">Có thể dùng các giá trị static như: COIN_CHANGE_TYPE_VIEW_VIDEO, COIN_CHANGE_TYPE_BUY_IAP ...</param>
//    public static void LogCoinChange(string coin_name, int coin_change_amount, int coin_total_after_change, string coin_change_type)
//    {
//        FirebaseAnalytics.LogEvent(
//        EVENT_COIN_CHANGE,
//        new Parameter(PARAM_COIN_NAME, coin_name),
//        new Parameter(PARAM_COIN_CHANGE_AMOUNT, coin_change_amount),
//        new Parameter(PARAM_COIN_TOTAL_AFTER_CHANGE, coin_total_after_change),
//        new Parameter(PARAM_COIN_CHANGE_TYPE, coin_change_type)
//        );
//    }
//    /// <summary>
//    ///
//    /// </summary>
//    /// <param name="player_id">Upgrade cho player id nào: "player_1","player_2", "wing_man", "pet", ....</param>
//    /// <param name="upgrade_name">Tên loại upgrade, skill, power, damage, bomb, shield, ...</param>
//    /// <param name="upgrade_value">Giá trị sau khi được upgrade </param>
//    /// <param name="level">Level hiện tại, nếu không lấy được truyền 0</param>
//    public static void LogUpgrade(string player_id, string upgrade_name, float upgrade_value, int level)
//    {
//        FirebaseAnalytics.LogEvent(
//        EVENT_UPGRADE_ACTION,
//        new Parameter(PARAM_PLAYER_ID, player_id),
//        new Parameter(PARAM_UPGRADE_NAME, upgrade_name),
//        new Parameter(PARAM_UPGRADE_VALUE, upgrade_value),
//        new Parameter(FirebaseAnalytics.ParameterLevel, FormatLevelName(level))
//        );
//    }

//    /// <summary>
//    /// Log khi user thực hiện thành công giao dịch IAP
//    /// </summary>
//    /// <param name="iap_pack_name">Mua gói tên gì (Gem pack 999, VIP pack ...)</param>
//    /// <param name="iap_pack_value">Giá trị gói 0.99, 1.99 ....</param>
//    /// <param name="level">Mua ở level nào, nếu không lấy được truyền 0</param>
//    public static void LogIAP(string iap_pack_name, float iap_pack_value, int level)
//    {
//        FirebaseAnalytics.LogEvent(
//        EVENT_USER_BUY_IAP,
//        new Parameter(PARAM_IAP_PACK_NAME, iap_pack_name),
//        new Parameter(PARAM_IAP_PACK_VALUE, iap_pack_value),
//        new Parameter(FirebaseAnalytics.ParameterLevel, FormatLevelName(level))
//        );
//    }
//    /// <summary>
//    /// Log thông tin hiển thị ads full màn hình
//    /// </summary>
//    /// <param name="ads_type">Loại ads, admob, unity, heyzap, ...</param>
//    /// <param name="screen_name"></param>
//    public static void LogShowFullScreenAds(string ads_type,string screen_name)
//    {
//        FirebaseAnalytics.LogEvent(
//        EVENT_SHOW_FULLSCREEN_ADS,
//        new Parameter(PARAM_SCREEN_NAME, screen_name),
//        new Parameter(PARAM_ADS_TYPE, ads_type)
//        );
//    }

//    public static string FormatLevelName(int level)
//    {
//        var level_name = level.ToString();
//        level_name = LEVEL_PREFIX + level_name.PadLeft(4, '0');
//        return level_name;
//    }

//    // Các hàm private không dùng
//    private static void LogVideoStatus(string eventName, int level, string video_type, string screen_name, string view_for_reason)
//    {
//        FirebaseAnalytics.LogEvent(
//        eventName,
//        new Parameter(FirebaseAnalytics.ParameterLevel, FormatLevelName(level)),
//        new Parameter(PARAM_VIDEO_TYPE, video_type),
//        new Parameter(PARAM_SCREEN_NAME, screen_name),
//        new Parameter(PARAM_VIEW_VIDEO_REASON, view_for_reason)
//        );
//    }

//    private static void LogLevelStatus(int level, string eventName, string currentPosition = null, int game_mode = 0)
//    {
//        float endTime = Time.realtimeSinceStartup;
//        var level_name = FormatLevelName(level);
//        float startTime = -1;
//        bool hasData = timeLog.TryGetValue(level_name, out startTime);
//        float level_play_time = -1;
//        if (hasData && startTime > 0)
//        {
//            level_play_time = (endTime - startTime);
//            timeLog.Remove(level_name);
//        }

//        if (currentPosition == null)
//        {
//            FirebaseAnalytics.LogEvent(
//                eventName,
//                new Parameter(FirebaseAnalytics.ParameterLevel, level_name),
//                new Parameter(PARAM_LEVEL_PLAY_TIME, (int)level_play_time),
//                new Parameter(EVENT_LEVEL_GAME_MODE, game_mode)
//            );
//        }
//        else
//        {
//            FirebaseAnalytics.LogEvent(
//                eventName,
//                new Parameter(FirebaseAnalytics.ParameterLevel, level_name),
//                new Parameter(PARAM_LEVEL_PLAY_TIME, (int)level_play_time),
//                new Parameter(PARAM_LEVEL_FAIL_POSITION, currentPosition),
//                new Parameter(EVENT_LEVEL_GAME_MODE, game_mode)
//            );
//        }
//    }


//	public const string EVENT_INTERTITIAL_STATUS = "intertitial_show_event";
//	public const string PARAM_INTERTITIAL_STATUS = "intertitial_show_status";
//	public const string INTERTITIAL_IRONSRC = "ironsrc";
//	public const string INTERTITIAL_ADMOB = "admob";
//	public const string INTERTITIAL_NOTHING = "nothing";
//	public static void LogShowIntertitialStatus(string status)
//    {
//        FirebaseAnalytics.LogEvent(
//        EVENT_INTERTITIAL_STATUS,
//        new Parameter(PARAM_INTERTITIAL_STATUS, status)

//        );
//    }
//}
