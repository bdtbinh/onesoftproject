﻿using UnityEngine;
using System.Collections;
using TCore;
using UnityEngine.SceneManagement;
using DG.Tweening;
using EasyMobile;
using Firebase.Analytics;
using System;
using I2.Loc;

public class PopupEndGameVideo : MonoBehaviour
{
    public UILabel lTime;

    public ListItemReward[] listItemReward;

    [System.Serializable]
    public class ListItemReward
    {
        [Tooltip("Item sẽ nhận được")]
        public UISprite sItem;
        public UILabel lValue;
    }

    string listTextGiftToast;
    Tweener tweenlTimeID = null;
    string keyGetData;
    void Start()
    {
        int indexGetData = ((CacheGame.GetNumUseVideoEndGameOneDay() + CacheGame.GetIndexRandomVideoEndGameFirst()) % 5) + 1;

        keyGetData = "World" + CacheGame.GetCurrentWorld() + "Show" + indexGetData;

        Debug.LogError("PopupEndGameVideo keyGetData: " + keyGetData);
        Debug.LogError("World" + CacheGame.GetCurrentWorld());

        //mainWeaponPower = WingM1GaltingSheet.Get(mainWeaponPowerLevel).mainweapon_power;
        StartTimer();
        ShowGiftInReward();
    }

    public void StartTimer()
    {
        tweenlTimeID = lTime.DOUILabelInt(10, 0, 15f).SetDelay(0.2f).SetUpdate(true).OnComplete(() =>
        {
            Debug.Log("completed Video");
            PopupManagerCuong.Instance.HideVideoEndgamePopup();
        });
    }

    private void OnDisable()
    {
        CacheGame.SetNumUseVideoEndGameOneDay(CacheGame.GetNumUseVideoEndGameOneDay() + 1);
    }


    public void ButtonVideoAds()
    {
        SoundManager.PlayClickButton();
        //CallBackFinishVideoAds();
        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            tweenlTimeID.Kill();
            CallBackFinishVideoAds();
        }
        else
        {
            if (OsAdsManager.Instance.isRewardedVideoAvailable())
            {
                tweenlTimeID.Kill();
                OsAdsManager.Instance.AddRewardedListenner(CallBackFinishVideoAds);
                OsAdsManager.Instance.AddClosedListenner(CallBackClosedVideoAds);
                OsAdsManager.Instance.ShowRewardedAds(OsAdsManager.LocationClickVideo.btn_video_EndGame);
            }
            else
            {
                if (PopupManagerCuong.Instance.readyShowNotifi)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_ads_not_available, false, 1.3f);
                }
            }
        }

    }

    public void ButtonClose()
    {
        PopupManagerCuong.Instance.HideVideoEndgamePopup();
    }


    bool videoCallBackFinish;
    void CallBackFinishVideoAds()
    {
        videoCallBackFinish = true;

        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(listItemReward.Length);

        for (int i = 0; i < listItemReward.Length; i++)
        {
            string typeGift = EndGameVideoRewardSheet.Get(keyGetData).typeGiftList[i];
            int numGift = EndGameVideoRewardSheet.Get(keyGetData).numGiftList[i];

            AddGiftToPlayer(typeGift, numGift);

            popupItemRewardIns.ShowOneItem(i + 1, typeItemPopupItemReward, numItemPopupItemReward);
            //---
        }
        //PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_HaveReceived + listTextGiftToast, true, 3f);
        //
        PopupManagerCuong.Instance.HideVideoEndgamePopup();

    }

    void CallBackClosedVideoAds()
    {
        if (!videoCallBackFinish)
        {
            PopupManagerCuong.Instance.HideVideoEndgamePopup();
        }
        videoCallBackFinish = false;
    }

    //--------------------------------------------------------------------------------------
    void ShowGiftInReward()
    {
        for (int i = 0; i < listItemReward.Length; i++)
        {
            string typeGift = EndGameVideoRewardSheet.Get(keyGetData).typeGiftList[i];
            int numGift = EndGameVideoRewardSheet.Get(keyGetData).numGiftList[i];

            listItemReward[i].lValue.text = "" + numGift;
            SetTypeGiftInItemReWard(typeGift, listItemReward[i].sItem);

            if (numGift < 0)
            {
                listItemReward[i].lValue.gameObject.SetActive(false);
            }
            else
            {
                listItemReward[i].lValue.gameObject.SetActive(true);
            }
            //---
        }
    }

    void SetTypeGiftInItemReWard(string typeItem, UISprite spriteItemGift)
    {
        switch (typeItem)
        {
            case "Gold":
                spriteItemGift.spriteName = "icon_coin";
                break;
            case "PowerUp":
                spriteItemGift.spriteName = "icon_dailyquest_powerup";
                break;
            case "EMP":
                spriteItemGift.spriteName = "icon_dailyquest_time";
                break;
            case "Life":
                spriteItemGift.spriteName = "icon_dailyquest_life";
                break;
            case "Gem":
                spriteItemGift.spriteName = "icon_daily_quest_gem";
                break;
            case "BoxCard1":
                spriteItemGift.spriteName = "card_R";
                break;
            case "BoxCard2":
                spriteItemGift.spriteName = "card_R";
                break;
            case "BoxCard3":
                spriteItemGift.spriteName = "card_R";
                break;
            case "BoxCard4":
                spriteItemGift.spriteName = "card_R";
                break;
        }
    }


    GameContext.TypeItemInPopupItemReward typeItemPopupItemReward;
    int numItemPopupItemReward;

    void AddGiftToPlayer(string giftType, int numGift)
    {
        numItemPopupItemReward = numGift;
        switch (giftType)
        {
            case "Gold":
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddCoinTop(numGift, FirebaseLogSpaceWar.VideoEndGame_why);
                }
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Gold;
                break;
            case "Gem":
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddGemTop(numGift, FirebaseLogSpaceWar.VideoEndGame_why);
                }
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Gem;
                break;
            case "PowerUp":
                CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + numGift);
                SoundManager.PlayClickButton();
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.PowerUp;
                break;
            case "EMP":
                CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + numGift);
                SoundManager.PlayClickButton();
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.EMP;
                break;
            case "Life":
                CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + numGift);
                SoundManager.PlayClickButton();
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Life;
                break;
            case "BoxCard1":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.VideoEndGame_why, "BoxCard1");
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
            case "BoxCard2":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.VideoEndGame_why, "BoxCard2");
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
            case "BoxCard3":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.VideoEndGame_why, "BoxCard3");
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
            case "BoxCard4":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.VideoEndGame_why, "BoxCard4");
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
        }
    }
}
