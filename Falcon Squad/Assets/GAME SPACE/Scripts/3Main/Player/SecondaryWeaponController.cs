﻿using PathologicalGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using UnityEngine.SceneManagement;

public class SecondaryWeaponController : MonoBehaviour {


    public GameObject[] listSecondaryWeapon;

    GameObject secondaryWeaponUse;

    void Start () {

        Invoke("SetSecondaryWeaponStart", 1f);
    }




    public void SetSecondaryWeaponStart()
    {

        //PlayerMove.Instance.useSecondaryWeaponTest = false;
        //if (PlayerMove.Instance.useSecondaryWeaponTest)
        //{
        //    CacheGame.SetSecondaryWeaponSelected(PlayerMove.Instance.indexSecondaryWeaponTest);
        //}

        ////---

        //int secondarySwitch = 0;
        //if (SceneManager.GetActiveScene().name == "LevelTryPlane" && GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.SecondaryWeapon)
        //{
        //    secondarySwitch = CacheGame.GetSecondaryWeaponUserTry();
        //}
        //else
        //{
        //    secondarySwitch = CacheGame.GetSecondaryWeaponSelected();

        //    //secondarySwitch = 4;
        //}
        ////Debug.LogError("secondarySwitch " + secondarySwitch);


        //if (secondarySwitch != 0 && secondarySwitch <= listSecondaryWeapon.Length)
        //{
        //    secondaryWeaponUse = Instantiate(listSecondaryWeapon[secondarySwitch - 1], transform);
        //    secondaryWeaponUse.transform.localPosition = Vector3.zero;
        //}
    }


    //-----

    public void SecondaryWeaponStartShoot()
    {
        if (secondaryWeaponUse != null)
        {
            secondaryWeaponUse.GetComponent<SecondaryWeaponShootManager>().secondaryWeaponGunIns.SecondaryWeaponStartShoot();
        }


    }


    public void SecondaryWeaponStopShoot()
    {
        if (secondaryWeaponUse != null)
        {
            secondaryWeaponUse.GetComponent<SecondaryWeaponShootManager>().secondaryWeaponGunIns.SecondaryWeaponStopShoot();
        }

    }

}
