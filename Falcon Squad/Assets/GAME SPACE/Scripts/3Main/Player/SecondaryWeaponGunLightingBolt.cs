﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.ThunderAndLightning;
using PathologicalGames;
using SkyGameKit;

public class SecondaryWeaponGunLightingBolt : SecondaryWeaponGun
{
    private const string tagNameTargetBoss = "Boss1";
    private const string tagNameTargetBoss2 = "Boss2";
    private const string tagNameTargetBoss3 = "Boss3";
    private const string tagNameTargetBoss3_Shield_Core = "Boss3_Shield_Core";
    private const string tagNameTargetBoss4 = "Boss4";
    private const string tagNameTargetBoss5 = "Boss5";
    private const string tagNameTargetNhen = "EnemyNhen_Child";
    private const string tagNameTargetEnemy = "Enemy";
    private const string tagNameTargetPlayer = "player";

    [SerializeField]
    private float damage;

    [SerializeField]
    private float showTime = 0.5f;

    [SerializeField]
    private float hideTime = 3f;

    [SerializeField]
    private LightningBoltPrefabScript[] lightningBoltPrefab;

    [SerializeField]
    private LightningBoltTransformTrackerScript[] tracker;

    [SerializeField]
    private ParticleSystem fxBeforeShoot;

    List<GameObject> listEnemy = new List<GameObject>(3);


    private void Awake()
    {
        for (int i = 0; i < lightningBoltPrefab.Length; i++)
        {
            lightningBoltPrefab[i].Camera = MainScene.Instance.cameraTk2d;
        }
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    public override void SecondaryWeaponStartShoot()
    {
        base.SecondaryWeaponStartShoot();

        coShowHide = CoShowHide();
        StartCoroutine(coShowHide);
    }


    public override void SecondaryWeaponStopShoot()
    {
        base.SecondaryWeaponStopShoot();
        StopAllCoroutines();

        StopParticles();
    }

    void UpdateListEnemy()
    {
        listEnemy.Clear();
        Collider2D[] colliders = Physics2D.OverlapAreaAll(MainScene.Instance.posAnchorTopLeft.position, MainScene.Instance.posAnchorBottomRight.position);

        for (int i = 0; i < colliders.Length; i++)
        {
            if (listEnemy.Count > 2)
                break;

            if (LayerMask.LayerToName(colliders[i].gameObject.layer) == "Enemy")
            {
                listEnemy.Add(colliders[i].gameObject);
            }
        }
    }

    void DamageEnemy(GameObject enemy)
    {
        if (enemy.GetComponent<EnemyCollisionManager>() != null)
        {
            if (enemy.GetComponent<EnemyCollisionManager>().enemyCollisionBase != null)
            {
                enemy.GetComponent<EnemyCollisionManager>().enemyCollisionBase.TakeDamage(GetDataShopToMain.Instance.secondaryWeaponPower);
            }
        }
    }

    void PlayParticles()
    {
        if (fxBeforeShoot != null)
        {
            fxBeforeShoot.gameObject.SetActive(true);
            fxBeforeShoot.Play(true);
        }
    }

    void StopParticles()
    {
        if (fxBeforeShoot != null)
        {
            fxBeforeShoot.gameObject.SetActive(false);
            fxBeforeShoot.Stop(true);
        }
    }

    IEnumerator coShowHide;
    IEnumerator CoShowHide()
    {
        while (true)
        {
            //Show lightning bolt
            UpdateListEnemy();

            if(listEnemy.Count > 0)
            {
                PlayParticles();
            }

            for (int i = 0; i < listEnemy.Count; i++)
            {
                lightningBoltPrefab[i].Destination = listEnemy[i];
                tracker[i].EndTarget = listEnemy[i].transform;
                lightningBoltPrefab[i].gameObject.SetActive(true);
                DamageEnemy(listEnemy[i]);
                Pool_LightningBoltFX(listEnemy[i].transform);
            }

            yield return new WaitForSecondsRealtime(showTime);


            //Hide target
            StopParticles();
            for (int i = 0; i < listEnemy.Count; i++)
            {
                lightningBoltPrefab[i].gameObject.SetActive(false);
            }

            yield return new WaitForSecondsRealtime(hideTime);
        }
    }


    private void Pool_LightningBoltFX(Transform enemyTrans)
    {
        if (EffectList.Instance.enemyTriggerBulletLightingBolt != null && enemyTrans.gameObject.activeInHierarchy)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyTriggerBulletLightingBolt, enemyTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
