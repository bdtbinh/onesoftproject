﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WingmanController : MonoBehaviour
{

    [Range(10, 50)]
    public float wingmanSpeed = 16;

    //[HideInInspector]
    public Transform playerTrans;

    //PlayerController playerControllerScript;
    //


    GameObject wingmanLeftUse, wingmanRightUse;

    public GameObject[] listWingmanPrefab;

    //private 



    //Vector3 posWingmanLeft = new Vector3 

    void Start()
    {
        //playerControllerScript = playerTrans.gameObject.GetComponent<PlayerController>();
        //ShowWingmanChoose();

        Invoke("ShowWingmanChoose", 2f);
        //playerControllerScript.wingmanControllerIns = this;
        
    }

    // Update is called once per frame
    void Update()
    {
        var targetpos = playerTrans.position;
        targetpos.y -= 0.5f;
        transform.position = Vector3.Lerp(transform.position, targetpos, Time.deltaTime * wingmanSpeed);
    }


    void ShowWingmanChoose()
    {
        //PlayerMove.Instance.useWingmanTest = false;
        //if (PlayerMove.Instance.useWingmanTest)
        //{
        //    CacheGame.SetWingManLeftUserSelected(PlayerMove.Instance.indexWingmanTest);
        //}


        //---
        int wingmanLeftSwitch = 0;
        int wingmanRightSwitch = 0;

        if (SceneManager.GetActiveScene().name == "LevelTryPlane" && GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Wingman)
        {
            //wingmanLeftSwitch = CacheGame.GetWingManUserTry();
            //wingmanRightSwitch = 0;
            if (GameContext.typeTryWingman == GameContext.TypeTryWingman.Left)
            {
                wingmanLeftSwitch = CacheGame.GetWingManUserTry();
                wingmanRightSwitch = CacheGame.GetWingManRightUserSelected();
            }
            else
            {
                wingmanLeftSwitch = CacheGame.GetWingManLeftUserSelected();
                wingmanRightSwitch = CacheGame.GetWingManUserTry();
            }
        }
        else
        {
            wingmanLeftSwitch = CacheGame.GetWingManLeftUserSelected();
            wingmanRightSwitch = CacheGame.GetWingManRightUserSelected();

            //wingmanLeftSwitch = 3;
            //wingmanRightSwitch = 4;
        }


        //planeUse = Instantiate(listPlanePrefab[indexPlaneTest - 1], transform);

        if (wingmanLeftSwitch != 0 && wingmanLeftSwitch <= listWingmanPrefab.Length)
        {
            wingmanLeftUse = Instantiate(listWingmanPrefab[wingmanLeftSwitch - 1], transform);
            wingmanLeftUse.transform.localPosition = new Vector3(-1f, 0f, 0f);
            //playerControllerScript.wingmanLeft = wingmanLeftUse;
        }

        if (wingmanRightSwitch != 0 && wingmanRightSwitch <= listWingmanPrefab.Length)
        {
            wingmanRightUse = Instantiate(listWingmanPrefab[wingmanRightSwitch - 1], transform);
            wingmanRightUse.transform.localPosition = new Vector3(1f, 0f, 0f);
            //playerControllerScript.wingmanRight = wingmanRightUse;
        }

    }


    //-------------------

    public void WingmanStartShoot()
    {
        if (wingmanLeftUse != null)
        {
            wingmanLeftUse.GetComponent<WingmanShootManager>().wingmanGunIns.WingmanStartShoot();
        }

        if (wingmanRightUse != null)
        {
            wingmanRightUse.GetComponent<WingmanShootManager>().wingmanGunIns.WingmanStartShoot();
        }
    }


    public void WingmanStopShoot()
    {
        if (wingmanLeftUse != null)
        {
            wingmanLeftUse.GetComponent<WingmanShootManager>().wingmanGunIns.WingmanStopShoot();
        }

        if (wingmanRightUse != null)
        {
            wingmanRightUse.GetComponent<WingmanShootManager>().wingmanGunIns.WingmanStopShoot();
        }
    }
}
