﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingmanGunBulletHell : WingmanGun
{

    [Title("Wingman bắn BulletHell", bold: true, horizontalLine: true)]
    public UbhShotCtrl mainWeaponUbh;
    //UbhBaseShot ubhBaseShotWingman;




    void OnDisable()
    {
        StopAllCoroutines();
    }



    public override void WingmanStartShoot()
    {
        base.WingmanStartShoot();
        StopAllCoroutines();
        if (mainWeaponUbh != null)
        {
            mainWeaponUbh.StartShotRoutine();
        }
    }


    public override void WingmanStopShoot()
    {
        base.WingmanStopShoot();
        if (mainWeaponUbh != null)
        {
            mainWeaponUbh.StopShotRoutine();
        }
    }

    
}
