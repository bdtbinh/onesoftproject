﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingmanGunLaser : WingmanGun
{

    [Title("Wingman laser", bold: true, horizontalLine: true)]
    public MinhLaser laserHitScript;

    public float timeShowLaser = 2f;
    public float timeHideLaser = 3f;

    void Start()
    {

        if (laserHitScript != null)
        {
            laserHitScript.OnLaserDamageTarget += LaserOnOnLaserHitTriggered;
        }
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }



    public override void WingmanStartShoot()
    {
        base.WingmanStartShoot();
        StopAllCoroutines();
        if (laserHitScript != null)
        {
            StartCoroutine("LaserShootLoop", 1f);
        }
    }


    public override void WingmanStopShoot()
    {
        base.WingmanStopShoot();
        StopAllCoroutines();
        if (laserHitScript != null)
        {
            laserHitScript.gameObject.SetActive(false);
        }
    }


    IEnumerator LaserShootLoop(float timeDelay)
    {
        //while (true)
        //{

        yield return new WaitForSeconds(timeDelay);
        if (gameObject.activeInHierarchy)
        {
            laserHitScript.gameObject.SetActive(true);
            //Debug.LogError("laser shoot");
            StartCoroutine("LaserPauseShoot", timeShowLaser);
        }
        else
        {
            StopAllCoroutines();
        }
        //}
    }
    IEnumerator LaserPauseShoot(float timeDelayStop)
    {
        yield return new WaitForSeconds(timeDelayStop);
        if (gameObject.activeInHierarchy)
        {
            //Debug.LogError("laser pause shoot");
            laserHitScript.gameObject.SetActive(false);
            StartCoroutine("LaserShootLoop", timeHideLaser);
        }
        else
        {
            StopAllCoroutines();
        }
    }
    





    private void LaserOnOnLaserHitTriggered(RaycastHit2D hitInfo)
    {

        if (LayerMask.LayerToName(hitInfo.collider.gameObject.layer) == "Enemy")
        {
            if (hitInfo.collider.GetComponent<EnemyCollisionManager>() != null)
            {
                if (hitInfo.collider.GetComponent<EnemyCollisionManager>().enemyCollisionBase != null)
                {
                    hitInfo.collider.GetComponent<EnemyCollisionManager>().enemyCollisionBase.LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.powerWingman3Laser);
                }
            }
        }
    }
}
