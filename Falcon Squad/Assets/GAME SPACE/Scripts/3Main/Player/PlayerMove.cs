﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using SkyGameKit;
using UnityEngine.SceneManagement;
using Sirenix.OdinInspector;

public class PlayerMove : Singleton<PlayerMove>
{
    //
    public float playerSpeed = 8f;

    private float posMaxMoveLeft, posMaxMoveRight;
    private float posMaxMoveTop, posMaxMoveBottom;

    public tk2dCamera tk2dCameraMain;
    private Camera cmr;

    private Vector3 screenPoint;
    private Vector3 offset;

    //--------

    public GameObject[] listPlanePrefab;

    [HideInInspector]
    public GameObject planeUse;
    Transform planeUse_Trans;

    void Start()
    {
        cmr = tk2dCameraMain.GetComponent<Camera>();

        posMaxMoveLeft = MainScene.Instance.posAnchorLeft.localPosition.x + 0.21f;
        posMaxMoveRight = MainScene.Instance.posAnchorRight.localPosition.x - 0.21f;
        //
        posMaxMoveTop = MainScene.Instance.posAnchorTop.localPosition.y - 0.5f;
        posMaxMoveBottom = MainScene.Instance.posAnchorBottom.localPosition.y + 0.5f;
    }



    void OnMouseDown()
    {
        if (!MainScene.Instance.gameStopping)
        {
            if (!MainScene.Instance.playerStopMove)
            {
                screenPoint = cmr.WorldToScreenPoint(gameObject.transform.position);
                if (Input.touchCount > 0)
                {
                    curScreenPointOrigi = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0f);
                }
                else
                {
                    curScreenPointOrigi = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
                }
                offset = planeUse_Trans.position - cmr.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));

                curScreenPointStep1 = cmr.ScreenToWorldPoint(curScreenPointOrigi);
                curScreenPointStep2 = curScreenPointStep1;
            }
        }
    }
    //

    void OnMouseUp()
    {
        if (!MainScene.Instance.gameStopping)
        {
            if (!MainScene.Instance.playerStopMove)
            {
                curScreenPointStep1 = curScreenPointStep2;
                offset = Vector3.zero;
                planeUse_Trans = planeUse.transform;
            }
        }
    }
    //
    Vector3 curScreenPointOrigi;
    Vector3 curScreenPointStep1, curScreenPointStep2, posScreenChange;
    Vector3 posChangePlayerBonus;
    Vector3 newPositionPlayer;

    void OnMouseDrag()
    {
        if (!MainScene.Instance.gameStopping)
        {

            if (!MainScene.Instance.playerStopMove)
            {

                if (Input.touchCount > 0)
                {
                    curScreenPointOrigi = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0f);
                }
                else
                {
                    curScreenPointOrigi = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
                }

                curScreenPointStep1 = cmr.ScreenToWorldPoint(curScreenPointOrigi);

                posScreenChange = curScreenPointStep2 - curScreenPointStep1;

                posChangePlayerBonus = posScreenChange * CacheGame.GetSpeedPlayer();

                newPositionPlayer = curScreenPointStep1 + offset - posChangePlayerBonus;

                planeUse_Trans.position = newPositionPlayer;

                offset = planeUse_Trans.position - curScreenPointStep1;

                if (planeUse_Trans.position.z != 0)
                {
                    planeUse_Trans.position = new Vector3(planeUse_Trans.position.x, planeUse_Trans.position.y, 0f);
                }

                if (planeUse_Trans.position.y > posMaxMoveTop)
                {
                    planeUse_Trans.position = new Vector3(planeUse_Trans.position.x, posMaxMoveTop, 0f);
                }
                else if (planeUse_Trans.position.y < posMaxMoveBottom)
                {
                    planeUse_Trans.position = new Vector3(planeUse_Trans.position.x, posMaxMoveBottom, 0f);
                }

                if (planeUse_Trans.position.x > posMaxMoveRight)
                {
                    planeUse_Trans.position = new Vector3(posMaxMoveRight, planeUse_Trans.position.y, 0f);
                }
                else if (planeUse_Trans.position.x < posMaxMoveLeft)
                {
                    planeUse_Trans.position = new Vector3(posMaxMoveLeft, planeUse_Trans.position.y, 0f);
                }
                //
                curScreenPointStep2 = curScreenPointStep1;
            }
        }
    }
}
