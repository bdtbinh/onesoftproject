﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;

public class BulletChainsawController : MonoBehaviour
{


    UbhBullet ubhBulletIns;
    Transform transCache;

    Vector3 tranStopMove;
    Vector3 posMove = new Vector3(0f, 3.8f, 0f);
    bool isMoving;
    void Start()
    {
        ubhBulletIns = gameObject.GetComponent<UbhBullet>();
        //transCache = transform;
    }


    private void Awake()
    {
        timelineIsEnable = CheckTimelineEnable();
    }

    //--------------------Check obj có phải thuộc player không đổi đổi time khi dùng EMP-----------------------------------------------------------------------------

    bool timelineIsEnable = false;
    Timeline timelineInObj;

    bool CheckTimelineEnable()
    {
        if (GetComponent<Timeline>() != null)
        {
            timelineInObj = GetComponent<Timeline>();
            return true;
        }
        return false;
    }

    float GetTimeSphereOfObj()
    {
        if (timelineIsEnable)
        {
            if (timelineInObj.timeScale != 0)
            {
                return timelineInObj.timeScale;
            }
        }
        return 1f;
    }
    //------------------------------------------------------------------------------------------


    void Update()
    {
        if (isMoving)
        {

            if (transform.localPosition.y >= tranStopMove.y)
            {
                ubhBulletIns.m_shooting = false;
                StartCoroutine(DelayDespawnBullet());
                isMoving = false;
            }

        }
    }

    void OnEnable()
    {
        //SetValueStart();

        //Debug.Log(transform.localPosition);
        StartCoroutine(DelayGetPosPause());
    }


    IEnumerator DelayGetPosPause()
    {
        yield return new WaitForSecondsRealtime(0.04f);
        isMoving = true;
        tranStopMove = transform.localPosition + posMove;
    }


    IEnumerator DelayDespawnBullet()
    {
        yield return new WaitForSeconds(3.5f / GetTimeSphereOfObj());
        MainScene.Instance.NoteObjPlayerChangeTime();
        Despawn();
    }

    void OnDisable()
    {
        StopAllCoroutines();
        isMoving = false;
    }

    void Despawn()
    {
        if (ubhBulletIns != null)
        {
            UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
        }
    }

}
