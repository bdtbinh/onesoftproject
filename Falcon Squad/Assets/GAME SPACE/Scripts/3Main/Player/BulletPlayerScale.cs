﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayerScale : MonoBehaviour
{

    public TweenScale tweenScaleBullet;

    public Vector3 sizeStart;
    //void Start()
    //{

    //}

    void OnEnable()
    {
        tweenScaleBullet.enabled = true;
        tweenScaleBullet.ResetToBeginning();
    }

    void OnDisable()
    {
        transform.localScale = sizeStart;
    }
}
