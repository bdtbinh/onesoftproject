﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondaryWeaponGunBulletHell : SecondaryWeaponGun
{


    [Title("SecondaryWeapon bắn BulletHell", bold: true, horizontalLine: true)]
    public UbhShotCtrl mainWeaponUbh;

    public GameObject fxBeforeShoot;
    float timeHideFx = 1f;
    public float timeDelayShootLoop = 6f;
    



    void OnDisable()
    {
        StopAllCoroutines();
    }

    public override void SecondaryWeaponStartShoot()
    {
        base.SecondaryWeaponStartShoot();
        StopAllCoroutines();
        if (mainWeaponUbh != null)
        {
            StartShoot();
        }
    }


    public override void SecondaryWeaponStopShoot()
    {
        base.SecondaryWeaponStopShoot();

        StopAllCoroutines();
        if (fxBeforeShoot != null)
        {
            fxBeforeShoot.SetActive(false);
        }

        if (mainWeaponUbh != null)
        {
            mainWeaponUbh.StopShotRoutine();
        }
    }

//----
    public void StartShoot()
    {
        StartCoroutine(ShootLoopBullet());
    }

    IEnumerator ShootLoopBullet()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelayShootLoop);

            FxBeforeShoot();
        }
    }

    void FxBeforeShoot()
    {
        if (fxBeforeShoot != null)
        {
            fxBeforeShoot.SetActive(true);
            fxBeforeShoot.GetComponent<ParticleSystem>().Play(true);
        }


        StartCoroutine(ShootOneBullet());
    }


    IEnumerator ShootOneBullet()
    {

        yield return new WaitForSeconds(timeHideFx);

        if (fxBeforeShoot != null)
        {
            fxBeforeShoot.SetActive(false);
        }

        if (mainWeaponUbh != null)
        {
            mainWeaponUbh.StartShotRoutine();
        }

    }
}
