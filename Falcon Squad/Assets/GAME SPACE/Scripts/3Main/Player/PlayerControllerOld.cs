﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using SkyGameKit;
using SkyGameKit.QuickAccess;
using UniRx;
using PathologicalGames;
using Firebase.Analytics;
using TCore;

public class PlayerControllerOld : MonoBehaviour
{

    public GameObject shieldObj;
    ParticleSystem shieldParticleSystem;

    Vector3 posRealoadInstance;
    Vector3 posStartTarget;
    Transform transform_Cache;
    //
    SpriteRenderer spritePlayer;
    CircleCollider2D collider2DIns;
    public GameObject fx_Duoi;

    TweenAlpha tweenAlphaDie;
    //
    PlayerShoot playerShootIns;
    //
    [HideInInspector]
    public WingmanController wingmanControllerIns;
    [HideInInspector]
    public SecondaryWeaponController secondaryWeaponControllerIns;


    void Start()
    {
        transform_Cache = transform;
        playerShootIns = GetComponent<PlayerShoot>();
        collider2DIns = GetComponent<CircleCollider2D>();
        spritePlayer = GetComponent<SpriteRenderer>();
        tweenAlphaDie = GetComponent<TweenAlpha>();

        shieldParticleSystem = shieldObj.GetComponent<ParticleSystem>();

        //MainScene.Instance.playerControllerScript = this;


        //
        //listWingman = playerMoveScript.wingmanControllerScript.listWingman1
        //

        PlayerStart();

        //		Debug.LogError ("playerTag" + gameObject.tag);
    }



    void PlayerStart()
    {
        posStartTarget = new Vector3(MainScene.Instance.posAnchorBottom.localPosition.x, MainScene.Instance.posAnchorBottom.localPosition.y + 1.8f, 0f);
        posRealoadInstance = new Vector3(MainScene.Instance.posAnchorBottom.localPosition.x, MainScene.Instance.posAnchorBottom.localPosition.y - 1.8f, 0f);

        transform_Cache.localPosition = posRealoadInstance;

        MainScene.Instance.playerStopMove = true;
        MainScene.Instance.playerReloading = true;
        playerShootIns.StopAllGun();

        this.Delay(0.5f, () =>
        {
            SoundManager.SoundPlayerShow();
        });
        transform.DOMove(posStartTarget, 1.38f).OnComplete(() =>
        {

            this.Delay(0.5f, () =>
            {
                if (gameObject.activeInHierarchy && ChangeBackground.Instance != null)
                {
                    ChangeBackground.Instance.SetSpeedMoveBackGround(ChangeBackground.Instance.speedBgReady, ChangeBackground.Instance.speedDecorReady);
                }
            });

            this.Delay(1.5f, () =>
            {

                if (gameObject.activeInHierarchy)
                {
                    playerShootIns.SetDataWeaponSystem();

                    playerShootIns.SetBulletGun();

                    //playerShootIns.SetMissleGun(100f);
                    //playerShootIns.SetLazerGun(100f);
                    //playerShootIns.SetWindSlash(100f);


                    MainScene.Instance.playerReloading = false;
                    MainScene.Instance.playerStopMove = false;
                    Pool_EffectStartLevel();


                    wingmanControllerIns.WingmanStartShoot();
                    secondaryWeaponControllerIns.SecondaryWeaponStartShoot();





                }
            });

            this.Delay(3f, () =>
            {
                if (gameObject.activeInHierarchy)
                {

                    LevelManager.Instance.NextWave();
                    //					playerShootIns.SetDataWeaponSystem ();
                    //					playerShootIns.SetBulletGun ();
                }
            });

        });
    }


    //-----------------------------PlayerStart------------------------------

    public void PlayerWin()
    {
        this.Delay(1f, () =>
        {
            playerShootIns.StopAllGun();
        });

        this.Delay(1.6f, () =>
        {
            Pool_EffectVictory();
            SoundManager.PlayGameWinText();
        });

        this.Delay(3.6f, () =>
        {
            MainScene.Instance.playerStopMove = true;
            MainScene.Instance.playerReloading = true;
            playerShootIns.StopAllGun();
            //WingMan
            wingmanControllerIns.WingmanStopShoot();

            secondaryWeaponControllerIns.SecondaryWeaponStopShoot();


            Vector3 targetPlayerWin = new Vector3(transform.localPosition.x, transform.localPosition.y + 16.8f, 0f);
            transform.DOMove(targetPlayerWin, 1.6f).OnComplete(() =>
            {

            });
        });
    }


    //-----------------------------PlayerDie------------------------------
    public void PlayerDie()
    {
        //PlayerMove.Instance.isPlayerImmortal = false;

        //if (!PlayerMove.Instance.isPlayerImmortal)
        //{

        if (MainScene.Instance.useRevival)
        {
            SoundManager.SoundPlayerShow();
            MainScene.Instance.useRevival = false;
            MainScene.Instance.playerStopMove = false;
            if (LevelManager.Instance.GameState != GameStateType.Playing)
            {
                LevelManager.Instance.OnGameStateChange(GameStateType.Playing);
            }

            //WingMan


            wingmanControllerIns.WingmanStartShoot();


            secondaryWeaponControllerIns.SecondaryWeaponStartShoot();
        }
        else
        {

            SoundManager.SoundPlayerDie();
            //

            WaveManager waveManagerIns = LevelManager.Instance.GetCurrentWave() as WaveManager;

            FirebaseLogSpaceWar.LogUserDead(waveManagerIns.name);

            PanelUITop.Instance.SetTextLife(-1);
            //				Debug.LogError ("curLifePlayer " + PanelUITop.Instance.curLife);
        }


        if (PanelUITop.Instance.curLife > 0)
        {

            Pool_EffectPlayerDieOneLife();
            //
            tweenAlphaDie.enabled = true;
            tweenAlphaDie.ResetToBeginning();
            //
            collider2DIns.enabled = true;
            spritePlayer.enabled = true;
            fx_Duoi.SetActive(true);
            //
            playerShootIns.SetBulletGun();
            //

            MainScene.Instance.playerReloading = true;
            MainScene.Instance.completeWithoutDying = false;
            //
            ShakeCamera.Instance.DoShake();

            Time.timeScale = 1f;

            this.Delay(0.5f, () =>
            {
                shieldObj.SetActive(true);
            });


            this.Delay(2.6f, () =>
            {
                MainScene.Instance.playerReloading = false;
                    //
                    tweenAlphaDie.enabled = false;
                spritePlayer.color = Color.white;
                    //
                    shieldObj.SetActive(false);
                    //

                });

        }
        else
        {
            if (LevelManager.Instance.GameState == GameStateType.Playing)
            {
                LevelManager.Instance.OnGameStateChange(GameStateType.GameOver);
            }
            playerShootIns.StopAllGun();
            collider2DIns.enabled = false;
            spritePlayer.enabled = false;
            fx_Duoi.SetActive(false);
            MainScene.Instance.playerReloading = true;
            MainScene.Instance.playerStopMove = true;
            Time.timeScale = 0.5f;
            //Debug.LogError("Change timeScale = " + Time.timeScale);
            Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");

            ShakeCamera.Instance.DoShake();
            Pool_EffectPlayerDieAllLife();

            //WingMan


            wingmanControllerIns.WingmanStopShoot();

            //SecondaryWeapon

            secondaryWeaponControllerIns.SecondaryWeaponStopShoot();

            //}
        }

    }



    public void Pool_EffectPlayerDieOneLife()
    {
        if (EffectList.Instance.playerDie != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    public void Pool_EffectPlayerDieAllLife()
    {
        if (EffectList.Instance.playerDieAllLife != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerDieAllLife, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    //

    public void Pool_EffectStartLevel()
    {
        if (EffectList.Instance.fxStartLevel != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.fxStartLevel, posFxVic, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
    //
    Vector3 posFxVic = new Vector3(0, 2f, 0);

    public void Pool_EffectVictory()
    {
        if (EffectList.Instance.fxVictory != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.fxVictory, posFxVic, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
