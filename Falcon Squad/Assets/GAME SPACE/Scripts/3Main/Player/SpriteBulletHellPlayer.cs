﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;

public class SpriteBulletHellPlayer : Singleton<SpriteBulletHellPlayer>
{
    public List<Sprite> listSpriteBulletUse;
    public List<Sprite> listSpriteBulletWingmanUse;
    //
    [Title("Plane1 BATA FD", bold: true, horizontalLine: true)]
    public List<Sprite> listSpriteBulletPlane1;

    [Title("Plane2 Sky Wraith", bold: true, horizontalLine: true)]
    public List<Sprite> listSpriteBulletPlane2;

    [Title("Plane3 Fury of Ares", bold: true, horizontalLine: true)]
    public List<Sprite> listSpriteBulletPlane3;

    [Title("Plane4  Noel 1", bold: true, horizontalLine: true)]
    public List<Sprite> listSpriteBulletPlane4Noel;

    [Title("Plane5 Noel 2", bold: true, horizontalLine: true)]
    public List<Sprite> listSpriteBulletPlane5Noel;

    [Title("Plane 6 MacBird", bold: true, horizontalLine: true)]
    public List<Sprite> listSpriteBulletPlane6;

    [Title("Plane7 Twilight X ", bold: true, horizontalLine: true)]
    public List<Sprite> listSpriteBulletPlane7;


    ///
    [Title("1Galting", bold: true, horizontalLine: true)]
    public List<Sprite> listSpriteBulletWingman1;

    [Title("2AutoGalting", bold: true, horizontalLine: true)]
    public List<Sprite> listSpriteBulletWingman2;

    [Title("4DouleGalting", bold: true, horizontalLine: true)]
    public List<Sprite> listSpriteBulletWingman4;


    void Awake()
    {
        base.Awake();
        GetDataChangePlane();
        //GetDataChangeWingman();
    }

    //	void Start ()
    //	{
    //
    //	}

    public void GetDataChangePlane()
    {
        int playerSwitch = 1;


        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane && GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Aircraft)
        {
            
            playerSwitch = CacheGame.GetSpaceShipUserTry();
        }
        else
        {
            playerSwitch = CacheGame.GetSpaceShipUserSelected();
        }
        switch (playerSwitch)
        {
            case 1:
                listSpriteBulletUse = listSpriteBulletPlane1;
                break;
            case 2:
                listSpriteBulletUse = listSpriteBulletPlane2;
                break;
            case 3:
                listSpriteBulletUse = listSpriteBulletPlane3;
                break;
            case 4:
                listSpriteBulletUse = listSpriteBulletPlane4Noel;
                break;
            case 5:
                listSpriteBulletUse = listSpriteBulletPlane5Noel;
                break;
            case 6:
                listSpriteBulletUse = listSpriteBulletPlane6;
                break;
            case 7:
                listSpriteBulletUse = listSpriteBulletPlane7;
                break;
            default:
                break;
        }
    }


    //

    //public List GetDataChangeWingman(int indexWingman)
    //{

    //    //int wingmanSwitch = 1;
    //    //if (SceneManager.GetActiveScene().name == "LevelTryPlane" && GameContex.typeOpenPopupFromTry == GameContex.TypeOpenPopupFromTry.Wingman)
    //    //{
    //    //    wingmanSwitch = CacheGame.GetWingManUserTry();
    //    //}
    //    //else
    //    //{
    //    //    wingmanSwitch = CacheGame.GetWingManLeftUserSelected();
    //    //}
    //    switch (indexWingman)
    //    {
    //        case 1:
    //            listSpriteBulletWingmanUse = listSpriteBulletWingman1;
    //            break;
    //        case 2:
    //            listSpriteBulletWingmanUse = listSpriteBulletWingman2;
    //            break;
    //        default:
    //            break;
    //    }
    //}
}
