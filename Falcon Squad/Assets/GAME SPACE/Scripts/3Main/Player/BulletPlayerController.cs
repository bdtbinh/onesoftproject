﻿using UnityEngine.UI;
using UnityEngine;
using UniRx;
using SkyGameKit;
using Sirenix.OdinInspector;
using System.Collections;
using PathologicalGames;
using System.Collections.Generic;
using System;


public class BulletPlayerController : MonoBehaviour
{
    public enum TypeBullet
    {
        MainWeapon,
        MissileHoming,
        Laser,
        WindSlash,
        WingMan1Galting,
        WingMan2AutoGalting,
        SecondaryWeaponBurstShot,
        SecondaryWeaponChainsaw,
        Wingman4DouleGalting,
        WingMan5HomingMissile,
        SecondaryWeaponSuperSonic,
        MainWeaponScale
    }


    public TypeBullet typeBullet;
    public int power;
    //

    public SpriteRenderer spriteBulletIns;


    UbhBullet ubhBulletIns;
    TweenScale scaleWindSlash;

    void Awake()
    {

        switch (typeBullet)
        {
            case TypeBullet.MainWeapon:
                ubhBulletIns = transform.parent.GetComponent<UbhBullet>();
                //			GetDataChangePlane ();
                break;
            case TypeBullet.MainWeaponScale:
                ubhBulletIns = transform.parent.GetComponent<UbhBullet>();
                if (scaleWindSlash == null)
                {
                    scaleWindSlash = GetComponent<TweenScale>();
                }
                break;
            case TypeBullet.MissileHoming:
                ubhBulletIns = GetComponent<UbhBullet>();
                break;
            case TypeBullet.WindSlash:
                ubhBulletIns = transform.parent.GetComponent<UbhBullet>();
                if (scaleWindSlash == null)
                {
                    scaleWindSlash = GetComponent<TweenScale>();
                }
                break;
            case TypeBullet.WingMan1Galting:
                ubhBulletIns = transform.parent.GetComponent<UbhBullet>();
                break;
            case TypeBullet.WingMan2AutoGalting:
                ubhBulletIns = transform.parent.GetComponent<UbhBullet>();
                break;
            case TypeBullet.Wingman4DouleGalting:
                ubhBulletIns = transform.parent.GetComponent<UbhBullet>();
                break;
            case TypeBullet.WingMan5HomingMissile:
                ubhBulletIns = GetComponent<UbhBullet>();
                break;
            case TypeBullet.SecondaryWeaponBurstShot:
                ubhBulletIns = transform.parent.GetComponent<UbhBullet>();
                break;
            case TypeBullet.SecondaryWeaponChainsaw:
                ubhBulletIns = transform.parent.GetComponent<UbhBullet>();
                break;
            case TypeBullet.SecondaryWeaponSuperSonic:
                ubhBulletIns = transform.parent.GetComponent<UbhBullet>();
                if (scaleWindSlash == null)
                {
                    scaleWindSlash = GetComponent<TweenScale>();
                }
                break;
        }
    }


    //Boss4SpiderSilk

    void OnTriggerEnter2D(Collider2D col)
    {
        //		Debug.LogError ("typeZone" + typeZone);
        if (col.CompareTag("Boss4SpiderSilk"))
        {
            Despawn();
        }
    }




    void OnEnable()
    {

        SetValueStart();

    }




    void SetValueStart()
    {
        Debug.LogError(gameObject.name);
        switch (typeBullet)
        {
            case TypeBullet.MainWeapon:
                power = GetDataShopToMain.Instance.mainWeaponPower;
                //Debug.LogError("power1:" + power);
                //Debug.LogError("power2:" + power);
                SetSpriteMainWeaponPlane(SpriteBulletHellPlayer.Instance.listSpriteBulletUse, 4);
                break;
            case TypeBullet.MainWeaponScale:
                power = GetDataShopToMain.Instance.mainWeaponPower;
                SetSpriteMainWeaponPlane(SpriteBulletHellPlayer.Instance.listSpriteBulletUse, 4);
                scaleWindSlash.enabled = true;
                scaleWindSlash.ResetToBeginning();
                break;
            case TypeBullet.MissileHoming:
                power = GetDataShopToMain.Instance.missilePower;
                break;
            case TypeBullet.WindSlash:
                power = GetDataShopToMain.Instance.windSlashPower;
                scaleWindSlash.enabled = true;
                scaleWindSlash.ResetToBeginning();
                break;
            case TypeBullet.WingMan1Galting:
                power = GetDataShopToMain.Instance.powerWingman1Galting;
                SetSpriteMainWeaponPlane(SpriteBulletHellPlayer.Instance.listSpriteBulletWingman1, 5);
                break;
            case TypeBullet.WingMan2AutoGalting:
                power = GetDataShopToMain.Instance.powerWingman2AutoGalting;
                SetSpriteMainWeaponPlane(SpriteBulletHellPlayer.Instance.listSpriteBulletWingman2, 5);
                break;
            case TypeBullet.Wingman4DouleGalting:
                power = GetDataShopToMain.Instance.powerWingman4DouleGalting;
                SetSpriteMainWeaponPlane(SpriteBulletHellPlayer.Instance.listSpriteBulletWingman4, 5);
                break;
            case TypeBullet.WingMan5HomingMissile:
                power = GetDataShopToMain.Instance.powerWingman5HomingMissile;
                break;
            case TypeBullet.SecondaryWeaponBurstShot:
                power = GetDataShopToMain.Instance.secondaryWeaponPower;
                break;
            case TypeBullet.SecondaryWeaponChainsaw:
                power = GetDataShopToMain.Instance.secondaryWeaponPower;
                break;
            case TypeBullet.SecondaryWeaponSuperSonic:
                power = GetDataShopToMain.Instance.secondaryWeaponPower;
                scaleWindSlash.enabled = true;
                scaleWindSlash.ResetToBeginning();
                break;
        }
    }


    void SetSpriteMainWeaponPlane(List<Sprite> listSpriteBulletHellUse, int distanceChange)
    {
        //listSpriteBulletHellUse = SpriteBulletHellPlayer.Instance.listSpriteBulletUse;
        if (listSpriteBulletHellUse.Count > 0)
        {
            int levelUpgrade = CacheGame.GetCurrentLvShopUpgrade(GameContext.MAIN_WEAPON, GameContext.POWER) + GameContext.numItemPowerGet + GetDataShopToMain.Instance.totalPowerBonus;

            int indexGetSprite = (int)(levelUpgrade / distanceChange);

            //Debug.LogError("levelUpgrade: " + levelUpgrade + "indexGetSprite " + indexGetSprite + "  listSpriteBulletHellUse.Count  " + listSpriteBulletHellUse.Count);
            if (indexGetSprite < listSpriteBulletHellUse.Count)
            {
            }
            else
            {
                indexGetSprite = listSpriteBulletHellUse.Count - 1;
                Debug.Log("indexGetValue get Sprite Bullet quá count ");
            }


            if (listSpriteBulletHellUse[indexGetSprite] != null)
            {
                //sprMainWeaponIns.sprite = listSpriteBulletHellUse[indexGetSprite];
            }
        }


    }





    public void Despawn()
    {

        switch (typeBullet)
        {
            case TypeBullet.MainWeapon:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
            case TypeBullet.MainWeaponScale:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
            case TypeBullet.MissileHoming:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
            case TypeBullet.WindSlash:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
            case TypeBullet.WingMan1Galting:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
            case TypeBullet.WingMan2AutoGalting:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
            case TypeBullet.Wingman4DouleGalting:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
            case TypeBullet.WingMan5HomingMissile:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
            case TypeBullet.SecondaryWeaponBurstShot:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
            case TypeBullet.SecondaryWeaponChainsaw:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
            case TypeBullet.SecondaryWeaponSuperSonic:
                if (ubhBulletIns != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                }
                break;
        }
    }




}
