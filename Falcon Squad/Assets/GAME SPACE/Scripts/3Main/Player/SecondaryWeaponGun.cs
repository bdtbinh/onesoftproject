﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SecondaryWeaponGun : MonoBehaviour {



    void OnDisable()
    {
        //StopAllCoroutines();
    }




    public virtual void SecondaryWeaponStopShoot()
    {


    }


    public virtual void SecondaryWeaponStartShoot()
    {


    }
}
