﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayerValue : MonoBehaviour
{

    //2 biến này dành cho trường hợp player bắn ra viện đạn sau đó từ viên đạn lại tiếp tục tỏa ra các viên đạn con nữa
    private PlayerInit playerInitIns;
    public PlayerInit PlayerInitIns { get { return playerInitIns; } }

    private TypeGunPlayerShotBulletHell typeGunPlayerShotBulletHellInBullet;
    public TypeGunPlayerShotBulletHell TypeGunPlayerShotBulletHellInBullet { get { return typeGunPlayerShotBulletHellInBullet; } }

    public int power;
    //
    string InfoBoxMessage = "Viên đạn nào ko thay đổi hình ảnh thì để null trường này";
    [InfoBox("$InfoBoxMessage")]
    public SpriteRenderer spriteBulletIns;

    UbhBullet ubhBulletIns;

    void Awake()
    {
        ubhBulletIns = GetComponent<UbhBullet>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Boss4SpiderSilk") || col.CompareTag("BulletBoss7_Stone") || col.CompareTag("Boss10VeTinh"))
        {
            Despawn();
        }
    }

    //void OnEnable()
    //{

    //}

    Color c = new Color(1, 1, 1, 0.5f);

    public void SetValueStart(Sprite spriteBullet, int powerBullet, PlayerInit playerInitScript, TypeGunPlayerShotBulletHell typeGunPlayerShotBulletHell)
    {
        power = powerBullet;
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2 && spriteBulletIns != null)
        {
            if (power == 0)
            {
                spriteBulletIns.color = c;
            }
            else
            {
                spriteBulletIns.color = Color.white;
            }
        }
        //if (powerBullet != 0)
        //{
        //}
        if (spriteBulletIns != null && spriteBullet != null)
        {
            spriteBulletIns.sprite = spriteBullet;
        }
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2 && spriteBulletIns == null)
        {
            TrailRenderer tr = GetComponent<TrailRenderer>();
            if (tr != null)
            {
                if (power == 0)
                {
                    tr.sharedMaterial = AllPlayerManager.Instance.listMaterial[1];
                }
                else
                {
                    tr.sharedMaterial = AllPlayerManager.Instance.listMaterial[0];
                }
                if (transform.childCount > 0)
                {
                    SpriteRenderer sr = transform.GetChild(0).GetComponent<SpriteRenderer>();
                    if (sr != null)
                    {
                        if (power == 0)
                        {
                            sr.color = c;
                        }
                        else
                        {
                            sr.color = Color.white;
                        }
                    }
                }
            }
        }

        playerInitIns = playerInitScript;
        typeGunPlayerShotBulletHellInBullet = typeGunPlayerShotBulletHell;
    }

    public void Despawn()
    {

        if (ubhBulletIns != null)
        {
            UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
        }
    }
}
