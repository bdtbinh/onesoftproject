﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwoDLaserPack;
using SkyGameKit;

public class PlayerShoot : MonoBehaviour
{
    public enum TypeBulletGun
    {
        MainWeapon,
        Lazer,
        HomingMissle,
        WindSlash

    }

    //	public WeaponSystem mainWeaponGunScript;
    public UbhShotCtrl mainWeaponUbh;
    SpaceWarNwayShot ubhNwayShotMainWeapon;
    //
    public MinhLaser laserHitScript;
    //private GameObject laserObj;
    //public MinhLaser
    bool setHomingMissle;
    bool setHitLaser;

    public UbhShotCtrl[] listGunMissile;
    public UbhShotCtrl windSlashUbh;



    void Start()
    {
        ubhNwayShotMainWeapon = mainWeaponUbh.transform.GetChild(0).GetComponent<SpaceWarNwayShot>();
    }



    void OnDisable()
    {
        StopAllCoroutines();
    }

    public void SetBulletGun(TypeBulletGun type = TypeBulletGun.MainWeapon)
    {
        if (type == TypeBulletGun.MainWeapon)
        {
            mainWeaponUbh.m_shotList[0].m_afterDelay = 0.21f;
            mainWeaponUbh.StartShotRoutine();
            //
            windSlashUbh.StopShotRoutine();
            //
            laserHitScript.gameObject.SetActive(false);
            //
            foreach (var gunMissle in listGunMissile)
            {
                gunMissle.StopShotRoutine();
            }
            setHomingMissle = false;

        }
        else if (type == TypeBulletGun.Lazer)
        {

            mainWeaponUbh.StopShotRoutine();
            //			mainWeaponUbh.StartShotRoutine ();
            //
            windSlashUbh.StopShotRoutine();
            //
            laserHitScript.gameObject.SetActive(true);
            //
            foreach (var gunMissle in listGunMissile)
            {
                gunMissle.StopShotRoutine();
            }
            setHomingMissle = false;

        }
        else if (type == TypeBulletGun.HomingMissle)
        {

            mainWeaponUbh.StartShotRoutine();
            //
            windSlashUbh.StopShotRoutine();
            //
            laserHitScript.gameObject.SetActive(false);

            foreach (var gunMissle in listGunMissile)
            {
                gunMissle.StartShotRoutine();
            }
            setHomingMissle = true;
        }
        else if (type == TypeBulletGun.WindSlash)
        {

            mainWeaponUbh.StopShotRoutine();
            //			mainWeaponUbh.StartShotRoutine ();
            //
            windSlashUbh.StartShotRoutine();
            //
            laserHitScript.gameObject.SetActive(false);
            //
            foreach (var gunMissle in listGunMissile)
            {
                gunMissle.StopShotRoutine();
            }
            setHomingMissle = false;
            //
        }

        //laserHitScript.gameObject.SetActive(true);
    }


    public void StopAllGun()
    {
        mainWeaponUbh.StopShotRoutine();
        //
        windSlashUbh.StopShotRoutine();
        //
        laserHitScript.gameObject.SetActive(false);
        //
        foreach (var gunMissle in listGunMissile)
        {
            gunMissle.StopShotRoutine();
        }
        //
        setHomingMissle = false;
        //
        StopAllCoroutines();
    }


    //----------------------------------------- Lazer ---------------------------------------
    //--------------------- Tao su kien de bat va cham enemy va lazer -----------------------
    //	int laserPower;

    public void SetLazerGun(float timeShow)
    {

        //		Debug.LogError ("SetLazerGun 1");
        if (!setHitLaser)
        {
            //			laserPower = GetDataShopToMain.Instance.laserPower;
            laserHitScript.OnLaserDamageTarget += LaserOnOnLaserHitTriggered;

            //			Debug.LogError ("setHitLaser 2 += " + setHitLaser);
            setHitLaser = true;

        }
        SetBulletGun(TypeBulletGun.Lazer);
        //
        //		StopCoroutine ("HideMissle");
        //		StopCoroutine ("HideLaser");
        StopAllCoroutines();
        StartCoroutine("HideLaser", timeShow);

    }

    //

    IEnumerator HideLaser(float timeShow)
    {
        //		Debug.LogError ("HideLaser ");
        yield return new WaitForSeconds(timeShow);
        //		laserHitScript.SetLaserState (false);
        SetBulletGun(TypeBulletGun.MainWeapon);
    }


    //


    private void LaserOnOnLaserHitTriggered(RaycastHit2D hitInfo)
    {
        //		Debug.LogError ("LaserOnOnLaserHitTriggered " + GetDataShopToMain.Instance.laserPower);
        //if (hitInfo.collider.tag == "Enemy")
        //{
        //    hitInfo.collider.GetComponent<EnemyCollision>().LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.laserPower);
        //}
        //else if (hitInfo.collider.tag == "EnemyNhenChild")
        //{
        //    hitInfo.collider.GetComponent<Enemy_NhenChild>().LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.laserPower);
        //}
        //else if (hitInfo.collider.tag == "Boss1")
        //{
        //    //			Debug.LogError ("LaserOnOnLaserHitTriggered Boss1"); 
        //    hitInfo.collider.GetComponent<Boss1Collision>().LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.laserPower);
        //}
        //else if (hitInfo.collider.tag == "Boss2")
        //{
        //    hitInfo.collider.GetComponent<Boss2Collision>().LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.laserPower);
        //}
        //else if (hitInfo.collider.tag == "Boss3")
        //{
        //    hitInfo.collider.GetComponent<Boss3Collision>().LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.laserPower);
        //}
        //else if (hitInfo.collider.tag == "Boss3_Shield")
        //{
        //    hitInfo.collider.GetComponent<Boss3ShieldController>().LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.laserPower);
        //}
        //else if (hitInfo.collider.tag == "Boss3_Shield_Core")
        //{
        //    hitInfo.collider.GetComponent<Boss3ShieldCore>().LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.laserPower);
        //}
        //else if (hitInfo.collider.tag == "Boss4")
        //{
        //    hitInfo.collider.GetComponent<Boss4Collision>().LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.laserPower);
        //}
        //else if (hitInfo.collider.tag == "Boss5")
        //{
        //    hitInfo.collider.GetComponent<Boss5Collision>().LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.laserPower);
        //}


        if (LayerMask.LayerToName(hitInfo.collider.gameObject.layer) == "Enemy")
        {

            //Debug.LogError("hit enemy: " + hitInfo.collider.gameObject.name);
            if (hitInfo.collider.GetComponent<EnemyCollisionManager>() != null)
            {
                //Debug.LogError("hit enemy2: " + hitInfo.collider.gameObject.name);
                if (hitInfo.collider.GetComponent<EnemyCollisionManager>().enemyCollisionBase != null)
                {
                    //Debug.LogError("hit enemy3: " + hitInfo.collider.gameObject.name + "power:" + GetDataShopToMain.Instance.laserPower);
                    hitInfo.collider.GetComponent<EnemyCollisionManager>().enemyCollisionBase.LaserOnOnLaserHitTriggeredEnemy(GetDataShopToMain.Instance.laserPower);
                }
            }
        }
    }
    //

    //----------------------------------------- HomingMissle ---------------------------------------
    //--------------------- Tao su kien de bat va cham enemy va lazer -----------------------


    public void SetMissleGun(float timeShow)
    {


        //		Debug.LogError ("SetMissleGun 1");

        if (!setHomingMissle)
        {
            SetBulletGun(TypeBulletGun.HomingMissle);
        }

        //
        //		StopCoroutine ("HideLaser");
        //		StopCoroutine ("HideMissle");
        StopAllCoroutines();
        StartCoroutine("HideMissle", timeShow);


    }

    IEnumerator HideMissle(float timeShow)
    {
        //		Debug.LogError ("HideMissle ");
        yield return new WaitForSeconds(timeShow);
        //		Debug.LogError ("HideMissle 2");
        SetBulletGun(TypeBulletGun.MainWeapon);
    }




    //----------------------------------------- Item Wind Slash ---------------------------------------
    //--------------------- Ăn được đạn Wind Slash  -----------------------

    public void SetWindSlash(float timeShow)
    {

        SetBulletGun(TypeBulletGun.WindSlash);

        StopAllCoroutines();
        StartCoroutine("HideMissle", timeShow);


    }

    //

    IEnumerator HideWindSlash(float timeRunSpeed)
    {
        yield return new WaitForSeconds(timeRunSpeed);
        SetBulletGun(TypeBulletGun.MainWeapon);
    }

    //----------------------------------------- Item Change Speed Shot ---------------------------------------
    //--------------------- zzzzzzzzzzzzzzz-----------------------

    public void SetChangeSpeedShot(float timeRunSpeed)
    {

        mainWeaponUbh.m_shotList[0].m_afterDelay = 0.1f;
        //		ubhNwayShotMainWeapon.m_betweenAngle
        StopCoroutine("RestoreSpeedShot");
        StartCoroutine("RestoreSpeedShot", timeRunSpeed);

    }

    //

    IEnumerator RestoreSpeedShot(float timeRunSpeed)
    {
        yield return new WaitForSeconds(timeRunSpeed);
        mainWeaponUbh.m_shotList[0].m_afterDelay = 0.21f;
    }
    //----------------------------------------- Main Weapon ---------------------------------------
    //--------------------- get data tu` shop de cho vao` bullet -----------------------

    public void SetDataWeaponSystem()
    {
        //		mainWeaponGunScript.bulletCount = GetDataShopToMain.Instance.mainWeaponQuantity;
        //		mainWeaponGunScript.bulletSpread = GetDataShopToMain.Instance.mainWeaponQuantity * 0.1f;

        // set so tia dan
        //		Debug.LogError ("mainWeaponQuantity " + GetDataShopToMain.Instance.mainWeaponQuantity);

        ubhNwayShotMainWeapon.m_wayNum = GetDataShopToMain.Instance.mainWeaponQuantity;
        ubhNwayShotMainWeapon.m_bulletNum = GetDataShopToMain.Instance.mainWeaponQuantity;
        //set speed
        ubhNwayShotMainWeapon.m_bulletSpeed = GetDataShopToMain.Instance.mainWeaponSpeed;
    }


}
