﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using DG.Tweening;
using DG.Tweening.Core;
using SkyGameKit;
using Chronos;
using TCore;

public class PlayerCollision : MonoBehaviour
{


    //	public GameObject effect_TriggerItem;
    //	public GameObject effect_TriggerCoin;
    //	public GameObject effect_PlayerDie;
    PlayerShoot playerShootIns;
    PlayerControllerOld playerControllerIns;

    //	int numItemPowerUpGet;

    void Start()
    {
        playerShootIns = GetComponent<PlayerShoot>();
        playerControllerIns = GetComponent<PlayerControllerOld>();

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Item")
        {

            ItemController itemControllerIsn = col.GetComponent<ItemController>();
            itemControllerIsn.Despawn();
            PlayerTriggerItem(itemControllerIsn.typeItem);
        }

        if (!MainScene.Instance.gameFinished && !MainScene.Instance.playerReloading && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing && !MainScene.Instance.bossDead)
        {
            switch (col.tag)
            {


                case "EnemyBullet":
                    Debug.Log("Player Trigger EnemyBullet");
                    Pool_EffectPlayerDie();
                    playerControllerIns.PlayerDie();
                    //
                    UbhBullet bulletDespawn = col.transform.parent.GetComponent<UbhBullet>();
                    if (bulletDespawn != null)
                    {
                        UbhObjectPool.instance.ReleaseBullet(bulletDespawn);
                    }
                    break;
                case "Enemy":
                    Debug.Log("Player Trigger Enemy");
                    playerControllerIns.PlayerDie();
                    break;
                case "Boss1":
                    Debug.Log("Player Trigger Boss1");
                    playerControllerIns.PlayerDie();
                    break;
                case "Boss2":
                    Debug.Log("Player Trigger Boss2");
                    playerControllerIns.PlayerDie();
                    break;
                case "Boss3":
                    Debug.Log("Player Trigger Boss3");
                    playerControllerIns.PlayerDie();
                    break;
                case "Boss4":
                    Debug.Log("Player Trigger Boss4");
                    playerControllerIns.PlayerDie();
                    break;
                case "Boss5":
                    Debug.Log("Player Trigger Boss5");
                    playerControllerIns.PlayerDie();
                    break;
                case "LazeNhen":
                    Debug.Log("Player Trigger LazeNhen");
                    playerControllerIns.PlayerDie();
                    break;
                case "Hazzard":
                    Debug.Log("Player Trigger Hazzard");
                    playerControllerIns.PlayerDie();
                    break;
                case "Boss2MissileExplosion":
                    Debug.Log("Player Trigger Boss2MissileExplosion");
                    playerControllerIns.PlayerDie();
                    break;
                case "Boss4SpiderSilk":
                    Debug.Log("Player Trigger Boss4SpiderSilk");
                    playerControllerIns.PlayerDie();
                    break;
                case "FXEnemy305_Bonangluong":
                    Debug.Log("Player Trigger FXEnemy305_Bonangluong");
                    playerControllerIns.PlayerDie();
                    break;
            }
        }
    }

    //--------------------- -----------------------
    public void PlayerTriggerItem(string typeItem)
    {

        switch (typeItem)
        {
            case "Gem":
                Pool_EffectTriggerGem();
                SoundManager.SoundGetCoinSmall();
                //CacheGame.SetTotalGem(CacheGame.GetTotalGem() + 1,FirebaseLogSpaceWar.DropIngame_Why, FirebaseLogSpaceWar.Unknown_why);
                break;
            case "CoinDiamond":
                Pool_EffectTriggerDiamond();
                PanelUITop.Instance.SetTextCoin(5);

                break;
            case "CoinGold":
                Pool_EffectTriggerCoin();
                PanelUITop.Instance.SetTextCoin(3);

                break;
            case "CoinSilver":
                Pool_EffectTriggerCoin();
                PanelUITop.Instance.SetTextCoin(2);

                break;
            case "CoinCopper":
                Pool_EffectTriggerCoin();
                PanelUITop.Instance.SetTextCoin(1);

                break;
            case "PowerUp":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerPowerUp();
                GameContext.numItemPowerGet++;
                //GetDataShopToMain.Instance.mainWeaponPower = (int)GetDataShopToMain.Instance.GetValueFromShop(GameContext.MAIN_WEAPON, GameContext.POWER, GetDataShopToMain.Instance.namePlaneUse, GameContext.numItemPowerGet, GetDataShopToMain.Instance.totalPowerBonus);
                //GetDataShopToMain.Instance.mainWeaponPower = (int)(GetDataShopToMain.Instance.mainWeaponPower * VipBonusValue.damePlaneRate);
                //
                //GetDataShopToMain.Instance.mainWeaponQuantity = (int)GetDataShopToMain.Instance.GetMainWeaponQuantity(GameContext.numItemPowerGet);
                playerShootIns.SetDataWeaponSystem();

                //
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);
                break;
            case "TimeSphere":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem();
                //PanelTimeSphere.Instance.GetItemTimeSphere();
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);
                break;
            case "HomingMissile":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem();
                playerShootIns.SetMissleGun(GetDataShopToMain.Instance.superWeaponDuration);
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);

                break;
            case "Lazer":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem();
                playerShootIns.SetLazerGun(GetDataShopToMain.Instance.superWeaponDuration);
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);

                break;
            case "Life":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem();
                PanelUITop.Instance.SetTextLife(1);
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);
                break;
            case "SpeedShot":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem();
                playerShootIns.SetChangeSpeedShot(6f);
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);
                break;
            case "WindSlash":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem();
                playerShootIns.SetWindSlash(GetDataShopToMain.Instance.superWeaponDuration);
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);
                break;




        }



        //--------------------- Effect-----------------------




    }

    public void Pool_EffectTriggerItem()
    {
        if (EffectList.Instance.playerTriggerItem != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerItem, transform.position, Quaternion.identity);
            effectIns.SetParent(transform);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    public void Pool_EffectTriggerGem()
    {
        if (EffectList.Instance.playerTriggerGem != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerGem, transform.position, Quaternion.identity);
            effectIns.SetParent(transform);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
    public void Pool_EffectTriggerCoin()
    {
        if (EffectList.Instance.playerTriggerCoin != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerCoin, transform.position, Quaternion.identity);
            effectIns.SetParent(transform);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
    

    public void Pool_EffectTriggerDiamond()
    {
        if (EffectList.Instance.playerTriggerDiamond != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerDiamond, transform.position, Quaternion.identity);
            effectIns.SetParent(transform);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    public void Pool_EffectTriggerPowerUp()
    {
        if (EffectList.Instance.playerTriggerPowerUp != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerPowerUp, transform.position, Quaternion.identity);
            effectIns.SetParent(transform);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    public void Pool_EffectPlayerDie()
    {
        if (EffectList.Instance.playerDie != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerDie, transform.position, Quaternion.identity);
            //			effectIns.SetParent (transform);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }


    // noel 

    public void Pool_EffectTriggerSock()
    {
        if (EffectList.Instance.playerTriggerSock != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerSock, transform.position, Quaternion.identity);
            effectIns.SetParent(transform);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }








}
