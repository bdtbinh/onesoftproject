
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class CoinBonusWaveEndless : MonoBehaviour
{
    public UILabel lnumWave;
    public UILabel lcoinBonusWave;

    int numcoinBonusWave;
    void Start()
    {

        //GameContex.modeGamePlay = GameContex.ModeGamePlay.EndLess;
        //Debug.LogError(GameContex.modeGamePlay);
        Debug.Log("<color=#ff33cc>" + "modeGamePlay: " + GameContext.modeGamePlay + "</color>");

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
        {
            LevelManager.OnNextWave += OnNextWave;
        }
        else
        {
            gameObject.SetActive(false);
        }

    }

    private void OnDisable()
    {

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
        {
            LevelManager.OnNextWave -= OnNextWave;
        }
    }

    int numWave;
    void OnNextWave(int numWaveOrigi)
    {
        numWave = numWaveOrigi + 1;

        //Debug.LogError("numWave " + numWave);

        if (numWave > 0)
        {
            if (numWave >= NextWaveCoinEnlessSheet.GetDictionary().Count)
            {
                numWave = NextWaveCoinEnlessSheet.GetDictionary().Count - 1;
            }
            numcoinBonusWave = NextWaveCoinEnlessSheet.Get(numWave).NumResourcesGet;

            lnumWave.gameObject.SetActive(true);
            lcoinBonusWave.gameObject.SetActive(true);

            lnumWave.text = "wave " + numWave;
            lcoinBonusWave.text = "+ " + numcoinBonusWave;
            PanelUITop.Instance.numCoinBonusEndless += numcoinBonusWave;
            PanelUITop.Instance.SetTextCoin(numcoinBonusWave);
            PanelUITop.Instance.numWaveEndlessPassed = numWave;

            this.Delay(1f, () =>
            {
                //lnumWave.gameObject.SetActive(false);
                lcoinBonusWave.gameObject.SetActive(false);
            });
        }
        else
        {
            lnumWave.gameObject.SetActive(false);
            lcoinBonusWave.gameObject.SetActive(false);
        }

    }
}
