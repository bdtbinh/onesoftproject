﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;

public class EffectTimeSphere : MonoBehaviour
{

	GlobalClock clock;

	ParticleSystem particleSystemIns;


	void Start ()
	{
		clock = Timekeeper.instance.Clock ("Player");
		particleSystemIns = GetComponent<ParticleSystem> ();
	}

	void Update ()
	{

		if (!MainScene.Instance.gameStopping) {
			if (particleSystemIns.main.loop == false) {
				if (clock.timeScale < 0.05f) {
					particleSystemIns.Simulate (Time.unscaledDeltaTime, true, false);
				}
			} else if (particleSystemIns.main.loop == true) {
				if (clock.timeScale < 0.05f) {
					particleSystemIns.Simulate (Time.unscaledDeltaTime, true, false);
				}
				if (clock.timeScale > 0.05f) {
					particleSystemIns.Simulate (Time.unscaledDeltaTime, true, false);
				}
			}
		}

	}
}
