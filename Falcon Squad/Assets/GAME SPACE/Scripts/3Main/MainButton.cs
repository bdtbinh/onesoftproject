﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
//using BigFox;
using TCore;
using System;

public class MainButton : MonoBehaviour
{
    public tk2dButton pauseButton;

    private void OnEnable()
    {
        CachePvp.canPause = false;
        MessageDispatcher.AddListener(EventName.PVP.StartGamePVP.ToString(), OnStartGamePVP, true);
        MessageDispatcher.AddListener(EventName.PVP.StartGamePVP2v2.ToString(), OnStartGamePVP, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.PVP.StartGamePVP.ToString(), OnStartGamePVP, true);
        MessageDispatcher.RemoveListener(EventName.PVP.StartGamePVP2v2.ToString(), OnStartGamePVP, true);
    }

    private void OnStartGamePVP(IMessage rMessage)
    {
        CachePvp.canPause = true;
    }

    void Start()
    {
        pauseButton.ButtonDownEvent += PauseButtonClickTK2D;
    }

    public void PauseButtonClickTK2D(tk2dButton button_)
    {
        //if (GameContex.modeGamePlay == GameContex.ModeGamePlay.pvp)
        //{
        //    SoundManager.PlayShowPopup();
        //    MainScene.Instance.popupPause.SetActive(true);
        //    MainScene.Instance.popupPause.GetComponent<PopupSetting>().Change_When_OpenPopup();

        //    MainScene.Instance.gameStopping = true;
        //    Time.timeScale = 0;
        //    Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
        //}
        //else
        //{
        //    if (!MainScene.Instance.gameFinished && !MainScene.Instance.playerStopMove)
        //    {
        //        SoundManager.PlayShowPopup();
        //        MainScene.Instance.popupPause.SetActive(true);
        //        MainScene.Instance.popupPause.GetComponent<PopupSetting>().Change_When_OpenPopup();

        //        MainScene.Instance.gameStopping = true;
        //        Time.timeScale = 0;
        //        Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
        //    }
        //}
    }

    public void PauseButtonClick()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            if (!MainScene.Instance.activeSkillIsPlaying)
            {
                if (CachePvp.canPause)
                {
                    SoundManager.PlayShowPopup();
                    MainScene.Instance.popupPause.SetActive(true);
                    MainScene.Instance.popupPause.GetComponent<PopupSetting>().Change_When_OpenPopup();
                }
            }
        }
        else
        {
            if (!MainScene.Instance.gameFinished && !AllPlayerManager.Instance.playerCampaign.Aircraft.aircraftIsStopMove && !MainScene.Instance.activeSkillIsPlaying)
            {
                SoundManager.PlayShowPopup();
                MainScene.Instance.popupPause.SetActive(true);
                MainScene.Instance.popupPause.GetComponent<PopupSetting>().Change_When_OpenPopup();

                MainScene.Instance.gameStopping = true;
                Time.timeScale = 0;
                Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
            }
        }
    }


    public void ButtonInitPlayerTest()
    {

        AllPlayerManager.Instance.playerCampaign.InitAircraftBackup(AircraftTypeEnum.TwilightX);
    }
}
