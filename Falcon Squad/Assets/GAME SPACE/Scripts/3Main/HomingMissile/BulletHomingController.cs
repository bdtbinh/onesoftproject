﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BulletHomingController : MonoBehaviour
{
	//	public enum TypeMissile
	//	{
	//		BezierMissile,
	//		StandardMissile
	//	}

	//	public TypeMissile typeMissile;
	public TrailRenderer trailRender;
	private float speedMovement;
	[Header ("Duration For Bezier")]
	public float duration = 1.1f;
	[HideInInspector]
	public float currentDuration = 0;
	[Tooltip ("True - TOP, False - DOWN")]
	public bool zoneMissileTop;

	private Vector2 _directMotion;

	public Vector3 setBetween {
		get { 
			return _directMotion;
		}
		set { 
			_directMotion = value;
			currentDuration = 0;
			if (trailRender != null)
				trailRender.Clear ();
		}
	}

	private Rigidbody2D rg2d;

	private bool acceptRot = true;
	private bool isSeekTarget = false;
	private Vector3 at = Vector3.zero;
	private Vector3 oldnat = Vector3.zero;
	private Quaternion wantedRotationMissile;

	private Vector3 startPos = Vector3.zero;
	private Vector3 endPos;


	void Awake ()
	{
		rg2d = GetComponent<Rigidbody2D> ();
	}

	void Start ()
	{


	}




	private Vector3 zeroVt = Vector3.zero;

	float t;

	void Update ()
	{
//		if (typeMissile == TypeMissile.BezierMissile) {
		if (currentDuration < duration) {
			t = currentDuration / duration;
			at = CalculateBezierPoint (t, startPos, endPos, setBetween);

			oldnat = at - transform.position;
			if (oldnat != zeroVt) {
				wantedRotationMissile = Quaternion.LookRotation (oldnat * Time.deltaTime);
				transform.rotation = wantedRotationMissile;
			}
			transform.position = at;
			currentDuration += Time.deltaTime;
//			if (isSeekTarget) {
			if (currentDuration > duration / 3) {
				ResetMissile ();
			}
//			}
		}
//		}
	}


	public void MovePanapol ()
	{


	}

	//For Bezier 3 Point
	float u, uu;
	Vector3 point;

	/// <summary>
	/// Tính toán đường bezier 3 điểm. 1 điểm điều khiển
	/// </summary>
	/// <param name="t"></param>
	/// <param name="startPosition">Vị trí bắt đầu</param>
	/// <param name="endPosition">Vị trí cuối</param>
	/// <param name="controlPoint">Vị trí điều khiển</param>
	/// <returns></returns>
	public Vector3 CalculateBezierPoint (float t, Vector3 startPosition, Vector3 endPosition, Vector3 controlPoint)
	{
		u = 1 - t;
		uu = u * u;

		point = uu * startPosition;
		point += (2 * u * t) * controlPoint;
		point += (t * t) * endPosition;

		return point;
	}

	public void SetupSpeedAndDuration (float speedFire, float durationBezier, Vector3 posSt, Vector3 distancePos, Vector3 dir)
	{
		speedMovement = speedFire;
		duration = durationBezier;
		startPos = posSt;
		endPos = startPos + distancePos;
		setBetween = dir;
	}

	public void AcceptSeekTarget ()
	{
		isSeekTarget = true;
	}

	public void DeclineSeekTarget ()
	{
		isSeekTarget = false;
	}

	public void ResetMissile ()
	{
		currentDuration = duration;
		Fire ();
	}

	public void Fire ()
	{
		rg2d.velocity = Vector2.zero;
		rg2d.AddForce (transform.forward * speedMovement, ForceMode2D.Impulse);
	}

}
