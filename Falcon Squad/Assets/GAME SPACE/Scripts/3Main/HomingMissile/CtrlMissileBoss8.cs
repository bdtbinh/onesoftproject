﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CtrlMissileBoss8 : UbhBaseShot
{
    //public ParticleSystem effectStep1Gun;
    public Boss8MissileGun boss8MissileGunIns;
    public float timeNextMissile = 0.2f;
    float numMissile = 2;
    //public float timeDelayResumeMove = 1f;

    //void Start ()
    //{
    //	if (boss8MissileGunIns != null) {
    //		boss8MissileGunIns.timeDelayResumeMove = timeDelayResumeMove;
    //	}
    //}

    public override void Shot()
    {
        StartCoroutine("StartShoot");
    }

    int numMissileShoted = 0;

    IEnumerator StartShoot()
    {
        int numMissileShoted = 0;
        if (!boss8MissileGunIns.bossActionObj.bossDamged)
        {
            numMissile = 1;
        }
        else
        {
            numMissile = 2;
        }
        while (true)
        {
            if (gameObject.activeInHierarchy)
            {

                var bullet = GetBullet(transform.position);
                numMissileShoted++;

                if (bullet != null)
                {
                    bullet.transform.rotation = transform.rotation;
                    MissileHommingCtrl com = bullet.GetComponent<MissileHommingCtrl>();
                    com.FindTarget();
                }
            }
            else
            {
                StopAllCoroutines();
                break;
            }
            if (numMissileShoted >= numMissile)
            {
                boss8MissileGunIns.ShootMainWeaponStep3();
                break;
            }
            yield return new WaitForSeconds(timeNextMissile);
        }

    }

}
