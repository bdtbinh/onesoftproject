﻿using Chronos;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

public class CtrlMissilePlayer : SpaceWarShootSetData
{

    public override void Shot()
    {
        //var bullet = GetBullet(transform.position);
        var bullet = GetBullet(transform.position) as UbhBulletPlayer;
        if (bullet != null)
        {
            bullet.transform.rotation = transform.rotation;
            MissileHommingCtrl com = bullet.GetComponent<MissileHommingCtrl>();
            com.FindTarget();

            // code thÊm để chỉnh power
            ChangePower();
            bullet.SetPower(spriteUse, powerUse);

            //
        }
    }



}
