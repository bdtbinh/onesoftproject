﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CtrlMissileBoss2 : UbhBaseShot
{
	public ParticleSystem effectStep1Gun;
	public Boss2MissileGun boss2MissileGunIns;
	public float timeNextMissile = 0.2f;
	public float numMissile = 1;
	public float timeDelayResumeMove = 1f;

	void Start ()
	{
		if (boss2MissileGunIns != null) {
			boss2MissileGunIns.timeDelayResumeMove = timeDelayResumeMove;
		}
	}

	public override void Shot ()
	{
		
		StartCoroutine ("StartShoot");
	}

	int numMissileShoted = 0;

	IEnumerator StartShoot ()
	{
		int numMissileShoted = 0;
		while (true) {

			if (gameObject.activeInHierarchy) {
				if (effectStep1Gun != null) {
					effectStep1Gun.gameObject.SetActive (true);
					effectStep1Gun.Play (true);
				}
				var bullet = GetBullet (transform.position);

				numMissileShoted++;
//				Debug.LogError (numMissileShoted + "  numMissile " + numMissile);

				if (bullet != null) {
					bullet.transform.rotation = transform.rotation;
					MissileHommingCtrl com = bullet.GetComponent<MissileHommingCtrl> ();
					com.FindTarget ();
				}
			} else {
				StopAllCoroutines ();
				if (effectStep1Gun != null) {
					effectStep1Gun.Stop ();
					effectStep1Gun.gameObject.SetActive (false);
				}
				break;
			}

			if (effectStep1Gun != null) {
				effectStep1Gun.Stop ();
				effectStep1Gun.gameObject.SetActive (false);
			}
			if (numMissileShoted >= numMissile) {
//				Debug.LogError ("break " + numMissileShoted + "  numMissile " + numMissile);
				break;
			}
			yield return new WaitForSeconds (timeNextMissile);
		}

	}

}
