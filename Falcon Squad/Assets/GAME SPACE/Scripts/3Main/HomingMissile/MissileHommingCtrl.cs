﻿using UnityEngine;

//using MovementEffects;
using UniRx;
using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;
using SkyGameKit;
using PathologicalGames;
using Chronos;


public enum TypeMissile
{
    PlayerMissile,
    EnemyMissile,
    Boss2Missile

}

public class MissileHommingCtrl : MonoBehaviour
{
    public TypeMissile typeMissile;
    public float timeFollow = 1f;
    public float initialVelocity;
    public float acceleration;

    public float speedRotation;

    private float _currentVelocity;

    public float CurrentVelocity
    {
        get
        {
            return _currentVelocity;
        }
        set
        {
            _currentVelocity = value;
        }
    }

    private Vector3 direction = Vector3.right;

    [DisplayAsString]
    public Transform m_target;

    private Vector3 diff;
    private Vector3 axis = Vector3.forward;
    private CompositeDisposable disUpdate = new CompositeDisposable();

    GameObject playerTarget;
    UbhBullet ubhBulletIns;
    float timeDelayBoss2MissileExplosion;
    public float timeDelayMissileExplosionMin = 128;
    public float timeDelayMissileExplosionMax = 188;




    //--------------------Check obj có phải thuộc player không đổi đổi time khi dùng EMP-----------------------------------------------------------------------------
    private void Awake()
    {
        timelineIsEnable = CheckTimelineEnable();
    }

    bool timelineIsEnable = false;
    Timeline timelineInObj;

    bool CheckTimelineEnable()
    {
        if (GetComponent<Timeline>() != null)
        {
            timelineInObj = GetComponent<Timeline>();
            return true;
        }
        return false;
    }

    float GetTimeSphereOfObj()
    {
        if (timelineIsEnable)
        {
            if (timelineInObj.timeScale != 0)
            {
                return timelineInObj.timeScale;
            }
        }
        return 1f;
    }
    //------------------------------------------------------------------------------------------

    void Start()
    {
        //		FindNewTarget ();
        ubhBulletIns = GetComponent<UbhBullet>();
        //playerTarget = GameObject.FindGameObjectWithTag("player");
        timeDelayBoss2MissileExplosion = ((float)UnityEngine.Random.Range(timeDelayMissileExplosionMin, timeDelayMissileExplosionMax)) / 100f;
    }


    private void OnEnable()
    {
        if (playerTarget == null)
        {
            playerTarget = GameObject.FindGameObjectWithTag("player");
        }
    }

    private void OnDestroy()
    {
        disUpdate.Dispose();
    }

    //	public List<string> listIconProgressLv;

    public string tagNameTargetBoss = "Boss1";
    public string tagNameTargetBoss2 = "Boss2";
    public string tagNameTargetBoss3 = "Boss3";
    public string tagNameTargetBoss3_Shield_Core = "Boss3_Shield_Core";
    public string tagNameTargetBoss4 = "Boss4";
    public string tagNameTargetBoss5 = "Boss5";
    public string tagNameTargetBoss7 = "Boss7";
    public string tagNameTargetBoss8 = "Boss8";
    public string tagNameTargetBoss9 = "Boss9";
    public string tagNameTargetBoss10 = "Boss10";

    public string tagNameTargetNhen = "EnemyNhen_Child";
    public string tagNameTargetEnemy = "Enemy";
    public string tagNameTargetPlayer = "player";



    public void FindTarget()
    {
        if (typeMissile == TypeMissile.EnemyMissile)
        {
            FindPlayer();
        }
        else if (typeMissile == TypeMissile.PlayerMissile)
        {
            FindEnemy();
        }
        else if (typeMissile == TypeMissile.Boss2Missile)
        {
            MissileBoss2FindPlayer();
        }
    }




    void FindEnemy()
    {
        CurrentVelocity = initialVelocity;
        m_target = null;
        //		Timing.RunCoroutine (_FindNewTarget ().CancelWith (gameObject));

        this.Delay(0.25f / GetTimeSphereOfObj(), () =>
        {
            MainScene.Instance.NoteObjPlayerChangeTime();
            if (FindNewEnemy() != null)
            {
                //				Debug.LogError (target.name);
                m_target = FindNewEnemy();
                this.Delay(timeFollow / GetTimeSphereOfObj(), () =>
                {
                    MainScene.Instance.NoteObjPlayerChangeTime();
                    if (MainScene.Instance.gameFinished)
                    {
                        m_target = null;
                    }
                });
            }
        });
        //		start
        Observable.EveryUpdate().Subscribe(_ =>
        {
            if (m_target == null)
            {
                CurrentVelocity += acceleration * Time.deltaTime * GetTimeSphereOfObj();
                transform.Translate(direction * CurrentVelocity * Time.deltaTime * GetTimeSphereOfObj());
                MainScene.Instance.NoteObjPlayerChangeTime();
            }
            else
            {
                if (m_target.gameObject.activeInHierarchy)
                {
                    //Target active in scene
                    diff = m_target.position - transform.position;
                    float angle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
                    Quaternion q = Quaternion.AngleAxis(angle, axis);
                    transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * GetTimeSphereOfObj() * speedRotation);
                    MainScene.Instance.NoteObjPlayerChangeTime();
                    CurrentVelocity += acceleration * Time.deltaTime;
                    transform.Translate(direction * CurrentVelocity * Time.deltaTime * GetTimeSphereOfObj());
                    MainScene.Instance.NoteObjPlayerChangeTime();
                }
                else
                {
                    m_target = null;
                }
            }
        }).AddTo(disUpdate);
    }


    //


    void FindPlayer()
    {
        CurrentVelocity = initialVelocity;
        m_target = null;
        //		Timing.RunCoroutine (_FindNewTarget ().CancelWith (gameObject));

        this.Delay(0.25f, () =>
        {

            if (playerTarget != null)
            {
                m_target = playerTarget.transform;

                this.Delay(timeFollow, () =>
                {
                    if (playerTarget != null)
                    {
                        m_target = null;
                    }

                });
            }
        });
        //		start
        Observable.EveryUpdate().Subscribe(_ =>
        {
            if (m_target == null)
            {
                CurrentVelocity += acceleration * Time.deltaTime;
                transform.Translate(direction * CurrentVelocity * Time.deltaTime);
            }
            else
            {
                if (m_target.gameObject.activeInHierarchy && m_target.parent.gameObject.activeInHierarchy)
                {
                    //Target active in scene
                    diff = m_target.position - transform.position;
                    float angle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
                    Quaternion q = Quaternion.AngleAxis(angle, axis);
                    transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speedRotation);

                    CurrentVelocity += acceleration * Time.deltaTime;
                    transform.Translate(direction * CurrentVelocity * Time.deltaTime);
                }
                else
                {
                    m_target = null;
                }
            }
        }).AddTo(disUpdate);
    }




    void MissileBoss2FindPlayer()
    {
        CurrentVelocity = initialVelocity;
        m_target = null;
        //		Timing.RunCoroutine (_FindNewTarget ().CancelWith (gameObject));

        this.Delay(0.29f, () =>
        {

            if (playerTarget != null)
            {
                m_target = playerTarget.transform;

                this.Delay(timeDelayBoss2MissileExplosion, () =>
                {
                    if (playerTarget != null)
                    {
                        m_target = null;
                    }

                    disUpdate.Clear();
                    Pool_Boss2MissileExplosion(transform);
                    //
                    if (ubhBulletIns != null)
                    {
                        UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
                    }
                });
            }
        });
        //		start
        Observable.EveryUpdate().Subscribe(_ =>
        {
            if (m_target == null)
            {
                CurrentVelocity += acceleration * Time.deltaTime;
                transform.Translate(direction * CurrentVelocity * Time.deltaTime);
            }
            else
            {
                if (m_target.gameObject.activeInHierarchy && m_target.parent.gameObject.activeInHierarchy)
                {
                    //Target active in scene
                    diff = m_target.position - transform.position;
                    float angle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
                    Quaternion q = Quaternion.AngleAxis(angle, axis);
                    transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speedRotation);

                    CurrentVelocity += acceleration * Time.deltaTime;
                    transform.Translate(direction * CurrentVelocity * Time.deltaTime);
                }
                else
                {
                    m_target = null;
                }
            }
        }).AddTo(disUpdate);
    }

    public void Pool_Boss2MissileExplosion(Transform bulletTrans)
    {
        if (EffectList.Instance.fxBoss2MissileExplosion != null)
        {
            //			Debug.LogError (bulletTrans.position);
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.fxBoss2MissileExplosion, bulletTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    //-----------------------------------------------------

    private void OnDisable()
    {
        disUpdate.Clear();
    }




    protected Transform FindNewEnemy()
    {
        foreach (Collider2D newTarget in Physics2D.OverlapAreaAll(MainScene.Instance.posAnchorTopLeft.localPosition, MainScene.Instance.posAnchorBottomRight.localPosition))
            if (newTarget.gameObject.CompareTag(tagNameTargetBoss) ||
                newTarget.gameObject.CompareTag(tagNameTargetBoss2) ||
                newTarget.gameObject.CompareTag(tagNameTargetBoss3) ||
                newTarget.gameObject.CompareTag(tagNameTargetBoss3_Shield_Core) ||
                 newTarget.gameObject.CompareTag(tagNameTargetBoss4) ||
                  newTarget.gameObject.CompareTag(tagNameTargetBoss5) ||
                   newTarget.gameObject.CompareTag(tagNameTargetBoss7) ||
                   newTarget.gameObject.CompareTag(tagNameTargetBoss8) ||
                   newTarget.gameObject.CompareTag(tagNameTargetBoss9) ||
                   newTarget.gameObject.CompareTag(tagNameTargetBoss10) ||
                newTarget.gameObject.CompareTag(tagNameTargetNhen) ||
                newTarget.gameObject.CompareTag(tagNameTargetEnemy))
            {
                //				Debug.LogError (newTarget.gameObject.name);
                return newTarget.transform;
            }

        return null;
    }

}
