﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CtrlMissile : UbhBaseShot {

    void Start() {
       
    }

    public override void Shot() {
        var bullet = GetBullet(transform.position);

        if (bullet != null) {
            bullet.transform.rotation = transform.rotation;
            MissileHommingCtrl com = bullet.GetComponent<MissileHommingCtrl>();
            com.FindTarget();
        }
    }

}
