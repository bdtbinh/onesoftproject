﻿using UnityEngine;

public class RestartTrail : MonoBehaviour
{

    TrailRenderer trail;


    // Use this for initialization
    void Awake()
    {
        trail = GetComponent<TrailRenderer>();
    }

    private void OnDisable()
    {
        trail.Clear();
    }
}
