﻿using PathologicalGames;
using UnityEngine;
using SkyGameKit;
using Sirenix.OdinInspector;
using System;
using SWS;
using DG.Tweening;
using System.Collections.Generic;

namespace SkyGameKit.Demo
{
    public class FreeTurnCuongvt : TurnManager
    {
        public Transform target;
        public bool followTarget;
        [ShowIf("followTarget")]
        public bool followTargetLerp = true;
        [ShowIf("followTarget")]
        public float followSpeed = 2f;
        public bool spawnInTarget;
        public MiniTurnEx1[] listMiniTurn;


        //-----------data truyỀn cho enemy----------------
        [TitleGroup("Data ChangeSpeed Action")]
        [TitleGroup("Data ChangeSpeed Action")]
        public float newSpeed = 2;
        //

        [TitleGroup("Data Pause Action")]
        public float timePauseAction = 3;
        //

        [TitleGroup("Data New Path")]
        public PathManager newPath;
        [TitleGroup("Data New Path")]
        public Vector3 posStartPathNew;

        //
        string InfoBoxMessage = "Chỉ con nào cần Anim giống nhau thì mới tick sync Animation";
        [TitleGroup("Sync Animation")]
        [InfoBox("$InfoBoxMessage")]
        [TitleGroup("Sync Animation")]
        public bool syncAnimation = false;
        //



        //
        string InfoBoxMessage2 = "Chỉ tick 1 trong 2";
        [TitleGroup("Start By Number")]
        [InfoBox("$InfoBoxMessage2")]
        [TitleGroup("Start By Number")]
        public bool enemyStartByNumber = true;

        [TitleGroup("Start By Number")]
        public int number = 1;
        //

        [TitleGroup("Start By Position")]
        [InfoBox("$InfoBoxMessage2")]
        [TitleGroup("Start By Position")]
        public bool enemyStartByPosition = false;

        [TitleGroup("Start By Position")]
        public List<Vector2> startPosition;

        //---




        private int[] backupNumberListMiniTurn;//Do số lượng enemy bị trừ dần (thay đổi) nên phải có biến này để backup nhằm sử dụng lại turn nhiều lần
        protected virtual void Start()
        {
            totalEnemy = 0;
            backupNumberListMiniTurn = new int[listMiniTurn.Length];
            if (listMiniTurn.Length > 0)
            {
                for (int i = 0; i < listMiniTurn.Length; i++)
                {
                    if (listMiniTurn[i].number <= 0)
                    {
                        listMiniTurn[i].number = 1;
                    }
                    backupNumberListMiniTurn[i] = listMiniTurn[i].number;
                    totalEnemy += listMiniTurn[i].number;
                }
            }
            timeToBegin = 0;//Gọi là phải bắt đầu luôn
        }



        void SetDataToEnemy(EnemyData enemyDataIns)
        {

            enemyDataIns.newSpeed = newSpeed;
            enemyDataIns.timeActionPause = timePauseAction;

            enemyDataIns.syncAnimation = syncAnimation;

            //
            enemyDataIns.newPath_Loop = newPath;
            enemyDataIns.posStartPathLoop = posStartPathNew;
        }





        public override int StartCountDelayTurn()
        {
            if (isRunning)
            {
                Debug.LogError("FreeTurn: Turn " + name + " đang chạy rồi, gọi ít thôi");
                return 0;
            }
            if (target == null)
            {
                Debug.LogWarning("FreeTurn: No targer");
            }
            if (timeToBegin > 0)
            {
                timeToBegin = 0;
                Debug.LogWarning("Free turn có timeToBegin > 0, cái này không được, gọi là phải chạy luôn");
            }
            return base.StartCountDelayTurn();
        }

        protected override void BeginTurn()
        {
            for (int j = 0; j < listMiniTurn.Length; j++)
            {
                listMiniTurn[j].number = backupNumberListMiniTurn[j];
            }
            int i = 0;
            this.DoActionEveryTime(timeToNextEnemy, totalEnemy, () => {
                Transform myInstance;
                if (spawnInTarget)
                {
                    myInstance = PoolManager.Pools[Const.enemyPoolName].Spawn(listMiniTurn[i].enemyPrefab, target.position, target.rotation, transform);
                }
                else
                {
                    myInstance = PoolManager.Pools[Const.enemyPoolName].Spawn(listMiniTurn[i].enemyPrefab, transform.position, transform.rotation, transform);
                }
                BaseEnemy enemy = myInstance.GetComponent<BaseEnemy>();
                EnemyData enemyDataIns = myInstance.GetComponent<EnemyData>();
                SetDataToEnemy(enemyDataIns);

                InitEnemy(enemy, listMiniTurn[i]);
                listMiniTurn[i].number--;
                if (listMiniTurn[i].number <= 0) i++;
            }, true);
            base.BeginTurn();
            if (followTarget)
            {//Cái này để tránh bị xuất hiện giũa màn hình
                if (target != null)
                {
                    transform.position = target.position;
                }
            }
        }

        protected virtual void Update()
        {
            if (isRunning)
            {
                if (followTarget)
                {
                    if (target != null)
                    {
                        if (followTargetLerp)
                        {
                            transform.position = Vector3.Lerp(transform.position, target.position, followSpeed * Time.deltaTime);
                        }
                        else
                        {
                            transform.position = target.position;
                        }
                    }
                }
            }
        }

        protected override void EndTurn()
        {
            if (isRunning)
            {
                isRunning = false;
                if (enemyRemain > 0)
                {
                    foreach (var enemyTmp in enemyList)
                    {
                        if (PoolManager.Pools[Const.enemyPoolName].IsSpawned(enemyTmp.transform))
                        {
                            if (!enemyTmp.isRemoved)
                            {
                                enemyTmp.ForceRemove();
                            }
                        }
                    }
                }
                if (endTurnEvent != null) endTurnEvent.Invoke();
            }
        }
    }
}
