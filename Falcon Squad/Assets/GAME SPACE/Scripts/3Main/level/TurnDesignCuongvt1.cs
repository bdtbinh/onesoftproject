﻿using PathologicalGames;
using UnityEngine;
using SkyGameKit;
using Sirenix.OdinInspector;
using System;
using SWS;
using DG.Tweening;
using System.Collections.Generic;


public class TurnDesignCuongvt1 : TurnManager
{
    public MiniTurnLevelInGame[] listMiniTurn;
    public ChangeDurationCamera changeDurationCamera;

    void Start()
    {
        //test
        //if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        //{
        //    timeToEnd = 8;
        //}
        //end test
        totalEnemy = 0;
        if (listMiniTurn.Length > 0)
        {
            foreach (var miniTurn in listMiniTurn)
            {
                if (miniTurn.enemyStartByNumber)
                {
                    if (miniTurn.number <= 0)
                    {
                        miniTurn.number = 1;
                    }
                    totalEnemy += miniTurn.number;
                }
                else if (miniTurn.enemyStartByPosition)
                {
                    if (miniTurn.startPosition.Count <= 0)
                    {
                        miniTurn.startPosition.Add(Vector3.zero);
                    }
                    totalEnemy += miniTurn.startPosition.Count;
                }
                else
                {
                    Debug.LogError("Da chon start by Number hay Position chua em ei");
                }
            }
            enemyRemain = totalEnemy;
        }


    }

#if UNITY_EDITOR
    protected override void OnDrawGizmosSelected()
    {
        if (UnityEditor.Selection.activeGameObject.GetComponent<LevelManager>() != null) return;//Không hiện khi chọn vào LevelManager
        if (listMiniTurn[0].enemyStartByPosition)
        {
            foreach (Vector3 posEnemy in listMiniTurn[0].startPosition)
            {
                ShowPathManager.ShowPathInScene(path, transform.position + posEnemy);
            }
        }
        else
        {
            ShowPathManager.ShowPathInScene(path, transform.position);
        }
    }
#endif




    protected override void BeginTurn()
    {
        int i = 0;
        if (listMiniTurn[i].enemyStartByNumber)
        {
            this.DoActionEveryTime(timeToNextEnemy, totalEnemy, () =>
            {
                Transform myInstance = PoolManager.Pools[Const.enemyPoolName].Spawn(listMiniTurn[i].enemyPrefab, transform.position, transform.rotation, transform);

                //Debug.LogError("myInstance---:" + myInstance);
                BaseEnemy enemy = myInstance.GetComponent<BaseEnemy>();
                //
                //Debug.LogError("enemy: " + enemy);
                EnemyData enemyDataIns = myInstance.GetComponent<EnemyData>();
                //Debug.LogError("enemyDataIns: " + enemyDataIns);
                SetDataToEnemy(enemyDataIns, i);
                //

                InitEnemy(enemy, listMiniTurn[i]);
                listMiniTurn[i].number--;
                if (listMiniTurn[i].number <= 0)
                    i++;
            }, true);
        }
        else if (listMiniTurn[i].enemyStartByPosition)
        {
            this.DoActionEveryTime(timeToNextEnemy, totalEnemy, () =>
            {
                Transform myInstance = PoolManager.Pools[Const.enemyPoolName].Spawn(
                                           listMiniTurn[i].enemyPrefab,
                                           transform.position + (Vector3)listMiniTurn[i].startPosition[0],
                                           transform.rotation,
                                           transform
                                       );
                //Debug.LogError("myInstance: " + myInstance);
                BaseEnemy enemy = myInstance.GetComponent<BaseEnemy>();
                //Debug.LogError("enemy: " + enemy);
                //
                EnemyData enemyDataIns = myInstance.GetComponent<EnemyData>();
                //Debug.LogError("enemyDataIns: " + enemyDataIns);
                SetDataToEnemy(enemyDataIns, i);
                //
                InitEnemy(enemy, listMiniTurn[i]);
                listMiniTurn[i].startPosition.RemoveAt(0);
                if (listMiniTurn[i].startPosition.Count <= 0)
                    i++;
            }, true);
        }
        else
        {
            Debug.LogError("Da chon start by Number hay Position chua em ei");
        }

        base.BeginTurn();
        //
        ChangeDurationBackGround();

    }


    void SetDataToEnemy(EnemyData enemyDataIns, int index)
    {

        enemyDataIns.newSpeed = listMiniTurn[index].newSpeed;
        enemyDataIns.timeActionPause = listMiniTurn[index].timePauseAction;
        enemyDataIns.syncAnimation = listMiniTurn[index].syncAnimation;
        //
        enemyDataIns.newPath_Loop = listMiniTurn[index].newPath;
        enemyDataIns.posStartPathLoop = listMiniTurn[index].posStartPathNew;
    }



    public void ChangeDurationBackGround()
    {
        //		Resources.UnloadUnusedAssets ();
        if (changeDurationCamera.changeSpeedCamera && ChangeBackground.Instance != null)
        {
            ChangeBackground.Instance.SetSpeedMoveBackGround(changeDurationCamera.speedBG, changeDurationCamera.speedDecor, changeDurationCamera.timeChange);
            //			DOTween.To (() => ChangeBackground.Instance.tweenMoveBG.duration, x => ChangeBackground.Instance.tweenMoveBG.duration = x, changeDurationCamera.durationBG, changeDurationCamera.timeChange);
            //			DOTween.To (() => ChangeBackground.Instance.tweenMoveCloud.duration, x => ChangeBackground.Instance.tweenMoveCloud.duration = x, changeDurationCamera.durationCloud, changeDurationCamera.timeChange);
        }

    }

}






[Serializable]
public class DataPauseAction
{
    public float timePause = 3;

}


[Serializable]
public class DataSetNewPath
{
    public PathManager newPath;
    public Vector2 startPositionPathNew;
}





[Serializable]
public class MiniTurnLevelInGame : MiniTurn
{
    //

    [TitleGroup("Data ChangeSpeed Action")]
    public float newSpeed = 2;
    //

    [TitleGroup("Data Pause Action")]
    public float timePauseAction = 3;
    //

    [TitleGroup("Data New Path")]
    public PathManager newPath;
    [TitleGroup("Data New Path")]
    public Vector3 posStartPathNew;

    //
    string InfoBoxMessage = "Chỉ con nào cần Anim giống nhau thì mới tick sync Animation";
    [TitleGroup("Sync Animation")]
    [InfoBox("$InfoBoxMessage")]
    [TitleGroup("Sync Animation")]
    public bool syncAnimation = false;
    //



    //
    string InfoBoxMessage2 = "Chỉ tick 1 trong 2";
    [TitleGroup("Start By Number")]
    [InfoBox("$InfoBoxMessage2")]
    [TitleGroup("Start By Number")]
    public bool enemyStartByNumber = true;

    [TitleGroup("Start By Number")]
    [ShowIf("enemyStartByNumber")]
    public int number = 1;
    //

    [TitleGroup("Start By Position")]
    [InfoBox("$InfoBoxMessage2")]
    [TitleGroup("Start By Position")]
    public bool enemyStartByPosition = false;

    [TitleGroup("Start By Position")]
    [ShowIf("enemyStartByPosition")]
    public List<Vector2> startPosition;



}




[Serializable]
public class ChangeDurationCamera
{
    //
    [ToggleGroup("changeSpeedCamera")]
    public bool changeSpeedCamera = false;

    [ToggleGroup("changeSpeedCamera")]
    public float speedBG = 2f;
    [ToggleGroup("changeSpeedCamera")]
    public float speedDecor = 2.6f;
    [ToggleGroup("changeSpeedCamera")]
    public float timeChange = 3f;
}