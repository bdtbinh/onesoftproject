﻿using PathologicalGames;
using UnityEngine;
using SkyGameKit;
using Sirenix.OdinInspector;
using DG.Tweening;
using SWS;
using UniRx;
//using Com.LuisPedroFonseca.ProCamera2D;

public class BaseEnemyAI : BaseEnemy {

    [HideInInspector]
    public DataTweenGoToPoint[] tweenGoPoint;
    [HideInInspector]
    public EFirstProperty eFirstProperty;
    [HideInInspector]
    public AttrSetPath attrSetPath;
    [HideInInspector]
    public ExtrasAction extraA;

    [BoxGroup("On Hit", true, true)]
    public SpriteRenderer spriteEnemy;
    [BoxGroup("On Hit")]
    public Color spriteColorChange;
    [BoxGroup("On Hit")]
    public float timeChangeColor;
  
    public UbhShotCtrl ubhShotCtrl;

    public void CheckStoreItem() {
        if(extraA.itemObj != null) {
            
        } else {
            
        }
    }

    /// <summary>
    /// AI Set New Position
    /// </summary>
    private int countTween;
    public Sequence tweenGoTPointSeq;
    private Vector3 newPositon = Vector3.zero;

    public void SetupFirstProperty() {
        if(eFirstProperty.score >= 0)
            score = eFirstProperty.score;
        if (eFirstProperty.HP >= 0)
            maxHP = eFirstProperty.HP;
        M_SplineMove.speed = eFirstProperty.speed;
        M_SplineMove.easeType = eFirstProperty.easeType;
        M_SplineMove.startPoint = eFirstProperty.startPoint;
        M_SplineMove.moveToPath = eFirstProperty.moveToPath;

        tempSpeedEnemy = M_SplineMove.speed;
    }

    public void SetGoToPoint() {
        M_SplineMove.Stop();

        if (PoolManager.Pools[Const.pathPool].IsSpawned(M_SplineMove.pathContainer.transform)) {
            PoolManager.Pools[Const.pathPool].Despawn(M_SplineMove.pathContainer.transform);
        }

        countTween = tweenGoPoint.Length;
        ProcessGoToPoint(countTween);
    }

    Vector3 processQRotate;
    float rot_z;

    private void ProcessGoToPoint(int maxTween) {
        if(countTween > 0) {
            tweenGoTPointSeq = DOTween.Sequence();

            DataTweenGoToPoint tween = tweenGoPoint[maxTween - countTween];
           
            newPositon.x = tween.position.x;
            newPositon.y = tween.position.y;

            tweenGoTPointSeq.Append (
                transform.DOMove(newPositon, tween.durationNDelay.x).SetDelay(tween.durationNDelay.y)
                .SetEase(tween.easeType).SetRelative(tween.isRelative).SetLoops(tween.numberLoop, tween.loopType)
            ).OnUpdate(()=> {
                if (tween.lookAhead) {  
                    if (Vector3.Distance(transform.position, newPositon) > 2* tween.durationNDelay.x) {
                        processQRotate = newPositon - transform.position;
                        processQRotate.Normalize();
                        rot_z = Mathf.Atan2(processQRotate.y, processQRotate.x) * Mathf.Rad2Deg;
                        transform.DORotateQuaternion (Quaternion.Euler(0,0,rot_z), tween.speedLook);
                    }          
                }   
            }).OnComplete(()=> {

                if (tween.lookBottomComplete > 0) {
                    transform.DORotateQuaternion(Quaternion.Euler(0, 0, tween.lookBottomComplete), tween.speedLook);
                }

                if ((tween.typeTimeShot & DataTweenGoToPoint.TypeShot.ShotODone) == DataTweenGoToPoint.TypeShot.ShotODone) {
                    if (tween.delayShot.y > 0)
                        this.Delay(tween.delayShot.y, () => StartShotingGun());
                    else
                        StartShotingGun();
                }

                if ((tween.typeTimeShot & DataTweenGoToPoint.TypeShot.SwitchSNP) == DataTweenGoToPoint.TypeShot.SwitchSNP) {
                    countTween = 0;
                    tweenGoTPointSeq.Kill();
                    SetNewPath();
                } else {
                    countTween -= 1;
                    ProcessGoToPoint(maxTween);
                } 

            }).OnPlay(()=> {
                if ((tween.typeTimeShot & DataTweenGoToPoint.TypeShot.ShotOPlay) == DataTweenGoToPoint.TypeShot.ShotOPlay) {
                    if (tween.delayShot.x > 0)
                        this.Delay(tween.delayShot.x, () => StartShotingGun());
                    else
                        StartShotingGun();
                }      
            });
        }
    }
    //End: AI Set New Position
    /// <summary>
    /// AI Set New Path And Event Speed
    /// </summary>
    public void SetNewPath() {
        SNP(attrSetPath.delaySetPath);
    }

    private void SNP(float t) {
        //Set New Path Process
        M_SplineMove.Stop();

        if(attrSetPath.setPathType == TypeSetPath.NewPath || attrSetPath.setPathType == TypeSetPath.NewPathES)
            M_SplineMove.easeType = attrSetPath.easyNew;

        if (PoolManager.Pools[Const.pathPool].IsSpawned(M_SplineMove.pathContainer.transform)) {
            PoolManager.Pools[Const.pathPool].Despawn(M_SplineMove.pathContainer.transform);
        }

        Transform newPathT = PoolManager.Pools[Const.pathPool].Spawn(attrSetPath.newPath.gameObject, transform.position,
            PoolManager.Pools[Const.pathPool].prefabs[attrSetPath.newPath.name].rotation);
        var bezier = newPathT.GetComponent<BezierPathManager>();
        if (bezier != null) bezier.CalculatePath();
        var newPathManager = newPathT.GetComponent<PathManager>();
        M_SplineMove.SetPath(newPathManager);

        if (t < 0) return;

        if (t == 0) {
            SNPPartTwo();
        } else if (t > 0) {
            this.Delay(t, () => SNPPartTwo());
        }
        
    }

    private void SNPPartTwo() {
        //SetupEvent
        if (attrSetPath.dataAction.Length > 0) {
            for (int i = 0; i < attrSetPath.dataAction.Length; i++) {
                int a = i;
                DataActionSetPath data = attrSetPath.dataAction[a];

                M_SplineMove.events[data.point].AddListener(() => {
                    if ((data.actionSetPath & ActionSetPath.SpeedChange) == ActionSetPath.SpeedChange) {
                        if (data.dataSpeed.y > 0) {             
                            this.Delay(data.dataSpeed.y, () => {
                                M_SplineMove.ChangeSpeed(data.dataSpeed.x);
                            });
                        } else {
                            M_SplineMove.ChangeSpeed(data.dataSpeed.x);
                        }
                    }

                    if ((data.actionSetPath & ActionSetPath.PauseMove) == ActionSetPath.PauseMove) {
                        M_SplineMove.Pause(data.delayPauseMove.x);
                    }

                    if ((data.actionSetPath & ActionSetPath.Shotting) == ActionSetPath.Shotting) {
                        if (data.delayPauseMove.y > 0) {
                            this.Delay(data.delayPauseMove.y, () => StartShotingGun());
                        } else {
                            StartShotingGun();
                        }
                    }
                });
            }
        }
    }

    //End : AI Set New Path And Event Speed

    public void DebugTest(string debug) {
        Debug.LogError(debug);
    }

    public void SpawnItem() {
        if(extraA.itemObj != null) {
            PoolManager.Pools[Const.itemPoolName].Spawn(extraA.itemObj, transform.position, Quaternion.identity);
        }
    }

    private void StartShotingGun() {
        if (ubhShotCtrl != null)
            ubhShotCtrl.StartShotRoutine();
    }

    //WARNING: FOR EVENT ORIGINAL
    public void StartShottingGunWithDelay() {
        if (extraA.delayShooting > 0) {
            this.Delay(extraA.delayShooting, () => StartShotingGun());
        } else {
            StartShotingGun();
        }
    }
    //

    public void StopShotingGun() {
        if (ubhShotCtrl != null)
            ubhShotCtrl.StopShotRoutine();
    }

    public void StopPathAfterDelay() {
        M_SplineMove.Pause(extraA.delayResume);
    }

    public void ForceDie() {
        if (dieExplosion != null) {
            PoolManager.Pools[Const.explosiveName].Spawn(dieExplosion, transform.position, Quaternion.identity);
        }
        Die(EnemyKilledBy.DeadZone);
    }

    //Save Old Data Enemy 
    private float tempSpeedEnemy;

    private void DataOldEnemy() {
        countTween = 0;
        M_SplineMove.speed = tempSpeedEnemy;

        //Kill Tweening
        tweenGoTPointSeq.Kill();
    }

    public override void Restart() {
        base.Restart();
    }

    public bool shakeItCamera;

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player) {
        DataOldEnemy();

        if (PoolManager.Pools[Const.pathPool].IsSpawned(M_SplineMove.pathContainer.transform)) {
            PoolManager.Pools[Const.pathPool].Despawn(M_SplineMove.pathContainer.transform);
        }

        if (type == EnemyKilledBy.Player)
            PoolManager.Pools[Const.itemPoolName].Spawn("Item_Gems1", transform.position, Quaternion.identity);
       
        if(shakeItCamera)
//            ProCamera2DShake.Instance.Shake("ShakeHit");

        base.Die(type);
    }

    public override void ForceRemove() {
        base.ForceRemove();
    }

    public override void OnTakeDamage(int power) {
        if(spriteEnemy != null) {
            spriteEnemy.DOColor(spriteColorChange, timeChangeColor).OnComplete(()=> {
                spriteEnemy.color = Color.white;
            });
        }
    }

}
