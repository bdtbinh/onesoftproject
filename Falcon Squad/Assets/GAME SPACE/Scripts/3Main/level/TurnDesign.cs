﻿using PathologicalGames;
using UnityEngine;
using SkyGameKit;
using Sirenix.OdinInspector;
using System;
using SWS;
using DG.Tweening;
using System.Collections.Generic;

public class TurnDesign : TurnManager {
    [BoxGroup("******** DESIGN TURN ********", true, true)]
    [LabelText("LIST ACTOR")]
    public MiniTurnExLevelInGame[] listMiniTurn;

    void Start() {
        totalEnemy = 0;
        if (listMiniTurn.Length > 0) {
            foreach (var miniTurn in listMiniTurn) {
                if (miniTurn.startPosition.Count <= 0) {
                    miniTurn.startPosition.Add(Vector3.zero);
                }
                totalEnemy += miniTurn.startPosition.Count;
            }
            enemyRemain = totalEnemy;
        }
    }

    protected override void BeginTurn() {
        int i = 0;
        this.DoActionEveryTime(timeToNextEnemy, totalEnemy, () => {
            Transform myInstance = PoolManager.Pools[Const.enemyPoolName].Spawn(
                listMiniTurn[i].enemyPrefab,
                transform.position + (Vector3)listMiniTurn[i].startPosition[0],
                transform.rotation,
                transform
                );
            BaseEnemy enemy = myInstance.GetComponent<BaseEnemy>();
            BaseEnemyAI baseAI = myInstance.GetComponent<BaseEnemyAI>();

            BeforeBaseEnemyExecute(baseAI, i);

            InitEnemy(enemy, listMiniTurn[i]);

            //AfterBaseEnemyExecute(baseAI, i);

            listMiniTurn[i].startPosition.RemoveAt(0);
            if (listMiniTurn[i].startPosition.Count <= 0) i++;
        }, false);
        base.BeginTurn();
    }

    protected void BeforeBaseEnemyExecute(BaseEnemyAI baseAI, int i) {
        //Setup First Property
        if (listMiniTurn[i].FirstProperty) {
            baseAI.eFirstProperty = listMiniTurn[i].propertyFirst;
            baseAI.SetupFirstProperty();
        }

        //Extra Resume
//        if (listMiniTurn[i].Extras) {
//            baseAI.extraA = listMiniTurn[i].extra;
//            baseAI.CheckStoreItem();
//        }

        //Change Speed By Path
        if (listMiniTurn[i].SetNewPathEvent) {
            baseAI.attrSetPath = listMiniTurn[i].attrSetPath;
            if(listMiniTurn[i].attrSetPath.setPathType == TypeSetPath.PathTurnES) {
                baseAI.attrSetPath.newPath = path;
                baseAI.attrSetPath.easyNew = listMiniTurn[i].propertyFirst.easeType;
            }
        }

        //Change Speed By Point
        if (listMiniTurn[i].TweenGoToPointEvent)
            baseAI.tweenGoPoint = listMiniTurn[i].tweenGoTPoint;

    }

    protected void AfterBaseEnemyExecute(BaseEnemyAI baseAI, int i) {
        
    }


}

[Serializable]
public class EFirstProperty {
    public int score = -1;
    public int HP = -1;
    public float speed = 3;
    public Ease easeType = Ease.Linear;
    public bool moveToPath = false;
    public int startPoint = 0;
}

[Serializable]
public class DataTweenGoToPoint {
    [Flags]
    public enum TypeShot {
        ShotOPlay = 1<<1,
        ShotODone = 1<<2,
        SwitchSNP = 1<<3
    }

    [EnumToggleButtons, HideLabel]
    public TypeShot typeTimeShot;
    [LabelText("Delay Shot: OnPlay / OnComplete")]
    public Vector2 delayShot;
    public Ease easeType = Ease.Linear;
    public Vector2 position;
    public bool isRelative;
    [LabelText("Duration / Delay")]
    public Vector2 durationNDelay;
    public LoopType loopType;
    public int numberLoop;

    public bool lookAhead;
    [ShowIf("lookAhead")]
    [Range(0,60)]
    public float speedLook = 0.5f;
    [ShowIf("lookAhead")]
    [Range(-1, 360)]
    public float lookBottomComplete = -1;

}

public enum TypeSetPath {
    NewPath,
    NewPathES,
    PathTurnES
}

[Flags]
public enum ActionSetPath {
    SpeedChange = 1 << 1,
    PauseMove = 1 << 2,
    Shotting = 1 << 3
}

[Serializable]
public class DataActionSetPath {
    [EnumToggleButtons, HideLabel]
    public ActionSetPath actionSetPath;
    [LabelText("Point Event")]
    public int point;
    [LabelText("Speed / Delay Speed")]
    public Vector2 dataSpeed;
    [LabelText("Delay: Pause Move / Shotting")]
    public Vector2 delayPauseMove;
}

[Serializable]
public class AttrSetPath {
    [EnumToggleButtons, HideLabel, Title("Set Event Path Type", bold: true, horizontalLine: true)]
    [OnValueChanged("ValueSetPathType")]
    public TypeSetPath setPathType = TypeSetPath.PathTurnES;
    public Ease easyNew;
    public PathManager newPath;
    public float delaySetPath;
    public DataActionSetPath[] dataAction;

    [HideInInspector]
    public bool useChangeSpeed = true;

    protected void ValueSetPathType() {
        if (setPathType == TypeSetPath.NewPath) {
            delaySetPath = -1;
            easyNew = Ease.Linear;
        } else if(setPathType == TypeSetPath.NewPathES) {
            delaySetPath = 0;
            easyNew = Ease.Linear;
        } else {
            delaySetPath = 0;
            easyNew = Ease.Unset;
        }      
    }

}

[Serializable]
public class ExtrasAction {
    public float delayShooting;
    [LabelText("Delay Pause Move")]
    public float delayResume;
    [LabelText("Type Item Spawn")]
    public GameObject itemObj;
}

[Serializable]
public class MiniTurnExLevelInGame : MiniTurn {

    [ToggleGroup("FirstProperty")]
    public bool FirstProperty = true;
    [ToggleGroup("FirstProperty")]
    [HideLabel]
    public EFirstProperty propertyFirst;

    [ToggleGroup("TweenGoToPointEvent")]
    public bool TweenGoToPointEvent;

    [ToggleGroup("TweenGoToPointEvent")]
    public DataTweenGoToPoint[] tweenGoTPoint;

    [ToggleGroup("SetNewPathEvent")]
    public bool SetNewPathEvent;

    [ToggleGroup("SetNewPathEvent")]
    [HideLabel]
    public AttrSetPath attrSetPath;

//    [ToggleGroup("Extras")]
//    public bool Extras;

//    [ToggleGroup("Extras")]
//    [HideLabel]
//    public ExtrasAction extra;

    [ToggleGroup("PositionEnemy")]
    public bool PositionEnemy = true;

    [ToggleGroup("PositionEnemy")]
    public List<Vector2> startPosition;

}
