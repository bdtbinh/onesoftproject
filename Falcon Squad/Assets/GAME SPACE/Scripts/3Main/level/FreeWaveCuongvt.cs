﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SkyGameKit.Demo
{
    public class FreeWaveCuongvt : SgkSingleton<FreeWaveCuongvt>
    {
        protected Dictionary<string, List<FreeTurnCuongvt>> allTurn = new Dictionary<string, List<FreeTurnCuongvt>>();
        protected override void Awake()
        {
            base.Awake();
            FreeTurnCuongvt[] freeTurnList = GetComponentsInChildren<FreeTurnCuongvt>();
            foreach (var freeTurn in freeTurnList)
            {
                if (allTurn.ContainsKey(freeTurn.name))
                {
                    allTurn[freeTurn.name].Add(freeTurn);
                }
                else
                {
                    allTurn.Add(freeTurn.name, new List<FreeTurnCuongvt>());
                    allTurn[freeTurn.name].Add(freeTurn);
                }
            }
        }

        public void StartTurn(string name)
        {
            if (allTurn.ContainsKey(name))
            {
                bool startOK = false;
                foreach (var turn in allTurn[name])
                {
                    if (!turn.IsRunning)
                    {
                        StartTurn(turn);
                        startOK = true;
                        break;
                    }
                }
                if (!startOK)
                {
                    Debug.LogError("Hết turn " + name + " tạo thêm turn mới đi");
                }
            }
            else
            {
                Debug.LogError("Không có turn " + name);
            }
        }

        public void StartTurn(FreeTurnCuongvt turn)
        {
            turn.StartCountDelayTurn();
        }

        public void StartTurn(string name, Transform target)
        {
            if (allTurn.ContainsKey(name))
            {
                bool startOK = false;
                foreach (var turn in allTurn[name])
                {
                    if (!turn.IsRunning)
                    {
                        StartTurn(turn, target);
                        startOK = true;
                        break;
                    }
                }
                if (!startOK)
                {
                    Debug.LogError("Hết turn " + name + " tạo thêm turn mới đi");
                }
            }
            else
            {
                Debug.LogError("Không có turn " + name);
            }
        }

        public void StartTurn(FreeTurnCuongvt turn, Transform target)
        {
            turn.target = target;
            turn.StartCountDelayTurn();
        }
    }
}
