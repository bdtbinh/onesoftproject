﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class BonusNextWaveEvent : MonoBehaviour
{
    public UILabel lnumWave;
    public UILabel lResourcesBonusWave;

    int numResourcesBonusWave;
    void Start()
    {

        //GameContex.modeGamePlay = GameContex.ModeGamePlay.EndLess;
        //Debug.LogError(GameContex.modeGamePlay);
        Debug.Log("<color=#ff33cc>" + "modeGamePlay: " + GameContext.modeGamePlay + "</color>");

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            LevelManager.OnNextWave += OnNextWave;
        }
        else
        {
            gameObject.SetActive(false);
        }

    }

    private void OnDisable()
    {

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            LevelManager.OnNextWave -= OnNextWave;
        }
    }

    int numWave;
    void OnNextWave(int numWaveOrigi)
    {
        numWave = numWaveOrigi + 1;
        //Debug.LogError("numWave " + numWave);
        if (numWave > 0)
        {
            if (numWave >= NextWaveResourcesEventSheet.GetDictionary().Count)
            {
                numWave = NextWaveResourcesEventSheet.GetDictionary().Count - 1;
            }
            numResourcesBonusWave = NextWaveResourcesEventSheet.Get(numWave).NumResourcesGet;

            lnumWave.gameObject.SetActive(true);
            lResourcesBonusWave.gameObject.SetActive(true);

            lnumWave.text = "wave " + numWave;
            lResourcesBonusWave.text = "+ " + numResourcesBonusWave;

            //PanelUITop.Instance.numResourcesBonusEvent += numResourcesBonusWave;
            PanelUITop.Instance.numWaveEventPassed = numWave;
            //PanelUITop.Instance.SetTextResourcesEvents(numResourcesBonusWave);


            //MinhCacheGame.AddEventResources(GameContext.EVENT_XMAS_2018, numResourcesBonusWave);

            this.Delay(1f, () =>
            {
                //lnumWave.gameObject.SetActive(false);
                lResourcesBonusWave.gameObject.SetActive(false);
            });
        }
        else
        {
            lnumWave.gameObject.SetActive(false);
            lResourcesBonusWave.gameObject.SetActive(false);
        }
    }
}
