﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector;
using SkyGameKit;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GetDataShopToMain : Singleton<GetDataShopToMain>
{
    //mainWeapon
    [DisplayAsString]
    public int mainWeaponPower;
    [DisplayAsString]
    public int mainWeaponQuantity;
    [DisplayAsString]
    public float mainWeaponSpeed;


    //  superWeapon
    [DisplayAsString]
    public int laserPower;
    [DisplayAsString]
    public int missilePower;
    [DisplayAsString]
    public int windSlashPower;
    [DisplayAsString]
    public float superWeaponDuration;


    //LIFE
    [DisplayAsString]
    public int lifeQuantity;

    //Gold
    [DisplayAsString]
    public int goldDropChance;

    //
    //wingman
    //[DisplayAsString]
    //public int wingmanPower;
    [DisplayAsString]
    public int powerWingman1Galting;
    [DisplayAsString]
    public int powerWingman2AutoGalting;
    [DisplayAsString]
    public int powerWingman3Laser;
    [DisplayAsString]
    public int powerWingman4DouleGalting;
    [DisplayAsString]
    public int powerWingman5HomingMissile;
    //[DisplayAsString]
    //public string nameWingmanUse;


    //SecondaryWeapon
    [DisplayAsString]
    public int secondaryWeaponPower;
    [DisplayAsString]
    public string nameSecondaryWeaponUse;


    //Value Bonus khi doi? may bay
    [DisplayAsString]
    public string namePlaneUse;
    [DisplayAsString]
    int powerBonusPlane;
    [DisplayAsString]
    int lifeBonusPlane;



    //
    [DisplayAsString]
    public int totalPowerBonus;


    //
    //	[DisplayAsString]
    //	public int numItemPowerGet;


    void Awake()
    {
        ConfigLoader.Load();
        base.Awake();
        GameContext.numItemPowerGet = 0;
        GetDataItemBonus();
        GetValueBullet();
        ShowWingmanChoose();
        ShowSecondaryWeaponChoose();
    }

    public void GetDataItemBonus()
    {
        int playerSwitch = 1;


        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane && GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Aircraft)
        {

            playerSwitch = CacheGame.GetSpaceShipUserTry();
        }
        else
        {
            playerSwitch = CacheGame.GetSpaceShipUserSelected();
        }

        switch (playerSwitch)
        {
            case 1:
                namePlaneUse = "Plane1";
                break;
            case 2:
                namePlaneUse = "Plane2";
                powerBonusPlane = 1;
                break;
            case 3:
                namePlaneUse = "Plane3";
                powerBonusPlane = 2;
                lifeBonusPlane = 1;
                break;
            case 4:
                namePlaneUse = "Plane4Noel";
                powerBonusPlane = 1;
                lifeBonusPlane = 1;
                break;
            case 5:
                namePlaneUse = "Plane5Noel";
                powerBonusPlane = 2;
                lifeBonusPlane = 2;
                break;
            case 6:
                namePlaneUse = "Plane6";
                powerBonusPlane = 1;
                lifeBonusPlane = 1;
                break;
            case 7:
                namePlaneUse = "Plane7";
                powerBonusPlane = 3;
                lifeBonusPlane = 3;
                break;
            default:
                break;
        }
    }


    void ShowWingmanChoose()
    {
        //nameWingmanUse = "";

        powerWingman1Galting = (int)GetValueFromShop(GameContext.WINGMAN, GameContext.POWER, "Galting1");
        powerWingman2AutoGalting = (int)GetValueFromShop(GameContext.WINGMAN, GameContext.POWER, "AutoGalting2");
        powerWingman3Laser = (int)GetValueFromShop(GameContext.WINGMAN, GameContext.POWER, "Laser3");
        powerWingman4DouleGalting = (int)GetValueFromShop(GameContext.WINGMAN, GameContext.POWER, "DouleGalting4");
        powerWingman5HomingMissile = (int)GetValueFromShop(GameContext.WINGMAN, GameContext.POWER, "HomingMissile5");
        //Debug.LogError("powerWingman1Galting1:"+ powerWingman1Galting);

        //powerWingman1Galting = (int)(powerWingman1Galting * VipBonusValue.dameDroneRate);
        //powerWingman2AutoGalting = (int)(powerWingman2AutoGalting * VipBonusValue.dameDroneRate);
        //powerWingman3Laser = (int)(powerWingman3Laser * VipBonusValue.dameDroneRate);
        //powerWingman4DouleGalting = (int)(powerWingman4DouleGalting * VipBonusValue.dameDroneRate);
        //powerWingman5HomingMissile = (int)(powerWingman5HomingMissile * VipBonusValue.dameDroneRate);

    }




    void ShowSecondaryWeaponChoose()
    {
        //nameSecondaryWeaponUse = "";

        //int secondarySwitch = 0;
        //if (SceneManager.GetActiveScene().name == "LevelTryPlane" && GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.SecondaryWeapon)
        //{
        //    secondarySwitch = CacheGame.GetSecondaryWeaponUserTry();
        //    //tpe
        //}
        //else
        //{
        //    secondarySwitch = CacheGame.GetSecondaryWeaponSelected();
        //}
        ////Debug.LogError("secondarySwitch " + secondarySwitch);

        //switch (secondarySwitch)
        //{
        //    case 1:
        //        nameSecondaryWeaponUse = "BurstShot1";
        //        break;
        //    case 2:
        //        nameSecondaryWeaponUse = "Chainsaw2";
        //        break;
        //    case 3:
        //        nameSecondaryWeaponUse = "SuperSonic3";
        //        break;
        //    case 4:
        //        nameSecondaryWeaponUse = "LightingBolt4";
        //        break;
        //    default:
        //        break;
        //}

        ////Debug.LogError("secondaryWeaponPower " + GameContex.secondaryWeapon + GameContex.power + nameSecondaryWeaponUse);
        //if (nameSecondaryWeaponUse != "")
        //{
        //    secondaryWeaponPower = (int)GetValueFromShop(GameContext.SECONDARY_WEAPON, GameContext.POWER, nameSecondaryWeaponUse);
        //}
        //Debug.Log("secondaryWeaponPower " + nameSecondaryWeaponUse + ":" + secondaryWeaponPower);
    }


    //------------------------------------------------------------------------
    public void GetValueBullet()
    {
        totalPowerBonus = powerBonusPlane + CacheGame.numItemPowerUpUse;
        //-------- Main Weapon ------------------
        mainWeaponPower = (int)GetValueFromShop(GameContext.MAIN_WEAPON, GameContext.POWER, namePlaneUse, GameContext.numItemPowerGet, totalPowerBonus);
        //mainWeaponPower = (int)(mainWeaponPower * VipBonusValue.damePlaneRate);
        mainWeaponQuantity = (int)GetMainWeaponQuantity(GameContext.numItemPowerGet);
        mainWeaponSpeed = GetValueFromShop(GameContext.MAIN_WEAPON, GameContext.SPEED, namePlaneUse);

        //-------- Super Weapon ------------------
        laserPower = (int)GetValueFromShop(GameContext.LASER, GameContext.POWER);
        missilePower = (int)GetValueFromShop(GameContext.MISSILE, GameContext.POWER);
        windSlashPower = (int)GetValueFromShop(GameContext.WINDSLASH, GameContext.POWER);
        superWeaponDuration = GetValueFromShop(GameContext.SUPER_WEAPON, GameContext.DURATION);

        //		Debug.LogError ("mainWeaponSpeed " + mainWeaponSpeed + "superWeaponDuration " + superWeaponDuration);
        //		Debug.LogError ("mainWeaponPower " + mainWeaponPower + " mainWeaponQuantity " + mainWeaponQuantity);
        //		Debug.LogError ("laserPower " + laserPower + " missilePower " + missilePower + " windSlashPower " + windSlashPower + " superWeaponDuration " + superWeaponDuration);
        //-------- Life ------------------


        lifeQuantity = (int)GetValueFromShop(GameContext.LIFE, GameContext.QUANTITY) + lifeBonusPlane + CacheGame.numLifeUpUse;

        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            lifeQuantity += 2;
        }


        //if (CacheGame.GetFirtSessionGame() != 1)
        //{
        //    lifeQuantity = 10;
        //}


        // build cho chị Linh
        lifeQuantity = 199;

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            lifeQuantity = 3;
        }



        //-------- Gold ------------------
        goldDropChance = (int)GetValueFromShop(GameContext.GOLD, GameContext.DROP_CHANCE);

    }


    //---- bien getItem dung` khi nguoi` choi an them duoc item trong luc choi------------

    public float GetValueFromShop(string nameItem, string typeUpgrade, string namePlane = "", int triggerItemPowerDrop = 0, int valueBonusPlane = 0)
    {

        int indexGetValue = CacheGame.GetCurrentLvShopUpgrade(nameItem, typeUpgrade) + triggerItemPowerDrop + valueBonusPlane;
        //Debug.LogError(nameItem + typeUpgrade + GameContex.value + namePlane);
        //		indexGetValue = 10;
        ShopUpgradeConfig dataConfig = ShopUpgradeConfig.Get(nameItem + typeUpgrade + GameContext.VALUE + namePlane);
        //		Debug.LogError ("indexGetValueFromShop" + indexGetValue);
        if (indexGetValue >= dataConfig.list_PriceValue.Length)
        {
            indexGetValue = dataConfig.list_PriceValue.Length - 1;
        }
        float value = dataConfig.list_PriceValue[indexGetValue];

        return value;
    }




    //-----main weapon rieng vi` moi~ may' bay co data khac nhau


    //---- phai co them ham` nay` vi` power va` quatity chuyen? qua nang cap'  cung` luc' voi' nhau-----------
    public float GetMainWeaponQuantity(int triggerItemPowerDrop = 0)
    {

        int indexGetValue = CacheGame.GetCurrentLvShopUpgrade(GameContext.MAIN_WEAPON, GameContext.POWER) + triggerItemPowerDrop + totalPowerBonus;

        //		Debug.LogError ("indexGetValue" + indexGetValue);
        ShopUpgradeConfig dataConfig = ShopUpgradeConfig.Get(GameContext.MAIN_WEAPON + GameContext.QUANTITY + GameContext.VALUE + namePlaneUse);

        //		Debug.LogError ("indexGetMainWeaponQuantity" + indexGetValue);
        if (indexGetValue >= dataConfig.list_PriceValue.Length)
        {
            indexGetValue = dataConfig.list_PriceValue.Length - 1;
        }

        float value = dataConfig.list_PriceValue[indexGetValue];

        return value;

    }
}
