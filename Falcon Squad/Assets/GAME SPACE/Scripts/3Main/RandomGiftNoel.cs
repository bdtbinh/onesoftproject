﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class RandomGiftNoel : MonoBehaviour
{
	[DisplayAsString]
	public string nameGiftGet = "Gold";
	[DisplayAsString]
	public int numGiftGet = 100;

	public ListGiftNoel[] listGiftNoel;


	float valueRamdomDropGift;


	[System.Serializable]
	public class ListGiftNoel
	{
		[Tooltip ("ty le roi ra Gift")]
		public float rateGetGift;
		public string nameGift;
		public int numGift;

	}

	void Start ()
	{
		
	}

	float rateSock;

	public void RandomTypeGiftNoel ()
	{

		valueRamdomDropGift = (Random.Range (0, 1000) / 10f);

		for (int i = 0; i < listGiftNoel.Length; i++) {
			rateSock += listGiftNoel [i].rateGetGift;

			if ((valueRamdomDropGift <= rateSock) && (listGiftNoel [i].rateGetGift != 0)) {

				nameGiftGet = listGiftNoel [i].nameGift;
				numGiftGet = listGiftNoel [i].numGift;
				break;
			}

		}

		rateSock = 0;
		SetTypeGift ();

	}

	void SetTypeGift ()
	{
		switch (nameGiftGet) {
		case "Gold":
			PanelUITop.Instance.SetTextCoin (numGiftGet);
			break;
		case "Power-Up":
			CacheGame.SetTotalItemPowerUp (CacheGame.GetTotalItemPowerUp () + numGiftGet);
			break;
		case "Life":
			CacheGame.SetTotalItemLife (CacheGame.GetTotalItemLife () + numGiftGet);
			break;
		case "EMP":
			CacheGame.SetTotalItemActiveSkill (CacheGame.GetTotalItemActiveSkill () + numGiftGet);
			break;
		}
	}
}
