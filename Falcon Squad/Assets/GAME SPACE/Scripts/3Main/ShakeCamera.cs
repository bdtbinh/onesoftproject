﻿using UnityEngine;
using System.Collections;
using SkyGameKit;
using DG.Tweening;

public class ShakeCamera : SkyGameKit.Singleton<ShakeCamera>
{
	//	public tk2dCamera m_gameCamera;
	public GameObject bgShakeObj;

	private Tweener m_camTweener;
	public float SHAKE_DURATION = 0.12f;

	public Vector3 SHAKE_STR = new Vector3 (25f, 25f, 0f);

	public int SHAKE_VIBRATO = 50;

	public float SHAKE_RANDOMNESS = 100f;

	public Ease SHAKE_EASE = Ease.OutQuad;

	public GameObject fxLighObj;
	TweenAlpha fxLighObjTween;
	SpriteRenderer fxLighObjSpr;

	void Start ()
	{

		fxLighObj.transform.localScale = new Vector2 ((MainScene.Instance.posAnchorRight.localPosition.x * 2) + 0.1f, (MainScene.Instance.posAnchorTop.localPosition.y * 2) + 0.1f);
		fxLighObjTween = fxLighObj.GetComponent<TweenAlpha> ();
		fxLighObjSpr = fxLighObj.GetComponent<SpriteRenderer> ();

//		this.Delay (3f, () => {
//			DoShake ();
//		});
	}



	public void DoShake ()
	{

		if (this.m_camTweener != null && this.m_camTweener.IsPlaying ()) {
			this.m_camTweener.Complete (true);
		}
		fxLighObj.SetActive (true);

		this.m_camTweener = this.bgShakeObj.transform.DOShakePosition (this.SHAKE_DURATION, this.SHAKE_STR, this.SHAKE_VIBRATO, this.SHAKE_RANDOMNESS, false, true).SetUpdate<Tweener> (true).SetEase<Tweener> (this.SHAKE_EASE).OnComplete (() => {

			fxLighObjTween.enabled = true;
			fxLighObjTween.ResetToBeginning ();

			this.Delay (0.5f, () => {
				fxLighObjTween.enabled = false;
				fxLighObjSpr.color = Color.white;
				fxLighObj.SetActive (false);
			});
//			this.Delay (4f, () => {
//				DoShake ();
//			});

		});
		;
	}
}