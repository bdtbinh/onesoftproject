﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;

public class Boss1Collision : EnemyCollisionBase
{
    private Boss1Action boss1Obj;
    private CheckEnemyInScene checkEnemyInScene;
    SpriteRenderer spr;

    void Awake()
    {
        boss1Obj = GetComponent<Boss1Action>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();

        spr = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            boss1Obj.Die(EnemyKilledBy.DeadZone);
        }
    }



    public override void TakeDamage(int damage, bool fromServer = false)
    {
        Debug.Log("boss 1 TakeDamage");
        if (boss1Obj == null)
        {
            boss1Obj = GetComponent<Boss1Action>();
        }
        boss1Obj.currentHP -= damage;
        PanelBossHealbar.Instance.ChangeBossHealbar(boss1Obj.currentHP, boss1Obj.maxHP);
        if (boss1Obj.currentHP <= 0)
        {
            boss1Obj.bossDamged = true;
        }
        if ((boss1Obj.currentHP <= boss1Obj.maxHP / 3) && !boss1Obj.bossDamged)
        {
            PoolExplosiveSmall(spr);
            boss1Obj.GetComponent<CircleCollider2D>().enabled = false;
            boss1Obj.SetNextPath();
            boss1Obj.bossDamged = true;
        }
        else
        {
            if (boss1Obj.currentHP <= 0)
            {
                if (!boss1Obj.bossFx)
                {
                    boss1Obj.bossFx = true;
                    PanelBossHealbar.Instance.HideBossHealbar();
                    boss1Obj.GetComponent<CircleCollider2D>().enabled = false;
                    boss1Obj.M_SplineMove.Stop();
                    Pool_EffectDie();
                    if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign || GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
                    {
                        MainScene.Instance.bossDead = true;
                    }
                    boss1Obj.boss1LaserGun.StopShootLaserWhenDie();
                    //
                    this.Delay(1.8f, () =>
                    {
                        boss1Obj.GetComponent<CircleCollider2D>().enabled = true;
                        if (!fromServer)
                        {
                            boss1Obj.Die(EnemyKilledBy.Player);
                        }
                        else
                        {
                            boss1Obj.Die(EnemyKilledBy.Server);
                        }
                    });
                }
            }
            else
            {
                PoolExplosiveSmall(spr);
                SoundManager.SoundBullet_Enemy();
            }
        }
        base.TakeDamage(damage, fromServer);
    }


    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            if (checkEnemyInScene.enemyInScreen)
            {
                TakeDamage(laserPower);
            }
        }
    }



    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.bossDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.bossDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
