﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;

public class Boss3Collision : EnemyCollisionBase
{
    private Boss3Action boss3Obj;
    private CheckEnemyInScene checkEnemyInScene;
    public SpriteRenderer spr;
    Transform transCache, transParent;

    void Awake()
    {
        transCache = transform;
        transParent = transCache.parent;

        boss3Obj = GetComponent<Boss3Action>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            boss3Obj.Die(EnemyKilledBy.DeadZone);
        }
    }



    public override void TakeDamage(int damage, bool fromServer = false)
    {
        Debug.Log("boss 3 TakeDamage");
        if (boss3Obj == null)
        {
            boss3Obj = GetComponent<Boss3Action>();
        }
        if (!boss3Obj.shieldIsActive)
        {
            boss3Obj.currentHP -= damage;
            PanelBossHealbar.Instance.ChangeBossHealbar(boss3Obj.currentHP, boss3Obj.maxHP);

            if (boss3Obj.currentHP <= 0)
            {
                if (!boss3Obj.bossFx)
                {
                    boss3Obj.bossFx = true;
                    PanelBossHealbar.Instance.HideBossHealbar();
                    boss3Obj.GetComponent<CircleCollider2D>().enabled = false;
                    boss3Obj.M_SplineMove.Stop();
                    Pool_EffectDie();
                    if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign || GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
                    {
                        MainScene.Instance.bossDead = true;
                    }
                    this.Delay(1.8f, () =>
                    {
                        boss3Obj.GetComponent<CircleCollider2D>().enabled = true;
                        if (!fromServer)
                        {
                            boss3Obj.Die(EnemyKilledBy.Player);
                        }
                        else
                        {
                            boss3Obj.Die(EnemyKilledBy.Server);
                        }
                    });
                }
            }
            else
            {
                PoolExplosiveSmall(spr);
                SoundManager.SoundBullet_Enemy();
            }
            base.TakeDamage(damage, fromServer);
        }

    }




    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            if (checkEnemyInScene.enemyInScreen)
            {
                TakeDamage(laserPower);
            }
        }
    }


    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.bossDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.bossDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }



}
