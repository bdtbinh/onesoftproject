﻿using UnityEngine;
using UniRx;
using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;
using SkyGameKit;
using PathologicalGames;

public class Boss4SpiderSilkDespawn : MonoBehaviour
{

    // Use this for initialization
    public ParticleSystem particleSystemIns;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "DeadZoneBulletHell")
        {
            if (PoolManager.Pools[Const.explosiveName].IsSpawned(transform))
            {
                PoolManager.Pools[Const.explosiveName].Despawn(transform);
            }
        }
    }
    void OnEnable()
    {
        particleSystemIns.Play(true);
        Despawn();
    }

    private void Despawn()
    {
        this.Delay(5f, () =>
        {
            particleSystemIns.Stop();
            if (PoolManager.Pools[Const.explosiveName].IsSpawned(transform))
            {
                PoolManager.Pools[Const.explosiveName].Despawn(transform);
            }
        });
    }
}
