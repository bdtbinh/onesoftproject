﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;

public class BossAction : EnemyData
{
    public bool bossDamged = false;
    public bool bossFx = false;
    public PathManager[] listPathRandom;
    EnemyDropItem enemyDropItem;

    //CircleCollider2D circleCollider2DIns;

    private void Awake()
    {
        ChangeDataByDifficult();
        enemyDropItem = GetComponent<EnemyDropItem>();
    }
    protected void Start()
    {
        //circleCollider2DIns = GetComponent<CircleCollider2D>();
    }

    //
    float valueAddHp;
    void ChangeDataByDifficult()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            switch (CacheGame.GetDifficultCampaign())
            {
                case "Normal":
                    valueAddHp = DataRemoteEnemy.Instance.HP_Normal;
                    break;
                case "Hard":
                    valueAddHp = DataRemoteEnemy.Instance.HP_Hard;
                    break;
                case "Hell":
                    valueAddHp = DataRemoteEnemy.Instance.HP_Hell;
                    break;
                default:
                    break;
            }


            maxHP = (int)(maxHP * valueAddHp);
            //---
            double coeff = 1.0;
            if (PvpUtil.dicLevelToCoeff.TryGetValue(CacheGame.GetCurrentLevel(), out coeff) != null)
            {
                maxHP = (int)(maxHP * coeff);
            }
            currentHP = maxHP;
        }

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
        {
            valueAddHp = DataRemoteEnemy.Instance.HP_Noel;
            maxHP = (int)(maxHP * valueAddHp);
            currentHP = maxHP;
        }

    }


    void OnDisable()
    {
        StopAllCoroutines();
    }

    //--------Warning--------------------------------

    public void WarningBoss()
    {
        //if (EffectList.Instance.warningBoss != null)
        //{
        //    Transform warningBossIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.warningBoss, new Vector3(0f, 3.6f, 0f), Quaternion.identity);
        //    warningBossIns.GetComponent<ParticleSystem>().Play(true);

        //}
        if (EffectList.Instance.warningBoss != null)
        {

            EffectList.Instance.warningBoss.SetActive(true);
            EffectList.Instance.warningBoss.GetComponent<ParticleSystem>().Play(true);
            this.Delay(1.5f, () =>
            {
                EffectList.Instance.warningBoss.SetActive(false);
            }, true);
        }

        M_SplineMove.Pause();

        this.Delay(1.8f, () =>
        {
            M_SplineMove.Resume();
        });
    }


    //------------------------ DropItem --------------------------------------------------------------

    public void DropItem()
    {
        enemyDropItem.Drop();
    }
    //------------------------ SetNextPath --------------------------------------------------------------
    public void SetNextPath()
    {
        M_SplineMove.Stop();
        M_SplineMove.moveToPath = true;
        PathManager nextPath = listPathRandom[Random.Range(0, listPathRandom.Length)];
        M_SplineMove.SetPath(nextPath);
    }


    public override void Restart()
    {
        base.Restart();
        bossDamged = false;
        bossFx = false;
        PanelBossHealbar.Instance.ShowBossHealbar(maxHP);
    }


    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        if (PoolManager.Pools[Const.enemyPoolName].IsSpawned(transform))
        {
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
            {
                (LevelManager.Instance.GetCurrentWave() as WaveManager).gameObject.SetActive(false);
                LevelManager.Instance.EndLevel();
            }

            if (type == EnemyKilledBy.Player)
            {
                PanelUITop.Instance.SetTextScore(score);
                MainScene.Instance.numEnemyKilled++;
                SoundManager.SoundBossDie();
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillBoss.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillBoss.ToString()) + 1);
                ////daily
                CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillBossEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillBossEasy.ToString()) + 1);
                CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillBossHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillBossHard.ToString()) + 1);
            }

            if (gameObject.tag != "Hazzard" && !MainScene.Instance.gameFinished)
            {
                MainScene.Instance.totalEnemyCanDie++;
            }
        }
        base.Die(type);
    }
}
