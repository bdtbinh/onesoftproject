﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;
using SkyGameKit.Demo;


public class Boss7Action : BossAction
{


    public Boss7MainWeanponCenter boss7MainWeanponCenter;
    public Boss7MainWeanponLeftRight boss7MainWeanponLeft, boss7MainWeanponRight;
    public Boss7MainWeanponHead boss7MainWeanponHead;


    void Start()
    {
        base.Start();
    }


    void OnDisable()
    {
        StopAllCoroutines();
    }


    //	------------------------ Shoot MainWeaPon Center--------------------------------------------------------------

    bool isShootMainWeaponCenter;

    public void ShootMainWeaponCenter(int timeDelay)
    {
        if (!isShootMainWeaponCenter)
        {
            isShootMainWeaponCenter = true;
            boss7MainWeanponCenter.ShootMainWeaponStep1();
            StartCoroutine("ShootMainWeaponCenterLoop", timeDelay);
        }
    }
    IEnumerator ShootMainWeaponCenterLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss7MainWeanponCenter.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }

    //	------------------------ Shoot MainWeaPon 2 bên trái phải  --------------------------------------------------------------

    bool isShootGunLeftRight;

    public void ShootMainWeaponLeftRight(int timeDelay2Shoot)
    {
        if (!isShootGunLeftRight)
        {
            isShootGunLeftRight = true;
            boss7MainWeanponLeft.ShootMainWeaponStep1();
            boss7MainWeanponRight.ShootMainWeaponStep1();
            StartCoroutine("ShootMainWeaponLeftRightLoop", timeDelay2Shoot);
        }
    }

    IEnumerator ShootMainWeaponLeftRightLoop(int timeDelay2Shoot)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay2Shoot);
            if (gameObject.activeInHierarchy)
            {
                boss7MainWeanponLeft.ShootMainWeaponStep1();
                boss7MainWeanponRight.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }


    //------------------------------ boss 7 bắn ra đạn vòng tròn ở trên đầu -----------------

    bool isShootMainWeaponHead;
    public void ShootMainWeaponHead(int timeDelay)
    {
        if (!isShootMainWeaponHead)
        {
            isShootMainWeaponHead = true;
            boss7MainWeanponHead.ShootMainWeaponStep1();
            StartCoroutine("ShootMainWeaponHeadLoop", timeDelay);
        }
    }

    IEnumerator ShootMainWeaponHeadLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss7MainWeanponHead.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }



    public override void Restart()
    {
        base.Restart();

    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);

        isShootMainWeaponCenter = false;
        isShootMainWeaponHead = false;
        isShootGunLeftRight = false;

    }

}
