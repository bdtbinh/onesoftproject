﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwoDLaserPack;
using Sirenix.OdinInspector;
using SkyGameKit;

public class Boss5LaserGun : MonoBehaviour
{
    public bool thisLaserSetBlink = true;
    public ParticleSystem effectLaser;
    public Boss5Action bossAction;
    Animator animLaserGun;

    public SpriteBasedLaser spriteBasedLaser;

    [ShowInInspector]
    private float timeLaserShow = 5f;

    public TweenPosition tweenLaserTarget;


    public float xTweenLaserTargetStep1 = 2f;
    public float xTweenLaserTargetStep2 = 8f;

    Vector3 posTweenStart = new Vector3(3f, -16f, 0f);

    void Start()
    {
        animLaserGun = GetComponent<Animator>();
        spriteBasedLaser.OnLaserHitTriggered += LaserOnOnLaserHitTriggered;
        spriteBasedLaser.SetLaserState(false);
    }


    void ChangeTweenTargetPos()
    {
        if (bossAction.bossDamged)
        {
            tweenLaserTarget.to = new Vector3(xTweenLaserTargetStep2, -16f, 0f);
        }
    }

    private void OnDisable()
    {
        tweenLaserTarget.to = new Vector3(xTweenLaserTargetStep1, -16f, 0f);
    }

    IEnumerator StartShootLaser()
    {

        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootLaserStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    public void ShootLaserStep1()
    {


        animLaserGun.SetTrigger("isAttack1");

        effectLaser.gameObject.SetActive(true);
        effectLaser.Play(true);
        ChangeTweenTargetPos();
        //ShootLaserStep2();
        StartCoroutine("ShootLaserStep2");
    }


    IEnumerator ShootLaserStep2()
    {
        yield return new WaitForSeconds(3f);
        //    public void ShootLaserStep2()
        //{

        //this.Delay(3f, () =>
        //{
        animLaserGun.SetTrigger("isAttack2");
        if (gameObject.activeInHierarchy)
        {
            spriteBasedLaser.SetLaserState(true);

            effectLaser.Stop();
            effectLaser.gameObject.SetActive(false);

            this.Delay(0.5f, () =>
            {
                tweenLaserTarget.enabled = true;
                tweenLaserTarget.ResetToBeginning();
            });

        }
        else
        {
            StopAllCoroutines();
        }
        //ShootLaserStep3();
        StartCoroutine("ShootLaserStep3");
        //});

    }


    IEnumerator ShootLaserStep3()
    {
        yield return new WaitForSeconds(timeLaserShow);
        //    public void ShootLaserStep3()
        //{

        //this.Delay(timeLaserShow, () =>
        //{
        if (gameObject.activeInHierarchy)
        {
            spriteBasedLaser.SetLaserState(false);

            animLaserGun.SetTrigger("isAttack3");

            tweenLaserTarget.enabled = false;
            tweenLaserTarget.transform.localPosition = posTweenStart;


            if (thisLaserSetBlink)
            {
                bossAction.M_SplineMove.Pause();
                bossAction.tweenAlphaBlinkDau.enabled = true;
                bossAction.tweenAlphaBlinkDau.ResetToBeginning();
                bossAction.tweenAlphaBlinkDuoi.enabled = true;
                bossAction.tweenAlphaBlinkDuoi.ResetToBeginning();
                this.Delay(0.5f, () =>
                {
                    bossAction.transform.localPosition = bossAction.GetPoinBlink();
                    this.Delay(0.5f, () =>
                    {
                        bossAction.SetNextPath();
                        bossAction.tweenAlphaBlinkDau.enabled = false;
                        bossAction.tweenAlphaBlinkDau.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                        bossAction.tweenAlphaBlinkDuoi.enabled = false;
                        bossAction.tweenAlphaBlinkDuoi.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                    });
                });

                spriteBasedLaser.transform.rotation = Quaternion.Euler(0, 0, -81f);
            }
            else
            {
                spriteBasedLaser.transform.rotation = Quaternion.Euler(0, 0, -99f);
            }

        }
        else
        {
            StopAllCoroutines();
        }
        //});
    }

    public void ChangeAnim_Atk_To_Idle()
    {
        animLaserGun.SetTrigger("isIdle");
    }

    public void FinishTweenShootLaser()
    {

        StartCoroutine("StopShootLaser");


    }

    IEnumerator StopShootLaser()
    {
        yield return new WaitForSeconds(1f);
        if (gameObject.activeInHierarchy)
        {
            spriteBasedLaser.SetLaserState(false);

            animLaserGun.SetTrigger("isLaserIdle");
        }
        else
        {
            StopAllCoroutines();
        }
    }



    public void StopShootLaserWhenDie()
    {
        spriteBasedLaser.SetLaserState(false);
        StopAllCoroutines();
    }


    private void LaserOnOnLaserHitTriggered(RaycastHit2D hitInfo)
    {
        if (hitInfo.collider.tag == "player" && !MainScene.Instance.gameFinished && !hitInfo.collider.GetComponent<Aircraft>().aircraftIsReloading && !hitInfo.collider.GetComponent<Aircraft>().aircraftUsingActiveShield && !MainScene.Instance.gameStopping && !MainScene.Instance.bossDead)
        {
            //hitInfo.collider.GetComponent<PlayerCollision>().Pool_EffectPlayerDie();
            //hitInfo.collider.GetComponent<PlayerController>().PlayerDie();

            hitInfo.collider.GetComponent<Aircraft>().playerControllerFalconScript.PlayerDie();
        }
    }

}
