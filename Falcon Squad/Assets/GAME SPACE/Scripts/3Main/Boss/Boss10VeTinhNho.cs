﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss10VeTinhNho : MonoBehaviour
{
    public ParticleSystem[] effectStep1Gun;
    //
    //Animator anim;

    //public UbhShotCtrl[] gunCtrlList;

    public Boss10Action bossActionObj;
    public UbhShotCtrl[] gunCtrlListPhase1;
    public UbhShotCtrl[] gunCtrlListPhase2;
    UbhShotCtrl[] gunCtrlListUse;



    IEnumerator StartShootTest()
    {
        ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    private void OnDisable()
    {
        foreach (ParticleSystem fx in effectStep1Gun)
        {
            fx.gameObject.SetActive(false);
        }
    }

    //
    public void ShootMainWeaponStep1()
    {

        //Debug.LogError("ShootMainWeaponStep1 ");

        //anim.SetTrigger("isAttack1");
        foreach (ParticleSystem fx in effectStep1Gun)
        {
            fx.gameObject.SetActive(true);
            fx.Play(true);
        }

        ShootMainWeaponStep2();
    }

    public void ShootMainWeaponStep2()
    {
        //Debug.LogError("ShootMainWeaponStep2");
        //anim.SetTrigger("isAttack2");

        this.Delay(2f, () =>
        {
            if (gameObject.activeInHierarchy)
            {
                if (!bossActionObj.bossDamged)
                {
                    gunCtrlListUse = gunCtrlListPhase1;
                }
                else
                {
                    gunCtrlListUse = gunCtrlListPhase2;
                }
                foreach (UbhShotCtrl gun in gunCtrlListUse)
                {
                    gun.StartShotRoutine();
                }
            }
            //ShootMainWeaponStep3();
        });

    }

    public void ShootMainWeaponStep3()
    {
        //this.Delay(2f, () =>
        //{
        foreach (UbhShotCtrl gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }
        foreach (ParticleSystem fx in effectStep1Gun)
        {
            fx.Stop();
            fx.gameObject.SetActive(false);
        }

        //});
        //Debug.LogError("ShootMainWeaponStep3");
    }


    public void ChangeAnim_Atk_To_Idle()
    {
        //anim.SetTrigger("isIdle");

        //Debug.LogError("isIdle");
    }
    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        foreach (UbhShotCtrl gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }

    }
}
