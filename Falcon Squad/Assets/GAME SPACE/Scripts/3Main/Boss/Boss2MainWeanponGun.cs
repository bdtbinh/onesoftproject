﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss2MainWeanponGun : MonoBehaviour
{
	public ParticleSystem effectStep1Gun;
	// Use this for initialization
	//
	public Boss2Action boss2ActionIns;
	UbhShotCtrl[] listGunCtrlUse;
	public TypeGunMainWeapon[] listTypeGun;

	[Serializable]
	public class TypeGunMainWeapon
	{
		public UbhShotCtrl[] oneTypeGun;
	}

	//
	int valueChoose;
	int valueChooseOld;



	IEnumerator StartShootTest ()
	{
		ShootMainWeaponStep1 ();
		while (true) {
			yield return new WaitForSeconds (8);
			if (gameObject.activeInHierarchy) {
				ShootMainWeaponStep1 ();
			} else {
				StopAllCoroutines ();
				break;
			}
		}

	}


	public void  GetTypeGunToShoot ()
	{
		while (true) {
			valueChoose = UnityEngine.Random.Range (0, listTypeGun.Length);

			if (valueChoose != valueChooseOld) {
				valueChooseOld = valueChoose;
				break;
			}
		}
		listGunCtrlUse = listTypeGun [valueChoose].oneTypeGun;
	}


	//
	public void ShootMainWeaponStep1 ()
	{
		effectStep1Gun.gameObject.SetActive (true);
		effectStep1Gun.Play (true);
		//
		StartCoroutine ("ShootMainWeaponStep2");
	}

	IEnumerator ShootMainWeaponStep2 ()
	{
		//		Debug.LogError ("ShootMainWeaponStep2 1");
		GetTypeGunToShoot ();
		yield return new WaitForSeconds (2.8f);
		boss2ActionIns.M_SplineMove.Pause ();
		boss2ActionIns.boss2Pausing = true;
		if (gameObject.activeInHierarchy) {
			foreach (UbhShotCtrl gun in listGunCtrlUse) {
				gun.StartShotRoutine ();
			}
		} 
	}

	public void ShootMainWeaponStep3 ()
	{
		foreach (UbhShotCtrl gun in listGunCtrlUse) {
			gun.StopShotRoutine ();
		}
		this.Delay (1f, () => {
			boss2ActionIns.M_SplineMove.Resume ();
			boss2ActionIns.boss2Pausing = false;
			effectStep1Gun.Stop ();
			effectStep1Gun.gameObject.SetActive (false);
		});
	}

	public void FinishTweenShootMainWeapon ()
	{
		StopShootMainWeapon ();
	}

	void StopShootMainWeapon ()
	{
		foreach (UbhShotCtrl gun in listGunCtrlUse) {
			gun.StopShotRoutine ();
		}
	}
}
