﻿using UnityEngine;
using UniRx;
using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;
using SkyGameKit;
using PathologicalGames;

public class Boss4BulletSpiderSilk : MonoBehaviour
{

    // Use this for initialization


    UbhBullet ubhBullet;
    public float timeExistSick = 5f;
    void Start()
    {
        ubhBullet = GetComponent<UbhBullet>();
    }


    private void OnEnable()
    {
        SetSick();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == Const.deadZone)
        {
            if (PoolManager.Pools[Const.explosiveName].IsSpawned(transform))
            {
                PoolManager.Pools[Const.explosiveName].Despawn(transform);

            }
        }
    }

    //viên đạn bay  1 lúc sẽ dừng lại và bung thành tơ nhện
    void SetSick()
    {

        float timepPauseBullet = UnityEngine.Random.RandomRange(0.2f, 0.8f);
        this.Delay(timepPauseBullet, () =>
        {
            if (ubhBullet != null)
            {
                Pool_Boss4SpiderSilk(transform);
                UbhObjectPool.instance.ReleaseBullet(ubhBullet);
            }

        });
    }


    public void Pool_Boss4SpiderSilk(Transform bulletTrans)
    {
        if (EffectList.Instance.fxBoss4SpiderSilk != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.fxBoss4SpiderSilk, bulletTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
