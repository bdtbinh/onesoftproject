﻿using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss10VeTinhTo : MonoBehaviour
{
    public Boss10Action bossActionObj;

    public TweenRotation tweenRotation;

    public float intDurationNormal = 8f;
    public float intDurationFast = 4f;
    public float timneRotationFast = 2f;

    public GameObject objVetinh1, objVetinh2, objVetinh3, objVetinh4;
    public GameObject[] fxRotationFastList;



    private void OnEnable()
    {
        ShowVeTinhStart();
    }

    private void OnDisable()
    {
        objVetinh1.SetActive(false);
        objVetinh2.SetActive(false);
        objVetinh3.SetActive(false);
        objVetinh4.SetActive(false);

        tweenRotation.duration = intDurationNormal;
        foreach (GameObject fx in fxRotationFastList)
        {
            fx.SetActive(false);
        }
    }

    void ShowVeTinhStart()
    {
        this.Delay(3f, () =>
        {
            objVetinh1.SetActive(true);
            objVetinh2.SetActive(true);
        });
    }
    public void AddVeTinhPhare2()
    {
        objVetinh3.SetActive(true);
        objVetinh4.SetActive(true);
    }

    public void ChangeDurationRotation()
    {
        tweenRotation.duration = intDurationFast;

        foreach (GameObject fx in fxRotationFastList)
        {
            fx.SetActive(true);
        }
        this.Delay(timneRotationFast, () =>
        {
            tweenRotation.duration = intDurationNormal;
            foreach (GameObject fx in fxRotationFastList)
            {
                fx.SetActive(false);
            }
        });
    }

}
