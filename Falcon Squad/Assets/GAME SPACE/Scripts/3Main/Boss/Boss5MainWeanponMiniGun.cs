﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss5MainWeanponMiniGun : MonoBehaviour
{
    //public ParticleSystem effectStep1Gun;
    // Use this for initialization
    //
    Animator anim;
    //public Enemy_MuiTenXanh_Rotate gunFollowPlayer;

    //public Boss3Action boss3ActionIns;
    //UbhShotCtrl[] listGunCtrlUse;
    //public TypeGunMainWeapon[] listTypeGun;

    public UbhShotCtrl[] gunCtrlList;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    IEnumerator StartShootTest()
    {
        ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }



    //
    public void ShootMainWeaponStep1()
    {

        //Debug.LogError("ShootMainWeaponStep1 ");

        anim.SetTrigger("isAttack1");
        //effectStep1Gun.gameObject.SetActive(true);
        //effectStep1Gun.Play(true);
    }

    public void ShootMainWeaponStep2()
    {
        //Debug.LogError("ShootMainWeaponStep2");
        anim.SetTrigger("isAttack2");

        if (gameObject.activeInHierarchy)
        {
            foreach (UbhShotCtrl gun in gunCtrlList)
            {
                gun.StartShotRoutine();
            }
        }

    }

    public void ShootMainWeaponStep3()
    {
        anim.SetTrigger("isAttack3");

        //this.Delay(1f, () =>
        //{
        //    if (boss3ActionIns.shieldIsActive)
        //    {
        //        gunFollowPlayer.isStartFollow = true;
        //    }

        //});

        foreach (UbhShotCtrl gun in gunCtrlList)
        {
            gun.StopShotRoutine();
        }
        //effectStep1Gun.Stop();
        //effectStep1Gun.gameObject.SetActive(false);
        //Debug.LogError("ShootMainWeaponStep3");

        //this.Delay(1f, () =>
        //{
        //    boss3ActionIns.M_SplineMove.Resume();
        //    boss3ActionIns.boss2Pausing = false;

        //});
    }


    public void ChangeAnim_Atk_To_Idle()
    {
        anim.SetTrigger("isIdle");

        //Debug.LogError("isIdle");
    }
    public void FinishTweenShootMainWeapon()
    {

        StopShootMainWeapon();


    }

    void StopShootMainWeapon()
    {
        //		Debug.LogError ("FinishTweenShootMainWeapon 1");
        foreach (UbhShotCtrl gun in gunCtrlList)
        {
            gun.StopShotRoutine();
        }

    }
}
