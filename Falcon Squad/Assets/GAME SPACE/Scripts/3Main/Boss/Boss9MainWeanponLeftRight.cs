﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss9MainWeanponLeftRight : MonoBehaviour
{
    //
    public Animator[] animMissileList;
    //public Boss9Action bossActionObj;
    public UbhShotCtrl[] gunCtrlListUse;


    public static int tyleShootRandomInBullet;

    IEnumerator StartShootTest()
    {
        ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }

    //
    public void ShootMainWeaponStep1()
    {
        //Debug.LogError("ShootMainWeaponStep1 ");
        foreach (Animator anim in animMissileList)
        {
            anim.SetTrigger("isAttack1");
        }
        //effectStep1Gun.gameObject.SetActive(true);
        //effectStep1Gun.Play(true);
        ShootMainWeaponStep2();
    }

    public void ShootMainWeaponStep2()
    {
        //Debug.LogError("ShootMainWeaponStep2");

        this.Delay(0.6f, () =>
        {
            foreach (Animator anim in animMissileList)
            {
                anim.SetTrigger("isAttack2");
            }
            if (gameObject.activeInHierarchy)
            {
                tyleShootRandomInBullet = UnityEngine.Random.Range(1, 3);
                foreach (UbhShotCtrl gun in gunCtrlListUse)
                {
                    gun.StartShotRoutine();
                }
            }
            //ShootMainWeaponStep3();
        });
    }

    public void ShootMainWeaponStep3()
    {
        foreach (Animator anim in animMissileList)
        {
            anim.SetTrigger("isAttack3");
        }
        foreach (UbhShotCtrl gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }

        this.Delay(0.6f, () =>
        {
            ChangeAnim_Atk_To_Idle();
        });
        //effectStep1Gun.Stop();
        //effectStep1Gun.gameObject.SetActive(false);
        //Debug.LogError("ShootMainWeaponStep3");

    }


    public void ChangeAnim_Atk_To_Idle()
    {
        foreach (Animator anim in animMissileList)
        {
            anim.SetTrigger("isIdle");
        }

        //Debug.LogError("isIdle");
    }
    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        //		Debug.LogError ("FinishTweenShootMainWeapon 1");
        foreach (UbhShotCtrl gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }

    }
}
