﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwoDLaserPack;
using UnityEngine.SceneManagement;

public class Boss1LaserGun : MonoBehaviour
{
    public ParticleSystem effectLaser;
    public Boss1Action boss1Action;
    Animator animLaser;
    int leftRightLaser;

    Vector3 laserToLeft, laserToRight;

    void Start()
    {
        animLaser = GetComponent<Animator>();

        foreach (SpriteBasedLaser spriteBasedLaserScript in boss1Action.spriteBasedLaserList)
        {
            spriteBasedLaserScript.OnLaserHitTriggered += LaserOnOnLaserHitTriggered;
            spriteBasedLaserScript.SetLaserState(false);
        }

        laserToLeft = new Vector3(0f, 0f, 90f);
        laserToRight = new Vector3(0f, 0f, -90f);
    }



    IEnumerator StartShootLaser()
    {

        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootLaserStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    public void ShootLaserStep1()
    {

        //		tweenRotUbhShot.enabled = true;
        //		tweenRotUbhShot.ResetToBeginning ();

        //Debug.LogError("ShootLaserStep1 ");
        animLaser.SetTrigger("isLaserAttack2");
        effectLaser.gameObject.SetActive(true);
        effectLaser.Play(true);
        StartCoroutine("ShootLaserStep2");
    }

    IEnumerator ShootLaserStep2()
    {
        //Debug.LogError("ShootLaserStep2 1");
        yield return new WaitForSeconds(3f);
        //Debug.LogError("ShootLaserStep2 2");
        if (gameObject.activeInHierarchy)
        {

            effectLaser.Stop();
            effectLaser.gameObject.SetActive(false);
            foreach (SpriteBasedLaser spriteBasedLaserScript in boss1Action.spriteBasedLaserList)
            {
                spriteBasedLaserScript.SetLaserState(true);
            }
            leftRightLaser++;

            //Debug.LogError("leftRightLaser: "+ leftRightLaser);

            if (leftRightLaser % 2 == 0)
            {
                boss1Action.tweenRotLaser.to = laserToLeft;
            }
            else
            {
                boss1Action.tweenRotLaser.to = laserToRight;
            }

            boss1Action.tweenRotLaser.enabled = true;
            boss1Action.tweenRotLaser.ResetToBeginning();
        }
        else
        {
            StopAllCoroutines();
        }
    }

    public void FinishTweenShootLaser()
    {

        StartCoroutine("StopShootLaser");
    }

    IEnumerator StopShootLaser()
    {
        //Debug.LogError("StopShootLaser 1");
        yield return new WaitForSeconds(1f);
        //Debug.LogError("StopShootLaser 2");
        if (gameObject.activeInHierarchy)
        {

            foreach (SpriteBasedLaser spriteBasedLaserScript in boss1Action.spriteBasedLaserList)
            {
                spriteBasedLaserScript.SetLaserState(false);
            }

            animLaser.SetTrigger("isLaserIdle");

            if (!boss1Action.bossDamged)
            {
                boss1Action.M_SplineMove.Resume();
            }

            boss1Action.tweenRotLaser.transform.localRotation = Quaternion.Euler(Vector3.zero);
            boss1Action.tweenRotLaser.enabled = false;

        }
        else
        {
            StopAllCoroutines();
        }
    }


    public void StopShootLaserWhenDie()
    {
        foreach (SpriteBasedLaser spriteBasedLaserScript in boss1Action.spriteBasedLaserList)
        {
            spriteBasedLaserScript.SetLaserState(false);
        }
        StopAllCoroutines();
    }


    private void LaserOnOnLaserHitTriggered(RaycastHit2D hitInfo)
    {
        //		Debug.LogError ("LaserOnOnLaserHitTriggered Player");

        if (hitInfo.collider.tag == "player" && !MainScene.Instance.gameFinished && !hitInfo.collider.GetComponent<Aircraft>().aircraftIsReloading && !hitInfo.collider.GetComponent<Aircraft>().aircraftUsingActiveShield && !MainScene.Instance.gameStopping && !MainScene.Instance.bossDead)
        {
            //hitInfo.collider.GetComponent<PlayerCollision> ().Pool_EffectPlayerDie ();
            //hitInfo.collider.GetComponent<PlayerController> ().PlayerDie ();
            if (SceneManager.GetActiveScene().name == "LevelTutorial")
            {
                hitInfo.collider.GetComponent<Aircraft>().playerInitScript.ChangeNumberLife(-9999);
            }
            hitInfo.collider.GetComponent<Aircraft>().playerControllerFalconScript.PlayerDie();
        }
    }


}
