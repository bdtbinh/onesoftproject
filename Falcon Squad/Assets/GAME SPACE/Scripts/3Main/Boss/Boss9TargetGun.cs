﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss9TargetGun : MonoBehaviour
{
    public ParticleSystem effectStep1Gun;
    //
    public Boss9Action bossActionObj;
    //public UbhShotCtrl[] gunCtrlList;

    public UbhShotCtrl[] gunCtrlListPhase1;
    public UbhShotCtrl[] gunCtrlListPhase2;
    UbhShotCtrl[] gunCtrlListUse;


    IEnumerator StartShootTest()
    {
        ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    private void OnDisable()
    {
        effectStep1Gun.gameObject.SetActive(false);
    }

    //
    public void ShootMainWeaponStep1()
    {

        //Debug.LogError("ShootMainWeaponStep1 ");

        //anim.SetTrigger("isAttack1");
        effectStep1Gun.gameObject.SetActive(true);
        effectStep1Gun.Play(true);
        ShootMainWeaponStep2();
    }

    public void ShootMainWeaponStep2()
    {
        //Debug.LogError("ShootMainWeaponStep2");
        //anim.SetTrigger("isAttack2");

        this.Delay(1.36f, () =>
        {
            if (!bossActionObj.bossDamged)
            {
                gunCtrlListUse = gunCtrlListPhase1;
            }
            else
            {
                gunCtrlListUse = gunCtrlListPhase2;
            }
            if (gameObject.activeInHierarchy)
            {
                foreach (UbhShotCtrl gun in gunCtrlListUse)
                {
                    gun.StartShotRoutine();
                }
            }
            //ShootMainWeaponStep3();
        });

    }

    public void ShootMainWeaponStep3()
    {
        foreach (UbhShotCtrl gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }
        this.Delay(1f, () =>
        {
            effectStep1Gun.Stop();
            effectStep1Gun.gameObject.SetActive(false);
        });

        //Debug.LogError("ShootMainWeaponStep3");

    }


    public void ChangeAnim_Atk_To_Idle()
    {
        //anim.SetTrigger("isIdle");

        //Debug.LogError("isIdle");
    }
    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        foreach (UbhShotCtrl gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }

    }
}
