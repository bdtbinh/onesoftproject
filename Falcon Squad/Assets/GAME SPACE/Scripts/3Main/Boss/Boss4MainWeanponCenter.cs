﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss4MainWeanponCenter : MonoBehaviour
{
    public ParticleSystem effectStep1Gun;
    // Use this for initialization
    //
    public Boss4Action boss4ActionIns;


    public UbhShotCtrl gunCtrlUse;
    public UbhNwayShot ubhNwayShot;

    //public TypeGunMainWeapon[] listTypeGun;

    //[Serializable]
    //public class TypeGunMainWeapon
    //{
    //    public UbhShotCtrl[] oneTypeGun;
    //}



    //
    int valueChoose;
    int valueChooseOld;

    void Start()
    {
        //		StartCoroutine ("StartShootTest");
        //listGunCtrlUse = listTypeGun[0].oneTypeGun;
        //SetNumBullet();
        ubhNwayShot.m_bulletNum = 2;
    }

    IEnumerator StartShootTest()
    {
        ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }


    public void SetNumBullet()
    {
        if (boss4ActionIns.bossDamged)
        {
            ubhNwayShot.m_bulletNum = 4;
        }
        else
        {
            ubhNwayShot.m_bulletNum = 2;
        }
    }


    //
    public void ShootMainWeaponStep1()
    {

        //Debug.LogError("ShootMainWeaponStep1 ");

        //effectStep1Gun.gameObject.SetActive(true);
        //effectStep1Gun.Play(true);
        //
        StartCoroutine("ShootMainWeaponStep2");
    }

    IEnumerator ShootMainWeaponStep2()
    {

        yield return new WaitForSeconds(1.1f);
        if (gameObject.activeInHierarchy)
        {
            gunCtrlUse.StartShotRoutine();
        }

    }

    public void ShootMainWeaponStep3()
    {
        //Debug.LogError("ShootMainWeaponStep3 ");

        gunCtrlUse.StartShotRoutine();

        //effectStep1Gun.Stop();
        //effectStep1Gun.gameObject.SetActive(false);

    }

    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        //		Debug.LogError ("FinishTweenShootMainWeapon 1");
        gunCtrlUse.StartShotRoutine();
    }
}
