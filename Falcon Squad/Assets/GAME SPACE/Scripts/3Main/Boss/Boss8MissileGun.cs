﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using Sirenix.OdinInspector;

public class Boss8MissileGun : MonoBehaviour
{
    //	public ParticleSystem effectStep1Gun;
    Animator anim;
    public Animator[] animMissileList;
    [DisplayAsString]
    public float timeDelayResumeMove = 0.36f;
    public Boss8Action bossActionObj;
    //
    public UbhShotCtrl[] listGunCtrlUse;


    public void ShootMainWeaponStep1()
    {
        //		Debug.LogError ("ShootMainWeaponStep1 ");
        foreach (Animator anim in animMissileList)
        {
            anim.SetTrigger("isAttack1");
        }
        StartCoroutine("ShootMainWeaponStep2");
    }

    IEnumerator ShootMainWeaponStep2()
    {
        //		Debug.LogError ("ShootMainWeaponStep2 1");

        yield return new WaitForSeconds(0.5f);

        foreach (Animator anim in animMissileList)
        {
            anim.SetTrigger("isAttack2");
        }

        if (gameObject.activeInHierarchy)
        {
            foreach (UbhShotCtrl gun in listGunCtrlUse)
            {
                gun.StartShotRoutine();
            }
        }

    }

    public void ShootMainWeaponStep3()
    {
        foreach (Animator anim in animMissileList)
        {
            anim.SetTrigger("isAttack3");
        }
        foreach (UbhShotCtrl gun in listGunCtrlUse)
        {
            gun.StopShotRoutine();
        }
        StartCoroutine("ChangeAnim_Atk_To_Idle");
    }

    IEnumerator ChangeAnim_Atk_To_Idle()
    {
        yield return new WaitForSeconds(0.5f);
        //anim.SetTrigger("isIdle");
        foreach (Animator anim in animMissileList)
        {
            anim.SetTrigger("isIdle");
        }
        //Debug.LogError("isIdle");
    }

    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        //		Debug.LogError ("FinishTweenShootMainWeapon 1");
        foreach (UbhShotCtrl gun in listGunCtrlUse)
        {
            gun.StopShotRoutine();
        }

    }
}
