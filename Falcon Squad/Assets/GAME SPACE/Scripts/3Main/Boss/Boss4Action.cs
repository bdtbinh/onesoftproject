﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;
using SkyGameKit.Demo;


public class Boss4Action : BossAction
{

    public Animator animBoss4Head;

    public Boss4MainWeanponCenter boss4MainWeanponCenter;

    public Boss4MainWeanponLeftRight boss4MainWeanponLeft, boss4MainWeanponRight;
    public Boss4SpawnSpiderBaby boss4SpawnSpiderBaby;




    void Start()
    {
        base.Start();
    }



    void OnDisable()
    {
        StopAllCoroutines();
    }


    //	------------------------ Shoot MainWeaPon Center--------------------------------------------------------------

    bool isShootMainWeaponCenter;

    public void ShootMainWeaponCenter(int timeDelay)
    {
        if (!isShootMainWeaponCenter)
        {
            isShootMainWeaponCenter = true;
            boss4MainWeanponCenter.ShootMainWeaponStep1();
            StartCoroutine("ShootMainWeaponCenterLoop", timeDelay);
        }

    }

    IEnumerator ShootMainWeaponCenterLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss4MainWeanponCenter.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }


    //	------------------------ Shoot MainWeaPon 2 bên trái phải  --------------------------------------------------------------

    bool isShootGunLeftRight;

    public void ShootMainWeaponLeftRight(int timeDelay2Shoot)
    {
        if (!isShootGunLeftRight)
        {
            isShootGunLeftRight = true;
            boss4MainWeanponLeft.ShootMainWeaponStep1();
            boss4MainWeanponRight.ShootMainWeaponStep1();
            StartCoroutine("ShootMainWeaponLeftRightLoop", timeDelay2Shoot);
        }

    }

    IEnumerator ShootMainWeaponLeftRightLoop(int timeDelay2Shoot)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay2Shoot);
            if (gameObject.activeInHierarchy)
            {
                boss4MainWeanponLeft.ShootMainWeaponStep1();
                boss4MainWeanponRight.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }



    //------------------------------ boss 4 đẻ ra con nhện con -----------------
    public void SpawnBabyEnemy(int timeDelaySpawn)
    {

        StartCoroutine("SpawnBabyEnemyLoop", timeDelaySpawn);
    }


    IEnumerator SpawnBabyEnemyLoop(int timeDelaySpawn)
    {
        //FreeWave.Instance.StartTurn("Baby", transform);
        boss4SpawnSpiderBaby.ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(timeDelaySpawn);
            if (gameObject.activeInHierarchy)
            {
                boss4SpawnSpiderBaby.ShootMainWeaponStep1();
            }
            else
            {
                break;
            }
        }

    }




    public override void Restart()
    {
        base.Restart();
        boss4MainWeanponCenter.ubhNwayShot.m_bulletNum = 2;
        //boss4Damged = false;

    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);

        isShootMainWeaponCenter = false;
        isShootGunLeftRight = false;
    }



}
