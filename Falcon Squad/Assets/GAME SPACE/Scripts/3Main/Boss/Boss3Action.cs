﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;

public class Boss3Action : BossAction
{

    public Animator animBoss3;
    public Boss3MainWeanponCenter boss3MainWeanponCenter;
    public Boss3MainWeanponLeftRight boss3MainWeanponLeft, boss3MainWeanponRight;

    [Title("Thời gian delay giữa 2 súng trái phải", bold: true, horizontalLine: true)]
    [ShowInInspector]
    private float timeBetweenGunLeft_Right = 1.6f;

    [Title("Thời gian Shield biến mất", bold: true, horizontalLine: true)]
    [ShowInInspector]
    private float timeHideShield = 6f;
    public bool shieldIsActive;
    public Boss3ShieldController shieldController;


    void Start()
    {
        base.Start();
    }


    void OnDisable()
    {
        StopAllCoroutines();
    }


    //	------------------------ Shoot MainWeaPon Center--------------------------------------------------------------

    bool isShootMainWeaponCenter;

    public void ShootMainWeaponCenter(int timeDelay)
    {
        if (!isShootMainWeaponCenter)
        {
            isShootMainWeaponCenter = true;
            boss3MainWeanponCenter.ShootMainWeaponStep1();
            StartCoroutine("ShootMainWeaponCenterLoop", timeDelay);
        }
    }

    IEnumerator ShootMainWeaponCenterLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                if (shieldIsActive)
                {
                    boss3MainWeanponCenter.ShootMainWeaponStep1();
                }
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }


    //	------------------------ Shoot MainWeaPon 2 bên trái phải  --------------------------------------------------------------

    bool isShootGunLeftRight;

    public void ShootMainWeaponLeftRight(int timeDelay2Shoot)
    {
        if (!isShootGunLeftRight)
        {
            isShootGunLeftRight = true;
            boss3MainWeanponLeft.ShootMainWeaponStep1();
            this.Delay(timeBetweenGunLeft_Right, () =>
            {
                boss3MainWeanponRight.ShootMainWeaponStep1();
            });
            StartCoroutine("ShootMainWeaponLeftRightLoop", timeDelay2Shoot);
        }

    }

    IEnumerator ShootMainWeaponLeftRightLoop(int timeDelay2Shoot)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay2Shoot);
            if (gameObject.activeInHierarchy)
            {
                if (shieldIsActive)
                {
                    boss3MainWeanponLeft.ShootMainWeaponStep1();
                    this.Delay(timeBetweenGunLeft_Right, () =>
                    {
                        if (shieldIsActive)
                        {
                            boss3MainWeanponRight.ShootMainWeaponStep1();
                        }
                    });
                }
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }


    //------------------------------ StartFollowPlayer dành cho 2 súng 2 bên -----------------

    bool isFollowed;

    public void GunFollowPlayer()
    {
        if (!isFollowed)
        {
            boss3MainWeanponLeft.gunFollowPlayer.isStartFollow = true;
            boss3MainWeanponLeft.gunFollowPlayer.GetTranformParent();
            boss3MainWeanponRight.gunFollowPlayer.isStartFollow = true;
            boss3MainWeanponRight.gunFollowPlayer.GetTranformParent();
            isFollowed = true;
        }
    }

    //------------------------------ Bật tắt Shield -----------------

    public void Boss3Shield_Hide()
    {
        if (shieldController.shieldCore1.isShieldCoreDie && shieldController.shieldCore2.isShieldCoreDie && shieldController.shieldCore3.isShieldCoreDie)
        {
            shieldController.gameObject.SetActive(false);
            shieldIsActive = false;
            M_SplineMove.Pause();
            animBoss3.SetTrigger("isStun");
            //
            boss3MainWeanponLeft.gunFollowPlayer.isStartFollow = false;
            boss3MainWeanponRight.gunFollowPlayer.isStartFollow = false;

            this.Delay(timeHideShield, () =>
            {
                M_SplineMove.Resume();
                animBoss3.SetTrigger("isIdle");

                boss3MainWeanponLeft.gunFollowPlayer.isStartFollow = true;
                boss3MainWeanponRight.gunFollowPlayer.isStartFollow = true;

                shieldController.gameObject.SetActive(true);
                shieldIsActive = true;
                shieldController.shieldCore1.ResetData();
                shieldController.shieldCore2.ResetData();
                shieldController.shieldCore3.ResetData();


            });
        }
    }


    //}

    public override void Restart()
    {
        base.Restart();
    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);
        isShootMainWeaponCenter = false;
        isShootGunLeftRight = false;
        isFollowed = false;

        boss3MainWeanponLeft.gunFollowPlayer.isStartFollow = true;
        boss3MainWeanponRight.gunFollowPlayer.isStartFollow = true;

        shieldController.gameObject.SetActive(true);
        shieldIsActive = true;
        shieldController.shieldCore1.ResetData();
        shieldController.shieldCore2.ResetData();
        shieldController.shieldCore3.ResetData();
    }



}
