﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;

public class Boss4Collision : EnemyCollisionBase
{
    private Boss4Action boss4Obj;
    private CheckEnemyInScene checkEnemyInScene;
    public SpriteRenderer spr;
    Transform transCache, transParent;

    void Awake()
    {
        transCache = transform;
        transParent = transCache.parent;

        boss4Obj = GetComponent<Boss4Action>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            boss4Obj.Die(EnemyKilledBy.DeadZone);
        }
    }



    public override void TakeDamage(int damage, bool fromServer = false)
    {
        Debug.Log("boss 4 TakeDamage");
        if (boss4Obj == null)
        {
            boss4Obj = GetComponent<Boss4Action>();
        }
        boss4Obj.currentHP -= damage;
        PanelBossHealbar.Instance.ChangeBossHealbar(boss4Obj.currentHP, boss4Obj.maxHP);

        if ((boss4Obj.currentHP <= boss4Obj.maxHP / 3) && !boss4Obj.bossDamged)
        {
            boss4Obj.bossDamged = true;
            boss4Obj.boss4MainWeanponCenter.ubhNwayShot.m_bulletNum = 4;
            boss4Obj.animBoss4Head.SetTrigger("isForm2");
        }

        if (boss4Obj.currentHP <= 0)
        {
            if (!boss4Obj.bossFx)
            {
                boss4Obj.bossFx = true;
                PanelBossHealbar.Instance.HideBossHealbar();
                boss4Obj.GetComponent<CircleCollider2D>().enabled = false;
                boss4Obj.M_SplineMove.Stop();
                Pool_EffectDie();
                if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign || GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
                {
                    MainScene.Instance.bossDead = true;
                }
                this.Delay(1.8f, () =>
                {
                    boss4Obj.GetComponent<CircleCollider2D>().enabled = true;
                    if (!fromServer)
                    {
                        boss4Obj.Die(EnemyKilledBy.Player);
                    }
                    else
                    {
                        boss4Obj.Die(EnemyKilledBy.Server);
                    }
                });
            }
        }
        else
        {
            PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
        }
        base.TakeDamage(damage, fromServer);
    }






    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            if (checkEnemyInScene.enemyInScreen)
            {
                TakeDamage(laserPower);
            }
        }
    }


    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.bossDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.bossDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }


}
