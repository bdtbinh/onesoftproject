﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;

public class Boss9Action : BossAction
{

    public Boss9TargetGun boss9TargetGun;
    public Boss9MainWeanponCenter boss9MainWeanponCenter;
    public Boss9MainWeanponLeftRight boss9MainWeanponLeftRight;
    public Boss9MainWeanponRau boss9MainWeanponRau;


    void Start()
    {
        base.Start();
    }


    //	------------------------ TargetDuoi --------------------------------------------------------------

    bool isShootTargetDuoi;

    public void ShootTargetDuoi(int timeDelay)
    {
        if (!isShootTargetDuoi)
        {
            isShootTargetDuoi = true;
            boss9TargetGun.ShootMainWeaponStep1();
            StartCoroutine("ShootTargetDuoiLoop", timeDelay);
        }

    }

    IEnumerator ShootTargetDuoiLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss9TargetGun.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    //	------------------------ short bong bóng ở râu --------------------------------------------------------------


    bool isShootRau;
    public void ShootRau(int timeDelay)
    {
        if (!isShootRau)
        {
            isShootRau = true;
            boss9MainWeanponRau.ShootMainWeaponStep1();
            StartCoroutine("ShootRauLoop", timeDelay);
        }

    }

    IEnumerator ShootRauLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss9MainWeanponRau.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    //	------------------------ Shoot MainWeanponCenter --------------------------------------------------------------


    bool isShootMainWeanponCenter;

    public void ShootMainWeanponCenter(int timeDelay)
    {
        if (!isShootMainWeanponCenter)
        {
            isShootMainWeanponCenter = true;
            boss9MainWeanponCenter.ShootMainWeaponStep1();
            StartCoroutine("ShootMainWeanponCenterLoop", timeDelay);
        }

    }

    IEnumerator ShootMainWeanponCenterLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss9MainWeanponCenter.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    //	------------------------ Shoot MainWeanponLeftRight --------------------------------------------------------------


    bool isShootMainWeanponLeftRight;

    public void ShootMainWeanponLeftRight(int timeDelay)
    {
        if (!isShootMainWeanponLeftRight)
        {
            isShootMainWeanponLeftRight = true;
            boss9MainWeanponLeftRight.ShootMainWeaponStep1();
            StartCoroutine("ShootMainWeanponLeftRightLoop", timeDelay);
        }

    }

    IEnumerator ShootMainWeanponLeftRightLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss9MainWeanponLeftRight.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }




    public override void Restart()
    {
        base.Restart();
    }



    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);
        isShootMainWeanponCenter = false;
        isShootTargetDuoi = false;
        isShootMainWeanponLeftRight = false;
        isShootRau = false;
    }


}
