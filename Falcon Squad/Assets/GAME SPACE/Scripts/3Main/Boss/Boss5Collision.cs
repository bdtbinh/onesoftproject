﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;

public class Boss5Collision : EnemyCollisionBase
{
    private Boss5Action bossActionObj;
    private CheckEnemyInScene checkEnemyInScene;
    public SpriteRenderer spr;
    Transform transCache, transParent;

    void Awake()
    {
        transCache = transform;
        transParent = transCache.parent;

        bossActionObj = GetComponent<Boss5Action>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();
    }

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            bossActionObj.Die(EnemyKilledBy.DeadZone);
        }
    }


    public override void TakeDamage(int damage, bool fromServer = false)
    {
        if (bossActionObj == null)
        {
            bossActionObj = GetComponent<Boss5Action>();
        }
        bossActionObj.currentHP -= damage;
        PanelBossHealbar.Instance.ChangeBossHealbar(bossActionObj.currentHP, bossActionObj.maxHP);

        if ((bossActionObj.currentHP <= bossActionObj.maxHP / 3) && !bossActionObj.bossDamged)
        {
            bossActionObj.bossDamged = true;
        }

        if (bossActionObj.currentHP <= 0)
        {
            if (!bossActionObj.bossFx)
            {
                bossActionObj.bossFx = true;
                PanelBossHealbar.Instance.HideBossHealbar();
                bossActionObj.GetComponent<CircleCollider2D>().enabled = false;
                bossActionObj.M_SplineMove.Stop();
                //PanelUITop.Instance.SetTextScore(bossActionObj.score);
                Pool_EffectDie();
                if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign || GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
                {
                    MainScene.Instance.bossDead = true;
                }
                bossActionObj.boss5LaserGunLeft.StopShootLaserWhenDie();
                bossActionObj.boss5LaserGunRight.StopShootLaserWhenDie();

                this.Delay(1.8f, () =>
                {
                    bossActionObj.GetComponent<CircleCollider2D>().enabled = true;
                    if (!fromServer)
                    {
                        bossActionObj.Die(EnemyKilledBy.Player);
                    }
                    else
                    {
                        bossActionObj.Die(EnemyKilledBy.Server);
                    }
                });
            }
        }
        else
        {
            PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
        }
        base.TakeDamage(damage, fromServer);
    }



    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            if (checkEnemyInScene.enemyInScreen)
            {
                TakeDamage(laserPower);
            }
        }
    }


    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.bossDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.bossDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

}
