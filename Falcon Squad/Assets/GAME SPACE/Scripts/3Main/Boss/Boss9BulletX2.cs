﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss9BulletX2 : MonoBehaviour
{
    UbhBullet ubhBulletIns;

    public UbhShotCtrl[] gunCtrlListUse;
    public float timeDelayShoot = 1f;

    void Start()
    {
        //		StartCoroutine ("StartShootTest");
        ubhBulletIns = gameObject.GetComponent<UbhBullet>();
    }

    //float t1, t2, t3;
    //float tyleShootRandom;
    private void OnEnable()
    {
        SetShootFromBullet();
    }

    private void OnDisable()
    {
        //ubhBulletIns.m_shooting = true;
        //tweenBulletRotation.enabled = false;
        StopMainWeapon();
    }


    void SetShootFromBullet()
    {
        //float timepPauseBullet = UnityEngine.Random.RandomRange(0.2f, 0.4f);
        this.Delay(timeDelayShoot, () =>
        {
            ubhBulletIns.m_shooting = false;
            ShootMainWeapon();

            UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
        });
    }

    //
    public void ShootMainWeapon()
    {
        if (gameObject.activeInHierarchy)
        {

            gunCtrlListUse[Boss9MainWeanponLeftRight.tyleShootRandomInBullet - 1].StartShotRoutine();
        }
    }


    public void StopMainWeapon()
    {
        //if (gameObject.activeInHierarchy)
        //{
        foreach (UbhShotCtrl gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }
        //}
    }

}
