﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss9MainWeanponCenter : MonoBehaviour
{
    //
    Animator anim;
    public Boss9Action bossActionObj;
    public UbhShotCtrl[] gunCtrlListPhase1;
    public UbhShotCtrl[] gunCtrlListPhase2;
    UbhShotCtrl[] gunCtrlListUse;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    IEnumerator StartShootTest()
    {
        ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    //
    public void ShootMainWeaponStep1()
    {

        //Debug.LogError("ShootMainWeaponStep1 ");
        anim.SetTrigger("isAttack1");
        //effectStep1Gun.gameObject.SetActive(true);
        //effectStep1Gun.Play(true);
    }

    public void ShootMainWeaponStep2()
    {
        //Debug.LogError("ShootMainWeaponStep2");
        anim.SetTrigger("isAttack2");

        if (gameObject.activeInHierarchy)
        {
            if (!bossActionObj.bossDamged)
            {
                gunCtrlListUse = gunCtrlListPhase1;
            }
            else
            {
                gunCtrlListUse = gunCtrlListPhase2;
            }
            foreach (UbhShotCtrl gun in gunCtrlListUse)
            {
                gun.StartShotRoutine();
            }
        }

    }

    public void ShootMainWeaponStep3()
    {
        anim.SetTrigger("isAttack3");

        foreach (UbhShotCtrl gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }
        //effectStep1Gun.Stop();
        //effectStep1Gun.gameObject.SetActive(false);
        //Debug.LogError("ShootMainWeaponStep3");

    }


    public void ChangeAnim_Atk_To_Idle()
    {
        anim.SetTrigger("isIdle");

        //Debug.LogError("isIdle");
    }
    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        //		Debug.LogError ("FinishTweenShootMainWeapon 1");
        foreach (UbhShotCtrl gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }

    }
}
