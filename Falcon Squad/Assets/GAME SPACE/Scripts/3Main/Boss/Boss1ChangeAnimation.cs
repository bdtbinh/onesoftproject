﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1ChangeAnimation : MonoBehaviour
{
	Boss1Action boss1Action;

	public GameObject effectForm2;

	void Start ()
	{
		boss1Action = GetComponentInParent<Boss1Action> ();
	}

    private void OnDisable()
    {
        effectForm2.SetActive(false);
    }



    public void Form1_To_Form2_Complete ()
	{
		effectForm2.SetActive (true);
		boss1Action.M_SplineMove.Resume ();
		boss1Action.animBoss1.SetTrigger ("isForm2");
		boss1Action.GetComponent<CircleCollider2D> ().enabled = true;
		boss1Action.enemyTransforming = false;
	}
}
