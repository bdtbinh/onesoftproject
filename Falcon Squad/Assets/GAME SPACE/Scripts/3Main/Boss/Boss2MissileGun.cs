﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using Sirenix.OdinInspector;

public class Boss2MissileGun : MonoBehaviour
{
	//	public ParticleSystem effectStep1Gun;
	[DisplayAsString]
	public float timeDelayResumeMove = 0.36f;
	public Boss2Action boss2ActionIns;
	//
	public UbhShotCtrl[] listGunCtrlUse;

	
	public void ShootMainWeaponStep1 ()
	{

		//		Debug.LogError ("ShootMainWeaponStep1 ");

//		effectStep1Gun.gameObject.SetActive (true);
//		effectStep1Gun.Play (true);
		//
		StartCoroutine ("ShootMainWeaponStep2");
	}

	IEnumerator ShootMainWeaponStep2 ()
	{
		//		Debug.LogError ("ShootMainWeaponStep2 1");

		yield return new WaitForSeconds (0.1f);
		if (!boss2ActionIns.boss2Pausing) {
			boss2ActionIns.M_SplineMove.Pause ();
		}
//		boss2ActionIns.M_SplineMove.Pause ();
		if (gameObject.activeInHierarchy) {
			foreach (UbhShotCtrl gun in listGunCtrlUse) {
				gun.StartShotRoutine ();
			}
		} 

		this.Delay (timeDelayResumeMove, () => {
			ShootMainWeaponStep3 ();
		});

	}

	public void ShootMainWeaponStep3 ()
	{
		if (!boss2ActionIns.boss2Pausing) {
			boss2ActionIns.M_SplineMove.Resume ();
		}
		foreach (UbhShotCtrl gun in listGunCtrlUse) {
			gun.StopShotRoutine ();
		}
	}
}
