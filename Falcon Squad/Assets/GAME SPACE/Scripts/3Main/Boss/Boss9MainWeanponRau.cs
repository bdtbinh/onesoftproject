﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss9MainWeanponRau : MonoBehaviour
{
    public ParticleSystem[] effectStep1GunList;
    //

    public UbhShotCtrl[] gunCtrlList;



    IEnumerator StartShootTest()
    {
        ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    private void OnDisable()
    {
        foreach (ParticleSystem effectStep1Gun in effectStep1GunList)
        {
            effectStep1Gun.gameObject.SetActive(false);
        }
    }

    //
    public void ShootMainWeaponStep1()
    {

        //Debug.LogError("ShootMainWeaponStep1 ");

        //anim.SetTrigger("isAttack1");
        foreach (ParticleSystem effectStep1Gun in effectStep1GunList)
        {
            effectStep1Gun.gameObject.SetActive(true);
            effectStep1Gun.Play(true);
        }

        ShootMainWeaponStep2();
    }

    public void ShootMainWeaponStep2()
    {
        //Debug.LogError("ShootMainWeaponStep2");
        //anim.SetTrigger("isAttack2");

        this.Delay(2f, () =>
        {
            if (gameObject.activeInHierarchy)
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StartShotRoutine();
                }
            }
            //ShootMainWeaponStep3();
        });

    }

    public void ShootMainWeaponStep3()
    {
        this.Delay(0.3f, () =>
        {
            foreach (UbhShotCtrl gun in gunCtrlList)
            {
                gun.StopShotRoutine();
            }
            foreach (ParticleSystem effectStep1Gun in effectStep1GunList)
            {
                effectStep1Gun.Stop();
                effectStep1Gun.gameObject.SetActive(false);
            }
        });

        //Debug.LogError("ShootMainWeaponStep3");

    }


    public void ChangeAnim_Atk_To_Idle()
    {
        //anim.SetTrigger("isIdle");

        //Debug.LogError("isIdle");
    }
    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        foreach (UbhShotCtrl gun in gunCtrlList)
        {
            gun.StopShotRoutine();
        }

    }
}
