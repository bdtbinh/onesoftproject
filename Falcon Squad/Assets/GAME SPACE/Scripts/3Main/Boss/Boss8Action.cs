﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;

public class Boss8Action : BossAction
{
    public Boss8LaserGun boss8LaserGun;
    public Boss8MissileGun boss8MissileGun;
    public Boss8BoomerangGun boss8BoomerangGun;

    void Start()
    {
        base.Start();
    }

    //	------------------------ ShootLaser --------------------------------------------------------------


    bool isShootLaser;

    public void ShootLaser(int timeDelay)
    {
        if (!isShootLaser)
        {
            isShootLaser = true;
            boss8LaserGun.ShootLaserStep1();
            StartCoroutine("ShootLaserLoop", timeDelay);
        }

    }

    IEnumerator ShootLaserLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss8LaserGun.ShootLaserStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    //	------------------------ Shoot MissilleGun --------------------------------------------------------------


    bool isShootMissilleGun;

    public void ShootMissilleGun(int timeDelay)
    {
        if (!isShootMissilleGun)
        {
            isShootMissilleGun = true;
            boss8MissileGun.ShootMainWeaponStep1();
            StartCoroutine("ShootMissilleGunLoop", timeDelay);
        }

    }

    IEnumerator ShootMissilleGunLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss8MissileGun.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    //	------------------------ Shoot MissilleGun --------------------------------------------------------------


    bool isShootBoomerangGun;

    public void ShootBoomerangGun(int timeDelay)
    {
        if (!isShootBoomerangGun)
        {
            isShootBoomerangGun = true;
            boss8BoomerangGun.ShootMainWeaponStep1();
            StartCoroutine("ShootBoomerangGunLoop", timeDelay);
        }

    }

    IEnumerator ShootBoomerangGunLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss8BoomerangGun.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }




    public override void Restart()
    {
        base.Restart();
    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);
        isShootMissilleGun = false;
        isShootLaser = false;
        isShootBoomerangGun = false;
    }

}
