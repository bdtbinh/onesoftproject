﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;
using SkyGameKit.Demo;


public class Boss5Action : BossAction
{

    public Boss5MainWeanponCenter boss5MainWeanponCenter;

    public Boss5MainWeanponMiniGun boss5MainWeanponMiniGunLeft, boss5MainWeanponMiniGunRight;

    [ShowInInspector]
    private float timeDelayShootCenter = 1.6f;

    public Boss5LaserGun boss5LaserGunLeft, boss5LaserGunRight;

    public TweenAlpha tweenAlphaBlinkDau;
    public TweenAlpha tweenAlphaBlinkDuoi;
    //
    public Vector3[] listPoinBlink;


    void Start()
    {
        base.Start();
    }


    void OnDisable()
    {
        StopAllCoroutines();
    }



    //	------------------------ Shoot MainWeaPon Center--------------------------------------------------------------

    bool isShootMainWeaponCenter;

    public void ShootMainWeapon(int timeDelay)
    {
        if (!isShootMainWeaponCenter)
        {
            isShootMainWeaponCenter = true;
            boss5MainWeanponMiniGunLeft.ShootMainWeaponStep1();
            boss5MainWeanponMiniGunRight.ShootMainWeaponStep1();
            this.Delay(timeDelayShootCenter, () =>
            {
                boss5MainWeanponCenter.ShootMainWeaponStep1();
            });
            StartCoroutine("ShootMainWeaponLoop", timeDelay);
        }

    }

    IEnumerator ShootMainWeaponLoop(int timeDelay)
    {
        while (true)
        {

            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss5MainWeanponMiniGunLeft.ShootMainWeaponStep1();
                boss5MainWeanponMiniGunRight.ShootMainWeaponStep1();
                this.Delay(timeDelayShootCenter, () =>
                {
                    boss5MainWeanponCenter.ShootMainWeaponStep1();
                });
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }


    //	------------------------ Shoot Laser --------------------------------------------------------------

    bool isShootLaser;

    public void ShootLaser(int timeDelay)
    {
        if (!isShootLaser)
        {
            isShootLaser = true;
            boss5LaserGunLeft.ShootLaserStep1();
            boss5LaserGunRight.ShootLaserStep1();
            StartCoroutine("ShootLaserLoop", timeDelay);
        }

    }

    IEnumerator ShootLaserLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss5LaserGunLeft.ShootLaserStep1();
                boss5LaserGunRight.ShootLaserStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }


    //	------------------------ Get Poin blink  --------------------------------------------------------------

    public Vector3 GetPoinBlink()
    {
        M_SplineMove.Stop();
        M_SplineMove.moveToPath = true;

        Vector3 nextPath = listPoinBlink[Random.Range(0, listPoinBlink.Length)];

        return nextPath - transform.parent.localPosition;
    }


    

    public override void Restart()
    {
        base.Restart();

    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);


        isShootMainWeaponCenter = false;
        isShootLaser = false;
    }



}
