﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;

public class Boss10Action : BossAction
{

    public Boss10GunTarger boss10GunTarger;
    public Boss10VeTinhNho boss10VeTinhNho;
    public Boss10VeTinhTo boss10VeTinhTo;


    void Start()
    {
        base.Start();
    }


    //	------------------------ TargetDuoi --------------------------------------------------------------


    bool isShootTarget;

    public void ShootTarget(int timeDelay)
    {
        if (!isShootTarget)
        {
            isShootTarget = true;
            boss10GunTarger.ShootMainWeaponStep1();
            StartCoroutine("ShootTargetLoop", timeDelay);
        }
    }

    IEnumerator ShootTargetLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss10GunTarger.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }

    //	------------------------ Shoot VeTinhNho --------------------------------------------------------------

    bool isShootVeTinhNho;

    public void ShootVeTinhNho(int timeDelay)
    {
        if (!isShootVeTinhNho)
        {
            isShootVeTinhNho = true;
            boss10VeTinhNho.ShootMainWeaponStep1();
            StartCoroutine("ShootVeTinhNhoLoop", timeDelay);
        }
    }

    IEnumerator ShootVeTinhNhoLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss10VeTinhNho.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }

    //	------------------------ Shoot MainWeanponLeftRight --------------------------------------------------------------



    bool isChangeSpeedVeTinhTo;

    public void ChangeSpeedVeTinhTo(int timeDelay)
    {
        if (!isChangeSpeedVeTinhTo)
        {
            isChangeSpeedVeTinhTo = true;
            boss10VeTinhTo.ChangeDurationRotation();
            StartCoroutine("ChangeSpeedVeTinhToLoop", timeDelay);
        }
    }

    IEnumerator ChangeSpeedVeTinhToLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss10VeTinhTo.ChangeDurationRotation();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }

    public override void Restart()
    {
        base.Restart();
    }


    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);
        isShootVeTinhNho = false;
        isShootTarget = false;
        isChangeSpeedVeTinhTo = false;
    }

}
