﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwoDLaserPack;

public class Boss1MainWeanponGun : MonoBehaviour
{
	//	public ParticleSystem effectLaser;

	public Boss1Action boss1Action;
	public Animator[] animMainWeaponList;
	int leftRightLaser;

	//	Vector3 laserToLeft, laserToRight;



	IEnumerator StartShootMainWeapon ()
	{
		while (true) {
			yield return new WaitForSeconds (8);
			if (gameObject.activeInHierarchy) {
				//				ShootMainWeaponStep1 ();
			} else {
				StopAllCoroutines ();
				break;
			}
		}

	}

	public void ShootMainWeaponStep1 ()
	{

//		Debug.LogError ("ShootMainWeaponStep1 ");

		foreach (Animator animMainWeapon in animMainWeaponList) {
			animMainWeapon.SetTrigger ("isAttack1");
		}

		StartCoroutine ("ShootMainWeaponStep2");
	}

	IEnumerator ShootMainWeaponStep2 ()
	{
//		Debug.LogError ("ShootMainWeaponStep2 1");
		yield return new WaitForSeconds (2f);
//		Debug.LogError ("ShootMainWeaponStep2 2");

//		animMainWeapon.SetTrigger ("isAttack2");
		foreach (Animator animMainWeapon in animMainWeaponList) {
			animMainWeapon.SetTrigger ("isAttack2");
		}
		if (gameObject.activeInHierarchy) {
			foreach (UbhShotCtrl gun in boss1Action.gunCtrlList) {
				gun.StartShotRoutine ();
			}
			boss1Action.tweenRotUbhShot.enabled = true;
			boss1Action.tweenRotUbhShot.ResetToBeginning ();
		} else {
			StopAllCoroutines ();
			foreach (UbhShotCtrl gun in boss1Action.gunCtrlList) {
				gun.StopShotRoutine ();

			}
		}

	}

	public void FinishTweenShootMainWeapon ()
	{
		StopShootMainWeapon ();
	}

	void StopShootMainWeapon ()
	{
//		Debug.LogError ("FinishTweenShootMainWeapon 1");
		foreach (UbhShotCtrl gun in boss1Action.gunCtrlList) {
			gun.StopShotRoutine ();
		}
		boss1Action.tweenRotUbhShot.transform.localRotation = Quaternion.Euler (Vector3.zero);
		boss1Action.tweenRotUbhShot.enabled = false;
//		animMainWeapon.SetTrigger ("isIdle");
		foreach (Animator animMainWeapon in animMainWeaponList) {
			animMainWeapon.SetTrigger ("isIdle");
		}
	}









}
