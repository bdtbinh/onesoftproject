﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;
using SkyGameKit.Demo;

public class Boss4SpawnSpiderBaby : MonoBehaviour
{
    public ParticleSystem effectStep1Gun;
    // Use this for initialization
    //
    Animator anim;

    public Boss4Action boss4ActionIns;
    public Transform tranSpawnBaby;
    //UbhShotCtrl[] listGunCtrlUse;
    //public TypeGunMainWeapon[] listTypeGun;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    IEnumerator StartShootTest()
    {
        ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }

    //
    public void ShootMainWeaponStep1()
    {
        anim.SetTrigger("isAttack1");
        //effectStep1Gun.gameObject.SetActive(true);
        //effectStep1Gun.Play(true);
    }

    public void ShootMainWeaponStep2()
    {
        //Debug.LogError("ShootMainWeaponStep2");
        anim.SetTrigger("isAttack2");
        FreeWaveCuongvt.Instance.StartTurn("BabySpiderBoss4", tranSpawnBaby);
    }

    public void ShootMainWeaponStep3()
    {
        anim.SetTrigger("isAttack3");
    }


    public void ChangeAnim_Atk_To_Idle()
    {
        anim.SetTrigger("isIdle");
    }
    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        //		Debug.LogError ("FinishTweenShootMainWeapon 1");
    }
}
