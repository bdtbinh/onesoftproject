﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;

public class Boss2Collision : EnemyCollisionBase
{
    private Boss2Action boss2Obj;
    private CheckEnemyInScene checkEnemyInScene;
    SpriteRenderer spr;
    Transform transCache, transParent;

    void Awake()
    {
        transCache = transform;
        transParent = transCache.parent;

        boss2Obj = GetComponent<Boss2Action>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();
        spr = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            boss2Obj.Die(EnemyKilledBy.DeadZone);
        }
    }


    public override void TakeDamage(int damage, bool fromServer = false)
    {
        Debug.Log("boss 2 TakeDamage");
        if (boss2Obj == null)
        {
            boss2Obj = GetComponent<Boss2Action>();
        }
        boss2Obj.currentHP -= damage;
        PanelBossHealbar.Instance.ChangeBossHealbar(boss2Obj.currentHP, boss2Obj.maxHP);
        if (boss2Obj.currentHP <= 0)
        {
            if (!boss2Obj.bossFx)
            {
                boss2Obj.bossFx = true;
                PanelBossHealbar.Instance.HideBossHealbar();
                boss2Obj.GetComponent<CircleCollider2D>().enabled = false;
                boss2Obj.M_SplineMove.Stop();
                Pool_EffectDie();
                if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign || GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
                {
                    MainScene.Instance.bossDead = true;
                }
                this.Delay(1.8f, () =>
                {
                    boss2Obj.GetComponent<CircleCollider2D>().enabled = true;
                    if (!fromServer)
                    {
                        boss2Obj.Die(EnemyKilledBy.Player);
                    }
                    else
                    {
                        boss2Obj.Die(EnemyKilledBy.Server);
                    }
                });
            }
        }
        else
        {
            PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
        }
        base.TakeDamage(damage, fromServer);
    }



    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {

        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            try
            {
                if (checkEnemyInScene.enemyInScreen)
                {
                    TakeDamage(laserPower);
                }
            }
            catch (System.NullReferenceException)
            {
                Debug.Log("Null LaserOnOnLaserHitTriggeredEnemy boss2");
                throw;
            }

        }
    }


    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.bossDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.bossDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
