﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;

public class Boss2MissileExplosion : MonoBehaviour
{

    // Use this for initialization
    public ParticleSystem particleSystemIns;

   
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "DeadZoneBulletHell")
        {
            if (PoolManager.Pools[Const.explosiveName].IsSpawned(transform))
            {
                PoolManager.Pools[Const.explosiveName].Despawn(transform);
            }
        }
    }

    void OnEnable()
    {
        particleSystemIns.Play(true);
        Despawn();
    }

    //	void OnDisable ()
    //	{
    //
    //	}


    private void Despawn()
    {
        this.Delay(4.5f, () =>
        {
            particleSystemIns.Stop();
            if (PoolManager.Pools[Const.explosiveName].IsSpawned(transform))
            {
                PoolManager.Pools[Const.explosiveName].Despawn(transform);
            }
        });
    }
}
