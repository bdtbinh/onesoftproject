﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;

public class Boss8Collision : EnemyCollisionBase
{
    private Boss8Action bossActionObj;
    private CheckEnemyInScene checkEnemyInScene;
    public SpriteRenderer spr;
    Transform transCache, transParent;

    void Awake()
    {
        transCache = transform;
        transParent = transCache.parent;

        bossActionObj = GetComponent<Boss8Action>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            bossActionObj.Die(EnemyKilledBy.DeadZone);
        }
    }



    public override void TakeDamage(int damage, bool fromServer = false)
    {
        if (bossActionObj == null)
        {
            bossActionObj = GetComponent<Boss8Action>();
        }
        bossActionObj.currentHP -= damage;
        PanelBossHealbar.Instance.ChangeBossHealbar(bossActionObj.currentHP, bossActionObj.maxHP);

        if ((bossActionObj.currentHP <= bossActionObj.maxHP / 3) && !bossActionObj.bossDamged)
        {
            bossActionObj.bossDamged = true;
            //bossActionObj.boss4MainWeanponCenter.ubhNwayShot.m_bulletNum = 4;
            //bossActionObj.animBoss4Head.SetTrigger("isForm2");
        }

        if (bossActionObj.currentHP <= 0)
        {
            if (!bossActionObj.bossFx)
            {
                bossActionObj.bossFx = true;
                PanelBossHealbar.Instance.HideBossHealbar();
                bossActionObj.GetComponent<CircleCollider2D>().enabled = false;
                bossActionObj.M_SplineMove.Stop();
                bossActionObj.boss8LaserGun.StopShootLaserWhenDie();
                Pool_EffectDie();
                if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign || GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
                {
                    MainScene.Instance.bossDead = true;
                }
                //			MainScene.Instance.parent_UbhPool.gameObject.SetActive (false);
                this.Delay(1.8f, () =>
                {
                    bossActionObj.GetComponent<CircleCollider2D>().enabled = true;
                    if (!fromServer)
                    {
                        bossActionObj.Die(EnemyKilledBy.Player);
                    }
                    else
                    {
                        bossActionObj.Die(EnemyKilledBy.Server);
                    }
                });
            }
        }
        else
        {
            PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
        }
        base.TakeDamage(damage, fromServer);
    }


    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            if (checkEnemyInScene.enemyInScreen)
            {
                TakeDamage(laserPower);
            }
        }
    }


    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.bossDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.bossDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }


}
