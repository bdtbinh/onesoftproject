﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwoDLaserPack;
using UnityEngine.SceneManagement;
using Sirenix.OdinInspector;

public class Boss8LaserGun : MonoBehaviour
{
    public TweenRotation tweenRotLaser;
    public ParticleSystem effectLaser;
    public Boss8Action bossActionObj;
    public Animator[] animLaserList;
    public SpriteBasedLaser[] spriteBasedLaserList;
    int leftRightLaser;

    [Title("GÓC QUAY CỦA LASER", bold: true, horizontalLine: true)]
    public float laserRotationPhase1;
    public float laserRotationPhase2;

    public float durationRotationPhase1;
    public float durationRotationPhase2;

    Vector3 laserToLeftPhase1, laserToRightPhase1;
    Vector3 laserToLeftPhase2, laserToRightPhase2;

    void Start()
    {
        //animLaser = GetComponent<Animator>();

        foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
        {
            spriteBasedLaserScript.OnLaserHitTriggered += LaserOnOnLaserHitTriggered;
            spriteBasedLaserScript.SetLaserState(false);
        }

        laserToLeftPhase1 = new Vector3(0f, 0f, laserRotationPhase1);
        laserToRightPhase1 = new Vector3(0f, 0f, -laserRotationPhase1);
        laserToLeftPhase2 = new Vector3(0f, 0f, laserRotationPhase2);
        laserToRightPhase2 = new Vector3(0f, 0f, -laserRotationPhase2);
    }



    IEnumerator StartShootLaser()
    {

        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootLaserStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    public void ShootLaserStep1()
    {
        //Debug.LogError("ShootLaserStep1 ");
        //animLaser.SetTrigger("isLaserAttack2");
        foreach (Animator animLaser in animLaserList)
        {
            animLaser.SetTrigger("isLaserAttack2");
        }

        effectLaser.gameObject.SetActive(true);
        effectLaser.Play(true);
        StartCoroutine("ShootLaserStep2");
    }

    IEnumerator ShootLaserStep2()
    {
        //Debug.LogError("ShootLaserStep2 1");
        yield return new WaitForSeconds(3f);
        //Debug.LogError("ShootLaserStep2 2");
        if (gameObject.activeInHierarchy)
        {

            effectLaser.Stop();
            effectLaser.gameObject.SetActive(false);
            foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
            {
                spriteBasedLaserScript.SetLaserState(true);
            }
            leftRightLaser++;

            if (bossActionObj.bossDamged)
            {
                tweenRotLaser.duration = durationRotationPhase2;
                if (leftRightLaser % 2 == 0)
                {
                    tweenRotLaser.to = laserToLeftPhase2;
                }
                else
                {
                    tweenRotLaser.to = laserToRightPhase2;
                }
            }
            else
            {
                tweenRotLaser.duration = durationRotationPhase1;
                if (leftRightLaser % 2 == 0)
                {
                    tweenRotLaser.to = laserToLeftPhase1;
                }
                else
                {
                    tweenRotLaser.to = laserToRightPhase1;
                }
            }

            tweenRotLaser.enabled = true;
            tweenRotLaser.ResetToBeginning();
        }
        else
        {
            StopAllCoroutines();
        }
    }

    public void FinishTweenShootLaser()
    {

        StartCoroutine("StopShootLaser");
    }

    IEnumerator StopShootLaser()
    {
        //Debug.LogError("StopShootLaser 1");
        yield return new WaitForSeconds(1f);
        //Debug.LogError("StopShootLaser 2");
        if (gameObject.activeInHierarchy)
        {
            foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
            {
                spriteBasedLaserScript.SetLaserState(false);
            }
            foreach (Animator animLaser in animLaserList)
            {
                //Debug.LogError("isLaserIdle");
                animLaser.SetTrigger("isLaserIdle");
            }

            //if (!bossActionObj.bossDamged)
            //{
            //    bossActionObj.M_SplineMove.Resume();
            //}

            tweenRotLaser.transform.localRotation = Quaternion.Euler(Vector3.zero);
            tweenRotLaser.enabled = false;

        }
        else
        {
            StopAllCoroutines();
        }
    }



    public void StopShootLaserWhenDie()
    {
        foreach (SpriteBasedLaser spriteBasedLaserScript in spriteBasedLaserList)
        {
            spriteBasedLaserScript.SetLaserState(false);
        }
        StopAllCoroutines();
    }


    private void LaserOnOnLaserHitTriggered(RaycastHit2D hitInfo)
    {
        //		Debug.LogError ("LaserOnOnLaserHitTriggered Player");

        if (hitInfo.collider.tag == "player" && !MainScene.Instance.gameFinished && !hitInfo.collider.GetComponent<Aircraft>().aircraftIsReloading && !hitInfo.collider.GetComponent<Aircraft>().aircraftUsingActiveShield && !MainScene.Instance.gameStopping && !MainScene.Instance.bossDead)
        {
            //hitInfo.collider.GetComponent<PlayerCollision> ().Pool_EffectPlayerDie ();
            //hitInfo.collider.GetComponent<PlayerController> ().PlayerDie ();
            if (SceneManager.GetActiveScene().name == "LevelTutorial")
            {
                hitInfo.collider.GetComponent<Aircraft>().playerInitScript.ChangeNumberLife(-9999);
            }
            hitInfo.collider.GetComponent<Aircraft>().playerControllerFalconScript.PlayerDie();
        }
    }


}
