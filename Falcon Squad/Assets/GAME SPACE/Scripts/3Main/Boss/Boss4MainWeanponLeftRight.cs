﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Boss4MainWeanponLeftRight : MonoBehaviour
{
    public ParticleSystem effectStep1Gun;
    //
    Animator anim;


    public UbhShotCtrl[] gunCtrlList;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    IEnumerator StartShootTest()
    {
        ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }

    //
    public void ShootMainWeaponStep1()
    {

        //Debug.LogError("ShootMainWeaponStep1 ");

        anim.SetTrigger("isAttack1");
        effectStep1Gun.gameObject.SetActive(true);
        effectStep1Gun.Play(true);
    }

    public void ShootMainWeaponStep2()
    {
        //Debug.LogError("ShootMainWeaponStep2");
        anim.SetTrigger("isAttack2");

        if (gameObject.activeInHierarchy)
        {
            foreach (UbhShotCtrl gun in gunCtrlList)
            {
                gun.StartShotRoutine();
            }
        }

    }

    public void ShootMainWeaponStep3()
    {
        anim.SetTrigger("isAttack3");


        foreach (UbhShotCtrl gun in gunCtrlList)
        {
            gun.StopShotRoutine();
        }
        effectStep1Gun.Stop();
        effectStep1Gun.gameObject.SetActive(false);
        //Debug.LogError("ShootMainWeaponStep3");
    }


    public void ChangeAnim_Atk_To_Idle()
    {
        //anim.SetTrigger("isIdle");

        //Debug.LogError("isIdle");
    }
    public void FinishTweenShootMainWeapon()
    {
        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        //		Debug.LogError ("FinishTweenShootMainWeapon 1");
        foreach (UbhShotCtrl gun in gunCtrlList)
        {
            gun.StopShotRoutine();
        }
    }
}
