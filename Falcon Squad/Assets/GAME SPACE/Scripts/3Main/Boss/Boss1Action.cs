﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;

public class Boss1Action : BossAction
{
    public bool enemyTransforming;
    public bool enemyTransformed;

    public Boss1GunTarger boss1ShootTarget;

    public UbhShotCtrl[] gunCtrlList;
    public SpriteBasedLaser[] spriteBasedLaserList;

    public TweenRotation tweenRotUbhShot, tweenRotLaser;

    public Animator animBoss1;
    public Boss1LaserGun boss1LaserGun;
    public Boss1MainWeanponGun boss1MainWeanponGun;



    public float speedStart, speedEnd;



    void Start()
    {
        base.Start();
        M_SplineMove.ChangeSpeed(speedStart);
    }




    public void ShootMainWeaponForm1()
    {
        if (!bossDamged && !enemyTransforming)
        {
            boss1MainWeanponGun.ShootMainWeaponStep1();
        }
    }

    public void ShootMainWeaponForm2()
    {
        if (bossDamged && !enemyTransforming)
        {
            boss1MainWeanponGun.ShootMainWeaponStep1();
        }
    }

    //	------------------------ ShootLaser --------------------------------------------------------------

    int numShootLaser;

    public void ShootLaserForm1()
    {
        if (!bossDamged && !enemyTransforming)
        {
            if (numShootLaser > 0)
            {
                Debug.LogError("ShootLaserForm1 ");
                M_SplineMove.Pause();
                boss1LaserGun.ShootLaserStep1();
            }
            numShootLaser++;
        }

    }

    public void ShootLaserForm2()
    {
        if (bossDamged && !enemyTransforming)
        {
            if (numShootLaser > 0)
            {
                boss1LaserGun.ShootLaserStep1();
            }
            numShootLaser++;
        }
    }

    //	------------------------ Shoot Target --------------------------------------------------------------

    bool isShootTarget;

    public void ShootTarget(int timeDelay)
    {
        if (!isShootTarget)
        {
            isShootTarget = true;
            boss1ShootTarget.ShootMainWeaponStep1();
            StartCoroutine("ShootTargetLoop", timeDelay);
        }
    }

    IEnumerator ShootTargetLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss1ShootTarget.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    //	------------------------ Boss Transforming khi den diem 1 --------------------------------------------------------------

    public void BossForm1_To_Form2()
    {
        if (!enemyTransformed && !enemyTransforming && bossDamged)
        {
            M_SplineMove.ChangeSpeed(speedEnd);
            M_SplineMove.Pause();

            boss1LaserGun.FinishTweenShootLaser();
            boss1MainWeanponGun.FinishTweenShootMainWeapon();

            animBoss1.SetTrigger("isTransform");
            enemyTransformed = true;
            enemyTransforming = true;
        }
    }


    public override void Restart()
    {
        base.Restart();
    }


    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);

        numShootLaser = 0;
        enemyTransformed = false;
        enemyTransforming = false;
        isShootTarget = false;
    }



}
