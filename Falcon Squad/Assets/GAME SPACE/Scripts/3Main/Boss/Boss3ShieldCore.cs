﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using Sirenix.OdinInspector;
using PathologicalGames;
using Assets.MuscleDrop.Scripts.Utilities.ExtensionMethods;
using TCore;

public class Boss3ShieldCore : EnemyCollisionBase
{

    //[ShowInInspector]
    public Boss3ShieldController boss3ShieldController;

    public int hpShieldCore;
    [DisplayAsString]
    public int curHpShieldCore;

    SpriteRenderer spr;
    CircleCollider2D boxCollider;
    Vector3 offsetEffect;
    public EnemyHPbar enemyHpBarIns;

    public bool isShieldCoreDie = false;
    public Sprite sShieldCoreShow, sShieldCoreHide;

    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<CircleCollider2D>();
        //
        ResetData();
        offsetEffect = new Vector3(0f, 0f, 0f);
    }

    public void ResetData()
    {
        curHpShieldCore = hpShieldCore;
        //enemyHpBarIns.sBgBar.SetActive(false);
        isShieldCoreDie = false;
        spr.sprite = sShieldCoreShow;
        boxCollider.enabled = true;
    }


    public override void TakeDamage(int damage, bool fromServer = false)
    {
        curHpShieldCore -= damage;

        if (curHpShieldCore <= 0)
        {
            Pool_EffectDie();
            isShieldCoreDie = true;

            spr.sprite = sShieldCoreHide;
            boxCollider.enabled = false;
            boss3ShieldController.boss3Action.Boss3Shield_Hide();
        }
        else
        {
            Pool_EffectTriggerBullet(transform);
            PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
            if (enemyHpBarIns != null)
            {
                enemyHpBarIns.ChangeHPBar(curHpShieldCore, hpShieldCore);
            }
        }
        base.TakeDamage(damage, fromServer);
    }


    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        //Debug.LogError("laserPower:" + laserPower + "_nameObj:" + gameObject.name);
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            TakeDamage(laserPower);
        }
    }



    //---------
    public override void Pool_EffectDie()
    {
        if (EffectList.Instance.enemyDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyDie, transform.position + offsetEffect, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
