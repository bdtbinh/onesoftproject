﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using TwoDLaserPack;
using DG.Tweening;
using TCore;

public class Boss2Action : BossAction
{
    public Boss2MainWeanponHead boss2MainWeanponHead;

    [DisplayAsString]
    public bool boss2Pausing;

    public Boss2MainWeanponGun boss2MainWeanponGun;
    public Boss2MissileGun boss2MissileGun;


    void Start()
    {
        base.Start();
    }


    //	------------------------ Shoot MainWeaPon --------------------------------------------------------------

    bool isShootMainWeapon;

    public void ShootMainWeapon(int timeDelay)
    {
        if (!isShootMainWeapon)
        {
            isShootMainWeapon = true;
            boss2MainWeanponGun.ShootMainWeaponStep1();
            StartCoroutine("ShootMainWeaponLoop", timeDelay);
        }
    }

    IEnumerator ShootMainWeaponLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss2MainWeanponGun.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }


    //	------------------------ Shoot MainWeaPon --------------------------------------------------------------

    bool isShootMissile;

    public void ShootMissile(int timeDelay)
    {
        if (!isShootMissile)
        {
            isShootMissile = true;
            boss2MissileGun.ShootMainWeaponStep1();
            StartCoroutine("ShootMissileLoop", timeDelay);
        }

    }

    IEnumerator ShootMissileLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss2MissileGun.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }


    //------------------------------ boss 7 bắn ra đạn vòng tròn ở trên đầu -----------------

    bool isShootMainWeaponHead;
    public void ShootMainWeaponHead(int timeDelay)
    {
        if (!isShootMainWeaponHead)
        {
            isShootMainWeaponHead = true;
            boss2MainWeanponHead.ShootMainWeaponStep1();
            StartCoroutine("ShootMainWeaponHeadLoop", timeDelay);
        }
    }

    IEnumerator ShootMainWeaponHeadLoop(int timeDelay)
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (gameObject.activeInHierarchy)
            {
                boss2MainWeanponHead.ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }
    }






    public override void Restart()
    {
        base.Restart();
    }

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {
        base.Die(type);

        isShootMainWeapon = false;
        isShootMissile = false;
        isShootMainWeaponHead = false;
    }



}
