﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum TypeZone
{
    LeftRight,
    Top

}

public class DeadZoneBullet : MonoBehaviour
{
    public TypeZone typeZone;


    void OnTriggerEnter2D(Collider2D col)
    {
        //		Debug.LogError ("typeZone" + typeZone);
        if (col.CompareTag("PlayerBullet"))
        {
            var bullet = col.GetComponent<BulletPlayerValue>();
            bullet.Despawn();
        }
        else if (col.CompareTag("EnemyBullet"))
        {
            UbhBullet bullet = col.transform.parent.GetComponent<UbhBullet>();
            if (bullet != null)
            {
                UbhObjectPool.instance.ReleaseBullet(bullet);
            }
        }
        else if (col.CompareTag("BulletBoss7_Stone"))
        {
            UbhBullet bullet = col.transform.parent.GetComponent<UbhBullet>();
            if (bullet != null)
            {
                UbhObjectPool.instance.ReleaseBullet(bullet);
            }
        }
        else if (col.CompareTag("PlayerBulletWindSlash"))
        {
            if (typeZone != TypeZone.LeftRight)
            {
                UbhBullet bullet = col.GetComponent<UbhBullet>();
                if (bullet != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(bullet);
                }
            }
        }
        else if (col.CompareTag("PlayerBulletIceShard"))
        {
            if (typeZone != TypeZone.LeftRight)
            {
                UbhBullet bullet = col.GetComponent<UbhBullet>();
                if (bullet != null)
                {
                    UbhObjectPool.instance.ReleaseBullet(bullet);
                }
            }
        }
        else if (col.CompareTag("PlayerBulletHoming"))
        {

            var bullet = col.GetComponent<BulletPlayerValue>();
            bullet.Despawn();
        }
        else if (col.CompareTag("PlayerBulletBurstShot"))
        {
            if (typeZone != TypeZone.LeftRight)
            {
                var bullet = col.GetComponent<BulletPlayerValue>();
                bullet.Despawn();
            }
        }
        else if (col.CompareTag("PlayerBulletStarbomb"))
        {
            if (typeZone != TypeZone.LeftRight)
            {
                var bullet = col.GetComponent<BulletPlayerValue>();
                bullet.Despawn();
            }
        }
        else if (col.CompareTag("PlayerBulletChainsaw"))
        {
            //Debug.LogError("PlayerBulletChainsaw trigger out");
            if (typeZone != TypeZone.LeftRight)
            {
                var bullet = col.GetComponent<BulletPlayerValue>();
                bullet.Despawn();
            }
        }
    }
}