﻿using UnityEngine;
using System.Collections;
using TCore;
using UnityEngine.SceneManagement;
using Firebase.Analytics;
using EasyMobile;
using SkyGameKit;
using Mp.Pvp;
using I2.Loc;

public class PopupPause : MonoBehaviour
{

    public UILabel lNumLevel;
    public GameObject buttonRestart;


    void Start()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
        {
            lNumLevel.text = "" + ScriptLocalization.lTry;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
            {
                lNumLevel.text = ScriptLocalization.level_upgrade + " " + CacheGame.GetCurrentLevel();
            }
            else
            {
                lNumLevel.text = ScriptLocalization.level_extra_name.Replace("%{number}", CacheGame.GetCurrentLevel().ToString());
            }
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
        {
            lNumLevel.text = ScriptLocalization.endless;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            lNumLevel.text = ScriptLocalization.title_summer_holiday;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp)
        {
            lNumLevel.text = "PvP";
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament)
        {
            lNumLevel.text = ScriptLocalization.tournament;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            lNumLevel.text = "PvP 2vs2";
        }
    }

    void OnEnable()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            buttonRestart.SetActive(false);
        }
        else
        {
            buttonRestart.SetActive(true);
        }
    }

    public void BackListLevel()
    {

        SoundManager.PlayClickButton();

        Time.timeScale = 1f;

        GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.NotShow;

        if (SceneManager.GetActiveScene().name == "LevelTutorial")
        {
            CacheGame.SetTutorialGame(1);
        }

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            MainScene.Instance.LoadNewSceneFromMain("SelectLevel");
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
        {
            if (GameContext.typeOpenSceneFromTry == GameContext.TypeOpenSceneFromTry.SelectLevel)
            {
                MainScene.Instance.LoadNewSceneFromMain("SelectLevel");
            }
            else
            {
                MainScene.Instance.LoadNewSceneFromMain("Home");
            }
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
            {
                new CSCoopPvPPlayerOut().Send();
            }
            else
            {
                new CSPlayerOut().Send();
            }
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
            {
                MainScene.Instance.LoadNewSceneFromMain("Home");
            }
            else
            {
                MainScene.Instance.LoadNewSceneFromMain("PVPMain");
            }
            PvpUtil.SendUpdatePlayer("Back_PvP_Pause");
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            GameContext.typeLoadPopupSelectEvent = GameContext.TypeLoadPopupSelectEvent.NotShow;
            MainScene.Instance.LoadNewSceneFromMain("Home");
        }
        else
        {
            GameContext.typeLoadPopupSelectEndless = GameContext.TypeLoadPopupSelectEndless.NotShow;
            MainScene.Instance.LoadNewSceneFromMain("Home");
        }

        FirebaseLogSpaceWar.LogLevelFail();
        FirebaseLogSpaceWar.LogGoldIn(PanelUITop.Instance.curCoin, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogGemIn(PanelUITop.Instance.curGem, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogTimePlayLevel("" + CacheGame.GetCurrentLevel(), MainScene.Instance.timePlayLevel);

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString()) + PanelUITop.Instance.curCoin);

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString()) + MainScene.Instance.numEnemyKilled);

        MainScene.Instance.popupPause.SetActive(false);

        //GameContext.isInGameCampaign = false;
        //PvpUtil.SendUpdatePlayer();

    }

    public void Reset()
    {
        Time.timeScale = 1f;

        SoundManager.PlayClickButton();

        //if (CacheGame.GetFirtSessionGame() != 1)
        //{
        //    MainScene.Instance.LoadNewSceneFromMain("Home");
        //    return;
        //}
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.Main_Click_Retry;
            MainScene.Instance.LoadNewSceneFromMain("SelectLevel");
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
        {
            MainScene.Instance.LoadNewSceneFromMain("LevelTryPlane");
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            GameContext.typeLoadPopupSelectEvent = GameContext.TypeLoadPopupSelectEvent.Main_Click_Retry;
            MainScene.Instance.LoadNewSceneFromMain("Home");
        }
        else
        {
            GameContext.typeLoadPopupSelectEndless = GameContext.TypeLoadPopupSelectEndless.Main_Click_Retry;
            MainScene.Instance.LoadNewSceneFromMain("Home");
        }

        FirebaseLogSpaceWar.LogLevelFail();
        FirebaseLogSpaceWar.LogGoldIn(PanelUITop.Instance.curCoin, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogGemIn(PanelUITop.Instance.curGem, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogTimePlayLevel("" + CacheGame.GetCurrentLevel(), MainScene.Instance.timePlayLevel);

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString()) + PanelUITop.Instance.curCoin);

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString()) + MainScene.Instance.numEnemyKilled);

        OsAdsManager.Instance.ShowInterstitialAds(OsAdsManager.LocationClickInterstitial.btn_Inter_RetryPause);

        MainScene.Instance.popupPause.SetActive(false);

        //GameContext.isInGameCampaign = false;
        //PvpUtil.SendUpdatePlayer();
    }


    public void Resume()
    {
        MainScene.Instance.gameStopping = false;

        //if (PanelTimeSphere.Instance.timeSpherePlaying)
        //{
        //    Time.timeScale = PanelTimeSphere.Instance.valueSlow;
        //    Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
        //}
        //else
        //{

        Time.timeScale = 1f;

        //}
        SoundManager.PlayClickButton();
        MainScene.Instance.popupPause.SetActive(false);
    }

}
