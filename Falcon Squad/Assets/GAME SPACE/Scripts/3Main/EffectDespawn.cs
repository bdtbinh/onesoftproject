﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;




public enum TypeEffectDestroy
{
	AnimEffect,
	PracticleEffect,

}

public class EffectDespawn : MonoBehaviour
{

	public TypeEffectDestroy typeEffect;
	public float timeDelay;
	ParticleSystem particle;

	void Start ()
	{
//		if (typeEffect == TypeEffectDestroy.PracticleEffect) {
//			DespawnEffectByTime ();
//		}
		particle = GetComponent<ParticleSystem> ();
	}

    void OnDisable()
    {
        StopAllCoroutines();
    }


    // Call o flame cuoi cung trong animation
    public void OnAnimationExplosionFinish ()
	{
		if (PoolManager.Pools [Const.explosiveName].IsSpawned (transform)) {
			PoolManager.Pools [Const.explosiveName].Despawn (transform);
		}
	}



	public void OnSpawned ()  // Might be able to make this private and still work?
	{
		// Start the timer as soon as this instance is spawned.

		if (typeEffect == TypeEffectDestroy.PracticleEffect) {
			StartCoroutine (DespawnByTime ());
		}
	}



	private IEnumerator DespawnByTime ()
	{
		yield return new WaitForSeconds (timeDelay);
		particle.Stop ();
		if (PoolManager.Pools [Const.explosiveName].IsSpawned (transform)) {
			PoolManager.Pools [Const.explosiveName].Despawn (transform, Const.ExplosionPoolTransform);
		}
	}



	private void DespawnactiveSkillText ()
	{
		this.Delay (timeDelay, () => {
			particle.Stop ();
			gameObject.SetActive (false);
		});

	}
}
