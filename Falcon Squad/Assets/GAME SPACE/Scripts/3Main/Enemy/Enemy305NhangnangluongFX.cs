﻿using UnityEngine;
using UniRx;
using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;
using SkyGameKit;
using PathologicalGames;

public class Enemy305NhangnangluongFX : MonoBehaviour
{

    TweenScale tweenScaleIns;
    ParticleSystem particleSystemIns;
    Transform transCache;

    bool startMoveDown = false;
    public float speedFxMoveDown = 1.5f;

    void Awake()
    {
        tweenScaleIns = GetComponent<TweenScale>();
        particleSystemIns = GetComponent<ParticleSystem>();
        transCache = GetComponent<Transform>();

    }

    public void SetDataStart()
    {

        this.Delay(0.1f, () =>
        {
            if (gameObject.activeInHierarchy)
            {
                tweenScaleIns.enabled = true;
                tweenScaleIns.ResetToBeginning();
                particleSystemIns.Play(true);
            }
        });

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == Const.deadZone)
        {
            if (PoolManager.Pools[Const.explosiveName].IsSpawned(transform))
            {
                PoolManager.Pools[Const.explosiveName].Despawn(transform, Const.ExplosionPoolTransform);
            }
        }
    }

    void Update()
    {
        if (startMoveDown)
        {
            transCache.position -= transCache.up * Time.deltaTime * speedFxMoveDown;
        }
    }


    private void OnDisable()
    {
        startMoveDown = false;
        particleSystemIns.Stop();
    }



    public void SetupWhenEnemyDie()
    {
        transCache.parent = Const.ExplosionPoolTransform;
        startMoveDown = true;
        tweenScaleIns.enabled = false;
    }


    public void OnfinishTweenScale()
    {
        //transCache.parent = Const.ExplosionPoolTransform;
        //startMoveDown = true;
        //Pool_EffectEnemyDie();
        //enemyActionIns.Die(EnemyKilledBy.DeadZone);
    }



    private void Despawn()
    {
        particleSystemIns.Stop();
        tweenScaleIns.enabled = false;
        if (PoolManager.Pools[Const.explosiveName].IsSpawned(transform))
        {
            PoolManager.Pools[Const.explosiveName].Despawn(transform, Const.ExplosionPoolTransform);
        }
    }


    public void Pool_EffectEnemyDie()
    {
        //		Debug.LogError ("transform eff " + transform.position);
        if (EffectList.Instance.enemyDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
