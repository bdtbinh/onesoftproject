﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using Sirenix.OdinInspector;
using PathologicalGames;
using Assets.MuscleDrop.Scripts.Utilities.ExtensionMethods;
using TCore;

public class Enemy_NhenChild : EnemyCollisionBase
{
    public string typeLeftRight;
    private EnemyAction2017 enemyAction;

    public int hpNhenChild;
    [DisplayAsString]
    public int curHpNhenChild;

    SpriteRenderer spr;
    public EnemyHPbar enemyHpBarIns;

    void Start()
    {

        if (typeLeftRight == "left")
        {
            transform.localPosition = new Vector3(MainScene.Instance.posAnchorLeft.localPosition.x + 0.28f, transform.localPosition.y, 0f);
        }
        else
        {
            transform.localPosition = new Vector3(MainScene.Instance.posAnchorRight.localPosition.x - 0.28f, transform.localPosition.y, 0f);
        }

        spr = GetComponent<SpriteRenderer>();
        //
        enemyAction = GetComponentInParent<EnemyAction2017>();


    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            if (typeLeftRight == "left")
            {
                enemyAction.Die(EnemyKilledBy.DeadZone);
            }
        }
    }


    public override void TakeDamage(int damage, bool fromServer = false)
    {
        curHpNhenChild -= damage;
        if (enemyHpBarIns != null)
        {
            enemyHpBarIns.ChangeHPBar(curHpNhenChild, hpNhenChild);
        }
        if (curHpNhenChild <= 0)
        {
            if (!enemyAction.fxDie)
            {
                enemyAction.fxDie = true;
                Pool_EffectDie();
                if (!fromServer)
                {
                    enemyAction.Die(EnemyKilledBy.Player);
                }
                else
                {
                    enemyAction.Die(EnemyKilledBy.Server);
                }
            }
        }
        else
        {
            Pool_EffectTriggerBullet(transform);
            PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
        }
        base.TakeDamage(damage, fromServer);
    }


    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {

        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            TakeDamage(laserPower);
        }
    }


    //---------
    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.enemyDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

}
