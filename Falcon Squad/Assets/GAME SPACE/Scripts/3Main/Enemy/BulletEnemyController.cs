﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemyController : MonoBehaviour
{

	public enum TypeEnemyBullet
	{
		MainWeapon,
		EchGreen,
	}

	public TypeEnemyBullet typeEnemyBullet;

	public TweenScale tweenScaleEchGreen;
	Transform trans_Cache;

	void Awake ()
	{
		trans_Cache = transform;
	}

	void OnEnable ()
	{
		if (typeEnemyBullet == TypeEnemyBullet.EchGreen) {
			if (tweenScaleEchGreen != null) {
				tweenScaleEchGreen.enabled = true;
				tweenScaleEchGreen.ResetToBeginning ();
			}
		}

	}

	void OnDisable ()
	{   

		if (typeEnemyBullet == TypeEnemyBullet.EchGreen) {
			if (tweenScaleEchGreen != null) {
				trans_Cache.localScale = Vector3.one;
				tweenScaleEchGreen.enabled = false;
			}
		}
	}
}
