﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class Bullet_EnemyW10_SplitshotAlien : MonoBehaviour
{
    UbhBullet ubhBulletIns;


    public UbhShotCtrl[] gunCtrlListUse;


    void Start()
    {
        ubhBulletIns = gameObject.GetComponent<UbhBullet>();
    }

    //float t1, t2, t3;
    int tyleShootRandom;
    private void OnEnable()
    {
        tyleShootRandom = UnityEngine.Random.Range(1, gunCtrlListUse.Length + 1);
        SetShootFromBullet();
    }

    private void OnDisable()
    {
        StopMainWeapon();
    }


    void SetShootFromBullet()
    {
        this.Delay(0.66f, () =>
        {
            ubhBulletIns.m_shooting = false;
            ShootMainWeapon();

            UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
        });
    }

    //
    public void ShootMainWeapon()
    {
        if (gameObject.activeInHierarchy)
        {
            gunCtrlListUse[tyleShootRandom - 1].StartShotRoutine();
        }
    }


    public void StopMainWeapon()
    {
        foreach (UbhShotCtrl gun in gunCtrlListUse)
        {
            gun.StopShotRoutine();
        }
    }

}
