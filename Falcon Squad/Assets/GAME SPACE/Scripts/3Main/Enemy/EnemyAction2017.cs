﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Diagnostics;
using UniRx;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using Assets.MuscleDrop.Scripts.Utilities.ExtensionMethods;
using DG.Tweening;
using TCore;
using Firebase.Analytics;
using UnityEngine.SceneManagement;


public class EnemyAction2017 : EnemyData
{

    public UbhShotCtrl[] gunCtrlList;

    EnemyChangeAnimation enemyChangeAnimation;
    EnemyDropItem enemyDropItem;

    [DisplayAsString]
    public EnemyChangeGun enemyChangeGunIns;

    public EnemyHPbar enemyHpBarIns;

    [HideInInspector]
    public bool enemyIsInScene;

    //
    Transform tran_cache, tranParent_Cache;
    float posTop, posBottom, posLeft, posRight;

    // get speed start o  splinemove luc ban dau`
    float speedStart;
    bool enemyInScreen;

    void Awake()
    {
        speedStart = M_SplineMove.speed;
        enemyChangeAnimation = GetComponentInChildren<EnemyChangeAnimation>();
        enemyDropItem = GetComponent<EnemyDropItem>();
        enemyChangeGunIns = GetComponent<EnemyChangeGun>();
    }



    void Start()
    {
        ChangeDataByDifficult();

        posTop = MainScene.Instance.posAnchorTop.localPosition.y;
        posBottom = MainScene.Instance.posAnchorBottom.localPosition.y;
        posLeft = MainScene.Instance.posAnchorLeft.localPosition.x - 0.1f;
        posRight = MainScene.Instance.posAnchorRight.localPosition.x + 0.1f;


        if (enemyHpBarIns != null)
        {
            enemyHpBarIns.baseEnemyTransIns = transform;
        }

    }

    private void OnBecameVisible()
    {
        enemyInScreen = true;
        //Debug.LogError("vao");
    }

    private void OnBecameInvisible()
    {
        enemyInScreen = false;
        //Debug.LogError("ra");
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    //------------------------------Change HP theo 3 do kho -----------------

    float valueAddHp;
    void ChangeDataByDifficult()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            switch (CacheGame.GetDifficultCampaign())
            {
                case "Normal":
                    valueAddHp = DataRemoteEnemy.Instance.HP_Normal;
                    break;
                case "Hard":
                    valueAddHp = DataRemoteEnemy.Instance.HP_Hard;
                    break;
                case "Hell":
                    valueAddHp = DataRemoteEnemy.Instance.HP_Hell;
                    break;
                default:
                    break;
            }

            maxHP = (int)(maxHP * valueAddHp);

            double coeff = CacheGame.GetValueLevelModifiServer(CacheGame.GetCurrentLevel());

            maxHP = (int)(maxHP * coeff);
            currentHP = maxHP;
        }

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
        {

            valueAddHp = DataRemoteEnemy.Instance.HP_Noel;
            maxHP = (int)(maxHP * valueAddHp);
            currentHP = maxHP;
        }
    }

    //------------------------------PAUSE -----------------

    public void Pause()
    {
        M_SplineMove.Pause();
        if (gameObject.activeInHierarchy)
        {
            if (timeActionPause == 0 && timeActionPause == null)
            {
                Debug.LogError("Chua truyen` timeActionPause vao` turn manager");
            }
            else
                StartCoroutine("Resume", timeActionPause);
        }
    }


    IEnumerator Resume(float timeDelay)
    {
        yield return new WaitForSeconds(timeDelay);
        if (gameObject.activeInHierarchy)
        {
            M_SplineMove.Resume();
        }
        else
        {
            StopAllCoroutines();
        }
    }

    //------------------------------Shoot----------------------------
    public void Shoot()
    {
        enemyChangeAnimation.ChangeAnim_Atk_Step1();
    }

    //------------------------------Pause And Shoot -----------------

    public void PauseAndShoot()
    {
        M_SplineMove.Pause();
        enemyChangeAnimation.ChangeAnim_Atk_Step1();
    }


    //------------------------------PAUSE AND SHOOT LOOP-----------------
    bool isPauseAndShootLoop;

    public void PauseAndShootLoop(float timeDelay)
    {

        if (!isPauseAndShootLoop)
        {
            tran_cache = transform;
            tranParent_Cache = tran_cache.parent;
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine("StartPauseAndShootLoop", timeDelay);
            }

            isPauseAndShootLoop = true;
        }

    }


    IEnumerator StartPauseAndShootLoop(float timeDelay)
    {
        M_SplineMove.Pause();
        enemyChangeAnimation.ChangeAnim_Atk_Step1();
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (enemyInScreen)
            {
                if (gameObject.activeInHierarchy)
                {
                    if (enemyChangeGunIns != null)
                    {
                        enemyChangeGunIns.GetTypeGunToShoot();
                    }

                    M_SplineMove.Pause();
                    enemyChangeAnimation.ChangeAnim_Atk_Step1();
                }
                else
                {
                    enemyChangeAnimation.StopAllCoroutines();
                    StopAllCoroutines();
                    break;
                }
            }
            else
            {
                //				Debug.LogError ("CheckEnemyInScene: " + CheckEnemyInScene ());
            }
        }
    }

    //------------------------------ SHOOT LOOP-----------------

    bool isShootLoop;

    public void ShootLoop(float timeDelay)
    {
        if (!isShootLoop)
        {
            tran_cache = transform;
            tranParent_Cache = tran_cache.parent;
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine("StartShootLoop", timeDelay);
            }

            isShootLoop = true;
        }
    }

    IEnumerator StartShootLoop(float timeDelay)
    {
        enemyChangeAnimation.ChangeAnim_Atk_Step1();
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            if (enemyInScreen)
            {
                if (gameObject.activeInHierarchy)
                {

                    if (enemyChangeGunIns != null)
                    {
                        enemyChangeGunIns.GetTypeGunToShoot();
                    }

                    enemyChangeAnimation.ChangeAnim_Atk_Step1();
                }
                else
                {
                    enemyChangeAnimation.StopAllCoroutines();
                    StopAllCoroutines();
                    break;
                }
            }
            else
            {
                //				Debug.LogError ("CheckEnemyInScene: " + CheckEnemyInScene ());
            }

        }
    }


    //------------------------------ Warning Hazzard-----------------
    public void WarningHazzard()
    {
        if (EffectList.Instance.warningHazzard != null)
        {
            Transform effectWarningIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.warningHazzard, new Vector3(transform.localPosition.x, 5.2f, 0f), Quaternion.identity);
            effectWarningIns.GetComponent<ParticleSystem>().Play(true);
        }

        M_SplineMove.Pause();

        this.Delay(1.8f, () =>
        {
            M_SplineMove.Resume();
        });
    }



    //------------------------------ Warning Nhen-----------------
    public void WarningNhen()
    {

        if (EffectList.Instance.warningHazzard != null)
        {
            Transform effectWarningLeft = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.warningHazzard, new Vector3(MainScene.Instance.posAnchorLeft.localPosition.x + 0.32f, 5.2f, 0f), Quaternion.identity);
            effectWarningLeft.GetComponent<ParticleSystem>().Play(true);
            Transform effectWarningRight = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.warningHazzard, new Vector3(MainScene.Instance.posAnchorRight.localPosition.x - 0.32f, 5.2f, 0f), Quaternion.identity);
            effectWarningRight.GetComponent<ParticleSystem>().Play(true);
        }

        M_SplineMove.Pause();

        this.Delay(1.8f, () =>
        {
            M_SplineMove.Resume();
        });
    }


    //------------------------------ Drop Item-----------------
    public void DropItem()
    {
        enemyDropItem.Drop();
    }



    public void PauseListEnemy(float timePause)
    {
        foreach (Transform enemyIns in transform.parent)
        {
            enemyIns.GetComponent<EnemyAction2017>().M_SplineMove.Pause(timePause);
        }
    }




    //	------------------------ SetNextPath --------------------------------------------------------------

    bool pathLoopChanged;
    //	protected Transform newPathLoopTransform;

    public void ChangeToPathLoop()
    {


        if (!pathLoopChanged)
        {
            if (newPath_Loop.name.Length < 1)
            {
                Debug.LogError("Da dien` new path vao turn chua???");
            }

            M_SplineMove.Stop();
            //
            if (PoolManager.Pools[Const.pathPool].IsSpawned(M_SplineMove.pathContainer.transform))
            {
                PoolManager.Pools[Const.pathPool].Despawn(M_SplineMove.pathContainer.transform);
            }
            //
            M_SplineMove.moveToPath = true;
            M_SplineMove.loopType = splineMove.LoopType.loop;

            newPathTransform = PoolManager.Pools[Const.pathPool].Spawn(
                newPath_Loop.transform,
                posStartPathLoop,
                newPath_Loop.transform.rotation
            );


            var bezier = newPathTransform.GetComponent<BezierPathManager>();
            if (bezier != null)
            {
                bezier.CalculatePath();
                M_SplineMove.SetPath(bezier);
            }
            else
            {
                var newPathManager = newPathTransform.GetComponent<PathManager>();
                M_SplineMove.SetPath(newPathManager);
            }

            pathLoopChanged = true;
        }

    }

    //------------------------------ StartFollowPlayer danh cho enemy mui ten xanh -----------------

    bool isFollowed;

    public void StartFollowPlayer()
    {
        if (!isFollowed)
        {
            GetComponent<Enemy_MuiTenXanh_Rotate>().isStartFollow = true;
            GetComponent<Enemy_MuiTenXanh_Rotate>().GetTranformParent();

            isFollowed = true;
        }
    }
    //------------------------------ ChangeSpeed-----------------
    bool speedChanged;

    public void ChangeSpeed()
    {
        if (!speedChanged)
        {
            if (newSpeed == null || newSpeed == 0)
            {
                Debug.LogError("Chua truyen` newSpeed vao` turn manager");
            }
            else
            {
                M_SplineMove.ChangeSpeed(newSpeed);
                speedChanged = true;
            }
        }
    }



    public override void Restart()
    {
        base.Restart();
        if (enemyChangeAnimation != null)
        {
            enemyChangeAnimation.ResetAnimator();
        }

        if (gunCtrlList != null)
        {
            foreach (UbhShotCtrl gun in gunCtrlList)
            {
                gun.StopShotRoutine();
            }
        }

        if (ID == "Enemy3Elite_Nhen")
        {
            Enemy_NhenChild[] nhenChilds = GetComponentsInChildren<Enemy_NhenChild>();

            foreach (Enemy_NhenChild nhenChild in nhenChilds)
            {
                nhenChild.curHpNhenChild = nhenChild.hpNhenChild;
            }
        }
    }

    //
    public override void ForceRemove()
    {
        base.ForceRemove();
    }
    //

    public override void Die(EnemyKilledBy type = EnemyKilledBy.Player)
    {

        if (PoolManager.Pools[Const.enemyPoolName].IsSpawned(transform))
        {
            if (type == EnemyKilledBy.Player)
            {
                PanelUITop.Instance.SetTextScore(score);

                SoundManager.SoundEnemyDie();
                MainScene.Instance.numEnemyKilled++;
                //CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString()) + 1);
                ////daily
                //CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString()) + 1);
                //CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString()) + 1);

                if (ID == "Enemy3305_Nhangnangluong")
                {
                    if (GetComponentInChildren<Enemy305NhangnangluongFX>() != null)
                    {
                        GetComponentInChildren<Enemy305NhangnangluongFX>().SetupWhenEnemyDie();
                    }
                }
                if (ID == "EnemyLaserRoundter")
                {
                    if (GetComponentInChildren<EnemyLaserRoundter>() != null)
                    {
                        GetComponentInChildren<EnemyLaserRoundter>().StopShootLaserWhenDie();
                    }
                }

            }
            if (gameObject.tag != "Hazzard" && !MainScene.Instance.gameFinished)
            {
                MainScene.Instance.totalEnemyCanDie++;
            }
        }



        base.Die(type);


        enemyChangeAnimation = GetComponentInChildren<EnemyChangeAnimation>();
        if (enemyChangeAnimation != null)
        {
            enemyChangeAnimation.StopAllCoroutines();
        }

        syncAnimation = false;
        speedChanged = false;
        isFollowed = false;
        pathLoopChanged = false;
        isShootLoop = false;
        isPauseAndShootLoop = false;
        //
        M_SplineMove.moveToPath = false;
        M_SplineMove.loopType = splineMove.LoopType.none;
        //

        M_SplineMove.ChangeSpeed(speedStart);
        StopAllCoroutines();

    }

    //----RestartAnimator cho cac enemy khi di  ra dap canh dong` thoi`
    // con nao can anim giong nhau thi tick

    protected override void RestartAnimator()
    {
        if (syncAnimation)
        {
            Animator anim = enemyChangeAnimation.GetComponent<Animator>();
            if (anim != null)
            {
                anim.Play(anim.GetCurrentAnimatorStateInfo(0).fullPathHash, -1, 0f);
            }
            else
            {
                Debug.LogError("khong tim thay Enemy Animator");
            }

        }
    }




}
