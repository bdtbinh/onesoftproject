﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum TypeEnemyFollow
{
    MuiTenXanh,
    Boss3

}
public class Enemy_MuiTenXanh_Rotate : MonoBehaviour
{
    public TypeEnemyFollow typeEnemyFollow;
    GameObject player;
    //	Transform player_Trans;

    Transform trans_Cache;
    Transform trans_Parent_Cache;
    Transform trans_GranParent_Cache;
    Vector3 positionParent_Cache;

    public bool isStartFollow;

    void Start()
    {

        if (typeEnemyFollow == TypeEnemyFollow.Boss3)
        {
            trans_Cache = transform;
            trans_Parent_Cache = trans_Cache.parent;
            trans_GranParent_Cache = trans_Parent_Cache.parent;
            positionParent_Cache = trans_Parent_Cache.localPosition + trans_Cache.parent.parent.localPosition;
        }
        else
        {
            trans_Cache = transform;
            trans_Parent_Cache = trans_Cache.parent;
            positionParent_Cache = trans_Parent_Cache.localPosition;
        }
    }

    public void GetTranformParent()
    {
        if (typeEnemyFollow == TypeEnemyFollow.Boss3)
        {
            trans_Cache = transform;
            positionParent_Cache = trans_Cache.parent.localPosition + trans_Cache.parent.parent.localPosition;
            //Debug.LogError(".parent.parent " + trans_Cache.parent.parent.localPosition);
        }
        else
        {
            trans_Cache = transform;
            positionParent_Cache = trans_Cache.parent.localPosition;
        }
    }

    void Update()
    {
        if (isStartFollow)
        {
            if (player == null)
            {
                player = GameObject.FindWithTag("player");
            }
            else
            {
                if (typeEnemyFollow == TypeEnemyFollow.Boss3)
                {
                    positionParent_Cache = trans_Parent_Cache.localPosition + trans_Cache.parent.parent.localPosition;
                }
                Quaternion posFollow = Quaternion.LookRotation(-(trans_Cache.localPosition + positionParent_Cache) + player.transform.localPosition, trans_Cache.TransformDirection(Vector3.forward));

                trans_Cache.rotation = new Quaternion(0, 0, posFollow.z, posFollow.w);
            }
        }
    }


}
