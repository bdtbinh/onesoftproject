﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using DG.Tweening;
using DG.Tweening.Core;
using SkyGameKit;
using TCore;


public class EnemyCollision : EnemyCollisionBase
{
    private EnemyAction2017 enemyObj;
    private CheckEnemyInScene checkEnemyInScene;
    //
    SpriteRenderer spr;

    Transform transCache, transParent;

    void Awake()
    {
        enemyObj = GetComponent<EnemyAction2017>();
        checkEnemyInScene = GetComponent<CheckEnemyInScene>();
        spr = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        transCache = transform;
        transParent = transform.parent;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.tag == Const.deadZone)
        {
            enemyObj.Die(EnemyKilledBy.DeadZone);
        }
    }

    //--------------
    public override void TakeDamage(int damage, bool fromServer = false)
    {
        if (enemyObj == null)
        {
            enemyObj = GetComponent<EnemyAction2017>();
        }
        enemyObj.currentHP -= damage;
        if (enemyObj.enemyHpBarIns != null)
        {
            enemyObj.enemyHpBarIns.ChangeHPBar(enemyObj.currentHP, enemyObj.maxHP);
        }
        if (enemyObj.currentHP <= 0)
        {
            if (!enemyObj.fxDie)
            {
                enemyObj.fxDie = true;
                if (!fromServer)
                {
                    enemyObj.Die(EnemyKilledBy.Player);
                }
                else
                {
                    enemyObj.Die(EnemyKilledBy.Server);
                }

                Pool_EffectDie();
            }
        }
        else
        {
            PoolExplosiveSmall(spr);
            SoundManager.SoundBullet_Enemy();
        }
        base.TakeDamage(damage, fromServer);
    }



    public override void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            if (checkEnemyInScene.enemyInScreen)
            {
                TakeDamage(laserPower);
            }
        }
    }

    public override void Pool_EffectDie()
    {
        base.Pool_EffectDie();
        if (EffectList.Instance.enemyDie != null)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}
