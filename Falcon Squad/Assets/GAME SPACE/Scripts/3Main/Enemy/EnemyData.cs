﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using SWS;
using Sirenix.OdinInspector;

public abstract class EnemyData : BaseEnemy
{


	[Title ("Data Get From Turn", bold: true, horizontalLine: true)]
	[DisplayAsString]
	public float newSpeed = 2f;
	[DisplayAsString]
	public float timeActionPause = 3f;
	[DisplayAsString]
	public PathManager newPath_Loop;
	[DisplayAsString]
	public Vector3 posStartPathLoop;
	[DisplayAsString]
	public bool syncAnimation;
	// Use this for initialization
}
