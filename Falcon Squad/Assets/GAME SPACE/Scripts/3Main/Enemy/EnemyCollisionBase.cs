﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using SkyGameKit;
using TCore;

public abstract class EnemyCollisionBase : MonoBehaviour
{
    public BaseEnemy baseEnemy;
    SpriteRenderer sprAnim;
    private void OnEnable()
    {
        if (baseEnemy == null)
        {
            baseEnemy = GetComponent<BaseEnemy>();
        }
    }

    private void OnDisable()
    {
        isTriggerWing2SmallBird = false;
        if (sprAnim != null)
            sprAnim.color = Color.white;
    }

    bool isTriggerWing2SmallBird;
    public virtual void OnTriggerEnter2D(Collider2D col)
    {
        if ((!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing) || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            switch (col.tag)
            {
                case "ZoneCheckEnemyInScene":
                    Debug.LogError("inScene");
                    break;
                case "PlayerBullet":
                    if (gameObject.tag != "Hazzard")
                    {
                        var bullet = col.GetComponent<BulletPlayerValue>();
                        Pool_EffectTriggerBullet(col.transform);
                        TakeDamage(bullet.power);
                        bullet.Despawn();
                    }
                    else
                    {
                        Pool_EffectTriggerBullet(col.transform);
                        SoundManager.SoundBullet_Enemy();
                        var bullet = col.GetComponent<BulletPlayerValue>();
                        bullet.Despawn();
                    }
                    break;
                case "PlayerBulletBurstShot":
                    if (gameObject.tag != "Hazzard")
                    {
                        var bullet = col.GetComponent<BulletPlayerValue>();
                        Pool_EffectTriggerBurstShot(col.transform);
                        TakeDamage(bullet.power);
                        bullet.Despawn();
                    }
                    else
                    {
                        Pool_EffectTriggerBurstShot(col.transform);
                        SoundManager.SoundBullet_Enemy();
                        var bullet = col.GetComponent<BulletPlayerValue>();
                        bullet.Despawn();
                    }
                    break;
                case "PlayerBulletStarbomb":
                    if (gameObject.tag != "Hazzard")
                    {
                        var bullet = col.GetComponent<BulletPlayerValue>();
                        var bulletStarbomb = col.GetComponent<BulletActiveSkillPlane8>();
                        TakeDamage(bullet.power);
                        bulletStarbomb.StartExplosive();

                        //bullet.Despawn();
                    }
                    else
                    {
                        SoundManager.SoundBullet_Enemy();
                        var bulletStarbomb = col.GetComponent<BulletActiveSkillPlane8>();
                        bulletStarbomb.StartExplosive();
                    }
                    break;
                case "PlayerBulletHoming":
                    if (gameObject.tag != "Hazzard")
                    {
                        var bullet = col.GetComponent<BulletPlayerValue>();
                        TakeDamage(bullet.power);
                        Pool_EffectTriggerBullet(col.transform);
                        bullet.Despawn();
                    }
                    else
                    {
                        Pool_EffectTriggerBullet(col.transform);
                        SoundManager.SoundBullet_Enemy();
                    }

                    break;
                case "PlayerBulletWindSlash":
                    if (gameObject.tag != "Hazzard")
                    {
                        var bullet = col.GetComponent<BulletPlayerValue>();
                        TakeDamage(bullet.power);
                        Pool_EffectTriggerBullet(col.transform);
                    }
                    else
                    {
                        Pool_EffectTriggerBullet(col.transform);
                        SoundManager.SoundBullet_Enemy();
                    }
                    break;
                case "Wing2SmallBird":
                    if (gameObject.tag != "Hazzard" && !isTriggerWing2SmallBird)
                    {
                        var bullet = col.GetComponent<Wing2SmallBird>();
                        Pool_EffectTriggerBullet(col.transform);
                        Debug.LogError("enemy Trigger Wing2SmallBird: " + bullet.power);

                        TakeDamage(bullet.power);

                        isTriggerWing2SmallBird = true;
                        this.Delay(0.5f, () =>
                        {
                            isTriggerWing2SmallBird = false;
                        });
                    }
                    else
                    {
                        SoundManager.SoundBullet_Enemy();
                        Pool_EffectTriggerBullet(col.transform);
                    }

                    break;
                case "PlayerMissile":
                    Debug.Log("enemy Trigger Player");
                    if (PoolManager.Pools["BulletPlayerPool"].IsSpawned(col.transform))
                    {
                        PoolManager.Pools["BulletPlayerPool"].Despawn(col.transform);
                    }
                    break;
            }
        }

    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (!MainScene.Instance.gameFinished && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing)
        {
            switch (col.tag)
            {
                case "PlayerBulletChainsaw":
                    if (gameObject.tag != "Hazzard")
                    {
                        var bullet = col.GetComponent<BulletPlayerValue>();
                        TakeDamage(bullet.power);
                        Pool_EffectTriggerBullet(col.transform);
                    }
                    break;
                case "Wing3Skill":
                    if (gameObject.tag != "Hazzard")
                    {
                        var bullet = col.GetComponent<Wing3Skill>();
                        TakeDamage(bullet.power);
                        //Debug.LogError("OnTriggerStay2D Wing3Skill: " + bullet.power);
                        Pool_EffectTriggerBullet(col.transform);
                    }
                    break;
            }
        }
    }

    //------------------------------FX----------------------------------------------------------

    Vector3 tempVector3;

    public virtual void TakeDamage(int damage, bool fromServer = false)
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            if (baseEnemy != null && !fromServer)
            {
                ConnectManager.Instance.SendData(CachePvp.ENEMY_HP, tempVector3, baseEnemy.uid, baseEnemy.currentHP, damage);
            }
        }

    }


    public virtual void LaserOnOnLaserHitTriggeredEnemy(int laserPower = 150)
    {
        Debug.Log("base laserPower:" + laserPower + "_nameObj:" + gameObject.name);
    }


    //------------------------------FX----------------------------------------------------------

    public void PoolExplosiveSmall(SpriteRenderer spr)
    {
        if (spr != null)
        {
            spr.color = EffectList.Instance.colorEffect;
            sprAnim = spr;
            this.Delay(0.05f, () =>
            {
                spr.color = Color.white;
            });
        }
    }

    public virtual void Pool_EffectTriggerBullet(Transform bulletTrans)
    {
        if (EffectList.Instance.enemyTriggerBullet != null && bulletTrans.gameObject.activeInHierarchy)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyTriggerBullet, bulletTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    public virtual void Pool_EffectTriggerBurstShot(Transform bulletTrans)
    {
        if (EffectList.Instance.enemyTriggerBulletBurstShot != null && bulletTrans.gameObject.activeInHierarchy)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyTriggerBulletBurstShot, bulletTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    public virtual void Pool_EffectDie()
    {

    }


}
