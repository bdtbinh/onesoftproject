﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;



public class CheckEnemyInScene : MonoBehaviour
{

    [DisplayAsString]
    public bool enemyInScreen;


    void OnDisable()
    {
        enemyInScreen = false;
    }

    private void OnBecameVisible()
    {
        enemyInScreen = true;
    }


    private void OnBecameInvisible()
    {
        enemyInScreen = false;
    }
}
