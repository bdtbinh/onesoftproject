﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using DG.Tweening.Core;
using Sirenix.OdinInspector;
using SWS;
using UniRx;
using SkyGameKit;
using PathologicalGames;



public class EnemyChangeAnimation : MonoBehaviour
{

    [HideInInspector]
    public string typeEnemy;

    private EnemyAction2017 enemyActionIns;
    private splineMove splineMoveS;

    private Sequence myTween;
    Animator anim;

    UbhShotCtrl[] gunCtrlList;

    public TweenPosition tween_MuiTenXanh_Rung;

    void Awake()
    {
        anim = GetComponent<Animator>();

        enemyActionIns = GetComponentInParent<EnemyAction2017>();
        typeEnemy = enemyActionIns.ID;
        gunCtrlList = enemyActionIns.gunCtrlList;

    }



    public void EnemyShootTest()
    {

        switch (typeEnemy)
        {
            case "Enemy2Normal_Ruoi":
                StartCoroutine("StartAnimShoot");
                break;
            case "Enemy1Elite_BoSungMay":
                StartCoroutine("StartAnimShoot");
                break;
            case "Enemy4Elite_BoTamNhiet":
                StartCoroutine("StartAnimShoot");
                break;
            case "Enemy2Elite_OcXoay":
                StartCoroutine("StartAnimShoot");
                break;
            case "Enemy5Elite_MuiTenXanh":
                StartCoroutine("StartAnimShoot");
                break;
            case "EnemyBigBug":
                StartCoroutine("StartAnimShoot");
                break;
            default:
                break;
        }
    }


    public void EnemyStopShoot()
    {
        switch (typeEnemy)
        {
            case "Enemy2Normal_Ruoi":
                StopCoroutine("StartAnimShoot");
                if (gunCtrlList != null)
                {
                    gunCtrlList[0].StopShotRoutine();
                }
                break;
            default:
                break;
        }
    }

    public IEnumerator StartAnimShoot()
    {
        yield return new WaitForSeconds(2f);
        ChangeAnim_Atk_Step1();
        StartCoroutine("StartShoot");
    }

    public IEnumerator StartShoot()
    {
        while (true)
        {
            yield return new WaitForSeconds(6f);
            ChangeAnim_Atk_Step1();
        }
    }



    public void KillTween()
    {
        if (myTween != null)
            myTween.Kill();
    }



    public void StopAPoint(int id)
    {

    }


    public void ChangeAnim_Atk_Step1()
    {

        if (typeEnemy == "Enemy2Normal_Ruoi")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "Enemy1Elite_BoSungMay")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "Enemy4Elite_BoTamNhiet")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "Enemy2Elite_OcXoay")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "Enemy5Elite_MuiTenXanh")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "EnemyBigBug1")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "EnemyBigBug2")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "EnemySeahorseCrabAlien")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "EnemyOverTake")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "EnemyWaveShooter")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "EnemyBlastBug")
        {
            anim.SetTrigger("isAttack1");
        }
        else if (typeEnemy == "EnemyMeteor")
        {
            if (gunCtrlList != null)
            {
                foreach (var item in gunCtrlList)
                {
                    item.StartShotRoutine();
                }
            }
        }
        else if (typeEnemy == "EnemyStatellite")
        {
            if (enemyActionIns.gameObject.transform.GetChild(2).GetComponent<EnemyOngShieldController>().isStun)
            {
                anim.SetTrigger("isStun");
            }
            else
            {
                anim.SetTrigger("isAttack1");
            }
        }
        else if (typeEnemy == "EnemySpiderBlue")
        {
            anim.SetTrigger("isAttack1");
        }
    }

    public void ChangeAnim_Atk_Step2()
    {
        if (enemyActionIns.enemyChangeGunIns != null)
        {
            gunCtrlList = enemyActionIns.gunCtrlList;
        }
        if (typeEnemy == "Enemy2Normal_Ruoi")
        {
            anim.SetTrigger("isAttack2");
            if (gunCtrlList != null)
            {
                gunCtrlList[0].StartShotRoutine();
            }
        }
        else if (typeEnemy == "Enemy1Elite_BoSungMay")
        {
            anim.SetTrigger("isAttack2");
            if (gunCtrlList != null)
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StartShotRoutine();
                }
            }
        }
        else if (typeEnemy == "Enemy4Elite_BoTamNhiet")
        {
            anim.SetTrigger("isAttack2");
            this.Delay(0.1f, () =>
            {
                if (gunCtrlList != null)
                {
                    foreach (UbhShotCtrl gun in gunCtrlList)
                    {
                        gun.StartShotRoutine();
                    }
                    this.Delay(0.31f, () =>
                    {
                        ChangeAnim_Atk_Step3();
                    });
                }
            });

        }
        else if (typeEnemy == "Enemy2Elite_OcXoay")
        {
            anim.SetTrigger("isAttack2");
            if (gunCtrlList != null)
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StartShotRoutine();
                }
            }
        }
        else if (typeEnemy == "Enemy5Elite_MuiTenXanh")
        {
            anim.SetTrigger("isAttack2");

            tween_MuiTenXanh_Rung.enabled = true;
            tween_MuiTenXanh_Rung.ResetToBeginning();

        }
        else if (typeEnemy == "EnemyBigBug1")
        {
            anim.SetTrigger("isAttack2");

        }
        else if (typeEnemy == "EnemyBigBug2")
        {
            anim.SetTrigger("isAttack2");
        }
        else if (typeEnemy == "EnemySeahorseCrabAlien")
        {
            anim.SetTrigger("isAttack2");
            if (gunCtrlList != null)
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StartShotRoutine();
                }
            }

        }
        else if (typeEnemy == "EnemyOverTake")
        {
            anim.SetTrigger("isAttack2");
            if (gunCtrlList != null)
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StartShotRoutine();
                }
            }

        }
        else if (typeEnemy == "EnemyWaveShooter")
        {
            anim.SetTrigger("isAttack2");

        }
        else if (typeEnemy == "EnemyBlastBug")
        {
            anim.SetTrigger("isAttack2");
        }
        else if (typeEnemy == "EnemyStatellite")
        {
            if (enemyActionIns.gameObject.transform.GetChild(2).GetComponent<EnemyOngShieldController>().isStun)
            {
                anim.SetTrigger("isStun");
            }
            else
            {
                anim.SetTrigger("isAttack2");
                if (gunCtrlList != null)
                {
                    this.Delay(0.75f, () =>
                    {
                        foreach (UbhShotCtrl gun in gunCtrlList)
                        {
                            gun.StartShotRoutine();
                        }
                    });
                }
            }

        }
        if (typeEnemy == "EnemySpiderBlue")
        {
            anim.SetTrigger("isAttack2");

        }
    }



    public void ChangeAnim_Atk_Step3()
    {
        //		Debug.LogError ("Changestep3  " + typeEnemy);
        if (enemyActionIns.enemyChangeGunIns != null)
        {
            gunCtrlList = enemyActionIns.gunCtrlList;
        }
        //		Enemy4Elite_BoTamNhiet
        if (typeEnemy == "Enemy2Normal_Ruoi")
        {
            //			anim.SetTrigger ("isIdle");
            // khong dung ham nay cho ruoi
            //			Debug.LogError ("Changestep3  " + typeEnemy);
        }
        else if (typeEnemy == "Enemy1Elite_BoSungMay")
        {
            anim.SetTrigger("isAttack3");
            if (gunCtrlList != null)
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StopShotRoutine();
                }
            }
        }
        else if (typeEnemy == "Enemy4Elite_BoTamNhiet")
        {
            //			Debug.LogError ("Changestep3BoTamNhiet  " + typeEnemy);
            anim.SetTrigger("isAttack3");
            if (gunCtrlList != null)
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StopShotRoutine();
                }
            }
        }
        else if (typeEnemy == "Enemy2Elite_OcXoay")
        {
            anim.SetTrigger("isAttack3");
            if (gunCtrlList != null)
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StopShotRoutine();
                }
            }
        }
        else if (typeEnemy == "Enemy5Elite_MuiTenXanh")
        {
            anim.SetTrigger("isAttack3");
            //			StartCoroutine ("DelayShoot_MuiTenXanh");
            tween_MuiTenXanh_Rung.enabled = false;
            transform.localPosition = Vector3.zero;
            if (gunCtrlList != null)
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StartShotRoutine();
                }
            }
        }
        else if (typeEnemy == "EnemyBigBug1")
        {
            anim.SetTrigger("isAttack3");
            if (gunCtrlList != null)
            {

                gunCtrlList[0].StartShotRoutine();

            }

        }
        else if (typeEnemy == "EnemyBigBug2")
        {
            anim.SetTrigger("isAttack3");
            if (gunCtrlList != null)
            {

                gunCtrlList[0].StartShotRoutine();

            }
        }
        else if (typeEnemy == "EnemySeahorseCrabAlien")
        {
            anim.SetTrigger("isAttack3");
        }
        else if (typeEnemy == "EnemyOverTake")
        {
            anim.SetTrigger("isAttack3");
        }
        else if (typeEnemy == "EnemyWaveShooter")
        {

            if (enemyActionIns.enemyChangeGunIns != null)
            {
                gunCtrlList = enemyActionIns.gunCtrlList;
            }
            this.Delay(0.2f, () =>
            {
                foreach (UbhShotCtrl item in gunCtrlList)
                {
                    item.StartShotRoutine();
                }
            });
            anim.SetTrigger("isAttack3");
        }
        else if (typeEnemy == "EnemyBlastBug")
        {
            if (gunCtrlList != null)
            {
                foreach (var item in gunCtrlList)
                {

                    item.StartShotRoutine();

                }
            }
            anim.SetTrigger("isAttack3");
        }
        else if (typeEnemy == "EnemyStatellite")
        {
            if (enemyActionIns.gameObject.transform.GetChild(2).GetComponent<EnemyOngShieldController>().isStun)
            {
                anim.SetTrigger("isStun");
            }
            else
            {
                anim.SetTrigger("isAttack3");
            }
        }
        else if (typeEnemy == "EnemySpiderBlue")
        {
            anim.SetTrigger("isAttack3");
            this.Delay(0.35f, () =>
            {
                if (gunCtrlList != null)
                {
                    gunCtrlList[0].StartShotRoutine();
                }
            });
        }
    }

    public void ChangeAnim_Atk_To_Idle()
    {
        //		Debug.LogError ("Changes Idle " + typeEnemy);
        if (enemyActionIns.enemyChangeGunIns != null)
        {
            gunCtrlList = enemyActionIns.gunCtrlList;
        }
        if (typeEnemy == "Enemy2Normal_Ruoi")
        {
            anim.SetTrigger("isIdle");
            //			enemyActionIns.M_SplineMove.Resume ();
            StartCoroutine("DelayResume_RuoiMove");
            if (gunCtrlList != null)
            {
                gunCtrlList[0].StopShotRoutine();
            }
            //			Debug.LogError ("Change Idle  " + typeEnemy);
        }
        else if (typeEnemy == "Enemy1Elite_BoSungMay")
        {
            anim.SetTrigger("isIdle");
            enemyActionIns.M_SplineMove.Resume();
        }
        else if (typeEnemy == "Enemy4Elite_BoTamNhiet")
        {
            anim.SetTrigger("isIdle");
            enemyActionIns.M_SplineMove.Resume();
        }
        else if (typeEnemy == "Enemy2Elite_OcXoay")
        {
            anim.SetTrigger("isIdle");
            enemyActionIns.M_SplineMove.Resume();
            //			Debug.LogError ("ChangestepIdle " + typeEnemy);
        }
        else if (typeEnemy == "Enemy5Elite_MuiTenXanh")
        {
            anim.SetTrigger("isIdle");
            enemyActionIns.M_SplineMove.Resume();
            if (gunCtrlList != null)
            {
                foreach (UbhShotCtrl gun in gunCtrlList)
                {
                    gun.StopShotRoutine();
                }
            }
        }
        else if (typeEnemy == "EnemyBigBug1")
        {
            anim.SetTrigger("isIdle");
            ChangeAnimBigBugStep1();
        }
        else if (typeEnemy == "EnemyBigBug2")
        {
            anim.SetTrigger("isIdle");
            ChangeAnimBigBugStep1();

        }
        else if (typeEnemy == "EnemySeahorseCrabAlien")
        {
            anim.SetTrigger("isIdle");
            enemyActionIns.M_SplineMove.Resume();
        }
        else if (typeEnemy == "EnemyOverTake")
        {
            anim.SetTrigger("isIdle");
            enemyActionIns.M_SplineMove.Resume();
        }
        else if (typeEnemy == "EnemyWaveShooter")
        {
            anim.SetTrigger("isIdle");
            enemyActionIns.M_SplineMove.Resume();
        }
        else if (typeEnemy == "EnemyBlastBug")
        {
            anim.SetTrigger("isIdle");
            enemyActionIns.M_SplineMove.Resume();
        }
        else if (typeEnemy == "EnemyStatellite")
        {

            if (enemyActionIns.gameObject.transform.GetChild(2).GetComponent<EnemyOngShieldController>().isStun)
            {
                anim.SetTrigger("isStun");
            }
            else
            {
                anim.SetTrigger("isIdle");
            }
        }
        else if (typeEnemy == "EnemySpiderBlue")
        {
            enemyActionIns.M_SplineMove.Resume();
            anim.SetTrigger("isIdle");
        }
    }
    public void ChangeAnimBigBugStep1()
    {
        anim.SetTrigger("isAttack2.1");
    }
    public void ChangeAnimBigBugStep2()
    {
        anim.SetTrigger("isAttack2.2");
        if (gunCtrlList != null)
        {
            //foreach (UbhShotCtrl gun in gunCtrlList)
            {
                gunCtrlList[1].StartShotRoutine();
            }
        }
    }
    public void ChangeAnimBigBugStep3()
    {
        anim.SetTrigger("isAttack2.3");
    }
    public void ChangeAnimBigBugIdle()
    {
        anim.SetTrigger("isIdle");
        enemyActionIns.M_SplineMove.Resume();
    }
    public IEnumerator DelayResume_RuoiMove()
    {
        yield return new WaitForSeconds(0.31f);
        if (gameObject.activeInHierarchy)
        {
            enemyActionIns.M_SplineMove.Resume();
        }
        else
        {
            StopAllCoroutines();
        }
    }




    //
    public void MoveTweenToTarget()
    {

    }

    public bool GetBoolAnim(string nameParameter)
    {
        return anim.GetBool(nameParameter);
    }

    public void ChangeParameterAnimBoolFalse(string para)
    {
        anim.SetBool(para, false);
    }

    public void ChangeParameterAnimBoolTrue(string para)
    {
        anim.SetBool(para, true);
    }



    public void ResetAnimator()
    {

        if (anim != null)
        {
            anim.Rebind();
        }

    }

}
