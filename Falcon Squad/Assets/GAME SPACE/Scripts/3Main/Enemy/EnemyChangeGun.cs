﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyChangeGun : MonoBehaviour
{

	public TypeGunEnemy[] listTypeGunEnemy;

	EnemyAction2017 enemyActionIns;

	void Start ()
	{
		enemyActionIns = GetComponent<EnemyAction2017> ();
	}

	int valueChoose;
	int valueChooseOld;


	public void  GetTypeGunToShoot ()
	{

		while (true) {
			valueChoose = UnityEngine.Random.Range (0, listTypeGunEnemy.Length);

			if (valueChoose != valueChooseOld) {
				valueChooseOld = valueChoose;
				break;
			}
		}
		enemyActionIns.gunCtrlList = listTypeGunEnemy [valueChoose].oneTypeGun;
	}
}




[Serializable]
public class TypeGunEnemy
{
	public UbhShotCtrl[] oneTypeGun;
}