﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using SkyGameKit;


public class EnemyHPbar : MonoBehaviour
{

    public bool useHpBar = false;
    public bool rotateHpBar = false;
    //
    public float timeDelayHide = 0.28f;

    public GameObject parentHpBar;
    public GameObject sBgBar;

    public tk2dClippedSprite tk2dClippedHp;
    SpriteRenderer spriteRendererBgBar;
    MeshRenderer meshRendererClippedHp;

    private Rect rect;

    public Transform baseEnemyTransIns;
    //
    Transform parentHpTrans;

    private void Awake()
    {
        if (useHpBar)
        {
            sBgBar.SetActive(true);
        }
        else
        {
            sBgBar.SetActive(false);
        }
    }
    void Start()
    {

        parentHpTrans = parentHpBar.transform;
        rect = new Rect(0, 0, 1, 1);
    }

    void OnEnable()
    {
        if (spriteRendererBgBar != null)
        {
            spriteRendererBgBar.enabled = false;
        }
        else
        {
            spriteRendererBgBar = sBgBar.GetComponent<SpriteRenderer>();
            spriteRendererBgBar.enabled = false;
        }
        if (meshRendererClippedHp != null)
        {
            meshRendererClippedHp.enabled = false;
        }
        else
        {
            meshRendererClippedHp = tk2dClippedHp.GetComponent<MeshRenderer>();
            meshRendererClippedHp.enabled = false;
        }
    }
    void OnDisable()
    {
        StopAllCoroutines();
    }

    void Update()
    {
        if (useHpBar && rotateHpBar && baseEnemyTransIns != null)
        {
            parentHpTrans.localRotation = new Quaternion(baseEnemyTransIns.localRotation.x, baseEnemyTransIns.localRotation.y, -baseEnemyTransIns.localRotation.z, baseEnemyTransIns.localRotation.w);
        }
    }


    float ratio;
    bool callDelay = false;

    public void ChangeHPBar(int currentHP, int maxHP)
    {
        if (useHpBar)
        {
            spriteRendererBgBar.enabled = true;
            meshRendererClippedHp.enabled = true;
            //
            ratio = (float)currentHP / (float)maxHP;

            rect.x = -(1 - ratio);

            if (rect.x < -1)
            {
                rect.x = -1;
            }
            else if (rect.x > 0)
            {
                rect.x = 0;
            }
            tk2dClippedHp.ClipRect = rect;

            if (gameObject.activeInHierarchy)
            {
                StopCoroutine("HideHpBar");
                StartCoroutine("HideHpBar");
            }
        }
    }

    IEnumerator HideHpBar()
    {
        yield return new WaitForSeconds(timeDelayHide);
        spriteRendererBgBar.enabled = false;
        meshRendererClippedHp.enabled = false;
    }


}
