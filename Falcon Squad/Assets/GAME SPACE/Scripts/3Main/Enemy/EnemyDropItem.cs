﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using UnityEngine.Events;
using SWS;
using Sirenix.OdinInspector;
using SkyGameKit;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine.SceneManagement;


public class EnemyDropItem : MonoBehaviour
{
    [Title("GOLD", bold: true, horizontalLine: true)]
    public int rateDropCoin;
    public int valueDropMin;
    public int valueDropMax;
    public GameObject coinCopperPrefab;
    public GameObject coinSilverPrefab;
    public GameObject coinGoldPrefab;
    public GameObject coinDiamondPrefab;

    public bool isSpecial;

    //

    [DisplayAsString]
    public int valueDropCoinRandom;
    //[DisplayAsString]
    //public int curValueDropCoin;
    //--------------------------------------------------
    [Title("Gem", bold: true, horizontalLine: true)]
    public int rateDropGem;
    public int valueDropGemMin;
    public int valueDropGemMax;
    public GameObject gemPrefab;

    //
    [DisplayAsString]
    public int valueDropGemRandom;


    //--------------------------------------
    [Title("ITEM", bold: true, horizontalLine: true)]
    public RateItemDrop[] listRateItemDrop;

    //--------------------------------------
    [Title("CARD", bold: true, horizontalLine: true)]
    public RateCardDrop[] listRateCardDrop;

    //-----

    [Title("ItemEvents: Được điền 1-1000", bold: true, horizontalLine: true)]
    public int rateDropItemEvents; // riêng cái này được điền 1 - 1000
    public int numItemEventDrop = 1;
    public GameObject itemEventsPrefab;

    [Title("ItemEnergy : Được điền 1-1000", bold: true, horizontalLine: true)]
    public int rateDropItemEnergy; // riêng cái này được điền 1 - 1000
    public GameObject itemEnergyPrefab;



    Transform transformCache;
    Transform parentTrans;
    float posXObjWhenDrop;

    int randomPosXCoinMin, randomPosXCoinMax;
    int randomPosYCoinMin, randomPosYCoinMax;


    void Start()
    {
        if (rateDropCoin > 0)
        {
            rateDropCoin = (int)(rateDropCoin * (1 + VipBonusValue.goldDropChangeRate));
        }

        transformCache = transform;
        randomPosYCoinMin = -8;
        randomPosYCoinMax = 9;
    }


    public void Drop()
    {
        int randomRateCoin = Random.Range(0, 1000) / 10;

        if (randomRateCoin <= rateDropCoin && GameContext.modeGamePlay != GameContext.ModeGamePlay.TryPlane && GameContext.modeGamePlay != GameContext.ModeGamePlay.pvp && GameContext.modeGamePlay != GameContext.ModeGamePlay.Tournament && GameContext.modeGamePlay != GameContext.ModeGamePlay.PVP2vs2)
        {
            parentTrans = transformCache.parent;
            posXObjWhenDrop = parentTrans.localPosition.x + transformCache.localPosition.x;

            if (posXObjWhenDrop <= MainScene.Instance.posAnchorLeft.localPosition.x + 0.8f)
            {
                randomPosXCoinMin = 3;
                randomPosXCoinMax = 15;
            }
            else if (posXObjWhenDrop >= MainScene.Instance.posAnchorRight.localPosition.x - 0.8f)
            {
                randomPosXCoinMin = -15;
                randomPosXCoinMax = -3;
            }
            else
            {
                randomPosXCoinMin = -8;
                randomPosXCoinMax = 8;
            }
            GetCoinDrop();
        }

        GetItemDrop();
        //
        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.TryPlane && GameContext.modeGamePlay != GameContext.ModeGamePlay.pvp && GameContext.modeGamePlay != GameContext.ModeGamePlay.Tournament && GameContext.modeGamePlay != GameContext.ModeGamePlay.PVP2vs2)
        {
            DropGem();
            GetCardDrop();
        }

        if ((GameContext.modeGamePlay == GameContext.ModeGamePlay.Events && PanelUITop.Instance.numResourcesBonusEvent < 125) || (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign && MinhCacheGame.GetEventMissionRemainProgress((int)MinhCacheGame.SummerHolidayMission.Campaign) >= numItemEventDrop))
        {
            DropItemEvents();
        }

        DropItemEnergy();
    }





    #region --------------DROP COIN-------------------

    void CoinDoTweenWhenDrop(Transform coinIns)
    {

        Vector3 offset = transformCache.position + new Vector3(Random.Range(randomPosXCoinMin, randomPosXCoinMax) / 10f, Random.Range(randomPosYCoinMin, randomPosYCoinMax) / 10f, 0f);

        //		Debug.LogError ("trans1 " + transformCache.position + "trans2 " + offset);
        coinIns.position = offset;
        //coinIns.DOMove(offset, 0.2f).SetDelay(0f).OnComplete(() =>
        //{
        //coinIns.GetComponent<ItemController>().SetMove();
        //});
    }


    public void GetCoinDrop()
    {
        valueDropCoinRandom = Random.Range(valueDropMin, (valueDropMax + 1));

        if (valueDropMin > 0 && valueDropMax <= 4)
        {
            DropCoinCopper();
        }
        else if (valueDropMax <= 8)
        {
            DropRandomCoinSilver(1, 3);
            DropCoinCopper();
        }
        else if (valueDropMax <= 12)
        {
            //			DropRandomCoinDiamond (1, 3);
            DropRandomCoinGold(1, 4);
            DropCoinSilver();
        }
        else if (valueDropMax <= 20)
        {
            DropRandomCoinDiamond(1, 3);
            DropRandomCoinGold(1, 4);
            DropCoinSilver();
        }
        else
        {
            DropRandomCoinDiamond(2, 5);
            DropRandomCoinGold(2, 5);
            DropCoinSilver();
        }
    }



    public void DropRandomCoinDiamond(int minRandom, int maxRandom)
    {
        if (valueDropCoinRandom >= minRandom * 5)
        {
            int valueRandom;
            while (true)
            {
                valueRandom = Random.Range(minRandom, (maxRandom + 1));
                if (valueRandom * 5 <= valueDropCoinRandom)
                {
                    valueDropCoinRandom -= valueRandom * 5;
                    break;
                }
            }


            for (int i = 0; i < valueRandom; i++)
            {
                if (coinDiamondPrefab != null)
                {
                    Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(coinDiamondPrefab, transformCache.position, Quaternion.identity);
                    CoinDoTweenWhenDrop(itemIns);
                }
            }
        }

    }

    public void DropRandomCoinGold(int minRandom, int maxRandom)
    {
        if (valueDropCoinRandom >= minRandom * 3)
        {
            int valueRandom;
            while (true)
            {
                valueRandom = Random.Range(minRandom, (maxRandom + 1));
                if (valueRandom * 3 <= valueDropCoinRandom)
                {
                    valueDropCoinRandom -= valueRandom * 3;
                    break;
                }
            }

            for (int i = 0; i < valueRandom; i++)
            {
                if (coinGoldPrefab != null)
                {
                    Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(coinGoldPrefab, transformCache.position, Quaternion.identity);
                    CoinDoTweenWhenDrop(itemIns);
                }
            }
        }
    }


    public void DropRandomCoinSilver(int minRandom, int maxRandom)
    {
        if (valueDropCoinRandom >= minRandom * 2)
        {
            int valueRandom;
            while (true)
            {
                valueRandom = Random.Range(minRandom, (maxRandom + 1));
                if (valueRandom * 2 <= valueDropCoinRandom)
                {
                    valueDropCoinRandom -= valueRandom * 2;
                    break;
                }
            }

            for (int i = 0; i < valueRandom; i++)
            {
                if (coinSilverPrefab != null)
                {
                    Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(coinSilverPrefab, transformCache.position, Quaternion.identity);
                    CoinDoTweenWhenDrop(itemIns);
                }
            }
        }
    }



    //-- dung` cho so' coin con` du lai. Khong can random
    public void DropCoinGold()
    {
        for (int i = 0; i < valueDropCoinRandom; i += 3)
        {
            if (coinGoldPrefab != null)
            {
                Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(coinGoldPrefab, transformCache.position, Quaternion.identity);
                CoinDoTweenWhenDrop(itemIns);
            }
        }
    }

    public void DropCoinSilver()
    {
        for (int i = 0; i < valueDropCoinRandom; i += 2)
        {
            if (coinSilverPrefab != null)
            {
                Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(coinSilverPrefab, transform.position, Quaternion.identity);
                CoinDoTweenWhenDrop(itemIns);
            }
        }
    }

    public void DropCoinCopper()
    {
        for (int i = 0; i < valueDropCoinRandom; i++)
        {
            if (coinCopperPrefab != null)
            {
                Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(coinCopperPrefab, transformCache.position, Quaternion.identity);
                CoinDoTweenWhenDrop(itemIns);
            }
        }
    }

    #endregion
    #region ------------------------DROP GEM -----------
    public void DropGem()
    {
        if (CacheGame.GetNumGemGetToday() < CacheFireBase.GetMaxGemDropOneDay)
        {
            valueDropGemRandom = Random.Range(valueDropGemMin, (valueDropGemMax + 1));
            for (int i = 0; i < valueDropGemRandom; i++)
            {
                if (gemPrefab != null)
                {
                    Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(gemPrefab, transformCache.position, Quaternion.identity);
                    CoinDoTweenWhenDrop(itemIns);

                    CacheGame.SetNumGemGetToday(CacheGame.GetNumGemGetToday() + 1);
                }
            }
        }
    }

    #endregion

    #region ------------------- DROP ITEM--------------------------
    [System.Serializable]
    public class RateItemDrop
    {
        [Tooltip("ty le roi ra Item")]
        public string name;
        public int rateDropItem;
        public GameObject itemObj;
    }

    //-----------------

    int rateDropOneItem;

    public void GetItemDrop()
    {

        float numRandomDrop = (Random.Range(0, 1000) / 10f);
        rateDropOneItem = 0;
        //		Debug.LogError ("numRandomDrop " + numRandomDrop);
        for (int i = 0; i < listRateItemDrop.Length; i++)
        {
            rateDropOneItem += listRateItemDrop[i].rateDropItem;
            //			listRateItem.Add (rate);

            if ((numRandomDrop <= rateDropOneItem) && (listRateItemDrop[i].rateDropItem != 0))
            {
                //				Debug.LogError ("DROPPP numRandomDrop " + numRandomDrop + " rate " + rate);
                if (listRateItemDrop[i].name == "Power-up" && !PosNotDropItem())
                {
                    Debug.Log("NumLose-" + CacheGame.GetDifficultCampaign() + CacheGame.GetNumLoseInOneLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()) + " numPowerDrop " + GameContext.numItemPowerDrop + "  MaxDrop " + DataChangeUI.Instance.GetNumItemPowerMax());
                    if (!isSpecial)
                    {

                        if (GameContext.numItemPowerDrop > DataChangeUI.Instance.GetNumItemPowerMax()
                            && GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign
                            && SceneManager.GetActiveScene().name != "LevelTutorial") return;



                        if (listRateItemDrop[i].itemObj != null)
                        {
                            Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(listRateItemDrop[i].itemObj, transformCache.position, Quaternion.identity);
                            itemIns.GetComponent<ItemController>().SetMove();
                        }
                        GameContext.numItemPowerDrop++;
                    }
                    else
                    {
                        if (listRateItemDrop[i].itemObj != null)
                        {
                            Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(listRateItemDrop[i].itemObj, transformCache.position, Quaternion.identity);
                            itemIns.GetComponent<ItemController>().SetMove();
                        }
                        GameContext.numItemPowerDrop++;
                    }
                }
                else
                {

                    if (listRateItemDrop[i].itemObj != null && !PosNotDropItem())
                    {
                        if (listRateItemDrop[i].itemObj.name != "ItemSpeedShot")
                        {
                            Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(listRateItemDrop[i].itemObj, transformCache.position, Quaternion.identity);
                            itemIns.GetComponent<ItemController>().SetMove();
                        }
                    }
                }
                break;
            }

        }
    }


    bool PosNotDropItem()
    {
        if (posXObjWhenDrop <= MainScene.Instance.posAnchorLeft.localPosition.x + 0.8f || posXObjWhenDrop >= MainScene.Instance.posAnchorRight.localPosition.x - 0.8f)
        {

            return true;
        }
        return false;
    }
    #endregion

    #region----------------DROP CARD----------------------
    [System.Serializable]
    public class RateCardDrop
    {
        [Tooltip("ty le roi ra Card")]
        public string nameCard;
        public int rateDropCard;
        public GameObject cardObj;
    }

    //-----------------

    int rateDropOneCard;

    public void GetCardDrop()
    {
        if (CacheGame.GetNumCardGetToday() < CacheFireBase.GetMaxCardDropOneDay)
        {
            float numRandomDropCard = (Random.Range(0, 1000) / 10f);
            rateDropOneCard = 0;
            for (int i = 0; i < listRateCardDrop.Length; i++)
            {
                rateDropOneCard += listRateCardDrop[i].rateDropCard;
                if ((numRandomDropCard <= rateDropOneCard) && (listRateCardDrop[i].rateDropCard != 0))
                {
                    if (listRateCardDrop[i].cardObj != null && !PosNotDropItem())
                    {
                        Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(listRateCardDrop[i].cardObj, transformCache.position, Quaternion.identity);
                        itemIns.GetComponent<ItemController>().SetMove();
                        CacheGame.SetNumCardGetToday(CacheGame.GetNumCardGetToday() + 1);
                    }
                    break;
                }
            }
        }
    }

    #endregion


    public void DropItemEvents()
    {
        if (!MinhCacheGame.IsInEvent(GameContext.EVENT_SUMMER_HOLIDAY_2019)) return;

        int randomRateItemEvents = Random.Range(0, 1000);
        if (rateDropItemEvents >= randomRateItemEvents && itemEventsPrefab != null)
        {
            //PanelUITop.Instance.numEventResourcesItemDrop += 1;
            for (int i = 0; i < numItemEventDrop; i++)
            {
                Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(itemEventsPrefab, transformCache.position, Quaternion.identity);
                CoinDoTweenWhenDrop(itemIns);

            }
        }
    }


    public void DropItemEnergy()
    {
        int randomRateItemEnergy = Random.Range(0, 1000);

        if (rateDropItemEnergy >= randomRateItemEnergy && itemEnergyPrefab != null)
        {
            Transform itemIns = PoolManager.Pools[Const.itemPoolName].Spawn(itemEnergyPrefab, transformCache.position, Quaternion.identity);
        }
    }
}
