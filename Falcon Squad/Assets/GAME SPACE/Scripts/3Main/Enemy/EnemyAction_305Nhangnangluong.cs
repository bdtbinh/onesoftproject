﻿using UnityEngine;
using UniRx;
using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;
using SkyGameKit;
using PathologicalGames;

public class EnemyAction_305Nhangnangluong : MonoBehaviour
{

    // Use this for initialization
    EnemyAction2017 enemyActionIns;

    void Start()
    {
        enemyActionIns = GetComponent<EnemyAction2017>();
    }

    private void OnEnable()
    {
        this.Delay(0.1f, () =>
        {
            Pool_FxBonangluong(transform);
        });

    }

    private void OnDisable()
    {

    }

    public void Pool_FxBonangluong(Transform enemyTrans)
    {
        Debug.Log("sinh ra bo nang luong");
        Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.fxEnemy305Nhangnangluong, enemyTrans.localPosition, Quaternion.identity, enemyTrans);
        effectIns.localPosition = Vector3.zero;
        effectIns.GetComponent<Enemy305NhangnangluongFX>().SetDataStart();
    }
}
