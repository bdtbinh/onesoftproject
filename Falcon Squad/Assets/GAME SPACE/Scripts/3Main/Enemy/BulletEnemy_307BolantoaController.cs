﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SkyGameKit;

public class BulletEnemy_307BolantoaController : MonoBehaviour
{

    public TweenRotation tweenBulletRotation;

    UbhBullet ubhBulletIns;

    UbhShotCtrl[] listGunCtrlUse;
    public TypeGunMainWeapon[] listTypeGun;

    [Serializable]
    public class TypeGunMainWeapon
    {
        public UbhShotCtrl[] oneTypeGun;
    }

    //
    int valueChoose;
    int valueChooseOld;

    void Start()
    {
        ubhBulletIns = gameObject.GetComponent<UbhBullet>();
    }


    IEnumerator StartShootTest()
    {
        ShootMainWeaponStep1();
        while (true)
        {
            yield return new WaitForSeconds(8);
            if (gameObject.activeInHierarchy)
            {
                ShootMainWeaponStep1();
            }
            else
            {
                StopAllCoroutines();
                break;
            }
        }

    }

    //------ Viên đạn bay 1 lúc sẽ dừng lại và nổ ra thành 8 viên đạn mới 

    private void OnEnable()
    {
        SetShootFromBullet();
    }
    void SetShootFromBullet()
    {
        float timepPauseBullet = UnityEngine.Random.RandomRange(0.2f, 0.4f);
        this.Delay(timepPauseBullet, () =>
        {
            ubhBulletIns.m_shooting = false;
            tweenBulletRotation.enabled = true;
            tweenBulletRotation.ResetToBeginning();

            ShootMainWeaponStep1();
        });
    }


    private void OnDisable()
    {
        tweenBulletRotation.enabled = false;
    }

    //-----------------------------------------------------------------------------------


    public void GetTypeGunToShoot()
    {
        while (true)
        {
            valueChoose = UnityEngine.Random.Range(0, listTypeGun.Length);

            if (valueChoose != valueChooseOld)
            {
                valueChooseOld = valueChoose;
                break;
            }
        }

        listGunCtrlUse = listTypeGun[valueChoose].oneTypeGun;

    }


    //
    public void ShootMainWeaponStep1()
    {

        StartCoroutine("ShootMainWeaponStep2");
    }

    IEnumerator ShootMainWeaponStep2()
    {
        GetTypeGunToShoot();
        yield return new WaitForSeconds(1.6f);

        if (gameObject.activeInHierarchy)
        {
            foreach (UbhShotCtrl gun in listGunCtrlUse)
            {
                gun.StartShotRoutine();
            }
        }

    }

    public void ShootMainWeaponStep3()
    {

        foreach (UbhShotCtrl gun in listGunCtrlUse)
        {
            gun.StopShotRoutine();
        }

        this.Delay(0.1f, () =>
        {
            if (ubhBulletIns != null)
            {
                UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
            }
        });


    }

    public void FinishTweenShootMainWeapon()
    {

        StopShootMainWeapon();
    }

    void StopShootMainWeapon()
    {
        foreach (UbhShotCtrl gun in listGunCtrlUse)
        {
            gun.StopShotRoutine();
        }

    }
}
