﻿using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class PopupFinishTutorial : MonoBehaviour
{
    public GameObject panelNPC;
    public GameObject panelNewPlane;

    private void Start()
    {
        CacheGame.SetTutorialGame(1);
    }
    public void ConfirmNPC()
    {
        panelNPC.SetActive(false);
        panelNewPlane.SetActive(true);
    }

    public void ConfirmPlane()
    {
        SoundManager.PlayClickButton();
        Time.timeScale = 1f;
        MainScene.Instance.LoadNewSceneFromMain("SelectLevel");
    }

}
