﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using DG.Tweening;
using Sirenix.OdinInspector;


public class DataChangeUI : Singleton<DataChangeUI>
{


    public int numGemRevival = 500;

    public int numLoseActiveBonusLife = 3;
    //- Count cua? 2 list nay` phai bang nhau va` ko gio' gia tri nao` null
    public List<int> numLoseLevelList = new List<int>();
    public List<int> numItemPowerMaxList = new List<int>();




    //	[DisplayAsString]
    //	public int numItemPowerMaxGet = 100;

    //
    void Start()
    {
        GameContext.numItemPowerDrop = 0;
        //		CacheGame.SetNumLoseToGetMaxItemPower ((CacheGame.GetCurrentLevel ()), 8);
    }


    public int GetNumItemPowerMax()
    {
        int indexGetNumMax = 0;
        int numItemPowerMaxGet = 100;

        for (int i = 0; i < numLoseLevelList.Count; i++)
        {
            if (CacheGame.GetNumLoseInOneLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()) <= numLoseLevelList[i])
            {
                indexGetNumMax = i;
                break;
            }
        }


        if (CacheGame.GetNumLoseInOneLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()) > numLoseLevelList[numLoseLevelList.Count - 1])
        {
            indexGetNumMax = numItemPowerMaxList.Count - 1;
        }

        if (numItemPowerMaxList[indexGetNumMax] != null)
        {
            numItemPowerMaxGet = numItemPowerMaxList[indexGetNumMax];
        }

        //		Debug.LogError ("NumLose " + CacheGame.GetNumLoseToGetMaxItemPower () + "MaxItem " + numItemPowerMaxGet);
        return numItemPowerMaxGet;

    }


}
