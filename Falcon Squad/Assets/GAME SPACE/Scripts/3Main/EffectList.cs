﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using SkyGameKit;
using Sirenix.OdinInspector;

public class EffectList : Singleton<EffectList>
{
    public Color colorEffect;
    public Color colorHideButton;
    public GameObject enemyTriggerBullet;
    public GameObject enemyTriggerBulletBurstShot;
    public GameObject enemyTriggerBulletLightingBolt;
    public GameObject enemyDie;
    public GameObject bossDie;

    public GameObject warningHazzard;
    public GameObject playerTriggerItem;
    public GameObject playerTriggerCoin;
    public GameObject playerTriggerDiamond;
    public GameObject playerTriggerGem;

    public GameObject playerTriggerPowerUp;

    public GameObject playerDie;
    public GameObject playerDieAllLife;


    //
    public GameObject fxBoss2MissileExplosion;
    public GameObject fxBoss4SpiderSilk;
    public GameObject fxEnemy305Nhangnangluong;


    //4fx này để sẵn trong scene
    public GameObject fxStartLevel;
    public GameObject fxVictory;
    public GameObject activeSkillText;
    public GameObject warningBoss;

    //public GameObject timeSpherescreen;
    //public SpriteRenderer spr_timeSpherescreen;


    //noel

    public GameObject playerTriggerSock;


    void Start()
    {
        //spr_timeSpherescreen.transform.localScale = new Vector2((MainScene.Instance.posAnchorRight.localPosition.x * 2) + 0.03f, (MainScene.Instance.posAnchorTop.localPosition.y * 2) + 0.02f);
    }

    //public void ResizeTimeSpherescreen(GameObject timeSpherescreenObj)
    //{


    //}

}
