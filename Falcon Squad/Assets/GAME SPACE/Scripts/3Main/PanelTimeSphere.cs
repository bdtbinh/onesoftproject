﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using Chronos;
//using SkyGameKit;
//using DG.Tweening;

//public class PanelTimeSphere : SkyGameKit.Singleton<PanelTimeSphere>
//{

//    public UISprite bgTimeSphereBar;
//    public UISprite bgBtnTimeSphere;
//    public TweenAlpha tweenAlphaFadeUp, tweenAlphaFadeDown;
//    private bool btnTimeSphere_Ready;
//    GlobalClock clock;

//    public bool timeSpherePlaying;
//    public float valueSlow;
//    public float timeExist;


//    public TweenColor tweenBGfull;
//    float valueTimeGlobalClock;

//    int numCanUse = 1;

//    void Start()
//    {
//        numCanUse = 1 + CacheGame.numUsingSkill;
//        //GameContex.numTimeSphereUse = 0;
//        //
//        bgTimeSphereBar.fillAmount = 0;
//        bgBtnTimeSphere.spriteName = "btn_timefrz_deactive";
//        DOTween.To(() => bgTimeSphereBar.fillAmount, x => bgTimeSphereBar.fillAmount = x, numCanUse * 0.5f, 1.2f).SetDelay(3.6f).OnComplete(() =>
//        {
//            btnTimeSphere_Ready = true;
//            bgBtnTimeSphere.spriteName = "btn_timefrz";

//        });



//        clock = Timekeeper.instance.Clock("Player");



//        valueTimeGlobalClock = 1f / valueSlow;

//    }

   


//    public void Btn_TimeSphere()
//    {


//        Firebase.Analytics.FirebaseAnalytics.LogEvent("Click_TimeSphere");
//        if (numCanUse > 0 && btnTimeSphere_Ready && !PlayerManager.Instance.playerCampaign.Aircraft.aircraftIsStopMove)
//        {
//            CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.UseTimeSphere.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.UseTimeSphere.ToString()) + 1);
//            //daily
//            CacheGame.SetNumberDailyQuestAchieved(GameContext.USE_ACTIVESKILL_EASY_DAILYQUEST, CacheGame.GetNumberDailyQuestAchieved(GameContext.USE_ACTIVESKILL_EASY_DAILYQUEST) + 1);
//            //

//            ShowEffectTextStart();

//            Time.timeScale = valueSlow;
//            clock.localTimeScale = valueTimeGlobalClock;
//            //
//            bgTimeSphereBar.fillAmount -= 0.5f;
//            fillbarNew -= 0.5f;
//            numCanUse--;
//            if (numCanUse < 1)
//            {
//                bgBtnTimeSphere.spriteName = "btn_timefrz_deactive";
//            }
//            //
//            ChangeAlphaUp();

//            EffectList.Instance.timeSpherescreen.SetActive(true);
//            timeSpherePlaying = true;
//            btnTimeSphere_Ready = false;
//            this.Delay((timeExist / valueTimeGlobalClock), () =>
//            {
//                ChangeAlphaDown();
//                Time.timeScale = 0.51f;
//                clock.localTimeScale = 2f;
//                btnTimeSphere_Ready = true;
//                EffectList.Instance.timeSpherescreen.SetActive(false);
//                timeSpherePlaying = false;
//                this.Delay((0.6f), () =>
//                {
//                    Time.timeScale = 1f;
//                });
//            });
//        }
//    }


//    float fillbarNew;

//    public void GetItemTimeSphere()
//    {
//        if (numCanUse < 2)
//        {

//            numCanUse++;
//            btnTimeSphere_Ready = false;
//            fillbarNew = numCanUse * 0.5f;
//            DOTween.To(() => bgTimeSphereBar.fillAmount, x => bgTimeSphereBar.fillAmount = x, fillbarNew, 0.7f).OnComplete(() =>
//            {

//                btnTimeSphere_Ready = true;
//                bgBtnTimeSphere.spriteName = "btn_timefrz";
//            });
//            ;
//        }
//        else
//        {
//            tweenBGfull.enabled = true;
//            tweenBGfull.ResetToBeginning();
//            this.Delay(2f, () =>
//            {
//                tweenBGfull.enabled = false;
//                bgTimeSphereBar.color = Color.white;
//            });
//        }
//    }


   



//    void ChangeAlphaUp()
//    {
//        tweenAlphaFadeUp.gameObject.SetActive(true);
//        tweenAlphaFadeDown.gameObject.SetActive(false);

//        tweenAlphaFadeUp.ResetToBeginning();
//        tweenAlphaFadeUp.enabled = true;
//        tweenAlphaFadeDown.enabled = false;
//    }


//    void ChangeAlphaDown()
//    {
//        tweenAlphaFadeDown.gameObject.SetActive(true);
//        tweenAlphaFadeUp.gameObject.SetActive(false);

//        tweenAlphaFadeDown.ResetToBeginning();
//        tweenAlphaFadeDown.enabled = true;
//        tweenAlphaFadeUp.enabled = false;
//    }

//    public void TweenAlphaDown_Callback()
//    {
//        tweenAlphaFadeUp.gameObject.SetActive(false);
//        tweenAlphaFadeDown.gameObject.SetActive(false);

//    }



//    //-------

//    public void ShowEffectTextStart()
//    {
//        EffectList.Instance.activeSkillText.SetActive(true);
//        EffectList.Instance.activeSkillText.GetComponent<ParticleSystem>().Play(true);

//        this.Delay(2f, () =>
//        {
//            EffectList.Instance.activeSkillText.GetComponent<ParticleSystem>().Stop();
//            EffectList.Instance.activeSkillText.SetActive(false);
//        });
//    }
//}
