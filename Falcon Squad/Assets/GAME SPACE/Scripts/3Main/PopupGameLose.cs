﻿using UnityEngine;
using System.Collections;
using TCore;
using UnityEngine.SceneManagement;
using DG.Tweening;
using EasyMobile;
using Firebase.Analytics;
using Sirenix.OdinInspector;
using SkyGameKit;
using I2.Loc;

public class PopupGameLose : MonoBehaviour
{
    public UISprite sStar1, sStar2, sStar3;

    public UILabel lGetStar1, lGetStar2, lGetStar3;
    //
    public UILabel lNumScore;
    public UILabel lNumCoin;
    public UILabel lNumLevel;

    public UISprite btnVideoAdsSpr;
    public BoxCollider btnVideoAdsBox;
    public GameObject fxVideoAds;

    public GameObject objIconVideoAds;

    public UILabel lNumGoldAddBtnVideoAds;

    public GameObject panelCampaignMode;
    //----Endless
    //
    [Title("Endless", bold: true, horizontalLine: true)]

    public GameObject panelEndLessMode;
    public UILabel lNumWave;
    public UILabel lGoldBonusEndLess;
    public UILabel lPlaytime;
    public UILabel lNumCoinEndLess;

    [Title("Events", bold: true, horizontalLine: true)]
    public GameObject panelEventsMode;
    public UILabel lNumCoinEvents;
    public UILabel lNumScoreEvents;
    public UILabel lNumResourcesEvents;

    void Start()
    {
        //GameContext.isInGameCampaign = false;
        //PvpUtil.SendUpdatePlayer();
        ChangeUiMode();
        SoundManager.SoundGameLose();
        FirebaseLogSpaceWar.LogLevelFail();
        FirebaseLogSpaceWar.LogGoldIn(PanelUITop.Instance.curCoin, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogGemIn(PanelUITop.Instance.curGem, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogTimePlayLevel("" + CacheGame.GetCurrentLevel(), MainScene.Instance.timePlayLevel);

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString()) + PanelUITop.Instance.curCoin);

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString()) + MainScene.Instance.numEnemyKilled);

        CachePvp.Instance.LogEndLevel(CacheGame.GetCurrentLevel(), 0, MainScene.Instance.timePlayLevel, CuongUtils.ChangeDifficultCampaignToInt());

        this.Delay(1.8f, () =>
        {
            if (CacheGame.GetMaxLevel3Difficult() > 2)
            {
                PopupManagerCuong.Instance.ShowPanelBtnVideoTop();
            }
            PopupManagerCuong.Instance.ShowPanelCoinGem();
            //PopupManagerCuong.Instance.ShowShopPopup();
            //if (PopupShop.Instance != null)
            //{
            //    PopupShop.Instance.SetShowGlowBtnShop();
            //}
        }, true);


    }


    //---cương - thay đổi hiện thị nút video khi đã mua gói p.pack

    void ChangeBtnVideoByPPack()
    {
        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            objIconVideoAds.SetActive(false);
        }
        else
        {
            objIconVideoAds.SetActive(true);
        }
    }

    //-------------------- mode choi-------------------------------------------------------------------------------------


    void ChangeUiMode()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
        {
            panelEndLessMode.SetActive(true);
            panelCampaignMode.SetActive(false);
            panelEventsMode.SetActive(false);
            ////--------------------
            if (CacheGame.GetMaxWavesEndlessReached() < PanelUITop.Instance.numWaveEndlessPassed)
            {
                CacheGame.SetMaxWavesEndlessReached(PanelUITop.Instance.numWaveEndlessPassed);
            }
            lNumWave.text = I2.Loc.ScriptLocalization.mission_endless_wavereached.Replace("%{number}", " " + PanelUITop.Instance.numWaveEndlessPassed);
            lGoldBonusEndLess.text = I2.Loc.ScriptLocalization.mission_endless_bonusgold.Replace("%{number}", " " + PanelUITop.Instance.numCoinBonusEndless);
            lPlaytime.text = I2.Loc.ScriptLocalization.mission_endless_playtime.Replace("%{number}", " " + MainScene.Instance.timePlayLevel + " s");


            lNumCoinEndLess.DOUILabelInt(0, PanelUITop.Instance.curCoin, 1f).SetUpdate(true).SetDelay(1f).OnComplete(() =>
            { });

            CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayEndlessEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayEndlessEasy.ToString()) + 1);
            CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayEndlessHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayEndlessHard.ToString()) + 1);
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            panelEndLessMode.SetActive(false);
            panelCampaignMode.SetActive(false);
            panelEventsMode.SetActive(true);
            ////--------------------

            lNumScoreEvents.DOUILabelInt(0, PanelUITop.Instance.curScore, 2f).SetUpdate(true).SetDelay(1.2f);
            lNumResourcesEvents.text = "" + PanelUITop.Instance.numResourcesBonusEvent;

            

            //lNumCoinEvents.text = "" + PanelUITop.Instance.curCoin;

        }
        else
        {
            panelEndLessMode.SetActive(false);
            panelCampaignMode.SetActive(true);
            panelEventsMode.SetActive(false);
            //--------------------
            ChangeBtnVideoByPPack();

            //
            if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
            {
                CacheGame.SetNumLoseInOneLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign(), CacheGame.GetNumLoseInOneLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()) + 1);
                lNumLevel.text = ScriptLocalization.level_upgrade + " " + CacheGame.GetCurrentLevel();
            }
            else
            {
                lNumLevel.text = ScriptLocalization.level_extra_name.Replace("%{number}", CacheGame.GetCurrentLevel().ToString());
            }

            lNumScore.DOUILabelInt(0, PanelUITop.Instance.curScore, 2f).SetUpdate(true).SetDelay(1.2f);
            lNumCoin.DOUILabelInt(0, PanelUITop.Instance.curCoin, 1f).SetUpdate(true).SetDelay(1f).OnComplete(() =>
            {
                if (PanelUITop.Instance.curCoin > 0)
                {
                    lNumGoldAddBtnVideoAds.gameObject.SetActive(true);
                    lNumGoldAddBtnVideoAds.text = "+" + PanelUITop.Instance.curCoin + " " + ScriptLocalization.gold;
                }
            });

            SetNumStar();
            AutoShowOtherPopup();
        }

    }



    //---------------------------------------------------------------------------------------------------------

    //int numShowPackMax;
    void AutoShowOtherPopup()
    {
        Debug.Log("GetNumLoseInOneLevel:lv " + CacheGame.GetCurrentLevel() + "---" + CacheGame.GetDifficultCampaign() + "---" + CacheGame.GetNumLoseInOneLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()));

        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.Campaign) return;

        //PopupManagerCuong.Instance.ShowVideoEndgamePopup();
        if (MainScene.Instance.timePlayLevel > 60 &&
            OsAdsManager.Instance.isRewardedVideoAvailable() &&
            CacheGame.GetNumUseVideoEndGameOneDay() < 5 &&
            CacheGame.GetMaxLevel3Difficult() > 6 &&
            CachePvp.sCClientConfig.turnOnVideoEndGame == 1)
        {
            PopupManagerCuong.Instance.ShowVideoEndgamePopup();
            return;
        }

        if (CacheGame.GetNumLoseInOneLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()) > 2)
        {
            if (isShowStarterPack())
            {
                //PopupManagerCuong.Instance.ShowStarterPackPopup();
                PopupManagerCuong.Instance.Show3PackPopup();
                Popup3Pack.Instance.ShowPackInPopup(TypePackShowInPopup3Pack.StarterPack);
                return;
            }
            else if (isShowVipPack())
            {
                //PopupManagerCuong.Instance.ShowVipPackPopup();
                PopupManagerCuong.Instance.Show3PackPopup();
                Popup3Pack.Instance.ShowPackInPopup(TypePackShowInPopup3Pack.VipPack);
                return;
            }
        }
    }

    bool isShowStarterPack()
    {
        if (CacheGame.GetPurchasedStarterPack() != 1)
        {
            return true;
        }
        return false;
    }

    bool isShowVipPack()
    {
        if (!isShowStarterPack() && CacheGame.GetPurchasedVIPPack() != 1 && CuongUtils.GetMaxPlaneUnlock() < 7)
        {
            return true;
        }
        return false;
    }



    //------------------------------------------------------

    string starShow;
    string starHide = "icon_blackstar_ingame";

    void SetNumStar()
    {

        // =1 la` star da~ tung` show, con` lai la` chua
        int trueFalseShowStar1 = CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 1, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);
        int trueFalseShowStar2 = CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 2, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);
        int trueFalseShowStar3 = CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 3, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);


        switch (CacheGame.GetDifficultCampaign())
        {
            case "Normal":
                starShow = "icon_bluestar_ingame";
                break;
            case "Hard":
                starShow = "icon_goldstar_ingame";
                break;
            case "Hell":
                starShow = "icon_redstar_ingame";
                break;
            default:
                break;
        }
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra)
        {
            starShow = "icon_viostar_ingame";
        }
        // --------Get Set star1----------
        if (trueFalseShowStar1 == 1)
        {
            sStar1.spriteName = starShow;
            lGetStar1.color = Color.white;
        }
        // --------Get Set star2----------
        if (trueFalseShowStar2 == 1)
        {
            sStar2.spriteName = starShow;
            lGetStar2.color = Color.white;
        }
        // --------Get Set star3----------

        if (trueFalseShowStar3 == 1)
        {
            sStar3.spriteName = starShow;
            lGetStar3.color = Color.white;
        }

    }



    public void ButtonRetry()
    {
        SoundManager.PlayClickButton();

        Time.timeScale = 1f;

        //if (CacheGame.GetFirtSessionGame() != 1)
        //{
        //    MainScene.Instance.LoadNewSceneFromMain("Home");
        //    return;
        //}
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.Main_Click_Retry;
            MainScene.Instance.LoadNewSceneFromMain("SelectLevel");
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            GameContext.typeLoadPopupSelectEvent = GameContext.TypeLoadPopupSelectEvent.Main_Click_Retry;
            MainScene.Instance.LoadNewSceneFromMain("Home");
        }
        else
        {
            GameContext.typeLoadPopupSelectEndless = GameContext.TypeLoadPopupSelectEndless.Main_Click_Retry;
            MainScene.Instance.LoadNewSceneFromMain("Home");

        }

        OsAdsManager.Instance.ShowInterstitialAds(OsAdsManager.LocationClickInterstitial.btn_Inter_RetryLose);

    }



    public void BackListLevel()
    {
        SoundManager.PlayClickButton();
        Time.timeScale = 1f;
        GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.NotShow;
        //if (CacheGame.GetFirtSessionGame() != 1)
        //{
        //    MainScene.Instance.LoadNewSceneFromMain("Home");
        //    return;
        //}
        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.Campaign)
        {
            GameContext.typeLoadPopupSelectEndless = GameContext.TypeLoadPopupSelectEndless.NotShow;
            MainScene.Instance.LoadNewSceneFromMain("Home");
        }
        else
        {
            MainScene.Instance.LoadNewSceneFromMain("SelectLevel");
        }
    }

    //
    public void ButtonShop()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowShopPopup();
        if (PopupShop.Instance != null)
        {
            //PopupShop.Instance.ChangeWhenOpenShop();
            PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.Upgrade);
        }
        //
        PopupManagerCuong.Instance.ShowPanelCoinGem();

        //FirebaseLogSpaceWar.LogClickButton("Upgrade");
    }


    //-----------------VIDEO ADS----------------------------------------------------------------------

    public void ButtonVideoAds()
    {
        SoundManager.PlayClickButton();

        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            CallBackFinishVideoAds();
        }
        else
        {
            Debug.Log("ads ready " + Advertising.IsRewardedAdReady());
            FirebaseLogSpaceWar.LogOtherEvent("Ad_ClickAdmob");
            FirebaseLogSpaceWar.LogOtherEvent("Ad_ClickDoubleCoin");
            if (OsAdsManager.Instance.isRewardedVideoAvailable())
            {
                OsAdsManager.Instance.AddRewardedListenner(CallBackFinishVideoAds);
                OsAdsManager.Instance.AddClosedListenner(CallBackClosedVideoAds);
                //
                OsAdsManager.Instance.ShowRewardedAds(OsAdsManager.LocationClickVideo.btn_video_X2CoinLose);
                FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdmob");
                FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdsDoubleCoin");

            }
            else
            {

                if (PopupManagerCuong.Instance.readyShowNotifi)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_ads_not_available, false, 1.3f);
                }

            }
        }

    }


    void CallBackFinishVideoAds()
    {
        btnVideoAdsSpr.spriteName = "btn_viewads_deactive";
        btnVideoAdsSpr.color = EffectList.Instance.colorHideButton;
        btnVideoAdsSpr.color = new Color(1f, 1f, 1f, 0.56f);
        btnVideoAdsSpr.GetComponent<TweenPosition>().enabled = false;
        fxVideoAds.SetActive(false);
        btnVideoAdsBox.enabled = false;
        //
        int curCoinNew = PanelUITop.Instance.curCoin * 2;

        lNumCoin.DOUILabelInt(PanelUITop.Instance.curCoin, curCoinNew, 2f).SetUpdate(true).SetDelay(0.2f);

        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.AddCoinTop(PanelUITop.Instance.curCoin, FirebaseLogSpaceWar.X2VideoMission_why, "level" + CacheGame.GetCurrentLevel());
        }
        else
        {
            PanelUITop.Instance.SetTextCoin(PanelUITop.Instance.curCoin);
        }

        if (!MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            FirebaseLogSpaceWar.LogOtherEvent("Ad_CompAdsDoubleCoin");
        }


    }
    void CallBackClosedVideoAds()
    {

    }

}
