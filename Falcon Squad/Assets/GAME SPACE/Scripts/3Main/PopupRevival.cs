﻿

using UnityEngine;
using System.Collections;
using TCore;
using UnityEngine.SceneManagement;
using DG.Tweening;
using EasyMobile;
using Firebase.Analytics;
using System;
using I2.Loc;

public class PopupRevival : MonoBehaviour
{

    public UILabel lTime;
    public UILabel lNumCoinBtn;

    public UILabel lDiscountRoyalPack;

    PlayerController playerIns;

    public BoxCollider btnVideoAdsBox;
    public GameObject objIconVideoAds;

    Tweener tweenlTimeID = null;

    public Animator animButtonBonusVideo;
    public UILabel lnumLifeBonusVideo;
    int numLifeBonusVideo = 1;

    void Start()
    {
        numLifeBonusVideo = 1;
        lnumLifeBonusVideo.text = "+" + numLifeBonusVideo;
        ChangeBtnVideoByPPack();
        //
        Debug.Log("NumLose " + CacheGame.GetNumLoseInOneLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()) + " LoseActiveBonusLife " + DataChangeUI.Instance.numLoseActiveBonusLife);

        if (CacheGame.GetCurrentLevel() == CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()))
        {
            if (CacheGame.GetNumLoseInOneLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()) >= DataChangeUI.Instance.numLoseActiveBonusLife)
            {
                numLifeBonusVideo = 2;
                lnumLifeBonusVideo.text = "+" + numLifeBonusVideo;
                animButtonBonusVideo.enabled = true;
                FirebaseLogSpaceWar.LogOtherEvent(
                    "Ad_Active2Life"
                );
            }
        }

        //
        playerIns = AllPlayerManager.Instance.playerCampaign.GetComponent<PlayerController>();
        lNumCoinBtn.text = "" + DataChangeUI.Instance.numGemRevival;

        lDiscountRoyalPack.gameObject.SetActive(MinhCacheGame.IsAlreadyBuyRoyalPack());
        if (MinhCacheGame.IsAlreadyBuyRoyalPack())
        {
            lDiscountRoyalPack.text = ScriptLocalization.discount_percent.Replace("%{percent}", "30");
        }
    }

    //--- thay đổi hiện thị nút video khi đã mua gói p.pack

    void ChangeBtnVideoByPPack()
    {
        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            objIconVideoAds.SetActive(false);
        }
        else
        {
            objIconVideoAds.SetActive(true);
        }
    }
    //-----------

    public void StartTimeRevival()
    {
        tweenlTimeID = lTime.DOUILabelInt(10, 0, 11f).SetDelay(3f).SetUpdate(true).OnComplete(() =>
        {
            Debug.Log("complete TimeRevival");

            gameObject.SetActive(false);
            MainScene.Instance.popupLose.SetActive(true);
        });
    }


    // giờ chuyển sang dùng hồi sinh bằng Gem
    public void Btn_UseGem()
    {
        //Debug.LogError("Time.timeScale :" + Time.timeScale);
        if (Time.timeScale == 0)
        {
            SoundManager.PlayClickButton();

            int numGemRevivalUse = DataChangeUI.Instance.numGemRevival;
            if (MinhCacheGame.IsAlreadyBuyRoyalPack())
            {
                numGemRevivalUse = (int)(DataChangeUI.Instance.numGemRevival * 0.7);
            }

            if (CacheGame.GetTotalGem() >= numGemRevivalUse)
            {
                tweenlTimeID.Kill();

                MainScene.Instance.gameFinished = false;
                //
                gameObject.SetActive(false);
                MainScene.Instance.useRevival = true;
                //PanelUITop.Instance.SetTextLife(1);
                AllPlayerManager.Instance.playerCampaign.ChangeNumberLife(1);
                playerIns.PlayerDie();

                Time.timeScale = 1f;
                MainScene.Instance.clockPlayer.localTimeScale = 1f;
                //CacheGame.SetTotalGem(CacheGame.GetTotalGem() - DataChangeUI.Instance.numGemRevival);
                PanelUITop.Instance.SetTextGem(-numGemRevivalUse);
            }
            else
            {
                if (PopupManagerCuong.Instance.readyShowNotifi)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_gem, false, 1.3f);
                }
            }
        }
    }


    public void BackListLevel()
    {
        SoundManager.PlayClickButton();
        Time.timeScale = 1f;
        GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.NotShow;
        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.Campaign)
        {
            MainScene.Instance.LoadNewSceneFromMain("Home");
        }
        else
        {
            MainScene.Instance.LoadNewSceneFromMain("SelectLevel");
        }
        FirebaseLogSpaceWar.LogLevelFail();
        FirebaseLogSpaceWar.LogGoldIn(PanelUITop.Instance.curCoin, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogGemIn(PanelUITop.Instance.curGem, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogTimePlayLevel("" + CacheGame.GetCurrentLevel(), MainScene.Instance.timePlayLevel);
        CachePvp.Instance.LogEndLevel(CacheGame.GetCurrentLevel(), 0, MainScene.Instance.timePlayLevel, CuongUtils.ChangeDifficultCampaignToInt());

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString()) + PanelUITop.Instance.curCoin);

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString()) + MainScene.Instance.numEnemyKilled);
        //GameContext.isInGameCampaign = false;
        //PvpUtil.SendUpdatePlayer();
    }

    public void ButtonRetry()
    {
        SoundManager.PlayClickButton();

        Time.timeScale = 1f;

        GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.Main_Click_Retry;
        MainScene.Instance.LoadNewSceneFromMain("SelectLevel");
        FirebaseLogSpaceWar.LogLevelFail();
        FirebaseLogSpaceWar.LogGoldIn(PanelUITop.Instance.curCoin, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogGemIn(PanelUITop.Instance.curGem, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogTimePlayLevel("" + CacheGame.GetCurrentLevel(), MainScene.Instance.timePlayLevel);

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString()) + PanelUITop.Instance.curCoin);

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString()) + MainScene.Instance.numEnemyKilled);

        CachePvp.Instance.LogEndLevel(CacheGame.GetCurrentLevel(), 0, MainScene.Instance.timePlayLevel, CuongUtils.ChangeDifficultCampaignToInt());
        OsAdsManager.Instance.ShowInterstitialAds(OsAdsManager.LocationClickInterstitial.btn_Inter_RetryRevival);
    }



    public void ButtonVideoAds()
    {
        SoundManager.PlayClickButton();
        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            tweenlTimeID.Kill();
            CallBackFinishVideoAds();
        }
        else
        {
            Debug.Log("ads ready " + Advertising.IsRewardedAdReady());
            // Show it if it's ready
            FirebaseLogSpaceWar.LogOtherEvent("Ad_ClickAdmob");
            FirebaseLogSpaceWar.LogOtherEvent("Ad_ClickSaveLife");
            if (numLifeBonusVideo > 1)
            {
                FirebaseLogSpaceWar.LogOtherEvent("Ad_ClickSave2Life");
            }
            if (OsAdsManager.Instance.isRewardedVideoAvailable() && Time.timeScale == 0)
            {
                tweenlTimeID.Kill();
                OsAdsManager.Instance.AddRewardedListenner(CallBackFinishVideoAds);
                OsAdsManager.Instance.AddClosedListenner(CallBackClosedVideoAds);
                //
                OsAdsManager.Instance.ShowRewardedAds(OsAdsManager.LocationClickVideo.btn_video_Revival);
                //
                FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdmob");
                FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdsSaveLife");
                if (numLifeBonusVideo > 1)
                {
                    FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdsSave2Life");
                }
            }
            else
            {
                if (PopupManagerCuong.Instance.readyShowNotifi)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_ads_not_available, false, 1.3f);
                }

            }
        }

    }


    bool videoCallBackFinish;
    void CallBackFinishVideoAds()
    {
        videoCallBackFinish = true;
        btnVideoAdsBox.enabled = false;
        //
        if (!MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            FirebaseLogSpaceWar.LogOtherEvent("Ad_CompAdsSaveLife");
            //
            if (numLifeBonusVideo > 1)
            {
                FirebaseLogSpaceWar.LogOtherEvent("Ad_CompAdsSave2Life");
            }
        }
        MainScene.Instance.gameFinished = false;
        //
        gameObject.SetActive(false);
        MainScene.Instance.useRevival = true;
        //PanelUITop.Instance.SetTextLife(numLifeBonusVideo);
        AllPlayerManager.Instance.playerCampaign.ChangeNumberLife(numLifeBonusVideo);
        playerIns.PlayerDie();

        Time.timeScale = 1f;
    }

    void CallBackClosedVideoAds()
    {
        if (!videoCallBackFinish)
        {
            gameObject.SetActive(false);
            MainScene.Instance.popupLose.SetActive(true);
        }
        videoCallBackFinish = false;
    }

}
