﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using DG.Tweening;
using DG.Tweening.Core;
using SkyGameKit;
using SWS;
using Sirenix.OdinInspector;
using TCore;
using UnityEngine.SceneManagement;

public class ItemController : MonoBehaviour
{
    public enum TypeItem
    {
        Item,
        Coin
    }

    public TypeItem typeItem_Coin;
    public string typeItem;
    public float speed;

    //
    bool goToPlayer;
    private float goToPlayerSpeed = 5f;
    Transform playerIns;


    Transform tranIns;


    private void Awake()
    {
        tranIns = transform;
    }
    //void Start()
    //{

    //}

    private void OnEnable()
    {
        SetMove();
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case Const.deadZone:
                Despawn();
                break;
            case "GetItem_Zone":
                if (typeItem_Coin == TypeItem.Coin)
                {
                    //				transform.DOMove (col.transform.parent.localPosition, 0.11f).SetUpdate (true);
                    if (!goToPlayer)
                    {
                        playerIns = col.transform.parent;
                        goToPlayer = true;
                    }
                }
                break;
        }
    }


    protected virtual void Update()
    {
        if (goToPlayer && !MainScene.Instance.popupRevival.activeInHierarchy && playerIns != null)
        {
            goToPlayerSpeed += 11f * Time.deltaTime;//Tốc độ tăng đần theo thời gian
            tranIns.position = Vector3.MoveTowards(tranIns.position, playerIns.position, goToPlayerSpeed * Time.deltaTime * MainScene.Instance.clockPlayer.timeScale);
        }
    }


    public void SetMove()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
    }

    public void Despawn()
    {
        if (PoolManager.Pools[Const.itemPoolName].IsSpawned(transform))
        {
            PoolManager.Pools[Const.itemPoolName].Despawn(transform);
            //
            goToPlayer = false;
        }
    }



    public void PlayerTriggerItem(Aircraft aircraftIns)
    {

        switch (typeItem)
        {
            case "Gem":
                Pool_EffectTriggerGem(aircraftIns.transform);
                SoundManager.SoundGetCoinSmall();
                //CacheGame.SetTotalGem(CacheGame.GetTotalGem() + 1);
                PanelUITop.Instance.SetTextGem(1);
                break;
            case "CoinDiamond":
                //Pool_EffectTriggerCoin(aircraftIns.transform);
                PanelUITop.Instance.SetTextCoin(5);

                break;
            case "CoinGold":
                //Pool_EffectTriggerCoin(aircraftIns.transform);
                PanelUITop.Instance.SetTextCoin(3);

                break;
            case "CoinSilver":
                //Pool_EffectTriggerCoin(aircraftIns.transform);
                PanelUITop.Instance.SetTextCoin(2);

                break;
            case "CoinCopper":
                //Pool_EffectTriggerCoin(aircraftIns.transform);
                PanelUITop.Instance.SetTextCoin(1);

                break;
            case "PowerUp":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerPowerUp(aircraftIns.transform);
                GameContext.numItemPowerGet++;
                if (SceneManager.GetActiveScene().name == "LevelTutorial")
                {
                    aircraftIns.AddMainWeaponPowerLevel(8);
                    aircraftIns.AddMainWeaponSpeedLevel(8);
                    aircraftIns.AddMainWeaponQuantityLevel(8);
                    aircraftIns.AddSecondWeaponPowerLevel(8);
                    aircraftIns.AddSecondWeaponSpeedLevel(8);
                }
                else
                {
                    if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
                    {
                        aircraftIns.AddMainWeaponPowerLevel(CacheFireBase.GetNumLevelAdd_When_GetPowerUp, true);
                    }
                    else
                    {
                        aircraftIns.AddMainWeaponPowerLevel(CacheFireBase.GetNumLevelAdd_When_GetPowerUp);
                    }
                    aircraftIns.AddMainWeaponSpeedLevel(CacheFireBase.GetNumLevelAdd_When_GetPowerUp);
                    aircraftIns.AddMainWeaponQuantityLevel(CacheFireBase.GetNumLevelAdd_When_GetPowerUp);

                    aircraftIns.AddSecondWeaponPowerLevel(CacheFireBase.GetNumLevelAdd_When_GetPowerUp);
                    aircraftIns.AddSecondWeaponSpeedLevel(CacheFireBase.GetNumLevelAdd_When_GetPowerUp);
                }
                //
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);
                break;
            case "ActiveSkill":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                aircraftIns.ChangeUsingSkillNumber(1);
                //PanelTimeSphere.Instance.GetItemTimeSphere();
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);
                break;
            case "Life":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                aircraftIns.GetComponentInParent<PlayerInit>().ChangeNumberLife(1);
                //PanelUITop.Instance.SetTextLife(1);
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);
                break;
            case "SpeedShot":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                //playerShootIns.SetChangeSpeedShot(6f);
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString()) + 1);
                break;
            //card------------------------
            case "CardPlane1_BataFD":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                PanelUITop.Instance.SetUiAddAircraftCard(AircraftTypeEnum.BataFD);
                break;
            case "CardPlane2_SkyWraith":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                PanelUITop.Instance.SetUiAddAircraftCard(AircraftTypeEnum.SkyWraith);
                break;
            case "CardPlane3_FuryOfAres":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                PanelUITop.Instance.SetUiAddAircraftCard(AircraftTypeEnum.FuryOfAres);
                break;
            case "CardPlane6_MacBird":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                PanelUITop.Instance.SetUiAddAircraftCard(AircraftTypeEnum.MacBird);
                break;
            case "CardPlane7_TwilightX":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                PanelUITop.Instance.SetUiAddAircraftCard(AircraftTypeEnum.TwilightX);
                break;
            case "Energy":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                PanelUITop.Instance.SetUiAddEnergy();
                break;
            case "CardWing1OfJustice":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                Debug.LogError("CardWing1OfJustice");
                PanelUITop.Instance.SetUiAddWingCard(WingTypeEnum.WingOfJustice);
                break;
            case "CardWing2OfRedemption":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);
                Debug.LogError("CardWing2OfRedemption");
                PanelUITop.Instance.SetUiAddWingCard(WingTypeEnum.WingOfRedemption);
                break;
            case "ItemEvents":
                SoundManager.SoundPlayer_Item();
                Pool_EffectTriggerItem(aircraftIns.transform);

                PanelUITop.Instance.SetTextResourcesEvents(1);
                break;

        }
    }
    //--------------------- Effect-----------------------

    public void Pool_EffectTriggerItem(Transform trans)
    {
        if (EffectList.Instance.playerTriggerItem != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerItem, transform.position, Quaternion.identity);
            effectIns.SetParent(trans);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    public void Pool_EffectTriggerGem(Transform trans)
    {
        if (EffectList.Instance.playerTriggerGem != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerGem, transform.position, Quaternion.identity);
            effectIns.SetParent(trans);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
    public void Pool_EffectTriggerCoin(Transform trans)
    {
        if (EffectList.Instance.playerTriggerCoin != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerCoin, transform.position, Quaternion.identity);
            effectIns.SetParent(trans);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    public void Pool_EffectTriggerDiamond(Transform trans)
    {
        if (EffectList.Instance.playerTriggerDiamond != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerDiamond, transform.position, Quaternion.identity);
            effectIns.SetParent(trans);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    public void Pool_EffectTriggerPowerUp(Transform trans)
    {
        if (EffectList.Instance.playerTriggerPowerUp != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerTriggerPowerUp, transform.position, Quaternion.identity);
            effectIns.SetParent(trans);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

}




