﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using SkyGameKit;
using Sirenix.OdinInspector;
using Chronos;
using DG.Tweening;
using Firebase.Analytics;
using TCore;
using UnityEngine.SceneManagement;
using Mp.Pvp;

public class MainScene : SkyGameKit.Singleton<MainScene>
{
    public GameObject popupPause;
    public GameObject popupLose;
    public GameObject popupWin;
    public GameObject popupRevival;
    public GameObject popupFinishTutorial;
    public PanelSkillController skillControllerScript;

    public Transform posAnchorLeft;
    public Transform posAnchorRight;
    public Transform posAnchorTop;
    public Transform posAnchorBottom;
    //
    public Transform posAnchorTopLeft;
    public Transform posAnchorBottomRight;

    //public Transform parent_UbhPool;

    public Camera cameraTk2d;
    //
    public bool gameStopping;
    public bool gameFinished;

    public bool playerReloading;
    public bool playerStopMove;

    public bool activeSkillIsPlaying;

    public bool bossDead;
    //dung khi su dung hoi sinh
    public bool useRevival;


    //---dung` cho popup win
    public bool completeWithoutDying = true;
    public int totalEnemyCanDie = 0;
    public int numEnemyKilled = 0;
    //
    public int numUseRevival = 0;
    //[DisplayAsString]
    //public PlayerController playerControllerScript;
    //
    [DisplayAsString]
    public GlobalClock clockPlayer;

    //public int indexDan;

    //
    //public tk2dTextMesh lTimePlay;
    public int timePlayLevel = 0;


    private void Awake()
    {
        //if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign || GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
        //{
        GameContext.isInGameCampaign = true;
        //}

        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.pvp && GameContext.modeGamePlay != GameContext.ModeGamePlay.Tournament && GameContext.modeGamePlay != GameContext.ModeGamePlay.PVP2vs2)
        {
            //--InitPopup từ đầu
            PopupManagerCuong.Instance.ShowPanelCoinGem();
            PopupManagerCuong.Instance.HidePanelCoinGem();

            PopupManagerCuong.Instance.ShowShopPopup();
            PopupManagerCuong.Instance.HideShopPopup();

            if (CacheGame.GetMaxLevel3Difficult() > 2)
            {
                PopupManagerCuong.Instance.ShowPanelBtnVideoTop();
                PopupManagerCuong.Instance.HidePanelBtnVideoTop();
            }

        }
    }

    void Start()
    {
        //Debug.LogError("GetCurrentWorld: " + CacheGame.GetCurrentWorld());
        Debug.Log("NumLose " + CacheGame.GetNumLoseInOneLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()) + " NumLifeBonus" + DataChangeUI.Instance.numLoseActiveBonusLife);
        SoundManager.StopMainBG();
        SoundManager.StopHomeBG();
        SoundManager.PlayMain2BG();

        GameContext.typeShowBarFromScene = GameContext.TypeShowBarFromScene.Main;

        Time.timeScale = 1f;


        LevelManager.Instance.OnGameStateChange += SetupStateFinishGame;

        clockPlayer = Timekeeper.instance.Clock("Player");
        gameStopping = false;

        StartCoroutine("TimePlayGame");

        FirebaseLogSpaceWar.LogLevelStart();

        readyClickBack = true;
        new CSSceneLoaded(2).Send();
    }


    public bool CanShowPopupRate()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.LogError("Network not found");
            return false;
        }

        if (System.DateTime.Now.Day == MinhCacheGame.GetLastDayRate())
        {
            Debug.LogError("Is the same day");
            return false;
        }

        if (CacheGame.GetCurrentLevel() < 5)
        {
            Debug.LogError("chua den level 5");
            return false;
        }

        if (MinhCacheGame.GetDayCountRate() >= 3)
        {
            Debug.LogError("DayCount > 3");
            return false;
        }
        return !MinhCacheGame.IsAlreadyRate();
    }
    //
    void OnApplicationPause()
    {

#if UNITY_EDITOR
        //		Debug.LogError (System.DateTime.Now.Day + " + " + System.DateTime.Now.Hour);
#else
		if (!gameFinished && !playerStopMove) {
		SoundManager.PlayShowPopup ();
		//cameraRoot.SetActive (true);
		popupPause.SetActive (true);
		popupPause.GetComponent<PopupSetting> ().Change_When_OpenPopup ();
		gameStopping = true;
		Time.timeScale = 0;
		Debug.LogError ("Change timeScale = " + Time.timeScale);
		}
#endif

    }


    //----------------------------------------------------------------

    private bool readyClickBack;
    float fRun = 0;

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && readyClickBack)
        {
            readyClickBack = false;

            this.Delay(0.2f, () =>
            {
                readyClickBack = true;
            }, true);
            if (PopupManagerCuong.Instance.IsActivePopupLoadingPurchase())
            {
                return;
            }
            if (PopupManagerCuong.Instance.IsActiveShopPopup())
            {
                PopupManagerCuong.Instance.HideShopPopup();
            }
            //if (PopupManagerCuong.Instance.IsActiveItemRewardPopup())
            //{
            //    //PopupManagerCuong.Instance.HideShopPopup();
            //}
            else if (PopupManager.Instance.IsClaimPopupActive())
            {
                //PopupManager.Instance.HideVideoRewardPopup();
            }
            else if (PopupManager.Instance.IsVideoRewardPopupActive())
            {
                PopupManager.Instance.HideVideoRewardPopup();
            }

            else if (PopupManagerCuong.Instance.IsActiveVideoEndgamePopup())
            {
                PopupManagerCuong.Instance.HideVideoEndgamePopup();
            }
            else if (PopupManagerCuong.Instance.IsActive3PackPopup())
            {
                PopupManagerCuong.Instance.Hide3PackPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveCardPackPopup())
            {
                PopupManagerCuong.Instance.HideCardPackPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveRatePopupPopup())
            {
                PopupManagerCuong.Instance.HideRatePopup();
            }
            else if (popupWin.activeInHierarchy)
            {
                SoundManager.PlayClickButton();
                Time.timeScale = 1f;
                //if (CacheGame.GetFirtSessionGame() != 1)
                //{
                //    MainScene.Instance.LoadNewSceneFromMain("Home");
                //    return;
                //}
                GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.NotShow;
                if (GameContext.modeGamePlay != GameContext.ModeGamePlay.Campaign)
                {
                    LoadNewSceneFromMain("Home");
                }
                else
                {
                    LoadNewSceneFromMain("SelectLevel");
                }
            }
            else if (popupLose.activeInHierarchy)
            {
                SoundManager.PlayClickButton();
                Time.timeScale = 1f;
                GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.NotShow;

                if (GameContext.modeGamePlay != GameContext.ModeGamePlay.Campaign)
                {
                    GameContext.typeLoadPopupSelectEndless = GameContext.TypeLoadPopupSelectEndless.NotShow;
                    LoadNewSceneFromMain("Home");
                }
                else
                {
                    LoadNewSceneFromMain("SelectLevel");
                }
            }
            else if (popupRevival.activeInHierarchy)
            {
                SoundManager.PlayClickButton();
                Time.timeScale = 1f;
                GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.NotShow;
                if (GameContext.modeGamePlay != GameContext.ModeGamePlay.Campaign)
                {
                    LoadNewSceneFromMain("Home");
                }
                else
                {
                    LoadNewSceneFromMain("SelectLevel");
                }
                FirebaseLogSpaceWar.LogLevelFail();
                FirebaseLogSpaceWar.LogGoldIn(PanelUITop.Instance.curCoin, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
                FirebaseLogSpaceWar.LogGemIn(PanelUITop.Instance.curGem, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
                FirebaseLogSpaceWar.LogTimePlayLevel("" + CacheGame.GetCurrentLevel(), MainScene.Instance.timePlayLevel);

                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString()) + PanelUITop.Instance.curCoin);
                CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString()) + PanelUITop.Instance.curCoin);
                CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString()) + PanelUITop.Instance.curCoin);

                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString()) + MainScene.Instance.numEnemyKilled);
                CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString()) + MainScene.Instance.numEnemyKilled);
                CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString()) + MainScene.Instance.numEnemyKilled);

                CachePvp.Instance.LogEndLevel(CacheGame.GetCurrentLevel(), 0, MainScene.Instance.timePlayLevel, CuongUtils.ChangeDifficultCampaignToInt());
                //GameContext.isInGameCampaign = false;
                //PvpUtil.SendUpdatePlayer();
            }
            else if (popupPause.activeInHierarchy)
            {
                gameStopping = false;
                Time.timeScale = 1f;
                SoundManager.PlayClickButton();
                popupPause.SetActive(false);
            }
            else
            {
                if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
                {
                    if (!activeSkillIsPlaying)
                    {
                        if (CachePvp.canPause)
                        {
                            SoundManager.PlayShowPopup();
                            popupPause.SetActive(true);
                            popupPause.GetComponent<PopupSetting>().Change_When_OpenPopup();
                        }
                    }
                }
                else
                {
                    if (!gameFinished && !playerStopMove && !activeSkillIsPlaying)
                    {
                        SoundManager.PlayShowPopup();
                        popupPause.SetActive(true);
                        popupPause.GetComponent<PopupSetting>().Change_When_OpenPopup();
                        gameStopping = true;
                        Time.timeScale = 0;
                        Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
                    }
                }
            }
        }
    }


    //----------------------------------------------------------------

    public void LoadNewSceneFromMain(string nameNewScene)
    {
        if (!LoadingSceneManager.Instance.loadingOpened)
        {
            SkyGameKit.Const.ExplosionPoolTransform.gameObject.SetActive(false);
            LoadingSceneManager.Instance.LoadSceneMainUI(nameNewScene);
        }
    }
    //----------------------------------------------------------------


    // keyState = 1 la lost  , keyState = 2 la win
    public void SetupStateFinishGame(GameStateType state)
    {
        if (!gameFinished)
        {
            gameFinished = true;

            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
            {

            }
            else if (SceneManager.GetActiveScene().name == "LevelTutorial")
            {
                this.Delay(2f, () =>
                {
                    popupFinishTutorial.SetActive(true);
                    Time.timeScale = 0;
                    Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
                }, true);
            }
            else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.TryPlane)
            {
                this.Delay(1f, () =>
                {
                    if (GameContext.typeOpenSceneFromTry == GameContext.TypeOpenSceneFromTry.SelectLevel)
                    {
                        LoadNewSceneFromMain("SelectLevel");
                    }
                    else if (GameContext.typeOpenSceneFromTry == GameContext.TypeOpenSceneFromTry.Home)
                    {
                        LoadNewSceneFromMain("Home");
                    }
                }, true);
            }
            else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
            {
                if (state == GameStateType.GameOver)
                {

                    this.Delay(1f, () =>
                    {
                        popupLose.SetActive(true);
                        Time.timeScale = 0;
                        Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
                    }, true);
                }
                else if (state == GameStateType.Victory)
                {
                    this.Delay(1f, () =>
                    {
                        popupLose.SetActive(true);
                        Time.timeScale = 0;
                        Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
                    }, true);
                }

            }
            else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
            {
                if (state == GameStateType.GameOver)
                {
                    numUseRevival++;
                    if (numUseRevival > 1)
                    {

                        if (!AllPlayerManager.Instance.playerCampaign.usingAircraftBackup && CacheGame.GetMaxLevel3Difficult() > 13 && GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
                        {
                            PopupManagerCuong.Instance.ShowCallingBackUpPopup();
                            AllPlayerManager.Instance.playerCampaign.usingAircraftBackup = true;
                        }
                        else
                        {
                            Debug.LogError("show lose " + numUseRevival);
                            this.Delay(1f, () =>
                            {
                                popupLose.SetActive(true);
                            }, true);
                        }

                    }
                    else
                    {
                        popupRevival.SetActive(true);
                        popupRevival.GetComponent<PopupRevival>().StartTimeRevival();
                    }

                    this.Delay(1f, () =>
                    {
                        Time.timeScale = 0;
                        Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
                    }, true);
                }
                else if (state == GameStateType.Victory)
                {
                    this.Delay(5f, () =>
                    {
                        popupWin.SetActive(true);
                    }, true);


                    //playerControllerScript.PlayerWin();
                    AllPlayerManager.Instance.playerCampaign.playerControllerScript.PlayerWin();
                    //daily
                    CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PassLevelEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PassLevelEasy.ToString()) + 1);
                    CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PassLevelHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PassLevelHard.ToString()) + 1);
                    //
                }
            }
            else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
            {
                if (state == GameStateType.GameOver)
                {
                    this.Delay(1f, () =>
                    {
                        popupLose.SetActive(true);
                    }, true);
                    this.Delay(1f, () =>
                    {
                        Time.timeScale = 0;
                        Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
                    }, true);
                }
                else if (state == GameStateType.Victory)
                {
                    this.Delay(1f, () =>
                    {
                        popupLose.SetActive(true);
                    }, true);
                    this.Delay(1f, () =>
                    {
                        Time.timeScale = 0;
                        Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
                    }, true);
                }

            }

            //-------------
        }

    }



    // những obj này sẽ được nhân với giá trị time scale lớn hơn khi dùng EMP
    public void NoteObjPlayerChangeTime()
    {
        //Debug.Log("Note Lại Obj cần đổi time khi dùng EMP");
    }




    //Tính time play level

    IEnumerator TimePlayGame()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(1f);
            timePlayLevel++;
        }
    }


}
