﻿using UnityEngine;
using System.Collections;
using TCore;
using UnityEngine.SceneManagement;
using DG.Tweening;
using EasyMobile;
using Firebase.Analytics;
using Sirenix.OdinInspector;
using SkyGameKit;
using I2.Loc;

public class PopupGameComplete : MonoBehaviour
{
    public GameObject panelFirstRewardsLevel;
    public GameObject panelObjective;
    public GameObject panelSumary;

    public UISprite sStar1, sStar2, sStar3;

    public UILabel lGetStar1, lGetStar2, lGetStar3;
    //
    public UILabel lNumScore;
    public UILabel lNumCoin;
    public UILabel lNumLevel;

    public UI2DSprite btnVideoAdsSpr;

    public BoxCollider btnVideoAdsBox;
    public GameObject fxVideoAds;
    public UILabel lTextInBtnVideoAds;
    public UILabel lNumGoldAddBtnVideoAds;
    public Sprite spriteHideBtnAds;
    int valueNhanCoin = 2;
    int numCoinAddVideoAds;

    public GameObject objIconVideoAds;
    public GameObject objBtnNext;

    //

    void Start()
    {
        SetShowFirstRewards();

        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            if (CacheGame.GetCurrentLevel() == CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) && (CacheGame.GetCurrentLevel() < GameContext.TOTAL_LEVEL + 1))
            {
                CacheGame.SetMaxLevel(CacheGame.GetDifficultCampaign(), CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) + 1);
            }
        }
        else if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra)
        {
            objBtnNext.SetActive(false);
            CacheGame.SetPassedLevelExtra(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign(), 1);
        }

        FirebaseLogSpaceWar.LogLevelComplete();
        FirebaseLogSpaceWar.LogLevelCompleteLv5();
        FirebaseLogSpaceWar.LogLevelCompleteLv10();
        FirebaseLogSpaceWar.LogTimePlayLevel("" + CacheGame.GetCurrentLevel(), MainScene.Instance.timePlayLevel);

        FirebaseLogSpaceWar.LogGoldIn(PanelUITop.Instance.curCoin, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());
        FirebaseLogSpaceWar.LogGemIn(PanelUITop.Instance.curGem, FirebaseLogSpaceWar.Mission_why, "level" + CacheGame.GetCurrentLevel());

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString()) + PanelUITop.Instance.curCoin);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString()) + PanelUITop.Instance.curCoin);

        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyEasy.ToString()) + MainScene.Instance.numEnemyKilled);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.KillEnemyHard.ToString()) + MainScene.Instance.numEnemyKilled);
        //this.Delay(1f, () =>
        //{
        if (CacheGame.GetMaxLevel3Difficult() > 2)
        {
            PopupManagerCuong.Instance.ShowPanelBtnVideoTop();
        }
        PopupManagerCuong.Instance.ShowPanelCoinGem();
        //PopupManagerCuong.Instance.ShowShopPopup();
        //if (PopupShop.Instance != null)
        //{
        //    PopupShop.Instance.SetShowGlowBtnShop();
        //}
        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.AddCoinTop();
            PanelCoinGem.Instance.AddGemTop();
        }

        ChangeUiCampaign();

        //}, true);

        //this.Delay(0.1f, () =>
        //{
        SetNumStar();

        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            string starsLog = CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 1, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode) + ","
        + CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 2, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode) + ","
        + CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 3, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);

            CachePvp.Instance.LogEndLevel(CacheGame.GetCurrentLevel(), 1, MainScene.Instance.timePlayLevel, CuongUtils.ChangeDifficultCampaignToInt(), starsLog);
            //}, true);
        }
        ChangeBtnVideoByPPack();
    }



    //---cương - thay đổi hiện thị nút video khi đã mua gói p.pack

    void ChangeBtnVideoByPPack()
    {
        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            objIconVideoAds.SetActive(false);
        }
        else
        {
            objIconVideoAds.SetActive(true);
        }
    }

    //-------------------- SetFirst-------------------------------------------------------------------------------------

    bool isReceivedFirstRewards()
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            return CacheGame.GetCurrentLevel() != CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign());
        }
        else
        {
            return CacheGame.GetPassedLevelExtra(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()) == 1;
        }
    }

    void SetShowFirstRewards()
    {
        if (!isReceivedFirstRewards() && CacheGame.GetDifficultCampaign() == GameContext.DIFFICULT_NOMAL)
        {
            panelFirstRewardsLevel.SetActive(true);
            panelObjective.transform.localPosition = new Vector3(0f, -75f, 0f);
            panelSumary.transform.localPosition = new Vector3(0f, -450f, 0f);
        }
        else
        {
            panelFirstRewardsLevel.SetActive(false);
            panelObjective.transform.localPosition = new Vector3(0f, 50f, 0f);
            panelSumary.transform.localPosition = new Vector3(0f, -325f, 0f);
        }
    }
    //-------------------- Campaign-------------------------------------------------------------------------------------
    void ChangeUiCampaign()
    {
        AutoShowOtherPopup();

        if (CacheGame.GetTotalShowX3Coin() < 5 && CacheGame.GetMaxLevel3Difficult() > 2)
        {
            RandomShowX4Coin();
        }
        //
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            lNumLevel.text = ScriptLocalization.level_upgrade + " " + CacheGame.GetCurrentLevel();
        }
        else
        {
            lNumLevel.text = ScriptLocalization.level_extra_name.Replace("%{number}", CacheGame.GetCurrentLevel().ToString());
        }

        lNumScore.DOUILabelInt(0, PanelUITop.Instance.curScore, 2f).SetDelay(0.3f);
        lNumCoin.DOUILabelInt(0, PanelUITop.Instance.curCoin, 1f).SetDelay(0.5f).OnComplete(() =>
        {
            lNumGoldAddBtnVideoAds.gameObject.SetActive(true);
            numCoinAddVideoAds = PanelUITop.Instance.curCoin * valueNhanCoin - PanelUITop.Instance.curCoin;
            lNumGoldAddBtnVideoAds.text = "+" + numCoinAddVideoAds + " " + ScriptLocalization.gold;
        });
        SoundManager.PlayGameWinPopup();
        //}).setIgnoreTimeScale(true);
    }


    void AutoShowOtherPopup()
    {
        if (MainScene.Instance.CanShowPopupRate())
        {
            PopupManagerCuong.Instance.ShowRatePopup();
            return;
        }

        Debug.LogError("timePlayLevel: " + MainScene.Instance.timePlayLevel);
        if (MainScene.Instance.timePlayLevel > 60 &&
            OsAdsManager.Instance.isRewardedVideoAvailable() &&
            CacheGame.GetNumUseVideoEndGameOneDay() < 5 &&
            CacheGame.GetMaxLevel3Difficult() > 6 &&
            CachePvp.sCClientConfig.turnOnVideoEndGame == 1)
        {
            PopupManagerCuong.Instance.ShowVideoEndgamePopup();
        }
    }




    //---------------------------------------------------------------------------------------------------------
    string starShow;

    void SetNumStar()
    {

        // =1 la` star da~ tung` show, con` lai la` chua

        int trueFalseShowStar1 = CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 1, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);
        int trueFalseShowStar2 = CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 2, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);
        int trueFalseShowStar3 = CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 3, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);


        switch (CacheGame.GetDifficultCampaign())
        {
            case GameContext.DIFFICULT_NOMAL:
                starShow = "icon_bluestar_ingame";
                break;
            case GameContext.DIFFICULT_HARD:
                starShow = "icon_goldstar_ingame";
                break;
            case GameContext.DIFFICULT_HELL:
                starShow = "icon_redstar_ingame";
                break;
            default:
                break;
        }
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra)
        {
            starShow = "icon_viostar_ingame";
        }
        float timeDelayShowStar = 0.6f;

        // --------Get Set star1----------
        if (trueFalseShowStar1 == 1)
        {
            sStar1.spriteName = starShow;
            lGetStar1.color = Color.white;
        }
        else
        {
            CacheGame.SetNumStarLevel(CacheGame.GetCurrentLevel(), 1, 1, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);
            //
            LeanTween.delayedCall(timeDelayShowStar, () =>
            {
                sStar1.spriteName = starShow;
            }).setIgnoreTimeScale(true);
            sStar1.transform.DOScale(2, 0.6f).SetDelay(timeDelayShowStar).OnComplete(() =>
            {
                sStar1.transform.localScale = Vector3.one;
                lGetStar1.color = Color.white;
                SoundManager.SoundStarWin();
            });
            timeDelayShowStar += 0.5f;
        }

        // --------Get Set star2----------
        if (trueFalseShowStar2 == 1)
        {
            sStar2.spriteName = starShow;
            lGetStar2.color = Color.white;
        }
        else
        {
            if (MainScene.Instance.completeWithoutDying)
            {
                CacheGame.SetNumStarLevel(CacheGame.GetCurrentLevel(), 2, 1, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);
                //
                LeanTween.delayedCall(timeDelayShowStar, () =>
                {
                    sStar2.spriteName = starShow;
                }).setIgnoreTimeScale(true);
                sStar2.transform.DOScale(2, 0.6f).SetDelay(timeDelayShowStar).OnComplete(() =>
                {
                    sStar2.transform.localScale = Vector3.one;
                    lGetStar2.color = Color.white;
                    SoundManager.SoundStarWin();
                });
                timeDelayShowStar += 0.5f;
            }

        }

        // --------Get Set star3----------

        if (trueFalseShowStar3 == 1)
        {
            sStar3.spriteName = starShow;
            lGetStar3.color = Color.white;
        }
        else
        {
            if (MainScene.Instance.numEnemyKilled >= MainScene.Instance.totalEnemyCanDie)
            {
                CacheGame.SetNumStarLevel(CacheGame.GetCurrentLevel(), 3, 1, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);

                LeanTween.delayedCall(timeDelayShowStar, () =>
                {
                    sStar3.spriteName = starShow;
                }).setIgnoreTimeScale(true);
                sStar3.transform.DOScale(2, 0.6f).SetDelay(timeDelayShowStar).OnComplete(() =>
                {
                    sStar3.transform.localScale = Vector3.one;
                    lGetStar3.color = Color.white;
                    SoundManager.SoundStarWin();
                });
            }
        }

    }



    int valueRandom;

    void RandomShowX4Coin()
    {
        valueRandom = Random.Range(1, 100);

        //Debug.Log("valueRandom" + valueRandom);
        if (valueRandom > 70 || GameContext.IsAdmobHigherPrice())
        {
            lTextInBtnVideoAds.text = ScriptLocalization.x2Gold.Replace("x2", "x4");

            valueNhanCoin = 4;
            CacheGame.SetTotalShowX3Coin(CacheGame.GetTotalShowX3Coin() + 1);

            btnVideoAdsSpr.GetComponent<Animator>().enabled = true;


            FirebaseLogSpaceWar.LogOtherEvent(
                "Ad_ActiveX4Coin"
            );

        }
    }

    //------------------------Button-----------------------------------------------------------------------

    public void ButtonRetry()
    {
        SoundManager.PlayClickButton();

        Time.timeScale = 1f;


        GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.Main_Click_Retry;

        MainScene.Instance.LoadNewSceneFromMain("SelectLevel");
        OsAdsManager.Instance.ShowInterstitialAds(OsAdsManager.LocationClickInterstitial.btn_Inter_RetryWin);
    }


    public void ButtonNextLevel()
    {
        SoundManager.PlayClickButton();

        Time.timeScale = 1f;

        if (CacheGame.GetFirtSessionGame() != 1 && CacheGame.GetMaxLevel3Difficult() == 4)
        {
            MainScene.Instance.LoadNewSceneFromMain("Home");
            CacheGame.SetFirtSessionGame(1);
            return;
        }
        GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.Main_Click_Next;
        MainScene.Instance.LoadNewSceneFromMain("SelectLevel");

        OsAdsManager.Instance.ShowInterstitialAds(OsAdsManager.LocationClickInterstitial.btn_Inter_NextWin);
    }



    public void BackListLevel()
    {
        SoundManager.PlayClickButton();
        Time.timeScale = 1f;
        //if (CacheGame.GetFirtSessionGame() != 1)
        //{
        //    MainScene.Instance.LoadNewSceneFromMain("Home");
        //    return;
        //}
        GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.NotShow;
        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.Campaign)
        {
            MainScene.Instance.LoadNewSceneFromMain("Home");
        }
        else
        {
            MainScene.Instance.LoadNewSceneFromMain("SelectLevel");
        }
    }


    public void ButtonShop()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowShopPopup();
        if (PopupShop.Instance != null)
        {
            PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.Upgrade);
        }
        //
        PopupManagerCuong.Instance.ShowPanelCoinGem();
        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.AddCoinTop();
            PanelCoinGem.Instance.AddGemTop();
        }

        //FirebaseLogSpaceWar.LogClickButton("Upgrade");
        //
    }


    //-----------------VIDEO ADS----------------------------------------------------------------------

    public void ButtonVideoAds()
    {
        SoundManager.PlayClickButton();
        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            CallBackFinishVideoAds();
        }
        else
        {
            FirebaseLogSpaceWar.LogOtherEvent("Ad_ClickAdmob");
            FirebaseLogSpaceWar.LogOtherEvent("Ad_ClickDoubleCoin");
            if (valueNhanCoin > 2)
            {
                FirebaseLogSpaceWar.LogOtherEvent("Ad_ClickX4Coin");
            }
            if (OsAdsManager.Instance.isRewardedVideoAvailable())
            {
                OsAdsManager.Instance.AddRewardedListenner(CallBackFinishVideoAds);
                OsAdsManager.Instance.AddClosedListenner(CallBackClosedVideoAds);
                //
                OsAdsManager.Instance.ShowRewardedAds(OsAdsManager.LocationClickVideo.btn_video_X2CoinWin);
                FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdmob");
                FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdsDoubleCoin");
                if (valueNhanCoin > 2)
                {
                    FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdsX4Coin");
                }
            }
            else
            {
                if (PopupManagerCuong.Instance.readyShowNotifi)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_ads_not_available, false, 1.3f);
                }
            }
        }

    }



    void CallBackFinishVideoAds()
    {
        btnVideoAdsSpr.color = EffectList.Instance.colorHideButton;
        btnVideoAdsSpr.GetComponent<TweenPosition>().enabled = false;
        btnVideoAdsSpr.GetComponent<Animator>().enabled = false;
        btnVideoAdsSpr.sprite2D = spriteHideBtnAds;

        btnVideoAdsBox.enabled = false;
        fxVideoAds.SetActive(false);
        //
        lNumCoin.DOUILabelInt(PanelUITop.Instance.curCoin, PanelUITop.Instance.curCoin * valueNhanCoin, 2f).SetDelay(0.2f);

        if (PanelCoinGem.Instance != null)
        {
            if (valueNhanCoin > 2)
            {
                PanelCoinGem.Instance.AddCoinTop(numCoinAddVideoAds, FirebaseLogSpaceWar.X4VideoMission_why, "level" + CacheGame.GetCurrentLevel());
            }
            else
            {
                PanelCoinGem.Instance.AddCoinTop(numCoinAddVideoAds, FirebaseLogSpaceWar.X2VideoMission_why, "level" + CacheGame.GetCurrentLevel());
            }
        }
        else
        {
            PanelUITop.Instance.SetTextCoin(numCoinAddVideoAds);
        }


        if (!MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            FirebaseLogSpaceWar.LogOtherEvent("Ad_CompAdsDoubleCoin");
            if (valueNhanCoin > 2)
            {
                FirebaseLogSpaceWar.LogOtherEvent("Ad_CompAdsX4Coin");
            }
        }

    }

    void CallBackClosedVideoAds()
    {

    }


}

