﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;

public class PanelBossHealbar : Singleton<PanelBossHealbar>
{

    public GameObject bgHealbar_Border;
    public UISprite bgHealbar_Filled;

    public UILabel lTimerBossEvent;

    float maxHP, currHP;

    void Start()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            bgHealbar_Border.transform.localPosition = new Vector3(0f, -111f, 0f);
        }
    }



    public void ShowBossHealbar(int maxHpBoss)
    {
        bgHealbar_Filled.fillAmount = 1f;
        bgHealbar_Border.SetActive(true);
        maxHP = (float)maxHpBoss;
    }

    public void HideBossHealbar()
    {
        //		bgHealbar_Filled.fillAmount == 1;
        bgHealbar_Border.SetActive(false);
    }



    public void ChangeBossHealbar(int currHPBoss, int maxHPBoss)
    {
        currHP = (float)currHPBoss;
        bgHealbar_Filled.fillAmount = (currHP / maxHPBoss);
        if (bgHealbar_Filled.fillAmount < 0)
        {
            bgHealbar_Filled.fillAmount = 0;
        }
    }
}
