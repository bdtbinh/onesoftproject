﻿using UnityEngine.UI;
using UnityEngine;
using UniRx;
using SkyGameKit;
using Sirenix.OdinInspector;
using System.Collections;
using TCore;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class PanelUITop : Singleton<PanelUITop>
{

    public UILabel lnumScore;
    public UILabel lnumLife;
    public UILabel lnumCoin;
    public UILabel lnumGem;

    public GameObject objectUICampaign;
    public GameObject objectButtonPause;

    public GameObject lifePanel;

    public System.Action<int> onScoreChange;
    public System.Action<int> onLifeChange;

    [DisplayAsString]
    public int curScore;
    [DisplayAsString]
    public int curCoin;
    [DisplayAsString]
    public int curLife;
    [DisplayAsString]
    public int curGem;
    //[DisplayAsString]
    //public int curCandyEvents;

    //
    [DisplayAsString]
    public int numCoinBonusEndless;
    [DisplayAsString]
    public int numWaveEndlessPassed;

    [DisplayAsString]
    public int numResourcesBonusEvent;
    [DisplayAsString]
    public int numWaveEventPassed;
    [DisplayAsString]
    public int numEventResourcesItemDrop = 0; // biến này chỉ dùng để chặn số lượng item rơi ra tối đa
    //ui Ăn được item card
    public GameObject panelUiAddResources;
    public UISprite sPlaneAddResources;
    //events
    public UILabel lTimeEvents;
    public UILabel lNumEventResources;

    public GameObject panelUiAddEnergy;


    private void Awake()
    {
        base.Awake();
    }

    void Start()
    {
        //GameContext.modeGamePlay = GameContext.ModeGamePlay.Events;

        SetTextCoin();
        SetTextGem();
        // hàm này để gán lại biến cache xem có còn đang trong thời gian event hay không 
        MinhCacheGame.UpdateIsInEvent(GameContext.EVENT_SUMMER_HOLIDAY_2019);

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            objectUICampaign.SetActive(false);
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
            {
                objectButtonPause.transform.localPosition = new Vector3(323f, -143f, 0f);
            }
            else
            {
                objectButtonPause.transform.localPosition = new Vector3(323f, -122f, 0f);
            }
            PopupManager.Instance.ShowUIPVPPopup();
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            lTimeEvents.gameObject.SetActive(true);
            lNumEventResources.gameObject.SetActive(true);
            SetTextResourcesEvents(0);
            StartCoroutine("TimePlayEventsMode");
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            if (MinhCacheGame.IsInEvent(GameContext.EVENT_SUMMER_HOLIDAY_2019))
            {
                lNumEventResources.gameObject.SetActive(true);
                SetTextResourcesEvents(0);
            }
        }
        if (SceneManager.GetActiveScene().name == "LevelTutorial")
        {
            lifePanel.SetActive(false);
            objectButtonPause.SetActive(false);
        }
    }

    #region --- mode chơi events--------
    const int maxTimePlayEvents = 302;
    int timePlayLevel;
    IEnumerator TimePlayEventsMode()
    {
        lTimeEvents.text = (maxTimePlayEvents - timePlayLevel) + " s";
        while (true)
        {
            yield return new WaitForSeconds(1f);
            timePlayLevel++;
            lTimeEvents.text = (maxTimePlayEvents - timePlayLevel) + " s";

            if (maxTimePlayEvents - timePlayLevel == 9)
            {
                lTimeEvents.color = Color.red;
            }
            if (maxTimePlayEvents - timePlayLevel <= 0)
            {
                if (LevelManager.Instance.GameState == GameStateType.Playing)
                {
                    LevelManager.Instance.OnGameStateChange(GameStateType.GameOver);
                }
                lTimeEvents.text = "0 s";
                break;
            }
        }
    }
    #endregion

    //System.IDisposable disposableEvent;

    public void SetTextResourcesEvents(int numResourcesAdd)
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            MinhCacheGame.AddRemoveEventResources(EventItemType.IceCream_4_2019.ToString(), numResourcesAdd, false);
            numResourcesBonusEvent += numResourcesAdd;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign && MinhCacheGame.GetEventMissionRemainProgress((int)MinhCacheGame.SummerHolidayMission.Campaign) > 0)
        {
            MinhCacheGame.AddEventMissionCollected((int)MinhCacheGame.SummerHolidayMission.Campaign, numResourcesAdd);
            numResourcesBonusEvent += numResourcesAdd;
        }
        lNumEventResources.text = "" + MinhCacheGame.GetEventResources(EventItemType.IceCream_4_2019.ToString());

        //if (disposableEvent != null) disposableEvent.Dispose();
        //disposableEvent = this.Delay(1f, () =>
        //{
        //    lNumEventResources.gameObject.SetActive(false);
        //});
    }

    public void SetTextScore(int numScoreAdd = 0)
    {
        //lnumScore.DOUILabelInt(curScore, (curScore + numScoreAdd), 1f);
        curScore += numScoreAdd;
        lnumScore.text = "" + curScore;
        if (onScoreChange != null) onScoreChange(curScore);
    }



    public void SetTextCoin(int numCoinAdd = 0)
    {
        if (numCoinAdd < 0 || numCoinAdd > 10)
        {
            SoundManager.SoundGetCoinBig();
        }
        else if (numCoinAdd != 0)
        {
            SoundManager.SoundGetCoinSmall();
        }
        //
        if (numCoinAdd > 0)
        {
            curCoin += numCoinAdd;
            //CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString()) + numCoinAdd);
            //CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldEasy.ToString()) + numCoinAdd);
            //CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.GetGoldHard.ToString()) + numCoinAdd);
        }
        CacheGame.SetTotalCoin(CacheGame.GetTotalCoin() + numCoinAdd, FirebaseLogSpaceWar.DropIngame_Why, FirebaseLogSpaceWar.Unknown_why);

        //lnumCoin.text = "" + GameContext.FormatNumber(CacheGame.GetTotalCoin());
        lnumCoin.text = "" + CacheGame.GetTotalCoin();
    }


    public void SetTextGem(int numGemAdd = 0)
    {
        SoundManager.SoundGetCoinSmall();
        CacheGame.SetTotalGem(CacheGame.GetTotalGem() + numGemAdd, FirebaseLogSpaceWar.DropIngame_Why, FirebaseLogSpaceWar.Unknown_why);
        //lnumGem.text = "" + GameContext.FormatNumber(CacheGame.GetTotalGem());
        lnumGem.text = "" + CacheGame.GetTotalGem();

    }




    public void SetTextLife(int numLife)
    {
        lnumLife.text = numLife + "";
        if (onLifeChange != null)
        {
            onLifeChange(numLife);
        }
    }

    //-----show ui add card
    System.IDisposable xxx;
    public void SetUiAddAircraftCard(AircraftTypeEnum aircraftAdd)
    {
        panelUiAddResources.SetActive(true);
        sPlaneAddResources.spriteName = "card_A" + (int)aircraftAdd;

        MinhCacheGame.SetSpaceShipCards(aircraftAdd, MinhCacheGame.GetSpaceShipCards(aircraftAdd) + 1, FirebaseLogSpaceWar.Mission_why, "", aircraftAdd.ToString());

        if (xxx != null) xxx.Dispose();

        xxx = this.Delay(1.5f, () =>
         {
             panelUiAddResources.SetActive(false);
         });
    }

    public void SetUiAddWingCard(WingTypeEnum wingAdd)
    {
        Debug.LogError("card_W" + (int)wingAdd);
        panelUiAddResources.SetActive(true);
        sPlaneAddResources.spriteName = "card_W" + (int)wingAdd;

        MinhCacheGame.SetWingCards(wingAdd, MinhCacheGame.GetWingCards(wingAdd) + 1, FirebaseLogSpaceWar.Mission_why, "", wingAdd.ToString());

        if (xxx != null) xxx.Dispose();

        xxx = this.Delay(1.5f, () =>
        {
            panelUiAddResources.SetActive(false);
        });
    }


    //  Add Energy
    System.IDisposable disposableEnergy;
    public void SetUiAddEnergy()
    {
        panelUiAddEnergy.SetActive(true);

        CacheGame.SetTotalEnergy(CacheGame.GetTotalEnergy() + 1, FirebaseLogSpaceWar.DropIngame_Why, FirebaseLogSpaceWar.Unknown_why);

        if (disposableEnergy != null) disposableEnergy.Dispose();
        disposableEnergy = this.Delay(1f, () =>
        {
            panelUiAddEnergy.SetActive(false);
        });
    }



    //-----demo cancel tất cả các hàm this.delay được add vào list

    //CompositeDisposable listDemo;
    //void DemoCancel()
    //{
    //    if (listDemo != null) listDemo.Dispose();

    //    this.Delay(1.5f, () =>
    //    {
    //        panelUiAddCard.SetActive(false);
    //    }).AddTo(listDemo);
    //}




}
