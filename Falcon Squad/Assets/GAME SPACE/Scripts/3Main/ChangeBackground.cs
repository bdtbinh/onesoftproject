﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using DG.Tweening;


//public enum TypeBackground
//{
//	BG_May,
//	BG_Bien,
//	BG_NuiDa,
//	BG_SaMac,
//	BG_Sao,
//
//}


public class ChangeBackground : Singleton<ChangeBackground>
{
	//

	public float speedBgStart = 8f;
	public float speedDecorStart = 11f;
	public float speedBgReady = 2f;
	public float speedDecorReady = 2.6f;
	//21
	//
	//van toc= quang duong /thoi gian
	//51.2 /7
	//-----move background

	public TweenPosition tweenMoveBG;
	public TweenPosition tweenMoveDecor;

	public float distanceMoveBG = 51.2f;
	public float distanceMoveDecor = 56;

	float durationMoveBg = 28f;
	float durationMoveDecor = 21f;

	void Start ()
	{
		SetSpeedMoveBackGround (speedBgStart, speedDecorStart, 0f);
		ShakeCamera.Instance.bgShakeObj = gameObject;
	}


	//
	//
	public void SetSpeedMoveBackGround (float speedBG = 1.8f, float  speedDecor = 1.6f, float timeChange = 3.6f)
	{
		//

		if (tweenMoveBG != null) {
			durationMoveBg = distanceMoveBG / speedBG;
			DOTween.To (() => tweenMoveBG.duration, x => tweenMoveBG.duration = x, durationMoveBg, timeChange);
		}

		if (tweenMoveDecor != null) {
			durationMoveDecor = distanceMoveDecor / speedDecor;
			DOTween.To (() => tweenMoveDecor.duration, x => tweenMoveDecor.duration = x, durationMoveDecor, timeChange);
		}




	}
}
