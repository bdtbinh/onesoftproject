﻿using UnityEngine;
using PathologicalGames;
using System.Collections;
using Chronos;

public class GunMissileHoming : MonoBehaviour
{
	//	public enum TypeMissileCtrl
	//	{
	//		BezierMissile,
	//		StandardMissile
	//	}

	public GameObject playerIns;
	public GameObject nameMissile;
	public Vector3 directMotion;
	public Vector3 distancePos;
	public bool autoFire;
	public float durationMoveFirst = 1;
	public float speedMoveFire = 6;
	public float timeLoop = 0.5f;
	//	private float timeStart;

	Transform trans;
	BulletHomingController a;
	Vector3 dir;

	//	WwBullet damageBullet;

	//    private PlayerState statePlayerScript;
	//    protected DataAttributePlayer dataPlane;
	Timeline timeline;

	float GetTimeLine ()
	{
		if (timeline.timeScale != 0) {
			return timeline.timeScale;
		}
		return 1;
	}

	void Start ()
	{

		SetupValueFromShop ();
		timeline = GetComponent<Timeline> ();
		//

//		StartShootMissile ();
	}


	//---------------------  goi ham ban' ten lua  -------------

	public void StartShootMissile ()
	{
		StartCoroutine ("ShootLoop");
	}




	IEnumerator ShootLoop ()
	{
		FireMissile ();
		while (true) {

//			Debug.LogError ("StartShootMissile " + (timeLoop / timeline.timeScale) + " timeScale " + timeline.timeScale);

			yield return new WaitForSeconds (timeLoop / GetTimeLine ());
//			FireMissile ();
			if (playerIns.activeInHierarchy) {
				FireMissile ();
			} else {
				StopAllCoroutines ();
				break;
			}
		}
	}

	public void StopShootMissile ()
	{
		StopAllCoroutines ();
	}

	public void SetupValueFromShop ()
	{
// -- dung de setup cac gia tri lay tu` shop cho bullet va gun
	}


	private void FireMissile ()
	{

		//---------

		trans = PoolManager.Pools ["BulletHomingPool"].Spawn (nameMissile, transform.position, transform.rotation);

		a = trans.GetComponent<BulletHomingController> ();

		dir = transform.position + directMotion;
		a.SetupSpeedAndDuration (speedMoveFire, durationMoveFirst, transform.position, distancePos, dir);

		//Setup Damage missile Bullet

//			damageBullet = trans.GetComponent<WwBullet> ();
//			maxPowerBullet = dataPlane.damageMissile + DataEntitySys.Instance.damageMissile;
//			damageBullet.PowerBullet = maxPowerBullet;
//		}
	}

	public void ActiveAutoFire ()
	{
		autoFire = true;
	}

	public void DeactiveAutoFire ()
	{
		autoFire = false;
	}

	#if UNITY_EDITOR
	void OnDrawGizmos ()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere ((Vector3)transform.position + directMotion, 0.25f);
	}
	#endif

}
