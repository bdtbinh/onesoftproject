﻿using com.ootii.Messages;
using DG.Tweening;
using SkyGameKit;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelSkillController : MonoBehaviour
{
    public static Dictionary<AircraftTypeEnum, string> NameSpriteSkillButton = new Dictionary<AircraftTypeEnum, string>
    {
        { AircraftTypeEnum.BataFD, "Skill9" },
        { AircraftTypeEnum.SkyWraith, "Skill10" },
        { AircraftTypeEnum.FuryOfAres, "Skill11" },
        { AircraftTypeEnum.CandyPine, "" },
        { AircraftTypeEnum.RadiantStar, "" },
        { AircraftTypeEnum.MacBird, "Skill12" },
        { AircraftTypeEnum.TwilightX, "Skill13" },
        { AircraftTypeEnum.StarBomb, "Skill19" },
        { AircraftTypeEnum.IceShard, "Skill21" },
        { AircraftTypeEnum.ThunderBolt, "Skill32" },

    };



    public UISprite sSkill;
    public UISprite sfadeCoolDown;
    public UILabel lNumSkill;

    public BoxCollider colliderButton; // bỏ
    public TweenRotation tweenGlowButton;

    public TweenPosition tweenPositionPanel;
    public GameObject fxTutorial;
    private bool isCoolDownButtonRunning;

    public Vector3 posFromLeft;
    public Vector3 posToLeft;
    public Vector3 posFromRight;
    public Vector3 posToRight;
    public Vector3 scaleLeft;
    public Vector3 scaleRight;

    Aircraft aircraftIns;
    float timeDelayCallFxStart;

    //Fx
    public GameObject timeSpherescreenPrefab;
    //GameObject activeSkillText;
    GameObject timeSpherescreen;

    //--Wing skill-------------------------
    public static Dictionary<WingTypeEnum, string> NameSpriteWingSkill = new Dictionary<WingTypeEnum, string>
    {
        { WingTypeEnum.WingOfJustice, "Skill23" },
        { WingTypeEnum.WingOfRedemption, "Skill24" },
        { WingTypeEnum.WingOfResolution, "Skill29" },

    };
    public GameObject objBtnWingSkill;

    public UISprite sWingSkill;
    public UISprite sfadeCoolDownWingSkill;
    public UILabel lNumWingSkill;

    //public BoxCollider colliderButtonWingSkill;
    public TweenRotation tweenGlowButtonWingSkill;

    private bool isCoolDownButtonRunningWingSkill;
    Wing wingIns;


    void Start()
    {
        SetupPositionLeftRightStart();
        if (SceneManager.GetActiveScene().name == "LevelTutorial")
        {
            tweenPositionPanel.enabled = false;
            LevelManager.OnNextWave += OnNextWave;
        }
        else
        {
            tweenPositionPanel.enabled = true;
        }
    }

    //------cho level tutorial--------
    void OnNextWave(int numWaveOrigi)
    {
        if (numWaveOrigi == 6)
        {
            ShowTutorial();
        }
    }

    #region //---CHỈNH VỊ TRÍ TRÁI PHẢI
    void SetupPositionLeftRightStart()
    {
        if (CacheGame.ValueColtrolButtonSkill == 1)
        {
            tweenPositionPanel.from = posFromLeft;
            transform.localScale = scaleLeft;
            transform.localPosition = posFromLeft;
            lNumSkill.transform.localScale = scaleLeft;
            sSkill.transform.localScale = scaleLeft;
            //
            lNumWingSkill.transform.localScale = scaleLeft;
            sWingSkill.transform.localScale = scaleLeft;

        }
        else
        {
            tweenPositionPanel.from = posFromRight;
            transform.localScale = scaleRight;
            transform.localPosition = posFromRight;
            lNumSkill.transform.localScale = scaleRight;
            sSkill.transform.localScale = scaleRight;
            //
            lNumWingSkill.transform.localScale = scaleRight;
            sWingSkill.transform.localScale = scaleRight;
        }
    }

    void SetupPositionLeftRightIngame(IMessage msg)
    {
        if (CacheGame.ValueColtrolButtonSkill == 1)
        {
            //tweenPositionPanel.from = posFromLeft;
            transform.localScale = scaleLeft;
            lNumSkill.transform.localScale = scaleLeft;
            sSkill.transform.localScale = scaleLeft;

            lNumWingSkill.transform.localScale = scaleLeft;
            sWingSkill.transform.localScale = scaleLeft;
        }
        else
        {
            //tweenPositionPanel.from = posFromRight;
            transform.localScale = scaleRight;
            lNumSkill.transform.localScale = scaleRight;
            sSkill.transform.localScale = scaleRight;

            lNumWingSkill.transform.localScale = scaleRight;
            sWingSkill.transform.localScale = scaleRight;

        }
    }

    #endregion


    void ShowTutorial()
    {
        fxTutorial.SetActive(true);
        tweenPositionPanel.enabled = true;
    }

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventID.ON_CHANGE_CONTROL, SetupPositionLeftRightIngame, true);
        MessageDispatcher.AddListener(EventName.UDP.ActiveSkill2v2.ToString(), OnActiveSkill2v2, true);
    }
    private void OnDisable()
    {
        //LeanTween.cancel(gameObject);
        if (SceneManager.GetActiveScene().name == "LevelTutorial")
        {
            LevelManager.OnNextWave -= OnNextWave;
        }
        MessageDispatcher.RemoveListener(EventName.UDP.ActiveSkill2v2.ToString(), OnActiveSkill2v2, true);
        MessageDispatcher.RemoveListener(EventID.ON_CHANGE_CONTROL, SetupPositionLeftRightIngame, true);
    }

    private void OnActiveSkill2v2(IMessage rMessage)
    {
        int type = (int)rMessage.Data;
        switch (type)
        {
            case CachePvp.TYPE_ACTIVE_SKILL_PLAYER:
                BtnUseSkillCoop();
                break;
            case CachePvp.TYPE_ACTIVE_SKILL_WING:
                BtnUseWingSkillCoop();
                break;
            default:
                break;
        }
    }

    public void InitButtonSkill()
    {
        aircraftIns = AllPlayerManager.Instance.playerCampaign.Aircraft;
        timeDelayCallFxStart = 5f;
        if (AllPlayerManager.Instance.playerCampaign.usingAircraftBackup)
        {
            timeDelayCallFxStart = 1f;
        }
        //Debug.LogError("timeDelayCallFxStart:" + timeDelayCallFxStart);
        SetDataStart();
    }

    void SetDataStart()
    {

        sSkill.spriteName = "" + NameSpriteSkillButton[aircraftIns.AircraftType];




        aircraftIns.onUsingSkillNumberChange += OnUsingSkillNumberChange;
        aircraftIns.ChangeUsingSkillNumber(0);
        isCoolDownButtonRunning = true;
        //sBG.spriteName = NAME_SPRITE_BG_DEACTIVE;
        sfadeCoolDown.fillAmount = 1f;
        //colliderButton.enabled = BoxCollierStatus();
        DOTween.To(() => sfadeCoolDown.fillAmount, x => sfadeCoolDown.fillAmount = x, 0, 2f).SetDelay(timeDelayCallFxStart).SetUpdate(true).OnComplete(() =>
        {
            isCoolDownButtonRunning = false;
            //sBG.spriteName = NAME_SPRITE_BG_ACTIVE;
            RunGlowReadyButton();
            //colliderButton.enabled = BoxCollierStatus();
        });

    }

    void OnUsingSkillNumberChange(int usingSkillNumber, int usingSkillNumChange)
    {
        lNumSkill.text = "" + usingSkillNumber;


        if (usingSkillNumber > 0 && !isCoolDownButtonRunning /*&& sfadeCoolDown.fillAmount != 1*/ && !MainScene.Instance.activeSkillIsPlaying && !MainScene.Instance.gameFinished)
        {
            if (usingSkillNumber > 1 && usingSkillNumChange > 0)
            {
                //Debug.LogError("return OnUsingSkillNumberChange");
                return;
            }
            CoolDownButtonReadyAgain();
        }
    }

    void RunGlowReadyButton()
    {
        tweenGlowButton.gameObject.SetActive(true);
        tweenGlowButton.ResetToBeginning();

        this.Delay(1.2f, () =>
        {
            tweenGlowButton.gameObject.SetActive(false);
        }, true);
    }



    public void BtnUseSkill()
    {
        if (aircraftIns.UsingSkillNumber > 0 && !isCoolDownButtonRunning && !MainScene.Instance.activeSkillIsPlaying && !aircraftIns.aircraftIsStopMove && !MainScene.Instance.gameFinished)
        {
            ShowEffectTextStart();
            aircraftIns.UseActiveSkill();
            //
            ActionUseSkill();
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2 && !AllPlayerManager.Instance.playerCampaign.isTestingMode)
            {
                AllPlayerManager.Instance.playerCoop.Aircraft.StopTimeScaleSkill();
                ConnectManager.Instance.SendData(CachePvp.ACTIVE_SKILL, posFromLeft, CachePvp.TYPE_ACTIVE_SKILL_PLAYER);
            }
        }
    }

    public void BtnUseSkillCoop()
    {
        AllPlayerManager.Instance.playerCampaign.Aircraft.StopTimeScaleSkill();
        AllPlayerManager.Instance.playerCoop.Aircraft.UseActiveSkill();
    }


    void ActionUseSkill()
    {
        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.UseTimeSphere.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.UseTimeSphere.ToString()) + 1);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.UseTimeSphereEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.UseTimeSphereEasy.ToString()) + 1);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.UseTimeSphereHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.UseTimeSphereHard.ToString()) + 1);

        MainScene.Instance.activeSkillIsPlaying = true;
        timeSpherescreen.SetActive(true);

        sfadeCoolDown.fillAmount = 1f;

        aircraftIns.ChangeUsingSkillNumber(-1);


        this.Delay(aircraftIns.UsingSkillTime, () =>
        {
            MainScene.Instance.activeSkillIsPlaying = false;
            timeSpherescreen.SetActive(false);
            if (aircraftIns.UsingSkillNumber > 0)
            {
                CoolDownButtonReadyAgain();
            }
        }, true);
        fxTutorial.SetActive(false);
    }

    void CoolDownButtonReadyAgain()
    {
        isCoolDownButtonRunning = true;
        DOTween.To(() => sfadeCoolDown.fillAmount, x => sfadeCoolDown.fillAmount = x, 0, aircraftIns.UsingSkillCountdownTime).SetDelay(0.1f).OnComplete(() =>
        {
            //sBG.spriteName = NAME_SPRITE_BG_ACTIVE;
            isCoolDownButtonRunning = false;
            //colliderButton.enabled = BoxCollierStatus();
            RunGlowReadyButton();
        });
    }



    //-------

    public void ShowEffectTextStart()
    {
        if (EffectList.Instance.activeSkillText != null)
        {
            //Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.fxStartLevel, posFxVic, Quaternion.identity);
            //effectIns.GetComponent<ParticleSystem>().Play(true);
            EffectList.Instance.activeSkillText.SetActive(true);
            EffectList.Instance.activeSkillText.GetComponent<ParticleSystem>().Play(true);
            this.Delay(2f, () =>
            {
                EffectList.Instance.activeSkillText.SetActive(false);
            }, true);
        }
        if (timeSpherescreen == null)
        {
            InitFxTimeSphereScreen();
        }
    }

    #region //---Init Fx TimeSphere Screen
    public void InitFxTimeSphereScreen()
    {
        //activeSkillText = Instantiate(activeSkillTextPrefab, Vector3.zero, Quaternion.identity);
        timeSpherescreen = Instantiate(timeSpherescreenPrefab, Vector3.zero, Quaternion.identity);
        timeSpherescreen.transform.GetChild(1).localScale = new Vector2((MainScene.Instance.posAnchorRight.localPosition.x * 2) + 0.03f, (MainScene.Instance.posAnchorTop.localPosition.y * 2) + 0.02f);
        timeSpherescreen.transform.localPosition = new Vector3(0, -6.55f, 0f);
    }
    #endregion


    //--------- WING SKILL---------------------------------------------------------------------------

    public void InitButtonWingSkill()
    {
        if (AllPlayerManager.Instance.playerCampaign.Wing != null)
        {
            objBtnWingSkill.SetActive(true);
            wingIns = AllPlayerManager.Instance.playerCampaign.Wing;
            //Debug.LogError("InitButtonWingSkill:");
            SetDataStartWingSkill();
        }
        else
        {
            objBtnWingSkill.SetActive(false);
        }
    }

    void SetDataStartWingSkill()
    {

        sWingSkill.spriteName = "" + NameSpriteWingSkill[wingIns.WingType];

        wingIns.onUsingWingSkillNumberChange += onUsingWingSkillNumberChange;
        wingIns.ChangeUsingSkillNumber(0);
        isCoolDownButtonRunningWingSkill = true;
        sfadeCoolDownWingSkill.fillAmount = 1f;
        DOTween.To(() => sfadeCoolDownWingSkill.fillAmount, x => sfadeCoolDownWingSkill.fillAmount = x, 0, 2f).SetDelay(timeDelayCallFxStart).SetUpdate(true).OnComplete(() =>
        {
            isCoolDownButtonRunningWingSkill = false;
            RunGlowReadyButtonWingSkill();
        });

    }

    void onUsingWingSkillNumberChange(int usingSkillNumber)
    {
        lNumWingSkill.text = "" + usingSkillNumber;
        //Debug.LogError("onUsingWingSkillNumberChange:" + usingSkillNumber);
        if (usingSkillNumber > 0 && !isCoolDownButtonRunningWingSkill && sfadeCoolDownWingSkill.fillAmount != 1 && !MainScene.Instance.activeSkillIsPlaying && !MainScene.Instance.gameFinished)
        {
            CoolDownButtonWingSkillReadyAgain();
        }
    }

    void CoolDownButtonWingSkillReadyAgain()
    {
        isCoolDownButtonRunningWingSkill = true;
        DOTween.To(() => sfadeCoolDownWingSkill.fillAmount, x => sfadeCoolDownWingSkill.fillAmount = x, 0, wingIns.UsingSkillCountdownTime).SetDelay(0.1f).OnComplete(() =>
        {
            isCoolDownButtonRunningWingSkill = false;
            RunGlowReadyButtonWingSkill();
        });
    }

    void RunGlowReadyButtonWingSkill()
    {
        tweenGlowButtonWingSkill.gameObject.SetActive(true);
        tweenGlowButtonWingSkill.ResetToBeginning();

        this.Delay(1.2f, () =>
        {
            tweenGlowButtonWingSkill.gameObject.SetActive(false);
        }, true);
    }


    public void BtnUseWingSkill()
    {
        if (wingIns.UsingSkillNumber > 0 && !isCoolDownButtonRunningWingSkill && !MainScene.Instance.activeSkillIsPlaying && !aircraftIns.aircraftIsStopMove && !MainScene.Instance.gameFinished)
        {
            ShowEffectTextStart();
            wingIns.UseActiveSkill();
            //
            ActionUseSkillWingSkill();
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2 && !AllPlayerManager.Instance.playerCampaign.isTestingMode)
            {
                ConnectManager.Instance.SendData(CachePvp.ACTIVE_SKILL, posFromLeft, CachePvp.TYPE_ACTIVE_SKILL_WING);
            }
        }
    }
    public void BtnUseWingSkillCoop()
    {
        AllPlayerManager.Instance.playerCoop.Wing.UseActiveSkill();
    }

    void ActionUseSkillWingSkill()
    {
        MainScene.Instance.activeSkillIsPlaying = true;
        timeSpherescreen.SetActive(true);

        //sBG.spriteName = NAME_SPRITE_BG_DEACTIVE;
        sfadeCoolDownWingSkill.fillAmount = 1f;

        wingIns.ChangeUsingSkillNumber(-1);

        this.Delay(wingIns.UsingSkillTime, () =>
        {
            MainScene.Instance.activeSkillIsPlaying = false;
            timeSpherescreen.SetActive(false);
            if (wingIns.UsingSkillNumber > 0)
            {
                CoolDownButtonWingSkillReadyAgain();
            }
        }, true);
    }

    public void BtnStopShoot()
    {
        AllPlayerManager.Instance.playerCampaign.playerControllerScript.PlayerStopShoot();
    }

    public void BtnStartShoot()
    {
        AllPlayerManager.Instance.playerCampaign.playerControllerScript.PlayerStartShoot();
    }

    public void BtnStopSkill()
    {
        AllPlayerManager.Instance.playerCoop.Aircraft.StopTimeScaleSkill();
    }
}
