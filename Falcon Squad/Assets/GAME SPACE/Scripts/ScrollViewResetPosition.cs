﻿using SkyGameKit;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections;

public class ScrollViewResetPosition : MonoBehaviour
{
    [Button]
    public void UpdatePosition()
    {
        GetComponent<UIScrollView>().ResetPosition();
    }



    void OnEnable()
    {
        if (GetComponent<UIScrollView>() != null)
        {
            StartCoroutine("ResetPos", 0.02f);
            StartCoroutine("ResetPos", 0.1f);
            StartCoroutine("ResetPos", 0.5f);
        }
    }

    IEnumerator ResetPos(float timedelay)
    {
        yield return new WaitForSecondsRealtime(timedelay);
        if (GetComponent<UIScrollView>() != null)
        {
            GetComponent<UIScrollView>().ResetPosition();
        }
    }

    private void OnDisable()
    {
        //LeanTween.cancel(gameObject);
        StopAllCoroutines();
        //LeanTween.
    }
}
