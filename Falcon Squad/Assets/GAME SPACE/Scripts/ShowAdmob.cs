﻿//using System;
//using UnityEngine;
//using GoogleMobileAds;
//using GoogleMobileAds.Api;
//
//
//
//// Example script showing how to invoke the Google Mobile Ads Unity plugin.
//public class ShowAdmob : MonoBehaviour
//{
//
//	private BannerView bannerView;
//	private InterstitialAd interstitial;
//	public RewardBasedVideoAd rewardBasedVideo;
//	//	public float deltaTime = 0.0f;
//	private string banerUnitId;
//	//	public string fullScreenId;
//	//	public string videoAdsId;
//
//
//	public string fullscreenAndroid;
//	public string fullscreenIos;
//	private string fullscreenId;
//	//
//	public string videoIdAndroid;
//	public string videoIdIos;
//	private string videoId;
//
//	private bool isInPreloadProcess_Video = false;
//	//	public static string outputMessage = "";
//	//
//	//	public static string OutputMessage {
//	//		set { outputMessage = value; }
//	//	}
//
//
//	private static ShowAdmob mInstance = null;
//
//	public static ShowAdmob Instance {
//		get {
//			if (mInstance == null)
//				mInstance = GameObject.FindObjectOfType<ShowAdmob> ();
//			return mInstance;
//		}
//	}
//
//	void Awake ()
//	{
//		if (mInstance) {
//			Destroy (gameObject);
//		} else {
//			mInstance = this;
//			DontDestroyOnLoad (gameObject);
//		}
//
//		#if UNITY_ANDROID
//		fullscreenId = fullscreenAndroid;
//		videoId = videoIdAndroid;
//		#elif UNITY_IPHONE
//		fullscreenId = fullscreenIos;
//		videoId = videoIdIos;
//		#else
//		fullscreenId = "unexpected_platform";
//		videoId = "unexpected_platform";
//		#endif
//
//	}
//
//	void Start ()
//	{
//
//		rewardBasedVideo = RewardBasedVideoAd.Instance;
//
//		rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
//		rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
//		rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
//		rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
//		rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
//		rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
//		rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
//		RequestShowBigBanner ();
//	}
//
//	void Update ()
//	{
//		// Calculate simple moving average for time to render screen. 0.1 factor used as smoothing
//		// value.
//		//		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
//	}
//
//	public void RequestShowSmallBanner ()
//	{
//		#if UNITY_EDITOR
//		//		string adUnitId = "unused";
//		#elif UNITY_ANDROID
//				string adUnitId = "INSERT_ANDROID_BANNER_AD_UNIT_ID_HERE";
//		#elif UNITY_IPHONE
//				string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
//		#else
//				string adUnitId = "unexpected_platform";
//		#endif
//
//		// Create a 320x50 banner at the top of the screen.
//		bannerView = new BannerView (banerUnitId, AdSize.SmartBanner, AdPosition.Top);
//		// Register for ad events.
//		bannerView.OnAdLoaded += HandleAdLoaded;
//		bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
//		bannerView.OnAdLoaded += HandleAdOpened;
//		bannerView.OnAdClosed += HandleAdClosed;
//		bannerView.OnAdLeavingApplication += HandleAdLeftApplication;
//		// Load a banner ad.
//		bannerView.LoadAd (createAdRequest ());
//	}
//
//	public void RequestShowBigBanner ()
//	{
//		#if UNITY_EDITOR
//		//		string adUnitId = "unused";
//		#elif UNITY_ANDROID
//				string adUnitId = "INSERT_ANDROID_INTERSTITIAL_AD_UNIT_ID_HERE";
//		#elif UNITY_IPHONE
//				string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
//		#else
//				string adUnitId = "unexpected_platform";
//		#endif
//
//		// Create an interstitial.
//		interstitial = new InterstitialAd (fullscreenId);
//		// Register for ad events.
//		interstitial.OnAdLoaded += HandleInterstitialLoaded;
//		interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
//		interstitial.OnAdOpening += HandleInterstitialOpened;
//		interstitial.OnAdClosed += HandleInterstitialClosed;
//		interstitial.OnAdLeavingApplication += HandleInterstitialLeftApplication;
//		// Load an interstitial ad.
//		interstitial.LoadAd (createAdRequest ());
//	}
//
//	// Returns an ad request with custom ad targeting.
//	private AdRequest createAdRequest ()
//	{
//		return new AdRequest.Builder ()
//				.AddTestDevice (AdRequest.TestDeviceSimulator)
//				.AddTestDevice ("0123456789ABCDEF0123456789ABCDEF")
//				.AddKeyword ("game")
//				.SetGender (Gender.Male)
//				.SetBirthday (new DateTime (1985, 1, 1))
//				.TagForChildDirectedTreatment (false)
//				.AddExtra ("color_bg", "9B30FF")
//				.Build ();
//	}
//
//	public void RequestRewardBasedVideo ()
//	{
//		if (isInPreloadProcess_Video == true) {
//			//			print ("There is another Preload RewardBasedVideoAd inprocess, simply ignore new preload Video now.");
//			return;
//		}
//
//		RewardBasedVideoAd rewardBasedVideo = RewardBasedVideoAd.Instance;
//		AdRequest request = null;
//
////		if (isTestDevice == true) {
////			request = new AdRequest.Builder ().AddTestDevice (SystemInfo.deviceUniqueIdentifier).Build ();
////		} else {
////			request = new AdRequest.Builder ().Build ();
////		}
//
//		rewardBasedVideo.LoadAd (createAdRequest (), videoId);
//	}
//
//	public void ShowBigBanner ()
//	{
//		if (interstitial.IsLoaded ()) {
//			interstitial.Show ();
//			Firebase.Analytics.FirebaseAnalytics.LogEvent ("Admob_Show");
//		} else {
//			print ("Interstitial is not ready yet.");
//		}
//	}
//
//	public void ShowSmallBanner ()
//	{
//		bannerView.Show ();
//	}
//
//	public void ShowRewardBasedVideo ()
//	{
//		if (rewardBasedVideo.IsLoaded ()) {
//			rewardBasedVideo.Show ();
//		} else {
//			print ("Reward based video ad is not ready yet.");
//		}
//	}
//
//	#region Banner callback handlers
//
//	public void HandleAdLoaded (object sender, EventArgs args)
//	{
//		print ("HandleAdLoaded event received.");
//	}
//
//	public void HandleAdFailedToLoad (object sender, AdFailedToLoadEventArgs args)
//	{
//		print ("HandleFailedToReceiveAd event received with message: " + args.Message);
//	}
//
//	public void HandleAdOpened (object sender, EventArgs args)
//	{
//		print ("HandleAdOpened event received");
//	}
//
//	void HandleAdClosing (object sender, EventArgs args)
//	{
//		print ("HandleAdClosing event received");
//	}
//
//	public void HandleAdClosed (object sender, EventArgs args)
//	{
//		print ("HandleAdClosed event received");
//	}
//
//	public void HandleAdLeftApplication (object sender, EventArgs args)
//	{
//		print ("HandleAdLeftApplication event received");
//	}
//
//	#endregion
//
//	#region Interstitial callback handlers
//
//	public void HandleInterstitialLoaded (object sender, EventArgs args)
//	{
//		print ("HandleInterstitialLoaded event received.");
//	}
//
//	public void HandleInterstitialFailedToLoad (object sender, AdFailedToLoadEventArgs args)
//	{
//		print ("HandleInterstitialFailedToLoad event received with message: " + args.Message);
//	}
//
//	public void HandleInterstitialOpened (object sender, EventArgs args)
//	{
//		print ("HandleInterstitialOpened event received");
//		Time.timeScale = 0;
//	}
//
//	void HandleInterstitialClosing (object sender, EventArgs args)
//	{
//		print ("HandleInterstitialClosing event received");
//		//		Time.timeScale = 1;
//	}
//
//	public void HandleInterstitialClosed (object sender, EventArgs args)
//	{
//		print ("HandleInterstitialClosed event received");
//		Time.timeScale = 1;
//		RequestShowBigBanner ();
//	}
//
//	public void HandleInterstitialLeftApplication (object sender, EventArgs args)
//	{
//		print ("HandleInterstitialLeftApplication event received");
//	}
//
//	#endregion
//
//	#region RewardBasedVideo callback handlers
//
//	public void HandleRewardBasedVideoLoaded (object sender, EventArgs args)
//	{
//		isInPreloadProcess_Video = false;
//		print ("HandleRewardBasedVideoLoaded event received.");
//	}
//
//	public void HandleRewardBasedVideoFailedToLoad (object sender, AdFailedToLoadEventArgs args)
//	{
//		isInPreloadProcess_Video = false;
//		print ("HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
//	}
//
//	public void HandleRewardBasedVideoOpened (object sender, EventArgs args)
//	{
//		print ("HandleRewardBasedVideoOpened event received");
//	}
//
//	public void HandleRewardBasedVideoStarted (object sender, EventArgs args)
//	{
//		print ("HandleRewardBasedVideoStarted event received");
//	}
//
//	public void HandleRewardBasedVideoClosed (object sender, EventArgs args)
//	{
//		print ("HandleRewardBasedVideoClosed event received");
//	}
//
//	public void HandleRewardBasedVideoRewarded (object sender, Reward args)
//	{
//		string type = args.Type;
//		double amount = args.Amount;
//		print ("HandleRewardBasedVideoRewarded event received for " + amount.ToString () + " " +
//		type);
//	}
//
//	public void HandleRewardBasedVideoLeftApplication (object sender, EventArgs args)
//	{
//		print ("HandleRewardBasedVideoLeftApplication event received");
//	}
//
//	#endregion
//}
