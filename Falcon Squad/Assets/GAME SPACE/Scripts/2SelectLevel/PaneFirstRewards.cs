﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaneFirstRewards : MonoBehaviour
{
    public UISprite sGift1, sGift2, sGift3;
    public UILabel lNumGift1, lNumGift2, lNumGift3;
    public GameObject sGift1Done, sGift2Done, sGift3Done;

    public enum TypeShowPaneFirstRewards
    {
        PopupSelectLevel,
        PopupGameComplete
    }

    public TypeShowPaneFirstRewards typeShow;


    private void OnEnable()
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            ShowGift(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift1,
            LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift1,
            sGift1,
            lNumGift1,
            sGift1Done);
            ShowGift(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift2,
               LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift2,
               sGift2,
               lNumGift2,
               sGift2Done);
            ShowGift(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift3,
               LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift3,
               sGift3,
               lNumGift3,
               sGift3Done);

            if (typeShow == TypeShowPaneFirstRewards.PopupGameComplete)
            {
                GameContext.AddGiftToPlayer(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift1,
                         LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift1, true, FalconFirebaseLogger.LevelClearRewards_why);
                GameContext.AddGiftToPlayer(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift2,
                              LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift2, true, FalconFirebaseLogger.LevelClearRewards_why);
                GameContext.AddGiftToPlayer(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift3,
                              LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift3, true, FalconFirebaseLogger.LevelClearRewards_why);
            }
        }
        else if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra)
        {
            ShowGift(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift4,
            LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift4,
            sGift1,
            lNumGift1,
            sGift1Done);
            ShowGift(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift5,
               LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift5,
               sGift2,
               lNumGift2,
               sGift2Done);
            ShowGift(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift6,
               LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift6,
               sGift3,
               lNumGift3,
               sGift3Done);

            if (typeShow == TypeShowPaneFirstRewards.PopupGameComplete)
            {
                GameContext.AddGiftToPlayer(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift4,
                         LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift4, true, FalconFirebaseLogger.LevelClearRewardsExtra_why);
                GameContext.AddGiftToPlayer(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift5,
                              LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift5, true, FalconFirebaseLogger.LevelClearRewardsExtra_why);
                GameContext.AddGiftToPlayer(LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).TypeGift6,
                              LevelClearRewardSheet.Get(CacheGame.GetCurrentLevel()).ValueGift6, true, FalconFirebaseLogger.LevelClearRewardsExtra_why);
            }
        }

    }


    bool isReceivedFirstRewards()
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            return CacheGame.GetCurrentLevel() != CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign());
        }
        else
        {
            return CacheGame.GetPassedLevelExtra(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()) == 1;
        }
        //return true;
    }

    void ShowGift(string typeGift, int numGift, UISprite sShowGift, UILabel lShowGift, GameObject sGiftDone)
    {
        lShowGift.text = /*numGift.ToString()+*/ GameContext.FormatNumber(numGift);
        if (typeShow == TypeShowPaneFirstRewards.PopupGameComplete)
        {
            sGiftDone.SetActive(true);
        }
        else
        {
            if (!isReceivedFirstRewards())
            {
                sGiftDone.SetActive(false);
            }
            else
            {
                sGiftDone.SetActive(true);
            }
        }

        sShowGift.spriteName = GameContext.getNameSpriteGift(typeGift);
        //switch (typeGift)
        //{
        //    case "Gold":
        //        sShowGift.spriteName = "icon_coin";
        //        break;
        //    case "Gem":
        //        sShowGift.spriteName = "icon_daily_quest_gem";
        //        break;
        //    case "PowerUp":
        //        sShowGift.spriteName = "icon_dailyquest_powerup";
        //        break;
        //    case "ActiveSkill":
        //        sShowGift.spriteName = "icon_dailyquest_time";
        //        break;
        //    case "Life":
        //        sShowGift.spriteName = "icon_dailyquest_life";
        //        break;
        //    case "AircraftGeneralCard":
        //        sShowGift.spriteName = "card_A0";
        //        break;
        //    case "Aircraft1Card":
        //        sShowGift.spriteName = "card_A1";
        //        break;
        //    case "Aircraft2Card":
        //        sShowGift.spriteName = "card_A2";
        //        break;
        //    case "Aircraft3Card":
        //        sShowGift.spriteName = "card_A3";
        //        break;
        //    case "Aircraft6Card":
        //        sShowGift.spriteName = "card_A6";
        //        break;
        //    case "Aircraft7Card":
        //        sShowGift.spriteName = "card_A7";
        //        break;
        //    case "Aircraft8Card":
        //        sShowGift.spriteName = "card_A8";
        //        break;
        //    case "Aircraft9Card":
        //        sShowGift.spriteName = "card_A9";
        //        break;
        //    case "Aircraft10Card":
        //        sShowGift.spriteName = "card_A10";
        //        break;
        //    case "WingmanGeneralCard":
        //        sShowGift.spriteName = "card_D0";
        //        break;
        //    case "Wingman1Card":
        //        sShowGift.spriteName = "card_D1";
        //        break;
        //    case "Wingman2Card":
        //        sShowGift.spriteName = "card_D2";
        //        break;
        //    case "Wingman3Card":
        //        sShowGift.spriteName = "card_D3";
        //        break;
        //    case "Wingman4Card":
        //        sShowGift.spriteName = "card_D4";
        //        break;
        //    case "Wingman5Card":
        //        sShowGift.spriteName = "card_D5";
        //        break;
        //}

    }




    //void GetGiftByType(string typeGift, int numGift)
    //{

    //    switch (typeGift)
    //    {
    //        case "Gold":
    //            CacheGame.SetTotalCoin(CacheGame.GetTotalCoin() + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "level" + CacheGame.GetCurrentLevel());
    //            //FirebaseLogSpaceWar.LogGoldIn(numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "level" + CacheGame.GetCurrentLevel());
    //            if (PanelCoinGem.Instance != null)
    //            {
    //                PanelCoinGem.Instance.AddCoinTop();
    //            }
    //            break;
    //        case "Gem":
    //            CacheGame.SetTotalGem(CacheGame.GetTotalGem() + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "level" + CacheGame.GetCurrentLevel());
    //            //FirebaseLogSpaceWar.LogGemIn(numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "level" + CacheGame.GetCurrentLevel());
    //            if (PanelCoinGem.Instance != null)
    //            {
    //                PanelCoinGem.Instance.AddGemTop();
    //            }
    //            break;
    //        case "PowerUp":
    //            CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + numGift);
    //            break;
    //        case "ActiveSkill":
    //            CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + numGift);
    //            break;
    //        case "Life":
    //            CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + numGift);
    //            break;
    //        case "AircraftGeneralCard":
    //            MinhCacheGame.SetSpaceShipGeneralCards(MinhCacheGame.GetSpaceShipGeneralCards() + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "");
    //            break;
    //        case "Aircraft1Card":
    //            MinhCacheGame.SetSpaceShipCards((AircraftTypeEnum)1, MinhCacheGame.GetSpaceShipCards((AircraftTypeEnum)1) + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "", ((AircraftTypeEnum)1).ToString());
    //            break;
    //        case "Aircraft2Card":
    //            MinhCacheGame.SetSpaceShipCards((AircraftTypeEnum)2, MinhCacheGame.GetSpaceShipCards((AircraftTypeEnum)2) + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "", ((AircraftTypeEnum)2).ToString());
    //            break;
    //        case "Aircraft3Card":
    //            MinhCacheGame.SetSpaceShipCards((AircraftTypeEnum)3, MinhCacheGame.GetSpaceShipCards((AircraftTypeEnum)3) + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "", ((AircraftTypeEnum)3).ToString());
    //            break;
    //        case "Aircraft6Card":
    //            MinhCacheGame.SetSpaceShipCards((AircraftTypeEnum)6, MinhCacheGame.GetSpaceShipCards((AircraftTypeEnum)6) + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "", ((AircraftTypeEnum)6).ToString());
    //            break;
    //        case "Aircraft7Card":
    //            MinhCacheGame.SetSpaceShipCards((AircraftTypeEnum)7, MinhCacheGame.GetSpaceShipCards((AircraftTypeEnum)7) + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "", ((AircraftTypeEnum)7).ToString());
    //            break;
    //        case "WingmanGeneralCard":
    //            MinhCacheGame.SetWingmanGeneralCards(MinhCacheGame.GetWingmanGeneralCards() + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "");
    //            break;
    //        case "Wingman1Card":
    //            MinhCacheGame.SetWingmanCards((WingmanTypeEnum)1, MinhCacheGame.GetWingmanCards((WingmanTypeEnum)1) + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "", ((WingmanTypeEnum)1).ToString());
    //            break;
    //        case "Wingman2Card":
    //            MinhCacheGame.SetWingmanCards((WingmanTypeEnum)2, MinhCacheGame.GetWingmanCards((WingmanTypeEnum)2) + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "", ((WingmanTypeEnum)2).ToString());
    //            break;
    //        case "Wingman3Card":
    //            MinhCacheGame.SetWingmanCards((WingmanTypeEnum)3, MinhCacheGame.GetWingmanCards((WingmanTypeEnum)3) + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "", ((WingmanTypeEnum)3).ToString());
    //            break;
    //        case "Wingman4Card":
    //            MinhCacheGame.SetWingmanCards((WingmanTypeEnum)4, MinhCacheGame.GetWingmanCards((WingmanTypeEnum)4) + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "", ((WingmanTypeEnum)4).ToString());
    //            break;
    //        case "Wingman5Card":
    //            MinhCacheGame.SetWingmanCards((WingmanTypeEnum)5, MinhCacheGame.GetWingmanCards((WingmanTypeEnum)5) + numGift, FirebaseLogSpaceWar.FirstTimeMission_why, "", ((WingmanTypeEnum)5).ToString());
    //            break;
    //    }

    //}


}
