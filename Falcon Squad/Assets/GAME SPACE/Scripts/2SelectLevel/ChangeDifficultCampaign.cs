﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit;
using Firebase.Analytics;
using TCore;
using com.ootii.Messages;

public class ChangeDifficultCampaign : Singleton<ChangeDifficultCampaign>
{
    public UISprite sNomal, sHard, sHell;

    public GameObject objLabelNomal, objLabelHard, objLabelHell;

    [HideInInspector]
    public int maxLevel;
    //
    public UISprite sIconStarTop;

    void Awake()
    {
        ChangeData();
    }


    public void ButtonNomal()
    {
        SoundManager.PlayClickButton();
        if (CacheGame.GetDifficultCampaign() != GameContext.DIFFICULT_NOMAL)
        {
            CacheGame.SetDifficultCampaign(GameContext.DIFFICULT_NOMAL);
            ChangeData();
            FirebaseLogSpaceWar.LogClickButton("NomalMode");

            MSLManager.Instance.OnChangeDifficultMode();

            MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_DIFFICULT_MODE, this, 0);
        }
    }

    public void ButtonHard()
    {
        SoundManager.PlayClickButton();
        if (CacheGame.GetMaxLevel3Difficult() > 7)
        {
            if (CacheGame.GetDifficultCampaign() != GameContext.DIFFICULT_HARD)
            {
                CacheGame.SetDifficultCampaign(GameContext.DIFFICULT_HARD);
                ChangeData();
                FirebaseLogSpaceWar.LogClickButton("HardMode");

                MSLManager.Instance.OnChangeDifficultMode();

                MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_DIFFICULT_MODE, this, 0);
            }
        }
        else
        {
            //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_complete_level_7, false, 1.8f);
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", "7"), false, 1.8f);
        }
    }

    public void ButtonHell()
    {

        SoundManager.PlayClickButton();
        if (CacheGame.GetMaxLevel3Difficult() > 14)
        {
            if (CacheGame.GetDifficultCampaign() != GameContext.DIFFICULT_HELL)
            {
                CacheGame.SetDifficultCampaign(GameContext.DIFFICULT_HELL);
                ChangeData();
                FirebaseLogSpaceWar.LogClickButton("HellMode");

                MSLManager.Instance.OnChangeDifficultMode();

                MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_DIFFICULT_MODE, this, 0);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", "14"), false, 1.8f);
        }
    }

    void ChangeData()
    {
        switch (CacheGame.GetDifficultCampaign())
        {
            case GameContext.DIFFICULT_NOMAL:
                sIconStarTop.spriteName = "icon_bluestar_ingame";

                sNomal.spriteName = "btn_lv_nomal_a";
                sHard.spriteName = "btn_lv_hard";
                sHell.spriteName = "btn_lv_hell";
                //
                objLabelNomal.SetActive(true);
                objLabelHard.SetActive(false);
                objLabelHell.SetActive(false);

                break;
            case GameContext.DIFFICULT_HARD:
                sIconStarTop.spriteName = "icon_goldstar_ingame";

                sNomal.spriteName = "btn_lv_nomal";
                sHard.spriteName = "btn_lv_hard_a";
                sHell.spriteName = "btn_lv_hell";
                //
                objLabelNomal.SetActive(false);
                objLabelHard.SetActive(true);
                objLabelHell.SetActive(false);

                break;
            case GameContext.DIFFICULT_HELL:
                sIconStarTop.spriteName = "icon_redstar_ingame";

                sNomal.spriteName = "btn_lv_nomal";
                sHard.spriteName = "btn_lv_hard";
                sHell.spriteName = "btn_lv_hell_a";
                //
                objLabelNomal.SetActive(false);
                objLabelHard.SetActive(false);
                objLabelHell.SetActive(true);

                break;
            default:
                break;
        }
        maxLevel = CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign());
        //		Debug.LogError ("maxlevel " + CacheGame.GetMaxLevel (CacheGame.GetDifficultCampaign ()) + maxLevel);
    }

}
