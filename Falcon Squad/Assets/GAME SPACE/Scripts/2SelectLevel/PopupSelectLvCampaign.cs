﻿using Firebase.Analytics;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;
using com.ootii.Messages;
using Mp.Pvp;
using System;

public class PopupSelectLvCampaign : MonoBehaviour
{

    public UISprite sStar1, sStar2, sStar3;

    public UILabel lGetStar1, lGetStar2, lGetStar3;
    //
    public Color32 colorTextStarHide;
    //
    public UILabel lNumLevel;
    //
    public UILabel numItemPowerUp, numItemTimeSphere, numItemLife;
    //public GameObject sChooseItemPowerUp, sChooseItemTimeSphere, sChooseItemLife;
    public UILabel labelNotifiClickItem;
    public UISprite spriteBtnPowerUp;
    public UISprite spriteBtnActiveSkill;
    public UISprite spriteBtnLife;

    //Title of popup in campaign mode
    public GameObject titleCampaign;

    public GameObject panelEndless;
    public GameObject panelCampaign;
    public GameObject panelEvent;

    //Event Halloween
    public UILabel lPlease;
    public GameObject btnStartHalloween;
    public GameObject btnStartCampaign;

    public GameObject panelFirstRewardsLevel;
    public GameObject panelPowerUp;
    public SelectDeviceHandler panelSelectDeviceHandler;
    public PanelSelectLevelEvent panelSelectLevelEvent;
    public PanelExLevelRequire panelExLevelRequire;
    //
    public GameObject listButtonEndless;
    public GameObject listButtonCampain;
    public GameObject listButtonEvent;

    public UILabel lNumCanPlayToday;
    public UILabel lNumWaveReached;

    public GameObject fxTutorialStart;
    public GameObject fxTutorialAircraft;

    private void Start()
    {
        if (CacheGame.GetMaxLevel3Difficult() == 1)
        {
            fxTutorialStart.SetActive(true);
        }
        else
        {
            fxTutorialStart.SetActive(false);
        }
    }

    void showFx()
    {
        //Debug.LogError("GetMaxLevel3Difficult " + CacheGame.GetMaxLevel3Difficult());
        if (CacheGame.GetMaxLevel3Difficult() < 2 || CacheGame.GetMaxLevel3Difficult() > 4)
        {
            fxTutorialAircraft.SetActive(false);
            return;
        }
        int curLevelUpgrade = CacheGame.GetSpaceShipLevel((AircraftTypeEnum)CacheGame.GetSpaceShipUserSelected());
        ///////////
        Rank rank = CacheGame.GetSpaceShipRank((AircraftTypeEnum)CacheGame.GetSpaceShipUserSelected());
        int maxLevel = RankSheet.Get((int)rank).max_level;
        if (curLevelUpgrade >= maxLevel)
        {
            fxTutorialAircraft.SetActive(false);
            return;
        }
        ///////////

        int upgradeGoldCost = AirCUpgradeCostSheet.Get(curLevelUpgrade + 1).gold;
        int upgradeGemCost = AirCUpgradeCostSheet.Get(curLevelUpgrade + 1).gem;

        if (upgradeGoldCost > 0)
        {
            //Debug.LogError("GetMaxLevel3Difficult: " + CacheGame.GetMaxLevel3Difficult());
            if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
            {
                fxTutorialAircraft.SetActive(true);
            }
            else
            {
                fxTutorialAircraft.SetActive(false);
            }
        }
        else
        {
            if (CacheGame.GetTotalGem() >= upgradeGemCost)
            {
                fxTutorialAircraft.SetActive(true);
            }
            else
            {
                fxTutorialAircraft.SetActive(false);
            }
        }
    }


    private void OnEnable()
    {
        SetDataItemStart();
        showFx();
        //LeanTween.delayedCall(0.18f, () =>
        //{
        //    if (PopupShop.Instance != null)
        //    {
        //        PopupShop.Instance.SetShowGlowBtnShop();
        //    }
        //}).setIgnoreTimeScale(true);

        MessageDispatcher.AddListener(EventID.ON_REWARD_SUCCESSED, OnRewardSuccessed, true);
        //MessageDispatcher.AddListener(EventID.ON_TUTORIAL_UPGRADE_SUCCESSED, OnTutorialUpgradeSuccessed, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_REWARD_SUCCESSED, OnRewardSuccessed, true);
        //MessageDispatcher.RemoveListener(EventID.ON_TUTORIAL_UPGRADE_SUCCESSED, OnTutorialUpgradeSuccessed, true);

        panelSelectDeviceHandler.gameObject.SetActive(false);
    }

    private void OnRewardSuccessed(IMessage msg)
    {
        numItemPowerUp.text = "" + (CacheGame.GetTotalItemPowerUp() - CacheGame.numItemPowerUpUse);
        numItemTimeSphere.text = "" + (CacheGame.GetTotalItemActiveSkill() - CacheGame.numUsingSkill);
        numItemLife.text = "" + (CacheGame.GetTotalItemLife() - CacheGame.numLifeUpUse);
    }

    //private void OnTutorialUpgradeSuccessed(IMessage msg)
    //{
    //    Debug.LogError("OnTutorialUpgradeSuccessed:");
    //    fxTutorialAircraft.SetActive(false);
    //}

    void SetDataItemStart()
    {
        //CacheGame.SetTotalItemPowerUp(10);
        //CacheGame.SetTotalItemActiveSkill(0);
        //CacheGame.SetTotalItemLife(10);
        //CacheGame.SetMaxLevel(CacheGame.GetDifficultCampaign(), 10);

        if (CacheGame.GetTotalItemPowerUp() < 1)
        {
            CacheGame.numItemPowerUpUse = 0;
        }
        if (CacheGame.GetTotalItemActiveSkill() < 1)
        {
            CacheGame.numUsingSkill = 0;
        }

        if (CacheGame.GetTotalItemLife() < 1)
        {
            CacheGame.numLifeUpUse = 0;
        }

        if (CacheGame.numItemPowerUpUse == 1)
        {
            spriteBtnPowerUp.GetComponent<UISpriteAnimation>().enabled = false;
            spriteBtnPowerUp.spriteName = "Btn_powerup_Used";
        }
        else
        {
            if (CacheGame.GetTotalItemPowerUp() < 1)
            {
                spriteBtnPowerUp.GetComponent<UISpriteAnimation>().enabled = false;
                spriteBtnPowerUp.spriteName = "Btn_powerup_Use_0";
            }
            else
            {
                spriteBtnPowerUp.GetComponent<UISpriteAnimation>().enabled = true;
                spriteBtnPowerUp.GetComponent<UISpriteAnimation>().ResetToBeginning();
            }

        }
        numItemPowerUp.text = "" + (CacheGame.GetTotalItemPowerUp() - CacheGame.numItemPowerUpUse);
        //
        if (CacheGame.numUsingSkill == 1)
        {
            spriteBtnActiveSkill.GetComponent<UISpriteAnimation>().enabled = false;
            spriteBtnActiveSkill.spriteName = "Btn_powerup_Used";
        }
        else
        {
            if (CacheGame.GetTotalItemActiveSkill() < 1)
            {
                spriteBtnActiveSkill.GetComponent<UISpriteAnimation>().enabled = false;
                spriteBtnActiveSkill.spriteName = "Btn_powerup_Use_0";
            }
            else
            {
                spriteBtnActiveSkill.GetComponent<UISpriteAnimation>().enabled = true;
                spriteBtnActiveSkill.GetComponent<UISpriteAnimation>().ResetToBeginning();
            }

        }
        numItemTimeSphere.text = "" + (CacheGame.GetTotalItemActiveSkill() - CacheGame.numUsingSkill);
        //
        if (CacheGame.numLifeUpUse == 1)
        {
            spriteBtnLife.GetComponent<UISpriteAnimation>().enabled = false;
            spriteBtnLife.spriteName = "Btn_powerup_Used";
        }
        else
        {
            if (CacheGame.GetTotalItemLife() < 1)
            {
                spriteBtnLife.GetComponent<UISpriteAnimation>().enabled = false;
                spriteBtnLife.spriteName = "Btn_powerup_Use_0";
            }
            else
            {
                spriteBtnLife.GetComponent<UISpriteAnimation>().enabled = true;
                spriteBtnLife.GetComponent<UISpriteAnimation>().ResetToBeginning();
            }
        }
        numItemLife.text = "" + (CacheGame.GetTotalItemLife() - CacheGame.numLifeUpUse);
    }

    public void SetPopupOpen()
    {
        titleCampaign.SetActive(true);

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            panelEndless.SetActive(false);
            panelCampaign.SetActive(true);
            panelEvent.SetActive(false);

            listButtonEndless.SetActive(false);
            listButtonCampain.SetActive(true);
            listButtonEvent.SetActive(false);
            SetNumStar();

            //Bật panel requirement khi chưa có máy bay hoặc máy bay chưa đủ cấp độ
            if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra)
            {
                lNumLevel.text = ScriptLocalization.level_extra_name.Replace("%{number}", CacheGame.GetCurrentLevel().ToString());
                int aircraftIndex = ExtraLevelRequirementSheet.Get(CacheGame.GetCurrentLevel()).aircraft_index;
                Rank aircraftRank = (Rank)ExtraLevelRequirementSheet.Get(CacheGame.GetCurrentLevel()).rank;

                CacheGame.SetSpaceShipUserSelectedExtraLevel(aircraftIndex);

                if (CacheGame.GetSpaceShipIsUnlocked(aircraftIndex) != 1 || CacheGame.GetSpaceShipRank((AircraftTypeEnum)aircraftIndex) < aircraftRank)
                {
                    panelSelectDeviceHandler.gameObject.SetActive(false);
                    panelExLevelRequire.ShowPanel((AircraftTypeEnum)aircraftIndex, aircraftRank);
                    btnStartCampaign.SetActive(false);
                }
                else
                {
                    panelSelectDeviceHandler.ShowPanel();
                    panelExLevelRequire.gameObject.SetActive(false);
                    btnStartCampaign.SetActive(true);
                }
            }
            else
            {
                lNumLevel.text = ScriptLocalization.level_upgrade + " " + CacheGame.GetCurrentLevel();
                panelSelectDeviceHandler.ShowPanel();
                panelExLevelRequire.gameObject.SetActive(false);
                btnStartCampaign.SetActive(true);
            }

            if (CacheGame.GetDifficultCampaign() == GameContext.DIFFICULT_NOMAL)
            {
                panelFirstRewardsLevel.SetActive(true);
                //panelFirstRewardsLevel.transform.localPosition = new Vector3(0f, 136f, 0f);

                panelCampaign.transform.localPosition = new Vector3(0f, 235f, 0f);
                panelPowerUp.transform.localPosition = new Vector3(0f, -216f, 0f);
                panelSelectDeviceHandler.transform.localPosition = new Vector3(0f, -265f, 0f);

                panelExLevelRequire.gameObject.transform.localPosition = new Vector3(0f, -379f, 0f);
            }
            else
            {
                panelFirstRewardsLevel.SetActive(false);

                panelCampaign.transform.localPosition = new Vector3(0f, 382f, 0f);
                panelPowerUp.transform.localPosition = new Vector3(0f, -119f, 0f);
                panelSelectDeviceHandler.transform.localPosition = new Vector3(0f, -200f, 0f);
                panelExLevelRequire.gameObject.transform.localPosition = new Vector3(0f, -316f, 0f);

            }

        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
        {
            panelFirstRewardsLevel.SetActive(false);

            panelEndless.SetActive(true);
            panelCampaign.SetActive(false);
            panelEvent.SetActive(false);
            //
            listButtonEndless.SetActive(true);
            listButtonCampain.SetActive(false);
            listButtonEvent.SetActive(false);
            //
            lNumLevel.text = I2.Loc.ScriptLocalization.endless;
            lNumCanPlayToday.text = CacheGame.GetTotalPlayEndless() + "";
            lNumWaveReached.text = ScriptLocalization.highest_wave.Replace("%{wave_number}", CacheGame.GetMaxWavesEndlessReached().ToString());

            panelEndless.transform.localPosition = new Vector3(0f, 355f, 0f);
            panelPowerUp.transform.localPosition = new Vector3(0f, -156f, 0f);
            panelSelectDeviceHandler.transform.localPosition = new Vector3(0f, -216f, 0f);
            panelSelectDeviceHandler.ShowPanel();

        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            //panelFirstRewardsLevel.SetActive(false);
            //ReloadHalloweenUI();
            titleCampaign.SetActive(false);
            panelEndless.SetActive(false);
            panelCampaign.SetActive(false);
            panelEvent.SetActive(true);
            ////
            listButtonEndless.SetActive(false);
            listButtonCampain.SetActive(false);
            listButtonEvent.SetActive(true);
            ////
            lNumLevel.text = "Christmas";
            //lNumCanPlayToday.text = CacheGame.GetTotalPlayEndless() + "";
            //lNumWaveReached.text = "" + CacheGame.GetMaxWavesEndlessReached();

            //panelEndless.transform.localPosition = new Vector3(0f, 355f, 0f);
            panelPowerUp.transform.localPosition = new Vector3(0f, -156f, 0f);
            panelSelectDeviceHandler.transform.localPosition = new Vector3(0f, -216f, 0f);
            panelSelectDeviceHandler.ShowPanel();
        }
    }

    private bool IsTheSameDay()
    {
        return MinhCacheGame.GetLastDayPlayLevelHalloween() == CachePvp.dateTime.Day;
    }




    //-----------------------------------------
    string starShow;
    string starHide = "icon_blackstar_ingame";
    //
    void SetNumStar()
    {
        // =1 la` star da~ tung` show, con` lai la` chua
        int trueFalseShowStar1 = CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 1, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);
        int trueFalseShowStar2 = CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 2, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);
        int trueFalseShowStar3 = CacheGame.GetNumStarLevel(CacheGame.GetCurrentLevel(), 3, CacheGame.GetDifficultCampaign(), GameContext.levelCampaignMode);


        switch (CacheGame.GetDifficultCampaign())
        {
            case "Normal":
                starShow = "icon_bluestar_ingame";
                break;
            case "Hard":
                starShow = "icon_goldstar_ingame";
                break;
            case "Hell":
                starShow = "icon_redstar_ingame";
                break;
            default:
                break;
        }
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra)
        {
            starShow = "icon_viostar_ingame";
        }


        // --------Get Set star1----------
        if (trueFalseShowStar1 == 1)
        {
            sStar1.spriteName = starShow;
            lGetStar1.color = Color.white;
        }
        else
        {
            sStar1.spriteName = starHide;
            lGetStar1.color = colorTextStarHide;
        }
        // --------Get Set star2----------
        if (trueFalseShowStar2 == 1)
        {
            sStar2.spriteName = starShow;
            lGetStar2.color = Color.white;
        }
        else
        {
            sStar2.spriteName = starHide;
            lGetStar2.color = colorTextStarHide;
        }


        // --------Get Set star3----------

        if (trueFalseShowStar3 == 1)
        {
            sStar3.spriteName = starShow;
            lGetStar3.color = Color.white;
        }
        else
        {
            sStar3.spriteName = starHide;
            lGetStar3.color = colorTextStarHide;
        }
    }

    //-------------------Click USE Item-----------------------------------
    void ShowNotifiClickUseItem(string text)
    {
        labelNotifiClickItem.text = text;
        labelNotifiClickItem.gameObject.SetActive(true);

        StopCoroutine("HideNotifiClickItem");
        StartCoroutine("HideNotifiClickItem");
    }

    IEnumerator HideNotifiClickItem()
    {
        yield return new WaitForSeconds(1.6f);
        labelNotifiClickItem.gameObject.SetActive(false);
    }



    public void Button_UseItemPowerUp()
    {
        SoundManager.PlayClickButton();

        if (CacheGame.GetTotalItemPowerUp() > 0)
        {
            if (CacheGame.numItemPowerUpUse == 1)
            {
                CacheGame.numItemPowerUpUse = 0;
                if (CacheGame.GetTotalItemPowerUp() < 1)
                {
                    spriteBtnPowerUp.GetComponent<UISpriteAnimation>().enabled = false;
                    spriteBtnPowerUp.spriteName = "Btn_powerup_Use_0";
                }
                else
                {
                    spriteBtnPowerUp.GetComponent<UISpriteAnimation>().enabled = true;
                    spriteBtnPowerUp.GetComponent<UISpriteAnimation>().ResetToBeginning();
                }
                //
                StopCoroutine("HideNotifiClickItem");
            }
            else
            {
                CacheGame.numItemPowerUpUse = 1;

                spriteBtnPowerUp.GetComponent<UISpriteAnimation>().enabled = false;
                spriteBtnPowerUp.spriteName = "Btn_powerup_Used";
                //ShowNotifiClickUseItem("+1 Power-Up");
                ShowNotifiClickUseItem("+1  " + ScriptLocalization.power_up);

            }
            numItemPowerUp.text = "" + (CacheGame.GetTotalItemPowerUp() - CacheGame.numItemPowerUpUse);
        }
        else
        {
            ShowNotifiClickUseItem("+1  " + ScriptLocalization.power_up);
        }

    }

    public void Button_UseItemSkill()
    {
        SoundManager.PlayClickButton();

        if (CacheGame.GetTotalItemActiveSkill() > 0)
        {
            if (CacheGame.numUsingSkill == 1)
            {
                CacheGame.numUsingSkill = 0;
                if (CacheGame.GetTotalItemActiveSkill() < 1)
                {
                    spriteBtnActiveSkill.GetComponent<UISpriteAnimation>().enabled = false;
                    spriteBtnActiveSkill.spriteName = "Btn_powerup_Use_0";
                }
                else
                {
                    spriteBtnActiveSkill.GetComponent<UISpriteAnimation>().enabled = true;
                    spriteBtnActiveSkill.GetComponent<UISpriteAnimation>().ResetToBeginning();
                }
                StopCoroutine("HideNotifiClickItem");
            }
            else
            {
                CacheGame.numUsingSkill = 1;
                //sChooseItemTimeSphere.SetActive(true);
                spriteBtnActiveSkill.GetComponent<UISpriteAnimation>().enabled = false;
                spriteBtnActiveSkill.spriteName = "Btn_powerup_Used";
                ShowNotifiClickUseItem("+1 " + ScriptLocalization.ActiveSkill);
            }
            numItemTimeSphere.text = "" + (CacheGame.GetTotalItemActiveSkill() - CacheGame.numUsingSkill);
        }
        else
        {
            ShowNotifiClickUseItem("+1 " + ScriptLocalization.ActiveSkill);
        }

    }

    public void Button_UseItemLife()
    {
        SoundManager.PlayClickButton();

        if (CacheGame.GetTotalItemLife() > 0)
        {
            if (CacheGame.numLifeUpUse == 1)
            {
                CacheGame.numLifeUpUse = 0;
                if (CacheGame.GetTotalItemLife() < 1)
                {
                    spriteBtnLife.GetComponent<UISpriteAnimation>().enabled = false;
                    spriteBtnLife.spriteName = "Btn_powerup_Use_0";
                }
                else
                {
                    spriteBtnLife.GetComponent<UISpriteAnimation>().enabled = true;
                    spriteBtnLife.GetComponent<UISpriteAnimation>().ResetToBeginning();
                }
                StopCoroutine("HideNotifiClickItem");
            }
            else
            {
                CacheGame.numLifeUpUse = 1;
                //sChooseItemLife.SetActive(true);
                spriteBtnLife.GetComponent<UISpriteAnimation>().enabled = false;
                spriteBtnLife.spriteName = "Btn_powerup_Used";
                ShowNotifiClickUseItem("+1 " + ScriptLocalization.life);
            }
            numItemLife.text = "" + (CacheGame.GetTotalItemLife() - CacheGame.numLifeUpUse);
        }
        else
        {
            ShowNotifiClickUseItem("+1 " + ScriptLocalization.life);
        }

    }


    //-------------------Button-----------------------------------

    public void ButtonUpgrade()
    {
        SoundManager.PlayShowPopup();
        //DontDestroyManager.Instance.popupShop_Obj.SetActive(true);
        //DontDestroyManager.Instance.popupShop_Script.ChangeWhenOpenShop();
        PopupManagerCuong.Instance.ShowShopPopup();
        if (PopupShop.Instance != null)
        {
            PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.Upgrade);
        }
        //FirebaseAnalytics.LogEvent("Click", new Parameter("Button", "Shop"));
        //FirebaseLogSpaceWar.LogClickButton("Upgrade");
        gameObject.SetActive(false);
    }


    public void Button_Back()
    {
        SoundManager.PlayClickButton();
        if (PopupManager.Instance.IsGloryChestPopupActive())
        {
            MessageDispatcher.SendMessage(EventName.GloryChest.CloseSelectLevel.ToString());
        }
        StopCoroutine("HideNotifiClickItem");
        labelNotifiClickItem.gameObject.SetActive(false);
        gameObject.SetActive(false);
        PopupManager.Instance.ShowChatWorldPopup();
    }

    public void Button_Play()
    {
        SoundManager.PlayClickButton();


        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() - CacheGame.numItemPowerUpUse);
            CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() - CacheGame.numUsingSkill);
            CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() - CacheGame.numLifeUpUse);

            //Debug.LogError("CacheFireBase.GetTypeTestLevelBoss: " + CacheFireBase.GetType14LevelFirst);

            //if (CacheGame.GetCurrentLevel() < 15)
            //{
            //    LoadingSceneManager.Instance.LoadSceneMainUI("Level" + CacheGame.GetCurrentLevel() + CacheFireBase.GetType14LevelFirst);
            //}
            //else
            //{

            if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
            {
                LoadingSceneManager.Instance.LoadSceneMainUI("Level" + CacheGame.GetCurrentLevel());
            }
            else if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra)
            {
                LoadingSceneManager.Instance.LoadSceneMainUI("Level" + CacheGame.GetCurrentLevel() + "Ex");
            }
            //}


        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.EndLess)
        {

            if (CacheGame.GetTotalPlayEndless() > 0)
            {
                CacheGame.SetTotalPlayEndless(CacheGame.GetTotalPlayEndless() - 1);
                CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() - CacheGame.numItemPowerUpUse);
                CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() - CacheGame.numUsingSkill);
                CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() - CacheGame.numLifeUpUse);

                LoadingSceneManager.Instance.LoadSceneMainUI("Endless");
            }
            else
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_comeback_tomorrow, false, 1.6f);
            }

        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            if (panelSelectLevelEvent.Ci == null)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.notifi_pvp_disconnect_server, false, 1.6f);
                return;
            }

            if (panelSelectLevelEvent.Ci.info == null)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.notifi_pvp_disconnect_server, false, 1.6f);
                return;
            }

            if (panelSelectLevelEvent.Ci.info.ticket <= 0)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.msg_not_enough_ticket, false, 1.6f);
                return;
            }

            new CSSelectLvEventTicket().Send();

            //CacheGame.SetTotalPlayEndless(CacheGame.GetTotalPlayEndless() - 1);
            //CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() - CacheGame.numItemPowerUpUse);
            //CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() - CacheGame.numUsingSkill);
            //CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() - CacheGame.numLifeUpUse);

            //LoadingSceneManager.Instance.LoadSceneMainUI("LevelSummer");

            //MinhCacheGame.SetAircraftUsedTimeHalloween((AircraftTypeEnum)CacheGame.GetSpaceShipUserSelected(), MinhCacheGame.GetAircraftUsedTimeHalloween((AircraftTypeEnum)CacheGame.GetSpaceShipUserSelected()) - 1);
        }


    }
}
