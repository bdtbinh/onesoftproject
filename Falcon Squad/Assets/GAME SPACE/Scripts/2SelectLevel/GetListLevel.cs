﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GetListLevel : MonoBehaviour
{
    public GameObject listLevel;
    //	public GameObject listLine;
    public GameObject levelItem;
    //	public GameObject levelBossItem;
    public UIScrollBar scrollBar;

    public List<GameObject> listLevelItemObj;
    //

    void Awake()
    {
        ChangeDifficultCampaign.Instance.maxLevel = CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign());
        Debug.LogError("VAOOOOO1: " + ChangeDifficultCampaign.Instance.maxLevel);
        Debug.Log("maxlevel " + CacheGame.GetDifficultCampaign() + CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) + "  MaxLevel3Difficult " + CacheGame.GetMaxLevel3Difficult() + "  CurrentWorld " + CacheGame.GetCurrentWorld());


        //SetScrollBarBySceneFrom();
        ChangeScrollBarByLevel(ChangeDifficultCampaign.Instance.maxLevel);

        //scrollBar.value = 1f - (float)ChangeDifficultCampaign.Instance.maxLevel / 35f;
        SetNumLevel();

    }

    public void SetNumLevel()
    {
        int num = 0;
        foreach (Transform child in listLevel.transform)
        {
            num++;

            GameObject levelItemIns = NGUITools.AddChild(child.gameObject, levelItem);
            levelItemIns.name = "LevelIns " + num;
            levelItemIns.GetComponent<LevelItemObj>().SetLevel(num);
            listLevelItemObj.Add(levelItemIns);
            child.GetComponent<UI2DSprite>().enabled = false;

        }
    }


    public void ChangeDataFollowDifficult()
    {
        for (int i = 0; i < listLevelItemObj.Count; i++)
        {
            listLevelItemObj[i].GetComponent<LevelItemObj>().SetLevel(i + 1);
        }
    }




    void SetScrollBarBySceneFrom()
    {

        if (GameContext.typeShowBarFromScene == GameContext.TypeShowBarFromScene.Main)
        {
            if (CacheGame.GetCurrentLevel() > 0)
            {
                ChangeScrollBarByLevel(CacheGame.GetCurrentLevel());
            }
            else
            {
                ChangeScrollBarByLevel(ChangeDifficultCampaign.Instance.maxLevel);
            }
        }
        else
        {
            ChangeScrollBarByLevel(ChangeDifficultCampaign.Instance.maxLevel);
        }
    }

    public void ChangeScrollBarByLevel(int levelSet)
    {
        if (levelSet <= 4)
        {
            scrollBar.value = 1f;
        }
        else if (levelSet <= 8)
        {
            scrollBar.value = 0.9239663f;
        }
        else if (levelSet <= 12)
        {
            scrollBar.value = 0.8362192f;
        }
        else if (levelSet <= 16)
        {
            scrollBar.value = 0.7462564f;
        }
        else if (levelSet <= 20)
        {
            scrollBar.value = 0.6681119f;
        }
        else if (levelSet <= 23)
        {
            scrollBar.value = 0.592087f;
        }
        else if (levelSet <= 26)
        {
            scrollBar.value = 0.5229135f;
        }
        else if (levelSet <= 30)
        {
            scrollBar.value = 0.4396975f;
        }
        else if (levelSet <= 33)
        {
            scrollBar.value = 0.3658711f;
        }
        else if (levelSet <= 37)
        {
            scrollBar.value = 0.2831288f;
        }
        else if (levelSet <= 40)
        {
            scrollBar.value = 0.2092554f;
        }
        else if (levelSet <= 43)
        {
            scrollBar.value = 0.142517f;
        }
        else if (levelSet <= 46)
        {
            scrollBar.value = 0.07668328f;
        }
        else
        {
            scrollBar.value = 0f;
        }

    }


}
