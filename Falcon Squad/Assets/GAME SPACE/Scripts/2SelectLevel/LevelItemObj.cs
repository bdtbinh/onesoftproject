﻿using UnityEngine;
using System.Collections.Generic;
using TCore;
using DG.Tweening;
using com.ootii.Messages;
using System;
using Mp.Pvp;

public class LevelItemObj : MonoBehaviour
{

    public int numLevel = 0;
    public UILabel lNumLevel;
    public GameObject sBossObj;
    public GameObject sLock;
    public SpriteRenderer sStar1, sStar2, sStar3;
    public GameObject parentStar;

    public GameObject fxLevelWaiting;
    //	public GameObject fxLevel;
    //public tk2dButton levelBtn;

    public GameObject gameObjectAvatar1;
    public GameObject gameObjectAvatar2;
    public UI2DSprite sAvatar;
    string facebookIdToShow = "";
    string gameCenterIdToShow = "";
    List<Player> listPlayer = new List<Player>();
    private LevelNode node;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    private void OnRefreshAvatar(IMessage rMessage)
    {
        if (!string.IsNullOrEmpty(facebookIdToShow) && CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdToShow))
        {
            sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdToShow];
        }
        if (!string.IsNullOrEmpty(gameCenterIdToShow) && CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(gameCenterIdToShow))
        {
            sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[gameCenterIdToShow];
        }
    }

    public void OnClickAvatar()
    {
        if (listPlayer.Count == 1)
        {
            PopupManager.Instance.ShowProfilePopup(listPlayer[0]);
        }
        else
        {
            node.levelPosManager.ShowFriends(listPlayer, numLevel);
        }
    }

    public void ReloadData(int num)
    {
        listPlayer.Clear();
        for (int i = 0; i < CachePvp.listPlayerLeaderboard.Count; i++)
        {
            int[] levelValue = PvpUtil.LevelValueDecode(CachePvp.listPlayerLeaderboard[i].levelValue);
            int levelNormal = Mathf.Max(0, Mathf.Min(levelValue[0] - 1, GameContext.TOTAL_LEVEL));
            int levelHard = Mathf.Max(0, Mathf.Min(levelValue[2] - 1, GameContext.TOTAL_LEVEL));
            int levelHell = Mathf.Max(0, Mathf.Min(levelValue[4] - 1, GameContext.TOTAL_LEVEL));
            switch (CacheGame.GetDifficultCampaign())
            {
                case GameContext.DIFFICULT_NOMAL:
                    if (levelNormal == num)
                    {
                        listPlayer.Add(CachePvp.listPlayerLeaderboard[i]);
                        if (string.IsNullOrEmpty(gameCenterIdToShow))
                        {
                            gameCenterIdToShow = CachePvp.listPlayerLeaderboard[i].appCenterId;
                        }
                        if (string.IsNullOrEmpty(facebookIdToShow))
                        {
                            facebookIdToShow = CachePvp.listPlayerLeaderboard[i].facebookId;
                        }
                    }
                    break;
                case GameContext.DIFFICULT_HARD:
                    if (levelHard == num)
                    {
                        listPlayer.Add(CachePvp.listPlayerLeaderboard[i]);
                        if (string.IsNullOrEmpty(gameCenterIdToShow))
                        {
                            gameCenterIdToShow = CachePvp.listPlayerLeaderboard[i].appCenterId;
                        }
                        if (string.IsNullOrEmpty(facebookIdToShow))
                        {
                            facebookIdToShow = CachePvp.listPlayerLeaderboard[i].facebookId;
                        }
                    }
                    break;
                case GameContext.DIFFICULT_HELL:
                    if (levelHell == num)
                    {
                        listPlayer.Add(CachePvp.listPlayerLeaderboard[i]);
                        if (string.IsNullOrEmpty(gameCenterIdToShow))
                        {
                            gameCenterIdToShow = CachePvp.listPlayerLeaderboard[i].appCenterId;
                        }
                        if (string.IsNullOrEmpty(facebookIdToShow))
                        {
                            facebookIdToShow = CachePvp.listPlayerLeaderboard[i].facebookId;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(gameCenterIdToShow))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(gameCenterIdToShow))
                {
                    showImageFB = false;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[gameCenterIdToShow];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(gameCenterIdToShow);
                    if (t != null)
                    {
                        showImageFB = false;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", gameCenterIdToShow, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (string.IsNullOrEmpty(facebookIdToShow))
            {
                sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            }
            else
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdToShow))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdToShow];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + facebookIdToShow + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, facebookIdToShow);
                }
            }
        }
        if (listPlayer.Count == 0)
        {
            gameObjectAvatar1.SetActive(false);
            gameObjectAvatar2.SetActive(false);
        }
        else if (listPlayer.Count == 1)
        {
            gameObjectAvatar1.SetActive(true);
            gameObjectAvatar2.SetActive(false);
        }
        else
        {
            gameObjectAvatar1.SetActive(true);
            gameObjectAvatar2.SetActive(true);
        }
    }

    public void SetLevel(int num)
    {
        //Debug.LogError("Nummmmmmmmmmmmmm: " + num);
        node = transform.parent.GetComponent<LevelNode>();
        facebookIdToShow = "";
        gameCenterIdToShow = "";
        listPlayer.Clear();
        numLevel = num;
        lNumLevel.text = "" + num;
        for (int i = 0; i < CachePvp.listPlayerLeaderboard.Count; i++)
        {
            int[] levelValue = PvpUtil.LevelValueDecode(CachePvp.listPlayerLeaderboard[i].levelValue);
            int levelNormal = Mathf.Max(0, Mathf.Min(levelValue[0] - 1, GameContext.TOTAL_LEVEL));
            int levelHard = Mathf.Max(0, Mathf.Min(levelValue[2] - 1, GameContext.TOTAL_LEVEL));
            int levelHell = Mathf.Max(0, Mathf.Min(levelValue[4] - 1, GameContext.TOTAL_LEVEL));
            switch (CacheGame.GetDifficultCampaign())
            {
                case GameContext.DIFFICULT_NOMAL:
                    if (levelNormal == num)
                    {
                        listPlayer.Add(CachePvp.listPlayerLeaderboard[i]);
                        if (string.IsNullOrEmpty(gameCenterIdToShow))
                        {
                            gameCenterIdToShow = CachePvp.listPlayerLeaderboard[i].appCenterId;
                        }
                        if (string.IsNullOrEmpty(facebookIdToShow))
                        {
                            facebookIdToShow = CachePvp.listPlayerLeaderboard[i].facebookId;
                        }
                    }
                    break;
                case GameContext.DIFFICULT_HARD:
                    if (levelHard == num)
                    {
                        listPlayer.Add(CachePvp.listPlayerLeaderboard[i]);
                        if (string.IsNullOrEmpty(gameCenterIdToShow))
                        {
                            gameCenterIdToShow = CachePvp.listPlayerLeaderboard[i].appCenterId;
                        }
                        if (string.IsNullOrEmpty(facebookIdToShow))
                        {
                            facebookIdToShow = CachePvp.listPlayerLeaderboard[i].facebookId;
                        }
                    }
                    break;
                case GameContext.DIFFICULT_HELL:
                    if (levelHell == num)
                    {
                        listPlayer.Add(CachePvp.listPlayerLeaderboard[i]);
                        if (string.IsNullOrEmpty(gameCenterIdToShow))
                        {
                            gameCenterIdToShow = CachePvp.listPlayerLeaderboard[i].appCenterId;
                        }
                        if (string.IsNullOrEmpty(facebookIdToShow))
                        {
                            facebookIdToShow = CachePvp.listPlayerLeaderboard[i].facebookId;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(gameCenterIdToShow))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(gameCenterIdToShow))
                {
                    showImageFB = false;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[gameCenterIdToShow];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(gameCenterIdToShow);
                    if (t != null)
                    {
                        showImageFB = false;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", gameCenterIdToShow, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (string.IsNullOrEmpty(facebookIdToShow))
            {
                sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            }
            else
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdToShow))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdToShow];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + facebookIdToShow + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, facebookIdToShow);
                }
            }
        }
        if (listPlayer.Count == 0)
        {
            gameObjectAvatar1.SetActive(false);
            gameObjectAvatar2.SetActive(false);
        }
        else if (listPlayer.Count == 1)
        {
            gameObjectAvatar1.SetActive(true);
            gameObjectAvatar2.SetActive(false);
        }
        else
        {
            gameObjectAvatar1.SetActive(true);
            gameObjectAvatar2.SetActive(true);
        }

        if (num > ChangeDifficultCampaign.Instance.maxLevel)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;

            fxLevelWaiting.SetActive(false);
            parentStar.SetActive(false);

            if (numLevel % 7 == 0)
            {
                sBossObj.gameObject.SetActive(true);
                sLock.SetActive(false);
                lNumLevel.gameObject.SetActive(false);
            }
            else
            {
                sBossObj.gameObject.SetActive(false);
                sLock.SetActive(true);
                lNumLevel.gameObject.SetActive(false);
            }

        }
        else
        {
            if (num == ChangeDifficultCampaign.Instance.maxLevel)
            {
                fxLevelWaiting.SetActive(true);
            }
            else
            {
                fxLevelWaiting.SetActive(false);
            }

            gameObject.GetComponent<BoxCollider>().enabled = true;
            SetNumStar();

            if (numLevel % 7 == 0)
            {
                sBossObj.gameObject.SetActive(true);
                sLock.SetActive(false);
                lNumLevel.gameObject.SetActive(false);
            }
            else
            {
                sBossObj.gameObject.SetActive(false);
                sLock.SetActive(false);
                lNumLevel.gameObject.SetActive(true);
            }
        }

    }



    //---------------------
    Sprite sStarShow;

    void SetNumStar()
    {

        switch (CacheGame.GetDifficultCampaign())
        {
            case "Normal":
                sStarShow = SelectLevelScene.Instance.spriteStarShowNomal;
                break;
            case "Hard":
                sStarShow = SelectLevelScene.Instance.spriteStarShowHard;
                break;
            case "Hell":
                sStarShow = SelectLevelScene.Instance.spriteStarShowHell;
                break;
            default:
                break;
        }

        parentStar.SetActive(true);


        // --------Get Set star1----------
        if (CacheGame.GetNumStarLevel(numLevel, 1, CacheGame.GetDifficultCampaign(), GameContext.LevelCampaignMode.Normal) == 1)
        {
            sStar1.sprite = sStarShow;
        }
        else
        {
            sStar1.sprite = SelectLevelScene.Instance.spriteStarHide;
        }

        // --------Get Set star2----------
        if (CacheGame.GetNumStarLevel(numLevel, 2, CacheGame.GetDifficultCampaign(), GameContext.LevelCampaignMode.Normal) == 1)
        {
            sStar2.sprite = sStarShow;
        }
        else
        {
            sStar2.sprite = SelectLevelScene.Instance.spriteStarHide;
        }


        // --------Get Set star3----------


        if (CacheGame.GetNumStarLevel(numLevel, 3, CacheGame.GetDifficultCampaign(), GameContext.LevelCampaignMode.Normal) == 1)
        {
            sStar3.sprite = sStarShow;
        }
        else
        {
            sStar3.sprite = SelectLevelScene.Instance.spriteStarHide;
        }


    }
    //------------------------------------------------
    //void OnClick()
    //{

    //    Debug.LogError("ClickLevel:" + numLevel);

    //    SoundManager.PlayClickButton();
    //    transform.DOScale(new Vector3(1.3f, 1.3f, 1f), 0.07f).OnComplete(() =>
    //    {
    //        transform.localScale = Vector3.one;
    //        GameContex.typeOpenSelectItemFrom = GameContex.TypeOpenSelectItemFrom.LevelButton;

    //        //GameContex.showChooseEndLessMode = false;
    //        CacheGame.SetCurrentLevel(numLevel);
    //        GameContex.modeGamePlay = GameContex.ModeGamePlay.Campaign;

    //        DontDestroyManager.Instance.popupSelectLevel.SetActive(true);
    //        DontDestroyManager.Instance.popupSelectLevel.GetComponent<PopupSelectLvCampaign>().SetPopupOpen();

    //    });


    //}



    public void OnClickLevelItem()
    {
        SoundManager.PlayClickButton();

        //Check xem có được phép mở level để chơi không
        //if (!CanOpenLevel())
        //{
        //    MSLManager.Instance.PopupUnlockLevel.ShowPopup(numLevel);
        //    return;
        //}

        //if (numLevel % 7 == 0 && !MinhCacheGame.IsAlreadyUnlockLevel(numLevel, CacheGame.GetDifficultCampaign()) && CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) > numLevel)
        //    return;

        //Debug.LogError("ClickLevel:" + numLevel);

        //transform.DOScale(new Vector3(1.3f, 1.3f, 1f), 0.07f).OnComplete(() =>
        //{
        //transform.localScale = Vector3.one;

        //GameContext.typeOpenSelectItemFrom = GameContext.TypeOpenSelectItemFrom.LevelButton;

        //GameContex.showChooseEndLessMode = false;
        CacheGame.SetCurrentLevel(numLevel);
        GameContext.modeGamePlay = GameContext.ModeGamePlay.Campaign;
        GameContext.levelCampaignMode = GameContext.LevelCampaignMode.Normal;

        //DontDestroyManager.Instance.popupSelectLevel.SetActive(true);
        //DontDestroyManager.Instance.popupSelectLevel.GetComponent<PopupSelectLvCampaign>().SetPopupOpen();
        PopupManagerCuong.Instance.ShowSelectLevelPopup();
        //});
    }

    //bool CanOpenLevel()
    //{
    //    print("CanOpenLevel: " + numLevel % 7);
    //    if (numLevel % 7 != 0)
    //        return true;

    //    print("CanOpenLevel: " + CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) + " * " + numLevel);
    //    if (CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) > numLevel)
    //        return true;

    //    print("CanOpenLevel: " + MinhCacheGame.IsAlreadyUnlockLevel(numLevel, CacheGame.GetDifficultCampaign()));
    //    if (MinhCacheGame.IsAlreadyUnlockLevel(numLevel, CacheGame.GetDifficultCampaign()))
    //        return true;

    //    return false;
    //}
}