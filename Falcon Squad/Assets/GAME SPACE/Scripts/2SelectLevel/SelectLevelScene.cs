﻿using UnityEngine;
using System.Collections;
using TCore;
using Mp.Pvp;

public class SelectLevelScene : MonoBehaviour
{

    public GameObject panel3Difficult;
    public GameObject btnBack;
    public GameObject btnShop;
    public GameObject btnQA;
    public GameObject btnSetting;
    public GameObject objTotalStar;
    public GameObject fxTutLevel1;

    //public GameObject popupSelectObj;
    //public GameObject glowBtnShop;

    //
    public Sprite spriteStarHide, spriteStarShowNomal, spriteStarShowHard, spriteStarShowHell;
    public Sprite spriteItemNomal, spriteItemHard, spriteItemHell;
    //
    //public GetListLevel getListLevelScr;

    //public SceneLoaderManager sceneLoaderHomeIns;
    //public SceneLoaderManager sceneLoaderMainIns;


    private static SelectLevelScene _instance;

    public static SelectLevelScene Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<SelectLevelScene>();
            return _instance;
        }
    }

    void Start()
    {
        if (GameContext.isInGameCampaign)
        {
            GameContext.isInGameCampaign = false;
            PvpUtil.SendUpdatePlayer("OpenSelectLevel");
        }
        //PlayerPrefs.Save();
        //
        GameContext.readyClickShowPopup = false;
        StartCoroutine("ReadyClickButtonShowPopup");
        readyClickBack = true;
        SoundManager.StopMainBG();
        SoundManager.StopMain2BG();
        SoundManager.PlayHomeBG();

        Time.timeScale = 1f;

        if (CacheGame.GetMaxLevel3Difficult() < 3)
        {
            panel3Difficult.SetActive(false);
            btnShop.SetActive(false);
            btnQA.SetActive(false);
            //btnSetting.SetActive(false);
            objTotalStar.SetActive(false);
            //if (CacheGame.GetTutorialGame() != 1)
            //{
            btnBack.SetActive(false);
            //}
        }
        if (CacheGame.GetMaxLevel3Difficult() == 1)
        {
            //fxTutLevel1.SetActive(true);
        }

        CheckOpenPopupSelect();
        new CSSceneLoaded(1).Send();

    }


    private bool readyClickBack;

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && readyClickBack)
        {
            readyClickBack = false;
            StartCoroutine("ReadyClickBack");

            SoundManager.PlayShowPopup();
            if (PopupManagerCuong.Instance.IsActivePopupLoadingPurchase())
            {
                return;
            }
            if (PopupManagerCuong.Instance.IsActiveItemRewardPopup())
            {
                return;
                //PopupManagerCuong.Instance.HideItemRewardPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveItemInfoPopup())
            {
                PopupManagerCuong.Instance.HideItemInfoPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveStarChestPopup())
            {
                PopupManagerCuong.Instance.HideStarChestPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveQuestOpenIAPPopup())
            {
                PopupManagerCuong.Instance.HideQuestOpenIAPPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveShopPopup())
            {
                if (PopupManager.Instance.IsVipPopupActive() && !PopupVip.OpenFromHome)
                {
                    if (PopupManager.Instance.IsVipInfoActive())
                        PopupManager.Instance.HideVipInfo();
                    else
                        PopupManager.Instance.HideVipPopup();
                }
                else
                    PopupManagerCuong.Instance.HideShopPopup();
            }
            else if (PopupManagerCuong.Instance.IsActivePopupPushInfinityPack())
            {
                PopupManagerCuong.Instance.HidePopupPushInfinityPack();
            }
            else if (PopupManagerCuong.Instance.IsActivePopupPushTicketLuckyPack())
            {
                PopupManagerCuong.Instance.HidePopupPushTicketLuckyPack();
            }
            else if (PopupManagerCuong.Instance.IsActivePopupSaleAircraft())
            {
                PopupManagerCuong.Instance.HidePopupSaleAircraft();
            }
            else if (PopupManagerCuong.Instance.IsActivePopupSaleGoldGem())
            {
                PopupManagerCuong.Instance.HidePopupSaleGoldGem();
            }

            else if (PopupManager.Instance.IsClaimPopupActive())
            {
                //PopupManager.Instance.HideVideoRewardPopup();
            }
            else if (PopupManager.Instance.IsVideoRewardPopupActive())
            {
                PopupManager.Instance.HideVideoRewardPopup();
            }
            else if (PopupManager.Instance.IsInvitePopupActive())
            {
                PopupManager.Instance.HideInvitePopup(true);
            }
            else if (PopupManagerCuong.Instance.IsActiveGiftCodePopup())
            {
                PopupManagerCuong.Instance.HideGiftCodePopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSettingPopup())
            {
                PopupManagerCuong.Instance.HideSettingPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveQAPopup())
            {
                PopupManagerCuong.Instance.HideQAPopup();
            }
            else if (PopupManagerCuong.Instance.IsActive3PackPopup())
            {
                PopupManagerCuong.Instance.Hide3PackPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveRoyaltyPopup())
            {
                PopupManagerCuong.Instance.HideRoyaltyPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveCardPackPopup())
            {
                PopupManagerCuong.Instance.HideCardPackPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSelectLevelPopup())
            {
                PopupManagerCuong.Instance.HideSelectLevelPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSelectLevelPopup())
            {
                PopupManagerCuong.Instance.HideSelectLevelPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSelectAircraftPopup())
            {
                //if (PopupSelectAircraft.openSelectFromType == SelectDeviceHandler.OpenSelectFromType.FromSelectLevel)
                //{
                //    PopupManagerCuong.Instance.ShowSelectLevelPopup();
                //}
                PopupManagerCuong.Instance.HideSelectAircraftPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSelectWingmanPopup())
            {
                //if (PopupSelectWingman.openSelectFromType == SelectDeviceHandler.OpenSelectFromType.FromSelectLevel)
                //{
                //    PopupManagerCuong.Instance.ShowSelectLevelPopup();
                //}
                PopupManagerCuong.Instance.HideSelectWingmanPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSelectWingPopup())
            {
                //if (PopupSelectAircraft.openSelectFromType == SelectDeviceHandler.OpenSelectFromType.FromSelectLevel)
                //{
                //    PopupManagerCuong.Instance.ShowSelectLevelPopup();
                //}
                PopupManagerCuong.Instance.HideSelectWingPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSelectLevelPopup())
            {
                PopupManagerCuong.Instance.HideSelectLevelPopup();
            }
            else
            {
                if (CacheGame.GetMaxLevel3Difficult() > 2)
                {
                    if (!LoadingSceneManager.Instance.loadingOpened)
                    {
                        LoadingSceneManager.Instance.LoadSceneHomeUI("Home");
                    }

                }
                else
                {
                    if (PopupManagerCuong.Instance.IsActiveExitGamePopup())
                    {
                        PopupManagerCuong.Instance.HideExitGamePopup();
                    }
                    else
                    {
                        PopupManagerCuong.Instance.ShowExitGamePopup();
                    }
                }
                //sceneLoaderHomeIns.LoadLevel("Home");
                //uiRoot.SetActive(false);
            }

        }

    }

    IEnumerator ReadyClickBack()
    {
        yield return new WaitForSeconds(0.38f);
        readyClickBack = true;
    }

    IEnumerator ReadyClickButtonShowPopup()
    {
        yield return new WaitForSeconds(1f);
        GameContext.readyClickShowPopup = true;
    }

    void CheckOpenPopupSelect()
    {
        //Kiểm tra xem để bật popup select hangar từ khi try
        if (GameContext.typeOpenSceneFromTry == GameContext.TypeOpenSceneFromTry.SelectLevel)
        {
            switch (GameContext.typeOpenPopupFromTry)
            {
                case GameContext.TypeOpenPopupFromTry.Aircraft:
                    PopupManagerCuong.Instance.ShowSelectAircraftPopup(SelectDeviceHandler.OpenSelectFromType.FromSelectLevel, true);
                    break;

                case GameContext.TypeOpenPopupFromTry.Wingman:
                    PopupManagerCuong.Instance.ShowSelectWingmanPopup(WingmanPosType.None, SelectDeviceHandler.OpenSelectFromType.FromSelectLevel, true);
                    break;

                default:
                    break;
            }
            GameContext.modeGamePlay = GameContext.ModeGamePlay.Campaign;
            GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.NotShow;
            return;
        }


        switch (GameContext.typeLoadPopupSelectCampaign)
        {
            case GameContext.TypeLoadPopupSelectCampaign.Main_Click_Retry:
                PopupManagerCuong.Instance.ShowSelectLevelPopup();
                break;
            case GameContext.TypeLoadPopupSelectCampaign.Main_Click_Next:
                if (CacheGame.GetCurrentLevel() < GameContext.TOTAL_LEVEL)
                {
                    CacheGame.SetCurrentLevel(CacheGame.GetCurrentLevel() + 1);
                }

                if (CanOpenLevel())
                {
                    //DontDestroyManager.Instance.popupSelectLevel.SetActive(true);
                    //DontDestroyManager.Instance.popupSelectLevel.GetComponent<PopupSelectLvCampaign>().SetPopupOpen();
                    PopupManagerCuong.Instance.ShowSelectLevelPopup();
                }
                else
                {
                    MSLManager.Instance.PopupUnlockLevel.ShowPopup(CacheGame.GetCurrentLevel());
                }

                break;

        }
        GameContext.typeLoadPopupSelectCampaign = GameContext.TypeLoadPopupSelectCampaign.NotShow;
    }



    void LoadDataUpgradeShop()
    {
        //		ConfigLoader.Load ();
        //		Debug.LogError ("ConfigLoader.Load ()");
        //DontDestroyManager.Instance.popupShop_Script.ChangeWhenOpenShop();

    }

    //Minh
    bool CanOpenLevel()
    {

        //Debug.LogError("FireBaseRemote.Instance.isUseStarUnlock : " + FireBaseRemote.Instance.isUseStarUnlock());
        //if (!FireBaseRemote.Instance.isUseStarUnlock())
        //{
        return true;
        //}

        //print("CanOpenLevel: " + numLevel % 7);
        if (CacheGame.GetCurrentLevel() % 7 != 0)
            return true;

        //print("CanOpenLevel: " + CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign() + " * " + numLevel));
        if (CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) > CacheGame.GetCurrentLevel())
            return true;

        //print("CanOpenLevel: " + MinhCacheGame.IsAlreadyUnlockLevel(numLevel, CacheGame.GetDifficultCampaign()));
        if (MinhCacheGame.IsAlreadyUnlockLevel(CacheGame.GetCurrentLevel(), CacheGame.GetDifficultCampaign()))
            return true;

        return false;
    }
}
