﻿using UnityEngine;
using System.Collections;
using TCore;
using Firebase.Analytics;

public class SelectLevelButton : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }


    public void ButtonBack()
    {
        SoundManager.PlayClickButton();
        //		Application.LoadLevel ("Home");
        LoadingSceneManager.Instance.LoadSceneHomeUI("Home");
        //SelectLevelScene.Instance.sceneLoaderHomeIns.LoadLevel("Home");
        //SelectLevelScene.Instance.uiRoot.SetActive(false);
    }

    public void ButtonSetup()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();
            //DontDestroyManager.Instance.popupSetting_Obj.SetActive(true);
            //DontDestroyManager.Instance.popupSetting_Script.Change_When_OpenPopup();
            //PopupManagerCuong.Instance.ShowSettingPopup();
            PopupManagerCuong.Instance.ShowSettingPopup();
            //FirebaseAnalytics.LogEvent("Click", new Parameter("Button", "Setting"));
            FirebaseLogSpaceWar.LogClickButton("Setting");
        }

    }

    public void ButtonShop()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();
            //DontDestroyManager.Instance.popupShop_Obj.SetActive(true);

            //DontDestroyManager.Instance.popupShop_Script.ChangeWhenOpenShop();

            PopupManagerCuong.Instance.ShowShopPopup();
            if (PopupShop.Instance != null)
            {
                //PopupShop.Instance.ChangeWhenOpenShop();
                PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.Upgrade);
            }

            //FirebaseAnalytics.LogEvent("Click", new Parameter("Button", "Shop"));
            //FirebaseLogSpaceWar.LogClickButton("Upgrade");
        }
    }
}
