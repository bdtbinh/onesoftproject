﻿using UnityEngine;
using System.Collections;

namespace TCore
{
	public class SoundManager : MonoBehaviour
	{
		public  AudioSource soundHomeBG;
		public  AudioSource soundMainBG;
		public  AudioSource soundMain2BG;
		public bool isSoundHomeBG = false;
		public bool isSoundMainBG = false;
		public bool issoundMain2BG = false;
		//
		public  AudioSource soundClickButton;
		public  AudioSource soundGameWinShowText;
		public  AudioSource soundGameWinShowPopup;
		public  AudioSource soundGameLose;
		public  AudioSource soundShowPopup;
		public  AudioSource soundStarWin;
		public  AudioSource soundScore;
		public  AudioSource soundEnemy_Bullet;
		public  AudioSource soundEnemy_Laser;
		public  AudioSource soundPlayer_Enemy;
		public  AudioSource soundPlayer_Item;
		public  AudioSource enemyDie;
		public  AudioSource bossDie;
		public  AudioSource playerDie;
		public  AudioSource playerShow;
		public  AudioSource itemShow;
		public  AudioSource enemyShow;
		public  AudioSource highScore;
		public  AudioSource getCoinBig;
		public  AudioSource getCoinSmall;

		//noel
		public  AudioSource soundPlayer_Sock;


		//

		private static SoundManager mInstance;

		public static SoundManager Instance {
			get {
				if (mInstance == null) {
					UIUtils.LoadPrefab<SoundManager> (ref mInstance, "SoundManager", true, true);
				}
				return mInstance;
			}
		}
		//
        
		public static void PlayHomeBG ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1 && PlayerPrefs.GetInt ("MusicValue") != 1) {
				if (!Instance.isSoundHomeBG) {
					Instance.soundHomeBG.Play ();
					Instance.isSoundHomeBG = true;
				}
			} else {
				Instance.soundHomeBG.Stop ();
				Instance.isSoundHomeBG = false;
			}
						
		}

		public static void PlayMain2BG ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1 && PlayerPrefs.GetInt ("MusicValue") != 1) {
				if (!Instance.issoundMain2BG) {
					Instance.soundMain2BG.Play ();
					Instance.issoundMain2BG = true;
				}
			} else {
				Instance.soundMain2BG.Stop ();
				Instance.issoundMain2BG = false;
			}
						
		}

		public static void PlayMainBG ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1 && PlayerPrefs.GetInt ("MusicValue") != 1) {
				if (!Instance.isSoundMainBG) {
					Instance.soundMainBG.Play ();
					Instance.isSoundMainBG = true;
				}
			} else {
				Instance.soundMainBG.Stop ();
				Instance.isSoundMainBG = false;
			}
		}

		//

		public static void StopHomeBG ()
		{
			Instance.soundHomeBG.Stop ();
			Instance.isSoundHomeBG = false;
		}

		public static void StopMain2BG ()
		{
			Instance.soundMain2BG.Stop ();
			Instance.issoundMain2BG = false;
		}

		public static void StopMainBG ()
		{
			Instance.soundMainBG.Stop ();
			Instance.isSoundMainBG = false;
		}




		//------------


		public static void PlayClickButton ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundClickButton.Play ();
			} else {
				Instance.soundClickButton.Stop ();
			}
		}

		public static void SoundScore ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundScore.Play ();
			} else {
				Instance.soundScore.Stop ();
			}
		}

		public static void SoundPlayer_Enemy ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundPlayer_Enemy.Play ();
			} else {
				Instance.soundPlayer_Enemy.Stop ();
			}
		}

		public static void SoundBullet_Enemy ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundEnemy_Bullet.Play ();
			} else {
				Instance.soundEnemy_Bullet.Stop ();
			}
		}

		public static void SoundLaser_Enemy ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundEnemy_Laser.Play ();
			} else {
				Instance.soundEnemy_Laser.Stop ();
			}
		}

		public static void PlayGameWinText ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundGameWinShowText.Play ();
			} else {
				Instance.soundGameWinShowText.Stop ();
			}
		}

		public static void PlayGameWinPopup ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundGameWinShowPopup.Play ();
			} else {
				Instance.soundGameWinShowPopup.Stop ();
			}
		}

		public static void SoundStarWin ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundStarWin.Play ();
			} else {
				Instance.soundStarWin.Stop ();
			}
		}

		//		public static void StopGameWinText ()
		//		{
		//			Instance.soundGameWinShowText.Stop ();
		//		}

		public static void PlayShowPopup ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundShowPopup.Play ();
			} else {
				Instance.soundShowPopup.Stop ();
			}
		}

		public static void SoundPlayer_Item ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundPlayer_Item.Play ();
			} else {
				Instance.soundPlayer_Item.Stop ();
			}
		}

		public static void SoundEnemyDie ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.enemyDie.Play ();
			} else {
				Instance.enemyDie.Stop ();
			}
		}

		public static void SoundPlayerDie ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.playerDie.Play ();
			} else {
				Instance.playerDie.Stop ();
			}
		}

		public static void SoundPlayerShow ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.playerShow.Play ();
			} else {
				Instance.playerShow.Stop ();
			}
		}

		public static void SoundItemShow ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.itemShow.Play ();
			} else {
				Instance.itemShow.Stop ();
			}
		}

		public static void SoundEnemyShow ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.enemyShow.Play ();
			} else {
				Instance.enemyShow.Stop ();
			}
		}

		public static void SoundHightScore ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.highScore.Play ();
			} else {
				Instance.highScore.Stop ();
			}
		}

		public static void SoundGetCoinBig ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.getCoinBig.Play ();
			} else {
				Instance.getCoinBig.Stop ();
			}
		}

		public static void SoundGetCoinSmall ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.getCoinSmall.Play ();
			} else {
				Instance.getCoinSmall.Stop ();
			}
		}


		public static void SoundGameLose ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundGameLose.Play ();
			} else {
				Instance.soundGameLose.Stop ();
			}
		}

		public static void SoundBossDie ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.bossDie.Play ();
			} else {
				Instance.bossDie.Stop ();
			}
		}



		//---noel

		public static void SoundPlayerGetSock ()
		{
			if (PlayerPrefs.GetInt ("SoundValue") != 1) {
				Instance.soundPlayer_Sock.Play ();
			} else {
				Instance.soundPlayer_Sock.Stop ();
			}
		}




	}
}