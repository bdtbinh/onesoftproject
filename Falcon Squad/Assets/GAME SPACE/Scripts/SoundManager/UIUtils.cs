﻿#define USE_NGUI

using UnityEngine;
using System.Collections;

namespace TCore
{
	public class UIUtils
	{
		#if USE_NGUI
		/// <summary>
		/// Apply tween to window. Note to assigne tween in editor first
		/// </summary>
		/// <param name="go">Go.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>

		public static void StartTween<T> (GameObject go) where T : UITweener
		{
			T[] tweens = go.GetComponents<T> ();
			foreach (T tween in tweens) {
				if (tween != null) {
					tween.enabled = true;
					tween.Play ();
				}
			}
		}
	
		/// <summary>
		/// Apply revert tween to window. Note to assigne tween in editor first
		/// </summary>
		/// <param name="go">Go.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void RevertTween<T> (GameObject go) where T : UITweener
		{
			T[] tweens = go.GetComponents<T> ();
			foreach (T tween in tweens) {
				if (tween != null) {
					tween.enabled = true;
					tween.PlayReverse ();
				}
			}
		}

		/// <summary>
		/// Load UI prefab that created by NGUI (Prefab must be stored in Resources folder)
		/// </summary>
		/// <param name="component">Component.</param>
		/// <param name="path">Path of prefab </param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void LoadUIPrefab<T> (ref T component, string path, bool setActive, bool dontDetroyOnLoad) where T : MonoBehaviour
		{
			GameObject go = (GameObject)GameObject.Instantiate ((GameObject)Resources.Load (path));
			string[] spliteName = path.Split (new char[]{'/'}, System.StringSplitOptions.RemoveEmptyEntries);
			go.name = spliteName[spliteName.Length-1];
			component = go.GetComponent<T> ();

			if (dontDetroyOnLoad) GameObject.DontDestroyOnLoad(go);
		
			UIRoot uiRoot = GameObject.FindObjectOfType<UIRoot> () as UIRoot;
			if (uiRoot == null) {
				GameObject.FindObjectOfType<Camera> ().gameObject.AddComponent<UICamera> ();
				component.gameObject.transform.localScale = new Vector3 (0.01f, 0.01f, 0.01f);
			} else {
				GameObject.FindObjectOfType<Camera> ().cullingMask = -1 << 0;
				component.gameObject.transform.parent = uiRoot.transform;
				component.gameObject.transform.localScale = Vector3.one;
			}

			component.gameObject.SetActive (setActive);
		}

		/// <summary>
		/// Loads the prefab (not UI elements).
		/// </summary>
		/// <param name="component">Component.</param>
		/// <param name="path">Path.</param>
		/// <param name="setActive">If set to <c>true</c> set active.</param>
		/// <param name="dontDetroyOnLoad">If set to <c>true</c> dont detroy on load.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void LoadPrefab<T> (ref T component, string path, bool setActive, bool dontDetroyOnLoad) where T : MonoBehaviour
		{
			GameObject go = (GameObject)GameObject.Instantiate ((GameObject)Resources.Load (path));
			string[] spliteName = path.Split (new char[]{'/'}, System.StringSplitOptions.RemoveEmptyEntries);
			go.name = spliteName[spliteName.Length-1];
			if (dontDetroyOnLoad) GameObject.DontDestroyOnLoad(go);
			component = go.GetComponent<T> ();
			component.gameObject.transform.localScale = Vector3.one;
			component.gameObject.SetActive (setActive);
		}

        public static GameObject LoadPrefab(string path, bool setActive, bool dontDetroyOnLoad)
        {
            GameObject go = (GameObject)GameObject.Instantiate((GameObject)Resources.Load(path));
            string[] spliteName = path.Split(new char[] { '/' }, System.StringSplitOptions.RemoveEmptyEntries);
            go.name = spliteName[spliteName.Length - 1];
            if (dontDetroyOnLoad) GameObject.DontDestroyOnLoad(go);
			go.SetActive (setActive);
			return go;
        }

		public static void UnRegisterAllButtonListener (UIButton button)
		{
			button.onClick.Clear ();
		}

		public static void RegisterButtonListener (UIButton button, EventDelegate.Callback listener)
		{
			button.onClick.Add (new EventDelegate (listener));
		}
	
		public static void RegisterTongleListener (UIToggle tongler, EventDelegate.Callback listener)
		{
			tongler.onChange.Add (new EventDelegate (listener));
		}

		public static void RegisterEventTriggerListener (UIEventTrigger tongler, EventDelegate.Callback listener)
		{
			tongler.onPress.Add (new EventDelegate (listener));
		}

		public static void UnRegisterEventTriggerListener (UIEventTrigger tongler, EventDelegate.Callback listener)
		{
			tongler.onPress.Remove (new EventDelegate (listener));
		}

		public static void UnRegisterTongleListener (UIToggle tongler, EventDelegate.Callback listener)
		{
			tongler.onChange.Remove (new EventDelegate (listener));
		}
	
		public static void UnRegisterButtonListener (UIButton button, EventDelegate.Callback listener)
		{
			button.onClick.Remove (new EventDelegate (listener));
		}
	
		public static void RegisterTweenFinishListener (UITweener tween, EventDelegate.Callback listener)
		{
			tween.onFinished.Add (new EventDelegate (listener));
		}
	
		public static void UnRegisterTweenFinishListener (UITweener tween, EventDelegate.Callback listener)
		{
			tween.onFinished.Remove (new EventDelegate (listener));
		}

		#endif
	}



}