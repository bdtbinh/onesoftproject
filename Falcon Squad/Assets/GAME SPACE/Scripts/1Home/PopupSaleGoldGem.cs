﻿using EasyMobile;
using I2.Loc;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class PopupPushSaleGoldGem : MonoBehaviour
{

    public UILabel namePack;
    public UILabel valueSaleGet;
    public UILabel valueBeforeSale;
    public UILabel vipPoint;
    public UILabel priceIAP;
    public UISprite sItemShow;
    public UISprite sGemGold;

    const string NAME_SPRITE_GOLD = "icon_coin_home";
    const string NAME_SPRITE_GEM = "icon_gem_home";

    public static string TYPE_SHOW_GOLD = "gold";
    public static string TYPE_SHOW_GEM = "gem";

    int numValueGetUse;
    string productIapUse;
    string typeGoldGemShow;


    //Dictionary<int, string> NamePackDictionary = new Dictionary<int, string>
    //{
    //    { 1, ScriptLocalization.title_iap_starter },
    //    { 2, ScriptLocalization.title_iap_young_pilot },
    //    { 3, ScriptLocalization.title_iap_captain },
    //    { 4, ScriptLocalization.title_iap_challenger },
    //    { 5, ScriptLocalization.title_iap_conqueror },
    //    { 6, ScriptLocalization.title_iap_commander },
    //};


    Dictionary<int, string> PriceIapGoldDictionary = new Dictionary<int, string>
    {
        { 1, EM_IAPConstants.Product_inapp_2usd },
        { 2, EM_IAPConstants.Product_inapp_gold_5usd },
        { 3, EM_IAPConstants.Product_inapp_10usd },
        { 4, EM_IAPConstants.Product_inapp_20usd },
        { 5, EM_IAPConstants.Product_inapp_gold_50usd },
        { 6, EM_IAPConstants.Product_inapp_100usd },
    };

    Dictionary<int, string> PriceIapGemDictionary = new Dictionary<int, string>
    {
        { 1, EM_IAPConstants.Product_inapp_gem_2usd },
        { 2, EM_IAPConstants.Product_inapp_gem_5usd },
        { 3, EM_IAPConstants.Product_inapp_gem_10usd },
        { 4, EM_IAPConstants.Product_inapp_gem_20usd },
        { 5, EM_IAPConstants.Product_inapp_gem_50usd },
        { 6, EM_IAPConstants.Product_inapp_gem_100usd },
    };


    [Title("ITEM GOLD SALE", bold: true, horizontalLine: true)]
    public ItemGoldSale[] listItemGoldSale;

    [System.Serializable]
    public class ItemGoldSale
    {
        [Tooltip("Item vàng khuyến mãi x2")]
        //public int numGoldSaleGet;
        public int numGoldBeforeSale;
        public string nameSpriteItemShow;
        public int numVipPoint;
    }

    [Title("ITEM GEM SALE", bold: true, horizontalLine: true)]
    public ItemGemSale[] listItemGemSale;

    [System.Serializable]
    public class ItemGemSale
    {
        [Tooltip("Item gem khuyến mãi x2")]
        //public int numGoldSaleGet;
        public int numGemBeforeSale;
        public string nameSpriteItemShow;
        public int numVipPoint;
    }


    public void ShowDataStart(string typeShow, int indexPackShow)
    {
        Debug.LogError("typeShow:" + typeShow + "---TYPE_SHOW_GEM:" + TYPE_SHOW_GEM);
        //namePack.text = NamePackDictionary[indexPackShow] + "";
        typeGoldGemShow = typeShow;
        if (typeShow == TYPE_SHOW_GOLD)
        {
            namePack.text = ScriptLocalization.x2_gold_today;
            sGemGold.spriteName = NAME_SPRITE_GOLD;
            sItemShow.spriteName = listItemGoldSale[indexPackShow - 1].nameSpriteItemShow;
            vipPoint.text = "+ " + listItemGoldSale[indexPackShow - 1].numVipPoint;
            valueBeforeSale.text = "+ " + GameContext.FormatNumber(listItemGoldSale[indexPackShow - 1].numGoldBeforeSale);
            valueSaleGet.text = "+ " + GameContext.FormatNumber(listItemGoldSale[indexPackShow - 1].numGoldBeforeSale * 2);

            priceIAP.text = "" + CuongUtils.GetIapLocalizedPrice(PriceIapGoldDictionary[indexPackShow]);

            //if (InAppPurchasing.GetProductLocalizedData(PriceIapGoldDictionary[indexPackShow]) != null)
            //{
            //    priceIAP.text = "" + InAppPurchasing.GetProductLocalizedData(PriceIapGoldDictionary[indexPackShow]).localizedPriceString;
            //}
            numValueGetUse = listItemGoldSale[indexPackShow - 1].numGoldBeforeSale * 2;
            productIapUse = PriceIapGoldDictionary[indexPackShow];

        }
        else if (typeShow == TYPE_SHOW_GEM)
        {
            namePack.text = ScriptLocalization.x2_gem_today;
            sGemGold.spriteName = NAME_SPRITE_GEM;
            sItemShow.spriteName = listItemGemSale[indexPackShow - 1].nameSpriteItemShow;
            vipPoint.text = "+ " + listItemGemSale[indexPackShow - 1].numVipPoint;
            valueBeforeSale.text = "+ " + GameContext.FormatNumber(listItemGemSale[indexPackShow - 1].numGemBeforeSale);
            valueSaleGet.text = "+ " + GameContext.FormatNumber(listItemGemSale[indexPackShow - 1].numGemBeforeSale * 2);
            if (InAppPurchasing.GetProductLocalizedData(PriceIapGemDictionary[indexPackShow]) != null)
            {
                priceIAP.text = "" + InAppPurchasing.GetProductLocalizedData(PriceIapGemDictionary[indexPackShow]).localizedPriceString;
            }
            numValueGetUse = listItemGemSale[indexPackShow - 1].numGemBeforeSale * 2;
            productIapUse = PriceIapGemDictionary[indexPackShow];
        }
        else
        {
            Debug.LogError("PopupSaleGoldGem: sai type sale");
            PopupManagerCuong.Instance.HidePopupSaleGoldGem();
        }
    }

    public void Btn_Buy_InApp()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(productIapUse, IAPCallbackManager.WherePurchase.push_server);
    }

    public void Btn_Back()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.HidePopupSaleGoldGem();
    }


    void OnEnable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
    }

    void OnDisable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }

    void OnPurchaseSuccessed(string productName)
    {
        if (productName == productIapUse)
        {
            if (typeGoldGemShow == TYPE_SHOW_GOLD)
            {
                PanelCoinGem.Instance.AddCoinTop(numValueGetUse, FirebaseLogSpaceWar.IAP_why, productName);
                PopupManagerCuong.Instance.ShowTextNotifiToast((string)(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + numValueGetUse + " " + ScriptLocalization.gold)), (bool)true, (float)1.8f);
            }
            else
            {
                PanelCoinGem.Instance.AddGemTop(numValueGetUse, FirebaseLogSpaceWar.IAP_why, productName);
                PopupManagerCuong.Instance.ShowTextNotifiToast((string)(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + numValueGetUse + " " + ScriptLocalization.gem)), (bool)true, (float)1.8f);

            }
        }
    }
}
