﻿using System;
using System.Collections;
using System.Collections.Generic;
using EasyMobile;
using I2.Loc;
using TCore;
using UnityEngine;

public class PopupPushAircraft : MonoBehaviour
{
    public UILabel lNumPercentSale;
    public GameObject percentSaleObj;
    public UILabel lNameAircraft;
    public UILabel lLevelAircraft;

    //public UISprite sAircraft;
    public AircraftAnimations aircraftAnimationsScript;

    public UILabel lNumVipPoint;

    public UILabel lPriceOrigi;
    public UILabel lPriceSale;

    public GameObject btnBuyObj;

    private string productIapUse = EM_IAPConstants.Product_unlock_aircraft_2;

    private int indexAircraftSaleUse;


    void OnEnable()
    {
        //SetDataStart();
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
    }
    void OnDisable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }


    public void SetDataStart(int percentSale, int indexAircraftSale)
    {
        PopupManagerCuong.Instance.HideSelectAircraftPopup();
        PopupManagerCuong.Instance.HideSelectWingmanPopup();

        indexAircraftSaleUse = indexAircraftSale;
        switch (percentSale)
        {
            case 0:
                percentSaleObj.SetActive(false);
                //lPriceOrigi.gameObject.SetActive(false);
                productIapUse = AircraftSheet.Get(indexAircraftSaleUse).purchase_id;
                break;
            case 25:
                lNumPercentSale.text = "-25%";
                productIapUse = AircraftSheet.Get(indexAircraftSaleUse).purchase_sale25;
                break;
            case 50:
                lNumPercentSale.text = "-50%";
                productIapUse = AircraftSheet.Get(indexAircraftSaleUse).purchase_sale50;
                break;
            default:
                percentSaleObj.SetActive(false);
                productIapUse = AircraftSheet.Get(indexAircraftSaleUse).purchase_id;
                break;
        }

        if (!string.IsNullOrEmpty(AircraftSheet.Get(indexAircraftSaleUse).purchase_id) && percentSale != 0)
        {
            lPriceOrigi.gameObject.SetActive(true);
            lPriceOrigi.text = CuongUtils.GetIapLocalizedPrice(AircraftSheet.Get(indexAircraftSaleUse).purchase_id);
        }
        else
        {
            // vì có những con máy bay chưa bán bằng iap trong list máy bay nên ko có giá gốc của nó
            lPriceOrigi.gameObject.SetActive(false);
        }
        lPriceSale.text = CuongUtils.GetIapLocalizedPrice(productIapUse);

        lNameAircraft.text = ScriptLocalization.des_name_aircraft_sale.Replace("%{aircraft_name}", "[FFFF00FF]" + HangarValue.AircraftNames((AircraftTypeEnum)indexAircraftSale) + "[-]");
        lLevelAircraft.text = ScriptLocalization.des_level_aircraft_sale.Replace("%{num_level}", "[FFFF00FF]" + 60 + "[-]");

        aircraftAnimationsScript.PlayAnimations((AircraftTypeEnum)indexAircraftSale, CacheGame.GetSpaceShipRank((AircraftTypeEnum)indexAircraftSale));

        Debug.LogError("productIapUse: " + productIapUse);

        if (IAPCallbackManager.Instance.GetPointByProductName.ContainsKey(productIapUse) && IAPCallbackManager.Instance.GetPointByProductName[productIapUse] != null)
        {
            lNumVipPoint.gameObject.SetActive(true);
            lNumVipPoint.text = "+" + IAPCallbackManager.Instance.GetPointByProductName[productIapUse];
        }
        else
        {
            lNumVipPoint.gameObject.SetActive(false);
        }
    }

    public void Btn_Buy_InApp()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(productIapUse, IAPCallbackManager.WherePurchase.push_server);
    }

    public void Btn_Back()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.HidePopupSaleAircraft();
    }

    void OnPurchaseSuccessed(string productName)
    {
        if (productName == productIapUse)
        {
            CacheGame.SetSpaceShipIsUnlocked(indexAircraftSaleUse, 1);
            CacheGame.SetSpaceShipLevel((AircraftTypeEnum)indexAircraftSaleUse, 60);

            btnBuyObj.SetActive(false);

            PopupManagerCuong.Instance.ShowTextNotifiToast(HangarValue.GetUnlockDeviceNotify(HangarValue.AircraftNames((AircraftTypeEnum)indexAircraftSaleUse)), true, 1.88f, 0.56f);
        }
    }
}
