﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using TCore;
using I2.Loc;

public class ItemDailyLogin : MonoBehaviour
{

    public int indexDayGetData;
    public UILabel lDay;
    //
    public UISprite sIconGift;
    public UILabel lnumGift;

    public UISprite sBg;
    public GameObject sGlowBg;

    public GameObject sGlowInButton;
    public UILabel lClaim;

    [DisplayAsString]
    public string typeGift;

    [DisplayAsString]
    public int numGift;

    void Start()
    {
        //		ConfigLoader.Load ();
        SetDataStart();
    }


    public void SetDataStart()
    {
        lDay.text = ScriptLocalization.day_dailyLogin.Replace("%{number_day}", " " + indexDayGetData);
        GetBgToday();
        GetDataIconGift();
        GetDataBtnClaim();
    }


    void GetBgToday()
    {
        if (CacheGame.GetTotalDayLogin() == indexDayGetData)
        {
            sGlowBg.SetActive(true);
        }
    }



    void GetDataIconGift()
    {

        DailyLoginConfig dailyLoginConfig = DailyLoginConfig.Get("Day" + indexDayGetData);


        typeGift = dailyLoginConfig.typeGiftList;
        numGift = (int)dailyLoginConfig.numGiftList;

        //
        lnumGift.text = "" + numGift;
        switch (typeGift)
        {
            case "Gold":
                sIconGift.spriteName = "icon_coin";
                break;
            case "PowerUp":
                sIconGift.spriteName = "icon_dailyquest_powerup";
                break;
            case "EMP":
                sIconGift.spriteName = "icon_dailyquest_time";
                break;
            case "Life":
                sIconGift.spriteName = "icon_life";
                break;
            case "Gem":
                sIconGift.spriteName = "icon_daily_quest_gem";
                break;
        }
    }


    void GetDataBtnClaim()
    {
        switch (CacheGame.GetStatusClaimDailyLogin(indexDayGetData))
        {
            case 1:
                //----1 = Chua duoc nhan qua`

                sGlowInButton.SetActive(false);
                lClaim.text = ScriptLocalization.wait;


                break;
            case 2:
                //----2 = San~ sang` nhan qua`
                sGlowInButton.SetActive(true);
                lClaim.text = ScriptLocalization.claim;
                break;
            case 3:
                //----3 = da~ nhan qua` xong

                sBg.spriteName = "dailygift_BG1";

                sGlowInButton.SetActive(false);
                sGlowBg.SetActive(false);
                lClaim.text = ScriptLocalization.done;

                break;
        }
    }




    //


    public void BtnClaim()
    {
        if (PopupManagerCuong.Instance.readyShowNotifi)
        {
            SoundManager.PlayClickButton();
            switch (CacheGame.GetStatusClaimDailyLogin(indexDayGetData))
            {
                case 1:
                    //----2 = Chua duoc nhan qua`
                    PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.notifi_need_more_loginday, false);
                    break;
                case 2:
                    //----2 = San~ sang` nhan qua`
                    AddGiftToPlayer(typeGift, numGift);

                    PopupManagerCuong.Instance.ShowItemRewardPopup();
                    PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
                    popupItemRewardIns.ShowListItem(1);
                    popupItemRewardIns.ShowOneItem(1, typeItemPopupItemReward, numItemPopupItemReward);

                    CacheGame.SetStatusClaimDailyLogin(indexDayGetData, 3);
                    GetDataBtnClaim();
                    PopupDailyLogin.Instance.CheckClaimedAllGift();
                    break;
                case 3:
                    //----3 = da~ nhan qua` xong
                    PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.notifi_claimed_reward, false);
                    break;
            }
        }
    }

    GameContext.TypeItemInPopupItemReward typeItemPopupItemReward;
    int numItemPopupItemReward;
    void AddGiftToPlayer(string giftType, int numGift)
    {
        numItemPopupItemReward = numGift;
        switch (giftType)
        {
            case "Gold":
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddCoinTop(numGift, FirebaseLogSpaceWar.DailyLogin_why);
                }
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Gold;
                break;
            case "PowerUp":
                CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + numGift);
                SoundManager.SoundItemShow();
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.PowerUp;
                break;
            case "EMP":
                CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + numGift);
                SoundManager.SoundItemShow();
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.EMP;
                break;
            case "Life":
                CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + numGift);
                SoundManager.SoundItemShow();
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Life;
                break;
            case "Gem":
                //DontDestroyManager.Instance.AddGemTop(numGift);
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddGemTop(numGift, FirebaseLogSpaceWar.DailyLogin_why);
                }
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Gem;
                break;
        }
        //PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_HaveReceived + " +" + numGift + " " + giftType, true);
    }

}
