﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TCore;
using SkyGameKit;

public class PopupDailyLogin : SkyGameKit.Singleton<PopupDailyLogin>
{
    public List<ItemDailyLogin> listItemDailyLogin;


    public void SetDataStartPopup()
    {
        SetNewDataLogin();
        ShowDataInAllDay();

        //CacheGame.SetTotalDayLogin(7);
        //CacheGame.SetStatusClaimDailyLogin(1, 2);
        //CacheGame.SetStatusClaimDailyLogin(2, 2);
        //CacheGame.SetStatusClaimDailyLogin(3, 2);
        //CacheGame.SetStatusClaimDailyLogin(4, 2);
        //CacheGame.SetStatusClaimDailyLogin(5, 2);

    }


    void SetNewDataLogin()
    {
        //		Debug.LogError (System.DateTime.Now.Day);
        if (CacheGame.GetLastDayLogin() != CachePvp.dateTime.Day && CacheGame.GetTotalDayLogin() < 8)
        {
            CacheGame.SetLastDayLogin(CachePvp.dateTime.Day);
            CacheGame.SetTotalDayLogin(CacheGame.GetTotalDayLogin() + 1);
            CacheGame.SetStatusClaimDailyLogin(CacheGame.GetTotalDayLogin(), 2);
        }

    }


    void ShowDataInAllDay()
    {
        for (int i = 0; i < listItemDailyLogin.Count; i++)
        {
            listItemDailyLogin[i].SetDataStart();
        }
    }

    public void CheckClaimedAllGift()
    {
        bool claimedAllGift = true;
        for (int i = 0; i < listItemDailyLogin.Count; i++)
        {
            if (CacheGame.GetStatusClaimDailyLogin(listItemDailyLogin[i].indexDayGetData) != 3)
            {
                claimedAllGift = false;
                break;
            }
        }

        //		Debug.LogError (claimedAllGift);
        if (claimedAllGift)
        {
            CacheGame.SetClaimedAllGiftDailyLogin(1);
        }
    }




    public void Btn_Back()
    {
        SoundManager.PlayClickButton();
        if (CacheGame.GetClaimedAllGiftDailyLogin() == 1)
        {
            HomeScene.Instance.dailyLoginBtn.SetActive(false);
            HomeScene.Instance.dailyLoginBtn.GetComponent<ButtonTopRightHome>().canShow = false;
        }
        HomeScene.Instance.popupDailyLoginObj.SetActive(false);

       
    }
}
