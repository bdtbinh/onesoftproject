﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupConvertNewSystem : MonoBehaviour
{

    [SerializeField]
    private UILabel lGoldNumber;

    [SerializeField]
    private UILabel lGemNumber;

    [SerializeField]
    private GameObject popupConvert;

    int numCoin;
    int numGem;

    int numCoinPower;
    int numGemPower;

    private void Awake()
    {



    }


    void BonusGemPower(int aircraftPowerLevel)
    {
        if (aircraftPowerLevel < 10)
        {
            numCoinPower += 35000;
            numGemPower += 100;
        }
        else if (aircraftPowerLevel < 20)
        {
            numCoinPower += 100000;
            numGemPower += 250;
        }
        else if (aircraftPowerLevel < 30)
        {
            numCoinPower += 350000;
            numGemPower += 450;
        }
        else if (aircraftPowerLevel < 40)
        {
            numCoinPower += 850000;
            numGemPower += 1000;
        }
        else
        {
            numCoinPower += 850000;
            numGemPower += 1500;
        }
        Debug.LogError("numCoinPower:" + numCoinPower + "---numGemPower:" + numGemPower);
    }



    public void ShowPopup()
    {
        Convert();
        lGoldNumber.text = GameContext.FormatNumber(numCoin);
        lGemNumber.text = GameContext.FormatNumber(numGem);
        popupConvert.SetActive(true);
    }

    public void OnClickBtnConfirm()
    {
        MinhCacheGame.SetAlreadyDataConverted1();
        PanelCoinGem.Instance.AddCoinTop(numCoin);
        PanelCoinGem.Instance.AddGemTop(numGem);
        popupConvert.SetActive(false);
    }

    void Convert()
    {
        GetDataConvertShop(GameContext.MAIN_WEAPON, GameContext.SPEED);
        GetDataConvertShop(GameContext.LIFE, GameContext.QUANTITY);
        GetDataConvertShop(GameContext.MISSILE, GameContext.POWER);
        GetDataConvertShop(GameContext.WINDSLASH, GameContext.POWER);
        GetDataConvertShop(GameContext.GOLD, GameContext.DROP_CHANCE);
        GetDataConvertShop(GameContext.SUPER_WEAPON, GameContext.DURATION);
        GetDataConvertShop(GameContext.LASER, GameContext.POWER);
        GetDataConvertShop(GameContext.SECONDARY_WEAPON, GameContext.POWER);
        //
        GetDataConvertUltimateWeapon();
    }


    void GetDataConvertShop(string nameItem, string typeUpgrade)
    {
        int indexLevelUpgradeOld = CacheGame.GetCurrentLvShopUpgrade(nameItem, typeUpgrade);
        ShopUpgradeConfig dataConfig = ShopUpgradeConfig.Get(nameItem + typeUpgrade + GameContext.PRICE);
        if (indexLevelUpgradeOld >= dataConfig.list_PriceValue.Length) indexLevelUpgradeOld = dataConfig.list_PriceValue.Length - 1;
        for (int i = 0; i <= indexLevelUpgradeOld; i++)
        {
            numCoin += (int)dataConfig.list_PriceValue[i];
        }
        CacheGame.SetCurrentLvShopUpgrade(nameItem, typeUpgrade, 0);
    }

    //1 Burst shot
    //2 Chainsaw
    //3 utra
    //4 LIGHTING
    void GetDataConvertUltimateWeapon()
    {
        if (CacheGame.GetSecondaryWeaponIsUnlocked(1) == 1)
        {
            numCoin += 20000;
            CacheGame.SetSecondaryWeaponIsUnlocked(1, 0);
        }
        if (CacheGame.GetSecondaryWeaponIsUnlocked(2) == 1)
        {
            numCoin += 10000;
            numGem += 800;
            CacheGame.SetSecondaryWeaponIsUnlocked(2, 0);
        }
        if (CacheGame.GetSecondaryWeaponIsUnlocked(3) == 1)
        {
            numCoin += 30000;
            CacheGame.SetSecondaryWeaponIsUnlocked(3, 0);
        }
        if (CacheGame.GetSecondaryWeaponIsUnlocked(4) == 1)
        {
            numCoin += 15000;
            numGem += 1200;
            CacheGame.SetSecondaryWeaponIsUnlocked(4, 0);
        }
    }

    public bool NeedShowPopup()
    {
        if (MinhCacheGame.IsAlreadyDataConverted1())
            return false;

        if (CacheGame.GetCurrentLvShopUpgrade(GameContext.MAIN_WEAPON, GameContext.SPEED) > 0) return true;
        if (CacheGame.GetCurrentLvShopUpgrade(GameContext.LIFE, GameContext.QUANTITY) > 0) return true;
        if (CacheGame.GetCurrentLvShopUpgrade(GameContext.MISSILE, GameContext.POWER) > 0) return true;
        if (CacheGame.GetCurrentLvShopUpgrade(GameContext.WINDSLASH, GameContext.POWER) > 0) return true;
        if (CacheGame.GetCurrentLvShopUpgrade(GameContext.GOLD, GameContext.DROP_CHANCE) > 0) return true;
        if (CacheGame.GetCurrentLvShopUpgrade(GameContext.SUPER_WEAPON, GameContext.DURATION) > 0) return true;
        if (CacheGame.GetCurrentLvShopUpgrade(GameContext.LASER, GameContext.POWER) > 0) return true;
        if (CacheGame.GetCurrentLvShopUpgrade(GameContext.SECONDARY_WEAPON, GameContext.POWER) > 0) return true;

        if (CacheGame.GetSecondaryWeaponIsUnlocked(1) == 1)
        {
            return true;
        }
        if (CacheGame.GetSecondaryWeaponIsUnlocked(2) == 1)
        {
            return true;
        }
        if (CacheGame.GetSecondaryWeaponIsUnlocked(3) == 1)
        {
            return true;
        }
        if (CacheGame.GetSecondaryWeaponIsUnlocked(4) == 1)
        {
            return true;
        }

        return false;
    }
}
