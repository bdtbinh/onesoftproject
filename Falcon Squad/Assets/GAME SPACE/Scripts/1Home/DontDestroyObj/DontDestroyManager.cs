﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TCore;
using SkyGameKit;
using DG.Tweening;
using Firebase.Analytics;

public class DontDestroyManager : Singleton<DontDestroyManager>
{

    //public Transform tranformTop;
    //
    //public GameObject panelCoinGemTop;
    //public UILabel lNumCoinTop;
    //public UILabel lNumGemTop;
    //public GameObject glowPackX2;

    //
    //public GameObject panelVideoTopBtn;
    //
    //public GameObject btnAchi_Quest_Obj;
    //
    //public GameObject btnStarterPack;
    //public GameObject btnPremiumPack;
    //public GameObject btnVipPack;
    //
    //public GameObject popupSetting_Obj;
    //public PopupSetting popupSetting_Script;
    //
    //public GameObject popupGiftCode_Obj;
    //
    //public GameObject popupStarterPack;
    //public GameObject popupPremiumPack;
    //public GameObject popupVipPack;

    //public GameObject popupSelectLevel;
    //
    //public PopupRate popupRate_Script;
    //
    //public GameObject popupShop_Obj;
    //public PopupShop popupShop_Script;
    //
    //public GameObject objNotifiButton;

    //[HideInInspector]
    //public bool readyShowNotifi = true;
    //


    void Awake()
    {
        base.Awake();
        //ConfigLoader.Load();
    }

    void Start()
    {
        ShowButtonStartScene();
    }

    //-----------------------------------------
    void ShowButtonStartScene()
    {
        if (SceneManager.GetActiveScene().name != "Home" && SceneManager.GetActiveScene().name != "SelectLevel" && SceneManager.GetActiveScene().name != "PVPMain")
        {
            //panelCoinGemTop.SetActive(false);
            //panelVideoTopBtn.SetActive(false);
            //btnAchi_Quest_Obj.SetActive(false);
            //btnVipPack.SetActive(false);
            //btnStarterPack.SetActive(false);
            //btnPremiumPack.SetActive(false);
            //
        }
        else if (SceneManager.GetActiveScene().name == "PVPMain")
        {
            //panelCoinGemTop.SetActive(true);
            //if (CacheGame.GetFirstInappBuyGold() != 1)
            //{
            //    glowPackX2.SetActive(true);
            //}
            //else
            //{
            //    glowPackX2.SetActive(false);
            //}
            //
            //AddCoinTop();
            //AddGemTop();
            //btnVipPack.SetActive(false);
            //btnStarterPack.SetActive(false);
            //btnPremiumPack.SetActive(false);
        }
        else
        {

            //panelCoinGemTop.SetActive(true);
            //if (CacheGame.GetFirstInappBuyGold() != 1)
            //{
            //    glowPackX2.SetActive(true);
            //}
            //else
            //{
            //    glowPackX2.SetActive(false);
            //}
            //
            //AddCoinTop();
            //AddGemTop();



                //
                //btnAchi_Quest_Obj.SetActive(true);
                //
                //if (CacheGame.GetMaxLevel3Difficult() > 2)
                //{
                //    panelVideoTopBtn.SetActive(true);
                //}

                ////--------btn VipPack

                //if (CacheGame.GetPurchasedVIPPack() == 1)
                //{
                //    btnVipPack.SetActive(false);
                //}
                //else
                //{
                //    btnVipPack.SetActive(true);
                //}

                ////--------btn StarterPack
                //if (CacheGame.GetPurchasedStarterPack() == 1)
                //{
                //    btnStarterPack.SetActive(false);
                //}
                //else
                //{
                //    btnStarterPack.SetActive(true);
                //}
        }

    }
    //-----------------------------------------


    float timeDurationAddCoin;

    public void AddCoinTop(int numCoinAdd = 0)
    {

        if (numCoinAdd > 10)
        {
            SoundManager.SoundGetCoinBig();
        }
        else if (numCoinAdd <= 10 && numCoinAdd != 0)
        {
            SoundManager.SoundGetCoinSmall();
        }


        if (numCoinAdd > 0)
        {
            timeDurationAddCoin = 0.003f * numCoinAdd;
        }
        else if (numCoinAdd < 0)
        {
            timeDurationAddCoin = -0.003f * numCoinAdd;
        }
        if (timeDurationAddCoin > 3f)
        {
            timeDurationAddCoin = 3f;
        }
        //----
        if (numCoinAdd != 0)
        {
            LeanTween.delayedCall(0.5f, () =>
            {
                //PopupShop.Instance.SetShowGlowBtnShop();
            }).setIgnoreTimeScale(true);
        }

        //lNumCoinTop.DOUILabelInt(CacheGame.GetTotalCoin(), (CacheGame.GetTotalCoin() + numCoinAdd), timeDurationAddCoin).SetUpdate(true);
        //CacheGame.SetTotalCoin(CacheGame.GetTotalCoin() + numCoinAdd);
    }

    //-----------------------------------------------------
    float timeDurationAddGem;
    public void AddGemTop(int numGemAdd = 0)
    {

        if (numGemAdd != 0)
        {
            SoundManager.SoundGetCoinSmall();
        }


        if (numGemAdd > 0)
        {
            timeDurationAddGem = 0.003f * numGemAdd;
        }
        else if (numGemAdd < 0)
        {
            timeDurationAddGem = -0.003f * numGemAdd;
        }
        if (timeDurationAddGem > 3f)
        {
            timeDurationAddGem = 3f;
        }

        //lNumGemTop.DOUILabelInt(CacheGame.GetTotalGem(), (CacheGame.GetTotalGem() + numGemAdd), timeDurationAddGem).SetUpdate(true);
        //CacheGame.SetTotalGem(CacheGame.GetTotalGem() + numGemAdd);
    }

    //-----------------------------------------


    //public void ShowTextNotifiToast(string textNotifi, bool clickSuccess, float timeShow = 1.31f, float timeDelay = 0.1f)
    //{
    //    if (readyShowNotifi)
    //    {
    //        readyShowNotifi = false;
    //        LeanTween.delayedCall(timeDelay, () =>
    //        {
    //            LeanTween.delayedCall(0.36f, () =>
    //            {
    //                readyShowNotifi = true;
    //            }).setIgnoreTimeScale(true);

    //            GameObject objNotifiIns = Instantiate(objNotifiButton, transform);
    //            objNotifiIns.GetComponent<NotificationText>().ShowTextNotifiClickButton(textNotifi, clickSuccess, timeShow);
    //        }).setIgnoreTimeScale(true);
    //    }

    //}

    //-----------------------------------------
    public void Btn_IapCoinTop()
    {
        SoundManager.PlayShowPopup();
        //popupShop_Obj.SetActive(true);

        //popupSetting_Obj.SetActive(false);
        //popupStarterPack.SetActive(false);

        //popupSelectAircraft.gameObject.SetActive(false);
        //popupSelectWeapon.gameObject.SetActive(false);
        //popupSelectWingman.gameObject.SetActive(false);
        //popupSelectLevel.SetActive(false);
        //Quest_Achi_Manager.Instance.Quest_Achi_InviObj.SetActive(false);
        //popupVipPack.SetActive(false);
        //popupPremiumPack.SetActive(false);
        //popupGiftCode_Obj.SetActive(false);
        //if (SceneManager.GetActiveScene().name == "Home")
        //{
        //    HomeScene.Instance.popupExit.SetActive(false);
        //    HomeScene.Instance.popupDailyLoginObj.SetActive(false);
        //    HomeScene.Instance.popupLuckyWheel.SetActive(false);
        //    HomeScene.Instance.popupCommunity.SetActive(false);

        //}


        //if (MSLManager.Instance != null)
        //{
        //    MSLManager.Instance.PopupUnlockLevel.gameObject.SetActive(false);
        //}

        //popupShop_Script.ShowPanelInShop(TypePanelShowInShop.Iap);
    }


    //public void ButtonStarterPack()
    //{
    //    if (GameContex.readyClickShowPopup)
    //    {
    //        SoundManager.PlayShowPopup();
    //        //
    //        popupStarterPack.SetActive(true);
    //        FirebaseAnalytics.LogEvent("Click", new Parameter("Button", "StarterPack"));
    //    }


    //}

    //public void ButtonVipPack()
    //{
    //    if (GameContex.readyClickShowPopup)
    //    {
    //        SoundManager.PlayShowPopup();
    //        //
    //        popupVipPack.SetActive(true);
    //        FirebaseAnalytics.LogEvent("Click", new Parameter("Button", "VipPack"));
    //    }


    //}




}
