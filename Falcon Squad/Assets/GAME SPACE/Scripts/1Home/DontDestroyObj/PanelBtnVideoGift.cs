﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using SgLib;
using System;
using com.ootii.Messages;
using System.Collections.Generic;
using TCore;
using SkyGameKit;
using DG.Tweening;

public class PanelBtnVideoGift : MonoBehaviour
{
    [Header("Daily Reward Config")]
    [Tooltip("Number of hours between 2 rewards")]
    public int rewardIntervalHours = 6;
    [Tooltip("Number of minues between 2 rewards")]
    public int rewardIntervalMinutes = 0;
    [Tooltip("Number of seconds between 2 rewards")]
    public int rewardIntervalSeconds = 0;
    public GameObject sBgTime;

    public UILabel lTimeToView;
    public BoxCollider boxBtnVideo;
    public GameObject fxBtnVideo;
    public int numUseHourlyRewardMax = 5;
    //
    int numCoinGet;

    private void OnEnable()
    {
        rewardIntervalMinutes = CachePvp.sCClientConfig.videoTopCountDown;
        numUseHourlyRewardMax = CachePvp.sCClientConfig.videoTopMaxShownADay;

        MessageDispatcher.AddListener(EventName.WatchVideo.WatchVideo.ToString(), OnWatchVideo, true);
    }

    private void OnWatchVideo(IMessage rMessage)
    {
        Btn_VideoGiftTop();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.WatchVideo.WatchVideo.ToString(), OnWatchVideo, true);
    }

    void Start()
    {
        StartCoroutine(CoUpdateEverySecond());
    }

    IEnumerator CoUpdateEverySecond()
    {
        while (true)
        {
            UpdateEverySecond();
            yield return new WaitForSecondsRealtime(1f);
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    bool CanRewardNow()
    {
        return MinhCacheGame.GetNextTimeReward().Subtract(DateTime.Now) <= TimeSpan.Zero;
    }

    public void ResetNextRewardTime()
    {
        DateTime next = DateTime.Now.Add(new TimeSpan(rewardIntervalHours, rewardIntervalMinutes, rewardIntervalSeconds));
        MinhCacheGame.SetNextTimeReward(next);
    }

    void UpdateEverySecond()
    {
        if (CacheGame.GetNumUseHourlyRewardOneDay() < numUseHourlyRewardMax)
        {
            if (CanRewardNow())
            {
                if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
                {
                    fxBtnVideo.SetActive(true);
                    sBgTime.SetActive(false);
                    //lTimeToView.text = "Free";
                }
                else
                {
                    fxBtnVideo.SetActive(true);
                    sBgTime.SetActive(false);
                    //lTimeToView.text = "video";
                }
                lTimeToView.text = "";
            }
            else
            {
                fxBtnVideo.SetActive(false);
                sBgTime.SetActive(true);
                TimeSpan timeToReward = MinhCacheGame.GetNextTimeReward().Subtract(DateTime.Now);
                lTimeToView.text = string.Format("{0:00}:{1:00}", timeToReward.Minutes, timeToReward.Seconds);
            }
        }
        else
        {
            fxBtnVideo.SetActive(false);
            sBgTime.SetActive(true);
            lTimeToView.text = " n/a";
        }
        if (PopupManager.Instance.IsVideoRewardPopupActive())
        {
            PopupVideoReward.Instance.RefreshTime(lTimeToView.text);
        }
    }

    public void bVideoTop()
    {
        if (!PopupManager.Instance.IsVideoRewardPopupActive())
        {
            PopupManager.Instance.ShowVideoRewardPopup();
        }
        FirebaseLogSpaceWar.LogClickButton("VideoTop");

    }



    public void Btn_VideoGiftTop()
    {

        TCore.SoundManager.PlayClickButton();
        if (CacheGame.GetNumUseHourlyRewardOneDay() < numUseHourlyRewardMax)
        {
            if (CanRewardNow())
            {
                if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
                {
                    CallBackFinishVideoAds();
                }
                else
                {
                    FirebaseLogSpaceWar.LogOtherEvent("Ad_ClickAdmob");
                    FirebaseLogSpaceWar.LogOtherEvent("Ad_ClickHourlyReward");

                    if (OsAdsManager.Instance.isRewardedVideoAvailable())
                    {
                        OsAdsManager.Instance.AddRewardedListenner(CallBackFinishVideoAds);
                        OsAdsManager.Instance.AddClosedListenner(CallBackClosedVideoAds);
                        //
                        OsAdsManager.Instance.ShowRewardedAds(OsAdsManager.LocationClickVideo.btn_video_Top);
                        FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdmob");
                        FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdHourlyReward");
                    }
                    else
                    {
                        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_ads_not_available, false, 1.5f);
                    }
                }
            }
            else
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_videoads_wait, false, 1.6f);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_videoads_tomorrow, false, 1.6f);
        }


    }



    void CallBackFinishVideoAds()
    {


        if (!MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            FirebaseLogSpaceWar.LogOtherEvent("Ad_CompAdsHourlyReward");
        }
        //
        CacheGame.SetNumUseHourlyRewardOneDay(CacheGame.GetNumUseHourlyRewardOneDay() + 1);
        //
        //PopupShop.Instance.SetShowGlowBtnShop();


        LeanTween.delayedCall(0.6f, (Action)(() =>
        {
            /*
            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.AddCoinTop(numCoinGet);
            }
            PopupManagerCuong.Instance.ShowTextNotifiToast((string)(I2.Loc.ScriptLocalization.Notifi_HaveReceived + " +" + numCoinGet + "  Gold"), (bool)true, (float)1.8f);
            */
            ItemVolumeFalconMail item = CachePvp.GetRewardFromVideo();
            List<ItemVolumeFalconMail> list = new List<ItemVolumeFalconMail>();
            list.Add(item);
            PopupManager.Instance.ShowClaimPopup(list, FirebaseLogSpaceWar.VideoTop_why);
        })).setIgnoreTimeScale(true);


        if (CacheGame.GetNumUseHourlyRewardOneDay() < numUseHourlyRewardMax)
        {
            ResetNextRewardTime();
        }

    }
    void CallBackClosedVideoAds()
    {

    }


}
