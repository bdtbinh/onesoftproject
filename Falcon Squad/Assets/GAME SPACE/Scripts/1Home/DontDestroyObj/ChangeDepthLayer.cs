﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class ChangeDepthLayer : MonoBehaviour
{


    UIPanel[] listPanelChilds;

    SkeletonAnimation[] skeletonAnimations;

    public int depthAdd = 5000;

    private void OnEnable()
    {
        Debug.LogError("ChangeDepthLayer: " + gameObject.name + depthAdd);
        listPanelChilds = GetComponentsInChildren<UIPanel>(true);
        for (int i = 0; i < listPanelChilds.Length; i++)
        {
            listPanelChilds[i].depth += depthAdd;
            listPanelChilds[i].sortingOrder += depthAdd;
            //listPanelChilds[i].sortingLayerName = "ServerPush";
        }

        skeletonAnimations = GetComponentsInChildren<SkeletonAnimation>();

        if (skeletonAnimations != null)
        {
            for (int i = 0; i < skeletonAnimations.Length; i++)
            {
                skeletonAnimations[i].GetComponent<MeshRenderer>().sortingOrder += depthAdd;
                //skeletonAnimations[i].GetComponent<MeshRenderer>().sortingLayerName = "ServerPush";
            }
        }

        //Debug.LogError("listPanelChildsChangeDepth:" + listPanelChilds.Length);
    }


    public void OnDisable()
    {
        for (int i = 0; i < listPanelChilds.Length; i++)
        {
            listPanelChilds[i].depth -= depthAdd;
            listPanelChilds[i].sortingOrder -= depthAdd;
            //listPanelChilds[i].sortingLayerName = "Default";

        }

        if (skeletonAnimations != null)
        {
            for (int i = 0; i < skeletonAnimations.Length; i++)
            {
                skeletonAnimations[i].GetComponent<MeshRenderer>().sortingOrder -= depthAdd;
                //skeletonAnimations[i].GetComponent<MeshRenderer>().sortingLayerName = "Default";

            }
        }

        depthAdd = 5000;
        this.enabled = false;
    }
}
