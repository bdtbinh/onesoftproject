﻿using Firebase.Analytics;
using System.Collections;
using UnityEngine;
using TCore;
using SkyGameKit;
using System;

public class PanelBtnPack : Singleton<PanelBtnPack>
{

    public GameObject btn3Pack;
    public GameObject btnCardPack;
    public GameObject btnRoyalPack;

    public UILabel lCountdownBtnCardPack;

    [HideInInspector]
    public UILabel lCountdownPopupCardPack; // biến này sẽ gán ở Awake khi bật PopupCardPack , phẢi check # null mới đếm

    //public GameObject btnAircraftSale;

    void Start()
    {
        //#if !UNITY_ANDROID
        //        if (CacheGame.PopupCardPackIsSelling != 1 && CacheFireBase.GetIsReviewVersion == 1)
        //        {
        //            PopupManagerCuong.Instance.ShowCardPackPopup();
        //            PopupManagerCuong.Instance.SetDataCardPackPopup(5, 0);
        //            IAPCallbackManager.Instance.isServerPush = true;
        //            PopupManagerCuong.Instance.HideCardPackPopup();
        //        }
        //#endif
        if (CacheGame.PopupCardPackIsSelling == 1)
        {
            StartCoroutine(Countdown());
        }

        TestShowRoyalPack();

    }

    void TestShowRoyalPack()
    {
        //Mua royalpack rồi thì return luôn

        if (MinhCacheGame.IsAlreadyBuyRoyalPack())
            return;
        btnRoyalPack.SetActive(false);
        //btnRoyalPack.SetActive(CacheFireBase.GetTestShowRoyalPack == 1);
    }


    void OnEnable()
    {
        ShowButtonStart();
    }

    public void ShowButtonStart()
    {
        btnCardPack.SetActive(CacheGame.PopupCardPackIsSelling == 1);

        //#if !UNITY_ANDROID
        //        btnAircraftSale.SetActive(CacheFireBase.GetIsReviewVersion == 1);
        //#endif

    }

    public void StartCountdown()
    {
        StartCoroutine(Countdown());
    }

    IEnumerator Countdown()
    {
        DateTime endDate = MinhCacheGame.GetTimeFinishSaleCardPack();

        while (endDate > DateTime.Now)
        {
            TimeSpan interval = endDate - DateTime.Now;
            string str = string.Format("{0:00}:{1:00}:{2:00}", interval.Hours + interval.Days * 24, interval.Minutes, interval.Seconds);

            lCountdownBtnCardPack.text = str;

            if (lCountdownPopupCardPack != null)
            {
                lCountdownPopupCardPack.text = I2.Loc.ScriptLocalization.deal_end_in.Replace("%{time}", "[FFFF00FF]" + str + "[-]");
            }

            yield return new WaitForSeconds(1f);
        }

        PopupManagerCuong.Instance.HideCardPackPopup();
        btnCardPack.SetActive(false);
        CacheGame.PopupCardPackIsSelling = 0;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }


    public void Button3Pack()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();
            PopupManagerCuong.Instance.Show3PackPopup();
            Popup3Pack.Instance.ShowPackInPopup();
            //PopupManagerCuong.Instance.ChangeDepth3PackPopup();

            FirebaseLogSpaceWar.LogClickButton("3Pack");
        }
    }

    public void ButtonCardPack()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();

            PopupManagerCuong.Instance.ShowCardPackPopup();

            FirebaseLogSpaceWar.LogClickButton("CardPack");

        }
    }

    public void ButtonRoyalty()
    {
        if (GameContext.readyClickShowPopup)
        {
            PopupManagerCuong.Instance.ShowRoyaltyPopup();
        }
    }
}
