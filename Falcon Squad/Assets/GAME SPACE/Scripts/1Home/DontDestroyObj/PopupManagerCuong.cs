﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TCore;
using SkyGameKit;
using DG.Tweening;
using Firebase.Analytics;
using com.ootii.Messages;

public class PopupManagerCuong : Singleton<PopupManagerCuong>
{
    public GameObject popupItemReward;


    public GameObject panelCoinGem_Top;
    public GameObject panelBtnVideo_Top;
    public GameObject panelBtnQA_Top;
    public GameObject panelPack_Top;

    //public GameObject popupStarterPack;
    //public GameObject popupPremiumPack;
    public GameObject popupCardPack;
    public GameObject popup3Pack;


    public GameObject popupSelectAircraft;
    public GameObject popupSelectWingman;
    public GameObject popupSelectWing;

    public GameObject popupShop;
    public GameObject popupQA;

    public GameObject popupVideoEndgame;
    public GameObject popupExitGame;

    public GameObject popupSetting;
    public GameObject popupGiftCode;
    public GameObject popupRate;

    public GameObject popupSelectLevel;

    public GameObject popupQuestOpenIAP;
    public GameObject popupSaleGoldGem;
    public GameObject popupPushSaleInfinityPack;
    public GameObject popupPushSaleTicketLuckyWheel;
    public GameObject popupPushSaleAircraft;

    public GameObject popupCallingBackup;
    public GameObject popupItemInfo;

    public GameObject popupNoticeInTrial;

    public GameObject popupStarChest;
    public GameObject popupLoadingPurchase;

    public GameObject popupRoyalty;

    public GameObject popupEvent;
    public GameObject popupPromoteEvent;
    public GameObject popupBuyEventItem;
    public GameObject popupNotifiFinishEvent;


    [HideInInspector]
    public bool readyShowNotifi = true;
    public GameObject notifiToast;
    //
    public Dictionary<string, GameObject> popupsDictionary = new Dictionary<string, GameObject>();
    //
    public const string PANEL_COIN_GEM_TOP = "PANEL_COIN_GEM_TOP";
    public const string PANEL_BTN_VIDEO_TOP = "PANEL_BTN_VIDEO_TOP";
    public const string PANEL_BTN_QA_TOP = "PANEL_BTN_QA_TOP";
    public const string PANEL_BTN_PACK_TOP = "PANEL_BTN_PACK_TOP";

    public const string POPUP_STARTERPACK = "POPUP_STARTERPACK";
    public const string POPUP_PREMIUMPACK = "POPUP_PREMIUMPACK";
    public const string POPUP_VIPPACK = "POPUP_VIPPACK";
    public const string POPUP_CARD_PACK = "POPUP_CARD_PACK";

    public const string POPUP_3PACK = "POPUP_3PACK";


    public const string POPUP_SELECT_AIRCRAFT = "POPUP_SELECT_AIRCRAFT";
    public const string POPUP_SELECT_WINGMAN = "POPUP_SELECT_WINGMAN";
    public const string POPUP_SELECT_WING = "POPUP_SELECT_WING";


    public const string POPUP_SHOP = "POPUP_SHOP";
    public const string POPUP_QA = "POPUP_QA";

    public const string POPUP_SETTING = "POPUP_SETTING";
    public const string POPUP_GIFTCODE = "POPUP_GIFTCODE";
    public const string POPUP_RATE = "POPUP_RATE";

    public const string POPUP_SELECT_LEVEL = "POPUP_SELECT_LEVEL";

    public const string POPUP_QUEST_OPEN_IAP = "POPUP_QUEST_OPEN_IAP";
    public const string POPUP_VIDEO_ENDGAME = "POPUP_VIDEO_ENDGAME";
    public const string POPUP_EXIT_GAME = "POPUP_EXIT_GAME";

    public const string POPUP_SHOW_ITEM_REWARD = "POPUP_SHOW_ITEM_REWARD";
    public const string POPUP_SALE_GOLD_GEM = "POPUP_SALE_GOLD_GEM";
    public const string POPUP_PUSH_SALE_INFINITY_PACK = "POPUP_PUSH_SALE_INFINITY_PACK";
    public const string POPUP_PUSH_SALE_TICKET_LUCKYWHEEL = "POPUP_PUSH_SALE_TICKET_LUCKYWHEEL";
    public const string POPUP_PUSH_SALE_AIRCRAFT = "POPUP_PUSH_SALE_AIRCRAFT";
    public const string POPUP_CALLING_BACKUP = "POPUP_CALLING_BACKUP";
    public const string POPUP_ITEM_INFO = "POPUP_ITEM_INFO";
    public const string POPUP_NOTICE_IN_TRIAL = "POPUP_NOTICE_IN_TRIAL";
    public const string POPUP_STAR_CHEST = "POPUP_STAR_CHEST";

    public const string POPUP_LOADING_PURCHASE = "POPUP_LOADING_PURCHASE";
    public const string POPUP_ROYALTY = "POPUP_ROYALTY";

    public const string POPUP_EVENT = "POPUP_HALLOWEEN";
    public const string POPUP_PROMOTE_EVENT = "POPUP_UNLOCK_EVOLVE_REWARD";
    public const string POPUP_BUY_EVENT_ITEM = "POPUP_BUY_EVENT_ITEM";

    public const string POPUP_NOTIFI_FINISH_EVENT = "POPUP_NOTIFI_FINISH_EVENT";


    void Awake()
    {
        //ConfigLoader.Load();
        popupsDictionary.Clear();
    }

    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Home" || SceneManager.GetActiveScene().name == "SelectLevel")
        {
            ShowPanelCoinGem();
            if (CacheGame.GetMaxLevel3Difficult() > 2)
            {
                ShowPanelBtnPackTop();
                ShowPanelBtnVideoTop();
            }
            ShowShopPopup();
            ShowQAPopup();
        }
        else if (SceneManager.GetActiveScene().name == "PVPMain")
        {
            ShowPanelCoinGem();
            ShowPanelBtnVideoTop();
            ShowShopPopup();
        }
        else if (SceneManager.GetActiveScene().name == "PVPEndGame")
        {
            ShowPanelCoinGem();
            ShowPanelBtnVideoTop();
            ShowShopPopup();
        }
    }


    public void ShowPopupLoadingPurchase()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_LOADING_PURCHASE, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupLoadingPurchase);
            popupsDictionary.Remove(POPUP_LOADING_PURCHASE);
            popupsDictionary.Add(POPUP_LOADING_PURCHASE, objShow);
        }
        objShow.SetActive(true);
    }

    public void HidePopupLoadingPurchase()
    {
        if (popupsDictionary.ContainsKey(POPUP_LOADING_PURCHASE) && popupsDictionary[POPUP_LOADING_PURCHASE] != null)
        {
            popupsDictionary[POPUP_LOADING_PURCHASE].SetActive(false);
        }
    }

    public bool IsActivePopupLoadingPurchase()
    {
        if (popupsDictionary.ContainsKey(POPUP_LOADING_PURCHASE) && popupsDictionary[POPUP_LOADING_PURCHASE] != null)
        {
            return popupsDictionary[POPUP_LOADING_PURCHASE].activeInHierarchy;
        }
        return false;
    }

    //----Sale gold gem

    public void ShowPopupSaleGoldGem(string typeShow, int index)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_SALE_GOLD_GEM, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupSaleGoldGem);
            popupsDictionary.Remove(POPUP_SALE_GOLD_GEM);
            popupsDictionary.Add(POPUP_SALE_GOLD_GEM, objShow);
        }
        objShow.SetActive(true);
        objShow.GetComponent<PopupPushSaleGoldGem>().ShowDataStart(typeShow, index);
    }

    public void HidePopupSaleGoldGem()
    {
        if (popupsDictionary.ContainsKey(POPUP_SALE_GOLD_GEM) && popupsDictionary[POPUP_SALE_GOLD_GEM] != null)
        {
            popupsDictionary[POPUP_SALE_GOLD_GEM].SetActive(false);
        }
    }

    public bool IsActivePopupSaleGoldGem()
    {
        if (popupsDictionary.ContainsKey(POPUP_SALE_GOLD_GEM) && popupsDictionary[POPUP_SALE_GOLD_GEM] != null)
        {
            return popupsDictionary[POPUP_SALE_GOLD_GEM].activeInHierarchy;
        }
        return false;
    }

    //  sale mÁy bay
    public void ShowPopupSaleAircraft(int percentSale, int indexAircraft)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_PUSH_SALE_AIRCRAFT, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupPushSaleAircraft);
            popupsDictionary.Remove(POPUP_PUSH_SALE_AIRCRAFT);
            popupsDictionary.Add(POPUP_PUSH_SALE_AIRCRAFT, objShow);
        }
        objShow.SetActive(true);
        objShow.GetComponent<PopupPushAircraft>().SetDataStart(percentSale, indexAircraft);
    }

    public void HidePopupSaleAircraft()
    {
        if (popupsDictionary.ContainsKey(POPUP_PUSH_SALE_AIRCRAFT) && popupsDictionary[POPUP_PUSH_SALE_AIRCRAFT] != null)
        {
            popupsDictionary[POPUP_PUSH_SALE_AIRCRAFT].SetActive(false);
        }
    }

    public bool IsActivePopupSaleAircraft()
    {
        if (popupsDictionary.ContainsKey(POPUP_PUSH_SALE_AIRCRAFT) && popupsDictionary[POPUP_PUSH_SALE_AIRCRAFT] != null)
        {
            return popupsDictionary[POPUP_PUSH_SALE_AIRCRAFT].activeInHierarchy;
        }
        return false;
    }

    //----Push Infinity Pack

    public void ShowPopupPushInfinityPack()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_PUSH_SALE_INFINITY_PACK, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupPushSaleInfinityPack);
            popupsDictionary.Remove(POPUP_PUSH_SALE_INFINITY_PACK);
            popupsDictionary.Add(POPUP_PUSH_SALE_INFINITY_PACK, objShow);
        }
        objShow.SetActive(true);
        //objShow.GetComponent<PopupPushSaleGoldGem>().ShowDataStart(typeShow, index);
    }

    public void HidePopupPushInfinityPack()
    {
        if (popupsDictionary.ContainsKey(POPUP_PUSH_SALE_INFINITY_PACK) && popupsDictionary[POPUP_PUSH_SALE_INFINITY_PACK] != null)
        {
            popupsDictionary[POPUP_PUSH_SALE_INFINITY_PACK].SetActive(false);
        }
    }

    public bool IsActivePopupPushInfinityPack()
    {
        if (popupsDictionary.ContainsKey(POPUP_PUSH_SALE_INFINITY_PACK) && popupsDictionary[POPUP_PUSH_SALE_INFINITY_PACK] != null)
        {
            return popupsDictionary[POPUP_PUSH_SALE_INFINITY_PACK].activeInHierarchy;
        }
        return false;
    }

    //----Push TICKET LUCKY WHEEL Pack

    public void ShowPopupPushTicketLuckyPack()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_PUSH_SALE_TICKET_LUCKYWHEEL, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupPushSaleTicketLuckyWheel);
            popupsDictionary.Remove(POPUP_PUSH_SALE_TICKET_LUCKYWHEEL);
            popupsDictionary.Add(POPUP_PUSH_SALE_TICKET_LUCKYWHEEL, objShow);
        }
        objShow.SetActive(true);
        //objShow.GetComponent<PopupPushSaleGoldGem>().ShowDataStart(typeShow, index);
    }

    public void HidePopupPushTicketLuckyPack()
    {
        if (popupsDictionary.ContainsKey(POPUP_PUSH_SALE_TICKET_LUCKYWHEEL) && popupsDictionary[POPUP_PUSH_SALE_TICKET_LUCKYWHEEL] != null)
        {
            popupsDictionary[POPUP_PUSH_SALE_TICKET_LUCKYWHEEL].SetActive(false);
        }
    }

    public bool IsActivePopupPushTicketLuckyPack()
    {
        if (popupsDictionary.ContainsKey(POPUP_PUSH_SALE_TICKET_LUCKYWHEEL) && popupsDictionary[POPUP_PUSH_SALE_TICKET_LUCKYWHEEL] != null)
        {
            return popupsDictionary[POPUP_PUSH_SALE_TICKET_LUCKYWHEEL].activeInHierarchy;
        }
        return false;
    }


    public void ShowTextNotifiToast(string textNotifi, bool clickSuccess, float timeShow = 1.31f, float timeDelay = 0.1f)
    {
        if (readyShowNotifi)
        {
            readyShowNotifi = false;
            this.Delay(timeDelay, () =>
            {
                this.Delay(0.36f, () =>
                {
                    readyShowNotifi = true;
                }, true);
                GameObject objNotifiIns = Instantiate(notifiToast, transform);
                objNotifiIns.GetComponent<NotificationText>().ShowTextNotifiClickButton(textNotifi, clickSuccess, timeShow);
            }, true);

        }

    }
    //----Coin gem
    public void ShowPanelCoinGem()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(PANEL_COIN_GEM_TOP, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, panelCoinGem_Top);
            popupsDictionary.Remove(PANEL_COIN_GEM_TOP);
            popupsDictionary.Add(PANEL_COIN_GEM_TOP, objShow);
        }
        objShow.SetActive(true);
        //Debug.LogError("ShowPanelCoinGem");
    }

    public void HidePanelCoinGem()
    {
        if (popupsDictionary.ContainsKey(PANEL_COIN_GEM_TOP) && popupsDictionary[PANEL_COIN_GEM_TOP] != null)
        {
            popupsDictionary[PANEL_COIN_GEM_TOP].SetActive(false);
        }
    }

    public bool IsActivePanelCoinGem()
    {
        if (popupsDictionary.ContainsKey(PANEL_COIN_GEM_TOP) && popupsDictionary[PANEL_COIN_GEM_TOP] != null)
        {
            return popupsDictionary[PANEL_COIN_GEM_TOP].activeInHierarchy;
        }
        return false;
    }


    //----BtnVideo_Top
    public void ShowPanelBtnVideoTop()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(PANEL_BTN_VIDEO_TOP, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, panelBtnVideo_Top);
            popupsDictionary.Remove(PANEL_BTN_VIDEO_TOP);
            popupsDictionary.Add(PANEL_BTN_VIDEO_TOP, objShow);
        }
        objShow.SetActive(true);
    }

    public void HidePanelBtnVideoTop()
    {
        if (popupsDictionary.ContainsKey(PANEL_BTN_VIDEO_TOP) && popupsDictionary[PANEL_BTN_VIDEO_TOP] != null)
        {
            popupsDictionary[PANEL_BTN_VIDEO_TOP].SetActive(false);
        }
    }

    public bool IsActivePanelBtnVideoTop()
    {
        if (popupsDictionary.ContainsKey(PANEL_BTN_VIDEO_TOP) && popupsDictionary[PANEL_BTN_VIDEO_TOP] != null)
        {
            return popupsDictionary[PANEL_BTN_VIDEO_TOP].activeInHierarchy;
        }
        return false;
    }



    //---- Pack_Top
    public void ShowPanelBtnPackTop()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(PANEL_BTN_PACK_TOP, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, panelPack_Top);
            popupsDictionary.Remove(PANEL_BTN_PACK_TOP);
            popupsDictionary.Add(PANEL_BTN_PACK_TOP, objShow);
        }
        objShow.SetActive(true);
    }

    public void HidePanelBtnPackTop()
    {
        if (popupsDictionary.ContainsKey(PANEL_BTN_PACK_TOP) && popupsDictionary[PANEL_BTN_PACK_TOP] != null)
        {
            popupsDictionary[PANEL_BTN_PACK_TOP].SetActive(false);
        }
    }

    public bool IsActivePanelBtnPackTop()
    {
        if (popupsDictionary.ContainsKey(PANEL_BTN_PACK_TOP) && popupsDictionary[PANEL_BTN_PACK_TOP] != null)
        {
            return popupsDictionary[PANEL_BTN_PACK_TOP].activeInHierarchy;
        }
        return false;
    }



    ////----Popup StarterPack
    public void ShowNotifiFinishEventPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_NOTIFI_FINISH_EVENT, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupNotifiFinishEvent);
            popupsDictionary.Remove(POPUP_NOTIFI_FINISH_EVENT);
            popupsDictionary.Add(POPUP_NOTIFI_FINISH_EVENT, objShow);

        }
        objShow.SetActive(true);
    }

    public void HideNotifiFinishEventPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_NOTIFI_FINISH_EVENT) && popupsDictionary[POPUP_NOTIFI_FINISH_EVENT] != null)
        {
            popupsDictionary[POPUP_NOTIFI_FINISH_EVENT].SetActive(false);
        }
    }

    public bool IsActiveNotifiFinishEventPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_NOTIFI_FINISH_EVENT) && popupsDictionary[POPUP_NOTIFI_FINISH_EVENT] != null)
        {
            return popupsDictionary[POPUP_NOTIFI_FINISH_EVENT].activeInHierarchy;
        }
        return false;
    }




    //----Popup CardPack
    public void ShowCardPackPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_CARD_PACK, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupCardPack);
            popupsDictionary.Remove(POPUP_CARD_PACK);
            popupsDictionary.Add(POPUP_CARD_PACK, objShow);
        }
        objShow.SetActive(true);
    }

    public void HideCardPackPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_CARD_PACK) && popupsDictionary[POPUP_CARD_PACK] != null)
        {
            popupsDictionary[POPUP_CARD_PACK].SetActive(false);
        }
    }

    //public void ChangeDepthCardPackPopup()
    //{
    //    if (popupsDictionary.ContainsKey(POPUP_CARD_PACK) && popupsDictionary[POPUP_CARD_PACK] != null)
    //    {
    //        popupsDictionary[POPUP_CARD_PACK].GetComponent<ChangeDepthLayer>().enabled = true;
    //    }
    //}
    public void SetDataCardPackPopup(int typePrice, int indexAircraftCard)
    {
        if (popupsDictionary.ContainsKey(POPUP_CARD_PACK) && popupsDictionary[POPUP_CARD_PACK] != null)
        {
            popupsDictionary[POPUP_CARD_PACK].GetComponent<PopupCardPack>().GetDataServer(typePrice, indexAircraftCard);
        }
    }

    public bool IsActiveCardPackPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_CARD_PACK) && popupsDictionary[POPUP_CARD_PACK] != null)
        {
            return popupsDictionary[POPUP_CARD_PACK].activeInHierarchy;
        }
        return false;
    }

    // open popup 3 pack

    // open popup 3 pack

    public void Show3PackPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_3PACK, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popup3Pack);
            popupsDictionary.Remove(POPUP_3PACK);
            popupsDictionary.Add(POPUP_3PACK, objShow);
        }
        objShow.SetActive(true);
        PopupManager.Instance.HideChatWorldPopup();
    }

    public void Hide3PackPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_3PACK) && popupsDictionary[POPUP_3PACK] != null)
        {
            popupsDictionary[POPUP_3PACK].SetActive(false);
        }
        PopupManager.Instance.ShowChatWorldPopup();
    }

    public void ChangeDepth3PackPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_3PACK) && popupsDictionary[POPUP_3PACK] != null)
        {
            popupsDictionary[POPUP_3PACK].GetComponent<ChangeDepthLayer>().enabled = true;
            //popupsDictionary[POPUP_3PACK].GetComponent<Popup3Pack>().ChangeDepthSerrverPush(typePack);
        }
    }

    public bool IsActive3PackPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_3PACK) && popupsDictionary[POPUP_3PACK] != null)
        {
            return popupsDictionary[POPUP_3PACK].activeInHierarchy;
        }
        return false;
    }



    //----Popup PopupItemReward
    public void ShowItemRewardPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_SHOW_ITEM_REWARD, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupItemReward);
            popupsDictionary.Remove(POPUP_SHOW_ITEM_REWARD);
            popupsDictionary.Add(POPUP_SHOW_ITEM_REWARD, objShow);

        }
        objShow.SetActive(true);
    }

    public void HideItemRewardPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SHOW_ITEM_REWARD) && popupsDictionary[POPUP_SHOW_ITEM_REWARD] != null)
        {
            popupsDictionary[POPUP_SHOW_ITEM_REWARD].SetActive(false);
        }
    }

    //public void ChangeDepthItemRewardPopup()
    //{
    //    if (popupsDictionary.ContainsKey(POPUP_STARTERPACK) && popupsDictionary[POPUP_STARTERPACK] != null)
    //    {
    //        popupsDictionary[POPUP_STARTERPACK].GetComponent<ChangeDepthLayer>().enabled = true;
    //    }
    //}

    public bool IsActiveItemRewardPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SHOW_ITEM_REWARD) && popupsDictionary[POPUP_SHOW_ITEM_REWARD] != null)
        {
            return popupsDictionary[POPUP_SHOW_ITEM_REWARD].activeInHierarchy;
        }
        return false;
    }

    //----Popup Shop
    public void ShowShopPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_SHOP, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupShop);
            popupsDictionary.Remove(POPUP_SHOP);
            popupsDictionary.Add(POPUP_SHOP, objShow);
            objShow.SetActive(true);
        }
        else
        {
            objShow.GetComponent<PopupShop>().popupShopObjInvi.SetActive(true);
            //objShow.GetComponent<PopupShop>().SetShowGlowBtnShop();
        }

    }

    public void HideShopPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SHOP) && popupsDictionary[POPUP_SHOP] != null)
        {
            popupsDictionary[POPUP_SHOP].GetComponent<PopupShop>().popupShopObjInvi.SetActive(false);
        }
    }

    public bool IsActiveShopPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SHOP) && popupsDictionary[POPUP_SHOP] != null)
        {
            return popupsDictionary[POPUP_SHOP].GetComponent<PopupShop>().popupShopObjInvi.activeInHierarchy;
        }
        return false;
    }

    //----Popup QA
    public void ShowQAPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_QA, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupQA);
            popupsDictionary.Remove(POPUP_QA);
            popupsDictionary.Add(POPUP_QA, objShow);
            objShow.SetActive(true);
        }
        else
        {
            //Debug.LogError("ShowShopPopup popupShopObjInvi");
            //objShow.GetComponent<PopupShop>().popupShopObjInvi.SetActive(true);
            objShow.GetComponent<Quest_Achi_Manager>().ChangeOpenPanel();
        }

    }

    public void HideQAPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_QA) && popupsDictionary[POPUP_QA] != null)
        {
            popupsDictionary[POPUP_QA].GetComponent<Quest_Achi_Manager>().Quest_Achi_InviObj.SetActive(false);
            MessageDispatcher.SendMessage(this, EventID.ON_DISABLE_SHOW_RECEIVE_ITEM_FX, this, 0);
        }
    }

    public bool IsActiveQAPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_QA) && popupsDictionary[POPUP_QA] != null)
        {
            return popupsDictionary[POPUP_QA].GetComponent<Quest_Achi_Manager>().Quest_Achi_InviObj.activeInHierarchy;
        }
        return false;
    }

    //----Popup Setting
    public void ShowSettingPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_SETTING, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupSetting);
            popupsDictionary.Remove(POPUP_SETTING);
            popupsDictionary.Add(POPUP_SETTING, objShow);
        }
        objShow.SetActive(true);
        objShow.GetComponent<PopupSetting>().Change_When_OpenPopup();
    }

    public void HideSettingPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SETTING) && popupsDictionary[POPUP_SETTING] != null)
        {
            popupsDictionary[POPUP_SETTING].SetActive(false);
        }
    }

    public bool IsActiveSettingPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SETTING) && popupsDictionary[POPUP_SETTING] != null)
        {
            return popupsDictionary[POPUP_SETTING].activeInHierarchy;
        }
        return false;
    }



    //----Popup GiftCode
    public void ShowGiftCodePopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_GIFTCODE, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupGiftCode);
            popupsDictionary.Remove(POPUP_GIFTCODE);
            popupsDictionary.Add(POPUP_GIFTCODE, objShow);
        }
        objShow.SetActive(true);
    }

    public void HideGiftCodePopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_GIFTCODE) && popupsDictionary[POPUP_GIFTCODE] != null)
        {
            popupsDictionary[POPUP_GIFTCODE].SetActive(false);
        }
    }

    public bool IsActiveGiftCodePopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_GIFTCODE) && popupsDictionary[POPUP_GIFTCODE] != null)
        {
            return popupsDictionary[POPUP_GIFTCODE].activeInHierarchy;
        }
        return false;
    }


    //----Popup ExitGame
    public void ShowExitGamePopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_EXIT_GAME, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupExitGame);
            popupsDictionary.Remove(POPUP_EXIT_GAME);
            popupsDictionary.Add(POPUP_EXIT_GAME, objShow);
        }
        objShow.SetActive(true);
        if (SceneManager.GetActiveScene().name.Equals("Home"))
        {
            PopupManager.Instance.HideChatWorldPopup();
        }
    }

    public void HideExitGamePopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_EXIT_GAME) && popupsDictionary[POPUP_EXIT_GAME] != null)
        {
            popupsDictionary[POPUP_EXIT_GAME].SetActive(false);
            if (SceneManager.GetActiveScene().name.Equals("Home"))
            {
                PopupManager.Instance.ShowChatWorldPopup();
            }
        }
    }

    public bool IsActiveExitGamePopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_EXIT_GAME) && popupsDictionary[POPUP_EXIT_GAME] != null)
        {
            return popupsDictionary[POPUP_EXIT_GAME].activeInHierarchy;
        }
        return false;
    }

    //----Popup Rate
    public void ShowRatePopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_RATE, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupRate);
            popupsDictionary.Remove(POPUP_RATE);
            popupsDictionary.Add(POPUP_RATE, objShow);
        }
        objShow.SetActive(true);
        objShow.GetComponent<PopupRate>().ShowPopupRate();
    }

    public void HideRatePopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_RATE) && popupsDictionary[POPUP_RATE] != null)
        {
            popupsDictionary[POPUP_RATE].SetActive(false);
        }
    }
    public bool IsActiveRatePopupPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_RATE) && popupsDictionary[POPUP_RATE] != null)
        {
            return popupsDictionary[POPUP_RATE].activeInHierarchy;
        }
        return false;
    }


    public void ChangeDepthRatePopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_RATE) && popupsDictionary[POPUP_RATE] != null)
        {
            popupsDictionary[POPUP_RATE].GetComponent<ChangeDepthLayer>().enabled = true;
        }
    }

    public bool IsActiveRatePopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_RATE) && popupsDictionary[POPUP_RATE] != null)
        {
            return popupsDictionary[POPUP_RATE].activeInHierarchy;
        }
        return false;
    }


    //----Popup Video EndGame
    public void ShowVideoEndgamePopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_VIDEO_ENDGAME, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupVideoEndgame);
            popupsDictionary.Remove(POPUP_VIDEO_ENDGAME);
            popupsDictionary.Add(POPUP_VIDEO_ENDGAME, objShow);
        }
        objShow.SetActive(true);
    }

    public void HideVideoEndgamePopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_VIDEO_ENDGAME) && popupsDictionary[POPUP_VIDEO_ENDGAME] != null)
        {
            popupsDictionary[POPUP_VIDEO_ENDGAME].SetActive(false);
        }
    }
    public bool IsActiveVideoEndgamePopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_VIDEO_ENDGAME) && popupsDictionary[POPUP_VIDEO_ENDGAME] != null)
        {
            return popupsDictionary[POPUP_VIDEO_ENDGAME].activeInHierarchy;
        }
        return false;
    }

    //----Popup Select Level
    public void ShowSelectLevelPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_SELECT_LEVEL, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupSelectLevel);
            popupsDictionary.Remove(POPUP_SELECT_LEVEL);
            popupsDictionary.Add(POPUP_SELECT_LEVEL, objShow);
        }
        objShow.SetActive(true);
        objShow.GetComponent<PopupSelectLvCampaign>().SetPopupOpen();

    }

    public void HideSelectLevelPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SELECT_LEVEL) && popupsDictionary[POPUP_SELECT_LEVEL] != null)
        {
            popupsDictionary[POPUP_SELECT_LEVEL].SetActive(false);
        }
        PopupManager.Instance.ShowChatWorldPopup();
    }

    public bool IsActiveSelectLevelPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SELECT_LEVEL) && popupsDictionary[POPUP_SELECT_LEVEL] != null)
        {
            return popupsDictionary[POPUP_SELECT_LEVEL].activeInHierarchy;
        }
        return false;
    }

    //----Popup Select Aircraft
    public void ShowSelectAircraftPopup(SelectDeviceHandler.OpenSelectFromType fromType, bool needToLoadFromTry)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_SELECT_AIRCRAFT, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupSelectAircraft);
            popupsDictionary.Remove(POPUP_SELECT_AIRCRAFT);
            popupsDictionary.Add(POPUP_SELECT_AIRCRAFT, objShow);
        }

        HideSelectWingmanPopup();
        objShow.GetComponent<PopupSelectAircraft>().ShowPopUp(fromType, needToLoadFromTry);
        if (SceneManager.GetActiveScene().name.Equals("Home"))
        {
            PopupManager.Instance.HideChatWorldPopup();
        }
    }

    public void HideSelectAircraftPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SELECT_AIRCRAFT) && popupsDictionary[POPUP_SELECT_AIRCRAFT] != null)
        {
            popupsDictionary[POPUP_SELECT_AIRCRAFT].GetComponent<PopupSelectAircraft>().OnClickBtnBack();
        }
        if (SceneManager.GetActiveScene().name.Equals("Home"))
        {
            PopupManager.Instance.ShowChatWorldPopup();
        }
    }

    public bool IsActiveSelectAircraftPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SELECT_AIRCRAFT) && popupsDictionary[POPUP_SELECT_AIRCRAFT] != null)
        {
            return popupsDictionary[POPUP_SELECT_AIRCRAFT].activeInHierarchy;
        }
        return false;
    }

    public void ChangeDepthSelectAircraftPopup()
    {
        Debug.LogError(popupsDictionary.ContainsKey(POPUP_SELECT_AIRCRAFT) + " * " + popupsDictionary[POPUP_SELECT_AIRCRAFT] != null);
        if (popupsDictionary.ContainsKey(POPUP_SELECT_AIRCRAFT) && popupsDictionary[POPUP_SELECT_AIRCRAFT] != null)
        {
            popupsDictionary[POPUP_SELECT_AIRCRAFT].GetComponent<ChangeDepthLayer>().enabled = true;
        }
    }

    //-----Popup Select Wingman
    public void ShowSelectWingmanPopup(WingmanPosType wingmanPosType, SelectDeviceHandler.OpenSelectFromType fromType, bool needToLoadFromTry)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_SELECT_WINGMAN, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupSelectWingman);
            popupsDictionary.Remove(POPUP_SELECT_WINGMAN);
            popupsDictionary.Add(POPUP_SELECT_WINGMAN, objShow);
        }

        HideSelectAircraftPopup();
        objShow.GetComponent<PopupSelectWingman>().ShowPopUp(wingmanPosType, fromType, needToLoadFromTry);
        if (SceneManager.GetActiveScene().name.Equals("Home"))
        {
            PopupManager.Instance.HideChatWorldPopup();
        }
    }

    public void HideSelectWingmanPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SELECT_WINGMAN) && popupsDictionary[POPUP_SELECT_WINGMAN] != null)
        {
            popupsDictionary[POPUP_SELECT_WINGMAN].GetComponent<PopupSelectWingman>().OnClickBtnBack();
        }
        if (SceneManager.GetActiveScene().name.Equals("Home"))
        {
            PopupManager.Instance.ShowChatWorldPopup();
        }
    }

    public bool IsActiveSelectWingmanPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SELECT_WINGMAN) && popupsDictionary[POPUP_SELECT_WINGMAN] != null)
        {
            return popupsDictionary[POPUP_SELECT_WINGMAN].activeInHierarchy;
        }
        return false;
    }

    public void ChangeDepthSelectWingmanPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SELECT_WINGMAN) && popupsDictionary[POPUP_SELECT_WINGMAN] != null)
        {
            popupsDictionary[POPUP_SELECT_WINGMAN].GetComponent<ChangeDepthLayer>().enabled = true;
        }
    }

    //Popup Select Wing
    public void ShowSelectWingPopup(SelectDeviceHandler.OpenSelectFromType fromType, bool needToLoadFromTry)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_SELECT_WING, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupSelectWing);
            popupsDictionary.Remove(POPUP_SELECT_WING);
            popupsDictionary.Add(POPUP_SELECT_WING, objShow);
        }

        //HideSelectWingmanPopup();
        objShow.GetComponent<PopupSelectWing>().ShowPopUp(fromType, needToLoadFromTry);
        if (SceneManager.GetActiveScene().name.Equals("Home"))
        {
            PopupManager.Instance.HideChatWorldPopup();
        }
    }

    public void HideSelectWingPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SELECT_WING) && popupsDictionary[POPUP_SELECT_WING] != null)
        {
            popupsDictionary[POPUP_SELECT_WING].GetComponent<PopupSelectWing>().OnClickBtnBack();
        }
        if (SceneManager.GetActiveScene().name.Equals("Home"))
        {
            PopupManager.Instance.ShowChatWorldPopup();
        }
    }

    public bool IsActiveSelectWingPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SELECT_WING) && popupsDictionary[POPUP_SELECT_WING] != null)
        {
            return popupsDictionary[POPUP_SELECT_WING].activeInHierarchy;
        }
        return false;
    }

    public void ChangeDepthSelectWingPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_SELECT_WING) && popupsDictionary[POPUP_SELECT_WING] != null)
        {
            popupsDictionary[POPUP_SELECT_WING].GetComponent<ChangeDepthLayer>().enabled = true;
        }
    }

    //------ Popup quest open IAP
    public void ShowQuestOpenIAPPopup(ResourceType type)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_QUEST_OPEN_IAP, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupQuestOpenIAP);
            popupsDictionary.Remove(POPUP_QUEST_OPEN_IAP);
            popupsDictionary.Add(POPUP_QUEST_OPEN_IAP, objShow);
        }
        objShow.GetComponent<PopupQuestOpenIAP>().ShowPopup(type);
    }

    public void HideQuestOpenIAPPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_QUEST_OPEN_IAP) && popupsDictionary[POPUP_QUEST_OPEN_IAP] != null)
        {
            popupsDictionary[POPUP_QUEST_OPEN_IAP].SetActive(false);
        }
    }

    public bool IsActiveQuestOpenIAPPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_QUEST_OPEN_IAP) && popupsDictionary[POPUP_QUEST_OPEN_IAP] != null)
        {
            return popupsDictionary[POPUP_QUEST_OPEN_IAP].activeInHierarchy;
        }
        return false;
    }

    //Popup Halloween
    public void ShowEventPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_EVENT, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupEvent);
            popupsDictionary.Remove(POPUP_EVENT);
            popupsDictionary.Add(POPUP_EVENT, objShow);
        }
        objShow.SetActive(true);
    }

    public void HideEventPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_EVENT) && popupsDictionary[POPUP_EVENT] != null)
        {
            popupsDictionary[POPUP_EVENT].SetActive(false);
        }
    }

    public bool IsActiveEventPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_EVENT) && popupsDictionary[POPUP_EVENT] != null)
        {
            return popupsDictionary[POPUP_EVENT].activeInHierarchy;
        }
        return false;
    }

    //Popup Buy Event Item
    public void ShowBuyEventItemPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_BUY_EVENT_ITEM, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupBuyEventItem);
            popupsDictionary.Remove(POPUP_BUY_EVENT_ITEM);
            popupsDictionary.Add(POPUP_BUY_EVENT_ITEM, objShow);
        }
        objShow.SetActive(true);
    }

    public void HideBuyEventItemPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_BUY_EVENT_ITEM) && popupsDictionary[POPUP_BUY_EVENT_ITEM] != null)
        {
            popupsDictionary[POPUP_BUY_EVENT_ITEM].SetActive(false);
        }
    }

    public bool IsActiveBuyEventItemPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_BUY_EVENT_ITEM) && popupsDictionary[POPUP_BUY_EVENT_ITEM] != null)
        {
            return popupsDictionary[POPUP_BUY_EVENT_ITEM].activeInHierarchy;
        }
        return false;
    }

    //Popup Calling Backup
    public void ShowCallingBackUpPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_CALLING_BACKUP, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupCallingBackup);
            popupsDictionary.Remove(POPUP_CALLING_BACKUP);
            popupsDictionary.Add(POPUP_CALLING_BACKUP, objShow);
        }
        objShow.SetActive(true);
    }

    public void HideCallingBackUpPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_CALLING_BACKUP) && popupsDictionary[POPUP_CALLING_BACKUP] != null)
        {
            popupsDictionary[POPUP_CALLING_BACKUP].SetActive(false);
        }
    }

    public bool IsActiveCallingBackUpPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_CALLING_BACKUP) && popupsDictionary[POPUP_CALLING_BACKUP] != null)
        {
            return popupsDictionary[POPUP_CALLING_BACKUP].activeInHierarchy;
        }
        return false;
    }

    //Popup item info
    public void ShowItemInfoPopup(string type, int quantity)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_ITEM_INFO, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupItemInfo);
            popupsDictionary.Remove(POPUP_ITEM_INFO);
            popupsDictionary.Add(POPUP_ITEM_INFO, objShow);
        }
        objShow.GetComponent<PopupItemInfo>().ShowPopup(type, quantity);

    }

    public void HideItemInfoPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_ITEM_INFO) && popupsDictionary[POPUP_ITEM_INFO] != null)
        {
            popupsDictionary[POPUP_ITEM_INFO].SetActive(false);
        }
    }

    public bool IsActiveItemInfoPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_ITEM_INFO) && popupsDictionary[POPUP_ITEM_INFO] != null)
        {
            return popupsDictionary[POPUP_ITEM_INFO].activeInHierarchy;
        }
        return false;
    }

    //Popup unlock evolve reward
    public void ShowPromoteEventPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_PROMOTE_EVENT, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupPromoteEvent);
            popupsDictionary.Remove(POPUP_PROMOTE_EVENT);
            popupsDictionary.Add(POPUP_PROMOTE_EVENT, objShow);
        }
        objShow.SetActive(true);
    }

    public void HidePromoteEventPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_PROMOTE_EVENT) && popupsDictionary[POPUP_PROMOTE_EVENT] != null)
        {
            popupsDictionary[POPUP_PROMOTE_EVENT].SetActive(false);
        }
    }

    public bool IsActivePromoteEventPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_PROMOTE_EVENT) && popupsDictionary[POPUP_PROMOTE_EVENT] != null)
        {
            return popupsDictionary[POPUP_PROMOTE_EVENT].activeInHierarchy;
        }
        return false;
    }

    //Popup Notice in trial:
    //Popup unlock evolve reward
    public void ShowNoticeInTrialAircraftPopup(AircraftTypeEnum aircraftType)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_NOTICE_IN_TRIAL, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupNoticeInTrial);
            popupsDictionary.Remove(POPUP_NOTICE_IN_TRIAL);
            popupsDictionary.Add(POPUP_NOTICE_IN_TRIAL, objShow);
        }

        objShow.GetComponent<PopupNoticeInTrial>().ShowPopupAircraftNotice(aircraftType);
    }

    public void ShowNoticeInTrialWingmanPopup(WingmanTypeEnum wingmanType, WingmanPosType posType)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_NOTICE_IN_TRIAL, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupNoticeInTrial);
            popupsDictionary.Remove(POPUP_NOTICE_IN_TRIAL);
            popupsDictionary.Add(POPUP_NOTICE_IN_TRIAL, objShow);
        }

        objShow.GetComponent<PopupNoticeInTrial>().ShowPopupWingmanNotice(wingmanType, posType);
    }

    public void ShowNoticeInTrialWingPopup(WingTypeEnum wingName)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_NOTICE_IN_TRIAL, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupNoticeInTrial);
            popupsDictionary.Remove(POPUP_NOTICE_IN_TRIAL);
            popupsDictionary.Add(POPUP_NOTICE_IN_TRIAL, objShow);
        }

        objShow.GetComponent<PopupNoticeInTrial>().ShowPopupWingNotice(wingName);
    }

    public void HideNoticeInTrialPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_NOTICE_IN_TRIAL) && popupsDictionary[POPUP_NOTICE_IN_TRIAL] != null)
        {
            popupsDictionary[POPUP_NOTICE_IN_TRIAL].SetActive(false);
        }
    }

    public bool IsActiveNoticeInTrialPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_NOTICE_IN_TRIAL) && popupsDictionary[POPUP_NOTICE_IN_TRIAL] != null)
        {
            return popupsDictionary[POPUP_NOTICE_IN_TRIAL].activeInHierarchy;
        }
        return false;
    }

    //Popup star chest
    public void ShowStarChestPopup(int currentWorld, int indexChest, int totalStarWorld, string starSpriteName, string chestSpriteName)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_STAR_CHEST, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupStarChest);
            popupsDictionary.Remove(POPUP_STAR_CHEST);
            popupsDictionary.Add(POPUP_STAR_CHEST, objShow);
        }

        objShow.GetComponent<PopupStarChest>().ShowPopup(currentWorld, indexChest, totalStarWorld, starSpriteName, chestSpriteName);
    }

    public void ShowStarChestPopupExtra(int currentWorld, int totalStarExtra, string starSpriteName, string chestSpriteName)
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_STAR_CHEST, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupStarChest);
            popupsDictionary.Remove(POPUP_STAR_CHEST);
            popupsDictionary.Add(POPUP_STAR_CHEST, objShow);
        }

        objShow.GetComponent<PopupStarChest>().ShowPopupExtra(currentWorld, totalStarExtra, starSpriteName, chestSpriteName);
    }

    public void HideStarChestPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_STAR_CHEST) && popupsDictionary[POPUP_STAR_CHEST] != null)
        {
            popupsDictionary[POPUP_STAR_CHEST].SetActive(false);
        }
    }

    public bool IsActiveStarChestPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_STAR_CHEST) && popupsDictionary[POPUP_STAR_CHEST] != null)
        {
            return popupsDictionary[POPUP_STAR_CHEST].activeInHierarchy;
        }
        return false;
    }

    //Popup Royalty
    public void ShowRoyaltyPopup()
    {
        GameObject objShow;
        if (!popupsDictionary.TryGetValue(POPUP_ROYALTY, out objShow) || objShow == null)
        {
            objShow = NGUITools.AddChild(gameObject, popupRoyalty);
            popupsDictionary.Remove(POPUP_ROYALTY);
            popupsDictionary.Add(POPUP_ROYALTY, objShow);
        }

        objShow.SetActive(true);
    }

    public void HideRoyaltyPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_ROYALTY) && popupsDictionary[POPUP_ROYALTY] != null)
        {
            popupsDictionary[POPUP_ROYALTY].SetActive(false);
        }
    }

    public bool IsActiveRoyaltyPopup()
    {
        if (popupsDictionary.ContainsKey(POPUP_ROYALTY) && popupsDictionary[POPUP_ROYALTY] != null)
        {
            return popupsDictionary[POPUP_ROYALTY].activeInHierarchy;
        }
        return false;
    }
}
