﻿using Firebase.Analytics;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class ButtonDontDestroy : MonoBehaviour
{



    public void ButtonSetup()
    {
        SoundManager.PlayShowPopup();
        PopupManagerCuong.Instance.ShowSettingPopup();
        FirebaseLogSpaceWar.LogClickButton("Setting");
        PopupManager.Instance.HideChatWorldPopup();
    }

    public void ButtonShop()
    {
        if (!CachePvp.sCVersionConfig.canRandomCardShop)
        {
            PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainRandomCardShop);
            return;
        }
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();

            if (!OSNet.NetManager.Instance.IsOnline())
            {
                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
                return;
            }

            PopupManagerCuong.Instance.ShowShopPopup();

            PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.CardRandom);


            //if (CachePvp.FirstTabIndex == 1)
            //{
            //    PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.IapGem);
            //}
            //else
            //{
            //    PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.IapGold);
            //}
        }
        //FirebaseLogSpaceWar.LogClickButton("Upgrade");
    }


}
