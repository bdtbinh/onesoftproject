﻿using Firebase.Analytics;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelBtnQA : Singleton<PanelBtnQA>
{
    public GameObject glowBtnQA;


    public void Button_Quest_Achi()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();

            PopupManagerCuong.Instance.ShowQAPopup();
            Quest_Achi_Manager.Instance.ChangeOpenPanel();
        }
        PopupManager.Instance.HideChatWorldPopup();
        FirebaseLogSpaceWar.LogClickButton("QA");
    }
}
