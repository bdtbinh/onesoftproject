﻿using DG.Tweening;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;


public class PanelCoinGem : Singleton<PanelCoinGem>
{

    public UILabel lNumCoinTop;
    public UILabel lNumGemTop;
    public UILabel lNumEnergyTop;

    public GameObject glowPackX2Gold;
    public GameObject glowPackSeverX2AllGold;
    //
    public GameObject glowPackX2Gem;
    public GameObject glowPackSeverX2AllGem;

    public GameObject energyButton;

    // Use this for initialization
    void Start()
    {
        AddCoinTop(0);
        AddGemTop();
        AddEnergyTop();

        ShowGlowButtonIapGold();
        ShowGlowButtonIapGem();
    }



    public void ShowGlowButtonIapGold()
    {
        //Debug.LogError("serverX2PurchaseGold: " + CachePvp.serverX2PurchaseGold);

        if (CachePvp.listPromotionsGold != null && CachePvp.serverX2PurchaseGold != 1)
        {
            glowPackX2Gold.SetActive(true);
        }
        else
        {
            glowPackX2Gold.SetActive(false);
        }


        if (CachePvp.serverX2PurchaseGold == 1)
        {
            glowPackSeverX2AllGold.SetActive(true);
        }
        else
        {
            glowPackSeverX2AllGold.SetActive(false);
        }

    }


    public void ShowGlowButtonIapGem()
    {
        //Debug.LogError("serverX2PurchaseGem  " + CachePvp.serverX2PurchaseGem);

        if (CachePvp.listPromotionsGem != null && CachePvp.serverX2PurchaseGem != 1)
        {
            glowPackX2Gem.SetActive(true);
        }
        else
        {
            glowPackX2Gem.SetActive(false);
        }

        if (CachePvp.serverX2PurchaseGem == 1)
        {
            glowPackSeverX2AllGem.SetActive(true);
        }
        else
        {
            glowPackSeverX2AllGem.SetActive(false);
        }
    }


    public void Btn_IapCoinTop()
    {
        SoundManager.PlayShowPopup();
        PopupManagerCuong.Instance.ShowShopPopup();
        if (PopupShop.Instance != null)
        {
            PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.IapGold);
        }
        FirebaseLogSpaceWar.LogClickButton("IapGoldTop");
    }

    public void Btn_IapGemTop()
    {
        SoundManager.PlayShowPopup();
        PopupManagerCuong.Instance.ShowShopPopup();
        if (PopupShop.Instance != null)
        {
            PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.IapGem);
        }
        FirebaseLogSpaceWar.LogClickButton("IapGemTop");
    }

    float timeDurationAddCoin;
    public void AddCoinTop(int numCoinAdd = 0, string whyLogFirebase = "unknown", string whereLogFirebase = "unknown")
    {
        if (numCoinAdd > 10)
        {
            SoundManager.SoundGetCoinBig();
        }
        else if (numCoinAdd <= 10 && numCoinAdd != 0)
        {
            SoundManager.SoundGetCoinSmall();
        }
        if (numCoinAdd > 0)
        {
            timeDurationAddCoin = 0.003f * numCoinAdd;
        }
        else if (numCoinAdd < 0)
        {
            timeDurationAddCoin = -0.003f * numCoinAdd;
        }
        if (timeDurationAddCoin > 2f)
        {
            timeDurationAddCoin = 2f;
        }
        //----

        lNumCoinTop.DOUILabelInt(CacheGame.GetTotalCoin(), (CacheGame.GetTotalCoin() + numCoinAdd), timeDurationAddCoin).SetUpdate(true);
        CacheGame.SetTotalCoin(CacheGame.GetTotalCoin() + numCoinAdd, whyLogFirebase, whereLogFirebase);

       
        
    }

    //-----------------------------------------------------
    float timeDurationAddGem;
    public void AddGemTop(int numGemAdd = 0, string whyLogFirebase = "unknown", string whereLogFirebase = "unknown")
    {
        if (numGemAdd != 0)
        {
            SoundManager.SoundGetCoinSmall();
        }
        if (numGemAdd > 0)
        {
            timeDurationAddGem = 0.003f * numGemAdd;
        }
        else if (numGemAdd < 0)
        {
            timeDurationAddGem = -0.003f * numGemAdd;
        }
        if (timeDurationAddGem > 2f)
        {
            timeDurationAddGem = 2f;
        }
        lNumGemTop.DOUILabelInt(CacheGame.GetTotalGem(), (CacheGame.GetTotalGem() + numGemAdd), timeDurationAddGem).SetUpdate(true);

        CacheGame.SetTotalGem(CacheGame.GetTotalGem() + numGemAdd, whyLogFirebase, whereLogFirebase);
    }

    //-----------------------------------------------------
    float timeDurationAddEnergy;
    public void AddEnergyTop(int numEnergyAdd = 0, string whyLogFirebase = "unknown", string whereLogFirebase = "unknown")
    {
        if (numEnergyAdd != 0)
        {
            SoundManager.SoundGetCoinSmall();
        }
        if (numEnergyAdd > 0)
        {
            timeDurationAddEnergy = 0.003f * numEnergyAdd;
        }
        else if (numEnergyAdd < 0)
        {
            timeDurationAddEnergy = -0.003f * numEnergyAdd;
        }
        if (timeDurationAddEnergy > 2f)
        {


            timeDurationAddEnergy = 2f;
        }
        lNumEnergyTop.DOUILabelInt(CacheGame.GetTotalEnergy(), (CacheGame.GetTotalEnergy() + numEnergyAdd), timeDurationAddGem).SetUpdate(true);

        CacheGame.SetTotalEnergy(CacheGame.GetTotalEnergy() + numEnergyAdd, whyLogFirebase, whereLogFirebase);
    }

    

    public void ShowEnergyButton()
    {
        energyButton.SetActive(true);
        AddEnergyTop();
    }

    public void HideEnergyButton()
    {
        energyButton.SetActive(false);
    }
}
