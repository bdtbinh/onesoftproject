﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangePlaneShow : SkyGameKit.Singleton<ChangePlaneShow>
{


    public List<GameObject> listPlaneUI;

    void Start()
    {
        ChangePlaneShowInUI();
    }


    public void ChangePlaneShowInUI()
    {
        if (SceneManager.GetActiveScene().name == "Home")
        {
            for (int i = 1; i < (listPlaneUI.Count + 1); i++)
            {
                if (CacheGame.GetSpaceShipUserSelected() == i)
                {
                    listPlaneUI[i - 1].SetActive(true);
                }
                else
                {
                    listPlaneUI[i - 1].SetActive(false);
                }
            }
        }
    }



}
