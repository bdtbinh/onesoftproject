﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using PathologicalGames;
using OneSoftGame.Tools;

public class NotificationText : MonoBehaviour
{

    public UILabel lNotifiButton;
    public UISprite sBgNotifiButton;
    public TweenPosition tweenPosition;
    Transform tran_Cache;
    float fRunHide = 0;


    public void ShowTextNotifiClickButton(string textNotifi, bool clickSuccess, float timeShow = 1.31f)
    {
        // Success
        if (clickSuccess)
        {
            sBgNotifiButton.spriteName = "titler_BG_glow4";
        }
        else
        {
            sBgNotifiButton.spriteName = "titler_BG_glow5";
        }
        lNotifiButton.text = "" + textNotifi;

        StartCoroutine("WaitForDestroy", timeShow);
    }

    IEnumerator WaitForDestroy(float timeShow)
    {
        yield return new WaitForSecondsRealtime(timeShow);
        Destroy(gameObject);
    }


    private void OnDisable()
    {
        //LeanTween.cancel(gameObject);
        StopAllCoroutines();
    }

}
