﻿using UnityEngine;
using System.Collections;
using TCore;

//using BigFox;
using Firebase.Analytics;
using UnityEngine.SceneManagement;
using com.ootii.Messages;

public class PopupSetting : MonoBehaviour
{
    public GameObject btn_sound;
    public GameObject btn_music;

    //
    public UIScrollBar scrollbarValue;
    public UILabel lSensitivity;
    public UISprite sFilled;

    public UISprite backgroundNotify;
    public UISprite backgroundColtrol;

    public GameObject panelChina, panelGlobal;

    public GameObject panelLanguages;



    public void buttonNotifycation()
    {
        if (CachePvp.CanReceiveNotify == 1)
        {
            CachePvp.CanReceiveNotify = 0;
            backgroundNotify.spriteName = "btn_soundoff";
        }
        else
        {
            CachePvp.CanReceiveNotify = 1;
            backgroundNotify.spriteName = "btn_sound";
        }
        SoundManager.PlayClickButton();
    }

    public void Button_Change_Control()
    {
        if (CacheGame.ValueColtrolButtonSkill == 1)
        {
            CacheGame.ValueColtrolButtonSkill = 0;
            backgroundColtrol.spriteName = "Btn_ControlScheme_R";
        }
        else
        {
            CacheGame.ValueColtrolButtonSkill = 1;
            backgroundColtrol.spriteName = "Btn_ControlScheme_L";
        }
        MessageDispatcher.SendMessage(EventID.ON_CHANGE_CONTROL);
        SoundManager.PlayClickButton();
    }

    public void Change_When_OpenPopup()
    {
        SoundManager.PlayShowPopup();

        if (PlayerPrefs.GetInt("SoundValue") == 1)
        {
            btn_sound.transform.GetChild(0).GetComponent<UISprite>().spriteName = "btn_soundoff";
        }
        else
        {
            btn_sound.transform.GetChild(0).GetComponent<UISprite>().spriteName = "btn_sound";
        }

        if (PlayerPrefs.GetInt("MusicValue") == 1)
        {
            btn_music.transform.GetChild(0).GetComponent<UISprite>().spriteName = "btn_musicoff";
        }
        else
        {
            btn_music.transform.GetChild(0).GetComponent<UISprite>().spriteName = "btn_music";
        }

        SetScrollBar_SpeedPlayer();
        if (CachePvp.CanReceiveNotify == 0)
        {
            if (backgroundNotify != null)
            {
                backgroundNotify.spriteName = "btn_soundoff";
            }
        }
        else
        {
            if (backgroundNotify != null)
            {
                backgroundNotify.spriteName = "btn_sound";
            }
        }


        if (CacheGame.ValueColtrolButtonSkill == 0)
        {
            if (backgroundColtrol != null)
            {
                backgroundColtrol.spriteName = "Btn_ControlScheme_R";
            }
        }
        else
        {
            if (backgroundColtrol != null)
            {
                backgroundColtrol.spriteName = "Btn_ControlScheme_L";
            }
        }

        SetupChinaVersion();
    }



    public void MusicButton()
    {
        int intMusic = PlayerPrefs.GetInt("MusicValue");
        if (intMusic == 1)
        {
            PlayerPrefs.SetInt("MusicValue", 2);
        }
        else
        {
            PlayerPrefs.SetInt("MusicValue", 1);
        }

        SoundManager.PlayClickButton();
        if (PlayerPrefs.GetInt("MusicValue") == 1)
        {
            btn_music.transform.GetChild(0).GetComponent<UISprite>().spriteName = "btn_musicoff";
        }
        else
        {
            btn_music.transform.GetChild(0).GetComponent<UISprite>().spriteName = "btn_music";
        }

        if (SceneManager.GetActiveScene().name != "Home" && SceneManager.GetActiveScene().name != "SelectLevel")
        {
            //			Debug.LogError ("PlayMain2BG");
            SoundManager.PlayMain2BG();
        }
        else
        {
            //			Debug.LogError ("PlayHomeBG");
            SoundManager.PlayHomeBG();
        }
    }



    public void SoundButton()
    {
        int intSound = PlayerPrefs.GetInt("SoundValue");
        if (intSound == 1)
        {
            PlayerPrefs.SetInt("SoundValue", 2);
        }
        else
        {
            PlayerPrefs.SetInt("SoundValue", 1);
        }

        SoundManager.PlayClickButton();
        if (PlayerPrefs.GetInt("SoundValue") == 1)
        {
            btn_sound.transform.GetChild(0).GetComponent<UISprite>().spriteName = "btn_soundoff";
        }
        else
        {
            btn_sound.transform.GetChild(0).GetComponent<UISprite>().spriteName = "btn_sound";
        }

        if (SceneManager.GetActiveScene().name != "Home" && SceneManager.GetActiveScene().name != "SelectLevel")
        {
            SoundManager.PlayMain2BG();
        }
        else
        {
            SoundManager.PlayHomeBG();
        }
    }

    public void BackButton()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
        PopupManager.Instance.ShowChatWorldPopup();
    }

    public void MoreGameButton()
    {
        SoundManager.PlayClickButton();

#if UNITY_ANDROID
        //Application.OpenURL("https://play.google.com/store/apps/developer?id=OneSoft+Studio");
        Application.OpenURL("market://details?id=com.os.space.force.galaxy.alien");
#elif UNITY_IPHONE
		//Application.OpenURL("itms-apps://itunes.apple.com/lookup?id=1337514468");
        Application.OpenURL("itms-apps://itunes.apple.com/app/id1304211798");
#endif
    }

    public void RateButton()
    {
        SoundManager.PlayClickButton();
#if UNITY_ANDROID
        //Application.OpenURL("market://details?id=invaders.os.galaxy.space.shooter.attack.classic");
        string bundleID = "" + Application.identifier;
        Application.OpenURL("market://details?id=" + bundleID);
#elif UNITY_IPHONE
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1337514468");
#endif
        FirebaseLogSpaceWar.LogClickButton("Rate");
        MinhCacheGame.SetAlreadyRate();
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.RateGame.ToString(), 2);
        //FirebaseAnalytics.LogEvent("Click", new Parameter("Button", "Rate"));
    }


    public void GiftCodeButton()
    {
        SoundManager.PlayClickButton();
        //DontDestroyManager.Instance.popupGiftCode_Obj.SetActive(true);
        PopupManagerCuong.Instance.ShowGiftCodePopup();
    }

    //-----------------------------------Sensityvity-----------------------------------------------------------

    // goi ham` nay o dau` ham` LocalToValue trong script UIScrollBar va truyen` vao` value

    public void Change_When_DragScrollBar()
    {
        CacheGame.SetSpeedPlayer(scrollbarValue.value * GameContext.numChangeSpeedPlayer);
        lSensitivity.text = "" + scrollbarValue.value * 5;
        sFilled.fillAmount = scrollbarValue.value;

        MessageDispatcher.SendMessage(EventID.ON_CHANGE_SENSITIVITY);
        //Debug.LogError("SetSpeedPlayer : " + CacheGame.GetSpeedPlayer());
    }

    public void SetScrollBar_SpeedPlayer()
    {
        if (!PlayerPrefs.HasKey("SpeedPlayer_Scrollbar"))
        {
            scrollbarValue.value = 0.2f;
            CacheGame.SetSpeedPlayer(scrollbarValue.value * GameContext.numChangeSpeedPlayer);
        }
        //Debug.LogError("scrollbarValue.value 1: " + scrollbarValue.value);
        scrollbarValue.value = CacheGame.GetSpeedPlayer() / GameContext.numChangeSpeedPlayer;

        sFilled.fillAmount = scrollbarValue.value;
        lSensitivity.text = "" + scrollbarValue.value * 5;

    }

    void SetupChinaVersion()
    {
        if (panelChina == null || panelGlobal == null)
        {
            return;
        }
        panelChina.SetActive(GameContext.IS_CHINA_VERSION);
        panelGlobal.SetActive(!GameContext.IS_CHINA_VERSION);

    }

    public void OnDisable()
    {
        if (panelLanguages != null)
        {
            panelLanguages.SetActive(false);
        }
    }

}
