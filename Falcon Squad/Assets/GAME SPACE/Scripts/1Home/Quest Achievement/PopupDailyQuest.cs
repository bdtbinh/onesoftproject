﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TCore;

public class PopupDailyQuest : SkyGameKit.Singleton<PopupDailyQuest>
{


    public Transform parentAllItem;
    public List<GameObject> listAllItemDaily;
    public List<GameObject> listItemHard;
    public List<GameObject> listItemEasy;
    //
    //


    List<GameObject> listItemShow = new List<GameObject>();

    List<int> listLevelItemShow = new List<int>();



    public void SetDataStartPopup()
    {
        if (CacheGame.GetDateOpenDailyQuest() != CachePvp.dateTime.Day)
        {
            CacheGame.SetDateOpenDailyQuest(CachePvp.dateTime.Day);
            ClearDataListItemRandom();
            GetDataAllItem();
            RandomItemShowDaily();
            ShowItem();
            InProgressDailyQuest.Instance.ShowDataProgressStart();
        }
        else
        {
            GetDataAllItem();
            ShowItem();
            InProgressDailyQuest.Instance.ShowDataProgressStart();
        }
    }

    //	--------------------------------------------------

    void GetDataAllItem()
    {
        for (int i = 0; i < listAllItemDaily.Count; i++)
        {
            listAllItemDaily[i].GetComponent<ItemDailyQuest>().GetData();
        }
    }


    public bool SetShowGlowBtnDaily()
    {
        bool showGlow = false;
        for (int i = 1; i <= 5; i++)
        {
            if (i != 4 && CacheGame.GetTypeShowGiftBoxDailyQuest(i) == 2)
            {
                showGlow = true;
                break;
            }
        }

        return showGlow;
    }


    public void ClearDataListItemRandom()
    {
        for (int i = 0; i < listAllItemDaily.Count; i++)
        {
            string nameItem = listAllItemDaily[i].GetComponent<ItemDailyQuest>().typeItemDailyQuest.ToString();
            CacheGame.SetItemDailyQuestChooseShow(nameItem, 0);
            CacheGame.SetCompleteItemDailyQuest(nameItem, 0);
            CacheGame.SetNumberDailyQuestAchieved(nameItem, 0);
        }



        //set type sh0w cua? ca? 5 box gift trong Progress ve` 0
        for (int i = 1; i <= 5; i++)
        {
            CacheGame.SetTypeShowGiftBoxDailyQuest(i, 0);
        }
    }



    void ShowItem()
    {

        //------get list cac' item se show len
        listItemShow.Clear();
        for (int i = 0; i < listAllItemDaily.Count; i++)
        {
            if (CacheGame.GetItemDailyQuestChooseShow(listAllItemDaily[i].GetComponent<ItemDailyQuest>().typeItemDailyQuest.ToString()) == 1)
            {
                listItemShow.Add(listAllItemDaily[i]);
                listAllItemDaily[i].SetActive(true);
            }
            else
            {
                listAllItemDaily[i].SetActive(false);
            }
        }
        //------sap xep cac item theo ti le hoan` thanh 
        GameObject trungGian;
        for (int i = 0; i < listItemShow.Count; i++)
        {
            for (int j = i + 1; j < listItemShow.Count; j++)
            {
                if (listItemShow[j].GetComponent<ItemDailyQuest>().valueScrollBarSapXep > listItemShow[i].GetComponent<ItemDailyQuest>().valueScrollBarSapXep)
                {
                    //cach trao doi gia tri

                    trungGian = listItemShow[i];
                    listItemShow[i] = listItemShow[j];
                    listItemShow[j] = trungGian;

                }
            }
        }

        //------set toa do cho cac item duoc show va show value scrollBar len . . Dong thoi` lay' luon so' luong Nhiem vu da~ hoan` than`h
        float posItem = 0;
        InProgressDailyQuest.Instance.numQuestComplete = 0;
        for (int i = 0; i < listItemShow.Count; i++)
        {
            listItemShow[i].GetComponent<ItemDailyQuest>().ShowDataInFillBar();
            listItemShow[i].transform.localPosition = new Vector3(0, posItem, 0);
            posItem -= 120f;

            //-----
            //Lay so luong Item da hoan thanh
            if (listItemShow[i].GetComponent<ItemDailyQuest>().completeQuest)
            {
                InProgressDailyQuest.Instance.numQuestComplete++;
            }
        }
        //InProgressDailyQuest.Instance.numQuestComplete = 5;
    }



    public void RandomItemShowDaily()
    {
        int indexItemHard;
        int indexItemEasy;
        bool chooseItem;
        ItemDailyQuest itemDailyQuestIns;
        int levelInItem;
        //
        listLevelItemShow.Clear();
        //
        //Random những item ở mức khó 

        for (int i = 0; i < 2; i++)
        {
            while (true)
            {
                chooseItem = true;
                indexItemHard = Random.Range(0, listItemHard.Count);
                itemDailyQuestIns = listItemHard[indexItemHard].GetComponent<ItemDailyQuest>();
                //
                levelInItem = itemDailyQuestIns.levelQuest;

                if (listLevelItemShow.Count > 0)
                {
                    for (int indexListLevel = 0; indexListLevel < listLevelItemShow.Count; indexListLevel++)
                    {
                        if (levelInItem == listLevelItemShow[indexListLevel])
                        {
                            chooseItem = false;
                        }
                    }
                }
                if (chooseItem)
                {
                    listLevelItemShow.Add(levelInItem);
                    CacheGame.SetItemDailyQuestChooseShow(itemDailyQuestIns.typeItemDailyQuest.ToString(), 1);
                    break;
                }
            }
        }


        //Random nhung iten o muc' Easy . Luu y' so' luong. item easy phai? du?  va` tru` di so' luong item  hard da~ chon van phai lon' hon 3

        for (int i = 0; i < 3; i++)
        {
            while (true)
            {
                chooseItem = true;
                indexItemEasy = Random.Range(0, listItemEasy.Count);
                itemDailyQuestIns = listItemEasy[indexItemEasy].GetComponent<ItemDailyQuest>();
                //
                levelInItem = itemDailyQuestIns.levelQuest;

                if (listLevelItemShow.Count > 0)
                {
                    for (int indexListLevel = 0; indexListLevel < listLevelItemShow.Count; indexListLevel++)
                    {
                        if (levelInItem == listLevelItemShow[indexListLevel])
                        {
                            chooseItem = false;
                        }
                    }
                }
                // max level phải lớn hơn 21 , đồng thời ngày hôm đó chưa share facebook thì mới chọn
                if (itemDailyQuestIns.typeItemDailyQuest == GameContext.TypeItemDailyQuest.ShareFacebook && (CacheGame.GetMaxLevel3Difficult() < 14 || CachePvp.dateTime.Subtract(MinhCacheGame.GetFacebookShareDate()).TotalDays < 1 || CacheFireBase.GetUseRateInQuest != 1 || GameContext.IS_CHINA_VERSION))
                {
                    Debug.LogError("Khong chọn TypeItemDailyQuest.ShareFacebook");
                    chooseItem = false;
                }
                //max level phải lớn hơn 21, đồng thời chưa rate game thì mới được chọn
                if (itemDailyQuestIns.typeItemDailyQuest == GameContext.TypeItemDailyQuest.RateGame && (CacheGame.GetMaxLevel3Difficult() < 14 || MinhCacheGame.IsAlreadyRate() || CacheFireBase.GetUseRateInQuest != 1))
                {
                    //    if (itemDailyQuestIns.typeItemDailyQuest == GameContext.TypeItemDailyQuest.RateGame)
                    //{
                    Debug.LogError("Khong chọn TypeItemDailyQuest.RateGame");
                    chooseItem = false;
                }

                if (chooseItem)
                {
                    listLevelItemShow.Add(levelInItem);
                    CacheGame.SetItemDailyQuestChooseShow(itemDailyQuestIns.typeItemDailyQuest.ToString(), 1);
                    break;
                }
            }
        }
    }



    //------------------
    public void RateButtonInQuest()
    {
        SoundManager.PlayClickButton();
        //Application.OpenURL ("market://details?id=invaders.os.galaxy.space.shooter.attack.classic");
        Debug.LogError("RateButtonInQuest");
#if UNITY_ANDROID
        Application.OpenURL("market://details?id=invaders.os.galaxy.space.shooter.attack.classic");
#elif UNITY_IPHONE
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1337514468");
#endif
        FirebaseLogSpaceWar.LogClickButton("Rate");
        MinhCacheGame.SetAlreadyRate();
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.RateGame.ToString(), 2);

        GetDataAllItem();
        ShowItem();
        InProgressDailyQuest.Instance.ShowDataProgressStart();
    }

}
