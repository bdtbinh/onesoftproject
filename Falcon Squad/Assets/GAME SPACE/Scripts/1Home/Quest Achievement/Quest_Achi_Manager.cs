﻿using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase.Analytics;
using com.ootii.Messages;

public enum TypePanelShowPopup
{
    DailyQuest,
    Achi

}
public class Quest_Achi_Manager : Singleton<Quest_Achi_Manager>
{
    public GameObject Quest_Achi_InviObj;
    //
    public GameObject panelDailyQuestObj;
    public GameObject panelAchiObj;

    //
    public PopupAchievements scripAchievements;
    public PopupDailyQuest scripDailyQuest;
    //
    public GameObject glowSelectBtnDailyQuest;
    public GameObject glowSelectBtnAchi;

    // glow chẤm than hiện lên khi có quest mới có thể nhận quà
    //public GameObject glowBtnQuest_Achi;
    public GameObject glowBtnAchiInPopup;
    public GameObject glowBtnQuestInPopup;


    void Start()
    {
        //if (SceneManager.GetActiveScene().name == "Home" || SceneManager.GetActiveScene().name == "SelectLevel")
        //{
        //    this.Delay(0.5f, () =>
        //{
        //    scripDailyQuest.SetDataStartPopup();
        //    scripAchievements.ShowDataStartAllItem();
        //    ShowGlowCompleteQuest();
        //});
        //}
        //else
        //{
        //    gameObject.SetActive(false);
        //}

        this.Delay(0.5f, () =>
        {
            scripDailyQuest.SetDataStartPopup();
            scripAchievements.ShowDataStartAllItem();
            ShowGlowCompleteQuest();
        });
    }

    public void ChangeOpenPanel()
    {
        Quest_Achi_InviObj.SetActive(true);
        ShowPanelInPopup(TypePanelShowPopup.DailyQuest);
    }

    //----------------------------------------------------


    public void ShowGlowCompleteQuest()
    {
        bool showGlowBtnAchi = PopupAchievements.Instance.ShowGlowBtnAchi();
        bool showGlowBtnDaily = PopupDailyQuest.Instance.SetShowGlowBtnDaily();

        if (!showGlowBtnAchi && !showGlowBtnDaily)
        {
            PanelBtnQA.Instance.glowBtnQA.SetActive(false);
            
        }
        else
        {
            PanelBtnQA.Instance.glowBtnQA.SetActive(true);
        }
        //
        if (!showGlowBtnAchi)
        {
            glowBtnAchiInPopup.SetActive(false);
        }
        else
        {
            glowBtnAchiInPopup.SetActive(true);
        }
        //
        if (!showGlowBtnDaily)
        {
            glowBtnQuestInPopup.SetActive(false);
        }
        else
        {
            glowBtnQuestInPopup.SetActive(true);
        }
    }
   


    //------------------------------------------------

    public void Achievement_Button()
    {
        SoundManager.PlayClickButton();
        ShowPanelInPopup(TypePanelShowPopup.Achi);
        //FirebaseAnalytics.LogEvent("Click", new Parameter("Button", "Achievement_InPopup"));
        //FirebaseLogSpaceWar.LogClickButton("Achievement_InPopup");
    }

    public void DailyQuest_Button()
    {
        SoundManager.PlayClickButton();
        ShowPanelInPopup(TypePanelShowPopup.DailyQuest);
        //FirebaseAnalytics.LogEvent("Click", new Parameter("Button", "DailyQuest_InPopup"));
        //FirebaseLogSpaceWar.LogClickButton("DailyQuest_InPopup");
    }


    public void ShowPanelInPopup(TypePanelShowPopup typeShow = TypePanelShowPopup.DailyQuest)
    {
        switch (typeShow)
        {
            case TypePanelShowPopup.DailyQuest:

                panelDailyQuestObj.SetActive(true);
                panelAchiObj.SetActive(false);

                PopupDailyQuest.Instance.SetDataStartPopup();

                glowSelectBtnDailyQuest.SetActive(true);
                glowSelectBtnAchi.SetActive(false);

                break;
            case TypePanelShowPopup.Achi:

                panelDailyQuestObj.SetActive(false);
                panelAchiObj.SetActive(true);

                PopupAchievements.Instance.ShowDataStartAllItem();

                glowSelectBtnDailyQuest.SetActive(false);
                glowSelectBtnAchi.SetActive(true);


                break;

        }
    }



    public void Btn_Back()
    {
        SoundManager.PlayClickButton();
        Quest_Achi_InviObj.SetActive(false);
        ShowGlowCompleteQuest();
        MessageDispatcher.SendMessage(this, EventID.ON_DISABLE_SHOW_RECEIVE_ITEM_FX, this, 0);
        PopupManager.Instance.ShowChatWorldPopup();
    }
}
