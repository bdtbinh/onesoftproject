﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TCore;

public class PopupAchievements : SkyGameKit.Singleton<PopupAchievements>
{
    public GameObject[] listItem_Obj;

    //----------
    int indexShowStart = 0;



    public void ShowDataStartAllItem()
    {
        //CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillBoss.ToString(), 1111101);
        //CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.KillEnemy.ToString(), 1111001);
        //CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetUpgrade.ToString(), 1111001);

        //CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.CollectItem.ToString(), 1111001);

        //CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetGold.ToString(), 1111001);

        //CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.UseTimeSphere.ToString(), 1111001);

        ShowGlowBtnAchi();
        SapXepItem();

    }


    //---------------
    int indexCheckGlow = 0;

    public bool ShowGlowBtnAchi()
    {
        //ShowData
        for (indexShowStart = 0; indexShowStart < listItem_Obj.Length; indexShowStart++)
        {
            listItem_Obj[indexShowStart].GetComponent<ItemAchievements>().ShowDataItemStart();
        }
        //

        bool showGlow = false;
        for (indexCheckGlow = 0; indexCheckGlow < listItem_Obj.Length; indexCheckGlow++)
        {
            if (listItem_Obj[indexCheckGlow].GetComponent<ItemAchievements>().valueSapXepItem >= 1)
            {
                showGlow = true;
                break;
            }
        }
        return showGlow;

    }

    //-------------------------------------------------------------------------

    int i, j;
    GameObject trungGian;
    public void SapXepItem()
    {
        for (i = 0; i < listItem_Obj.Length; i++)
        {
            for (j = i + 1; j < listItem_Obj.Length; j++)
            {
                if (listItem_Obj[j].GetComponent<ItemAchievements>().valueSapXepItem > listItem_Obj[i].GetComponent<ItemAchievements>().valueSapXepItem)
                {
                    //cach trao doi gia tri

                    trungGian = listItem_Obj[i];
                    listItem_Obj[i] = listItem_Obj[j];
                    listItem_Obj[j] = trungGian;
                }
            }
        }

        float posItem = 0;
        for (i = 0; i < listItem_Obj.Length; i++)
        {
            listItem_Obj[i].transform.localPosition = new Vector3(0, posItem, 0);
            posItem -= 153f;

        }
    }


}
