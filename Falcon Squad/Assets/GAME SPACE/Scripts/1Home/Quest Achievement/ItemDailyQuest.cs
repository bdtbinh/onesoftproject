﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using I2.Loc;

public class ItemDailyQuest : MonoBehaviour
{

    [Title("Dung de sap xep thu tu item", bold: true, horizontalLine: true)]
    [DisplayAsString]
    float valueScrollBar;
    public float valueScrollBarSapXep;
    //
    [Title("Key To Get Data", bold: true, horizontalLine: true)]
    public GameContext.TypeItemDailyQuest typeItemDailyQuest;
    //public string typeItemAchievements.ToString();



    [Title("Obj In Item", bold: true, horizontalLine: true)]

    //text dien` trong thanh  scroll bar
    public UILabel lFillBar;
    public UISprite sFillBar;

    //


    [Title("Data", bold: true, horizontalLine: true)]
    //
    [DisplayAsString]
    //  số lượng tối đa phải hoàn thành trong 1 nhiệm vụ
    public int valueMaxInMission;
    [DisplayAsString]
    //  số lượng đã hoàn thành trong 1 nhiệm vụ
    public int valueCompleteInMission;
    [DisplayAsString]
    // Loại Quest dễ hay khó
    public string typeQuest;
    [DisplayAsString]
    // để phân biệt các quest giống nhau nhưng khác mức độ khó  dễ
    public int levelQuest;
    [DisplayAsString]
    public bool completeQuest;


    void Start()
    {
        //		ConfigLoader.Load ();

        //		GetData ();
        //		ShowDataInFillBar ();
    }



    public void GetData()
    {
        if (CacheGame.GetCompleteItemDailyQuest(typeItemDailyQuest.ToString()) == 1)
        {
            completeQuest = true;
        }
        else
        {
            completeQuest = false;
        }
        //
        valueCompleteInMission = CacheGame.GetNumberDailyQuestAchieved(typeItemDailyQuest.ToString());

        DailyQuestConfig dailyQuestConfig = DailyQuestConfig.Get(typeItemDailyQuest.ToString());
        valueMaxInMission = (int)(dailyQuestConfig.valueQuest);

        typeQuest = dailyQuestConfig.typeQuest;
        levelQuest = (int)(dailyQuestConfig.levelQuest);
        //
        valueScrollBar = (float)valueCompleteInMission / (float)valueMaxInMission;

        if (valueScrollBar >= 1)
        {
            CacheGame.SetCompleteItemDailyQuest(typeItemDailyQuest.ToString(), 1);
            completeQuest = true;
        }
        if (!completeQuest)
        {
            valueScrollBarSapXep = valueScrollBar;
        }
        else
        {
            valueScrollBarSapXep = -1f;
        }
    }

    public void ShowDataInFillBar()
    {
        if (valueScrollBar > 1)
        {
            valueScrollBar = 1;
        }
        sFillBar.fillAmount = valueScrollBar;
        ///----
        if (!completeQuest)
        {
            lFillBar.text = GameContext.FormatNumber(valueCompleteInMission) + "/" + GameContext.FormatNumber(valueMaxInMission);
        }
        else
        {
            lFillBar.text = "" + ScriptLocalization.completed;
        }
    }
}
