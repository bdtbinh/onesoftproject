﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
using TCore;
using Firebase.Analytics;
using I2.Loc;
using SkyGameKit;
using com.ootii.Messages;

public class InProgressDailyQuest : SkyGameKit.Singleton<InProgressDailyQuest>
{
    [SerializeField]
    private ShowReceiveItemsFX showReceiveItemsFx;

    //
    public Transform listLevelProgress;
    public List<UISprite> listIconProgressLv;
    public GameObject objBtnBox1, objBtnBox2, objBtnBox3, objBtnBox5;
    public UISprite sprBtnBox1, sprBtnBox2, sprBtnBox3, sprBtnBox5;
    public Color colorGiftBoxHide;

    //

    public List<GameObject> listObjItemGift;
    public List<UILabel> listLableNUmGift;

    public GameObject sGlowClamp;
    public GameObject objBtnClamp;


    //------
    [DisplayAsString]
    public int indexBoxSelect = 0;


    [DisplayAsString]
    //typeGift hien thi o reward
    public List<string> listTypeGift;
    [DisplayAsString]
    //NumGift hien thi o reward
    public List<int> listNumGift;
    [DisplayAsString]
    // so' luong cac nhiem vu da~ hoan` thanh`
    public int numQuestComplete;

    //
    //	[DisplayAsString]
    // Khi click vao` 1 hop qua` thi` se lay ra data cua hop qua do'

    private int indexBoxGiftClick1;

    GameObject objBtnBoxGiftClick;
    UISprite sprBtnBoxGiftClick;





    //void Start()
    //{
    //		ConfigLoader.Load ();
    //		GetDataGiftInBox (2);
    //		ShowGiftInReward ();
    //		PlayerPrefs.DeleteAll ();
    //}



    public void ShowDataProgressStart()
    {
        ShowFillBar();
        SetTypeShowGiftBoxStart();

        ShowGiftBox(1, objBtnBox1, sprBtnBox1);
        ShowGiftBox(2, objBtnBox2, sprBtnBox2);
        ShowGiftBox(3, objBtnBox3, sprBtnBox3);
        ShowGiftBox(5, objBtnBox5, sprBtnBox5);

        //
        //		listTypeGift.Clear ();
        //		ShowGiftInReward ();

        //Show
        ShowGiftInRewardWhenStart();
    }

    public void ShowFillBar()
    {
        int indexChild = 0;
        foreach (Transform dotChild in listLevelProgress)
        {


            if (indexChild < numQuestComplete)
            {
                dotChild.GetComponent<UISprite>().spriteName = "bar_daily_quest_filled";

                if (indexChild == 3)
                {
                    listIconProgressLv[indexChild].spriteName = "icon_progress4_gold";
                }
                else
                {
                    listIconProgressLv[indexChild].spriteName = "icon_progress_gold";
                }

            }
            else
            {
                dotChild.GetComponent<UISprite>().spriteName = "bar_daily_quest_empty";

                if (indexChild == 3)
                {
                    listIconProgressLv[indexChild].spriteName = "icon_progress4_blue";
                }
                else
                {
                    listIconProgressLv[indexChild].spriteName = "icon_progress_blue";
                }
            }

            indexChild++;
        }

    }




    void SetTypeShowGiftBoxStart()
    {


        // de lay' ra hop. qua` dau` tien o reward
        for (int i = 1; i <= numQuestComplete; i++)
        {

            if (CacheGame.GetTypeShowGiftBoxDailyQuest(i) != 3)
            {
                CacheGame.SetTypeShowGiftBoxDailyQuest(i, 2);
            }
        }
    }

    void ShowGiftBox(int indexBox, GameObject objBtnBoxShow, UISprite sprBtnBoxShow)
    {
        //typeShow=1  :box chua duoc phep' nhan qua  ; typeShow=2  :box da san sang nhan qua  ; typeShow=3  :box da nhan qua xong
        int typeShow = CacheGame.GetTypeShowGiftBoxDailyQuest(indexBox);

        if (typeShow == 2)
        {
            sprBtnBoxShow.spriteName = "icon_box" + indexBox;
            sprBtnBoxShow.color = Color.white;

            objBtnBoxShow.GetComponent<BoxCollider>().enabled = true;
        }
        else if (typeShow == 3)
        {
            sprBtnBoxShow.spriteName = "icon_box" + indexBox;
            sprBtnBoxShow.color = colorGiftBoxHide;

            objBtnBoxShow.GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            sprBtnBoxShow.spriteName = "icon_box" + indexBox + "_x";
            sprBtnBoxShow.color = Color.white;

            objBtnBoxShow.GetComponent<BoxCollider>().enabled = true;
        }

    }


    void GetDataGiftInBox(int indexBoxSelect)
    {

        listTypeGift.Clear();
        listNumGift.Clear();
        DailyQuestGiftConfig dailyQuestGiftConfig = DailyQuestGiftConfig.Get("Level" + indexBoxSelect + "World" + CacheGame.GetCurrentWorld());
        for (int i = 0; i < dailyQuestGiftConfig.typeGiftList.Length; i++)
        {
            listTypeGift.Add(dailyQuestGiftConfig.typeGiftList[i]);
            listNumGift.Add((int)(dailyQuestGiftConfig.numGiftList[i]));
        }
    }


    void ShowGiftInReward()
    {

        for (int i = 0; i < listObjItemGift.Count; i++)
        {
            if (i < listTypeGift.Count)
            {
                //---
                listObjItemGift[i].SetActive(true);
                //---
                SetTypeGiftInItemReWard(listTypeGift[i], listObjItemGift[i].GetComponent<UISprite>());
                listLableNUmGift[i].text = "" + GameContext.FormatNumber(listNumGift[i]);

                if (listNumGift[i] < 0)
                {
                    listLableNUmGift[i].gameObject.SetActive(false);
                }
                else
                {
                    listLableNUmGift[i].gameObject.SetActive(true);
                }
                //---
            }
            else
            {
                listObjItemGift[i].SetActive(false);
            }
        }
    }


    //goi hàm này khi start popup lên hoắc sau khi slick clamp
    void ShowGiftInRewardWhenStart()
    {
        bool getGift = false;
        for (int i = 1; i <= 5; i++)
        {
            if (i != 4 && CacheGame.GetTypeShowGiftBoxDailyQuest(i) != 3)
            {
                switch (i)
                {
                    case 1:
                        Btn_BoxGift1();
                        getGift = true;
                        break;
                    case 2:
                        Btn_BoxGift2();
                        getGift = true;
                        break;
                    case 3:
                        Btn_BoxGift3();
                        getGift = true;
                        break;
                    case 5:
                        Btn_BoxGift5();
                        getGift = true;
                        break;
                }
                break;
            }
        }
        if (!getGift)
        {
            listTypeGift.Clear();
            listNumGift.Clear();
            ShowGiftInReward();
            sGlowClamp.SetActive(false);
        }
    }



    void SetTypeGiftInItemReWard(string typeItem, UISprite spriteItemGift)
    {
        switch (typeItem)
        {
            case "Gold":
                spriteItemGift.spriteName = "icon_coin";
                break;
            case "PowerUp":
                spriteItemGift.spriteName = "icon_dailyquest_powerup";
                break;
            case "EMP":
                spriteItemGift.spriteName = "icon_dailyquest_time";
                break;
            case "Life":
                spriteItemGift.spriteName = "icon_dailyquest_life";
                break;
            case "Gem":
                spriteItemGift.spriteName = "icon_daily_quest_gem";
                break;
            case "BoxCard1":
                spriteItemGift.spriteName = "card_R";
                break;
            case "BoxCard2":
                spriteItemGift.spriteName = "card_R";
                break;
            case "BoxCard3":
                spriteItemGift.spriteName = "card_R";
                break;
            case "BoxCard4":
                spriteItemGift.spriteName = "card_R";
                break;
        }
    }




    //--------------------------------------------Button------------------------------
    public void Btn_BoxGift1()
    {
        GetDataGiftInBox(1);
        //
        indexBoxGiftClick1 = 1;
        objBtnBoxGiftClick = objBtnBox1;
        sprBtnBoxGiftClick = sprBtnBox1;
        //
        ShowGiftInReward();
        ShowData_BtnClamp(1);
    }
    //---
    public void Btn_BoxGift2()
    {
        SoundManager.PlayClickButton();
        GetDataGiftInBox(2);
        //
        indexBoxGiftClick1 = 2;
        objBtnBoxGiftClick = objBtnBox2;
        sprBtnBoxGiftClick = sprBtnBox2;
        //
        ShowGiftInReward();
        ShowData_BtnClamp(2);
    }
    //---
    public void Btn_BoxGift3()
    {
        SoundManager.PlayClickButton();
        GetDataGiftInBox(3);
        //
        indexBoxGiftClick1 = 3;
        objBtnBoxGiftClick = objBtnBox3;
        sprBtnBoxGiftClick = sprBtnBox3;
        //
        ShowGiftInReward();
        ShowData_BtnClamp(3);
    }
    //---
    public void Btn_BoxGift5()
    {
        SoundManager.PlayClickButton();
        GetDataGiftInBox(5);
        //
        indexBoxGiftClick1 = 5;
        objBtnBoxGiftClick = objBtnBox5;
        sprBtnBoxGiftClick = sprBtnBox5;
        //
        ShowGiftInReward();
        ShowData_BtnClamp(5);
    }




    //------------------------------Button Clamp--------------------

    void ShowData_BtnClamp(int indexBoxGiftClamp)
    {

        if (CacheGame.GetTypeShowGiftBoxDailyQuest(indexBoxGiftClamp) == 2)
        {
            sGlowClamp.SetActive(true);
        }
        else
        {
            sGlowClamp.SetActive(false);
        }
    }

    //string listTextGiftToast;
    //float timeshowToast;
    public void Btn_Clamp()
    {
        if (PopupManagerCuong.Instance.readyShowNotifi)
        {
            SoundManager.PlayClickButton();
            if (CacheGame.GetTypeShowGiftBoxDailyQuest(indexBoxGiftClick1) == 2)
            {
                MinhCacheGame.UpdateIsInEvent(GameContext.EVENT_SUMMER_HOLIDAY_2019);

                if (MinhCacheGame.IsInEvent(GameContext.EVENT_SUMMER_HOLIDAY_2019))
                {
                    int num = MinhCacheGame.GetEventMissionCollectedToShow(2, 50);
                    if (num > 0)
                    {
                        MinhCacheGame.AddEventMissionCollected(2, num);

                        this.Delay(2f, () =>
                        {
                            showReceiveItemsFx.PlayFX(num);
                        });
                    }
                }
                //listTextGiftToast = "";
                //timeshowToast = 1.5f;
                PopupManagerCuong.Instance.ShowItemRewardPopup();
                PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
                popupItemRewardIns.ShowListItem(listTypeGift.Count);

                for (int i = 0; i < listTypeGift.Count; i++)
                {
                    AddGiftToPlayer(listTypeGift[i], listNumGift[i]);
                    //timeshowToast += 0.1f;
                    popupItemRewardIns.ShowOneItem(i + 1, typeItemPopupItemReward, numItemPopupItemReward);
                }


                //PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_HaveReceived + listTextGiftToast, true, timeshowToast);

                CacheGame.SetTypeShowGiftBoxDailyQuest(indexBoxGiftClick1, 3);
                //
                ShowGiftBox(indexBoxGiftClick1, objBtnBoxGiftClick, sprBtnBoxGiftClick);
                //
                ShowGiftInRewardWhenStart();
                //
                ShowData_BtnClamp(indexBoxGiftClick1);
                Quest_Achi_Manager.Instance.ShowGlowCompleteQuest();
                //PvpUtil.SendUpdatePlayer();
                PvpUtil.SendUpdatePlayer("Btn_Clamp_DailyQuest");
            }
            else
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_DailyQuest_PleaseComplete, false);
            }
        }

    }

    GameContext.TypeItemInPopupItemReward typeItemPopupItemReward;
    int numItemPopupItemReward;

    void AddGiftToPlayer(string giftType, int numGift)
    {
        numGift = numGift * (1 + VipBonusValue.rewardsDailyTask);
        numItemPopupItemReward = numGift;
        switch (giftType)
        {
            case "Gold":
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddCoinTop(numGift, FirebaseLogSpaceWar.DailyQuest_why);
                }
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Gold;
                break;
            case "Gem":
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddGemTop(numGift, FirebaseLogSpaceWar.DailyQuest_why);
                }
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Gem;
                break;
            case "PowerUp":
                CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + numGift);
                SoundManager.PlayClickButton();
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.PowerUp;
                break;
            case "EMP":
                CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + numGift);
                SoundManager.PlayClickButton();
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.EMP;
                break;
            case "Life":
                CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + numGift);
                SoundManager.PlayClickButton();
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Life;
                break;
            case "BoxCard1":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.DailyQuest_why, "BoxCard1", (1 + VipBonusValue.rewardsDailyTask));
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
            case "BoxCard2":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.DailyQuest_why, "BoxCard2", (1 + VipBonusValue.rewardsDailyTask));
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
            case "BoxCard3":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.DailyQuest_why, "BoxCard3", (1 + VipBonusValue.rewardsDailyTask));
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
            case "BoxCard4":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.DailyQuest_why, "BoxCard4", (1 + VipBonusValue.rewardsDailyTask));
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
        }
    }
}




