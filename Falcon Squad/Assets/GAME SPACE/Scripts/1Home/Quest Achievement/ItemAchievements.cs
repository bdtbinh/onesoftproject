﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using TCore;
using Firebase.Analytics;
using I2.Loc;

public class ItemAchievements : MonoBehaviour
{

    [Title("Dung de sap xep thu tu item", bold: true, horizontalLine: true)]
    [DisplayAsString]
    public float valueSapXepItem;
    float valueCompleteScrollBar;
    //
    [Title("Key To Get Data", bold: true, horizontalLine: true)]
    public GameContext.TypeItemAchievements typeItemAchievements;
    //public string nameTypeItem;
    //	public string

    [Title("Obj In Item", bold: true, horizontalLine: true)]

    //text dien` trong thanh  scroll bar
    public UILabel lFillBar;
    public UISprite sFillBar;
    //text dien` dien giai? ngay tren thanh scroll bar : ex, "collect 100 enemy"
    //	public UILabel lTextNotifi;
    //parent list star
    public GameObject sItemListStars;

    public UISprite sBG;

    public UISprite sGilfInButton;
    public UILabel lNumGilfInButton;

    //
    public GameObject sGlowInButton;
    public GameObject btnReward_Obj;
    public GameObject sBgButtonFinish;
    public UILabel lReward;
    //



    [Title("Data", bold: true, horizontalLine: true)]
    //
    [DisplayAsString]
    //  so'  luong. phai hoan` thanh` trong 1 nhiem vu
    public int valueInMission;
    [DisplayAsString]
    //  so'  luong. da hoan` thanh` trong 1 nhiem vu
    public int valueCompleteInMission;
    [DisplayAsString]
    //  tong so luong cap' do nhiem vu
    public int maxLevelAchievements;
    [DisplayAsString]
    //  cap do nhiem vu hien tai
    public int currLevelAchievements;
    [DisplayAsString]
    public int numGilfInButton;
    [DisplayAsString]
    public string typeGilfInButton;
    [DisplayAsString]
    public bool completeAllLevel;
    //---
    string textNotifiStart;
    TweenAlpha sGlowBGTween;



    public void ShowDataItemStart()
    {
        //		PlayerPrefs.DeleteAll ();


        //		textNotifiStart = lTextNotifi.text;
        sGlowBGTween = sGlowInButton.GetComponent<TweenAlpha>();


        GetDataItem();

        ShowTextInItem(typeGilfInButton, numGilfInButton);
        SetFillBar();
        ShowStar();
        SetSpriteBG();
        SetGlowInButton();
        SetHideButton();
    }


    public void GetDataItem()
    {
        valueCompleteInMission = CacheGame.GetNumberAchievementAchieved(typeItemAchievements.ToString());
        //
        currLevelAchievements = (CacheGame.GetCurrentLevelAchievement(typeItemAchievements.ToString()));

        maxLevelAchievements = (int)AchievementsDataConfig.Get(typeItemAchievements.ToString() + GameContext.NUMGIFT_ACHIEVEMENTS).list_Value.Length;

        if (currLevelAchievements >= maxLevelAchievements)
        {
            //if (CacheGame.GetCompleteAllLevelAchievement(typeItemAchievements.ToString()) == 1)
            //{
            completeAllLevel = true;
        }
        else
        {
            completeAllLevel = false;
        }

        if (!completeAllLevel)
        {
            AchievementsDataConfig valueInMissionAchievement = AchievementsDataConfig.Get(typeItemAchievements.ToString() + GameContext.VALUE_ACHIEVEMENTS);

            valueInMission = int.Parse(valueInMissionAchievement.list_Value[currLevelAchievements]);

            //typeGift
            AchievementsDataConfig typeGiftAchievement = AchievementsDataConfig.Get(typeItemAchievements.ToString() + GameContext.TYPEGIFT_ACHIEVEMENTS);

            typeGilfInButton = typeGiftAchievement.list_Value[currLevelAchievements];

            // numGift
            AchievementsDataConfig numGiftAchievement = AchievementsDataConfig.Get(typeItemAchievements.ToString() + GameContext.NUMGIFT_ACHIEVEMENTS);

            numGilfInButton = int.Parse(numGiftAchievement.list_Value[currLevelAchievements]);

        }
        else
        {
            AchievementsDataConfig valueInMissionAchievement = AchievementsDataConfig.Get(typeItemAchievements.ToString() + GameContext.VALUE_ACHIEVEMENTS);

            valueInMission = int.Parse(valueInMissionAchievement.list_Value[currLevelAchievements - 1]);
        }
    }

    public void ShowTextInItem(string giftType, int numGift)
    {

        if (!completeAllLevel)
        {
            lFillBar.text = GameContext.FormatNumber(valueCompleteInMission) + "/" + GameContext.FormatNumber(valueInMission);

            lNumGilfInButton.text = "" + GameContext.FormatNumber(numGift);

            switch (giftType)
            {
                case "Gold":
                    sGilfInButton.spriteName = "icon_coin";
                    break;
                case "PowerUp":
                    sGilfInButton.spriteName = "icon_dailyquest_powerup";
                    break;
                case "EMP":
                    sGilfInButton.spriteName = "icon_dailyquest_time";
                    break;
                case "Life":
                    sGilfInButton.spriteName = "icon_life";
                    break;
                case "Gem":
                    sGilfInButton.spriteName = "icon_daily_quest_gem";
                    break;
            }
        }
        else
        {
            lFillBar.text = "Completed!";


        }

    }


    public void SetFillBar()
    {
        valueCompleteScrollBar = (float)valueCompleteInMission / (float)valueInMission;

        if (!completeAllLevel)
        {
            if (valueCompleteScrollBar < 1)
            {
                sFillBar.fillAmount = valueCompleteScrollBar;
                sFillBar.spriteName = "bar_achievement_fill";
            }
            else
            {
                sFillBar.fillAmount = 1f;
                sFillBar.spriteName = "bar_achievement_fill2";
            }

            valueSapXepItem = valueCompleteScrollBar;
        }
        else
        {
            sFillBar.fillAmount = 1f;
            sFillBar.spriteName = "bar_achievement_fill2";
            //
            valueSapXepItem = -1f;
        }

    }


    public void ShowStar()
    {
        int indexChild = 0;
        foreach (Transform child in sItemListStars.transform)
        {
            if (completeAllLevel)
            {
                child.GetComponent<UISprite>().spriteName = "icon_smallstar_gold";
            }
            else
            {
                if (indexChild < currLevelAchievements)
                {
                    child.GetComponent<UISprite>().spriteName = "icon_smallstar_gold";
                }
                else
                {
                    child.GetComponent<UISprite>().spriteName = "icon_smallstar_black";
                }
            }

            indexChild++;
        }
    }


    public void SetSpriteBG()
    {
        if (!completeAllLevel)
        {
            sBG.spriteName = "achie_itemBG_" + (currLevelAchievements + 1);
        }
        else
        {
            sBG.spriteName = "achie_itemBG_9";
        }
    }

    public void SetGlowInButton()
    {
        if (!completeAllLevel)
        {
            if (valueCompleteInMission >= valueInMission)
            {
                sGlowInButton.SetActive(true);
            }
            else
            {
                sGlowInButton.SetActive(false);
            }
        }
        else
        {
            sGlowInButton.SetActive(false);
        }

    }



    public void SetHideButton()
    {
        if (completeAllLevel)
        {
            btnReward_Obj.SetActive(false);
            sBgButtonFinish.SetActive(true);
            lReward.gameObject.SetActive(false);
        }

    }



    public void Btn_Reward()
    {
        if (PopupManagerCuong.Instance.readyShowNotifi)
        {
            SoundManager.PlayClickButton();
            if (valueCompleteInMission >= valueInMission)
            {
                GetGiftInButton(typeGilfInButton, numGilfInButton);
                PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + numGilfInButton + " " + typeGilfInButton), true);
                //------
                CacheGame.SetCurrentLevelAchievement(typeItemAchievements.ToString(), CacheGame.GetCurrentLevelAchievement(typeItemAchievements.ToString()) + 1);
                if (CacheGame.GetCurrentLevelAchievement(typeItemAchievements.ToString()) >= maxLevelAchievements)
                {
                    completeAllLevel = true;
                    CacheGame.SetCompleteAllLevelAchievement(typeItemAchievements.ToString(), 1);

                    SetHideButton();
                }
                GetDataItem();
                ShowTextInItem(typeGilfInButton, numGilfInButton);
                SetFillBar();
                ShowStar();
                SetSpriteBG();
                SetGlowInButton();
                Quest_Achi_Manager.Instance.ShowGlowCompleteQuest();
                //
                //PvpUtil.SendUpdatePlayer();
                PvpUtil.SendUpdatePlayer("Reward_Achievement");
            }
            else
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_Achi_PleaseComplete, false);
            }
        }
    }


    void GetGiftInButton(string giftType, int numGift)
    {

        //if (typeGilfInButton == "Gold")
        //{
        //    DontDestroyManager.Instance.AddCoinTop(numGilfInButton);
        //}
        switch (giftType)
        {
            case "Gold":
                //DontDestroyManager.Instance.AddCoinTop(numGift);
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddCoinTop(numGift, FirebaseLogSpaceWar.Achievement_why);
                }
                break;
            case "PowerUp":
                CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + numGift);
                SoundManager.PlayClickButton();
                break;
            case "EMP":
                CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + numGift);
                SoundManager.PlayClickButton();
                break;
            case "Life":
                CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + numGift);
                SoundManager.PlayClickButton();
                break;
            case "Gem":
                //DontDestroyManager.Instance.AddGemTop(numGift);
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddGemTop(numGift, FirebaseLogSpaceWar.Achievement_why);
                }
                break;
        }
    }
}
