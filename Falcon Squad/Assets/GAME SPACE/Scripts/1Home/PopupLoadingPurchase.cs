﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupLoadingPurchase : MonoBehaviour
{

    void OnEnable()
    {
        StartCoroutine("WaitForHide");
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }


    IEnumerator WaitForHide()
    {
        yield return new WaitForSecondsRealtime(10f);
        PopupManagerCuong.Instance.HidePopupLoadingPurchase();
    }
}
