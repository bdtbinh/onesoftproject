﻿using UnityEngine;
using System.Collections;
using TCore;

public class ExitPopup : MonoBehaviour
{

    public void ButtonYes()
    {
        SoundManager.PlayClickButton();
        Application.Quit();
    }

    public void ButtonNo()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.HideExitGamePopup();
    }

}
