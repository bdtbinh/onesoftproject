﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using SgLib;
using System;
using EasyMobile;
using TCore;
using SkyGameKit;
using DG.Tweening;

public class WatchAdsBuyPack : MonoBehaviour
{
    public enum TypePackWatchAds
    {
        StarterPack,
        PremiumPack,
        VipPack
    }

    public TypePackWatchAds typePackWatchAds;
    //nếu bật panel  watch ads thì popup dài nên phải dùng script scale
    public ScalePopup scalePopupScript;

    public UISprite sBtnAds;


    public GameObject packObj;

    public int numWatchAdsNeed;
    //public int numAdsw ;

    public UILabel lFillBar;
    public UISprite sFillBar;

    public UILabel lRemainToday;

    void Start()
    {
        scalePopupScript.enabled = true;
        ShowDataStart();
    }


    public void ShowDataStart()
    {

        lFillBar.text = CacheGame.GetNumberWatchAdsToGetPack() + "/" + numWatchAdsNeed;
        sFillBar.fillAmount = (float)CacheGame.GetNumberWatchAdsToGetPack() / (float)numWatchAdsNeed;

        lRemainToday.text = "Chance remain today: " + CacheGame.GetRemainWatchAdsToGetPackToday();


        if (CacheGame.GetRemainWatchAdsToGetPackToday() > 0)
        {
            sBtnAds.spriteName = "btn_video_ads";
        }
        else
        {
            sBtnAds.spriteName = "btn_free_d";
        }

    }


    public void ButtonWatchAds()
    {


        if (CacheGame.GetRemainWatchAdsToGetPackToday() > 0)
        {
            //CallBackFinishVideoAds();
            if (OsAdsManager.Instance.isRewardedVideoAvailable())
            {
                OsAdsManager.Instance.AddRewardedListenner(CallBackFinishVideoAds);
                OsAdsManager.Instance.AddClosedListenner(CallBackClosedVideoAds);
                //
                OsAdsManager.Instance.ShowRewardedAds(OsAdsManager.LocationClickVideo.btn_video_BuyPack);
                FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdmob");
                FirebaseLogSpaceWar.LogOtherEvent("Ad_ShowAdsInPack");
            }
            else
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_ads_not_available, false, 1.5f);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_videoads_tomorrow, false, 1.6f);
        }




    }
    void CallBackFinishVideoAds()
    {


        LeanTween.delayedCall(0.38f, () =>
        {
            FirebaseLogSpaceWar.LogOtherEvent("Ad_CompAdsInPack");
            CacheGame.SetNumberWatchAdsToGetPack(CacheGame.GetNumberWatchAdsToGetPack() + 1);

            CacheGame.SetRemainWatchAdsToGetPackToday(CacheGame.GetRemainWatchAdsToGetPackToday() - 1);
            ShowDataStart();

            if (typePackWatchAds == TypePackWatchAds.StarterPack)
            {
                if (CacheGame.GetNumberWatchAdsToGetPack() >= numWatchAdsNeed)
                {
                    FirebaseLogSpaceWar.LogOtherEvent("Get_StarterPack_Ads");
                    CacheGame.SetNumberWatchAdsToGetPack(0);
                    PopupStarterPack.Instance.CallBackBuyPack();
                    gameObject.SetActive(false);
                }
            }
            else if (typePackWatchAds == TypePackWatchAds.PremiumPack)
            {
                if (CacheGame.GetNumberWatchAdsToGetPack() >= numWatchAdsNeed)
                {
                    FirebaseLogSpaceWar.LogOtherEvent("Get_PremiumPack_Ads");
                    CacheGame.SetNumberWatchAdsToGetPack(0);
                    PopupPremiumPack.Instance.CallBackBuyPack();
                    gameObject.SetActive(false);
                }
            }
            else
            {
                if (CacheGame.GetNumberWatchAdsToGetPack() >= numWatchAdsNeed)
                {
                    FirebaseLogSpaceWar.LogOtherEvent("Get_VipPack_Ads");
                    CacheGame.SetNumberWatchAdsToGetPack(0);
                    PopupVipPack.Instance.CallBackBuyPack();
                    gameObject.SetActive(false);
                }
            }

        }).setIgnoreTimeScale(true);



    }



    void CallBackClosedVideoAds()
    {

    }

}
