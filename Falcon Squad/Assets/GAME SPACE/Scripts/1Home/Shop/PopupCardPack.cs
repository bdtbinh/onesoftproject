﻿using System;
using System.Collections;
using System.Collections.Generic;
using EasyMobile;
using I2.Loc;
using TCore;
using UnityEngine;

public class PopupCardPack : MonoBehaviour
{
    const string CACHE_TYPE_PRICE = "TypePricePopupCardPack";
    const string CACHE_INDEX_AIRCRAFT = "IndexAircaftPopupCardPack";

    public int numCardGet5Usd;
    public int numCardGet10Usd;
    public int numCardGet20Usd;

    public int numGemGet5Usd;
    public int numGemGet10Usd;
    public int numGemGet20Usd;


    public UILabel lPrice_VipPack_Sever;

    public UILabel lNameCardGet;
    public UILabel lnumCardGet;
    public UISprite sCardGet;

    public UILabel lnumGemGet;
    public UILabel lnumVipPoint;

    public GameObject btnBuyObj;

    public UILabel lCountdownPopupCardPack; // biến này sẽ gán ở Awake cho PanelBtnPack để dùng hàm đếm bên đó  



    private string productIapUse = EM_IAPConstants.Product_card_great_offer_10;
    private int numCardGetUse, numGemGetUse, numVipPointUse;

    string nameCardSell;

    int TypePriceSell
    {
        //5=5usd,
        //10=10usd
        get
        {
            return PlayerPrefs.GetInt(CACHE_TYPE_PRICE, 5);
        }
        set
        {
            PlayerPrefs.SetInt(CACHE_TYPE_PRICE, value);
        }
    }

    int IndexAircraftSell
    {
        get
        {
            return PlayerPrefs.GetInt(CACHE_INDEX_AIRCRAFT, 2);
        }
        set
        {
            PlayerPrefs.SetInt(CACHE_INDEX_AIRCRAFT, value);
        }
    }
    void Start()
    {
        //lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", AircraftSheet.Get(1).name);
        PanelBtnPack.Instance.lCountdownPopupCardPack = lCountdownPopupCardPack;
    }

    void OnEnable()
    {
        SetDataStart();
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
    }
    void OnDisable()
    {
        IAPCallbackManager.Instance.isServerPush = false;
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }

    public void GetDataServer(int typePrice, int indexAircraftCard)
    {
        TypePriceSell = typePrice;
        IndexAircraftSell = indexAircraftCard;
        CacheGame.PopupCardPackIsSelling = 1;
        btnBuyObj.SetActive(true);

        if (IndexAircraftSell > GameContext.TOTAL_AIRCRAFT)
        {
            IndexAircraftSell = IndexAircraftSell - GameContext.TOTAL_AIRCRAFT;
        }

        SetDataStart();

        MinhCacheGame.SetTimeFinishSaleCardPack();
        PanelBtnPack.Instance.StartCountdown();
        PanelBtnPack.Instance.btnCardPack.SetActive(true);
    }

    void SetDataStart()
    {
        switch (TypePriceSell)
        {
            case 5:
                productIapUse = EM_IAPConstants.Product_card_great_offer_5;
                numCardGetUse = numCardGet5Usd;
                numGemGetUse = numGemGet5Usd;
                numVipPointUse = 5;
                break;
            case 10:
                productIapUse = EM_IAPConstants.Product_card_great_offer_10;
                numCardGetUse = numCardGet10Usd;
                numGemGetUse = numGemGet10Usd;
                numVipPointUse = 10;
                break;
            case 20:
                productIapUse = EM_IAPConstants.Product_card_great_offer_20;
                numCardGetUse = numCardGet20Usd;
                numGemGetUse = numGemGet20Usd;
                numVipPointUse = 20;
                break;
            default:
                productIapUse = EM_IAPConstants.Product_card_great_offer_5;
                numCardGetUse = numCardGet5Usd;
                numGemGetUse = numGemGet5Usd;
                numVipPointUse = 5;
                break;
        }

        sCardGet.spriteName = "card_A" + IndexAircraftSell;

        if (IndexAircraftSell == 0)
        {
            lNameCardGet.text = ScriptLocalization.item_name_uni_aircraft_card;
            nameCardSell = "Aircraft General Card";
        }
        else
        {
            lNameCardGet.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.AircraftNames((AircraftTypeEnum)IndexAircraftSell));
            nameCardSell = "" + (AircraftTypeEnum)IndexAircraftSell;
        }

        lnumCardGet.text = "+" + numCardGetUse;
        lnumGemGet.text = "" + numGemGetUse;
        lnumVipPoint.text = "" + numVipPointUse;
        lPrice_VipPack_Sever.text = "" + CuongUtils.GetIapLocalizedPrice(productIapUse);

    }

    public void Btn_InApp_CardPack()
    {
        SoundManager.PlayClickButton();

        if (!IAPCallbackManager.Instance.isServerPush)
        {
            IAPCallbackManager.Instance.BuyProduct(productIapUse, IAPCallbackManager.WherePurchase.shop, nameCardSell);
        }
        else
        {
            IAPCallbackManager.Instance.BuyProduct(productIapUse, IAPCallbackManager.WherePurchase.push_server, nameCardSell);
        }
    }

    public void Btn_Back()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.HideCardPackPopup();
    }

    void OnPurchaseSuccessed(string productName)
    {
        if (productName == productIapUse)
        {
            CacheGame.PopupCardPackIsSelling = 0;
            PanelCoinGem.Instance.AddGemTop(numGemGetUse, FirebaseLogSpaceWar.IAP_why);
            if (IndexAircraftSell == 0)
            {
                MinhCacheGame.SetSpaceShipGeneralCards(MinhCacheGame.GetSpaceShipGeneralCards() + numCardGetUse, FirebaseLogSpaceWar.IAP_why, "");
                PopupManagerCuong.Instance.ShowTextNotifiToast((string)(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}",
                     Environment.NewLine + " +" + numCardGetUse + " " + ScriptLocalization.item_name_uni_aircraft_card
                    + Environment.NewLine + " +" + numGemGetUse + " " + ScriptLocalization.gem
                     )), (bool)true, (float)1.8f);
            }
            else
            {
                MinhCacheGame.SetSpaceShipCards((AircraftTypeEnum)IndexAircraftSell, MinhCacheGame.GetSpaceShipCards((AircraftTypeEnum)IndexAircraftSell) + numCardGetUse, FirebaseLogSpaceWar.IAP_why, "", ((AircraftTypeEnum)IndexAircraftSell).ToString());
                PopupManagerCuong.Instance.ShowTextNotifiToast((string)(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}",
                     Environment.NewLine + " +" + numCardGetUse + " " + ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.AircraftNames((AircraftTypeEnum)IndexAircraftSell))
                    + Environment.NewLine + " +" + numGemGetUse + " " + ScriptLocalization.gem
                     )), (bool)true, (float)1.8f);
            }
            btnBuyObj.SetActive(false);
            PanelBtnPack.Instance.btnCardPack.SetActive(false);
        }

    }
}
