﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using Firebase.Analytics;
using TCore;
using System;
using UnityEngine.SceneManagement;
using DG.Tweening;
using I2.Loc;
using UnityEngine.Purchasing.Security;
using SkyGameKit;

public class PopupPremiumPack : Singleton<PopupPremiumPack>
{

    public GameObject panelWatchAdsBuyPack;
    public GameObject btnBuyPack;

    [SerializeField]
    private GameObject popupPremiumPack;

    public UILabel lPrice_BeforeSale;
    public UILabel lPrice_PremiumPack;

    private bool isClickBtnPurchase = false;


    void Start()
    {
        if (SceneManager.GetActiveScene().name != "Home" && SceneManager.GetActiveScene().name != "SelectLevel")
        {
            gameObject.SetActive(false);
        }
        else
        {
            //Invoke("DelayGetLocalizeData", 1f);
            DelayGetLocalizeData();
            //ShowPanelBuyPackByAds();
        }


    }

    void DelayGetLocalizeData()
    {
        if (InAppPurchasing.GetProductLocalizedData(EM_IAPConstants.Product_inapp_20usd) != null)
        {
            lPrice_BeforeSale.text = "" + InAppPurchasing.GetProductLocalizedData(EM_IAPConstants.Product_inapp_20usd).localizedPriceString;
        }

        if (InAppPurchasing.GetProductLocalizedData(EM_IAPConstants.Product_inapp_premiumpack) != null)

        {
            lPrice_PremiumPack.text = "" + InAppPurchasing.GetProductLocalizedData(EM_IAPConstants.Product_inapp_premiumpack).localizedPriceString;
        }
    }





    public void Btn_BuyPremiumPack()
    {
        SoundManager.PlayClickButton();
        if (!IAPCallbackManager.Instance.isServerPush)
        {
            IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_premiumpack, IAPCallbackManager.WherePurchase.shop);
        }
        else
        {
            IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_premiumpack, IAPCallbackManager.WherePurchase.push_server);
        }
    }

    void OnEnable()
    {
        isClickBtnPurchase = false;

        //countdownTimeText.GetComponent<Localize>().SetTerm(ScriptLocalization.msg_premium_pack_time_again);

        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            btnBuyPack.SetActive(false);
            countdownTimeText.gameObject.SetActive(true);
            //countdownTimeNumber.gameObject.SetActive(true);

            panelWatchAdsBuyPack.SetActive(false);
            StartCoroutine(CountDown());
        }
        else
        {
            countdownTimeText.gameObject.SetActive(false);
            //countdownTimeNumber.gameObject.SetActive(false);
            btnBuyPack.SetActive(true);
        }

        //InAppPurchasing.PurchaseCompleted += PurchaseCompletedHandler;
        //InAppPurchasing.PurchaseFailed += PurchaseFailedHandler;
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
    }

    // Unsubscribe when the game object is disabled
    void OnDisable()
    {
        StopAllCoroutines();
        IAPCallbackManager.Instance.isServerPush = false;

        //InAppPurchasing.PurchaseCompleted -= PurchaseCompletedHandler;
        //InAppPurchasing.PurchaseFailed -= PurchaseFailedHandler;
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }







    void OnPurchaseSuccessed(string productName)
    {
        if (productName == EM_IAPConstants.Product_inapp_premiumpack)
        {
            CallBackBuyPack();
            if (panelWatchAdsBuyPack.activeInHierarchy)
            {
                CacheGame.SetNumberWatchAdsToGetPack(0);
                panelWatchAdsBuyPack.SetActive(false);
            }
        }
    }

    public void CallBackBuyPack()
    {

        //DontDestroyManager.Instance.AddCoinTop(10000);
        MinhCacheGame.SetAlreadyPurchasePremiumPack(true);
        MinhCacheGame.SetPremiumPackExpiredTime(DateTime.Now.AddDays(GameContext.EXPIRED_TIME_PREMIUM_PACK));
        CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + 10);

        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.AddCoinTop(10000, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_premiumpack);
        }

        btnBuyPack.SetActive(false);

        ShowNotifiComplete(ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", Environment.NewLine + "10.000 " + ScriptLocalization.gold + Environment.NewLine + "10 " + ScriptLocalization.power_up));

        countdownTimeText.gameObject.SetActive(true);
        //countdownTimeNumber.gameObject.SetActive(true);
        btnBuyPack.SetActive(false);
        StartCoroutine(CountDown());


    }



    //
    ///---text notifi
    void ShowNotifiComplete(string text)
    {
        LeanTween.delayedCall(0.6f, (Action)(() =>
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast((string)text, (bool)true, (float)1.8f);
        })).setIgnoreTimeScale(true);
    }


    ////////Minh code

    //Thời gian hết hạn của pack (tính bằng ngày)
    //public int packExpiredTime = 3;

    //Thời gian hết hạn của items trong pack (tính bằng ngày)
    //public int itemExpiredTime = 7;

    //Thời gian xuất hiện lại pack sau khi hết hạn (tính bằng ngày)
    //public int resetTime = 2;

    [SerializeField]
    private UILabel countdownTimeText;

    public bool IsPurchased
    {
        get { return MinhCacheGame.IsAlreadyPurchasePremiumPack(); }
    }



    public void OnClickButtonPremiumPack()
    {
        SoundManager.PlayClickButton();
        popupPremiumPack.SetActive(true);
    }

    public void OnClickButtonBack()
    {
        SoundManager.PlayClickButton();
        //popupPremiumPack.SetActive(false);
        //if (PopupManagerCuong.Instance != null)
        //{
        //    PopupManagerCuong.Instance.HidePremiumPackPopup();
        //}
    }

    public void ResetPurchase()
    {
        btnBuyPack.gameObject.SetActive(true);
        MinhCacheGame.SetAlreadyPurchasePremiumPack(false);
    }

    IEnumerator CountDown()
    {
        DateTime expiredTime = MinhCacheGame.GetPremiumPackTimeExpiredTime();

        while (DateTime.Now < expiredTime)
        {
            TimeSpan interval = expiredTime - DateTime.Now;
            string time = string.Format("{0:00}:{1:00}:{2:00}", interval.Hours + interval.Days * 24, interval.Minutes, interval.Seconds);
            countdownTimeText.text = ScriptLocalization.msg_premium_pack_time_again.Replace("%{time}", time);
            yield return new WaitForSeconds(1f);
        }

        countdownTimeText.gameObject.SetActive(false);
        //countdownTimeNumber.gameObject.SetActive(false);
        btnBuyPack.SetActive(true);
        MinhCacheGame.SetAlreadyPurchasePremiumPack(false);
        //PvpUtil.SendUpdatePlayer();
        PvpUtil.SendUpdatePlayer("CountDown_PremiumPack");
        //PlayerPrefs.Save();
    }

}
