﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using SkyGameKit;
using DG.Tweening;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using TCore;
using Sirenix.OdinInspector;
using I2;

public class PopupIAPGem : MonoBehaviour
{
    [System.Serializable]
    public class ListPackIAPGem
    {
        [Tooltip("ty le roi ra Item")]
        public GameObject itemObj;
        public UISprite sBgPack;
        public GameObject titleOrigiObj;
        public GameObject titleOneTimeObj;
        public int numGemOrigi;
        public GameObject saleObj;

        [HideInInspector]
        public int numGemSaleUse;
    }

    public ListPackIAPGem[] listPackIAPGem;

    //lis coin get tương đương 1 - 5 -10 -20 -50 usd-100 usd
    //public List<int> listGemGetOrigi = new List<int>() { 160, 450, 1000, 2500, 2000, 7000, 16000 };

    //List<int> listGemGetUse = new List<int>() { 160, 450, 1000, 2500, 2000, 7000, 16000 };

    public UILabel lPrice_Inapp1Usd, lPrice_Inapp5Usd, lPrice_Inapp10Usd, lPrice_Inapp20Usd, lPrice_Inapp50Usd, lPrice_InappGem100USD;

    //1-6
    //public GameObject[] listItemSaleAllPack;
    //1-6
    //public GameObject[] listPack;

    [HideInInspector]
    //1-6
    public List<Vector3> listPosOrigi;

    private bool isClickBtnPurchase = false;


    void Awake()
    {
        listPosOrigi.Clear();
        for (int i = 0; i < listPackIAPGem.Length; i++)
        {
            listPosOrigi.Add(listPackIAPGem[i].itemObj.transform.localPosition);
        }
    }


    void Start()
    {
        SetTextPrice();
    }

    IAPCallbackManager.WherePurchase getTypeProductLogServer(int indexProduct)
    {
        if (CachePvp.serverX2PurchaseGem == 1)
        {
            return IAPCallbackManager.WherePurchase.shop_sale;
        }
        if (CachePvp.listPromotionsGem != null)
        {
            for (int i = 0; i < CachePvp.listPromotionsGem.Count; i++)
            {
                if (indexProduct == CachePvp.listPromotionsGem[i])
                {
                    return IAPCallbackManager.WherePurchase.shop_sale;
                }
            }
        }
        return IAPCallbackManager.WherePurchase.shop;
    }

    public void SortItemBySever()
    {

        for (int i = 0; i < CachePvp.listOrdersGem.Count; i++)
        {
            listPackIAPGem[CachePvp.listOrdersGem[i] - 1].itemObj.transform.localPosition = listPosOrigi[i];
        }
    }


    public void SetSalePackStart()
    {
        //CachePvp.sCClientConfig.turnOnPurchaseOneTime = 1;
        //CachePvp.serverX2PurchaseGem = 1;
        //Debug.LogError("turnOnPurchaseOneTime:" + CachePvp.sCClientConfig.turnOnPurchaseOneTime + "----serverX2PurchaseGem:" + CachePvp.serverX2PurchaseGem);
        if (CachePvp.sCClientConfig.turnOnPurchaseOneTime == 0)
        {
            SortItemBySever();
            SetSaleAllPack();
            SetListPackX2FirstBuy();
        }
        else
        {
            SetDataBuyOneTime();
        }
        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.ShowGlowButtonIapGem();
        }
    }

    void SetDataBuyOneTime()
    {
        //Debug.LogError("SetDataBuyOneTime");
        for (int i = 0; i < listPackIAPGem.Length; i++)
        {
            if (CacheGame.GetInappGemOneTime(i + 1) == 0)
            {
                listPackIAPGem[i].saleObj.SetActive(true);
                listPackIAPGem[i].numGemSaleUse = listPackIAPGem[i].numGemOrigi * 2;
                listPackIAPGem[i].sBgPack.spriteName = "IAP_itemBG4";
                listPackIAPGem[i].titleOrigiObj.SetActive(false);
                listPackIAPGem[i].titleOneTimeObj.SetActive(true);
            }
            else
            {
                listPackIAPGem[i].saleObj.SetActive(false);
                listPackIAPGem[i].numGemSaleUse = listPackIAPGem[i].numGemOrigi;
                listPackIAPGem[i].sBgPack.spriteName = "IAP_itemBG4_2";
                listPackIAPGem[i].titleOrigiObj.SetActive(true);
                listPackIAPGem[i].titleOneTimeObj.SetActive(false);
            }
        }
    }




    void SetSaleAllPack()
    {
        //Debug.LogError("SetSaleAllPack");
        if (CachePvp.serverX2PurchaseGem == 1)
        {
            for (int i = 0; i < listPackIAPGem.Length; i++)
            {
                listPackIAPGem[i].saleObj.SetActive(true);
                listPackIAPGem[i].numGemSaleUse = listPackIAPGem[i].numGemOrigi * 2;
                listPackIAPGem[i].sBgPack.spriteName = "IAP_itemBG4";
                listPackIAPGem[i].titleOrigiObj.SetActive(true);
                listPackIAPGem[i].titleOneTimeObj.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < listPackIAPGem.Length; i++)
            {
                listPackIAPGem[i].saleObj.SetActive(false);
                listPackIAPGem[i].numGemSaleUse = listPackIAPGem[i].numGemOrigi;
                listPackIAPGem[i].sBgPack.spriteName = "IAP_itemBG4_2";
                listPackIAPGem[i].titleOrigiObj.SetActive(true);
                listPackIAPGem[i].titleOneTimeObj.SetActive(false);
            }
        }
    }
    //--- indexPack = 1-5

    void SetListPackX2FirstBuy()
    {
        //CachePvp.listPromotionsGem = new List<int> { 1, 2, 3, 4, 5, 6 };
        if (CachePvp.listPromotionsGem == null)
        {
            return;
        }
        for (int i = 0; i < CachePvp.listPromotionsGem.Count; i++)
        {
            SetOnePackX2FirstBuy(CachePvp.listPromotionsGem[i]);
        }
    }
    void SetOnePackX2FirstBuy(int indexPack)
    {
        if (indexPack > listPackIAPGem.Length || indexPack < 1)
        {
            return;
        }
        if (CachePvp.serverX2PurchaseGem == 1)
        {
            return;
        }
        listPackIAPGem[indexPack - 1].numGemSaleUse = listPackIAPGem[indexPack - 1].numGemOrigi * 2;
        listPackIAPGem[indexPack - 1].sBgPack.spriteName = "IAP_itemBG4";
        listPackIAPGem[indexPack - 1].saleObj.SetActive(true);
    }


    void SetTextPrice()
    {
        lPrice_Inapp1Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gem_2usd);
        lPrice_Inapp5Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gem_5usd);
        lPrice_Inapp10Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gem_10usd);
        lPrice_Inapp20Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gem_20usd);
        lPrice_Inapp50Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gem_50usd);
        lPrice_InappGem100USD.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gem_100usd);
    }

    //---------------------------------------------------------------------------


    public void Btn_InApp_1USD()
    {
        SoundManager.PlayClickButton();
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_gem_2usd);
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_gem_2usd, getTypeProductLogServer(1));
    }

    public void Btn_InApp_5USD()
    {
        SoundManager.PlayClickButton();
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_gem_5usd);
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_gem_5usd, getTypeProductLogServer(2));
    }

    public void Btn_InApp_10USD()
    {
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_gem_10usd);
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_gem_10usd, getTypeProductLogServer(3));
    }

    public void Btn_InApp_20USD()
    {
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_gem_20usd);
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_gem_20usd, getTypeProductLogServer(4));
    }

    public void Btn_InApp_50USD()
    {
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_gem_50usd);
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_gem_50usd, getTypeProductLogServer(5));
    }

    public void Btn_InApp_100USD()
    {
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_gem_100usd);
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_gem_100usd, getTypeProductLogServer(6));
    }


    //---------------------------------------------------------------------------

    // Subscribe to IAP purchase events
    void OnEnable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
        SetSalePackStart();
    }

    // Unsubscribe when the game object is disabled
    void OnDisable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }


    void OnPurchaseSuccessed(string productName)
    {
        switch (productName)
        {
            case EM_IAPConstants.Product_inapp_gem_2usd:
                PanelCoinGem.Instance.AddGemTop(listPackIAPGem[0].numGemSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gem_2usd);
                ShowNotifiComplete(listPackIAPGem[0].numGemSaleUse, " gem");
                CacheGame.SetInappGemOneTime(1, 1);
                break;
            case EM_IAPConstants.Product_inapp_gem_5usd:
                PanelCoinGem.Instance.AddGemTop(listPackIAPGem[1].numGemSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gem_5usd);
                ShowNotifiComplete(listPackIAPGem[1].numGemSaleUse, " gem");
                CacheGame.SetInappGemOneTime(2, 1);
                break;
            case EM_IAPConstants.Product_inapp_gem_10usd:
                PanelCoinGem.Instance.AddGemTop(listPackIAPGem[2].numGemSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gem_10usd);
                //
                ShowNotifiComplete(listPackIAPGem[2].numGemSaleUse, " gem");
                CacheGame.SetInappGemOneTime(3, 1);

                break;
            case EM_IAPConstants.Product_inapp_gem_20usd:
                PanelCoinGem.Instance.AddGemTop(listPackIAPGem[3].numGemSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gem_20usd);
                //
                ShowNotifiComplete(listPackIAPGem[3].numGemSaleUse, " gem");
                CacheGame.SetInappGemOneTime(4, 1);

                break;
            case EM_IAPConstants.Product_inapp_gem_50usd:
                PanelCoinGem.Instance.AddGemTop(listPackIAPGem[4].numGemSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gem_50usd);
                //
                ShowNotifiComplete(listPackIAPGem[4].numGemSaleUse, " gem");
                CacheGame.SetInappGemOneTime(5, 1);

                break;
            case EM_IAPConstants.Product_inapp_gem_100usd:
                PanelCoinGem.Instance.AddGemTop(listPackIAPGem[5].numGemSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gem_100usd);
                //
                ShowNotifiComplete(listPackIAPGem[5].numGemSaleUse, " gem");
                CacheGame.SetInappGemOneTime(6, 1);
                break;
            default:
                break;
        }
        SetSalePackStart();
    }


    void ShowNotifiComplete(int numGemAdd, string typeGift)
    {
        LeanTween.delayedCall(0.56f, (System.Action)(() =>
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast((string)(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + numGemAdd + typeGift)), (bool)true, (float)1.8f);
        })).setIgnoreTimeScale(true);
    }
}
