﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolTipShop : MonoBehaviour
{

	public  UILabel lToolTip;
	Transform trans_cache;

	void Awake ()
	{
		trans_cache = transform;
	}


	void Update ()
	{
		if (Input.GetMouseButtonDown (0)) {
			gameObject.SetActive (false);
		}
	}

	public void ChangeToolTipOpen (Vector3 position, string keyLocalization)
	{
		trans_cache.localPosition = position;
		lToolTip.text = keyLocalization;
	}



}
