﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using SkyGameKit;
using DG.Tweening;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using TCore;
using Sirenix.OdinInspector;

public class PopupIAP : MonoBehaviour
{
    //
    public UIScrollView scrollViewPopupIAP;
    public int numCoin_InappInfinity = 10000;
    public GameObject objInfinityPack;
    public UILabel lPrice_InfinityPack;

    [System.Serializable]
    public class ListPackIAPGold
    {
        [Tooltip("ty le roi ra Item")]
        public GameObject itemObj;
        public UISprite sBgPack;
        public GameObject titleOrigiObj;
        public GameObject titleOneTimeObj;
        public int numGoldOrigi;
        public GameObject saleObj;

        [HideInInspector]
        public int numGoldSaleUse;
    }
    public ListPackIAPGold[] listPackIAPGold;

    public UILabel lPrice_Inapp1Usd, lPrice_Inapp5Usd, lPrice_Inapp10Usd, lPrice_Inapp20Usd, lPrice_Inapp50Usd, lPrice_InappGem100USD;


    [HideInInspector] //1-6
    public List<Vector3> listPosOrigi;

    void Awake()
    {
        listPosOrigi.Clear();
        for (int i = 0; i < listPackIAPGold.Length; i++)
        {
            listPosOrigi.Add(listPackIAPGold[i].itemObj.transform.localPosition);
        }
    }

    void Start()
    {
        SetTextPrice();
    }

    IAPCallbackManager.WherePurchase getTypeProductLogServer(int indexProduct)
    {
        if (CachePvp.serverX2PurchaseGold == 1)
        {
            return IAPCallbackManager.WherePurchase.shop_sale;
        }
        if (CachePvp.listPromotionsGold != null)
        {
            for (int i = 0; i < CachePvp.listPromotionsGold.Count; i++)
            {
                if (indexProduct == CachePvp.listPromotionsGold[i])
                {
                    return IAPCallbackManager.WherePurchase.shop_sale;
                }
            }
        }
        return IAPCallbackManager.WherePurchase.shop;
    }

    public void SortItemBySever()
    {
        for (int i = 0; i < CachePvp.listOrdersGold.Count; i++)
        {
            listPackIAPGold[CachePvp.listOrdersGold[i] - 1].itemObj.transform.localPosition = listPosOrigi[i];
        }
    }


    public void SetSalePackStart()
    {
        if (CachePvp.sCClientConfig.turnOnPurchaseOneTime == 0)
        {
            SortItemBySever();
            SetSaleAllPack();
            SetListPackX2FirstBuy();
        }
        else
        {
            SetDataBuyOneTime();
        }

        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.ShowGlowButtonIapGold();
        }
        if (CacheGame.GetBuyInfinityPack() == 1)
        {
            objInfinityPack.SetActive(false);
            scrollViewPopupIAP.ResetPosition();
        }
    }

    void SetDataBuyOneTime()
    {
        Debug.LogError("SetDataBuyOneTime");
        for (int i = 0; i < listPackIAPGold.Length; i++)
        {
            if (CacheGame.GetInappGoldOneTime(i + 1) == 0)
            {
                listPackIAPGold[i].saleObj.SetActive(true);
                listPackIAPGold[i].numGoldSaleUse = listPackIAPGold[i].numGoldOrigi * 2;
                listPackIAPGold[i].sBgPack.spriteName = "IAP_itemBG3";
                listPackIAPGold[i].titleOrigiObj.SetActive(false);
                listPackIAPGold[i].titleOneTimeObj.SetActive(true);
            }
            else
            {
                listPackIAPGold[i].saleObj.SetActive(false);
                listPackIAPGold[i].numGoldSaleUse = listPackIAPGold[i].numGoldOrigi;
                listPackIAPGold[i].sBgPack.spriteName = "IAP_itemBG2";
                listPackIAPGold[i].titleOrigiObj.SetActive(true);
                listPackIAPGold[i].titleOneTimeObj.SetActive(false);
            }
        }
    }

    void SetSaleAllPack()
    {
        //CachePvp.serverX2PurchaseGold = 1;
        if (CachePvp.serverX2PurchaseGold == 1)
        {
            for (int i = 0; i < listPackIAPGold.Length; i++)
            {
                listPackIAPGold[i].saleObj.SetActive(true);
                listPackIAPGold[i].numGoldSaleUse = listPackIAPGold[i].numGoldOrigi * 2;
                listPackIAPGold[i].sBgPack.spriteName = "IAP_itemBG3";
                listPackIAPGold[i].titleOrigiObj.SetActive(true);
                listPackIAPGold[i].titleOneTimeObj.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < listPackIAPGold.Length; i++)
            {
                listPackIAPGold[i].saleObj.SetActive(false);
                listPackIAPGold[i].numGoldSaleUse = listPackIAPGold[i].numGoldOrigi;
                listPackIAPGold[i].sBgPack.spriteName = "IAP_itemBG2";
                listPackIAPGold[i].titleOrigiObj.SetActive(true);
                listPackIAPGold[i].titleOneTimeObj.SetActive(false);
            }
        }
    }
    //--- indexPack = 1-5-6

    void SetListPackX2FirstBuy()
    {
        //CachePvp.listPromotionsGold = new List<int> { 1, 6 };
        if (CachePvp.listPromotionsGold == null)
        {
            return;
        }
        for (int i = 0; i < CachePvp.listPromotionsGold.Count; i++)
        {
            SetOnePackX2FirstBuy(CachePvp.listPromotionsGold[i]);
        }
    }
    void SetOnePackX2FirstBuy(int indexPack)
    {
        if (indexPack > listPackIAPGold.Length || indexPack < 1)
        {
            return;
        }
        if (CachePvp.serverX2PurchaseGold == 1)
        {
            return;
        }

        listPackIAPGold[indexPack - 1].numGoldSaleUse = listPackIAPGold[indexPack - 1].numGoldOrigi * 2;
        listPackIAPGold[indexPack - 1].sBgPack.spriteName = "IAP_itemBG3";
        listPackIAPGold[indexPack - 1].saleObj.SetActive(true);
    }


    void SetTextPrice()
    {
        lPrice_Inapp1Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_2usd);
        lPrice_Inapp5Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gold_5usd);
        lPrice_Inapp10Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_10usd);
        lPrice_Inapp20Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_20usd);
        lPrice_Inapp50Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gold_50usd);
        lPrice_InappGem100USD.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_100usd);

        lPrice_InfinityPack.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gold_infinitypack_new);
    }

    //---------------------------------------------------------------------------


    public void Btn_InApp_1USD()
    {
        SoundManager.PlayClickButton();
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_2usd);
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_2usd, getTypeProductLogServer(1));
    }

    public void Btn_InApp_5USD()
    {

        SoundManager.PlayClickButton();
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_gold_5usd);
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_gold_5usd, getTypeProductLogServer(2));
    }

    public void Btn_InApp_10USD()
    {
        SoundManager.PlayClickButton();
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_10usd);
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_10usd, getTypeProductLogServer(3));
    }

    public void Btn_InApp_20USD()
    {
        SoundManager.PlayClickButton();
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_20usd);
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_20usd, getTypeProductLogServer(4));
    }

    public void Btn_InApp_50USD()
    {
        SoundManager.PlayClickButton();
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_gold_50usd);
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_gold_50usd, getTypeProductLogServer(5));
    }

    public void Btn_InApp_100USD()
    {
        SoundManager.PlayClickButton();
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_100usd);
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_100usd, getTypeProductLogServer(6));
    }

    public void Btn_InApp_InfinityPack()
    {
        //OnPurchaseSuccessed(EM_IAPConstants.Product_inapp_gold_infinitypack_new);
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_gold_infinitypack_new, getTypeProductLogServer(-1));
    }

    //---------------------------------------------------------------------------

    // Subscribe to IAP purchase events
    void OnEnable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
        SetSalePackStart();
    }

    // Unsubscribe when the game object is disabled
    void OnDisable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }


    void OnPurchaseSuccessed(string productName)
    {
        switch (productName)
        {
            case EM_IAPConstants.Product_inapp_2usd:
                PanelCoinGem.Instance.AddCoinTop(listPackIAPGold[0].numGoldSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_2usd);
                ShowNotifiComplete(listPackIAPGold[0].numGoldSaleUse, " gold");
                CacheGame.SetInappGoldOneTime(1, 1);
                break;
            case EM_IAPConstants.Product_inapp_gold_5usd:
                PanelCoinGem.Instance.AddCoinTop(listPackIAPGold[1].numGoldSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gold_5usd);
                ShowNotifiComplete(listPackIAPGold[1].numGoldSaleUse, " gold");
                CacheGame.SetInappGoldOneTime(2, 1);
                break;
            case EM_IAPConstants.Product_inapp_10usd:
                PanelCoinGem.Instance.AddCoinTop(listPackIAPGold[2].numGoldSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_10usd);
                ShowNotifiComplete(listPackIAPGold[2].numGoldSaleUse, " gold");
                CacheGame.SetInappGoldOneTime(3, 1);
                break;
            case EM_IAPConstants.Product_inapp_20usd:
                PanelCoinGem.Instance.AddCoinTop(listPackIAPGold[3].numGoldSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_20usd);
                ShowNotifiComplete(listPackIAPGold[3].numGoldSaleUse, " gold");
                CacheGame.SetInappGoldOneTime(4, 1);
                break;
            case EM_IAPConstants.Product_inapp_gold_50usd:
                PanelCoinGem.Instance.AddCoinTop(listPackIAPGold[4].numGoldSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gold_50usd);
                ShowNotifiComplete(listPackIAPGold[4].numGoldSaleUse, " gold");
                CacheGame.SetInappGoldOneTime(5, 1);
                break;
            case EM_IAPConstants.Product_inapp_100usd:
                PanelCoinGem.Instance.AddCoinTop(listPackIAPGold[5].numGoldSaleUse, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_100usd);
                ShowNotifiComplete(listPackIAPGold[5].numGoldSaleUse, " gold");
                CacheGame.SetInappGoldOneTime(6, 1);
                break;
            case EM_IAPConstants.Product_inapp_gold_infinitypack_new:
                PanelCoinGem.Instance.AddCoinTop(numCoin_InappInfinity, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gold_infinitypack_new);
                ShowNotifiComplete(numCoin_InappInfinity, " gold");
                CacheGame.SetBuyInfinityPack(1);
                //objInfinityPack.SetActive(false);
                //scrollViewPopupIAP.ResetPosition();
                break;
            default:
                break;
        }
        SetSalePackStart();
    }


    void ShowNotifiComplete(int numCoinAdd, string typeGift)
    {
        LeanTween.delayedCall(0.56f, (System.Action)(() =>
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast((string)(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + numCoinAdd + typeGift)), (bool)true, (float)1.8f);
        })).setIgnoreTimeScale(true);
    }
}
