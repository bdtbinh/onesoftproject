﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TCore;
using Firebase.Analytics;
using UnityEngine.SceneManagement;
using PathologicalGames;
using I2.Loc;


public class ItemUpgradeManager : MonoBehaviour
{
    //	private  string
    public Transform itemMainWeapon, itemSuperWeapon, itemLife, itemGold, itemWingman, itemSecondaryWeapon;

    public GameObject objToolTip;
    ToolTipShop toolTipShopScr;

    //
    public UILabel lMainWeapon_Power_Lv;
    public UILabel lMainWeapon_Power_Price;
    public GameObject sMainWeapon_Power_Level;
    public GameObject sBtnBuy_MainWeapon_Power;

    //

    public UILabel lMainWeapon_Quantity_Lv;
    public UILabel lMainWeapon_Quantity_Price;
    public GameObject sMainWeapon_Quantity_Level;
    public GameObject sBtnBuy_MainWeapon_Quantity;

    //

    public UILabel lMainWeapon_Speed_Lv;
    public UILabel lMainWeapon_Speed_Price;
    public GameObject sMainWeapon_Speed_Level;
    public GameObject sBtnBuy_MainWeapon_Speed;
    //

    public UILabel lLife_Quantity_Lv;
    public UILabel lLife_Quantity_Price;
    public GameObject sLife_Quantity_Level;
    public GameObject sBtnBuy_Life_Quantity;

    //
    public UILabel lLaser_Power_Lv;
    public UILabel lLaser_Power_Price;
    public GameObject sLaser_Power_Level;
    public GameObject sBtnBuy_Laser_Power;

    //
    public UILabel lMissile_Power_Lv;
    public UILabel lMissile_Power_Price;
    public GameObject sMissile_Power_Level;
    public GameObject sBtnBuy_Missile_Power;

    //
    public UILabel lWindSlash_Power_Lv;
    public UILabel lWindSlash_Power_Price;
    public GameObject sWindSlash_Power_Level;
    public GameObject sBtnBuy_WindSlash_Power;
    //
    public UILabel lSuperWeapon_Duration_Lv;
    public UILabel lSuperWeapon_Duration_Price;
    public GameObject sSuperWeapon_Duration_Level;
    public GameObject sBtnBuy_SuperWeapon_Duration;


    //
    public UILabel lGold_DropChance_Lv;
    public UILabel lGold_DropChance_Price;
    public GameObject sGold_DropChance_Level;
    public GameObject sBtnBuy_Gold_DropChance;

    //
    public UILabel lWingman_Power_Lv;
    public UILabel lWingman_Power_Price;
    public GameObject sWingman_Power_Level;
    public GameObject sBtnBuy_Wingman_Power;

    //
    public UILabel lSecondaryWeapon_Power_Lv;
    public UILabel lSecondaryWeapon_Power_Price;
    public GameObject sSecondaryWeapon_Power_Level;
    public GameObject sBtnBuy_SecondaryWeapon_Power;

    //

    string nameDotShow = "bar_upgrade_dot1";
    string nameDotHide = "bar_upgrade_dot0";


    void Start()
    {
        //		ConfigLoader.Load ();
        toolTipShopScr = objToolTip.GetComponent<ToolTipShop>();

    }




    public void ShowStartInfo()
    {

        //-------

        ShowLengthLevel(GameContext.MAIN_WEAPON, GameContext.POWER, sMainWeapon_Power_Level);
        //		ShowLengthLevel (GameContex.mainWeapon, GameContex.quantity, sMainWeapon_Quantity_Level);
        ShowLengthLevel(GameContext.MAIN_WEAPON, GameContext.SPEED, sMainWeapon_Speed_Level);
        //
        ShowLengthLevel(GameContext.LIFE, GameContext.QUANTITY, sLife_Quantity_Level);
        //
        ShowLengthLevel(GameContext.LASER, GameContext.POWER, sLaser_Power_Level);
        ShowLengthLevel(GameContext.MISSILE, GameContext.POWER, sMissile_Power_Level);
        ShowLengthLevel(GameContext.WINDSLASH, GameContext.POWER, sWindSlash_Power_Level);
        ShowLengthLevel(GameContext.SUPER_WEAPON, GameContext.DURATION, sSuperWeapon_Duration_Level);
        //
        ShowLengthLevel(GameContext.GOLD, GameContext.DROP_CHANCE, sGold_DropChance_Level);
        //
        ShowLengthLevel(GameContext.WINGMAN, GameContext.POWER, sWingman_Power_Level);
        //
        ShowLengthLevel(GameContext.SECONDARY_WEAPON, GameContext.POWER, sSecondaryWeapon_Power_Level);



        //------------------
        ShowLabelCurLevel(GameContext.MAIN_WEAPON, GameContext.POWER, lMainWeapon_Power_Lv);
        //		ShowLabelCurLevel (GameContex.mainWeapon, GameContex.quantity, lMainWeapon_Quantity_Lv);
        ShowLabelCurLevel(GameContext.MAIN_WEAPON, GameContext.SPEED, lMainWeapon_Speed_Lv);
        //
        ShowLabelCurLevel(GameContext.LIFE, GameContext.QUANTITY, lLife_Quantity_Lv);
        //
        ShowLabelCurLevel(GameContext.LASER, GameContext.POWER, lLaser_Power_Lv);
        ShowLabelCurLevel(GameContext.MISSILE, GameContext.POWER, lMissile_Power_Lv);
        ShowLabelCurLevel(GameContext.WINDSLASH, GameContext.POWER, lWindSlash_Power_Lv);
        ShowLabelCurLevel(GameContext.SUPER_WEAPON, GameContext.DURATION, lSuperWeapon_Duration_Lv);
        //
        ShowLabelCurLevel(GameContext.GOLD, GameContext.DROP_CHANCE, lGold_DropChance_Lv);
        //
        ShowLabelCurLevel(GameContext.WINGMAN, GameContext.POWER, lWingman_Power_Lv);
        //
        ShowLabelCurLevel(GameContext.SECONDARY_WEAPON, GameContext.POWER, lSecondaryWeapon_Power_Lv);


        //-------------------


        ShowPriceInAllButton();
        //-------------------

        ShowHideBuyBtn(GameContext.MAIN_WEAPON, GameContext.POWER, sBtnBuy_MainWeapon_Power);
        //		ShowHideBuyBtn (GameContex.mainWeapon, GameContex.quantity, sBtnBuy_MainWeapon_Quantity);
        ShowHideBuyBtn(GameContext.MAIN_WEAPON, GameContext.SPEED, sBtnBuy_MainWeapon_Speed);
        //
        ShowHideBuyBtn(GameContext.LIFE, GameContext.QUANTITY, sBtnBuy_Life_Quantity);
        //
        ShowHideBuyBtn(GameContext.LASER, GameContext.POWER, sBtnBuy_Laser_Power);
        ShowHideBuyBtn(GameContext.MISSILE, GameContext.POWER, sBtnBuy_Missile_Power);
        ShowHideBuyBtn(GameContext.WINDSLASH, GameContext.POWER, sBtnBuy_WindSlash_Power);
        ShowHideBuyBtn(GameContext.SUPER_WEAPON, GameContext.DURATION, sBtnBuy_SuperWeapon_Duration);
        //
        ShowHideBuyBtn(GameContext.GOLD, GameContext.DROP_CHANCE, sBtnBuy_Gold_DropChance);
        //
        ShowHideBuyBtn(GameContext.WINGMAN, GameContext.POWER, sBtnBuy_Wingman_Power);
        //
        ShowHideBuyBtn(GameContext.SECONDARY_WEAPON, GameContext.POWER, sBtnBuy_SecondaryWeapon_Power);
    }

    void ShowPriceInAllButton()
    {
        ShowPriceInButton(GameContext.MAIN_WEAPON, GameContext.POWER, lMainWeapon_Power_Price);
        ShowPriceInButton(GameContext.MAIN_WEAPON, GameContext.SPEED, lMainWeapon_Speed_Price);
        //
        ShowPriceInButton(GameContext.LIFE, GameContext.QUANTITY, lLife_Quantity_Price);
        //
        ShowPriceInButton(GameContext.LASER, GameContext.POWER, lLaser_Power_Price);
        ShowPriceInButton(GameContext.MISSILE, GameContext.POWER, lMissile_Power_Price);
        ShowPriceInButton(GameContext.WINDSLASH, GameContext.POWER, lWindSlash_Power_Price);
        ShowPriceInButton(GameContext.SUPER_WEAPON, GameContext.DURATION, lSuperWeapon_Duration_Price);
        //
        ShowPriceInButton(GameContext.GOLD, GameContext.DROP_CHANCE, lGold_DropChance_Price);
        //
        ShowPriceInButton(GameContext.WINGMAN, GameContext.POWER, lWingman_Power_Price);
        //
        ShowPriceInButton(GameContext.SECONDARY_WEAPON, GameContext.POWER, lSecondaryWeapon_Power_Price);
    }



    public void ShowLengthLevel(string nameItem, string typeUpgrade, GameObject sLevelParent)
    {

        int indexChild = 0;
        int currentLvShopUpgrade = CacheGame.GetCurrentLvShopUpgrade(nameItem, typeUpgrade);

        int totalLevel = (int)ShopUpgradeConfig.Get(nameItem + typeUpgrade + GameContext.PRICE).totalLevel;


        int totalDotActive = 10;

        if ((currentLvShopUpgrade > (totalLevel - (totalLevel % 10))) && totalLevel % 10 != 0)
        {
            totalDotActive = totalLevel % 10;
        }
        else if (totalLevel < 10)
        {
            totalDotActive = totalLevel;
        }


        //

        int totalDotShow = currentLvShopUpgrade % 10;
        if (currentLvShopUpgrade % 10 == 0 && currentLvShopUpgrade > 0)
        {
            totalDotShow = 10;
        }

        //Debug.Log("totalDotActive " + totalDotActive + "totalDotShow" + totalDotShow);

        foreach (Transform child in sLevelParent.transform)
        {
            indexChild++;
            if (indexChild > totalDotActive)
            {
                child.gameObject.SetActive(false);
            }
            //
            if (indexChild <= totalDotShow)
            {
                child.GetComponent<UISprite>().spriteName = nameDotShow;
            }
            else
            {
                child.GetComponent<UISprite>().spriteName = nameDotHide;
            }

        }

    }

    public void ShowLabelCurLevel(string nameItem, string typeUpgrade, UILabel labelShow)
    {
        int currentLvShopUpgrade = CacheGame.GetCurrentLvShopUpgrade(nameItem, typeUpgrade);

        labelShow.text = "LV." + currentLvShopUpgrade;

    }

    //--

    public void ShowPriceInButton(string nameItem, string typeUpgrade, UILabel labelShow)
    {
        //+1 cua level tiep theo, nhung lai tru 1 vi list bat dau tu phan tu 0
        int indexGetPrice = (CacheGame.GetCurrentLvShopUpgrade(nameItem, typeUpgrade) + 1);
        int maxLevel = (int)ShopUpgradeConfig.Get(nameItem + typeUpgrade + GameContext.PRICE).totalLevel;
        //		Debug.LogError ("indexGetPrice" + indexGetPrice);

        if (indexGetPrice <= maxLevel)
        {
            ShopUpgradeConfig typesShop = ShopUpgradeConfig.Get(nameItem + typeUpgrade + GameContext.PRICE);
            int priceShow = (int)typesShop.list_PriceValue[indexGetPrice];
            //			int value = (int)typesShop.list_PriceValue [indexGetPrice];


            //PopupShop.Instance.listPriceShop.Add(priceShow);

            labelShow.text = "" + GameContext.FormatNumber(priceShow);


            //
            GameObject glowBtn = labelShow.transform.GetChild(0).gameObject;
            UISprite sprbgBtn = labelShow.transform.parent.GetComponent<UISprite>();
            //			Debug.LogError ("glowBtn " + glowBtn.name + "priceShow " + priceShow);

            if (priceShow <= CacheGame.GetTotalCoin())
            {
                glowBtn.SetActive(true);
                sprbgBtn.spriteName = "btn_upgrade_item_t";
                //				Debug.LogError ("glowBtn TRUE ");
            }
            else
            {
                //				Debug.LogError ("glowBtn f ");
                glowBtn.SetActive(false);
                sprbgBtn.spriteName = "btn_upgrade_item";
            }
        }
    }


    public void ShowHideBuyBtn(string nameItem, string typeUpgrade, GameObject btn_Obj)
    {
        int curLevel = CacheGame.GetCurrentLvShopUpgrade(nameItem, typeUpgrade);
        //		int maxLevel = SaveLoadUpgradeShop.Instance.LoadTotalLevelData (nameItem, typeUpgrade);
        int maxLevel = (int)ShopUpgradeConfig.Get(nameItem + typeUpgrade + GameContext.PRICE).totalLevel;

        //		Debug.LogError ("curLevel " + curLevel + "  maxLevel " + maxLevel);
        if (curLevel >= maxLevel)
        {
            btn_Obj.SetActive(false);
        }

    }







    //--------------
    public void ClickButtonBuy(string nameItem, string typeUpgrade)
    {

        if (PopupManagerCuong.Instance.readyShowNotifi)
        {
            //+1 cua level tiep theo
            int indexGetPrice = (CacheGame.GetCurrentLvShopUpgrade(nameItem, typeUpgrade) + 1);

            ShopUpgradeConfig typesShop = ShopUpgradeConfig.Get(nameItem + typeUpgrade + GameContext.PRICE);
            int priceLevel = (int)typesShop.list_PriceValue[indexGetPrice];
            //
            SoundManager.PlayClickButton();
            if (priceLevel <= CacheGame.GetTotalCoin())
            {

                //PopupShop.Instance.listPriceShop.Clear();

                CacheGame.SetCurrentLvShopUpgrade(nameItem, typeUpgrade, CacheGame.GetCurrentLvShopUpgrade(nameItem, typeUpgrade) + 1);
                //
               
                CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetUpgrade.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetUpgrade.ToString()) + 1);
                //
                //DontDestroyManager.Instance.AddCoinTop(-priceLevel);
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddCoinTop(-priceLevel);
                }
                PopupManagerCuong.Instance.ShowTextNotifiToast(typeUpgrade + " " + ScriptLocalization.notifi_shop_upgraded + " " + CacheGame.GetCurrentLvShopUpgrade(nameItem, typeUpgrade), true, 1f);
                //FirebaseLogSpaceWar.LogBuyByGold(nameItem + "_" + typeUpgrade);

                ShowStartInfo();
                //			DontDestroyManager.Instance.lNumCoinTop.text = "" + CacheGame.GetTotalCoin ();


                //FirebaseAnalytics.LogEvent("Upgrade", new Parameter("ClickUpgrade", nameItem + "_" + typeUpgrade));

                if (SceneManager.GetActiveScene().name == "Home" || SceneManager.GetActiveScene().name == "SelectLevel")
                {
                    Quest_Achi_Manager.Instance.ShowGlowCompleteQuest();
                }
            }
            else
            {

                //PopupShop.Instance.popupQuestOpenIAP.SetActive(true);
            }
        }
    }

    //-----------------------------------------Button -------------------------
    public void Btn_MainWeaPon_Power()
    {

        ClickButtonBuy(GameContext.MAIN_WEAPON, GameContext.POWER);

        //----dùng thêm cái này vì khi upgrade power thì quantity của đạn cũng tăng
    }

    public void Btn_MainWeaPon_Quantity()
    {

        ClickButtonBuy(GameContext.MAIN_WEAPON, GameContext.QUANTITY);
    }

    public void Btn_MainWeaPon_Speed()
    {

        ClickButtonBuy(GameContext.MAIN_WEAPON, GameContext.SPEED);
    }

    //---
    public void Btn_Life_Quantity()
    {

        ClickButtonBuy(GameContext.LIFE, GameContext.QUANTITY);
    }

    //---

    public void Btn_Laser_Power()
    {

        ClickButtonBuy(GameContext.LASER, GameContext.POWER);
    }

    public void Btn_Missile_Power()
    {

        ClickButtonBuy(GameContext.MISSILE, GameContext.POWER);
    }

    public void Btn_WindSlash_Power()
    {

        ClickButtonBuy(GameContext.WINDSLASH, GameContext.POWER);
    }

    public void Btn_SuperWeaPon_Duration()
    {

        ClickButtonBuy(GameContext.SUPER_WEAPON, GameContext.DURATION);
    }

    //---
    public void Btn_Gold_DropChance()
    {

        ClickButtonBuy(GameContext.GOLD, GameContext.DROP_CHANCE);
    }

    public void Btn_WingMan_Power()
    {

        ClickButtonBuy(GameContext.WINGMAN, GameContext.POWER);
    }

    //---
    public void Btn_SecondaryWeapon_Power()
    {

        ClickButtonBuy(GameContext.SECONDARY_WEAPON, GameContext.POWER);
    }



    //-------------------------------------Tool Tip  ------------------------------------------------

    Vector3 posToolTipPowerMainWeapon = new Vector3(0, 100f, 0);

    public void Btn_IconPowerMainWeapon()
    {
        objToolTip.SetActive(true);
        objToolTip.transform.parent = itemMainWeapon;
        //toolTipShopScr.ChangeToolTipOpen(posToolTipPowerMainWeapon, ScriptLocalization.tooltip_mainweapon_power);
    }

    Vector3 posToolTipSpeedMainWeapon = new Vector3(0, -5f, 0);

    public void Btn_IconSpeedMainWeapon()
    {
        objToolTip.SetActive(true);
        objToolTip.transform.parent = itemMainWeapon;
        //toolTipShopScr.ChangeToolTipOpen(posToolTipSpeedMainWeapon, ScriptLocalization.tooltip_mainweapon_speed);
    }

    //---------
    Vector3 posToolTipLaserPower = new Vector3(0, 199f, 0);

    public void Btn_IconLaserPower()
    {
        objToolTip.SetActive(true);
        objToolTip.transform.parent = itemSuperWeapon;
        //toolTipShopScr.ChangeToolTipOpen(posToolTipLaserPower, ScriptLocalization.tooltip_laser_power);
    }

    Vector3 posToolTipMissilePower = new Vector3(0, 93f, 0);

    public void Btn_IconMissilePower()
    {
        objToolTip.SetActive(true);
        objToolTip.transform.parent = itemSuperWeapon;
        //toolTipShopScr.ChangeToolTipOpen(posToolTipMissilePower, ScriptLocalization.tooltip_missile_power);
    }

    Vector3 posToolTipWindSlashPower = new Vector3(0, -12f, 0);

    public void Btn_IconWindSlashPower()
    {
        objToolTip.SetActive(true);
        objToolTip.transform.parent = itemSuperWeapon;
        //toolTipShopScr.ChangeToolTipOpen(posToolTipWindSlashPower, ScriptLocalization.tooltip_windslash_power);
    }

    Vector3 posToolTipDurationSuperWeapon = new Vector3(0, -118f, 0);

    public void Btn_IconDurationSuperWeapon()
    {
        objToolTip.SetActive(true);
        objToolTip.transform.parent = itemSuperWeapon;
        //toolTipShopScr.ChangeToolTipOpen(posToolTipDurationSuperWeapon, ScriptLocalization.tooltip_superweapon_duration);
    }

    //---

    Vector3 posToolTipLifeQuantity = new Vector3(0, 42f, 0);

    public void Btn_IconLifeQuantity()
    {
        objToolTip.SetActive(true);
        objToolTip.transform.parent = itemLife;
        //toolTipShopScr.ChangeToolTipOpen(posToolTipLifeQuantity, ScriptLocalization.tooltip_life_quantity);
    }

    //---

    Vector3 posToolTipGoldDropChange = new Vector3(0, 42f, 0);

    public void Btn_IconGoldDropChange()
    {
        objToolTip.SetActive(true);
        objToolTip.transform.parent = itemGold;
        //toolTipShopScr.ChangeToolTipOpen(posToolTipGoldDropChange, ScriptLocalization.tooltip_gold_dropchange);
    }

    //---

    Vector3 posToolTipWingmanPower = new Vector3(0, 42f, 0);

    public void Btn_IconWingmanPower()
    {
        objToolTip.SetActive(true);
        objToolTip.transform.parent = itemWingman;
        //toolTipShopScr.ChangeToolTipOpen(posToolTipWingmanPower, ScriptLocalization.tooltip_wingman_power);
    }

    //---

    Vector3 posToolTipSecondaryWeaponPower = new Vector3(0, 42f, 0);

    public void Btn_IconSecondaryWeaponPower()
    {
        objToolTip.SetActive(true);
        objToolTip.transform.parent = itemSecondaryWeapon;
        //toolTipShopScr.ChangeToolTipOpen(posToolTipSecondaryWeaponPower, ScriptLocalization.tooltip_secondary_weapon_power);
    }
}


