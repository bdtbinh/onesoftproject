﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using SkyGameKit;
using DG.Tweening;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using TCore;
using Sirenix.OdinInspector;
using System;

public class PopupIAPCard : MonoBehaviour
{
    Dictionary<string, int> GetNumCardByProductName = new Dictionary<string, int> {

        { EM_IAPConstants.Product_inapp_event_black_1usd, 10 },
        { EM_IAPConstants.Product_inapp_event_black_2usd, 21 },
        { EM_IAPConstants.Product_inapp_event_black_5usd, 55 },
        { EM_IAPConstants.Product_inapp_event_black_10usd, 120 },
        { EM_IAPConstants.Product_inapp_event_black_15usd, 200 },
        { EM_IAPConstants.Product_inapp_event_black_20usd, 320 },
        { EM_IAPConstants.Product_inapp_event_black_50usd, 320 },
        { EM_IAPConstants.Product_inapp_event_black_100usd, 560 },
    };

    public UILabel lTimeEnd;
    public GameObject megaComboObj;
    public GameObject standardComboObj;
    public UIScrollView scrollViewPopupIAPCard;

    public UILabel lMegaCombo100Usd, lStandardCombo50Usd;
    public UILabel lPrice_Inapp1Usd, lPrice_Inapp2Usd, lPrice_Inapp5Usd, lPrice_Inapp10Usd, lPrice_Inapp15Usd, lPrice_napp20Usd;



    void Start()
    {
        SetTextPrice();
    }


    public void SetPackStart()
    {
        if (CacheGame.ValueBuyedMegaCombo == 1)
        {
            megaComboObj.SetActive(false);
            standardComboObj.transform.localPosition = megaComboObj.transform.localPosition;
            scrollViewPopupIAPCard.ResetPosition();
        }
        if (CacheGame.ValueBuyedStandardCombo == 1)
        {
            standardComboObj.SetActive(false);

            scrollViewPopupIAPCard.ResetPosition();
        }

    }


    void SetTextPrice()
    {
        lPrice_Inapp1Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_1usd);

        lPrice_Inapp2Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_2usd);

        lPrice_Inapp5Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_5usd);

        lPrice_Inapp10Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_10usd);

        lPrice_Inapp15Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_15usd);

        lPrice_napp20Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_20usd);

        lStandardCombo50Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_50usd);

        lMegaCombo100Usd.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_100usd);

    }

    //---------------------------------------------------------------------------


    public void Btn_InApp_1USD()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_event_black_1usd, IAPCallbackManager.WherePurchase.shop);
    }

    public void Btn_InApp_2USD()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_event_black_2usd, IAPCallbackManager.WherePurchase.shop);
    }

    public void Btn_InApp_5USD()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_event_black_5usd, IAPCallbackManager.WherePurchase.shop);
    }

    public void Btn_InApp_10USD()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_event_black_10usd, IAPCallbackManager.WherePurchase.shop);
    }

    public void Btn_InApp_15USD()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_event_black_15usd, IAPCallbackManager.WherePurchase.shop);
    }

    public void Btn_InApp_20USD()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_event_black_20usd, IAPCallbackManager.WherePurchase.shop);
    }

    public void Btn_InApp_50USD()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_event_black_50usd, IAPCallbackManager.WherePurchase.shop);
    }

    public void Btn_InApp_100USD()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_event_black_100usd, IAPCallbackManager.WherePurchase.shop);
    }



    //---------------------------------------------------------------------------

    // Subscribe to IAP purchase events
    void OnEnable()
    {
        //IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
        SetPackStart();

        //StartCoroutine(CountDown());
    }

    // Unsubscribe when the game object is disabled
    void OnDisable()
    {
        //IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
        StopAllCoroutines();
    }



    void OnPurchaseSuccessed(string productName)
    {
        switch (productName)
        {

            case EM_IAPConstants.Product_inapp_event_black_1usd:
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_1usd], FirebaseLogSpaceWar.IAP_why, AircraftTypeEnum.StarBomb.ToString(), AircraftTypeEnum.StarBomb.ToString());
                ShowNotifiComplete(GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_1usd], " Starbomb Card");
                break;
            case EM_IAPConstants.Product_inapp_event_black_2usd:
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_2usd], FirebaseLogSpaceWar.IAP_why, AircraftTypeEnum.StarBomb.ToString(), AircraftTypeEnum.StarBomb.ToString());
                ShowNotifiComplete(GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_2usd], " Starbomb Card");
                break;
            case EM_IAPConstants.Product_inapp_event_black_5usd:
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_5usd], FirebaseLogSpaceWar.IAP_why, AircraftTypeEnum.StarBomb.ToString(), AircraftTypeEnum.StarBomb.ToString());
                ShowNotifiComplete(GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_5usd], " Starbomb Card");
                break;
            case EM_IAPConstants.Product_inapp_event_black_10usd:
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_10usd], FirebaseLogSpaceWar.IAP_why, AircraftTypeEnum.StarBomb.ToString(), AircraftTypeEnum.StarBomb.ToString());
                ShowNotifiComplete(GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_10usd], " Starbomb Card");
                break;
            case EM_IAPConstants.Product_inapp_event_black_15usd:
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_15usd], FirebaseLogSpaceWar.IAP_why, AircraftTypeEnum.StarBomb.ToString(), AircraftTypeEnum.StarBomb.ToString());
                ShowNotifiComplete(GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_15usd], " Starbomb Card");
                break;
            case EM_IAPConstants.Product_inapp_event_black_20usd:
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_20usd], FirebaseLogSpaceWar.IAP_why, AircraftTypeEnum.StarBomb.ToString(), AircraftTypeEnum.StarBomb.ToString());
                ShowNotifiComplete(GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_20usd], " Starbomb Card");
                break;
            case EM_IAPConstants.Product_inapp_event_black_50usd:
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddGemTop(500, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_event_black_50usd);
                    PanelCoinGem.Instance.AddCoinTop(120000, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_event_black_50usd);
                }
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_50usd], FirebaseLogSpaceWar.IAP_why, AircraftTypeEnum.StarBomb.ToString(), AircraftTypeEnum.StarBomb.ToString());
                ShowNotifiComplete(GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_50usd], " Starbomb Card" + Environment.NewLine + "500 gem" + Environment.NewLine + "120.000 gold");
                CacheGame.ValueBuyedStandardCombo = 1;
                standardComboObj.SetActive(false);
                scrollViewPopupIAPCard.ResetPosition();
                break;
            case EM_IAPConstants.Product_inapp_event_black_100usd:
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddGemTop(2300, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_event_black_100usd);
                    PanelCoinGem.Instance.AddCoinTop(750000, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_event_black_100usd);
                }
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_100usd], FirebaseLogSpaceWar.IAP_why, AircraftTypeEnum.StarBomb.ToString(), AircraftTypeEnum.StarBomb.ToString());
                ShowNotifiComplete(GetNumCardByProductName[EM_IAPConstants.Product_inapp_event_black_100usd], " Starbomb Card" + Environment.NewLine + "2.300 gem" + Environment.NewLine + "750.000 gold");
                CacheGame.ValueBuyedMegaCombo = 1;
                megaComboObj.SetActive(false);
                standardComboObj.transform.localPosition = megaComboObj.transform.localPosition;
                scrollViewPopupIAPCard.ResetPosition();
                break;
            default:
                break;
        }


    }


    void ShowNotifiComplete(int numGemAdd, string typeGift)
    {
        LeanTween.delayedCall(0.56f, (System.Action)(() =>
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast((string)(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + numGemAdd + typeGift)), (bool)true, (float)1.8f);
        })).setIgnoreTimeScale(true);
    }


    //Minh countdown:
    //IEnumerator CountDown()
    //{
    //    DateTime expiredTime = DateTime.Now;
    //    DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out expiredTime);

    //    while (DateTime.Now < expiredTime)
    //    {
    //        TimeSpan interval = expiredTime - DateTime.Now;

    //        string time = "";

    //        if (interval.Days > 0)
    //            time = "[FF0000]" + string.Format("{0:0} days:{1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
    //        else
    //            time = "[FF0000]" + string.Format("{0:0}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds) + "[-]";

    //        lTimeEnd.text = I2.Loc.ScriptLocalization.msg_premium_pack_time_expired.Replace("%{time}", time);
    //        yield return new WaitForSeconds(1f);
    //    }
    //}
}
