﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TCore;
using UnityEngine.SceneManagement;
using SkyGameKit;
using DG.Tweening;
using Firebase.Analytics;
using com.ootii.Messages;
using System;

public enum TypePanelShowInShop
{
    Upgrade,
    SpaceShip,
    IapGold,
    IapGem,
    ComboEvent,
    CardRandom,
}

public class PopupShop : Singleton<PopupShop>
{

    public GameObject popupShopObjInvi;

    //public GameObject panelUpgrade;
    public GameObject panelIAPGold;
    public GameObject panelIAPGem;
    public GameObject panelIAPEvent;
    public GameObject panelIAPCardRandom;
    //
    //public ItemUpgradeManager panelUpgradeScript;
    //


    public GameObject glowSelectBtnIAPGold;
    public GameObject glowSelectBtnIAPGem;
    public GameObject glowSelectBtnIAPEvent;
    public GameObject glowSelectBtnIAPCardRandom;

    public GameObject btnIAPEventObj;

    //[HideInInspector]
    //public GameObject glowBtnShopUse;
    //

    //public List<int> listPriceShop = new List<int>();




    void Awake()
    {
        base.Awake();
        //ConfigLoader.Load();
    }
    private void OnEnable()
    {
        //CheckFinishEvent();
    }


    void CheckFinishEvent()
    {
        DateTime endDate = DateTime.Now;
        //DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out endDate);

        Debug.LogError("PopupShop---CachePvp.dateTime" + CachePvp.dateTime + ", " + "EndDate: " + endDate);

        if (CachePvp.dateTime < endDate && DateTime.Now < endDate)
        {
            btnIAPEventObj.SetActive(true);
        }
        else
        {
            btnIAPEventObj.SetActive(false);
        }
    }







    public void BackButton()
    {
        SoundManager.PlayClickButton();
        if (SceneManager.GetActiveScene().name == "LevelTryPlane")
        {
            MainScene.Instance.LoadNewSceneFromMain("Home");
        }
        else
        {
            //SetShowGlowBtnShop();
            //DontDestroyManager.Instance.popupShop_Obj.SetActive(false);
            PopupManagerCuong.Instance.HideShopPopup();

            if (SceneManager.GetActiveScene().name != "Home" && SceneManager.GetActiveScene().name != "SelectLevel" && SceneManager.GetActiveScene().name != "PVPMain")
            {
                //PopupManagerCuong.Instance.HidePanelCoinGem();
                if (PanelUITop.Instance != null)
                {
                    PanelUITop.Instance.SetTextCoin();
                }

            }
        }
    }

    public void Upgrade_Button()
    {
        SoundManager.PlayClickButton();

        ShowPanelInShop(TypePanelShowInShop.Upgrade);
    }

    public void IAP_Gold_Button()
    {
        SoundManager.PlayClickButton();

        ShowPanelInShop(TypePanelShowInShop.IapGold);
    }

    public void IAP_Gem_Button()
    {
        SoundManager.PlayClickButton();

        ShowPanelInShop(TypePanelShowInShop.IapGem);
    }

    public void IAP_Event_Button()
    {
        SoundManager.PlayClickButton();
        ShowPanelInShop(TypePanelShowInShop.ComboEvent);
    }

    public void IAP_Card_Random()
    {
        if (!CachePvp.sCVersionConfig.canRandomCardShop)
        {
            PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainRandomCardShop);
            return;
        }
        SoundManager.PlayClickButton();

        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }

        ShowPanelInShop(TypePanelShowInShop.CardRandom);
    }


    public void ShowPanelInShop(TypePanelShowInShop typeShow = TypePanelShowInShop.Upgrade)
    {
        //popupQuestOpenIAP.SetActive(false);
        PopupManagerCuong.Instance.HidePopupSaleGoldGem();
        PopupManagerCuong.Instance.HidePopupPushInfinityPack();
        switch (typeShow)
        {
            case TypePanelShowInShop.ComboEvent:

                panelIAPEvent.SetActive(true);
                panelIAPGold.SetActive(false);
                panelIAPGem.SetActive(false);
                panelIAPCardRandom.SetActive(false);

                glowSelectBtnIAPEvent.SetActive(true);
                glowSelectBtnIAPGold.SetActive(false);
                glowSelectBtnIAPGem.SetActive(false);
                glowSelectBtnIAPCardRandom.SetActive(false);
                break;

            case TypePanelShowInShop.IapGold:
                panelIAPEvent.SetActive(false);
                panelIAPGold.SetActive(true);
                panelIAPGem.SetActive(false);
                panelIAPCardRandom.SetActive(false);

                glowSelectBtnIAPEvent.SetActive(false);
                glowSelectBtnIAPGold.SetActive(true);
                glowSelectBtnIAPGem.SetActive(false);
                glowSelectBtnIAPCardRandom.SetActive(false);
                break;

            case TypePanelShowInShop.IapGem:
                panelIAPEvent.SetActive(false);
                panelIAPGold.SetActive(false);
                panelIAPGem.SetActive(true);
                panelIAPCardRandom.SetActive(false);

                glowSelectBtnIAPEvent.SetActive(false);
                glowSelectBtnIAPGold.SetActive(false);
                glowSelectBtnIAPGem.SetActive(true);
                glowSelectBtnIAPCardRandom.SetActive(false);
                break;

            case TypePanelShowInShop.CardRandom:
                panelIAPEvent.SetActive(false);
                panelIAPGold.SetActive(false);
                panelIAPGem.SetActive(false);
                panelIAPCardRandom.SetActive(true);

                glowSelectBtnIAPEvent.SetActive(false);
                glowSelectBtnIAPGold.SetActive(false);
                glowSelectBtnIAPGem.SetActive(false);
                glowSelectBtnIAPCardRandom.SetActive(true);
                break;
        }
    }
}
