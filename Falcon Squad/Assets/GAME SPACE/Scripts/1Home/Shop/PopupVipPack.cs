﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using Firebase.Analytics;
using TCore;
using System;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using SkyGameKit;
using com.ootii.Messages;
using I2.Loc;

public class PopupVipPack : Singleton<PopupVipPack>
{
    public GameObject panelWatchAdsBuyPack;

    //public GameObject btnBuyPack;
    //public GameObject btnBuyPackSale2;

    public UILabel lPrice_BeforeSale;
    public UILabel lPrice_VipPack;

    public UILabel lPrice_BeforeSale_Sever;
    public UILabel lPrice_VipPack_Sever;
    public UILabel lPrice_VipPack_Sale; //sale khi sever trẢ về



    private bool isClickBtnPurchase = false;

    public UILabel discountPercent;
    private string productIapUse = EM_IAPConstants.Product_inapp_vippack_sale_lv2;

    public GameObject objButton20Usd, objButtonSale15Usd;
    public UILabel lVipPoint;

    void Start()
    {
        //ShowPanelBuyPackByAds();
        //productIapUse = GetProductUse();
        DelayGetLocalizeData();
    }




    void DelayGetLocalizeData()
    {
        lPrice_BeforeSale.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_vippack_beforesale);

        lPrice_VipPack.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_vippack_sale_lv2);

        //Khi sever trả về sale còn 15 usd
        lPrice_BeforeSale_Sever.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_vippack_beforesale);

        lPrice_VipPack_Sever.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_vippack_sale_lv2);

        lPrice_VipPack_Sale.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_vippack_sale_lv3);

    }

    void GetProductUse()
    {
        if (!GameContext.serverSaleVipPack)
        {
            objButton20Usd.SetActive(true);
            objButtonSale15Usd.SetActive(false);
            // Product_inapp_vippack_sale2 = 20usd
            discountPercent.text = "-60%";
            lVipPoint.text = "+20";
            productIapUse = EM_IAPConstants.Product_inapp_vippack_sale_lv2;
        }
        else
        {
            objButton20Usd.SetActive(false);
            objButtonSale15Usd.SetActive(true);
            // Product_inapp_vippack = 15usd - sever trả về
            discountPercent.text = "-75%";
            lVipPoint.text = "+20";
            productIapUse = EM_IAPConstants.Product_inapp_vippack_sale_lv3;
        }
    }


    public void Btn_Back()
    {
        SoundManager.PlayClickButton();
        //if (PopupManagerCuong.Instance != null)
        //{
        //    PopupManagerCuong.Instance.HideVipPackPopup();
        //}
    }

    public void Btn_BuyVipPack()
    {
        SoundManager.PlayClickButton();

        if (!IAPCallbackManager.Instance.isServerPush)
        {
            IAPCallbackManager.Instance.BuyProduct(productIapUse, IAPCallbackManager.WherePurchase.shop);
        }
        else
        {
            IAPCallbackManager.Instance.BuyProduct(productIapUse, IAPCallbackManager.WherePurchase.push_server);
        }
    }


    void OnEnable()
    {
        isClickBtnPurchase = false;
        GetProductUse();

        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
    }

    // Unsubscribe when the game object is disabled
    void OnDisable()
    {
        StopAllCoroutines();
        IAPCallbackManager.Instance.isServerPush = false;
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }




    void OnPurchaseSuccessed(string productName)
    {
        if (productName == productIapUse)
        {
            CallBackBuyPack();
            if (panelWatchAdsBuyPack.activeInHierarchy)
            {
                CacheGame.SetNumberWatchAdsToGetPack(0);
                panelWatchAdsBuyPack.SetActive(false);
            }
        }
    }


    public void CallBackBuyPack()
    {
        //
        CacheGame.SetSpaceShipIsUnlocked(7, 1);
        CacheGame.SetWingManIsUnlocked(3, 1);
        CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + 10);
        MinhCacheGame.SetAlreadyUnlockWeapon();
        PanelCoinGem.Instance.AddCoinTop(50000, FirebaseLogSpaceWar.IAP_why, productIapUse);

        //
        objButton20Usd.SetActive(false);
        objButtonSale15Usd.SetActive(false);

        if (PanelBtnPack.Instance != null)
        {
            PanelBtnPack.Instance.ShowButtonStart();
        }
        CacheGame.SetPurchasedVIPPack(1);
        Popup3Pack.Instance.btnVipPack.SetActive(!(CacheGame.GetPurchasedVIPPack() == 1));

        ShowNotifiComplete(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}",
            Environment.NewLine + "50.000 " + ScriptLocalization.gold
            + Environment.NewLine + "unlock Twilight x"
             + Environment.NewLine + "LaserDrone"
              + Environment.NewLine + "10 " + ScriptLocalization.ActiveSkill));

        MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI, 0f);
    }



    //
    ///---text notifi
    void ShowNotifiComplete(string text)
    {
        float ftest = 0;
        DOTween.To(() => ftest, x => ftest = x, 1f, 0.56f).SetUpdate(true).OnComplete((TweenCallback)(() =>
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast((string)text, (bool)true, (float)1.8f);
        }));
    }

}
