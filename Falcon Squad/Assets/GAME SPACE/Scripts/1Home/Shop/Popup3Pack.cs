﻿using System.Collections;
using System.Collections.Generic;
using SkyGameKit;
using TCore;
using UnityEngine;


public enum TypePackShowInPopup3Pack
{
    StarterPack,
    PremiumPack,
    VipPack,
    Config

}
public class Popup3Pack : Singleton<Popup3Pack>
{

    public PopupStarterPack popupStarterPackScript;
    public PopupPremiumPack popupPremiumPackScript;
    public PopupVipPack popupVipPackScript;

    public GameObject btnStarterPack;
    public GameObject btnPremiumPack;
    public GameObject btnVipPack;

    public GameObject glowBtnStarterPack;
    public GameObject glowBtnPremiumPack;
    public GameObject glowBtnVipPack;


    void OnEnable()
    {
        ShowButtonStart();
    }

    public void ShowButtonStart()
    {
        Debug.LogError("GetPurchasedVIPPack: " + CacheGame.GetPurchasedVIPPack() + "---GetPurchasedStarterPack: " + CacheGame.GetPurchasedStarterPack());

        btnStarterPack.SetActive(!(CacheGame.GetPurchasedStarterPack() == 1));
        btnVipPack.SetActive(!(CacheGame.GetPurchasedVIPPack() == 1));
    }


    public void ChangeDepthSerrverPush(TypePackShowInPopup3Pack typePack)
    {
        switch (typePack)
        {
            case TypePackShowInPopup3Pack.StarterPack:
                popupStarterPackScript.GetComponent<ChangeDepthLayer>().enabled = true;
                break;
            case TypePackShowInPopup3Pack.PremiumPack:
                popupPremiumPackScript.GetComponent<ChangeDepthLayer>().enabled = true;
                break;
            case TypePackShowInPopup3Pack.VipPack:
                popupVipPackScript.GetComponent<ChangeDepthLayer>().enabled = true;
                break;

        }
    }

    public void ButtonStarterPack()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();
            ShowPackInPopup(TypePackShowInPopup3Pack.StarterPack);

            FirebaseLogSpaceWar.LogClickButton("StarterPack");
        }
    }

    public void ButtonPremiumPack()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();
            //PopupManagerCuong.Instance.ShowPremiumPackPopup();
            ShowPackInPopup(TypePackShowInPopup3Pack.PremiumPack);

            FirebaseLogSpaceWar.LogClickButton("PremiumPack");
        }
    }

    public void ButtonVipPack()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();
            //PopupManagerCuong.Instance.ShowVipPackPopup();
            ShowPackInPopup(TypePackShowInPopup3Pack.VipPack);

            FirebaseLogSpaceWar.LogClickButton("VipPack");
        }
    }

    public void ButtonBack()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();
            //PopupManagerCuong.Instance.ShowStarterPackPopup();
            PopupManagerCuong.Instance.Hide3PackPopup();
        }
    }




    public void ShowPackInPopup(TypePackShowInPopup3Pack typePack = TypePackShowInPopup3Pack.Config)
    {

        switch (typePack)
        {
            case TypePackShowInPopup3Pack.StarterPack:
                popupStarterPackScript.gameObject.SetActive(true);
                popupPremiumPackScript.gameObject.SetActive(false);
                popupVipPackScript.gameObject.SetActive(false);

                glowBtnStarterPack.SetActive(true);
                glowBtnPremiumPack.SetActive(false);
                glowBtnVipPack.SetActive(false);
                break;
            case TypePackShowInPopup3Pack.PremiumPack:
                popupStarterPackScript.gameObject.SetActive(false);
                popupPremiumPackScript.gameObject.SetActive(true);
                popupVipPackScript.gameObject.SetActive(false);

                glowBtnStarterPack.SetActive(false);
                glowBtnPremiumPack.SetActive(true);
                glowBtnVipPack.SetActive(false);
                break;
            case TypePackShowInPopup3Pack.VipPack:
                popupStarterPackScript.gameObject.SetActive(false);
                popupPremiumPackScript.gameObject.SetActive(false);
                popupVipPackScript.gameObject.SetActive(true);
                //Debug.LogError("VipPack 3pack");

                glowBtnStarterPack.SetActive(false);
                glowBtnPremiumPack.SetActive(false);
                glowBtnVipPack.SetActive(true);
                break;
            case TypePackShowInPopup3Pack.Config:
                if (CacheGame.GetPurchasedStarterPack() != 1)
                {
                    ShowPackInPopup(TypePackShowInPopup3Pack.StarterPack);
                }
                else if (CacheGame.GetPurchasedVIPPack() != 1)
                {
                    ShowPackInPopup(TypePackShowInPopup3Pack.VipPack);
                }
                else
                {
                    ShowPackInPopup(TypePackShowInPopup3Pack.PremiumPack);
                }
                break;
        }


    }

}
