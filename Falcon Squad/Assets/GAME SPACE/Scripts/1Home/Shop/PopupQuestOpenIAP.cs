﻿using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PopupQuestOpenIAP : MonoBehaviour
{
    [SerializeField]
    private UILabel lMessage;

    private ResourceType resourceType;

	public void Btn_Yes_OpenIAP ()
	{
        SoundManager.PlayClickButton();
        //DontDestroyManager.Instance.popupStarterPack.SetActive (false);
        //DontDestroyManager.Instance.popupSetting_Obj.SetActive (false);

        //DontDestroyManager.Instance.popupShop_Obj.SetActive (true);
        PopupManagerCuong.Instance.ShowShopPopup();
        //DontDestroyManager.Instance.popupSelectAircraft.gameObject.SetActive(false);
        //DontDestroyManager.Instance.popupSelectWeapon.gameObject.SetActive(false);
        //DontDestroyManager.Instance.popupSelectWingman.gameObject.SetActive(false);
        if (PopupShop.Instance != null)
        {
            if (resourceType == ResourceType.Gold)
                PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.IapGold);
            else if(resourceType == ResourceType.Gem)
                PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.IapGem);
        }

        gameObject.SetActive (false);
	}

	public void Btn_No_OpenIAP ()
	{
        SoundManager.PlayClickButton();
        gameObject.SetActive (false);
	}

    public void ShowPopup(ResourceType type)
    {
        resourceType = type;
        gameObject.SetActive(true);

        switch (type)
        {
            case ResourceType.None:
                break;

            case ResourceType.Gold:
                lMessage.text = I2.Loc.ScriptLocalization.notifi_not_enough_coin;
                break;

            case ResourceType.Gem:
                lMessage.text = I2.Loc.ScriptLocalization.notifi_not_enough_gem;
                break;

            case ResourceType.Card:
                break;
            default:
                break;
        }
    }

    public void ClosePopup()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
    }
}
