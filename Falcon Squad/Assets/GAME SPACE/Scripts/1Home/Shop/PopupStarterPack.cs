﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using Firebase.Analytics;
using TCore;
using System;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.Purchasing.Security;
using SkyGameKit;
using I2.Loc;

public class PopupStarterPack : Singleton<PopupStarterPack>
{
    public GameObject panelWatchAdsBuyPack;
    public GameObject btnBuyPack;

    public UILabel lPrice_BeforeSale;
    public UILabel lPrice_StartPack;

    private bool isClickBtnPurchase = false;

    //
    public UILabel discountPercent;
    private string productIapUse = EM_IAPConstants.Product_inapp_starterpack_v2;



    void Start()
    {

        productIapUse = GetProductUse();

        lPrice_BeforeSale.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gem_20usd);
        lPrice_StartPack.text = "" + CuongUtils.GetIapLocalizedPrice(productIapUse);

    }


    string GetProductUse()
    {
        discountPercent.text = "-85%";
        return EM_IAPConstants.Product_inapp_starterpack_v2;
    }



    public void Btn_Back()
    {
        SoundManager.PlayClickButton();
    }

    public void Btn_BuyStarterPack()
    {
        SoundManager.PlayClickButton();

        if (!IAPCallbackManager.Instance.isServerPush)
        {
            IAPCallbackManager.Instance.BuyProduct(productIapUse, IAPCallbackManager.WherePurchase.shop);
        }
        else
        {
            IAPCallbackManager.Instance.BuyProduct(productIapUse, IAPCallbackManager.WherePurchase.push_server);
        }

    }

    void OnEnable()
    {
        isClickBtnPurchase = false;
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
    }

    // Unsubscribe when the game object is disabled
    void OnDisable()
    {
        StopAllCoroutines();
        IAPCallbackManager.Instance.isServerPush = false;
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }



    void OnPurchaseSuccessed(string productName)
    {
        if (productName == productIapUse)
        {
            CallBackBuyPack();
            if (panelWatchAdsBuyPack.activeInHierarchy)
            {
                CacheGame.SetNumberWatchAdsToGetPack(0);
                panelWatchAdsBuyPack.SetActive(false);
            }
        }
    }


    public void CallBackBuyPack()
    {
        //CacheGame.SetSpaceShipIsUnlocked(2, 1);
        CacheGame.SetPurchasedStarterPack(1);
        CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + 10);
        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.AddCoinTop(20000, FirebaseLogSpaceWar.IAP_why, productIapUse);
            PanelCoinGem.Instance.AddGemTop(800, FirebaseLogSpaceWar.IAP_why, productIapUse);
        }

        btnBuyPack.SetActive(false);
        ShowNotifiComplete(ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " " + Environment.NewLine + "20.000 " + ScriptLocalization.gold + Environment.NewLine + "800 " + ScriptLocalization.gem + Environment.NewLine + "10 " + ScriptLocalization.life));
        if (PanelBtnPack.Instance != null)
        {
            PanelBtnPack.Instance.ShowButtonStart();
        }
        Popup3Pack.Instance.btnStarterPack.SetActive(!(CacheGame.GetPurchasedStarterPack() == 1));

    }

    ///---text notifi
    void ShowNotifiComplete(string text)
    {
        LeanTween.delayedCall(0.56f, (Action)(() =>
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast((string)text, (bool)true, (float)1.8f);
        })).setIgnoreTimeScale(true);
    }
}
