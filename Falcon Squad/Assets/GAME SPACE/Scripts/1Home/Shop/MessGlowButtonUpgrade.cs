﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class MessGlowButtonUpgrade : MonoBehaviour {


    public GameObject glowInButton;



    private void OnEnable()
    {
        MessageDispatcher.AddListener(GameContext.GLOW_BTN_UPGRADE_EVENT, OnGlowEvent, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(GameContext.GLOW_BTN_UPGRADE_EVENT, OnGlowEvent, true);
        glowInButton.SetActive(false);
    }


    bool showGlow;
    void OnGlowEvent(IMessage msg)
    {
        showGlow = (bool)msg.Data;

        if (showGlow)
        {
            glowInButton.SetActive(true);
        }
        else
        {
            glowInButton.SetActive(false);
        }
    }



}
