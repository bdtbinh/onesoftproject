﻿using EasyMobile;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class PopupPushTicket : MonoBehaviour
{

    int numTicket_Add = 12;

    public GameObject buttonBuyObj;
    public UILabel lPrice_TicketPack;

    void Start()
    {
        lPrice_TicketPack.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_luckywheel);
    }

    public void Btn_InApp_Ticket()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_luckywheel, IAPCallbackManager.WherePurchase.push_server);
    }

    public void Btn_Back()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.HidePopupPushTicketLuckyPack();
    }


    void OnEnable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
    }

    void OnDisable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }


    void OnPurchaseSuccessed(string productName)
    {
        switch (productName)
        {
            case EM_IAPConstants.Product_inapp_luckywheel:
                MinhCacheGame.AddSpinTickets(numTicket_Add);
                buttonBuyObj.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast((string)(ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + numTicket_Add + " " + ScriptLocalization.ticket)), (bool)true, (float)1.8f);
                break;
            default:
                break;
        }
    }
}
