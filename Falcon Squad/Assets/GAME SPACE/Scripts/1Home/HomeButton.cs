﻿using UnityEngine;
using System.Collections;
using TCore;
using UnityEngine.SceneManagement;
using Firebase.Analytics;
using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using SkyGameKit;

public class HomeButton : MonoBehaviour
{

    public void ButtonPlay()
    {
        SoundManager.PlayClickButton();
        if (!CachePvp.sCVersionConfig.canStart)
        {
            PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainStart);
            return;
        }
        FirebaseLogSpaceWar.LogClickButton("StartHome");
        LoadingSceneManager.Instance.LoadSceneHomeUI("SelectLevel");
        //LoadingSceneManager.Instance.LoadSceneHomeUI("SelectLevelParallax");
    }

    public void OnClickButtonExtra()
    {
        SoundManager.PlayClickButton();
        if (!CachePvp.sCVersionConfig.canExtra)
        {
            PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainRank);
            return;
        }
        PopupManager.Instance.HideChatWorldPopup();
        HomeScene.Instance.popupExtra.SetActive(true);
        FirebaseLogSpaceWar.LogClickButton("ExtraHome");
    }

    public void ButtonSetup()
    {
        SoundManager.PlayShowPopup();
        PopupManagerCuong.Instance.ShowSettingPopup();
        FirebaseLogSpaceWar.LogClickButton("Setting");
    }

    public void ButtonShop()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();
            PopupManagerCuong.Instance.ShowShopPopup();
            if (PopupShop.Instance != null)
            {
                if (CachePvp.FirstTabIndex == 1)
                {
                    PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.Upgrade);
                }
                else
                {
                    PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.Upgrade);
                }
            }
        }
    }


    public void Button_DailyLogin()
    {
        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();
            if (CacheGame.GetClaimedAllGiftDailyLogin() != 1)
            {
                HomeScene.Instance.popupDailyLoginObj.SetActive(true);
                HomeScene.Instance.scripDailyLogin.SetDataStartPopup();
            }
            else
            {
                HomeScene.Instance.popupThirtyDaysLogin.ShowPopup();
            }
        }
        FirebaseLogSpaceWar.LogClickButton("DailyLogin");
    }

    public void Button_ShowVip()
    {
        PopupManager.Instance.ShowVipPopup();
        FirebaseLogSpaceWar.LogClickButton("Vip");
    }

    public void Button_AddCard_Test()
    {
        for (int i = 1; i <= GameContext.TOTAL_AIRCRAFT; i++)
        {
            MinhCacheGame.SetSpaceShipCardsNamNX((AircraftTypeEnum)i, MinhCacheGame.GetSpaceShipCards((AircraftTypeEnum)i) + 10);
        }
        MinhCacheGame.SetSpaceShipGeneralCardsNamNX(MinhCacheGame.GetSpaceShipGeneralCards() + 20);

        for (int i = 1; i <= GameContext.TOTAL_WINGMAN; i++)
        {
            MinhCacheGame.SetWingmanCardsNamNX((WingmanTypeEnum)i, MinhCacheGame.GetWingmanCards((WingmanTypeEnum)i) + 10);
        }
        MinhCacheGame.SetWingmanGeneralCardsNamNX(MinhCacheGame.GetWingmanGeneralCards() + 20);

        for (int i = 1; i <= GameContext.TOTAL_WING; i++)
        {
            MinhCacheGame.SetWingCardsNamNX((WingTypeEnum)i, MinhCacheGame.GetWingCards((WingTypeEnum)i) + 80);
        }
        MinhCacheGame.SetWingGeneralCardsNamNX(MinhCacheGame.GetWingGeneralCards() + 80);
    }



    public void ButtonTestInterAds()
    {
        OsAdsManager.Instance.ShowInterstitialAds(OsAdsManager.LocationClickInterstitial.unknown);
    }
    //
    public void ButtonTestVideoAds()
    {
        GameContext.IsAdmobHigherPrice();

        if (OsAdsManager.Instance.isRewardedVideoAvailable())
        {
            OsAdsManager.Instance.AddRewardedListenner(CallBackFinishVideoAds);
            OsAdsManager.Instance.AddClosedListenner(CallBackClosedVideoAds);
            //
            OsAdsManager.Instance.ShowRewardedAds(OsAdsManager.LocationClickVideo.btn_video_Top);
        }
    }
    void CallBackFinishVideoAds()
    {
        Debug.Log("CallBackFinishVideoAds");
    }

    void CallBackClosedVideoAds()
    {
        Debug.Log("CallBackClosedVideoAds");
    }

    //

    public void ButtonUnlockTotalLevel()
    {
        //PlayerPrefs.DeleteAll();

        //CacheGame.SetRemainWatchAdsToGetPackToday(3);
        CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, GameContext.TOTAL_LEVEL);
        for (int i = 1; i <= 20; i++)
        {
            CacheGame.SetNumStarLevel(i, 1, 1, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal);
        }

    }

    UI2DSprite a;
    public void ButtonClearData()
    {
        PlayerPrefs.DeleteAll();
        //CacheGame.SetTotalCoin(1000000);
    }


    int addlist = 0;
    public void Get1000Gold()
    {

        CacheGame.SetTotalCoin(CacheGame.GetTotalCoin() + 100000, "test", "test");
        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.AddCoinTop();
        }

        CacheGame.SetTotalGem(CacheGame.GetTotalGem() + 500, "test", "test");
        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.AddGemTop();
        }
        CacheGame.SetTotalEnergy(CacheGame.GetTotalEnergy() + 10000, "test", "test");

    }


    public int id;
    public int url2 = 1;
    public string urlImage;
    public int type;
    public List<PromotionItem> promotionItems;
    int indexTest = 1;
    int indexTest2 = 1;
    public void SaleGem()
    {
        Debug.LogError("indexTest: " + "25," + indexTest);
        PopupManager.Instance.ShowNotifyPopup(id, "25," + indexTest, "test mess", "", 1, promotionItems);
        //url2++;
        indexTest++;
        //CachePvp.timeFromServer += 0;
    }

    public void SaleGold()
    {
        Debug.LogError("indexTest: " + "25," + indexTest);
        PopupManager.Instance.ShowNotifyPopup(id, "26," + indexTest2, "test mess", "", 1, promotionItems);
        //url2++;
        indexTest2++;
        //CachePvp.timeFromServer += 0;
    }
    int indexTestCard = 0;

    public void SaleCard5()
    {
        Debug.LogError("indexTestCard: " + "29," + 5 + "," + indexTestCard);
        PopupManager.Instance.ShowNotifyPopup(id, "29,5," + indexTestCard, "test mess", "", 1, promotionItems);
        //url2++;
        indexTestCard++;
        if (indexTestCard > GameContext.TOTAL_AIRCRAFT)
        {
            indexTestCard = 1;
        }
        if (indexTestCard == 4 || indexTestCard == 5)
        {
            indexTestCard = 6;
        }
        //CachePvp.timeFromServer += 0;
    }
    public void SaleCard10()
    {
        Debug.LogError("indexTestCard: " + "29," + 10 + "," + indexTestCard);
        //url2++;
        this.Delay(3.6f, () =>
        {
            PopupManager.Instance.ShowNotifyPopup(id, "29,20," + indexTestCard, "test mess", "", 1, promotionItems);
        }, true);
        indexTestCard++;
        if (indexTestCard > GameContext.TOTAL_AIRCRAFT)
        {
            indexTestCard = 1;
        }
        if (indexTestCard == 4 || indexTestCard == 5)
        {
            indexTestCard = 6;
        }
        //CachePvp.timeFromServer += 0;
    }

    int indexAircraftTest = 2;

    public void SaleAircraft25()
    {
        Debug.LogError("indexTestCard: " + "30,25," + indexAircraftTest);
        PopupManager.Instance.ShowNotifyPopup(id, "30,25," + indexAircraftTest, "test mess", "", 1, promotionItems);
        //url2++;
        indexAircraftTest++;
        if (indexAircraftTest > GameContext.TOTAL_AIRCRAFT)
        {
            indexAircraftTest = 1;
        }
        if (indexAircraftTest == 4 || indexAircraftTest == 5)
        {
            indexAircraftTest = 6;
        }
    }
    public void SaleAircraft50()
    {
        Debug.LogError("indexTestCard: " + "30,50," + indexAircraftTest);
        PopupManager.Instance.ShowNotifyPopup(id, "30,50," + indexAircraftTest, "test mess", "", 1, promotionItems);
        //url2++;
        indexAircraftTest++;
        if (indexAircraftTest > GameContext.TOTAL_AIRCRAFT)
        {
            indexAircraftTest = 1;
        }
        if (indexAircraftTest == 4 || indexAircraftTest == 5)
        {
            indexAircraftTest = 6;
        }
    }

    public void PushInfinity()
    {
        //Debug.LogError("indexTest: " + "25," + indexTest);
        this.Delay(2.6f, () =>
        {
            PopupManager.Instance.ShowNotifyPopup(id, "28", "test mess", "", 1, promotionItems);
            //
        }, true);
    }

    public void TestPopupShowItem()
    {
        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(5);

        popupItemRewardIns.ShowOneItem(1, GameContext.TypeItemInPopupItemReward.Aircraft1Card, 1);
        popupItemRewardIns.ShowOneItem(2, GameContext.TypeItemInPopupItemReward.Gem, 11);
        popupItemRewardIns.ShowOneItem(3, GameContext.TypeItemInPopupItemReward.EMP, 21);
        popupItemRewardIns.ShowOneItem(4, GameContext.TypeItemInPopupItemReward.AircraftGeneralCard, 31);
        popupItemRewardIns.ShowOneItem(5, GameContext.TypeItemInPopupItemReward.Aircraft7Card, 2);
    }
    public void MaxAircraft()
    {
        for (int i = 0; i < GameContext.TOTAL_AIRCRAFT + 1; i++)
        {
            CacheGame.SetSpaceShipIsUnlocked(i, 1);
            CacheGame.SetSpaceShipRank((AircraftTypeEnum)i, Rank.SSS);
            CacheGame.SetSpaceShipLevel((AircraftTypeEnum)i, 199);
        }
        for (int i = 0; i < GameContext.TOTAL_WINGMAN + 1; i++)
        {
            CacheGame.SetWingManIsUnlocked(i, 1);
            CacheGame.SetWingmanRank((WingmanTypeEnum)i, Rank.SSS);
            CacheGame.SetWingmanLevel((WingmanTypeEnum)i, 199);
        }
    }







    public void OnClickBtnPromoteEvent()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowPromoteEventPopup();
    }

}
