﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScalePopup : MonoBehaviour
{
	public Vector3 scaleScreenLong = new Vector3 (0.938f, 0.95f, 1f);
	public Vector3 scaleScreenNormal = new Vector3 (1f, 1f, 1f);
	public Vector3 scaleScreenNormalWide = new Vector3 (0.91f, 0.89f, 1f);
	public Vector3 scaleScreenWide = new Vector3 (1.11f, 1f, 1f);
	public Vector3 scaleScreenBigWide = new Vector3 (0.75f, 0.68f, 1f);

	void Start ()
	{

        //Debug.LogError ((float)Screen.height / (float)Screen.width);
        CacheGame.SetRatioScreen((float)Screen.height / (float)Screen.width);

        //

        SetScaleScreen ();
	}

	//	void Update ()
	//	{
	//		CacheGame.SetRatioScreen ((float)Screen.height / (float)Screen.width);
	//		//
	//		SetScaleScreen ();
	//	}

	void SetScaleScreen ()
	{
		if (CacheGame.GetRatioScreen () > 1.9f) {
			transform.localScale = scaleScreenLong;
		} else if (CacheGame.GetRatioScreen () >= 1.72f) {
			transform.localScale = scaleScreenNormal;
		} else if (CacheGame.GetRatioScreen () >= 1.65f) {
			transform.localScale = scaleScreenNormalWide;
		} else if (CacheGame.GetRatioScreen () >= 1.45f) {
			transform.localScale = scaleScreenWide;
		} else {
			transform.localScale = scaleScreenBigWide;
		}
	}
}
