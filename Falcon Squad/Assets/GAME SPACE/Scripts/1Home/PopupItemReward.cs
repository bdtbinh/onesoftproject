﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupItemReward : MonoBehaviour
{
    public Vector3 posListItemChan, posListItemLe;
    public Transform transListAllItem;
    public TweenAlpha tweenAlphaAllItem;
    public TweenPosition tweenPositionAllItem;
    [Title("LIST ITEM", bold: true, horizontalLine: true)]


    public ItemRewardShow[] listRateItemDrop;
    [System.Serializable]
    public class ItemRewardShow
    {
        [Tooltip("item hiển thị lên trong popup")]
        public UILabel lNumItem;
        public UISprite sItem;
    }

    public void Btn_Clamp()
    {
        PopupManagerCuong.Instance.HideItemRewardPopup();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            PopupManagerCuong.Instance.HideItemRewardPopup();
        }
    }

    public void ShowListItem(int totalItem)
    {
        tweenAlphaAllItem.enabled = true;
        tweenPositionAllItem.enabled = true;
        tweenAlphaAllItem.ResetToBeginning();
        tweenPositionAllItem.ResetToBeginning();

        if (totalItem % 2 == 0)
        {
            transListAllItem.localPosition = posListItemChan;
        }
        else
        {
            transListAllItem.localPosition = posListItemLe;
        }
        for (int i = 0; i < listRateItemDrop.Length; i++)
        {
            if (i < totalItem)
            {
                listRateItemDrop[i].sItem.gameObject.SetActive(true);
            }
            else
            {
                listRateItemDrop[i].sItem.gameObject.SetActive(false);
            }
        }
    }


    public void ShowOneItem(int indexItem, GameContext.TypeItemInPopupItemReward typeItem, int numItem)
    {

        listRateItemDrop[indexItem - 1].lNumItem.text = "" + GameContext.FormatNumber(numItem) ;
        //listRateItemDrop[indexItem - 1].sItem.spriteName = "" + numItem;
        SetItemSprite(typeItem, listRateItemDrop[indexItem - 1].sItem);

    }


    void SetItemSprite(GameContext.TypeItemInPopupItemReward typeItem, UISprite sItem)
    {
        switch (typeItem)
        {
            case GameContext.TypeItemInPopupItemReward.Gold:
                sItem.spriteName = "icon_coin";
                break;
            case GameContext.TypeItemInPopupItemReward.Gem:
                sItem.spriteName = "icon_daily_quest_gem";
                break;
            case GameContext.TypeItemInPopupItemReward.PowerUp:
                sItem.spriteName = "icon_dailyquest_powerup";
                break;
            case GameContext.TypeItemInPopupItemReward.EMP:
                sItem.spriteName = "icon_dailyquest_time";
                break;
            case GameContext.TypeItemInPopupItemReward.Life:
                sItem.spriteName = "icon_dailyquest_life";
                break;
            case GameContext.TypeItemInPopupItemReward.AircraftGeneralCard:
                sItem.spriteName = "card_A0";
                break;
            case GameContext.TypeItemInPopupItemReward.Aircraft1Card:
                sItem.spriteName = "card_A1";
                break;
            case GameContext.TypeItemInPopupItemReward.Aircraft2Card:
                sItem.spriteName = "card_A2";
                break;
            case GameContext.TypeItemInPopupItemReward.Aircraft3Card:
                sItem.spriteName = "card_A3";
                break;
            case GameContext.TypeItemInPopupItemReward.Aircraft6Card:
                sItem.spriteName = "card_A6";
                break;
            case GameContext.TypeItemInPopupItemReward.Aircraft7Card:
                sItem.spriteName = "card_A7";
                break;
            case GameContext.TypeItemInPopupItemReward.Aircraft8Card:
                sItem.spriteName = "card_A8";
                break;
            case GameContext.TypeItemInPopupItemReward.Aircraft9Card:
                sItem.spriteName = "card_A9";
                break;
            case GameContext.TypeItemInPopupItemReward.DroneGeneralCard:
                sItem.spriteName = "card_D0";
                break;
            case GameContext.TypeItemInPopupItemReward.Drone1Card:
                sItem.spriteName = "card_D1";
                break;
            case GameContext.TypeItemInPopupItemReward.Drone2Card:
                sItem.spriteName = "card_D2";
                break;
            case GameContext.TypeItemInPopupItemReward.Drone3Card:
                sItem.spriteName = "card_D3";
                break;
            case GameContext.TypeItemInPopupItemReward.Drone4Card:
                sItem.spriteName = "card_D4";
                break;
            case GameContext.TypeItemInPopupItemReward.Drone5Card:
                sItem.spriteName = "card_D5";
                break;
            case GameContext.TypeItemInPopupItemReward.Drone6Card:
                sItem.spriteName = "card_D6";
                break;
            case GameContext.TypeItemInPopupItemReward.unknown:
                Debug.LogError("Chưa có ảnh item này trong atlas DailyAchive");
                break;
            case GameContext.TypeItemInPopupItemReward.WingGeneralCard:
                sItem.spriteName = "card_W0";
                break;
            case GameContext.TypeItemInPopupItemReward.Wing1Card:
                sItem.spriteName = "card_W1";
                break;
            case GameContext.TypeItemInPopupItemReward.Wing2Card:
                sItem.spriteName = "card_W2";
                break;
            case GameContext.TypeItemInPopupItemReward.Wing3Card:
                sItem.spriteName = "card_W3";
                break;
            case GameContext.TypeItemInPopupItemReward.Energy:
                sItem.spriteName = "icon_daily_quest_enegy";
                break;
        }
    }
}
