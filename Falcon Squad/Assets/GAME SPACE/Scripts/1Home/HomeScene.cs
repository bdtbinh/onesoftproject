﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
//using Facebook.Unity;
using TCore;
using System.Collections.Generic;
using PathologicalGames;
using UnityEngine.SceneManagement;


using Firebase.Analytics;
using SkyGameKit;
using I2.Loc;
using System;
using EasyMobile;
//using EasyMobile.Demo;

public class HomeScene : MonoBehaviour
{
    public GameObject btnNormalExtra;
    public GameObject btnExtraEvent;

    public GameObject popupExtra;
    public GameObject popupLuckyBox_Invi;
    //
    public GameObject popupDailyLoginObj;
    public PopupThirtyDaysLogin popupThirtyDaysLogin;
    public PopupDailyLogin scripDailyLogin;

    public GameObject dailyLoginBtn;
    public GameObject popupCommunity;
    //
    public GameObject popupLuckyWheel;

    public GameObject btnLuckyWheelObj;
    public GameObject btnOfferWallObj;

    //Black Friday
    //public GameObject popupBlackFriday;

    //nếu max level nhỏ hơn 6 thì để ảnh này ở trạng thái disable
    public UISprite sBtnEndless;

    //public PopupConvertNewSystem popupConvertNewSystem;

    private static HomeScene _instance;

    public static HomeScene Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<HomeScene>();
            return _instance;
        }
    }

    string star;
    //
    void Awake()
    {
        //MinhCacheGame.AddSpinTickets(100);
        GameContext.isInGameCampaign = false;
        GameContext.levelCampaignMode = GameContext.LevelCampaignMode.Normal;

        //CachePvp.myVipPoint = 236;
        //CacheGame.SetSpaceShipIsUnlocked(7, 1);
        //CacheGame.SetSpaceShipRank(AircraftTypeEnum.TwilightX, Rank.SS);
        //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.TwilightX, 180);
        //star = "ListStar:--";
        //for (int i = 1; i < 70; i++)
        //{
        //    //Debug.LogError(i);
        //    star += "{level" + i + ": ";
        //    for (int j = 1; j < 4; j++)
        //    {
        //        star += "-" + CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_NOMAL);
        //    }
        //    star += "-} - ";

        //}
        //Debug.Log(star);
        CheckToShowButtonEvent();
    }

    //Check show button Halloween:
    private void CheckToShowButtonEvent()
    {
        DateTime endDate = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out endDate);

        if (CachePvp.dateTime < endDate && DateTime.Now < endDate)
        {
            btnExtraEvent.SetActive(true);
            btnNormalExtra.SetActive(false);
        }
        else
        {
            btnExtraEvent.SetActive(false);
            btnNormalExtra.SetActive(true);
        }
    }


    void Start()
    {
        //CachePvp.myVipPoint += 1100;
        //MinhCacheGame.SetSpinTickets(50);
        //Debug.LogError("AlreadyPremiumPack:" + MinhCacheGame.IsAlreadyPurchasePremiumPack() + "--ExpiredTime+" + MinhCacheGame.GetPremiumPackTimeExpiredTime());

        readyClickBack = true;
        GameContext.readyClickShowPopup = false;

        StartCoroutine("ReadyClickButtonShowPopup");
        Time.timeScale = 1f;

        SoundManager.StopMainBG();
        SoundManager.StopMain2BG();
        SoundManager.PlayHomeBG();

        ChangeDataByDate();
        LoadDataStart();
        AutoShowPoupStart();

        //this.Delay(1f, () =>
        //{
        //    CacheGame.SetFirtSessionGame(1);
        //});


        GameContext.typeShowBarFromScene = GameContext.TypeShowBarFromScene.Home;

        //RestAPIClient.WhenHasOfferWall += WhenHasOfferWall;

        new CSSceneLoaded(0).Send();

        //PlayerPrefs.Save();
        //CachePvp.Instance.LogAds("VideoAds", "test");
        //Debug.LogError("GetLevelUnlockOfferWall:" + CacheFireBase.GetLevelUnlockOfferWall + "---GetVersionUnlockOfferWall:" + CacheFireBase.GetVersionUnlockOfferWall + "---GetUrlGiftCode:" + CacheFireBase.GetUrlGiftCode);
    }

    private void WhenHasOfferWall(bool obj)
    {
        if (CacheGame.GetMaxLevel3Difficult() > CacheFireBase.GetLevelUnlockOfferWall && CacheFireBase.GetVersionUnlockOfferWall <= GameContext.VERSION_OFFERWALL_NOW)
        {
            btnOfferWallObj.SetActive(obj);
            btnOfferWallObj.GetComponent<ButtonTopRightHome>().canShow = obj;
        }
    }

    //private void OnDestroy()
    //{
    //    RestAPIClient.WhenHasOfferWall -= WhenHasOfferWall;
    //}

    //-------------------------


    private bool readyClickBack;

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && readyClickBack && GameContext.readyClickShowPopup)
        {
            StartCoroutine("ReadyClickBack");
            SoundManager.PlayShowPopup();

            if (PopupManagerCuong.Instance.IsActivePopupLoadingPurchase())
            {
                return;
            }
            if (PopupManager.Instance.IsSyncPopupActive())
            {
                return;
            }
            if (PopupManager.Instance.IsNotifyPopupActive())
            {
                return;
            }
            if (PopupManager.Instance.IsForceUpdatePopupActive())
            {
                PopupManager.Instance.HideForceUpdatePopup();
            }
            else if (PopupManager.Instance.IsChatWorldPopupActive())
            {

            }
            else if (PopupManagerCuong.Instance.IsActiveItemInfoPopup())
            {
                PopupManagerCuong.Instance.HideItemInfoPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveItemRewardPopup())
            {
                //PopupManagerCuong.Instance.HideItemRewardPopup(); //Phải bấm confirm mới tắt popup
            }
            else if (PopupManagerCuong.Instance.IsActiveQuestOpenIAPPopup())
            {
                PopupManagerCuong.Instance.HideQuestOpenIAPPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveShopPopup())
            {
                if (PopupManager.Instance.IsVipPopupActive() && !PopupVip.OpenFromHome)
                {
                    if (PopupManager.Instance.IsVipInfoActive())
                        PopupManager.Instance.HideVipInfo();
                    else
                        PopupManager.Instance.HideVipPopup();
                }
                else
                    PopupManagerCuong.Instance.HideShopPopup();
            }
            //
            else if (PopupManagerCuong.Instance.IsActivePopupPushInfinityPack())
            {
                PopupManagerCuong.Instance.HidePopupPushInfinityPack();
            }
            else if (PopupManagerCuong.Instance.IsActivePopupPushTicketLuckyPack())
            {
                PopupManagerCuong.Instance.HidePopupPushTicketLuckyPack();
            }
            else if (PopupManagerCuong.Instance.IsActivePopupSaleAircraft())
            {
                PopupManagerCuong.Instance.HidePopupSaleAircraft();
            }
            else if (PopupManagerCuong.Instance.IsActivePopupSaleGoldGem())
            {
                PopupManagerCuong.Instance.HidePopupSaleGoldGem();
            }
            //
            else if (PopupManager.Instance.IsPVP2V2PopupActive())
            {

            }
            else if (PopupManager.Instance.IsClaimPopupActive())
            {
                //PopupManager.Instance.HideVideoRewardPopup();
            }
            else if (PopupManager.Instance.IsVideoRewardPopupActive())
            {
                PopupManager.Instance.HideVideoRewardPopup();
            }
            else if (PopupManager.Instance.IsInvitePopupActive())
            {
                PopupManager.Instance.HideInvitePopup(true);
            }
            else if (PopupManagerCuong.Instance.IsActiveGiftCodePopup())
            {
                PopupManagerCuong.Instance.HideGiftCodePopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSettingPopup())
            {
                PopupManagerCuong.Instance.HideSettingPopup();
                PopupManager.Instance.ShowChatWorldPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveQAPopup())
            {
                PopupManagerCuong.Instance.HideQAPopup();
                PopupManager.Instance.ShowChatWorldPopup();
            }
            else if (PopupManagerCuong.Instance.IsActive3PackPopup())
            {
                PopupManagerCuong.Instance.Hide3PackPopup();
                PopupManager.Instance.ShowChatWorldPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveRoyaltyPopup())
            {
                PopupManagerCuong.Instance.HideRoyaltyPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveCardPackPopup())
            {
                PopupManagerCuong.Instance.HideCardPackPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSelectAircraftPopup())
            {
                PopupManagerCuong.Instance.HideSelectAircraftPopup();
                PopupManager.Instance.ShowChatWorldPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSelectWingmanPopup())
            {
                PopupManagerCuong.Instance.HideSelectWingmanPopup();
                PopupManager.Instance.ShowChatWorldPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSelectWingPopup())
            {
                PopupManagerCuong.Instance.HideSelectWingPopup();
                PopupManager.Instance.ShowChatWorldPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveSelectLevelPopup())
            {
                PopupManagerCuong.Instance.HideSelectLevelPopup();
            }
            //
            else if (popupLuckyWheel.activeInHierarchy)
            {
                popupLuckyWheel.SetActive(false);
                PopupManager.Instance.ShowChatWorldPopup();
            }
            else if (popupCommunity.activeInHierarchy)
            {
                popupCommunity.SetActive(false);
                PopupManager.Instance.ShowChatWorldPopup();
            }
            else if (popupDailyLoginObj.activeInHierarchy)
            {
                popupDailyLoginObj.SetActive(false);
            }
            else if (popupThirtyDaysLogin.gameObject.activeInHierarchy)
            {
                if (!ThirtyDaysRewardPanel.isShowed)
                {
                    popupThirtyDaysLogin.gameObject.SetActive(false);
                }
            }
            //

            else if (popupLuckyBox_Invi.activeInHierarchy)
            {
                popupLuckyBox_Invi.transform.parent.GetComponent<PopupLuckyBox>().OnClickBtnBackOnDevice();
            }

            else if (PopupManager.Instance.IsClanPopupActive())
            {
                Debug.LogError("BackDevice IsClanPopupActive");
            }
            else if (PopupManager.Instance.IsGloryChestPopupActive())
            {
                Debug.LogError("BackDevice IsGloryChestPopupActive");
            }
            else if (PopupManager.Instance.IsFollowPopupActive())
            {
                Debug.LogError("BackDevice IsFollowPopupActive");
            }
            else if (PopupManager.Instance.IsFriendsRequestPopupActive())
            {
                Debug.LogError("BackDevice IsFriendsRequestPopupActive");
            }
            else if (PopupManager.Instance.IsProfilePopupOnTopActive())
            {
                Debug.LogError("BackDevice IsProfilePopupOnTopActive");
            }
            else if (PopupManager.Instance.IsProfilePopupActive())
            {
                Debug.LogError("BackDeviceIsProfilePopupActive");
            }
            else if (PopupManager.Instance.IsRankingPopupActive())
            {
                PopupManager.Instance.HideRankingPopup();
            }
            else if (PopupManager.Instance.IsRegisterPopupActive())
            {
                PopupManager.Instance.HideRegisterPopup();
            }
            else if (PopupManager.Instance.IsVipInfoActive())
            {
                PopupManager.Instance.HideVipInfo();
            }
            else if (PopupManager.Instance.IsVipPopupActive())
            {
                PopupManager.Instance.HideVipPopup();
            }
            else if (PopupManager.Instance.IsCongratulationVipActive())
            {
                PopupManager.Instance.HideCongratulationVip();
            }
            else if (PopupManager.Instance.IsMailPopupActive())
            {
                //PopupManager.Instance.HideMailPopup();
            }
            //
            //----halloween-----
            else if (PopupManagerCuong.Instance.IsActivePromoteEventPopup())
            {
                PopupManagerCuong.Instance.HidePromoteEventPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveBuyEventItemPopup())
            {
                PopupManagerCuong.Instance.HideBuyEventItemPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveEventPopup())
            {
                PopupManagerCuong.Instance.HideEventPopup();
            }
            else if (PopupManager.Instance.IsSelectAircraftPopupActive() || PopupManager.Instance.IsSelectWingmanPopupActive() || PopupManager.Instance.IsSelectWingPopupActive() || PopupManager.Instance.IsSelectCardTypePopupActive())
            {

            }
            else if (PopupManager.Instance.IsLiveScorePopupActive())
            {
                PopupManager.Instance.HideLiveScorePopup();
            }
            else if (PopupManager.Instance.IsBonusRewardPopupActive())
            {
                PopupManager.Instance.HideBonusRewardPopup();
            }
            else if (PopupManager.Instance.IsBuyTicketPopupActive())
            {
                PopupManager.Instance.HideBuyTicketPopup();
            }
            else if (PopupManager.Instance.IsTournamentPopupActive())
            {
                PopupManager.Instance.HideTournamentPopup();
            }
            else if (popupExtra.activeInHierarchy)
            {
                popupExtra.SetActive(false);
                PopupManager.Instance.ShowChatWorldPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveNotifiFinishEventPopup())
            {
                PopupManagerCuong.Instance.HideNotifiFinishEventPopup();
            }

            else if (PopupManagerCuong.Instance.IsActiveExitGamePopup())
            {
                PopupManagerCuong.Instance.HideExitGamePopup();
            }
            else
            {
                PopupManagerCuong.Instance.ShowExitGamePopup();
            }
        }
        //Debug.LogError("isHasOfferWall: " + OfferWallController.Instance.isHasOfferWall + "------GetLevelUnlockOfferWall: " + FireBaseRemote.Instance.GetLevelUnlockOfferWall());
    }

    IEnumerator ReadyClickBack()
    {
        readyClickBack = false;
        yield return new WaitForSecondsRealtime(0.38f);
        readyClickBack = true;
    }


    IEnumerator ReadyClickButtonShowPopup()
    {
        yield return new WaitForSeconds(0.68f);
        GameContext.readyClickShowPopup = true;
        Debug.Log("GameContext.readyClickShowPopup : " + GameContext.readyClickShowPopup);
        //GameContext.isLoadedHome = true;
    }

    public void SetActiveBtnLuckyWheel()
    {
        if (CacheGame.GetMaxLevel3Difficult() < 3)
        {
            //btnLuckyWheelObj.SetActive(false);
        }
    }


    void LoadDataStart()
    {
        SetActiveBtnLuckyWheel();
        //SetActiveBtnOfferWall();

        //	---daily login
        if (CacheGame.GetMaxLevel3Difficult() > 2)
        {
            //dailyLoginBtn.SetActive(true);
            dailyLoginBtn.GetComponent<ButtonTopRightHome>().canShow = true;
        }
        else
        {
            //dailyLoginBtn.SetActive(false);
            dailyLoginBtn.GetComponent<ButtonTopRightHome>().canShow = false;
        }

        //----
        this.Delay(1f, () =>
        {
            GameContext.readyClickShowPopup = true;
        });

    }

    void AutoShowPoupStart()
    {
        if (PopupManager.Instance.goToTournament)
        {
            PopupManager.Instance.ShowTournamentPopup();
            PopupManager.Instance.goToTournament = false;
        }

        if (PopupManager.Instance.goTo2v2)
        {
            PopupManager.Instance.ShowPVP2V2Popup();
            PopupManager.Instance.goTo2v2 = false;
        }

        switch (GameContext.typeLoadPopupSelectEndless)
        {
            case GameContext.TypeLoadPopupSelectEndless.Main_Click_Retry:
                PopupManagerCuong.Instance.ShowSelectLevelPopup();
                GameContext.typeLoadPopupSelectEndless = GameContext.TypeLoadPopupSelectEndless.NotShow;
                return;

        }

        switch (GameContext.typeLoadPopupSelectEvent)
        {
            case GameContext.TypeLoadPopupSelectEvent.Main_Click_Retry:
                GameContext.modeGamePlay = GameContext.ModeGamePlay.Events;
                PopupManagerCuong.Instance.ShowEventPopup();
                PopupManagerCuong.Instance.ShowSelectLevelPopup();
                GameContext.typeLoadPopupSelectEvent = GameContext.TypeLoadPopupSelectEvent.NotShow;
                return;

        }
        //Kiểm tra xem để bật popup select hangar từ khi try
        if (GameContext.typeOpenSceneFromTry == GameContext.TypeOpenSceneFromTry.Home)
        {
            switch (GameContext.typeOpenPopupFromTry)
            {
                case GameContext.TypeOpenPopupFromTry.Aircraft:
                    PopupManagerCuong.Instance.ShowSelectAircraftPopup(SelectDeviceHandler.OpenSelectFromType.FromHome, true);
                    break;

                case GameContext.TypeOpenPopupFromTry.Wingman:
                    PopupManagerCuong.Instance.ShowSelectWingmanPopup(WingmanPosType.None, SelectDeviceHandler.OpenSelectFromType.FromHome, true);
                    break;

                default:
                    break;
            }
            GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.NotShow;
            return;
        }
        else
        {
            if (GameContext.typeOpenSceneFromTry == GameContext.TypeOpenSceneFromTry.Tournament)
            {
                switch (GameContext.typeOpenPopupFromTry)
                {
                    case GameContext.TypeOpenPopupFromTry.Aircraft:
                        PopupManagerCuong.Instance.ShowSelectAircraftPopup(SelectDeviceHandler.OpenSelectFromType.FromTournament, true);
                        break;

                    case GameContext.TypeOpenPopupFromTry.Wingman:
                        PopupManagerCuong.Instance.ShowSelectWingmanPopup(WingmanPosType.None, SelectDeviceHandler.OpenSelectFromType.FromTournament, true);
                        break;

                    default:
                        break;
                }
                GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.NotShow;
                return;
            }
        }

        //---DailyLogin

        if (CacheGame.GetClaimedAllGiftDailyLogin() != 1)
        {
            if (CacheGame.GetMaxLevel3Difficult() > 2)
            {
                scripDailyLogin.SetDataStartPopup();
                if (GameContext.showPopupDailyLogin && CacheGame.GetStatusClaimDailyLogin(CacheGame.GetTotalDayLogin()) == 2 && CacheGame.GetTotalDayLogin() < 8)
                {
                    this.Delay(0.2f, () =>
                    {
                        if (!PopupManagerCuong.Instance.IsActiveSelectLevelPopup())
                        {
                            GameContext.showPopupDailyLogin = false;
                            popupDailyLoginObj.SetActive(true);
                            return;
                        }
                    });
                }
            }
        }
        else
        {
            popupThirtyDaysLogin.CheckToAutoShowPopup();
        }


        //---Popupnotifi_Finish_Events và còn 3 ngày event

        DateTime expiredTime = CachePvp.dateTime;
        DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out expiredTime);

        if (GameContext.showPopupNotifiFinishEvent && (CachePvp.dateTime > expiredTime.AddDays(-3) && CachePvp.dateTime < expiredTime))
        {
            GameContext.showPopupNotifiFinishEvent = false;
            //PopupManagerCuong.Instance.ShowNotifiFinishEventPopup(); // hết event nên tạm bỏ 
            return;
        }

    }

    string notifi = "";
    void ChangeDataByDate()
    {
        //Debug.LogError("GetLastDayOpenGame: "+ CacheGame.GetLastDayOpenGame() + "-----CachePvp.dateTime : " + CachePvp.dateTime.Day);
        if (CacheGame.GetLastDayOpenGame() != CachePvp.dateTime.Day)
        {
            CacheGame.SetTotalShowX3Coin(0);
            CacheGame.SetNumUseHourlyRewardOneDay(0);
            CacheGame.SetNumShowStarterPackInLose(0);
            CacheGame.SetRemainWatchAdsToGetPackToday(5);
            CacheGame.SetNumGemGetToday(0);
            CacheGame.SetNumCardGetToday(0);
            CacheGame.SetNumUseVideoEndGameOneDay(0);
            CacheGame.SetIndexRandomVideoEndGameFirst(UnityEngine.Random.Range(1, 6));

            //--số lần sử dụng endless còn lại
            CacheGame.SetTotalPlayEndless(5);

            //--trả 1000 gold mỗi ngày cho thằng đã nạp infinity pack
            this.Delay(1f, () =>
            {
                if (CacheGame.GetBuyInfinityPack() == 1)
                {
                    if (PanelCoinGem.Instance != null)
                    {
                        PanelCoinGem.Instance.AddCoinTop(1000, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gold_infinitypack_new);
                    }
                    //
                    notifi = ScriptLocalization.notifi_get_gold_infinity_pack + " : + 1000 " + ScriptLocalization.gold + Environment.NewLine;

                }
                //bonus cho vip
                if (VipBonusValue.itemLife > 0)
                {
                    CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + VipBonusValue.itemLife);
                    notifi += Environment.NewLine + ScriptLocalization.vip_daily_rewards + " : + 1 " + ScriptLocalization.life;
                }
                if (VipBonusValue.itemPowerUp > 0)
                {
                    CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + VipBonusValue.itemPowerUp);
                    notifi += Environment.NewLine + ScriptLocalization.vip_daily_rewards + " : + 1 " + ScriptLocalization.power_up;
                }
                if (VipBonusValue.ticketLuckyWheel > 0)
                {
                    MinhCacheGame.AddSpinTickets(VipBonusValue.ticketLuckyWheel);
                    notifi += Environment.NewLine + ScriptLocalization.vip_daily_rewards + " : +1 " + ScriptLocalization.ticket;
                }

                if (notifi != "")
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(notifi, true, 4f);
                }

            });


            //Reset Data Minh:
            int totalAircraft = System.Enum.GetValues(typeof(AircraftTypeEnum)).Length;
            for (int i = 1; i <= totalAircraft; i++)
            {
                if (i != 4 && i != 5)
                {
                    MinhCacheGame.SetAircraftUsedTimePerDay((AircraftTypeEnum)i, 0);
                }
            }

            //Đặt lại các gói iap đã mua 
            //CacheGame.SetFirstInappBuyGold(1, 0);
            //CacheGame.SetFirstInappBuyGold(2, 0);
            //CacheGame.SetFirstInappBuyGold(3, 0);
            //CacheGame.SetFirstInappBuyGold(4, 0);
            //CacheGame.SetFirstInappBuyGold(5, 0);

            //Đặt lại summer holiday mission progress
            for (int i = 1; i <= 4; i++)
            {
                MinhCacheGame.ResetEventMissionProgress(i);
            }

            CacheGame.SetLastDayOpenGame(CachePvp.dateTime.Day);
        }
    }





}
