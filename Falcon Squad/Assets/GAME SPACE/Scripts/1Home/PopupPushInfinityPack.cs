﻿using EasyMobile;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class PopupPushInfinityPack : MonoBehaviour
{

    public int numCoin_InappInfinity = 10000;

    public GameObject buttonBuyObj;
    public UILabel lPrice_InfinityPack;

    void Start()
    {
        lPrice_InfinityPack.text = "" + CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gold_infinitypack_new);
    }

    public void Btn_InApp_InfinityPack()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_gold_infinitypack_new, IAPCallbackManager.WherePurchase.push_server);
    }

    public void Btn_Back()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.HidePopupPushInfinityPack();
    }


    void OnEnable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
    }

    void OnDisable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }


    void OnPurchaseSuccessed(string productName)
    {
        switch (productName)
        {
            case EM_IAPConstants.Product_inapp_gold_infinitypack_new:
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddCoinTop(numCoin_InappInfinity, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_gold_infinitypack_new);
                }
                //
                CacheGame.SetBuyInfinityPack(1);
                buttonBuyObj.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast((string)(ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + numCoin_InappInfinity + " " + ScriptLocalization.gold)), (bool)true, (float)1.8f);
                break;
            default:
                break;
        }
        //PanelCoinGem.Instance.ShowGlowButtonIapGold();
        //SetListPackX2FirstBuy();
    }
}
