﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TCore;
using UnityEngine.Networking;
using I2.Loc;
using System;
using Mp.Pvp;
using com.ootii.Messages;
public class PopupGiftCode : MonoBehaviour
{
    public UIInput uiInputScript;

    private void OnEnable()
    {
        uiInputScript.Submit();
        uiInputScript.value = "";
        uiInputScript.isSelected = false;
        MessageDispatcher.AddListener(EventName.UIHome.GiftCode.ToString(), OnGiftCode, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.UIHome.GiftCode.ToString(), OnGiftCode, true);
    }

    private void OnGiftCode(IMessage rMessage)
    {
        SCGiftcode gc = rMessage.Data as SCGiftcode;
        switch (gc.status)
        {
            case SCGiftcode.GIFTCODE_WAS_NOT_FOUND:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_giftcode_notfound, false);
                break;
            case SCGiftcode.GIFTCODE_WAS_USED:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_giftcode_used, false);
                break;
            case SCGiftcode.SUCCESS:
                List<GiftcodeItem> listGiftCode = gc.data.items;
                List<ItemVolumeFalconMail> list = new List<ItemVolumeFalconMail>();
                for (int i = 0; i < listGiftCode.Count; i++)
                {
                    if (i < 3)
                    {
                        ItemVolumeFalconMail item = new ItemVolumeFalconMail();
                        item.key = listGiftCode[i].id;
                        item.value = listGiftCode[i].value;
                        list.Add(item);
                    }
                }
                PopupManager.Instance.ShowClaimPopup(list, EventName.GIFT_CODE);
                break;
            default:
                PopupManagerCuong.Instance.ShowTextNotifiToast(gc.message, false, 1.31f);
                break;
        }
    }

    //-----------------Button----------------------------------------------------
    public void ButtonSubmit()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        if (PopupManagerCuong.Instance.readyShowNotifi)
        {
            if (uiInputScript.value.Length > 5)
            {
                new CSGiftcode(uiInputScript.value).Send();
                uiInputScript.value = "";
                uiInputScript.isSelected = false;
            }
            else
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_enter_giftcode, false, 1.31f);
            }
        }
    }

    public void BackButton()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
    }
}
