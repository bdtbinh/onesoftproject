﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GetCardInBox
{

    public static string textCardShowToast;
    public static GameContext.TypeItemInPopupItemReward typeItemPopupItemReward;
    public static int numItemPopupItemReward;

    public static void GetCardInBoxByRate(string whyLogFirebase, string nameBox, int valueBonusCard = 1, bool getRewardNow = true)
    {
        int valueRandom = Random.RandomRange(1, 101);
        //valueRandom = 98;
        Debug.LogError("valueRandom:" + valueRandom + "--nameBox:" + nameBox);
        switch (nameBox)
        {
            case "BoxCard1":
                for (int i = 1; i <= RateCardBoxSheet.GetDictionary().Count; i++)
                {
                    if (RateCardBoxSheet.GetDictionary()[i].BoxCard1 >= valueRandom)
                    {
                        GetPlaneAddCard(i, RateCardBoxSheet.GetDictionary()[i].Type, RateCardBoxSheet.GetDictionary()[i].Index, (RateCardBoxSheet.GetDictionary()[i].BoxCard1Num * valueBonusCard), whyLogFirebase, getRewardNow);
                        break;
                    }
                }
                break;
            case "BoxCard2":
                for (int i = 1; i <= RateCardBoxSheet.GetDictionary().Count; i++)
                {
                    if (RateCardBoxSheet.GetDictionary()[i].BoxCard2 >= valueRandom)
                    {
                        GetPlaneAddCard(i, RateCardBoxSheet.GetDictionary()[i].Type, RateCardBoxSheet.GetDictionary()[i].Index, (RateCardBoxSheet.GetDictionary()[i].BoxCard2Num * valueBonusCard), whyLogFirebase, getRewardNow);
                        break;
                    }
                }
                break;
            case "BoxCard3":
                for (int i = 1; i <= RateCardBoxSheet.GetDictionary().Count; i++)
                {
                    if (RateCardBoxSheet.GetDictionary()[i].BoxCard3 >= valueRandom)
                    {
                        GetPlaneAddCard(i, RateCardBoxSheet.GetDictionary()[i].Type, RateCardBoxSheet.GetDictionary()[i].Index, (RateCardBoxSheet.GetDictionary()[i].BoxCard3Num * valueBonusCard), whyLogFirebase, getRewardNow);
                        break;
                    }
                }
                break;
            case "BoxCard4":
                for (int i = 1; i <= RateCardBoxSheet.GetDictionary().Count; i++)
                {
                    if (RateCardBoxSheet.GetDictionary()[i].BoxCard4 >= valueRandom)
                    {
                        GetPlaneAddCard(i, RateCardBoxSheet.GetDictionary()[i].Type, RateCardBoxSheet.GetDictionary()[i].Index, (RateCardBoxSheet.GetDictionary()[i].BoxCard4Num * valueBonusCard), whyLogFirebase, getRewardNow);
                        break;
                    }
                }
                break;
            default:
                break;
        }

    }


    static void GetPlaneAddCard(int key, string typePlane, int indexPlane, int numCardAdd, string whyLogFirebase, bool getRewardNow)
    {
        textCardShowToast = "";
        Debug.LogError("GetPlaneAddCard:--" + " key:" + key + " typePlane:" + typePlane + " indexPlane:" + indexPlane + " numCardAdd:" + numCardAdd);
        numItemPopupItemReward = numCardAdd;
        switch (typePlane)
        {
            case "GeneralAircraft":
                if (getRewardNow)
                {
                    MinhCacheGame.SetSpaceShipGeneralCards(MinhCacheGame.GetSpaceShipGeneralCards() + numCardAdd, whyLogFirebase, "");
                }
                textCardShowToast = numCardAdd + " " + I2.Loc.ScriptLocalization.item_name_uni_aircraft_card;
                //typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.AircraftGeneralCard;
                PlaneGetTypeItemPopupItemReward(indexPlane);
                break;
            case "Aircraft":
                if (getRewardNow)
                {
                    MinhCacheGame.SetSpaceShipCards((AircraftTypeEnum)indexPlane, MinhCacheGame.GetSpaceShipCards((AircraftTypeEnum)indexPlane) + numCardAdd, whyLogFirebase, "", ((AircraftTypeEnum)indexPlane).ToString());
                }
                textCardShowToast = numCardAdd + " " + ((AircraftTypeEnum)indexPlane).ToString() + " Cards";
                PlaneGetTypeItemPopupItemReward(indexPlane);
                break;
            case "GeneralWingman":
                if (getRewardNow)
                {
                    MinhCacheGame.SetWingmanGeneralCards(MinhCacheGame.GetWingmanGeneralCards() + numCardAdd, whyLogFirebase, "");
                }
                textCardShowToast = numCardAdd + " " + I2.Loc.ScriptLocalization.item_name_uni_drone_card;
                WingmanGetTypeItemPopupItemReward(indexPlane);
                break;
            case "Wingman":
                if (getRewardNow)
                {
                    MinhCacheGame.SetWingmanCards((WingmanTypeEnum)indexPlane, MinhCacheGame.GetWingmanCards((WingmanTypeEnum)indexPlane) + numCardAdd, whyLogFirebase, "", ((WingmanTypeEnum)indexPlane).ToString());
                }
                textCardShowToast = numCardAdd + " " + ((WingmanTypeEnum)indexPlane).ToString() + " Cards";
                WingmanGetTypeItemPopupItemReward(indexPlane);

                break;
            default:
                break;
        }
        Debug.LogError("textCardShowToast: " + textCardShowToast);
    }


    static void PlaneGetTypeItemPopupItemReward(int indexPlane)
    {
        switch (indexPlane)
        {
            case 0:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.AircraftGeneralCard;
                break;
            case 1:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Aircraft1Card;
                break;
            case 2:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Aircraft2Card;
                break;
            case 3:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Aircraft3Card;
                break;
            case 6:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Aircraft6Card;
                break;
            case 7:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Aircraft7Card;
                break;
            case 8:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Aircraft8Card;
                break;
            case 9:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Aircraft9Card;
                break;
            default:
                break;
        }
    }


    static void WingmanGetTypeItemPopupItemReward(int indexWingman)
    {
        switch (indexWingman)
        {
            case 0:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.DroneGeneralCard;
                break;
            case 1:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Drone1Card;
                break;
            case 2:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Drone2Card;
                break;
            case 3:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Drone3Card;
                break;
            case 4:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Drone4Card;
                break;
            case 5:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Drone5Card;
                break;
            case 6:
                typeItemPopupItemReward = GameContext.TypeItemInPopupItemReward.Drone6Card;
                break;
            default:
                break;
        }
    }


}
