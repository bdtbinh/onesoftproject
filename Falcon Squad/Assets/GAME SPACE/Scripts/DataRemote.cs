﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OneSoftGame.Tools;

public class DataRemote : PersistentSingleton<DataRemote>
{
	public int packnorm, packsaleoff, packbonous, packsale699, packsale1199, packsale2499;
	public int starLoadWave = 200;
	public float modenormal = 1f;
	public float modehard = 1.5f;
	public float modecrazy = 2f;

	//setting remote................
	public static float SpawnRateFactor{ get; private set; }

	public static float EnemySpeedFactor{ get; private set; }

	public static float EnemyStrengthFactor{ get; private set; }

	public static float PackRemote699{ get; private set; }

	public static float PackRemote1199{ get; private set; }

	public static float PackRemote2499{ get; private set; }

	public static float PackRemoteStarsLoadWave{ get; private set; }

	public static float PackRemoteModeHard{ get; private set; }

	public static float PackRemoteModeCrazy{ get; private set; }

	public static float PackRemoteModeNormal{ get; private set; }
	// Use this for initialization
	void Start ()
	{
		SpawnRateFactor = packnorm;
		EnemySpeedFactor = packsaleoff;
		EnemyStrengthFactor = packbonous;
		PackRemote699 = packsale699;
		PackRemote1199 = packsale1199;
		PackRemote2499 = packsale2499;
		PackRemoteStarsLoadWave = starLoadWave;
		PackRemoteModeHard = modehard;
		PackRemoteModeCrazy = modecrazy;
		PackRemoteModeNormal = modenormal;
		RemoteSettings.Updated += new RemoteSettings.UpdatedEventHandler (HandleRemoteUpdate);
	}

	private void HandleRemoteUpdate ()
	{
		SpawnRateFactor = RemoteSettings.GetInt ("SpawnRateFactor", packnorm);
		EnemySpeedFactor = RemoteSettings.GetInt ("EnemySpeedFactor", packsaleoff);
		EnemyStrengthFactor = RemoteSettings.GetInt ("EnemyStrengthFactor", packbonous);
		PackRemote699 = RemoteSettings.GetInt ("PackRemote699", packsale699);
		PackRemote1199 = RemoteSettings.GetInt ("PackRemote1199", packsale1199);
		PackRemote2499 = RemoteSettings.GetInt ("PackRemote2499", packsale2499);
		PackRemoteStarsLoadWave = RemoteSettings.GetInt ("PackRemoteStarsLoadWave", starLoadWave);
		PackRemoteModeNormal = RemoteSettings.GetFloat ("PackRemoteModeNormal", modenormal);
		PackRemoteModeHard = RemoteSettings.GetFloat ("PackRemoteModeHard", modehard);
		PackRemoteModeCrazy = RemoteSettings.GetFloat ("PackRemoteModeCrazy", modecrazy);
	}

}
