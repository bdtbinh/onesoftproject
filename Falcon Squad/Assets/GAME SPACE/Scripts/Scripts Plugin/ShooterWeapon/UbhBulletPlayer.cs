﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UbhBulletPlayer : UbhBullet
{

    string InfoBoxMessage = "Viên đạn nào ko thay power hay sprite thì để null";
    [InfoBox("$InfoBoxMessage")]
    public BulletPlayerValue bulletPlayerValue;


    //void Start()
    //{

    //}

    public void SetPower(Sprite spriteBullet, int power, PlayerInit playerInitScript = null, TypeGunPlayerShotBulletHell typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.None)
    {
        if (bulletPlayerValue != null)
        {
            //Debug.LogError(gameObject.name + "-power--" + power);
            bulletPlayerValue.SetValueStart(spriteBullet, power, playerInitScript, typeGunPlayerShotBulletHell);
        }

    }
}
