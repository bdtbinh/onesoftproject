﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// Ubh nWay shot.
/// </summary>
[AddComponentMenu("UniBulletHell/Shot Pattern/Galaxy Shooter Nway Shot")]
public class SpaceWarNwayShot : SpaceWarShootSetData
{



    [Header("===== NwayShot Settings =====")]
    // "Set a number of shot way."
    [FormerlySerializedAs("_WayNum")]
    public int m_wayNum = 5;
    // "Set a center angle of shot. (0 to 360)"
    [Range(0f, 360f), FormerlySerializedAs("_CenterAngle")]
    public float m_centerAngle = 180f;
    // "Set a angle between bullet and next bullet. (0 to 360)"
    [Range(0f, 360f), FormerlySerializedAs("_BetweenAngle")]
    public float m_betweenAngle = 10f;
    // "Set a delay time between shot and next line shot. (sec)"
    [FormerlySerializedAs("_NextLineDelay")]
    public float m_nextLineDelay = 0.1f;

    public float m_padding_x = 0.5f;
    public float m_padding_y = 0.5f;

    public bool m_AngleLeftAndRightOnly = false;



    public override void Shot()
    {
        StartCoroutine(ShotCoroutine());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator ShotCoroutine()
    {
        // code thÊm để chỉnh num way
        if (wayNumFromAircraft != 0)
        {
            m_wayNum = wayNumFromAircraft;
        }
        //-------

        if (m_bulletNum <= 0 || m_bulletSpeed <= 0f || m_wayNum <= 0)
        {
            Debug.LogWarning("Cannot shot because BulletNum or BulletSpeed or WayNum is not set.");
            yield break;
        }
        if (m_shooting)
        {
            yield break;
        }
        m_shooting = true;

        int wayIndex = 0;

        for (int i = 0; i < m_bulletNum; i++)
        {
            if (m_wayNum <= wayIndex)
            {
                wayIndex = 0;

                if (0f < m_nextLineDelay)
                {
                    FiredShot();
                    yield return UbhUtil.WaitForSeconds(m_nextLineDelay);
                }
            }
            var bullet_pos = transform.position;
            float x = 0;
            float y = 0;
            if (m_padding_x != 0 || m_padding_y != 0)
            {
                if (m_padding_x != 0)
                {
                    x = GetShiftedX(wayIndex, m_wayNum);
                }
                if (m_padding_y != 0)
                {
                    y = GetShiftedY(wayIndex, m_wayNum);
                }

                if (m_AngleLeftAndRightOnly)
                {
                    if (wayIndex != 0 && wayIndex != m_wayNum - 1)
                    {
                        bullet_pos = bullet_pos + new Vector3(x, -y, 0);
                    }
                }
                else
                {
                    bullet_pos = bullet_pos + new Vector3(x, -y, 0);
                }
            }

            // sửa thêm để chỉnh power
            var bullet = GetBullet(bullet_pos) as UbhBulletPlayer;
            //
            if (bullet == null)
            {
                break;
            }

            float angle = m_centerAngle + GetShiftedAngle(wayIndex, m_wayNum);

            if (m_AngleLeftAndRightOnly)
            {
                if (wayIndex != 0 && wayIndex != (m_wayNum - 1))
                {
                    angle = m_centerAngle;
                }
            }



            ShotBullet(bullet, m_bulletSpeed, angle);

            // code thÊm để chỉnh power
            ChangePower();
            bullet.SetPower(spriteUse, powerUse, playerInitScript, typeGunPlayerShotBulletHell);

            //
            wayIndex++;
        }

        FiredShot();

        FinishedShot();

        yield break;
    }


    private float GetShiftedY(int wayIndex, int m_wayNum)
    {
        if (m_wayNum % 2 == 0)
        {
            var centerLineIndexRight = (int)((m_wayNum + 1) / 2);
            var centerLineIndexLeft = centerLineIndexRight - 1;
            if (wayIndex <= centerLineIndexLeft)
            {
                return (centerLineIndexLeft - wayIndex) * m_padding_y;
            }
            else
            {
                return (wayIndex - centerLineIndexRight) * m_padding_y;
            }

        }
        else
        {
            var centerLineIndex = Mathf.Floor((float)m_wayNum / 2f);
            var lineSpaceNumber = Math.Abs(centerLineIndex - wayIndex);
            return lineSpaceNumber * m_padding_y;

        }
    }

    private float GetShiftedX(int wayIndex, int m_wayNum)
    {
        var leftX = -(m_wayNum - 1) * 0.5f * m_padding_x;
        return leftX + (wayIndex) * m_padding_x;
    }

    private float GetShiftedAngle(int wayIndex, int m_wayNum)
    {
        var leftX = -(m_wayNum - 1) * 0.5f * m_betweenAngle;
        return leftX + (wayIndex) * m_betweenAngle;
    }

    //protected  void ShotBullet(UbhBullet bullet, float speed, float angle,
    //                          bool homing = false, Transform homingTarget = null, float homingAngleSpeed = 0f,
    //                          bool wave = false, float waveSpeed = 0f, float waveRangeSize = 0f)
    //{
    //    if (bullet == null)
    //    {
    //        return;
    //    }
    //    bullet.Shot(this,
    //                speed, angle, m_accelerationSpeed, m_accelerationTurn,
    //                homing, homingTarget, homingAngleSpeed,
    //                wave, waveSpeed, waveRangeSize,
    //                m_usePauseAndResume, m_pauseTime, m_resumeTime,
    //                m_useAutoRelease, m_autoReleaseTime,
    //                m_shotCtrl.m_axisMove, m_shotCtrl.m_inheritAngle,
    //                m_useMaxSpeed, m_maxSpeed, m_useMinSpeed, m_minSpeed);
    //}

}