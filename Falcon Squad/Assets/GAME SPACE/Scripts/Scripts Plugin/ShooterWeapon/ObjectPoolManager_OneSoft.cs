﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Main pooling singleton script for bullet / object pool usage. The WeaponSystem will use this to retrieve re-usable bullets from defined bullet pools, allowing for faster performance when firing lots of bullets.
/// </summary>
public class ObjectPoolManager_OneSoft : MonoBehaviour
{

	public static ObjectPoolManager_OneSoft instance;

	public GameObject bullet1_Prefab, bullet2_Prefab, bullet3_Prefab, bullet4_Prefab, bullet5_Prefab, bullet6_Prefab;

	public int numBullet1_ToSpawn, numBullet2_ToSpawn, numBullet3_ToSpawn, numBullet4_ToSpawn, numBullet5_ToSpawn, numBullet6_ToSpawn, numTurretBulletsToSpawn;

	public static List<GameObject> listBullet1_ObjectPool, listBullet2_ObjectPool, listBullet3_ObjectPool, listBullet4_ObjectPool, listBullet5_ObjectPool, listBullet6_ObjectPool, turretBulletObjectPool;

	//
	public GameObject effect_parkPrefab, effect_bloodPrefab;
	public int numSparksToSpawn, numBloodParticleToSpawn;
	public static List<GameObject> sparkEffect_ObjectPool, bloodEffect_ObjectPool;

	//
	/// <summary>
	/// Only really used for demo scenes - bullets can be made to use the layer for turret bullets, this is so that the demo scene player can have his/her own bullets that apply damage to the turrets, and the turrets can have their own bullets.
	/// </summary>
	public bool tagAsTurretBullets;

	private GameObject pooledObjectFolder;

	// Use this for initialization
	private void Start ()
	{
		instance = this;
		pooledObjectFolder = GameObject.Find ("[-BulletPlayer_PoolManager-]");

		// Standard horizontal bullet object pool
		listBullet1_ObjectPool = new List<GameObject> ();

		for (var i = 1; i <= numBullet1_ToSpawn; i++) {
			var stdHorizontalBullet = (GameObject)Instantiate (bullet1_Prefab);

			SetParentTransform (stdHorizontalBullet);

			stdHorizontalBullet.SetActive (false);
			listBullet1_ObjectPool.Add (stdHorizontalBullet);
		}

		// Sphere bullet object pool
		listBullet2_ObjectPool = new List<GameObject> ();

		for (var i = 1; i <= numBullet2_ToSpawn; i++) {
			var sphereBullet = (GameObject)Instantiate (bullet2_Prefab);

			SetParentTransform (sphereBullet);

			sphereBullet.SetActive (false);
			listBullet2_ObjectPool.Add (sphereBullet);
		}

		// turret bullet object pool
		turretBulletObjectPool = new List<GameObject> ();

		for (var i = 1; i <= numTurretBulletsToSpawn; i++) {
			var turretBullet = (GameObject)Instantiate (bullet1_Prefab);

			SetParentTransform (turretBullet);

			turretBullet.SetActive (false);
			if (tagAsTurretBullets)
				turretBullet.layer = LayerMask.NameToLayer ("TurretWeaponSystemBullets");
			turretBulletObjectPool.Add (turretBullet);
		}

		// beam1 bullet object pool
		listBullet3_ObjectPool = new List<GameObject> ();

		for (var i = 1; i <= numBullet3_ToSpawn; i++) {
			var beam1Bullet = (GameObject)Instantiate (bullet3_Prefab);

			SetParentTransform (beam1Bullet);

			beam1Bullet.SetActive (false);
			if (tagAsTurretBullets)
				beam1Bullet.layer = LayerMask.NameToLayer ("TurretWeaponSystemBullets");
			listBullet3_ObjectPool.Add (beam1Bullet);
		}

		// beam2 bullet object pool
		listBullet4_ObjectPool = new List<GameObject> ();

		for (var i = 1; i <= numBullet4_ToSpawn; i++) {
			var beam2Bullet = (GameObject)Instantiate (bullet4_Prefab);

			SetParentTransform (beam2Bullet);

			beam2Bullet.SetActive (false);
			if (tagAsTurretBullets)
				beam2Bullet.layer = LayerMask.NameToLayer ("TurretWeaponSystemBullets");
			listBullet4_ObjectPool.Add (beam2Bullet);
		}

		// beam3 bullet object pool
		listBullet5_ObjectPool = new List<GameObject> ();

		for (var i = 1; i <= numBullet5_ToSpawn; i++) {
			var beam3Bullet = (GameObject)Instantiate (bullet5_Prefab);

			SetParentTransform (beam3Bullet);

			beam3Bullet.SetActive (false);
			if (tagAsTurretBullets)
				beam3Bullet.layer = LayerMask.NameToLayer ("TurretWeaponSystemBullets");
			listBullet5_ObjectPool.Add (beam3Bullet);
		}

		// beam4 bullet object pool
		listBullet6_ObjectPool = new List<GameObject> ();

		for (var i = 1; i <= numBullet6_ToSpawn; i++) {
			var beam4Bullet = (GameObject)Instantiate (bullet6_Prefab);

			SetParentTransform (beam4Bullet);

			beam4Bullet.SetActive (false);
			if (tagAsTurretBullets)
				beam4Bullet.layer = LayerMask.NameToLayer ("TurretWeaponSystemBullets");
			listBullet6_ObjectPool.Add (beam4Bullet);
		}

		// Spark object pool
		sparkEffect_ObjectPool = new List<GameObject> ();

		for (var i = 1; i <= numSparksToSpawn; i++) {
			var spark = (GameObject)Instantiate (effect_parkPrefab);

			SetParentTransform (spark);

			spark.SetActive (false);
			sparkEffect_ObjectPool.Add (spark);
		}

		// Blood object pool
		bloodEffect_ObjectPool = new List<GameObject> ();

		for (var i = 1; i <= numBloodParticleToSpawn; i++) {
			var bloodObject = (GameObject)Instantiate (effect_bloodPrefab);

			SetParentTransform (bloodObject);

			bloodObject.SetActive (false);
			bloodEffect_ObjectPool.Add (bloodObject);
		}
	}

	private void SetParentTransform (GameObject gameObjectRef)
	{
		if (pooledObjectFolder != null) {
			gameObjectRef.transform.parent = pooledObjectFolder.transform;
		}
	}

	public GameObject GetUsable_Bullet1 ()
	{
		var obj = (from item in listBullet1_ObjectPool
		           where item.activeSelf == false
		           select item).FirstOrDefault ();

		if (obj != null) {
			return obj;
		}

		Debug.Log ("<color=orange>WARNING: Ran out of reusable std horizontal bullet objects! Now instantiating a new one</color>");
		var stdBullet = (GameObject)Instantiate (instance.bullet1_Prefab);

		SetParentTransform (stdBullet);

		stdBullet.SetActive (false);
		listBullet1_ObjectPool.Add (stdBullet);

		return stdBullet;
	}

	public GameObject GetUsable_Bullet2 ()
	{
		var obj = (from item in listBullet2_ObjectPool
		           where item.activeSelf == false
		           select item).FirstOrDefault ();

		if (obj != null) {
			return obj;
		}

		Debug.Log ("<color=orange>WARNING: Ran out of reusable sphere bullet objects! Now instantiating a new one</color>");
		var sphereBullet = (GameObject)Instantiate (instance.bullet2_Prefab);

		SetParentTransform (sphereBullet);

		sphereBullet.SetActive (false);
		listBullet2_ObjectPool.Add (sphereBullet);

		return sphereBullet;
	}

	public GameObject GetUsableTurretBullet ()
	{
		var obj = (from item in turretBulletObjectPool
		           where item.activeSelf == false
		           select item).FirstOrDefault ();

		if (obj != null) {
			return obj;
		}

		Debug.Log ("<color=orange>WARNING: Ran out of reusable turret bullet objects! Now instantiating a new one</color>");
		var turretBullet = (GameObject)Instantiate (instance.bullet1_Prefab);

		SetParentTransform (turretBullet);

		turretBullet.SetActive (false);
		listBullet1_ObjectPool.Add (turretBullet);

		if (tagAsTurretBullets)
			turretBullet.layer = LayerMask.NameToLayer ("TurretWeaponSystemBullets");

		return turretBullet;
	}

	public GameObject GetUsable_Bullet3 ()
	{
		var obj = (from item in listBullet3_ObjectPool
		           where item.activeSelf == false
		           select item).FirstOrDefault ();

		if (obj != null) {
			return obj;
		}

		Debug.Log ("<color=orange>WARNING: Ran out of reusable turret bullet objects! Now instantiating a new one</color>");
		var beam1Bullet = (GameObject)Instantiate (instance.bullet3_Prefab);

		SetParentTransform (beam1Bullet);

		beam1Bullet.SetActive (false);
		listBullet3_ObjectPool.Add (beam1Bullet);

		if (tagAsTurretBullets)
			beam1Bullet.layer = LayerMask.NameToLayer ("TurretWeaponSystemBullets");

		return beam1Bullet;
	}

	public GameObject GetUsable_Bullet4 ()
	{
		var obj = (from item in listBullet4_ObjectPool
		           where item.activeSelf == false
		           select item).FirstOrDefault ();

		if (obj != null) {
			return obj;
		}

		Debug.Log ("<color=orange>WARNING: Ran out of reusable turret bullet objects! Now instantiating a new one</color>");
		var beam2Bullet = (GameObject)Instantiate (instance.bullet4_Prefab);

		SetParentTransform (beam2Bullet);

		beam2Bullet.SetActive (false);
		listBullet4_ObjectPool.Add (beam2Bullet);

		if (tagAsTurretBullets)
			beam2Bullet.layer = LayerMask.NameToLayer ("TurretWeaponSystemBullets");

		return beam2Bullet;
	}

	public GameObject GetUsable_Bullet5 ()
	{
		var obj = (from item in listBullet5_ObjectPool
		           where item.activeSelf == false
		           select item).FirstOrDefault ();

		if (obj != null) {
			return obj;
		}

		Debug.Log ("<color=orange>WARNING: Ran out of reusable turret bullet objects! Now instantiating a new one</color>");
		var beam3Bullet = (GameObject)Instantiate (instance.bullet5_Prefab);

		SetParentTransform (beam3Bullet);

		beam3Bullet.SetActive (false);
		listBullet5_ObjectPool.Add (beam3Bullet);

		if (tagAsTurretBullets)
			beam3Bullet.layer = LayerMask.NameToLayer ("TurretWeaponSystemBullets");

		return beam3Bullet;
	}

	public GameObject GetUsable_Bullet6 ()
	{
		var obj = (from item in listBullet6_ObjectPool
		           where item.activeSelf == false
		           select item).FirstOrDefault ();

		if (obj != null) {
			return obj;
		}

		Debug.Log ("<color=orange>WARNING: Ran out of reusable turret beam4 objects! Now instantiating a new one</color>");
		var beam4Bullet = (GameObject)Instantiate (instance.bullet6_Prefab);

		SetParentTransform (beam4Bullet);

		beam4Bullet.SetActive (false);
		listBullet6_ObjectPool.Add (beam4Bullet);

		if (tagAsTurretBullets)
			beam4Bullet.layer = LayerMask.NameToLayer ("TurretWeaponSystemBullets");

		return beam4Bullet;
	}

	public GameObject GetUsableSparkParticle ()
	{
		var obj = (from item in sparkEffect_ObjectPool
		           where item.activeSelf == false
		           select item).FirstOrDefault ();

		if (obj != null) {
			return obj;
		}

		Debug.Log ("<color=orange>WARNING: Ran out of reusable spark objects! Now instantiating a new one</color>");
		var sparkParticle = (GameObject)Instantiate (instance.effect_parkPrefab);

		SetParentTransform (sparkParticle);

		sparkParticle.SetActive (false);
		sparkEffect_ObjectPool.Add (sparkParticle);

		return sparkParticle;
	}

	public GameObject GetUsableBloodSplatterParticleEffect ()
	{
		var obj = (from item in bloodEffect_ObjectPool
		           where item.activeSelf == false
		           select item).FirstOrDefault ();

		if (obj != null) {
			return obj;
		}

		Debug.Log ("<color=orange>WARNING: Ran out of reusable blood particle objects! Now instantiating a new one</color>");
		var bloodEffectParticleGo = (GameObject)Instantiate (instance.effect_bloodPrefab);

		SetParentTransform (bloodEffectParticleGo);

		bloodEffectParticleGo.SetActive (false);
		bloodEffect_ObjectPool.Add (bloodEffectParticleGo);

		return bloodEffectParticleGo;
	}
}
