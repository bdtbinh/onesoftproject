﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;


public class SpaceWarShootSetData : UbhBaseShot
{

    //----------------------------------------------
    [Header("===== DATA FROM Aircraft =====")]
    public PlayerInit playerInitScript;
    public TypePlaneShot typePlaneShotBulletHell;
    public TypeGunPlayerShotBulletHell typeGunPlayerShotBulletHell;

    protected Sprite spriteUse;
    protected int powerUse;

    [DisplayAsString]
    public int wayNumFromAircraft;


    protected void ChangePower()
    {
        if (playerInitScript != null)
        {
            switch (typePlaneShotBulletHell)
            {
                case TypePlaneShot.Aircraft:
                    switch (typeGunPlayerShotBulletHell)
                    {
                        case TypeGunPlayerShotBulletHell.AircraftMainPower:
                            powerUse = playerInitScript.Aircraft.mainWeaponPower;
                            spriteUse = playerInitScript.Aircraft.mainWeaponSprite;
                            break;
                        case TypeGunPlayerShotBulletHell.AircraftSecondPower:
                            powerUse = playerInitScript.Aircraft.secondWeaponPower;
                            spriteUse = playerInitScript.Aircraft.secondWeaponSprite;
                            break;
                        case TypeGunPlayerShotBulletHell.ActiveSkillGunPower:
                            powerUse = playerInitScript.Aircraft.powerGunActiveSkill;
                            break;
                        default:
                            break;
                    }
                    //Debug.LogError(gameObject.name + "---Aircraft" + powerUse);

                    break;
                case TypePlaneShot.LeftWingman:
                    switch (typeGunPlayerShotBulletHell)
                    {
                        case TypeGunPlayerShotBulletHell.WingmanMainPower:
                            powerUse = playerInitScript.LeftWingman.mainWeaponPower;
                            spriteUse = playerInitScript.LeftWingman.mainWeaponSprite;
                            break;
                        default:
                            break;
                    }
                    break;
                case TypePlaneShot.RightWingman:
                    switch (typeGunPlayerShotBulletHell)
                    {
                        case TypeGunPlayerShotBulletHell.WingmanMainPower:
                            powerUse = playerInitScript.RightWingman.mainWeaponPower;
                            spriteUse = playerInitScript.RightWingman.mainWeaponSprite;
                            //Debug.LogError("powerUse:" + powerUse);
                            break;
                        default:
                            break;
                    }
                    break;
                case TypePlaneShot.Wing:
                    switch (typeGunPlayerShotBulletHell)
                    {
                        case TypeGunPlayerShotBulletHell.WingPower:
                            powerUse = playerInitScript.Wing.mainWeaponPower;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            if (playerInitScript.typePlayerByMode == TypePlayerByModeEnum.PvP2v2Player)
            {
                //Debug.LogError(gameObject.name + "---CoopPlayer" + powerUse);
                powerUse = 0;
            }

        }

    }


    public override void Shot()
    {
    }
}
