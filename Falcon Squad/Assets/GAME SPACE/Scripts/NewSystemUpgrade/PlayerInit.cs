﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;
using com.ootii.Messages;
using SkyGameKit;
using System;
using System.Collections;
using System.Collections.Generic;

public class PlayerInit : MonoBehaviour
{
    public TypePlayerByModeEnum typePlayerByMode;

    [HideInInspector]
    public bool usingAircraftBackup = false;

    [SerializeField]
    private GameObject[] listAircraftPrefab;

    public PlayerController playerControllerScript;
    public WingmanMove wingmanMoveScript;

    [SerializeField]
    private GameObject[] listWingmanPrefab;

    [SerializeField]
    private GameObject[] listWingPrefab;

    private Aircraft aircraft;
    public Aircraft Aircraft { get { return aircraft; } }

    private Wingman leftWingman;
    public Wingman LeftWingman { get { return leftWingman; } }

    private Wingman rightWingman;
    public Wingman RightWingman { get { return rightWingman; } }

    private Wing wing;
    public Wing Wing { get { return wing; } }

    private int life;
    public int Life { get { return life; } }

    private float coinPercentBonus;
    public float CoinPercentBonus { get { return coinPercentBonus; } }

    private float scorePercentBonus;
    public float ScorePercentBonus { get { return scorePercentBonus; } }



    #region ---testingMode----
    public bool isTestingMode;
    [ShowIf("isTestingMode")]
    public AircraftTypeEnum aircraftTest;
    [ShowIf("isTestingMode")]
    public Rank aircraftRankTest;
    [ShowIf("isTestingMode")]
    public int aircraftLevelTest = 10;

    [ShowIf("isTestingMode")]
    public WingmanTypeEnum wingManLeftTest;
    [ShowIf("isTestingMode")]
    public Rank wingManLeftRankTest;
    [ShowIf("isTestingMode")]
    public int wingManLeftLevelTest = 10;

    [ShowIf("isTestingMode")]
    public WingmanTypeEnum wingManRightTest;
    [ShowIf("isTestingMode")]
    public Rank wingManRightRankTest;
    [ShowIf("isTestingMode")]
    public int wingManRightLevelTest = 10;

    [ShowIf("isTestingMode")]
    public WingTypeEnum wingTest;
    [ShowIf("isTestingMode")]
    public Rank wingRankTest;
    [ShowIf("isTestingMode")]
    public int wingLevelTest = 10;
    #endregion

    private void Awake()
    {
        //GameContext.modeGamePlay = GameContext.ModeGamePlay.PVP2vs2;
        isTestingMode = true;
        if (isTestingMode && typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            SerializableSet set = Resources.Load<SerializableSet>("SerializableSet");
            Deserializer.Deserialize(set);
            Resources.UnloadUnusedAssets();

            CacheGame.SetSpaceShipUserSelected((int)aircraftTest);
            CacheGame.SetSpaceShipRank(aircraftTest, aircraftRankTest);
            CacheGame.SetSpaceShipLevel(aircraftTest, aircraftLevelTest);

            CacheGame.SetWingManLeftUserSelected((int)wingManLeftTest);
            CacheGame.SetWingmanRank(wingManLeftTest, wingManLeftRankTest);
            CacheGame.SetWingmanLevel(wingManLeftTest, wingManLeftLevelTest);

            CacheGame.SetWingManRightUserSelected((int)wingManRightTest);
            CacheGame.SetWingmanRank(wingManRightTest, wingManRightRankTest);
            CacheGame.SetWingmanLevel(wingManRightTest, wingManRightLevelTest);

            CacheGame.SetWingUserSelected((int)wingTest);
            CacheGame.SetWingRank(wingTest, wingRankTest);
            CacheGame.SetWingLevel(wingTest, wingLevelTest);
        }
    }

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.UDP.AddPowerUp.ToString(), OnAddPowerUp, true);
    }

    private void OnAddPowerUp(IMessage rMessage)
    {
        if (typePlayerByMode == TypePlayerByModeEnum.PvP2v2Player)
        {
            int amount = (int)rMessage.Data;
            Debug.Log("============ duoc cong them amount power up : " + amount);
            aircraft.AddMainWeaponPowerLevel(amount);
            aircraft.AddMainWeaponSpeedLevel(amount);
            aircraft.AddMainWeaponQuantityLevel(amount);

            aircraft.AddSecondWeaponPowerLevel(amount);
            aircraft.AddSecondWeaponSpeedLevel(amount);
        }
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.UDP.AddPowerUp.ToString(), OnAddPowerUp, true);
    }

    private IEnumerator Start()
    {
        InitPlayer();
        yield return null;
        if (isTestingMode && GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2 && typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            LevelManager.Instance.NextWaveTime();
        }
    }


    public void InitPlayer()
    {
        if (typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            InitPlayerCampaign();
            MainScene.Instance.skillControllerScript.InitButtonSkill();
            MainScene.Instance.skillControllerScript.InitButtonWingSkill();
        }
        else
        {
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
            {
                InitPlayerPvP2v2();
            }
        }
        playerControllerScript.SetAircraftTrans();
        wingmanMoveScript.WingmanStartMove();
    }

    public void InitAircraftBackup(AircraftTypeEnum typeAircraftBackup)
    {
        wingmanMoveScript.wingmanReadyMove = false;

        AircraftTypeEnum typeAircraftCampaignInit = typeAircraftBackup;

        Aircraft aircraftBackup = Instantiate(listAircraftPrefab[(int)typeAircraftCampaignInit - 1], transform).GetComponent<Aircraft>();
        aircraftBackup.transform.localPosition = aircraft.transform.localPosition;
        if (wing != null)
        {
            wing.transform.parent = aircraftBackup.transform;
        }


        Destroy(aircraft.gameObject);
        aircraft = aircraftBackup;
        aircraft.InitData(typeAircraftCampaignInit, CacheGame.GetSpaceShipRank(typeAircraftCampaignInit), TypePlaneShot.Aircraft);

        InitCarryOnItemAircraft();

        playerControllerScript.SetAircraftTrans();
        MainScene.Instance.skillControllerScript.InitButtonSkill();
        wingmanMoveScript.WingmanStartMove();

    }

    public void InitPlayerCampaign()
    {
        InitAircraftCampaign(transform);        //Thay transform bằng object cha cần gắn aircraft                               
        InitLeftWingmanCampaign(transform);
        InitRightWingmanCampaign(transform);

        InitWingCampaign(Aircraft.transform);

        InitCarryOnItemAircraft();
        InitCarryOnItemWingmanLeft();
        InitCarryOnItemWingmanRight();
        InitCarryOnItemWingCampaign();
    }

    private void InitAircraftCampaign(Transform parrent)
    {
        AircraftTypeEnum typeAircraftCampaignInit;
        Rank rankAircraftInit;
        if (SceneManager.GetActiveScene().name == "LevelTutorial")
        {
            typeAircraftCampaignInit = AircraftTypeEnum.TwilightX;
            rankAircraftInit = Rank.S;
        }
        else if (SceneManager.GetActiveScene().name == "LevelTryPlane" && GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Aircraft)
        {
            typeAircraftCampaignInit = (AircraftTypeEnum)CacheGame.GetSpaceShipUserTry();
            rankAircraftInit = Rank.SSS;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament)
        {
            typeAircraftCampaignInit = (AircraftTypeEnum)CacheGame.GetSpaceShipTournament();
            rankAircraftInit = Rank.SSS;
        }
        else
        {
            if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
            {
                typeAircraftCampaignInit = (AircraftTypeEnum)CacheGame.GetSpaceShipUserSelected();
            }
            else
            {
                typeAircraftCampaignInit = (AircraftTypeEnum)CacheGame.GetSpaceShipUserSelectedExtraLevel();
            }
            rankAircraftInit = CacheGame.GetSpaceShipRank(typeAircraftCampaignInit);
        }
        Debug.Log("typeAircraftCampaignInit:" + typeAircraftCampaignInit);

        aircraft = Instantiate(listAircraftPrefab[(int)typeAircraftCampaignInit - 1], parrent).GetComponent<Aircraft>();
        aircraft.InitData(typeAircraftCampaignInit, rankAircraftInit, TypePlaneShot.Aircraft);

    }

    private void InitLeftWingmanCampaign(Transform parrent)
    {
        WingmanTypeEnum typeWingmanLeft;
        Rank rankWingmanLeft;
        if (SceneManager.GetActiveScene().name == "LevelTutorial")
        {
            typeWingmanLeft = WingmanTypeEnum.HomingMissile;
            if (typeWingmanLeft <= 0) return;
            rankWingmanLeft = CacheGame.GetWingmanRank(typeWingmanLeft);
        }
        else if (SceneManager.GetActiveScene().name == "LevelTryPlane" &&
                   GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Wingman &&
                   GameContext.typeTryWingman == GameContext.TypeTryWingman.Left)
        {
            typeWingmanLeft = (WingmanTypeEnum)CacheGame.GetWingManUserTry();
            if (typeWingmanLeft <= 0) return;
            rankWingmanLeft = Rank.SSS;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament)
        {
            typeWingmanLeft = (WingmanTypeEnum)CacheGame.GetWingManLeftTournament();
            if (typeWingmanLeft <= 0) return;
            rankWingmanLeft = Rank.SSS;
        }
        else
        {
            typeWingmanLeft = (WingmanTypeEnum)CacheGame.GetWingManLeftUserSelected();
            if (typeWingmanLeft <= 0) return;
            rankWingmanLeft = CacheGame.GetWingmanRank(typeWingmanLeft);
        }
        //if (typeWingmanLeft <= 0) return;
        leftWingman = Instantiate(listWingmanPrefab[(int)typeWingmanLeft - 1], wingmanMoveScript.transform).GetComponent<Wingman>();
        leftWingman.transform.localPosition = new Vector3(-1f, 0f, 0f);
        //playerInitedWingman = true;
        leftWingman.InitData(typeWingmanLeft, rankWingmanLeft, TypePlaneShot.LeftWingman);
    }

    private void InitRightWingmanCampaign(Transform parrent)
    {
        WingmanTypeEnum typeWingmanRight;
        Rank rankWingmanRight;
        if (SceneManager.GetActiveScene().name == "LevelTutorial")
        {
            typeWingmanRight = WingmanTypeEnum.AutoGatlingGun;
            if (typeWingmanRight <= 0) return;
            rankWingmanRight = CacheGame.GetWingmanRank(typeWingmanRight);
        }
        else if (SceneManager.GetActiveScene().name == "LevelTryPlane" &&
                   GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Wingman &&
                   GameContext.typeTryWingman == GameContext.TypeTryWingman.Right)
        {
            typeWingmanRight = (WingmanTypeEnum)CacheGame.GetWingManUserTry();
            if (typeWingmanRight <= 0) return;
            rankWingmanRight = Rank.SSS;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament)
        {
            typeWingmanRight = (WingmanTypeEnum)CacheGame.GetWingManRightTournament();
            if (typeWingmanRight <= 0) return;
            rankWingmanRight = Rank.SSS;
        }
        else
        {
            typeWingmanRight = (WingmanTypeEnum)CacheGame.GetWingManRightUserSelected();
            if (typeWingmanRight <= 0) return;
            rankWingmanRight = CacheGame.GetWingmanRank(typeWingmanRight);

        }

        //if (typeWingmanRight <= 0) return;
        rightWingman = Instantiate(listWingmanPrefab[(int)typeWingmanRight - 1], wingmanMoveScript.transform).GetComponent<Wingman>();
        rightWingman.transform.localPosition = new Vector3(1f, 0f, 0f);
        rightWingman.InitData(typeWingmanRight, rankWingmanRight, TypePlaneShot.RightWingman);
    }

    private void InitWingCampaign(Transform parrent)
    {
        WingTypeEnum wingTypeCampaignInit;
        Rank rankWingInit;

        //if (SceneManager.GetActiveScene().name == "LevelTutorial")
        //{
        //    typeAircraftCampaignInit = AircraftTypeEnum.TwilightX;
        //    rankAircraftInit = Rank.S;
        //}
        if (SceneManager.GetActiveScene().name == "LevelTryPlane" && GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Wing)
        {
            wingTypeCampaignInit = (WingTypeEnum)CacheGame.GetWingUserTry();
            if (wingTypeCampaignInit <= 0) return;
            rankWingInit = Rank.SSS;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament)
        {
            wingTypeCampaignInit = (WingTypeEnum)CacheGame.GetWingTournament();
            if (wingTypeCampaignInit <= 0) return;
            rankWingInit = Rank.SSS;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            wingTypeCampaignInit = WingTypeEnum.WingOfResolution;
            if (wingTypeCampaignInit <= 0) return;
            rankWingInit = Rank.S;
        }
        else
        {
            wingTypeCampaignInit = (WingTypeEnum)CacheGame.GetWingUserSelected();
            if (wingTypeCampaignInit <= 0) return;
            rankWingInit = CacheGame.GetWingRank(wingTypeCampaignInit);
        }
        if (wingTypeCampaignInit <= 0) return;
        Debug.LogError("wingTypeCampaignInit:" + wingTypeCampaignInit);

        wing = Instantiate(listWingPrefab[(int)wingTypeCampaignInit - 1], parrent).GetComponent<Wing>();
        wing.transform.localPosition = Vector3.zero;
        wing.InitData(wingTypeCampaignInit, rankWingInit, TypePlaneShot.Wing, this);

    }


    int mainWeaponPowerLevelInitAircraft;
    int mainWeaponQuantityLevelInitAircraft;
    int mainWeaponSpeedLevelInitAircraft;

    int secondWeaponPowerLevelInitAircraft;
    int secondWeaponQuantityLevelInitAircraft;
    int secondWeaponSpeedLevelInitAircraft;

    int mainWeaponPowerLevelInitWingmanLeft;
    int mainWeaponPowerLevelInitWingmanRight;
    //
    float mainWeaponPowerPercentBonusAircraft;
    float mainWeaponSpeedPercentBonusAircraft;
    float secondWeaponPowerPercentBonusAircraft;
    float secondWeaponSpeedPercentBonusAircraft;

    float mainWeaponPowerPercentBonusWingman;

    int mainWeaponPowerLevelWing;
    float mainWeaponPowerPercentWing;


    int numItemPowerUpUse;
    private void InitCarryOnItemAircraft()              //Add item mang vào
    {
        //
        //AircraftTypeEnum type = (AircraftTypeEnum)CacheGame.GetSpaceShipUserSelected();
        if (SceneManager.GetActiveScene().name == "LevelTutorial")
        {
            numItemPowerUpUse = 86;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            numItemPowerUpUse = 0;

        }
        else if (SceneManager.GetActiveScene().name == "LevelTryPlane" && GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Aircraft)
        {
            numItemPowerUpUse = 210;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament)
        {
            numItemPowerUpUse = 210 - CacheGame.GetSpaceShipLevel(aircraft.AircraftType);
        }
        else
        {
            if (usingAircraftBackup)
            {
                numItemPowerUpUse = 0;
            }
            else
            {
                numItemPowerUpUse = CacheGame.numItemPowerUpUse * CacheFireBase.GetNumLevelAdd_When_GetPowerUp;
            }
        }
        Debug.LogError("numItemPowerUpUse: " + numItemPowerUpUse);
        mainWeaponPowerLevelInitAircraft = numItemPowerUpUse + CacheGame.GetSpaceShipLevel(aircraft.AircraftType);
        mainWeaponQuantityLevelInitAircraft = numItemPowerUpUse + CacheGame.GetSpaceShipLevel(aircraft.AircraftType);
        mainWeaponSpeedLevelInitAircraft = numItemPowerUpUse + CacheGame.GetSpaceShipLevel(aircraft.AircraftType);

        secondWeaponPowerLevelInitAircraft = numItemPowerUpUse + CacheGame.GetSpaceShipLevel(aircraft.AircraftType);
        secondWeaponQuantityLevelInitAircraft = numItemPowerUpUse + CacheGame.GetSpaceShipLevel(aircraft.AircraftType);
        secondWeaponSpeedLevelInitAircraft = numItemPowerUpUse + CacheGame.GetSpaceShipLevel(aircraft.AircraftType);


        mainWeaponPowerPercentBonusAircraft = VipBonusValue.damePlaneRate;

        //Thêm cho Aircraft Main Weapon
        aircraft.AddMainWeaponPowerLevel(mainWeaponPowerLevelInitAircraft);
        aircraft.AddMainWeaponQuantityLevel(mainWeaponQuantityLevelInitAircraft);
        aircraft.AddMainWeaponSpeedLevel(mainWeaponSpeedLevelInitAircraft);
        aircraft.ChangeMainWeaponPowerPercent(mainWeaponPowerPercentBonusAircraft);
        aircraft.ChangeMainWeaponSpeedPercent(mainWeaponSpeedPercentBonusAircraft);

        //Thêm cho Aircraft Second Weapon
        aircraft.AddSecondWeaponPowerLevel(secondWeaponPowerLevelInitAircraft);
        aircraft.AddSecondWeaponSpeedLevel(secondWeaponSpeedLevelInitAircraft);
        aircraft.ChangeSecondWeaponPowerPercent(secondWeaponPowerPercentBonusAircraft);
        aircraft.ChangeSecondWeaponSpeedPercent(secondWeaponSpeedPercentBonusAircraft);

        //Thêm mạng + UsingSkillNumber
        life = 0;
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            //life = 3;
            InitLife(3);
        }
        else
        {
            if (usingAircraftBackup)
            {
                InitLife(GameContext.numLifeInitAircraftBackup);
            }
            else
            {
                InitLife(GameContext.numLifeInitAircraftFirst + CacheGame.numLifeUpUse);
                aircraft.ChangeUsingSkillNumber(CacheGame.numUsingSkill);
                if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
                {
                    InitLife(2);
                }
            }

            if (SceneManager.GetActiveScene().name == "LevelTutorial")
            {
                life = 999;
                InitLife(0);
            }
        }
        if (MinhCacheGame.IsAlreadyBuyRoyalPack())
        {
            aircraft.ChangeUsingSkillNumber(1);
        }

        //--test
        //if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        //{
            life = 999;
        //}

        //-----wingman

    }
    private void InitCarryOnItemWingmanLeft()              //Add item mang vào
    {
        if (leftWingman != null)
        {
            if (SceneManager.GetActiveScene().name == "LevelTutorial")
            {
                mainWeaponPowerLevelInitWingmanLeft = 50;
            }
            else if (SceneManager.GetActiveScene().name == "LevelTryPlane" &&
                  GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Wingman &&
                  GameContext.typeTryWingman == GameContext.TypeTryWingman.Left)
            {
                mainWeaponPowerLevelInitWingmanLeft = 210;
                Debug.LogError("mainWeaponPowerLevelInitWingmanLeft " + mainWeaponPowerLevelInitWingmanLeft);

            }
            else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament)
            {
                mainWeaponPowerLevelInitWingmanLeft = 210;
            }
            else
            {
                mainWeaponPowerLevelInitWingmanLeft = CacheGame.GetWingmanLevel(leftWingman.WingmanType);
            }

            leftWingman.AddPowerLevel(mainWeaponPowerLevelInitWingmanLeft);
            leftWingman.ChangeMainWeaponPowerPercent(mainWeaponPowerPercentBonusWingman);
        }
    }
    private void InitCarryOnItemWingmanRight()              //Add item mang vào
    {
        if (rightWingman != null)
        {
            if (SceneManager.GetActiveScene().name == "LevelTutorial")
            {
                mainWeaponPowerLevelInitWingmanRight = 50;
            }
            else if (SceneManager.GetActiveScene().name == "LevelTryPlane" &&
                  GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Wingman &&
                  GameContext.typeTryWingman == GameContext.TypeTryWingman.Right)
            {
                mainWeaponPowerLevelInitWingmanRight = 210;
                Debug.LogError("mainWeaponPowerLevelInitWingmanRight " + mainWeaponPowerLevelInitWingmanRight);
            }
            else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament)
            {
                mainWeaponPowerLevelInitWingmanRight = 210;
            }
            else
            {
                mainWeaponPowerLevelInitWingmanRight = CacheGame.GetWingmanLevel(rightWingman.WingmanType);
            }
            //mainWeaponPowerLevelInitWingmanRight = CacheGame.GetWingmanLevel(rightWingman.WingmanType);
            rightWingman.AddPowerLevel(mainWeaponPowerLevelInitWingmanRight);
            rightWingman.ChangeMainWeaponPowerPercent(mainWeaponPowerPercentBonusWingman);
        }

    }

    private void InitCarryOnItemWingCampaign()              //Add item mang vào
    {
        if (wing == null) return;

        if (SceneManager.GetActiveScene().name == "LevelTutorial")
        {
            mainWeaponPowerLevelWing = 50;
        }
        else if (SceneManager.GetActiveScene().name == "LevelTryPlane" &&
              GameContext.typeOpenPopupFromTry == GameContext.TypeOpenPopupFromTry.Wing)
        {
            mainWeaponPowerLevelWing = 210;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament)
        {
            mainWeaponPowerLevelWing = 210;
        }
        else if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            mainWeaponPowerLevelWing = 120;
        }
        else
        {
            mainWeaponPowerLevelWing = CacheGame.GetWingLevel(wing.WingType);
        }
        Debug.LogError("mainWeaponPowerLevelWing " + mainWeaponPowerLevelWing);

        wing.AddPowerLevel(mainWeaponPowerLevelWing);
        wing.ChangePowerPercent(mainWeaponPowerPercentWing);
    }



    //Hàm này dùng để set số life của máy bay lúc bắt đầu
    public void InitLife(int amount)
    {
        life += amount;
        PanelUITop.Instance.SetTextLife(life);
    }
    //Hàm này dùng thay Đổi số life trong quá trình chơi . Nếu pvp thì chỉ trừ life chứ ko cộng 
    public void ChangeNumberLife(int amount)
    {
        if ((GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2) && amount > 0)
        {
            return;
        }
        life += amount;
        PanelUITop.Instance.SetTextLife(life);
    }

    //truyền vào lượng phần trăm bonus thêm cho coin. ex 30% = 0.3
    public void ChangeCoinPercentBonus(float amount)
    {
        if ((GameContext.modeGamePlay == GameContext.ModeGamePlay.pvp || GameContext.modeGamePlay == GameContext.ModeGamePlay.Tournament || GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2))
        {
            return;
        }
        coinPercentBonus += amount;
    }

    //truyền vào lượng phần trăm bonus thêm cho Score. ex 30% = 0.3
    public void ChangeScorePercentBonus(float amount)
    {
        scorePercentBonus += amount;
    }



    //------------------2v2-------------------------------------------------
    Color color50 = new Color(1, 1, 1, 0.66f);
    Color color10 = new Color(1, 1, 1, 0.1f);
    public void InitPlayerPvP2v2()
    {
        if (isTestingMode)
        {
            InitAircraft2v2(transform, aircraftTest, aircraftRankTest);

            InitLeftWingman2v2(transform, wingManLeftTest, wingManLeftRankTest);
            InitRightWingman2v2(transform, wingManRightTest, wingManRightRankTest);

            InitWingCampaign(Aircraft.transform, wingTest, wingRankTest);


            InitCarryOnItemAircraft2v2(aircraftLevelTest, 0);
            InitCarryOnItemWingmanLeft2v2(wingManLeftLevelTest);
            InitCarryOnItemWingmanRight2v2(wingManRightLevelTest);
            InitCarryOnItemWing2v2(wingLevelTest);
        }
        else
        {
            PlayerDataUtil.PlayerProfileData profile = PlayerDataUtil.StringToObject(CachePvp.MyTeamate.data);
            int selected = CachePvp.HighestShip(profile);
            InitAircraft2v2(transform, (AircraftTypeEnum)selected, (Rank)profile.listRankPlane[selected - 1]);
            InitCarryOnItemAircraft2v2(profile.listLevelPlane[selected - 1], 0);
            int selectedLeftWingman = CachePvp.HighestWingmanLeft(profile);
            if (selectedLeftWingman > 0)
            {
                InitLeftWingman2v2(transform, (WingmanTypeEnum)selectedLeftWingman, (Rank)profile.listRankWingman[selectedLeftWingman - 1]);
                InitCarryOnItemWingmanLeft2v2(profile.listLevelWingmanNew[selectedLeftWingman - 1]);
            }
            int selectedRightWingman = CachePvp.HighestWingmanRight(profile);
            if (selectedRightWingman > 0)
            {
                InitRightWingman2v2(transform, (WingmanTypeEnum)selectedRightWingman, (Rank)profile.listRankWingman[selectedRightWingman - 1]);
                InitCarryOnItemWingmanRight2v2(profile.listLevelWingmanNew[selectedRightWingman - 1]);
            }
            int selectedWing = CachePvp.HighestWing(profile);
            if (selectedWing > 0)
            {
                InitWingCampaign(Aircraft.transform, (WingTypeEnum)selectedWing, (Rank)profile.listRankWing[selectedWing - 1]);
                InitCarryOnItemWing2v2(profile.listLevelWing[selectedWing - 1]);
            }
        }
    }

    private void InitAircraft2v2(Transform parrent, AircraftTypeEnum aircraft2v2Init, Rank rankAircraftInit)
    {
        Debug.Log("typeAircraft2v2Init:" + aircraft2v2Init);

        aircraft = Instantiate(listAircraftPrefab[(int)aircraft2v2Init - 1], parrent).GetComponent<Aircraft>();
        aircraft.InitData(aircraft2v2Init, rankAircraftInit, TypePlaneShot.Aircraft);
        if (aircraft.transform.childCount > 0)
        {
            SpriteMask s = aircraft.transform.GetChild(0).GetComponent<SpriteMask>();
            if (s != null)
            {
                s.enabled = false;
            }
        }

        SpriteRenderer sprite;
        sprite = aircraft.GetComponent<SpriteRenderer>();
        sprite.color = color50;
        sprite.sortingOrder = 11;

        Transform flatFlameLeft = aircraft.transform.Find("particle_duoi").GetChild(1);
        Transform flatFlameRight = aircraft.transform.Find("particle_duoi").GetChild(0);

#pragma warning disable CS0618 // Type or member is obsolete
        flatFlameLeft.GetComponent<ParticleSystem>().startColor = color10;
        flatFlameLeft.GetChild(0).GetComponent<ParticleSystem>().startColor = color10;
        flatFlameRight.GetComponent<ParticleSystem>().startColor = color10;
        flatFlameRight.GetChild(0).GetComponent<ParticleSystem>().startColor = color10;
#pragma warning restore CS0618 // Type or member is obsolete
    }

    private void InitLeftWingman2v2(Transform parrent, WingmanTypeEnum leftWingman2v2Init, Rank rankLeftWingman2v2Init)
    {
        if (leftWingman2v2Init <= 0) return;
        leftWingman = Instantiate(listWingmanPrefab[(int)leftWingman2v2Init - 1], wingmanMoveScript.transform).GetComponent<Wingman>();
        leftWingman.transform.localPosition = new Vector3(-1f, 0f, 0f);
        //playerInitedWingman = true;
        leftWingman.InitData(leftWingman2v2Init, rankLeftWingman2v2Init, TypePlaneShot.LeftWingman);
        SpriteRenderer sprite;
        sprite = leftWingman.GetComponent<SpriteRenderer>();
        sprite.color = color50;
        sprite.sortingOrder = 10;
#pragma warning disable CS0618 // Type or member is obsolete
        leftWingman.transform.FindChild("particle_duoi").GetChild(0).GetComponent<ParticleSystem>().startColor = color10;
#pragma warning restore CS0618 // Type or member is obsolete
    }
    private void InitRightWingman2v2(Transform parrent, WingmanTypeEnum rightWingman2v2Init, Rank rankRightWingman2v2Init)
    {
        if (rightWingman2v2Init <= 0) return;
        rightWingman = Instantiate(listWingmanPrefab[(int)rightWingman2v2Init - 1], wingmanMoveScript.transform).GetComponent<Wingman>();
        rightWingman.transform.localPosition = new Vector3(1f, 0f, 0f);
        rightWingman.InitData(rightWingman2v2Init, rankRightWingman2v2Init, TypePlaneShot.RightWingman);
        SpriteRenderer sprite;
        sprite = rightWingman.GetComponent<SpriteRenderer>();
        sprite.color = color50;
        sprite.sortingOrder = 10;
#pragma warning disable CS0618 // Type or member is obsolete
        rightWingman.transform.FindChild("particle_duoi").GetChild(0).GetComponent<ParticleSystem>().startColor = color10;
#pragma warning restore CS0618 // Type or member is obsolete
    }

    private void InitWingCampaign(Transform parrent, WingTypeEnum wing2v2Init, Rank rankWing2v2Init)
    {
        if (wing2v2Init <= 0) return;
        //Debug.LogError("wingTypeCampaignInit:" + wingTypeCampaignInit);
        wing = Instantiate(listWingPrefab[(int)wing2v2Init - 1], parrent).GetComponent<Wing>();
        wing.transform.localPosition = Vector3.zero;
        wing.InitData(wing2v2Init, rankWing2v2Init, TypePlaneShot.Wing, this);
        try
        {
            if (wing2v2Init == WingTypeEnum.WingOfJustice)
            {
                Transform child = wing.transform.GetChild(wing.transform.childCount - 1);
                for (int i = 0; i < child.childCount; i++)
                {
                    if (i == 1)
                    {
                        child.GetChild(i).GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = 12;
                    }
                    else
                    {
                        child.GetChild(i).GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = 10;
                    }
                    if (i == 4)
                    {
#pragma warning disable CS0618 // Type or member is obsolete
                        child.GetChild(i).GetComponent<ParticleSystem>().startColor = color10;
#pragma warning restore CS0618 // Type or member is obsolete
                    }
                }
            }
            else if (wing2v2Init == WingTypeEnum.WingOfRedemption)
            {
                Transform child = wing.transform.GetChild(0);
                for (int i = 0; i < child.childCount; i++)
                {
                    child.GetChild(i).GetComponent<SpriteRenderer>().color = color50;
                    child.GetChild(i).GetComponent<TweenAlpha>().from = 0.5f;
                }
                child = wing.transform.GetChild(1);
                for (int i = 0; i < child.childCount; i++)
                {
                    if (i == 1)
                    {
                        child.GetChild(i).GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = 12;
                    }
                    else
                    {
                        child.GetChild(i).GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = 10;
                    }
                    if (i == 4)
                    {
#pragma warning disable CS0618 // Type or member is obsolete
                        child.GetChild(i).GetComponent<ParticleSystem>().startColor = color10;
#pragma warning restore CS0618 // Type or member is obsolete
                    }
                }
            }
            else if (wing2v2Init == WingTypeEnum.WingOfResolution)
            {
                Transform child = wing.transform.GetChild(wing.transform.childCount - 1);
                for (int i = 0; i < child.childCount; i++)
                {
                    child.GetChild(i).GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = 10;
#pragma warning disable CS0618 // Type or member is obsolete
                    child.GetChild(i).GetComponent<ParticleSystem>().startColor = color10;
#pragma warning restore CS0618 // Type or member is obsolete
                }
            }
        }
        catch (Exception)
        {

        }
    }

    //--Item 2v2

    private void InitCarryOnItemAircraft2v2(int numPowerUpInit, int numLifeInit)              //Add item mang vào
    {
        life = 3;
        Debug.LogError("numItemPowerUpUse: " + numPowerUpInit);
        //Thêm cho Aircraft Main Weapon

        aircraft.AddMainWeaponPowerLevel(numPowerUpInit);
        aircraft.AddMainWeaponQuantityLevel(numPowerUpInit);
        aircraft.AddMainWeaponSpeedLevel(numPowerUpInit);
        //Thêm cho Aircraft Second Weapon
        aircraft.AddSecondWeaponPowerLevel(numPowerUpInit);
        aircraft.AddSecondWeaponSpeedLevel(numPowerUpInit);
        //Thêm mạng + UsingSkillNumber
    }

    private void InitCarryOnItemWingmanLeft2v2(int powerLevelWingmanLeft2v2)              //Add item mang vào
    {
        if (leftWingman == null) return;
        leftWingman.AddPowerLevel(powerLevelWingmanLeft2v2);
    }
    private void InitCarryOnItemWingmanRight2v2(int powerLevelWingmanRight2v2)              //Add item mang vào
    {
        if (rightWingman == null) return;
        rightWingman.AddPowerLevel(powerLevelWingmanRight2v2);
    }

    private void InitCarryOnItemWing2v2(int powerLevelWing2v2)              //Add item mang vào
    {
        if (wing == null) return;
        //Debug.LogError("mainWeaponPowerLevelWing " + mainWeaponPowerLevelWing);
        wing.AddPowerLevel(powerLevelWing2v2);
    }
}
