﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

public abstract class Wingman : MonoBehaviour
{

    private WingmanTypeEnum wingmanType;
    public WingmanTypeEnum WingmanType { get { return wingmanType; } }

    private Rank rank;
    public Rank Rank { get { return rank; } }

    private TypePlaneShot typeWingmanShotBulletHell;
    public TypePlaneShot TypeWingmanShotBulletHell { get { return typeWingmanShotBulletHell; } }

    //private TypePlayerByModeEnum typeWingmanByMode;
    //public TypePlayerByModeEnum TypeWingmanByMode { get { return typeWingmanByMode; } }


    public UbhShotCtrl mainWeaponUbhShotCtrl;
    public SpaceWarShootSetData[] listMainWeaponUbhSetData;

    //public RuntimeAnimatorController[] listAnimatorController;
    public List<Sprite> listSpriteMainWeaponBullet;

    //public Animator animWingman;
    protected PlayerController playerControllerFalconScript;
    protected PlayerInit playerInitScript;

    protected int mainWeaponPowerLevel;
    protected float mainWeaponPowerPercent = 1.0f; //Mặc định là 1, tương ứng với 100%

    [DisplayAsString]
    public int mainWeaponPower;
    [DisplayAsString]
    public Sprite mainWeaponSprite;

    public bool HasSkillRank(Rank rank)
    {
        return (int)this.rank >= (int)rank;
    }

    public void Start()
    {
        //animWingman = GetComponent<Animator>();
        playerInitScript = GetComponentInParent<WingmanMove>().playerInitIns;
    }
    //------------------- CHANGE DATA ------------------------------------------------------
    //Load data khi chưa thêm bất cứ extra nào cả
    public void InitData(WingmanTypeEnum type, Rank rank, TypePlaneShot typePlaneShot )
    {
        wingmanType = type;
        this.rank = rank;
        typeWingmanShotBulletHell = typePlaneShot;
        //this.typeWingmanByMode = typeWingmanByModeInit;
    }

    public void AddPowerLevel(int amount)
    {
        if (amount < 0) return;
        mainWeaponPowerLevel += amount;
        LoadMainWeaponPowerData();
        //mainWeaponPower = (int)(mainWeaponPower * mainWeaponPowerPercent);
        //Debug.LogError("Winaman: mainWeaponPowerLevel: " + mainWeaponPowerLevel + "----mainWeaponPower:" + mainWeaponPower);
        Debug.Log("<color=#00c336>" +
        "Wingman: mainWeaponPowerLevel: " + mainWeaponPowerLevel + "----mainWeaponPower:" + mainWeaponPower
        + "</color>");
    }

    public void ChangeMainWeaponPowerPercent(float amount)
    {
        mainWeaponPowerPercent += amount;
        LoadMainWeaponPowerData();
        mainWeaponPower = (int)(mainWeaponPower * mainWeaponPowerPercent);
        //Debug.LogError("mainWeaponPowerPercent: " + mainWeaponPowerPercent + "----mainWeaponPower:" + mainWeaponPower);
        Debug.Log("<color=#00c336>" +
        "Wingman: mainWeaponPowerPercent: " + mainWeaponPowerPercent + "----mainWeaponPower:" + mainWeaponPower
        + "</color>");
    }

    //---get sprite sử dụng

    protected void GetSpriteMainWeaponUse()
    {
        int indexGetSprite = (int)(mainWeaponPowerLevel / 30);

        if (indexGetSprite >= listSpriteMainWeaponBullet.Count)
        {
            indexGetSprite = listSpriteMainWeaponBullet.Count - 1;
            Debug.Log("indexGetValue get Sprite Bullet quá count ");
        }
        if (listSpriteMainWeaponBullet[indexGetSprite] != null)
        {
            mainWeaponSprite = listSpriteMainWeaponBullet[indexGetSprite];
        }
        //Debug.LogError("mainWeaponPowerLevel:" + mainWeaponPowerLevel + "---indexGetSprite:" + indexGetSprite + "---mainWeaponSprite:" + mainWeaponSprite);
        Debug.Log("<color=#00c336>" +
         "mainWeaponPowerLevel:" + mainWeaponPowerLevel + "---indexGetSprite:" + indexGetSprite + "---mainWeaponSprite:" + mainWeaponSprite
          + "</color>");
    }


    //-------------
    protected abstract void LoadMainWeaponPowerData();

    public abstract void ShootMainWeapon();
    public abstract void StopShootMainWeapon();
}
