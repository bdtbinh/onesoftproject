﻿using UnityEngine;

public class WingmanAnimations : MonoBehaviour {

    private UISpriteAnimation spriteAnim;

    private UISpriteAnimation SpriteAnim
    {
        get
        {
            if (spriteAnim == null)
            {
                spriteAnim = GetComponent<UISpriteAnimation>();
                return spriteAnim;
            }
            else
            {
                return spriteAnim;
            }
        }
    }

    private void Awake()
    {
        spriteAnim = GetComponent<UISpriteAnimation>();
    }

    public void PlayAnimations(WingmanTypeEnum type, Rank rank)
    {
        SpriteAnim.namePrefix = "wingman" + (int)type + "_e" + (int)rank + "_idle";
        SpriteAnim.ResetToBeginning();
    }
}
