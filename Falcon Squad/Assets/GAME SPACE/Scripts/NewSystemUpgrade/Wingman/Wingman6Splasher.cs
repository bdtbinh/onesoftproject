﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wingman6Splasher : Wingman
{
    [Title("Data riêng", bold: true, horizontalLine: true)]


    public void Start()
    {
        base.Start();
        for (int i = 0; i < listMainWeaponUbhSetData.Length; i++)
        {
            listMainWeaponUbhSetData[i].playerInitScript = playerInitScript;
            listMainWeaponUbhSetData[i].typePlaneShotBulletHell = TypeWingmanShotBulletHell;
            listMainWeaponUbhSetData[i].typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.WingmanMainPower;
        }
    }

    protected override void LoadMainWeaponPowerData()
    {
        if (mainWeaponPowerLevel >= WingM6SplasherSheet.GetDictionary().Count)
        {
            mainWeaponPowerLevel = WingM6SplasherSheet.GetDictionary().Count - 1;
        }
        mainWeaponPower = WingM6SplasherSheet.Get(mainWeaponPowerLevel).mainweapon_power;
        GetSpriteMainWeaponUse();
    }


    public override void ShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StartShotRoutine();
    }
    public override void StopShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StopShotRoutine();
    }
}
