﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WingmanMove : MonoBehaviour
{

    [Range(10, 50)]
    public float wingmanSpeed = 16;
    public PlayerInit playerInitIns;
    public bool wingmanReadyMove;
    Transform playerTrans;

    private void Start()
    {
        //StartCoroutine(StartMove());
    }

    public void WingmanStartMove()
    {
        //Debug.LogError("vao StartMove WingmanMove");
        //yield return new WaitUntil(() => playerInitIns.initPlayerCompleted == true);
        if (playerInitIns.LeftWingman !=null || playerInitIns.RightWingman != null)
        {
            playerTrans = playerInitIns.Aircraft.transform;
            wingmanReadyMove = true;
        }
        else
        {
            gameObject.SetActive(false);
        }


    }
    void Update()
    {
        if (wingmanReadyMove && playerTrans != null)
        {
            var targetpos = playerTrans.position;
            targetpos.y -= 0.5f;
            transform.position = Vector3.Lerp(transform.position, targetpos, Time.deltaTime * wingmanSpeed);
        }

    }
}
