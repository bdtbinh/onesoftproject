﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wingman1Galting : Wingman
{
    [Title("Data riêng", bold: true, horizontalLine: true)]


    public void Start()
    {
        base.Start();

        InitDataByRank();

        for (int i = 0; i < listMainWeaponUbhSetData.Length; i++)
        {
            listMainWeaponUbhSetData[i].playerInitScript = playerInitScript;
            listMainWeaponUbhSetData[i].typePlaneShotBulletHell = TypeWingmanShotBulletHell;
            listMainWeaponUbhSetData[i].typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.WingmanMainPower;
        }
    }

    void InitDataByRank()
    {
        if (HasSkillRank(Rank.C))
        {
            playerInitScript.ChangeScorePercentBonus(0.1f);
        }
        if (HasSkillRank(Rank.B))
        {
        }
        if (HasSkillRank(Rank.A))
        {
        }
        if (HasSkillRank(Rank.S))
        {
        }
        if (HasSkillRank(Rank.SS))
        {
        }
        if (HasSkillRank(Rank.SSS))
        {
        }
    }

    protected override void LoadMainWeaponPowerData()
    {
        if (mainWeaponPowerLevel >= WingM1GaltingSheet.GetDictionary().Count)
        {
            mainWeaponPowerLevel = WingM1GaltingSheet.GetDictionary().Count - 1;
        }
        mainWeaponPower = WingM1GaltingSheet.Get(mainWeaponPowerLevel).mainweapon_power;
        GetSpriteMainWeaponUse();
    }


    public override void ShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StartShotRoutine();
        //Debug.LogError("ShootMainWeapon");
    }
    public override void StopShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StopShotRoutine();
        //Debug.LogError("StopShootMainWeapon");
    }
}
