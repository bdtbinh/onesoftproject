﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wingman3Lazer : Wingman
{
    [Title("Data riêng", bold: true, horizontalLine: true)]
    public MinhLaser laserHitScript;

    public float timeShowLaser = 2f;
    public float timeHideLaser = 3f;

    public void Start()
    {
        base.Start();

        if (laserHitScript != null)
        {
            laserHitScript.OnLaserDamageTarget += LaserOnOnLaserHitTriggered;
        }
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    protected override void LoadMainWeaponPowerData()
    {
        if (mainWeaponPowerLevel >= WingM3LazerSheet.GetDictionary().Count)
        {
            mainWeaponPowerLevel = WingM3LazerSheet.GetDictionary().Count - 1;
        }
        mainWeaponPower = WingM3LazerSheet.Get(mainWeaponPowerLevel).mainweapon_power;
    }


    public override void ShootMainWeapon()
    {
        StopAllCoroutines();
        if (laserHitScript != null)
        {
            StartCoroutine("LaserShootLoop", 1f);
        }
    }
    public override void StopShootMainWeapon()
    {
        StopAllCoroutines();
        if (laserHitScript != null)
        {
            laserHitScript.gameObject.SetActive(false);
        }
    }


    IEnumerator LaserShootLoop(float timeDelay)
    {

        yield return new WaitForSeconds(timeDelay);
        if (gameObject.activeInHierarchy)
        {
            laserHitScript.gameObject.SetActive(true);
            //Debug.LogError("laser shoot");
            StartCoroutine("LaserPauseShoot", timeShowLaser);
        }
        else
        {
            StopAllCoroutines();
        }
    }
    IEnumerator LaserPauseShoot(float timeDelayStop)
    {
        yield return new WaitForSeconds(timeDelayStop);
        if (gameObject.activeInHierarchy)
        {
            laserHitScript.gameObject.SetActive(false);
            StartCoroutine("LaserShootLoop", timeHideLaser);
        }
        else
        {
            StopAllCoroutines();
        }
    }

    private void LaserOnOnLaserHitTriggered(RaycastHit2D hitInfo)
    {
        if (LayerMask.LayerToName(hitInfo.collider.gameObject.layer) == "Enemy" && playerInitScript.typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            if (hitInfo.collider.GetComponent<EnemyCollisionManager>() != null)
            {
                if (hitInfo.collider.GetComponent<EnemyCollisionManager>().enemyCollisionBase != null)
                {
                    hitInfo.collider.GetComponent<EnemyCollisionManager>().enemyCollisionBase.LaserOnOnLaserHitTriggeredEnemy(mainWeaponPower);
                }
                else
                {
                    Debug.LogError("wingman 3 lazer khong tru mau " + hitInfo.collider.gameObject.name);
                }
            }
            else
            {
                Debug.LogError("wingman 3 lazer khong tru mau " + hitInfo.collider.gameObject.name);
            }
        }
    }
}
