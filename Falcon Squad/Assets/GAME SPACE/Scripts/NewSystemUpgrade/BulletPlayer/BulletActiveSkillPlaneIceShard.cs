﻿using PathologicalGames;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletActiveSkillPlaneIceShard : MonoBehaviour
{
    UbhBullet ubhBulletIns;
    public BulletPlayerValue bulletPlayerValueIns;

    public ParticleSystem explosiveFx;
    //public GameObject sBullet;
    public UbhShotCtrl[] listGunCtrlUse;


    public SpaceWarShootSetData[] listGunSkillActiveUbhSetData;

    //public CircleCollider2D circleCollider2D;

    public float timeBeforeShoot;
    //public float timeExistExplosive;

    ////public float sizeBoxColliderNormal = 0.35f;
    //public float sizeBoxColliderExplosive = 1.1f;

    bool bulletInScene = true;

    bool isExplosiving = false;

    void Start()
    {
        ubhBulletIns = gameObject.GetComponent<UbhBullet>();

        for (int i = 0; i < listGunSkillActiveUbhSetData.Length; i++)
        {
            //Debug.LogError("BulletActiveSkillPlaneIceShard Start listGunSkillActiveUbhSetData");
            listGunSkillActiveUbhSetData[i].playerInitScript = bulletPlayerValueIns.PlayerInitIns;
            listGunSkillActiveUbhSetData[i].typeGunPlayerShotBulletHell = bulletPlayerValueIns.TypeGunPlayerShotBulletHellInBullet;
        }
    }

    void OnEnable()
    {
        bulletInScene = true;
        StartShoot(timeBeforeShoot);
        //Debug.Log(transform.localPosition);
    }

    void OnDisable()
    {
        StopAllCoroutines();
        //isExplosiving = false;
        //circleCollider2D.radius = sizeBoxColliderNormal;
        explosiveFx.gameObject.SetActive(false);
        //sBullet.SetActive(true);
    }
    //private void OnBecameVisible()
    //{
    //    bulletInScene = true;
    //    Debug.LogError("OnBecameVisible vao");
    //}


    //private void OnBecameInvisible()
    //{
    //    bulletInScene = false;
    //    Debug.LogError("OnBecameInvisible ra");
    //}
    public void StartShoot(float timeBeforeShootUse)
    {
        //sBullet.SetActive(false);
        explosiveFx.gameObject.SetActive(true);
        explosiveFx.Play(true);
        this.Delay(timeBeforeShootUse, () =>
        {
            ubhBulletIns.m_shooting = false;
            this.Delay(0.5f, () =>
            {
                if (gameObject.activeInHierarchy)
                {
                    foreach (UbhShotCtrl gun in listGunCtrlUse)
                    {
                        gun.StartShotRoutine();
                    }
                }
            }, true);

        }, true);
    }


    public void FinishTweenShootMainWeapon()
    {
        //Debug.LogError("BulletActiveSkillPlaneIceShard FinishTweenShootMainWeapon");
        //sBullet.SetActive(true);
        explosiveFx.Stop();
        explosiveFx.gameObject.SetActive(false);
        foreach (UbhShotCtrl gun in listGunCtrlUse)
        {
            gun.StopShotRoutine();
            //Debug.LogError("BulletActiveSkillPlaneIceShard FinishTweenShootMainWeapon gun.shooting: " + gun.shooting);
        }
        Despawn();
    }

    void Despawn()
    {
        //Debug.LogError("BulletActiveSkillPlaneIceShard Despawn");
        if (ubhBulletIns != null)
        {
            UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
        }
    }
}
