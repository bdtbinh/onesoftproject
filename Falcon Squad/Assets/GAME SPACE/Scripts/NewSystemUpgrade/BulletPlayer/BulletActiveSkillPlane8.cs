﻿using PathologicalGames;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletActiveSkillPlane8 : MonoBehaviour
{
    public UbhBullet ubhBulletIns;

    public ParticleSystem explosiveFx;
    public GameObject sBullet;

    public CircleCollider2D circleCollider2D;

    public float timeBeforeExplosive;
    public float timeExistExplosive;

    public float sizeBoxColliderNormal = 0.35f;
    public float sizeBoxColliderExplosive = 1.1f;



    bool isExplosiving = false;

    //void Start()
    //{
    //    ubhBulletIns = gameObject.GetComponent<UbhBullet>();
    //}

    void OnEnable()
    {
        StartExplosive(timeBeforeExplosive);

        //Debug.Log(transform.localPosition);
    }

    void OnDisable()
    {
        StopAllCoroutines();
        isExplosiving = false;
        circleCollider2D.radius = sizeBoxColliderNormal;
        explosiveFx.gameObject.SetActive(false);
        sBullet.SetActive(true);
    }

    public void StartExplosive(float timeBeforeExplosiveUse = 0)
    {
        this.Delay(timeBeforeExplosiveUse, () =>
        {
            if (!isExplosiving)
            {
                isExplosiving = true;
                ubhBulletIns.m_shooting = false;
                sBullet.SetActive(false);
                circleCollider2D.radius = sizeBoxColliderExplosive;
                explosiveFx.gameObject.SetActive(true);
                explosiveFx.Play(true);

                this.Delay(timeExistExplosive, () =>
                {
                    Despawn();
                }, true);
            }
        }, true);
    }


    void Despawn()
    {
        if (ubhBulletIns != null)
        {
            UbhObjectPool.instance.ReleaseBullet(ubhBulletIns);
        }
    }
}
