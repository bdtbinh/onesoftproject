﻿using PathologicalGames;
using Sirenix.OdinInspector;
using SkyGameKit;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public abstract class Wing : MonoBehaviour
{
    [HideInInspector]
    public PlayerInit playerInitScript;

    GameObject fxAnimWingUse;
    public GameObject[] listFxAnimWing;


    private WingTypeEnum wingType;
    public WingTypeEnum WingType { get { return wingType; } }

    private Rank rank;
    public Rank Rank { get { return rank; } }

    private TypePlaneShot typeShotBulletHell;
    public TypePlaneShot TypeShotBulletHell { get { return typeShotBulletHell; } }

    protected int usingSkillNumber;         //Số lần sử dụng skill
    public int UsingSkillNumber { get { return usingSkillNumber; } }

    protected float usingSkillTime;         //Thời gian 1 lần sử dụng skill
    public float UsingSkillTime { get { return usingSkillTime; } }

    protected float usingSkillCountdownTime;
    public float UsingSkillCountdownTime { get { return usingSkillCountdownTime; } }

    protected int mainWeaponPowerLevel;
    protected float mainWeaponPowerPercent = 1.0f; //Mặc định là 1, tương ứng với 100%


    [DisplayAsString]
    public int mainWeaponPower;
    [DisplayAsString]
    public Sprite mainWeaponSprite;

    public System.Action<int> onUsingWingSkillNumberChange;

    public bool HasSkillRank(Rank rank)
    {
        return (int)this.rank >= (int)rank;
    }

    public void InitData(WingTypeEnum type, Rank rank, TypePlaneShot typeShot, PlayerInit playerInit)
    {
        wingType = type;
        this.rank = rank;
        this.typeShotBulletHell = typeShot;
        playerInitScript = playerInit;

        //animWing.runtimeAnimatorController = listAnimatorController[((int)rank) - 1] as RuntimeAnimatorController;

        fxAnimWingUse = Instantiate(listFxAnimWing[((int)rank) - 1], transform);
        fxAnimWingUse.transform.localPosition = Vector3.zero;
        //fxAnimWingUse.GetComponent<ParticleSystem>().Play(true);
        //ChangeUsingSkillNumber(10);
    }


    public void AddPowerLevel(int amount)
    {
        if (amount < 0) return;
        mainWeaponPowerLevel += amount;
        LoadMainWeaponPowerData();
        mainWeaponPower = (int)(mainWeaponPower * mainWeaponPowerPercent);
        //GetSpriteMainWeaponUse();
        Debug.Log("<color=#549fc4>" +
            "Wing: PowerLevel: " + mainWeaponPowerLevel + "----Wing: mainWeaponPower:" + mainWeaponPower
            + "</color>");
    }

    //--- tăng phần trăm bonus cho đạn
    public void ChangePowerPercent(float amount)
    {
        mainWeaponPowerPercent += amount;
        LoadMainWeaponPowerData();
        mainWeaponPower = (int)(mainWeaponPower * mainWeaponPowerPercent);
        //Debug.LogError("mainWeaponPowerPercent: " + mainWeaponPowerPercent + "----mainWeaponPower:" + mainWeaponPower);
        Debug.Log("<color=#549fc4>" +
        "Wing: mainWeaponPowerPercent: " + mainWeaponPowerPercent + "----Wing: mainWeaponPower:" + mainWeaponPower
        + "</color>");
    }



    //--- số lần sử dụng skill
    public void ChangeUsingSkillNumber(int amount)
    {
        usingSkillNumber += amount;

        if (onUsingWingSkillNumberChange != null) onUsingWingSkillNumberChange(usingSkillNumber);

        Debug.Log("<color=#549fc4>" +
        "Wing: usingSkillNumber:" + usingSkillNumber
        + "</color>");
    }

    public void ChangeUsingSkillTime(float amount)
    {
        usingSkillTime += amount;
        //Debug.LogError("usingSkillTime:" + usingSkillTime);
        Debug.Log("<color=#549fc4>" +
        "Wing: usingSkillTime:" + usingSkillTime
        + "</color>");
    }

    public void ChangeUsingSkillCountdownTime(float amount)
    {
        usingSkillCountdownTime += amount;
        //Debug.LogError("usingSkillCountdownTime:" + usingSkillCountdownTime);
        Debug.Log("<color=#549fc4>" +
        "Wing: usingSkillCountdownTime:" + usingSkillCountdownTime
        + "</color>");
    }


    protected abstract void LoadMainWeaponPowerData();
    public abstract void UseActiveSkill();

}
