﻿using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wing3OfResolution : Wing
{
    public Wing3Skill wing3SkillScript;
    //public Wing2SmallBird[] listWing2SmallBird;
    public GameObject[] listFxWingSkill;

    GameObject fxWingSkillUse;
    // Use this for initialization
    void Start()
    {
        InitDataByRank();
    }

    void InitDataByRank()
    {
        if (HasSkillRank(Rank.C))
        {
            ChangeUsingSkillNumber(WingSheet.Get((int)WingType).using_skill_number);
            ChangeUsingSkillTime(WingSheet.Get((int)WingType).using_skill_time);
            ChangeUsingSkillCountdownTime(WingSheet.Get((int)WingType).using_skill_cooldown_time);
        }
        if (HasSkillRank(Rank.B))
        {
            playerInitScript.ChangeNumberLife(1);
        }
        if (HasSkillRank(Rank.A))
        {
            ChangeUsingSkillTime(3f);
        }
        if (HasSkillRank(Rank.S))
        {
            ChangeUsingSkillNumber(1);
        }
        if (HasSkillRank(Rank.SS))
        {
            ChangePowerPercent(0.2f);
        }
        if (HasSkillRank(Rank.SSS))
        {
            playerInitScript.Aircraft.ChangeMainWeaponPowerPercent(0.15f);
        }
    }

    protected override void LoadMainWeaponPowerData()
    {
        if (mainWeaponPowerLevel >= WingOfResolution3Sheet.GetDictionary().Count)
        {
            mainWeaponPowerLevel = WingOfResolution3Sheet.GetDictionary().Count - 1;
        }
        mainWeaponPower = WingOfResolution3Sheet.Get(mainWeaponPowerLevel).mainweapon_power;
    }



    public override void UseActiveSkill()
    {
        InitActiveSkill();
    }

    void InitActiveSkill()
    {
        wing3SkillScript.gameObject.SetActive(true);
        wing3SkillScript.playerInitScript = playerInitScript;
        if (fxWingSkillUse == null)
        {
            fxWingSkillUse = Instantiate(listFxWingSkill[((int)Rank) - 1], wing3SkillScript.transform);
            fxWingSkillUse.transform.localPosition = Vector3.zero;
            //fxWingSkillUse.transform.localPosition = new Vector3(0f, 1.68f, 0f);

            if (playerInitScript.typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
            {
                wing3SkillScript.power = mainWeaponPower;
            }
            else
            {
                wing3SkillScript.power = 0;
            }
        }

        this.Delay(UsingSkillTime - 0.5f, () =>
        {
            //for (int i = 0; i < listWing2SmallBird.Length; i++)
            //{
            //    listWing2SmallBird[i].ChangeAlphaBird();
            //}
            this.Delay(1f, () =>
            {
                wing3SkillScript.gameObject.SetActive(false);
            }, true);
        }, true);
    }

}
