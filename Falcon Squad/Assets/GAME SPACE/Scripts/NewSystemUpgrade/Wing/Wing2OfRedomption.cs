﻿using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wing2OfRedomption : Wing
{
    public GameObject objWing;
    public Wing2SmallBird[] listWing2SmallBird;

    // Use this for initialization
    void Start()
    {
        InitDataByRank();
    }

    void InitDataByRank()
    {
        if (HasSkillRank(Rank.C))
        {
            ChangeUsingSkillNumber(WingSheet.Get((int)WingType).using_skill_number);
            ChangeUsingSkillTime(WingSheet.Get((int)WingType).using_skill_time);
            ChangeUsingSkillCountdownTime(WingSheet.Get((int)WingType).using_skill_cooldown_time);
        }
        if (HasSkillRank(Rank.B))
        {
            playerInitScript.ChangeNumberLife(1);
        }
        if (HasSkillRank(Rank.A))
        {
            playerInitScript.Aircraft.ChangeUsingSkillNumber(1);
        }
        if (HasSkillRank(Rank.S))
        {
            ChangeUsingSkillNumber(1);
        }
        if (HasSkillRank(Rank.SS))
        {
            ChangePowerPercent(0.2f);
        }
        if (HasSkillRank(Rank.SSS))
        {
            playerInitScript.ChangeNumberLife(1);
        }
    }

    protected override void LoadMainWeaponPowerData()
    {
        if (mainWeaponPowerLevel >= WingOfRedemption2Sheet.GetDictionary().Count)
        {
            mainWeaponPowerLevel = WingOfRedemption2Sheet.GetDictionary().Count - 1;
        }
        mainWeaponPower = WingOfRedemption2Sheet.Get(mainWeaponPowerLevel).mainweapon_power;
    }


    public override void UseActiveSkill()
    {
        ActiveListBird();
    }

    void ActiveListBird()
    {
        objWing.SetActive(true);
        if (playerInitScript.typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            for (int i = 0; i < listWing2SmallBird.Length; i++)
            {
                listWing2SmallBird[i].power = mainWeaponPower;
                listWing2SmallBird[i].playerInitScript = playerInitScript;
            }
        }
        else
        {
            for (int i = 0; i < listWing2SmallBird.Length; i++)
            {
                listWing2SmallBird[i].power = 0;
                listWing2SmallBird[i].playerInitScript = playerInitScript;
            }
        }

        this.Delay(UsingSkillTime - 0.5f, () =>
          {
              for (int i = 0; i < listWing2SmallBird.Length; i++)
              {
                  listWing2SmallBird[i].ChangeAlphaBird();
              }
              this.Delay(1f, () =>
              {
                  objWing.SetActive(false);
              }, true);
          }, true);
    }


}
