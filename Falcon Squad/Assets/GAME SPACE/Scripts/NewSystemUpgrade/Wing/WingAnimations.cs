﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingAnimations : MonoBehaviour {

    private UISpriteAnimation spriteAnim;

    private UISpriteAnimation SpriteAnim
    {
        get
        {
            if (spriteAnim == null)
            {
                spriteAnim = GetComponent<UISpriteAnimation>();
                return spriteAnim;
            }
            else
            {
                return spriteAnim;
            }
        }
    }

    private void Awake()
    {
        spriteAnim = GetComponent<UISpriteAnimation>();
    }

    public void PlayAnimations(WingTypeEnum type, Rank rank)
    {
        SpriteAnim.namePrefix = "wing" + (int)type + "_e" + (int)rank + "_idle_";
        SpriteAnim.ResetToBeginning();
    }
}
