﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wing3Skill : MonoBehaviour {

    public int power = 0;

    [HideInInspector]
    public PlayerInit playerInitScript;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (playerInitScript.typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            switch (col.tag)
            {
                case "EnemyBullet":
                    Debug.Log("Wing2SmallBird Trigger EnemyBullet");
                    UbhBullet bullet = col.transform.parent.GetComponent<UbhBullet>();
                    if (bullet != null)
                    {
                        UbhObjectPool.instance.ReleaseBullet(bullet);
                    }
                    break;
                case "BulletEnemyDestroyByWing":
                    Debug.Log("Wing2SmallBird Trigger BulletEnemyDestroyByWing");
                    UbhBullet bullet2 = col.transform.parent.GetComponent<UbhBullet>();
                    if (bullet2 != null)
                    {
                        UbhObjectPool.instance.ReleaseBullet(bullet2);
                    }
                    break;
            }
        }
    }



    //void OnEnable()
    //{

    //}

    //public void ChangeAlphaBird()
    //{
    //    tweenAlphaBird.enabled = true;
    //    tweenAlphaBird.ResetToBeginning();
    //}

    //void OnDisable()
    //{
    //    sBird.color = Color.white;
    //    tweenAlphaBird.enabled = false;
    //}
}
