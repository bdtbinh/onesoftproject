﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wing2SmallBird : MonoBehaviour
{
    public SpriteRenderer sBird;
    public TweenAlpha tweenAlphaBird;
    public int power = 0;

    [HideInInspector]
    public PlayerInit playerInitScript;
    void OnTriggerEnter2D(Collider2D col)
    {

        if (playerInitScript.typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            switch (col.tag)
            {
                case "EnemyBullet":
                    Debug.Log("Wing2SmallBird Trigger EnemyBullet");
                    UbhBullet bullet = col.transform.parent.GetComponent<UbhBullet>();
                    if (bullet != null)
                    {
                        UbhObjectPool.instance.ReleaseBullet(bullet);
                    }
                    break;
                case "BulletEnemyDestroyByWing":
                    Debug.Log("Wing2SmallBird Trigger EnemyBullet");
                    UbhBullet bullet1 = col.transform.parent.GetComponent<UbhBullet>();
                    if (bullet1 != null)
                    {
                        UbhObjectPool.instance.ReleaseBullet(bullet1);
                    }
                    break;
            }

        }
    }



    //void OnEnable()
    //{

    //}

    public void ChangeAlphaBird()
    {
        tweenAlphaBird.enabled = true;
        tweenAlphaBird.ResetToBeginning();
    }

    void OnDisable()
    {
        sBird.color = Color.white;
        tweenAlphaBird.enabled = false;
    }
}
