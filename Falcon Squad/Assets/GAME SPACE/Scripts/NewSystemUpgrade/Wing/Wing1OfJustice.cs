﻿using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wing1OfJustice : Wing
{
    public UbhShotCtrl ubhShotCtrl;
    public SpaceWarShootSetData ubhShotCtrlSetData;

    // Use this for initialization
    void Start()
    {
        InitDataByRank();

        ubhShotCtrl.transform.parent = playerInitScript.transform;
        ubhShotCtrl.transform.position = MainScene.Instance.posAnchorBottom.position;

        ubhShotCtrlSetData.playerInitScript = playerInitScript;
        ubhShotCtrlSetData.typePlaneShotBulletHell = TypePlaneShot.Wing;
        ubhShotCtrlSetData.typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.WingPower;
    }

    void InitDataByRank()
    {
        if (HasSkillRank(Rank.C))
        {
            ChangeUsingSkillNumber(WingSheet.Get((int)WingType).using_skill_number);
            ChangeUsingSkillTime(WingSheet.Get((int)WingType).using_skill_time);
            ChangeUsingSkillCountdownTime(WingSheet.Get((int)WingType).using_skill_cooldown_time);
        }
        if (HasSkillRank(Rank.B))
        {
            ChangeUsingSkillCountdownTime(-2f);
        }
        if (HasSkillRank(Rank.A))
        {
            playerInitScript.ChangeNumberLife(1);
        }
        if (HasSkillRank(Rank.S))
        {
            ChangeUsingSkillNumber(1);
        }
        if (HasSkillRank(Rank.SS))
        {
            ChangePowerPercent(0.2f);
        }
        if (HasSkillRank(Rank.SSS))
        {
            ChangeUsingSkillNumber(1);
        }
    }

    protected override void LoadMainWeaponPowerData()
    {
        if (mainWeaponPowerLevel >= WingOfJustice1Sheet.GetDictionary().Count)
        {
            mainWeaponPowerLevel = WingOfJustice1Sheet.GetDictionary().Count - 1;
        }
        mainWeaponPower = WingOfJustice1Sheet.Get(mainWeaponPowerLevel).mainweapon_power;
    }

    public override void UseActiveSkill()
    {
        ShootWing();
    }

    void ShootWing()
    {
        //fxBeforeShootActiveSkill.SetActive(true);
        //fxBeforeShootActiveSkill.GetComponent<ParticleSystem>().Play(true);
        ubhShotCtrl.StartShotRoutine();
        this.Delay(UsingSkillTime, () =>
        {
            ubhShotCtrl.StopShotRoutine();
        }, true);
    }
}
