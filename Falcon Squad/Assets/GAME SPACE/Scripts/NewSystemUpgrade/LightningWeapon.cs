﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.ThunderAndLightning;

public class LightningWeapon : MonoBehaviour
{

    [SerializeField]
    private float showTime = 0.5f;

    [SerializeField]
    private float hideTime = 3f;

    [SerializeField]
    private LightningBoltPrefabScript[] lightningBoltPrefab;

    [SerializeField]
    private LightningBoltTransformTrackerScript[] tracker;

    public GameObject topLeft;
    public GameObject bottomRight;

    private void Start()
    {
        Invoke("Shoot", 5f);
    }

    void Shoot()
    {
        coShowHide = CoShowHide();
        StartCoroutine(coShowHide);
    }

    List<GameObject> listEnemy = new List<GameObject>(3);

    void UpdateListEnemy()
    {
        listEnemy.Clear();
        Collider2D[] colliders = Physics2D.OverlapAreaAll(topLeft.transform.position, bottomRight.transform.position);

        for (int i = 0; i < colliders.Length; i++)
        {
            if (listEnemy.Count > 2)
                break;

            listEnemy.Add(colliders[i].gameObject);
        }
    }

    IEnumerator coShowHide;
    IEnumerator CoShowHide()
    {
        while (true)
        {
            //Show target
            UpdateListEnemy();

            for (int i = 0; i < listEnemy.Count; i++)
            {
                lightningBoltPrefab[i].Destination = listEnemy[i];
                tracker[i].EndTarget = listEnemy[i].transform;
                lightningBoltPrefab[i].gameObject.SetActive(true);
            }

            yield return new WaitForSecondsRealtime(showTime);


            //Hide target
            for (int i = 0; i < listEnemy.Count; i++)
            {
                lightningBoltPrefab[i].gameObject.SetActive(false);
            }

            yield return new WaitForSecondsRealtime(hideTime);
        }
    }
}
