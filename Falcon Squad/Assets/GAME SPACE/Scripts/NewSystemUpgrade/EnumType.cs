﻿
//public enum PlayerTypeEnum
//{
//    Campaign,
//    CoOp
//}

public enum EventItemType
{
    IceCream_4_2019,                     //Summer Holiday
}

public enum Rank
{
    C = 1,
    B,
    A,
    S,
    SS,
    SSS
}

public enum AircraftTypeEnum
{
    BataFD = 1,
    SkyWraith,
    FuryOfAres,
    CandyPine,                  //Không hiển thị nữa
    RadiantStar,                //Không hiển thị nữa
    MacBird,
    TwilightX,
    StarBomb,
    IceShard,
    ThunderBolt,
}

public enum WingmanTypeEnum
{
    None,
    GatlingGun = 1,
    AutoGatlingGun,
    Lazer,
    DoubleGalting,
    HomingMissile,
    Splasher
}

public enum WingTypeEnum
{
    None,
    WingOfJustice = 1,
    WingOfRedemption,
    WingOfResolution,
}

public enum ResourceType
{
    None,
    Gold,
    Gem,
    Card,
    Energy
}

public enum BuyType
{
    None,
    Gold,
    Gem,
    Dollar,
    Energy
}

//thằng cha chứa khẩu súng bắn ra bullet hell

public enum TypePlayerByModeEnum //cop-op hay Player chính 
{
    CampaignPlayer,
    PvP2v2Player
}


public enum TypePlaneShot
{
    Aircraft,
    LeftWingman,
    RightWingman,
    Wing
}

public enum TypeGunPlayerShotBulletHell
{
    AircraftMainPower,
    AircraftSecondPower,
    WingmanMainPower,
    WingmanSecondPower,
    RightWingmanSecondPower,
    ActiveSkillGunPower,
    WingPower,
    None
}

public enum ItemInfoEnumType
{
    Gold,
    Gold1,
    Gold2,
    Gold3,
    Gem,
    Life,
    ActiveSkill,
    PowerUp,
    AircraftGeneralCard,
    Aircraft1Card,
    Aircraft2Card,
    Aircraft3Card,
    Aircraft6Card,
    Aircraft7Card,
    Aircraft8Card,
    Aircraft9Card,
    DroneGeneralCard,
    Drone1Card,
    Drone2Card,
    Drone3Card,
    Drone4Card,
    Drone5Card,
    Drone6Card,
    BoxCard,
    BoxCard1,
    BoxCard2,
    BoxCard3,
    BoxCard4,
    HalloweenCandy,
    XMasBell,
    WingGeneralCard,
    Wing1Card,
    Wing2Card,
    Wing3Card,
    Energy,
    Aircraft10Card,
}

/// <summary>
///Thêm 1 trường vào thì nhớ sửa những chỗ liên quan. Find reference
/// </summary>
public static class ItemInfoStringType
{
    public const string Gold = "Gold";
    public const string Gold1 = "Gold1";
    public const string Gold2 = "Gold2";
    public const string Gold3 = "Gold3";
    public const string Gem = "Gem";
    public const string Life = "Life";
    public const string ActiveSkill = "ActiveSkill";
    public const string PowerUp = "PowerUp";
    public const string AircraftGeneralCard = "AircraftGeneralCard";
    public const string Aircraft1Card = "Aircraft1Card";
    public const string Aircraft2Card = "Aircraft2Card";
    public const string Aircraft3Card = "Aircraft3Card";
    public const string Aircraft6Card = "Aircraft6Card";
    public const string Aircraft7Card = "Aircraft7Card";
    public const string Aircraft8Card = "Aircraft8Card";
    public const string Aircraft9Card = "Aircraft9Card";
    public const string DroneGeneralCard = "DroneGeneralCard";
    public const string Drone1Card = "Drone1Card";
    public const string Drone2Card = "Drone2Card";
    public const string Drone3Card = "Drone3Card";
    public const string Drone4Card = "Drone4Card";
    public const string Drone5Card = "Drone5Card";
    public const string Drone6Card = "Drone6Card";
    public const string BoxCard = "BoxCard";
    public const string BoxCard1 = "BoxCard1";
    public const string BoxCard2 = "BoxCard2";
    public const string BoxCard3 = "BoxCard3";
    public const string BoxCard4 = "BoxCard4";
    public const string HalloweenCandy = "HalloweenCandy";
    public const string XMasBell = "XMasBell";
    public const string WingGeneralCard = "WingGeneralCard";
    public const string Wing1Card = "Wing1Card";
    public const string Wing2Card = "Wing2Card";
    public const string Wing3Card = "Wing3Card";
    public const string Energy = "Energy";
    public const string Aircraft10Card = "Aircraft10Card";
}