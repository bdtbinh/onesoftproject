﻿using UnityEngine;

public class AircraftAnimations : MonoBehaviour
{

    private UISpriteAnimation spriteAnim;

    private UISpriteAnimation SpriteAnim
    {
        get
        {
            if(spriteAnim == null)
            {
                spriteAnim = GetComponent<UISpriteAnimation>();
                return spriteAnim;
            }
            else
            {
                return spriteAnim;
            }
        }
    }

    private void Awake()
    {
        spriteAnim = GetComponent<UISpriteAnimation>();
    }

    public void PlayAnimations(AircraftTypeEnum type, Rank rank)
    {
        SpriteAnim.namePrefix = "aircraft" + (int)type + "_e" + (int)rank + "_idle";
        SpriteAnim.ResetToBeginning();
    }
}

