﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using SkyGameKit;
using SkyGameKit.QuickAccess;
using UniRx;
using PathologicalGames;
using Firebase.Analytics;
using TCore;
using com.ootii.Messages;
using Mp.Pvp;

public class PlayerController : MonoBehaviour
{
    PlayerInit playerInitIns;

    private float posMaxMoveLeft, posMaxMoveRight;
    private float posMaxMoveTop, posMaxMoveBottom;

    Camera cmr;

    private Vector3 screenPoint;
    private Vector3 offset;
    Vector3 curScreenPointOrigi;
    Vector3 curScreenPointStep1, curScreenPointStep2, posScreenChange;
    Vector3 posChangePlayerBonus;
    Vector3 newPositionPlayer;

    Transform aircraftTrans;
    //----
    Vector3 posInitStart;
    Vector3 posInitTarget;
    //
    SpriteRenderer sAircraft;
    CircleCollider2D collider2DIns;

    TweenAlpha tweenAlphaDie;

    Vector3 tempVector3;
    bool isAwake;

    private void Awake()
    {
        isAwake = true;
        if (playerInitIns == null) playerInitIns = GetComponent<PlayerInit>();
        cmr = MainScene.Instance.cameraTk2d;

        //posMaxMoveLeft = MainScene.Instance.posAnchorLeft.localPosition.x + 0.21f;
        //posMaxMoveRight = MainScene.Instance.posAnchorRight.localPosition.x - 0.21f;
        ////
        //posMaxMoveTop = MainScene.Instance.posAnchorTop.localPosition.y - 1f;
        //posMaxMoveBottom = MainScene.Instance.posAnchorBottom.localPosition.y + 0.5f;



        speedSensitivity = CacheGame.GetSpeedPlayer() * 1.38f + 1f;
    }

    void Start()
    {
        posMaxMoveLeft = MainScene.Instance.posAnchorLeft.localPosition.x + 0.21f;
        posMaxMoveRight = MainScene.Instance.posAnchorRight.localPosition.x - 0.21f;
        //
        posMaxMoveTop = MainScene.Instance.posAnchorTop.localPosition.y - 1f;
        posMaxMoveBottom = MainScene.Instance.posAnchorBottom.localPosition.y + 0.5f;

    }

    public void SetAircraftTrans()
    {
        if (playerInitIns == null) playerInitIns = GetComponent<PlayerInit>();

        aircraftTrans = playerInitIns.Aircraft.transform;

        collider2DIns = playerInitIns.Aircraft.GetComponent<CircleCollider2D>();
        if (playerInitIns.typePlayerByMode == TypePlayerByModeEnum.PvP2v2Player)
        {
            collider2DIns.enabled = false;
        }
        sAircraft = playerInitIns.Aircraft.GetComponent<SpriteRenderer>();
        tweenAlphaDie = playerInitIns.Aircraft.GetComponent<TweenAlpha>();

        if (!playerInitIns.usingAircraftBackup)
        {
            PlayerStart();
        }
        else
        {
            PlayerBackupInit();
        }
    }


    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventID.ON_CHANGE_SENSITIVITY, OnChangeSpeedSensitivity, true);
    }
    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_CHANGE_SENSITIVITY, OnChangeSpeedSensitivity, true);
    }

    private void OnChangeSpeedSensitivity(IMessage msg)
    {
        speedSensitivity = CacheGame.GetSpeedPlayer() * 1.38f + 1f;
        //Debug.LogError("OnChangeSpeedSensitivity "+ speedSensitivity);
    }

    #region di chuyyển kiểu cũ
    /*
    
    void OnMouseDown()
    {

        if (!MainScene.Instance.gameStopping && aircraftTrans != null)
        {
            if (!playerInitIns.Aircraft.aircraftIsStopMove && playerInitIns.initPlayerCompleted)
            {
                screenPoint = cmr.WorldToScreenPoint(gameObject.transform.position);
                if (Input.touchCount > 0)
                {
                    curScreenPointOrigi = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0f);
                }
                else
                {
                    curScreenPointOrigi = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
                }
                offset = aircraftTrans.position - cmr.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
                //
                curScreenPointStep1 = cmr.ScreenToWorldPoint(curScreenPointOrigi);
                curScreenPointStep2 = curScreenPointStep1;
            }
        }
    }
    //

    void OnMouseUp()
    {

        if (!MainScene.Instance.gameStopping && aircraftTrans != null)
        {
            if (!playerInitIns.Aircraft.aircraftIsStopMove && playerInitIns.initPlayerCompleted)
            {
                curScreenPointStep1 = curScreenPointStep2;
                offset = Vector3.zero;
                //aircraftTrans = transform;
            }
        }
    }

    //


    void OnMouseDrag()
    {
        if (!MainScene.Instance.gameStopping && aircraftTrans != null)
        {

            if (!playerInitIns.Aircraft.aircraftIsStopMove && playerInitIns.initPlayerCompleted)
            {

                if (Input.touchCount > 0)
                {
                    curScreenPointOrigi = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0f);
                }
                else
                {
                    curScreenPointOrigi = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
                }

                //				#if UNITY_EDITOR
                //				curScreenPointOrigi = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0f);
                //				#else
                //				curScreenPointOrigi = new Vector3 (Input.GetTouch (0).position.x, Input.GetTouch (0).position.y, 0f);
                //				#endif

                curScreenPointStep1 = cmr.ScreenToWorldPoint(curScreenPointOrigi);

                posScreenChange = curScreenPointStep2 - curScreenPointStep1;

                posChangePlayerBonus = posScreenChange * CacheGame.GetSpeedPlayer();

                newPositionPlayer = curScreenPointStep1 + offset - posChangePlayerBonus;

                aircraftTrans.position = newPositionPlayer;

                offset = aircraftTrans.position - curScreenPointStep1;

                if (aircraftTrans.position.z != 0)
                {
                    aircraftTrans.position = new Vector3(aircraftTrans.position.x, aircraftTrans.position.y, 0f);
                }

                if (aircraftTrans.position.y > posMaxMoveTop)
                {
                    aircraftTrans.position = new Vector3(aircraftTrans.position.x, posMaxMoveTop, 0f);
                }
                else if (aircraftTrans.position.y < posMaxMoveBottom)
                {
                    aircraftTrans.position = new Vector3(aircraftTrans.position.x, posMaxMoveBottom, 0f);
                }

                if (aircraftTrans.position.x > posMaxMoveRight)
                {
                    aircraftTrans.position = new Vector3(posMaxMoveRight, aircraftTrans.position.y, 0f);
                }
                else if (aircraftTrans.position.x < posMaxMoveLeft)
                {
                    aircraftTrans.position = new Vector3(posMaxMoveLeft, aircraftTrans.position.y, 0f);
                }
                curScreenPointStep2 = curScreenPointStep1;
            }
        }
    }
    **/
    #endregion
    float speedSensitivity = 1f;
    public float playerSpeed = 8f;
    protected Vector3 lastMousePos;
    public float relativeScale = 1f;
    public bool lerpMove = true;

    private float step;
    private int currentTouchId = -1;
    private Touch currentTouch;
    private bool currentTouchChanged = false;

    private Vector3 mousePos;

    protected virtual void Update()
    {
        if (playerInitIns.typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            PlayerCampaignMove();
        }
        else
        {
            if (flagPlayerCoopMove && isAwake)
            {
                flagPlayerCoopMove = false;
                if (aircraftTrans != null)
                {
                    tempVector3.x = Mathf.Clamp(x, posMaxMoveLeft, posMaxMoveRight);
                    tempVector3.y = Mathf.Clamp(y, posMaxMoveBottom, posMaxMoveTop);
                    aircraftTrans.position = tempVector3;
                }
            }
            //PlayerCoOpMove(AllPlayerManager.Instance.playerCampaign.Aircraft.transform.position - Vector3.one - Vector3.one);
        }

    }

    void PlayerCampaignMove()
    {
        if (MainScene.Instance.gameStopping || aircraftTrans == null || playerInitIns.Aircraft.aircraftIsStopMove)
        {
            currentTouchId = -1;
            if (Input.GetMouseButton(0))
            {
                mousePos = cmr.ScreenToWorldPoint(Input.mousePosition);//Tính lại trực tiếp
                mousePos.z = 0;
                lastMousePos = mousePos;
            }
            return;
        }

        if (Input.GetMouseButton(0) || Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
        {
            if (Input.touchCount > 0)
            {
                if (currentTouchId < 0)
                {
                    //Không có ngón nào được chọn, lấy ngón đầu tiên trong danh sách
                    currentTouchId = Input.touches[0].fingerId;
                    currentTouchChanged = true;
                }
                currentTouch = GetTouch(currentTouchId);
                if (currentTouch.phase == TouchPhase.Ended || currentTouch.phase == TouchPhase.Canceled)
                {
                    //Người chơi thả ngón tay đang bấm ra khỏi màn hình
                    currentTouchId = -1;
                }
                mousePos = cmr.ScreenToWorldPoint(currentTouch.position);
            }
            else
            {
                mousePos = cmr.ScreenToWorldPoint(Input.mousePosition);
            }
            mousePos.z = 0;
            step = playerSpeed * Time.unscaledDeltaTime;

            RelativeMove();
        }
    }

    bool flagPlayerCoopMove;
    float x;
    float y;

    public void PlayerCoOpMove(float x, float y)
    {
        flagPlayerCoopMove = true;
        this.x = x;
        this.y = y;
    }


    public static Touch GetTouch(int fingerId)
    {
        foreach (var touch in Input.touches)
        {
            if (touch.fingerId == fingerId)
            {
                return touch;
            }
        }
        return new Touch() { fingerId = -1 };
    }
    protected virtual void RelativeMove()
    {
        //Xác định điểm đầu tiên của di chuyển là lastMousePos
        if (Input.touchCount == 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                lastMousePos = mousePos;
                return;
            }
        }
        else
        {
            if (currentTouchChanged)
            {
                lastMousePos = mousePos;
                currentTouchChanged = false;
                return;
            }
        }
        Vector3 offsetMouse = mousePos - lastMousePos;
        Vector3 trans;

        //float speedSensitivity = CacheGame.GetSpeedPlayer() * 2f + 1f;

        if (lerpMove)
        {
            trans = aircraftTrans.position + offsetMouse * (0.4f * step < 1 ? 0.5f * step : 1) * speedSensitivity;
        }
        else
        {
            trans = Vector3.MoveTowards(aircraftTrans.position, aircraftTrans.position + offsetMouse * speedSensitivity, step);
        }
        if (speedSensitivity > 1.001f)
        {
            lastMousePos = mousePos;
        }
        else
        {
            lastMousePos += trans - aircraftTrans.position;//Giúp việc dừng di chuyển ngón tay nhưng vẫn giữ ngón tay trên màn hình thì player vẫn di chuyển nếu chưa đến
        }
        trans.x = Mathf.Clamp(trans.x, posMaxMoveLeft, posMaxMoveRight);
        trans.y = Mathf.Clamp(trans.y, posMaxMoveBottom, posMaxMoveTop);
        aircraftTrans.position = trans;
        if (CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
        {
            ConnectManager.Instance.SendData(CachePvp.PLAYER_MOVE, trans);
        }
    }

    #region action của player
    //--action
    public void PlayerStartShoot()
    {
        playerInitIns.Aircraft.ShootMainWeapon();
        playerInitIns.Aircraft.ShootSecondWeapon();
        if (playerInitIns.LeftWingman != null)
        {
            playerInitIns.LeftWingman.ShootMainWeapon();
        }
        if (playerInitIns.RightWingman != null)
        {
            playerInitIns.RightWingman.ShootMainWeapon();
        }

    }

    public void PlayerStopShoot()
    {
        playerInitIns.Aircraft.StopShootMainWeapon();
        playerInitIns.Aircraft.StopShootSecondWeapon();
        if (playerInitIns.LeftWingman != null)
        {
            playerInitIns.LeftWingman.StopShootMainWeapon();
        }
        if (playerInitIns.RightWingman != null)
        {
            playerInitIns.RightWingman.StopShootMainWeapon();
        }
    }
    //

    void PlayerBackupInit()
    {
        playerInitIns.Aircraft.aircraftIsStopMove = false;
        playerInitIns.Aircraft.aircraftIsReloading = true;
        playerInitIns.Aircraft.shieldReloadObj.SetActive(true);
        PlayerStartShoot();
        this.Delay(2.6f, () =>
        {
            playerInitIns.Aircraft.aircraftIsReloading = false;
            playerInitIns.Aircraft.shieldReloadObj.SetActive(false);
            //
        }, true);
    }

    void PlayerStart()
    {
        //start p1, p2

        float distance = 0;
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            if (CachePvp.myIndex == 1)
            {
                distance = 1.34f;
            }
            else
            {
                distance = -1.34f;
            }
        }

        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.PVP2vs2)
        {
            distance = 0;
        }
        if (playerInitIns.typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            posInitTarget = new Vector3(MainScene.Instance.posAnchorBottom.localPosition.x - distance, MainScene.Instance.posAnchorBottom.localPosition.y + 1.8f, 0f);
            posInitStart = new Vector3(MainScene.Instance.posAnchorBottom.localPosition.x - distance, MainScene.Instance.posAnchorBottom.localPosition.y - 1.8f, 0f);
        }
        else if (playerInitIns.typePlayerByMode == TypePlayerByModeEnum.PvP2v2Player)
        {
            posInitTarget = new Vector3(MainScene.Instance.posAnchorBottom.localPosition.x + distance, MainScene.Instance.posAnchorBottom.localPosition.y + 1.8f, 0f);
            posInitStart = new Vector3(MainScene.Instance.posAnchorBottom.localPosition.x + distance, MainScene.Instance.posAnchorBottom.localPosition.y - 1.8f, 0f);
        }

        aircraftTrans.localPosition = posInitStart;
        playerInitIns.Aircraft.aircraftIsStopMove = true;
        playerInitIns.Aircraft.aircraftIsReloading = true;

        this.Delay(0.5f, () =>
        {
            SoundManager.SoundPlayerShow();
        });


        aircraftTrans.DOMove(posInitTarget, 1.38f).OnComplete(() =>
        {
            this.Delay(0.5f, () =>
            {
                if (gameObject.activeInHierarchy && ChangeBackground.Instance != null)
                {
                    ChangeBackground.Instance.SetSpeedMoveBackGround(ChangeBackground.Instance.speedBgReady, ChangeBackground.Instance.speedDecorReady);
                }
            });

            this.Delay(1.5f, () =>
            {
                if (gameObject.activeInHierarchy)
                {
                    PlayerStartShoot();
                    playerInitIns.Aircraft.aircraftIsReloading = false;
                    playerInitIns.Aircraft.aircraftIsStopMove = false;
                    if (GameContext.modeGamePlay != GameContext.ModeGamePlay.pvp && GameContext.modeGamePlay != GameContext.ModeGamePlay.Tournament && GameContext.modeGamePlay != GameContext.ModeGamePlay.PVP2vs2)
                    {
                        Pool_EffectStartLevel();
                    }
                }
            });
            if (GameContext.modeGamePlay != GameContext.ModeGamePlay.pvp && GameContext.modeGamePlay != GameContext.ModeGamePlay.Tournament && GameContext.modeGamePlay != GameContext.ModeGamePlay.PVP2vs2)
            {
                this.Delay(3f, () =>
                {
                    if (gameObject.activeInHierarchy)
                    {
                        LevelManager.Instance.NextWave();
                    }
                });
            }
            else
            {
                if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
                {
                    if (playerInitIns.typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
                    {
                        new CSCoopPvPPlayerReady().Send();
                    }
                }
                else
                {
                    new CSPlayerReady().Send();
                }
            }
        });


    }


    //-----------------------------PlayerStart------------------------------

    public void PlayerWin()
    {
        this.Delay(1f, () =>
        {
            PlayerStopShoot();
        }, true);

        this.Delay(1.6f, () =>
        {
            Pool_EffectVictory();
            SoundManager.PlayGameWinText();
        }, true);

        this.Delay(3.6f, () =>
        {
            playerInitIns.Aircraft.aircraftIsStopMove = true;
            playerInitIns.Aircraft.aircraftIsReloading = true;
            PlayerStopShoot();

            Vector3 targetPlayerWin = new Vector3(transform.localPosition.x, transform.localPosition.y + 16.8f, 0f);
            transform.DOMove(targetPlayerWin, 1.6f).OnComplete(() =>
            {

            }).SetUpdate(true);
        }, true);
    }


    //-----------------------------PlayerDie------------------------------
    public void PlayerDie(bool fromServer = false)
    {
        if (!fromServer)
        {
            if (MainScene.Instance.useRevival)
            {
                Time.timeScale = 1f;
                SoundManager.SoundPlayerShow();
                MainScene.Instance.useRevival = false;
                playerInitIns.Aircraft.aircraftIsStopMove = false;
                if (LevelManager.Instance.GameState != GameStateType.Playing)
                {
                    LevelManager.Instance.OnGameStateChange(GameStateType.Playing);
                }
            }
            else
            {
                SoundManager.SoundPlayerDie();
                //
                WaveManager waveManagerIns = LevelManager.Instance.GetCurrentWave() as WaveManager;

                FirebaseLogSpaceWar.LogUserDead(waveManagerIns.name);
                playerInitIns.ChangeNumberLife(-1);
            }
        }
        if (playerInitIns.Life > 0)
        {
            Pool_EffectPlayerDieOneLife();
            //
            tweenAlphaDie.enabled = true;
            tweenAlphaDie.ResetToBeginning();
            //
            collider2DIns.enabled = true;
            sAircraft.enabled = true;
            if (fromServer)
            {
                collider2DIns.enabled = false;
            }
            //fx_Duoi.SetActive(true);
            //
            PlayerStartShoot();
            //
            playerInitIns.Aircraft.aircraftIsReloading = true;
            MainScene.Instance.completeWithoutDying = false;
            //
            if (!fromServer)
            {
                ShakeCamera.Instance.DoShake();
            }
            if (MainScene.Instance.activeSkillIsPlaying)
            {

            }
            this.Delay(0.5f, () =>
            {
                playerInitIns.Aircraft.shieldReloadObj.SetActive(true);
            }, true);

            this.Delay(2.6f, () =>
            {
                playerInitIns.Aircraft.aircraftIsReloading = false;
                //
                tweenAlphaDie.enabled = false;
                sAircraft.color = Color.white;
                //
                playerInitIns.Aircraft.shieldReloadObj.SetActive(false);
                //
            }, true);
        }
        else
        {
            if (LevelManager.Instance.GameState == GameStateType.Playing)
            {
                LevelManager.Instance.OnGameStateChange(GameStateType.GameOver);
            }
            PlayerStopShoot();
            collider2DIns.enabled = false;
            sAircraft.enabled = false;
            playerInitIns.Aircraft.aircraftIsReloading = true;
            playerInitIns.Aircraft.aircraftIsStopMove = true;
            if (GameContext.modeGamePlay != GameContext.ModeGamePlay.PVP2vs2)
            {
                Time.timeScale = 0.5f;
                Debug.Log("<color=#ff33cc>" + "Change timeScale = " + Time.timeScale + "</color>");
            }
            else
            {
                playerInitIns.Aircraft.StopTimeScaleSkill();
            }
            if (!fromServer)
            {
                ShakeCamera.Instance.DoShake();
            }
            Pool_EffectPlayerDieAllLife();
        }

        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2 && !playerInitIns.isTestingMode && !fromServer)
        {
            InGamePvP.Instance.PlayerDie(CachePvp.Code, playerInitIns.Life);
            ConnectManager.Instance.SendData(CachePvp.PLAYER_HP, tempVector3, playerInitIns.Life);
        }
    }
    #endregion


    public void PlayerCoopDie(int life)
    {
        if (life > 0)
        {
            Pool_EffectPlayerDieOneLife();
        }
        else
        {
            PlayerStopShoot();
            sAircraft.enabled = false;
            Pool_EffectPlayerDieAllLife();
            this.Delay(1f, () =>
            {
                gameObject.SetActive(false);
            }, true);
        }
    }


    #region fx cho player
    public void Pool_EffectPlayerDieOneLife()
    {
        if (EffectList.Instance.playerDie != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerDie, aircraftTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    public void Pool_EffectPlayerDieAllLife()
    {
        if (EffectList.Instance.playerDieAllLife != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerDieAllLife, aircraftTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }

    //

    public void Pool_EffectStartLevel()
    {
        if (EffectList.Instance.fxStartLevel != null)
        {
            //Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.fxStartLevel, posFxVic, Quaternion.identity);
            //effectIns.GetComponent<ParticleSystem>().Play(true);
            EffectList.Instance.fxStartLevel.SetActive(true);
            EffectList.Instance.fxStartLevel.GetComponent<ParticleSystem>().Play(true);
            this.Delay(1.5f, () =>
            {
                EffectList.Instance.fxStartLevel.SetActive(false);
            }, true);
        }
    }
    //
    Vector3 posFxVic = new Vector3(0, 2f, 0);

    public void Pool_EffectVictory()
    {
        if (EffectList.Instance.fxVictory != null)
        {
            //Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.fxVictory, posFxVic, Quaternion.identity);
            //effectIns.GetComponent<ParticleSystem>().Play(true);
            EffectList.Instance.fxVictory.SetActive(true);
            EffectList.Instance.fxVictory.GetComponent<ParticleSystem>().Play(true);
            this.Delay(1.5f, () =>
            {
                EffectList.Instance.fxVictory.SetActive(false);
            }, true);
        }
    }
    #endregion
}
