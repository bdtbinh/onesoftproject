﻿using DG.Tweening;
using Sirenix.OdinInspector;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aircraft3FuryOfAres : Aircraft
{
    [Title("Data riêng", bold: true, horizontalLine: true)]
    public float valueEmpSlow;

    public UbhShotCtrl chainsawUbhShotCtrl;
    public SpaceWarShootSetData[] listGunSkillActiveUbhSetData;

    public GameObject fxBeforeShootChainsaw;

    private void Start()
    {
        base.Start();
        InitDataByRank();

    }
    void InitDataByRank()
    {

        if (HasSkillRank(Rank.C))
        {
            //ChangeUsingSkillNumber(5);

            ChangeUsingSkillNumber(AircraftSheet.Get((int)AircraftType).using_skill_number);
            ChangeUsingSkillTime(AircraftSheet.Get((int)AircraftType).using_skill_time);
            ChangeUsingSkillCountdownTime(AircraftSheet.Get((int)AircraftType).using_skill_cooldown_time);

            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_c;
        }
        if (HasSkillRank(Rank.B))
        {
            ChangeUsingSkillTime(1f);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_b;
        }
        if (HasSkillRank(Rank.A))
        {
            playerInitScript.ChangeNumberLife(1);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_a;
        }
        if (HasSkillRank(Rank.S))
        {
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_s;
        }
        if (HasSkillRank(Rank.SS))
        {
            ChangeMainWeaponPowerPercent(0.2f);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_ss;
        }
        if (HasSkillRank(Rank.SSS))
        {
            ChangeUsingSkillNumber(1);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_sss;
        }

        for (int i = 0; i < listGunSkillActiveUbhSetData.Length; i++)
        {
            listGunSkillActiveUbhSetData[i].playerInitScript = playerInitScript;
            listGunSkillActiveUbhSetData[i].typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.ActiveSkillGunPower;
        }

    }

    public override void StopTimeScaleSkill()
    {
    }

    protected override void LoadMainWeaponPowerData()
    {
        if (mainWeaponPowerLevel >= AirC3FuryOfAresSheet.GetDictionary().Count)
        {
            mainWeaponPowerLevel = AirC3FuryOfAresSheet.GetDictionary().Count - 1;
        }
        mainWeaponPower = AirC3FuryOfAresSheet.Get(mainWeaponPowerLevel).mainweapon_power;
    }
    protected override void LoadMainWeaponQuantityData()
    {
        if (mainWeaponQuantityLevel >= AirC3FuryOfAresSheet.GetDictionary().Count)
        {
            mainWeaponQuantityLevel = AirC3FuryOfAresSheet.GetDictionary().Count - 1;
        }
        mainWeaponQuantity = AirC3FuryOfAresSheet.Get(mainWeaponQuantityLevel).mainweapon_quantity;
    }
    protected override void LoadMainWeaponSpeedData()
    {
        if (mainWeaponSpeedLevel >= AirC3FuryOfAresSheet.GetDictionary().Count)
        {
            mainWeaponSpeedLevel = AirC3FuryOfAresSheet.GetDictionary().Count - 1;
        }
        mainWeaponSpeed = AirC3FuryOfAresSheet.Get(mainWeaponSpeedLevel).mainweapon_speed;
    }
    //
    protected override void LoadSecondWeaponPowerData()
    {
        if (secondWeaponPowerLevel >= AirC3FuryOfAresSheet.GetDictionary().Count)
        {
            secondWeaponPowerLevel = AirC3FuryOfAresSheet.GetDictionary().Count - 1;
        }
        secondWeaponPower = AirC3FuryOfAresSheet.Get(secondWeaponPowerLevel).secondweapon_power;
    }

    protected override void LoadSecondWeaponSpeedData()
    {
        if (secondWeaponSpeedLevel >= AirC3FuryOfAresSheet.GetDictionary().Count)
        {
            secondWeaponSpeedLevel = AirC3FuryOfAresSheet.GetDictionary().Count - 1;
        }
        secondWeaponSpeed = AirC3FuryOfAresSheet.Get(secondWeaponSpeedLevel).secondweapon_speed;
    }

    public override void UseActiveSkill()
    {
        UseSkillEMP();
        ShootChainsaw();
    }

    //

    public override void ShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StartShotRoutine();
        //Debug.LogError("ShootMainWeapon");
    }
    public override void ShootSecondWeapon()
    {
        if (HasSkillRank(Rank.S))
        {
            secondWeaponUbhShotCtrl.StartShotRoutine();
            //Debug.LogError("ShootSecondWeapon");
        }

    }
    public override void ShootUltimateWeapon()
    {

    }

    public override void StopShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StopShotRoutine();
        //Debug.LogError("StopShootMainWeapon");
    }
    public override void StopShootSecondWeapon()
    {
        if (HasSkillRank(Rank.S))
        {
            secondWeaponUbhShotCtrl.StopShotRoutine();
            //Debug.LogError("StopShootSecondWeapon");
        }
    }
    public override void StopShootUltimateWeapon()
    {
    }


    //----------EMP

    void UseSkillEMP()
    {
        //if (playerInitScript.typePlayerByMode == TypePlayerByModeEnum.CoopPlayer) return;

        Time.timeScale = valueEmpSlow;
        MainScene.Instance.clockPlayer.localTimeScale = 1f / valueEmpSlow;

        this.Delay(UsingSkillTime, () =>
        {
            if (!MainScene.Instance.gameFinished || (MainScene.Instance.gameFinished && playerInitScript.typePlayerByMode == TypePlayerByModeEnum.PvP2v2Player))
            {
                Time.timeScale = 0.5f;
            }
            MainScene.Instance.clockPlayer.localTimeScale = 1f / 0.5f;
            this.Delay(0.5f, () =>
            {
                if (!MainScene.Instance.gameFinished || (MainScene.Instance.gameFinished && playerInitScript.typePlayerByMode == TypePlayerByModeEnum.PvP2v2Player))
                {
                    Time.timeScale = 1f;
                }
                MainScene.Instance.clockPlayer.localTimeScale = 1f;
            }, true);
        }, true);
    }


    void ShootChainsaw()
    {
        fxBeforeShootChainsaw.SetActive(true);
        fxBeforeShootChainsaw.GetComponent<ParticleSystem>().Play(true);

        this.Delay(0.6f, () =>
        {
            fxBeforeShootChainsaw.SetActive(false);
            chainsawUbhShotCtrl.StartShotRoutine();
        }, true);

    }

}
