﻿using DG.Tweening;
using Sirenix.OdinInspector;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aircraft10ThunderBolt : Aircraft
{
    [Title("Data riêng", bold: true, horizontalLine: true)]

    //public UbhShotCtrl activeSkillUbhShotCtrl;
    //public SpaceWarShootSetData[] listGunSkillActiveUbhSetData;

    public GameObject fxBeforeShootActiveSkill;
    float timeHideFxShootActiveSkill = 1f;
    public GameObject shieldActiveSkill;

    [SerializeField]
    private MinhLightningBolt minhLightningBolt;

    private void Start()
    {
        base.Start();
        InitDataByRank();

    }
    void InitDataByRank()
    {

        if (HasSkillRank(Rank.C))
        {
            //ChangeUsingSkillNumber(50);
            ChangeUsingSkillNumber(AircraftSheet.Get((int)AircraftType).using_skill_number);
            ChangeUsingSkillTime(AircraftSheet.Get((int)AircraftType).using_skill_time);
            ChangeUsingSkillCountdownTime(AircraftSheet.Get((int)AircraftType).using_skill_cooldown_time);
            //ChangeUsingSkillCountdownTime(1);

            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_c;
        }
        if (HasSkillRank(Rank.B))
        {
            playerInitScript.ChangeNumberLife(1);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_b;
        }
        if (HasSkillRank(Rank.A))
        {
            ChangeMainWeaponPowerPercent(0.2f);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_a;
        }
        if (HasSkillRank(Rank.S))
        {
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_s;
        }
        if (HasSkillRank(Rank.SS))
        {
            ChangeSecondWeaponPowerPercent(0.2f);
            Debug.LogError("ChangeSecondWeaponPowerPercent");
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_ss;
        }
        if (HasSkillRank(Rank.SSS))
        {
            ChangeUsingSkillNumber(1);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_sss;

        }
        //for (int i = 0; i < listGunSkillActiveUbhSetData.Length; i++)
        //{
        //    listGunSkillActiveUbhSetData[i].playerInitScript = playerInitScript;
        //    listGunSkillActiveUbhSetData[i].typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.ActiveSkillGunPower;
        //}
    }

    protected override void LoadMainWeaponPowerData()
    {
        if (mainWeaponPowerLevel >= AirC10ThunderBoltSheet.GetDictionary().Count)
        {
            mainWeaponPowerLevel = AirC10ThunderBoltSheet.GetDictionary().Count - 1;
        }
        mainWeaponPower = AirC10ThunderBoltSheet.Get(mainWeaponPowerLevel).mainweapon_power;
    }
    protected override void LoadMainWeaponQuantityData()
    {
        if (mainWeaponQuantityLevel >= AirC10ThunderBoltSheet.GetDictionary().Count)
        {
            mainWeaponQuantityLevel = AirC10ThunderBoltSheet.GetDictionary().Count - 1;
        }
        mainWeaponQuantity = AirC10ThunderBoltSheet.Get(mainWeaponQuantityLevel).mainweapon_quantity;
    }
    protected override void LoadMainWeaponSpeedData()
    {
        if (mainWeaponSpeedLevel >= AirC10ThunderBoltSheet.GetDictionary().Count)
        {
            mainWeaponSpeedLevel = AirC10ThunderBoltSheet.GetDictionary().Count - 1;
        }
        mainWeaponSpeed = AirC10ThunderBoltSheet.Get(mainWeaponSpeedLevel).mainweapon_speed;
    }
    //
    protected override void LoadSecondWeaponPowerData()
    {
        if (secondWeaponPowerLevel >= AirC10ThunderBoltSheet.GetDictionary().Count)
        {
            secondWeaponPowerLevel = AirC10ThunderBoltSheet.GetDictionary().Count - 1;
        }
        secondWeaponPower = AirC10ThunderBoltSheet.Get(secondWeaponPowerLevel).secondweapon_power;
    }

    protected override void LoadSecondWeaponSpeedData()
    {
        if (secondWeaponSpeedLevel >= AirC10ThunderBoltSheet.GetDictionary().Count)
        {
            secondWeaponSpeedLevel = AirC10ThunderBoltSheet.GetDictionary().Count - 1;
        }
        secondWeaponSpeed = AirC10ThunderBoltSheet.Get(secondWeaponSpeedLevel).secondweapon_speed;
    }

    public override void UseActiveSkill()
    {
        UseSkillShield();
        ShootActiveSkill();
    }

    public override void StopTimeScaleSkill()
    {
        if (!MainScene.Instance.gameFinished)
        {
            Time.timeScale = 1f;
        }
        MainScene.Instance.clockPlayer.localTimeScale = 1f;
    }

    public override void ShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StartShotRoutine();
        //Debug.LogError("ShootMainWeapon");
    }
    public override void ShootSecondWeapon()
    {
        if (HasSkillRank(Rank.S))
        {
            secondWeaponUbhShotCtrl.StartShotRoutine();
            //Debug.LogError("ShootSecondWeapon");
        }

    }
    public override void ShootUltimateWeapon()
    {

    }

    public override void StopShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StopShotRoutine();
        //Debug.LogError("StopShootMainWeapon");
    }
    public override void StopShootSecondWeapon()
    {
        if (HasSkillRank(Rank.S))
        {
            secondWeaponUbhShotCtrl.StopShotRoutine();
            //Debug.LogError("StopShootSecondWeapon");
        }
    }
    public override void StopShootUltimateWeapon()
    {
    }


    //----------Shield
    void UseSkillShield()
    {
        aircraftUsingActiveShield = true;
        shieldActiveSkill.SetActive(true);

        this.Delay(UsingSkillTime, () =>
        {
            aircraftUsingActiveShield = false;
            shieldActiveSkill.SetActive(false);
        }, true);
    }



    void ShootActiveSkill()
    {
        fxBeforeShootActiveSkill.SetActive(true);
        fxBeforeShootActiveSkill.GetComponent<ParticleSystem>().Play(true);

        if (playerInitScript.typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            minhLightningBolt.ActiveLightning(powerGunActiveSkill, UsingSkillTime);
        }
        else
        {
            minhLightningBolt.ActiveLightning(0, UsingSkillTime);
        }

        this.Delay(timeHideFxShootActiveSkill, () =>
        {
            fxBeforeShootActiveSkill.SetActive(false);
        }, true);
    }

}
