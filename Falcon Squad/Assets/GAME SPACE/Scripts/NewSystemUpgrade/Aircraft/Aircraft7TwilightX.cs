﻿using DG.Tweening;
using Sirenix.OdinInspector;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aircraft7TwilightX : Aircraft
{
    [Title("Data riêng", bold: true, horizontalLine: true)]

    public GameObject shieldActiveSkill;

    public MinhLaser laserHitScript;
    public GameObject fxBeforeShootActiveSkill;
    float timeHideFxShootActiveSkill = 1f;

    private void Start()
    {
        base.Start();
        InitDataByRank();
        if (laserHitScript != null)
        {
            laserHitScript.OnLaserDamageTarget += LaserOnOnLaserHitTriggered;
        }

    }
    void InitDataByRank()
    {

        if (HasSkillRank(Rank.C))
        {
            //ChangeUsingSkillNumber(5);

            ChangeUsingSkillNumber(AircraftSheet.Get((int)AircraftType).using_skill_number);
            ChangeUsingSkillTime(AircraftSheet.Get((int)AircraftType).using_skill_time);
            ChangeUsingSkillCountdownTime(AircraftSheet.Get((int)AircraftType).using_skill_cooldown_time);

            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_c;
        }
        if (HasSkillRank(Rank.B))
        {
            ChangeUsingSkillTime(1f);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_b;
        }
        if (HasSkillRank(Rank.A))
        {
            playerInitScript.ChangeNumberLife(1);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_a;
        }
        if (HasSkillRank(Rank.S))
        {
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_s;
        }
        if (HasSkillRank(Rank.SS))
        {
            ChangeMainWeaponPowerPercent(0.30f);
            //ChangeSecondWeaponSpeedPercent(0.15f);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_ss;
        }
        if (HasSkillRank(Rank.SSS))
        {
            ChangeUsingSkillNumber(1);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_sss;
        }

    }

    protected override void LoadMainWeaponPowerData()
    {
        if (mainWeaponPowerLevel >= AirC7TwilightXSheet.GetDictionary().Count)
        {
            mainWeaponPowerLevel = AirC7TwilightXSheet.GetDictionary().Count - 1;
        }
        mainWeaponPower = AirC7TwilightXSheet.Get(mainWeaponPowerLevel).mainweapon_power;
    }
    protected override void LoadMainWeaponQuantityData()
    {
        if (mainWeaponQuantityLevel >= AirC7TwilightXSheet.GetDictionary().Count)
        {
            mainWeaponQuantityLevel = AirC7TwilightXSheet.GetDictionary().Count - 1;
        }
        mainWeaponQuantity = AirC7TwilightXSheet.Get(mainWeaponQuantityLevel).mainweapon_quantity;
    }
    protected override void LoadMainWeaponSpeedData()
    {
        if (mainWeaponSpeedLevel >= AirC7TwilightXSheet.GetDictionary().Count)
        {
            mainWeaponSpeedLevel = AirC7TwilightXSheet.GetDictionary().Count - 1;
        }
        mainWeaponSpeed = AirC7TwilightXSheet.Get(mainWeaponSpeedLevel).mainweapon_speed;
    }
    //
    protected override void LoadSecondWeaponPowerData()
    {
        if (secondWeaponPowerLevel >= AirC7TwilightXSheet.GetDictionary().Count)
        {
            secondWeaponPowerLevel = AirC7TwilightXSheet.GetDictionary().Count - 1;
        }
        secondWeaponPower = AirC7TwilightXSheet.Get(secondWeaponPowerLevel).secondweapon_power;
    }

    protected override void LoadSecondWeaponSpeedData()
    {
        if (secondWeaponSpeedLevel >= AirC7TwilightXSheet.GetDictionary().Count)
        {
            secondWeaponSpeedLevel = AirC7TwilightXSheet.GetDictionary().Count - 1;
        }
        secondWeaponSpeed = AirC7TwilightXSheet.Get(secondWeaponSpeedLevel).secondweapon_speed;
    }

    public override void UseActiveSkill()
    {
        UseSkillShield();
        ShootLaser();
    }

    public override void StopTimeScaleSkill()
    {
    }

    public override void ShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StartShotRoutine();
        //Debug.LogError("ShootMainWeapon");
    }
    public override void ShootSecondWeapon()
    {
        if (HasSkillRank(Rank.S))
        {
            secondWeaponUbhShotCtrl.StartShotRoutine();
            //Debug.LogError("ShootSecondWeapon");
        }

    }
    public override void ShootUltimateWeapon()
    {

    }

    public override void StopShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StopShotRoutine();
        //Debug.LogError("StopShootMainWeapon");
    }
    public override void StopShootSecondWeapon()
    {
        if (HasSkillRank(Rank.S))
        {
            secondWeaponUbhShotCtrl.StopShotRoutine();
            //Debug.LogError("StopShootSecondWeapon");
        }
    }
    public override void StopShootUltimateWeapon()
    {
    }


    //----------EMP

    void UseSkillShield()
    {
        aircraftUsingActiveShield = true;
        shieldActiveSkill.SetActive(true);

        this.Delay(UsingSkillTime, () =>
        {
            aircraftUsingActiveShield = false;
            shieldActiveSkill.SetActive(false);
        }, true);
    }


    void ShootLaser()
    {
        fxBeforeShootActiveSkill.SetActive(true);
        fxBeforeShootActiveSkill.GetComponent<ParticleSystem>().Play(true);

        laserHitScript.gameObject.SetActive(true);

        this.Delay(0.8f, () =>
        {
            fxBeforeShootActiveSkill.SetActive(false);
            this.Delay(UsingSkillTime, () =>
            {
                laserHitScript.gameObject.SetActive(false);
            }, true);
        }, true);
    }



    private void LaserOnOnLaserHitTriggered(RaycastHit2D hitInfo)
    {

        if (LayerMask.LayerToName(hitInfo.collider.gameObject.layer) == "Enemy" && playerInitScript.typePlayerByMode == TypePlayerByModeEnum.CampaignPlayer)
        {
            if (hitInfo.collider.GetComponent<EnemyCollisionManager>() != null)
            {
                if (hitInfo.collider.GetComponent<EnemyCollisionManager>().enemyCollisionBase != null)
                {
                    Debug.Log("<color=#00fff9>" +
                     "LaserOnOnLaserHitTriggered:" + powerGunActiveSkill
                     + "</color>");
                    hitInfo.collider.GetComponent<EnemyCollisionManager>().enemyCollisionBase.LaserOnOnLaserHitTriggeredEnemy(powerGunActiveSkill);
                }
                else
                {
                    Debug.LogError("Aircaft7 lazer khong tru mau " + hitInfo.collider.gameObject.name);
                }
            }
            else
            {
                Debug.LogError("Aircaft7 lazer khong tru mau " + hitInfo.collider.gameObject.name);
            }
        }
    }

}
