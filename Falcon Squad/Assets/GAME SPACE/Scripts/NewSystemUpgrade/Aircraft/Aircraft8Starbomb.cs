﻿using DG.Tweening;
using Sirenix.OdinInspector;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aircraft8Starbomb : Aircraft
{
    [Title("Data riêng", bold: true, horizontalLine: true)]

    public GameObject shieldActiveSkill;
    public UbhShotCtrl fireBallUbhShotCtrl;
    public SpaceWarShootSetData[] listGunSkillActiveUbhSetData;

    public GameObject fxBeforeShootActiveSkill;
    float timeHideFxShootActiveSkill = 1f;

    private void Start()
    {
        base.Start();
        InitDataByRank();

    }
    void InitDataByRank()
    {
        if (HasSkillRank(Rank.C))
        {
            //ChangeUsingSkillNumber(5);

            ChangeUsingSkillNumber(AircraftSheet.Get((int)AircraftType).using_skill_number);
            ChangeUsingSkillTime(AircraftSheet.Get((int)AircraftType).using_skill_time);
            Debug.LogError("Aircraft using_skill_number: " + AircraftSheet.Get((int)AircraftType).using_skill_number);
            Debug.LogError("Aircraft usingSkillNumber: " + usingSkillNumber);

            ChangeUsingSkillCountdownTime(AircraftSheet.Get((int)AircraftType).using_skill_cooldown_time);

            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_c;
        }
        if (HasSkillRank(Rank.B))
        {
            playerInitScript.ChangeNumberLife(1);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_b;
        }
        if (HasSkillRank(Rank.A))
        {
            ChangeUsingSkillCountdownTime(-3f);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_a;
        }
        if (HasSkillRank(Rank.S))
        {
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_s;
        }
        if (HasSkillRank(Rank.SS))
        {
            ChangeMainWeaponPowerPercent(0.25f);

            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_ss;
        }
        if (HasSkillRank(Rank.SSS))
        {
            ChangeUsingSkillNumber(1);
            //Debug.LogError("Aircraft usingSkillNumber: " + usingSkillNumber);
            powerGunActiveSkill = AircraftSheet.Get((int)AircraftType).activevalue_rank_sss;
        }

        for (int i = 0; i < listGunSkillActiveUbhSetData.Length; i++)
        {
            listGunSkillActiveUbhSetData[i].playerInitScript = playerInitScript;
            listGunSkillActiveUbhSetData[i].typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.ActiveSkillGunPower;
        }
    }

    protected override void LoadMainWeaponPowerData()
    {
        if (mainWeaponPowerLevel >= AirC8StarbombSheet.GetDictionary().Count)
        {
            mainWeaponPowerLevel = AirC8StarbombSheet.GetDictionary().Count - 1;
        }
        mainWeaponPower = AirC8StarbombSheet.Get(mainWeaponPowerLevel).mainweapon_power;
    }
    protected override void LoadMainWeaponQuantityData()
    {
        if (mainWeaponQuantityLevel >= AirC8StarbombSheet.GetDictionary().Count)
        {
            mainWeaponQuantityLevel = AirC8StarbombSheet.GetDictionary().Count - 1;
        }
        mainWeaponQuantity = AirC8StarbombSheet.Get(mainWeaponQuantityLevel).mainweapon_quantity;
    }
    protected override void LoadMainWeaponSpeedData()
    {
        if (mainWeaponSpeedLevel >= AirC8StarbombSheet.GetDictionary().Count)
        {
            mainWeaponSpeedLevel = AirC8StarbombSheet.GetDictionary().Count - 1;
        }
        mainWeaponSpeed = AirC8StarbombSheet.Get(mainWeaponSpeedLevel).mainweapon_speed;
    }
    //
    protected override void LoadSecondWeaponPowerData()
    {
        if (secondWeaponPowerLevel >= AirC8StarbombSheet.GetDictionary().Count)
        {
            secondWeaponPowerLevel = AirC8StarbombSheet.GetDictionary().Count - 1;
        }
        secondWeaponPower = AirC8StarbombSheet.Get(secondWeaponPowerLevel).secondweapon_power;
    }

    protected override void LoadSecondWeaponSpeedData()
    {
        if (secondWeaponSpeedLevel >= AirC8StarbombSheet.GetDictionary().Count)
        {
            secondWeaponSpeedLevel = AirC8StarbombSheet.GetDictionary().Count - 1;
        }
        secondWeaponSpeed = AirC8StarbombSheet.Get(secondWeaponSpeedLevel).secondweapon_speed;
    }

    public override void UseActiveSkill()
    {
        UseSkillShield();
        ShootFireBall();
    }

    public override void StopTimeScaleSkill()
    {
    }

    public override void ShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StartShotRoutine();
        //Debug.LogError("ShootMainWeapon");
    }
    public override void ShootSecondWeapon()
    {
        if (HasSkillRank(Rank.S))
        {
            secondWeaponUbhShotCtrl.StartShotRoutine();
            //Debug.LogError("ShootSecondWeapon");
        }

    }
    public override void ShootUltimateWeapon()
    {

    }

    public override void StopShootMainWeapon()
    {
        mainWeaponUbhShotCtrl.StopShotRoutine();
        //Debug.LogError("StopShootMainWeapon");
    }
    public override void StopShootSecondWeapon()
    {
        if (HasSkillRank(Rank.S))
        {
            secondWeaponUbhShotCtrl.StopShotRoutine();
            //Debug.LogError("StopShootSecondWeapon");
        }
    }
    public override void StopShootUltimateWeapon()
    {
    }


    //----------EMP

    void UseSkillShield()
    {
        aircraftUsingActiveShield = true;
        shieldActiveSkill.SetActive(true);

        this.Delay(UsingSkillTime, () =>
        {
            aircraftUsingActiveShield = false;
            shieldActiveSkill.SetActive(false);
        }, true);
    }


    void ShootFireBall()
    {
        fxBeforeShootActiveSkill.SetActive(true);
        fxBeforeShootActiveSkill.GetComponent<ParticleSystem>().Play(true);

        this.Delay(timeHideFxShootActiveSkill, () =>
        {
            fxBeforeShootActiveSkill.SetActive(false);
            fireBallUbhShotCtrl.StartShotRoutine();
        }, true);
    }

}
