﻿using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AllPlayerManager : MonoBehaviour
{
    protected static AllPlayerManager _instance;
    public static AllPlayerManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public PlayerInit playerCampaign;
    public PlayerInit playerCoop;

    public static PlayerInit playerCoopIns;

    public List<Material> listMaterial;

    private void Awake()
    {
        _instance = this;
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            playerCoop.gameObject.SetActive(true);
        }
        else
        {
            playerCoop.gameObject.SetActive(false);
        }
    }
}
