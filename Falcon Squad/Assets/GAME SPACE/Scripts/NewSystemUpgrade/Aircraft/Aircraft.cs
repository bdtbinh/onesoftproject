﻿using PathologicalGames;
using Sirenix.OdinInspector;
using SkyGameKit;
using System.Collections.Generic;
using TCore;
using UnityEngine;
//using UnityEditor.Animations;

public abstract class Aircraft : MonoBehaviour
{
    public bool aircraftIsReloading;
    public bool aircraftIsStopMove;
    public bool aircraftUsingActiveShield;
    //
    [HideInInspector]
    public PlayerController playerControllerFalconScript;
    [HideInInspector]
    public PlayerInit playerInitScript;

    public UbhShotCtrl mainWeaponUbhShotCtrl;
    public UbhShotCtrl secondWeaponUbhShotCtrl;
    public SpaceWarShootSetData[] listMainWeaponUbhSetData;
    public SpaceWarShootSetData[] listSecondWeaponUbhSetData;
    //
    public GameObject shieldReloadObj;
    public Animator animAircraft;

    public RuntimeAnimatorController[] listAnimatorController;
    public List<Sprite> listSpriteMainWeaponBullet;
    public List<Sprite> listSpriteSecondWeaponBullet;
    //public string nameSpriteIconSkill;

    private AircraftTypeEnum aircraftType;
    public AircraftTypeEnum AircraftType { get { return aircraftType; } }

    private Rank rank;
    public Rank Rank { get { return rank; } }

    private TypePlaneShot typePlaneShotBulletHell;
    public TypePlaneShot TypePlaneShotBulletHell { get { return typePlaneShotBulletHell; } }




    protected int usingSkillNumber;         //Số lần sử dụng skill
    public int UsingSkillNumber { get { return usingSkillNumber; } }

    protected float usingSkillTime;         //Thời gian 1 lần sử dụng skill
    public float UsingSkillTime { get { return usingSkillTime; } }

    protected float usingSkillCountdownTime;
    public float UsingSkillCountdownTime { get { return usingSkillCountdownTime; } }
    public System.Action<int, int> onUsingSkillNumberChange;


    protected int mainWeaponPowerLevel;
    protected int mainWeaponQuantityLevel;
    protected int mainWeaponSpeedLevel;

    protected int secondWeaponPowerLevel;
    protected int secondWeaponSpeedLevel;

    protected float mainWeaponPowerPercent = 1.0f; //Mặc định là 1, tương ứng với 100%
    protected float mainWeaponSpeedPercent = 1.0f;
    protected float mainWeaponQuantityPercent = 1.0f;

    protected float secondWeaponPowerPercent = 1.0f;
    protected float secondWeaponSpeedPercent = 1.0f;

    [DisplayAsString]
    public float speedGunPercentBonusUseActiveSkill;
    [DisplayAsString]
    public int powerGunActiveSkill;

    [DisplayAsString]
    public int mainWeaponPower;
    [DisplayAsString]
    public int mainWeaponQuantity;
    [DisplayAsString]
    public float mainWeaponSpeed;
    [DisplayAsString]
    public Sprite mainWeaponSprite;

    [DisplayAsString]
    public int secondWeaponPower;
    [DisplayAsString]
    public float secondWeaponSpeed;
    [DisplayAsString]
    public Sprite secondWeaponSprite;

    // tốc độ bắn
    protected float mainWeaponSpeedGunPercent = 1.0f; // đây là tốc độ bắn. CÒn secondWeaponSpeedLevel là tốc độ bay của đạn
    List<float> listMainWeaponSpeedGunOrigi = new List<float>(); // đây là value mặc định ban đầu lúc chưa được bonus
    //[DisplayAsString]
    //public float mainWeaponSpeedGun;


    public void Start()
    {
        animAircraft = GetComponent<Animator>();
        playerControllerFalconScript = GetComponentInParent<PlayerController>();
        playerInitScript = GetComponentInParent<PlayerInit>();

        for (int i = 0; i < listMainWeaponUbhSetData.Length; i++)
        {
            listMainWeaponUbhSetData[i].playerInitScript = playerInitScript;

            listMainWeaponUbhSetData[i].typePlaneShotBulletHell = TypePlaneShot.Aircraft;
            listMainWeaponUbhSetData[i].typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.AircraftMainPower;
        }
        for (int i = 0; i < listSecondWeaponUbhSetData.Length; i++)
        {
            //Debug.LogError("listSecondWeaponUbhSetData.Length: " + listSecondWeaponUbhSetData.Length + "---i:" + i);
            listSecondWeaponUbhSetData[i].playerInitScript = playerInitScript;
            listSecondWeaponUbhSetData[i].typePlaneShotBulletHell = TypePlaneShot.Aircraft;
            listSecondWeaponUbhSetData[i].typeGunPlayerShotBulletHell = TypeGunPlayerShotBulletHell.AircraftSecondPower;
        }

        //ChangeUsingSkillNumber(10);
    }
    //------------------- CHANGE DATA ------------------------------------------------------
    //Load data khi chưa thêm bất cứ extra nào cả
    public void InitData(AircraftTypeEnum type, Rank rank, TypePlaneShot typePlaneShot)
    {
        //Debug.LogError("InitData");
        aircraftIsStopMove = true;
        aircraftType = type;
        this.rank = rank;
        this.typePlaneShotBulletHell = typePlaneShot;

        listMainWeaponSpeedGunOrigi.Clear();

        for (int i = 0; i < mainWeaponUbhShotCtrl.m_shotList.Count; i++)
        {
            listMainWeaponSpeedGunOrigi.Add(mainWeaponUbhShotCtrl.m_shotList[i].m_afterDelay);
        }

        animAircraft.runtimeAnimatorController = listAnimatorController[((int)rank) - 1] as RuntimeAnimatorController;
    }

    Vector3 tempVector3;

    // ----tĂng level để lấy data
    public void AddMainWeaponPowerLevel(int amount, bool sendData = false)
    {
        if (amount < 0) return;
        mainWeaponPowerLevel += amount;
        LoadMainWeaponPowerData();
        mainWeaponPower = (int)(mainWeaponPower * mainWeaponPowerPercent);
        GetSpriteMainWeaponUse();
        if (sendData)
        {
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
            {
                ConnectManager.Instance.SendData(CachePvp.PLAYER_BULLET, tempVector3, amount);
            }
        }
        //Debug.LogError("mainWeaponPowerLevel: " + mainWeaponPowerLevel + "----mainWeaponPower:" + mainWeaponPower);
        Debug.Log("<color=#00fff9>" +
            "mainWeaponPowerLevel: " + mainWeaponPowerLevel + "----mainWeaponPower:" + mainWeaponPower
            + "</color>");
    }
    public void AddMainWeaponQuantityLevel(int amount)
    {
        if (amount < 0) return;
        mainWeaponQuantityLevel += amount;
        LoadMainWeaponQuantityData();

        for (int i = 0; i < listMainWeaponUbhSetData.Length; i++)
        {
            listMainWeaponUbhSetData[i].wayNumFromAircraft = mainWeaponQuantity;
            listMainWeaponUbhSetData[i].m_bulletNum = mainWeaponQuantity;
        }
        //Debug.LogError("mainWeaponQuantityLevel: " + mainWeaponQuantityLevel + "----mainWeaponQuantity:" + mainWeaponQuantity);
        Debug.Log("<color=#00fff9>" +
        "mainWeaponQuantityLevel: " + mainWeaponQuantityLevel + "----mainWeaponQuantity:" + mainWeaponQuantity
         + "</color>");

    }
    public void AddMainWeaponSpeedLevel(int amount)
    {
        if (amount < 0) return;
        mainWeaponSpeedLevel += amount;
        LoadMainWeaponSpeedData();

        for (int i = 0; i < listMainWeaponUbhSetData.Length; i++)
        {
            listMainWeaponUbhSetData[i].m_bulletSpeed = mainWeaponSpeed;
        }
        //Debug.LogError("mainWeaponSpeedLevel: " + mainWeaponSpeedLevel + "----mainWeaponSpeed:" + mainWeaponSpeed);
        Debug.Log("<color=#00fff9>" +
        "mainWeaponSpeedLevel: " + mainWeaponSpeedLevel + "----mainWeaponSpeed:" + mainWeaponSpeed
        + "</color>");
    }
    //
    public void AddSecondWeaponPowerLevel(int amount)
    {
        if (amount < 0) return;
        secondWeaponPowerLevel += amount;
        LoadSecondWeaponPowerData();
        secondWeaponPower = (int)(secondWeaponPower * secondWeaponPowerPercent);
        GetSpriteSecondWeaponUse();
        //Debug.LogError("secondWeaponPowerLevel: " + secondWeaponPowerLevel + "----secondWeaponPower:" + secondWeaponPower);
        Debug.Log("<color=#00fff9>" +
       "secondWeaponPowerLevel: " + secondWeaponPowerLevel + "----secondWeaponPower:" + secondWeaponPower
       + "</color>");
    }

    public void AddSecondWeaponSpeedLevel(int amount)
    {
        if (amount < 0) return;
        secondWeaponSpeedLevel += amount;
        LoadSecondWeaponSpeedData();
        //secondWeaponUbhSetData.m_bulletSpeed = secondWeaponSpeed;
        for (int i = 0; i < listSecondWeaponUbhSetData.Length; i++)
        {
            listSecondWeaponUbhSetData[i].m_bulletSpeed = secondWeaponSpeed;
        }
        //Debug.LogError("secondWeaponSpeedLevel: " + secondWeaponSpeedLevel + "----secondWeaponSpeed:" + secondWeaponSpeed);
        Debug.Log("<color=#00fff9>" +
        "secondWeaponSpeedLevel: " + secondWeaponSpeedLevel + "----secondWeaponSpeed:" + secondWeaponSpeed
        + "</color>");
    }

    //--- tăng phần trăm bonus cho đạn
    public void ChangeMainWeaponPowerPercent(float amount)
    {
        mainWeaponPowerPercent += amount;
        LoadMainWeaponPowerData();
        mainWeaponPower = (int)(mainWeaponPower * mainWeaponPowerPercent);
        //Debug.LogError("mainWeaponPowerPercent: " + mainWeaponPowerPercent + "----mainWeaponPower:" + mainWeaponPower);
        Debug.Log("<color=#00fff9>" +
        "mainWeaponPowerPercent: " + mainWeaponPowerPercent + "----mainWeaponPower:" + mainWeaponPower
        + "</color>");
    }
    public void ChangeMainWeaponSpeedPercent(float amount)
    {
        mainWeaponSpeedPercent += amount;
        LoadMainWeaponSpeedData();
        mainWeaponSpeed = mainWeaponSpeed * mainWeaponSpeedPercent;
        //mainWeaponUbhSetData.m_bulletSpeed = mainWeaponSpeed;
        for (int i = 0; i < listMainWeaponUbhSetData.Length; i++)
        {
            listMainWeaponUbhSetData[i].m_bulletSpeed = mainWeaponSpeed;
        }

        //Debug.LogError("mainWeaponSpeedPercent: " + mainWeaponSpeedPercent + "----mainWeaponSpeed:" + mainWeaponSpeed);
        Debug.Log("<color=#00fff9>" +
        "mainWeaponSpeedPercent: " + mainWeaponSpeedPercent + "----mainWeaponSpeed:" + mainWeaponSpeed
        + "</color>");
    }

    public void ChangeMainWeaponSpeedGunPercentForever(float amount)
    {
        mainWeaponSpeedGunPercent -= amount; // phẢi trừ thì khi nhân vào cái thời gian giữa 2 lần bắn nó mới giảm
        //mainWeaponSpeedGun = mainWeaponSpeedGunOrigi * mainWeaponSpeedGunPercent;
        //mainWeaponUbhShotCtrl.m_shotList[0].m_afterDelay = mainWeaponSpeedGun;

        for (int i = 0; i < mainWeaponUbhShotCtrl.m_shotList.Count; i++)
        {
            mainWeaponUbhShotCtrl.m_shotList[i].m_afterDelay = listMainWeaponSpeedGunOrigi[i] * mainWeaponSpeedGunPercent;
        }

        //Debug.LogError("mainWeaponSpeedGunPercent: " + mainWeaponSpeedGunPercent + "----mainWeaponSpeedGun:" + mainWeaponSpeedGun);
    }

    public void ChangeMainWeaponSpeedGunPercentTimer(float amount, float time)
    {
        mainWeaponSpeedGunPercent -= amount; // phẢi trừ thì khi nhân vào cái thời gian giữa 2 lần bắn nó mới giảm
        if (mainWeaponSpeedGunPercent < 0)
        {
            Debug.LogError("mainWeaponSpeedGunPercent không được nhỏ hơn 0 ");
            return;
        }
        //mainWeaponSpeedGun = mainWeaponSpeedGunOrigi * mainWeaponSpeedGunPercent;
        //mainWeaponUbhShotCtrl.m_shotList[0].m_afterDelay = mainWeaponSpeedGun;

        for (int i = 0; i < mainWeaponUbhShotCtrl.m_shotList.Count; i++)
        {
            //Debug.LogError("for 1");
            mainWeaponUbhShotCtrl.m_shotList[i].m_afterDelay = listMainWeaponSpeedGunOrigi[i] * mainWeaponSpeedGunPercent;
        }

        LeanTween.delayedCall(time, () =>
        {
            mainWeaponSpeedGunPercent += amount;
            //mainWeaponSpeedGun = mainWeaponSpeedGunOrigi * mainWeaponSpeedGunPercent;
            //mainWeaponUbhShotCtrl.m_shotList[0].m_afterDelay = mainWeaponSpeedGun;
            //Debug.LogError("for 2");
            for (int i = 0; i < mainWeaponUbhShotCtrl.m_shotList.Count; i++)
            {
                //Debug.LogError("for 3");
                mainWeaponUbhShotCtrl.m_shotList[i].m_afterDelay = listMainWeaponSpeedGunOrigi[i] * mainWeaponSpeedGunPercent;
            }

        }).setIgnoreTimeScale(true);

        //Debug.LogError("mainWeaponSpeedGunPercent: " + mainWeaponSpeedGunPercent + "----mainWeaponSpeedGun:" + mainWeaponSpeedGun);
    }

    public void ChangeSecondWeaponPowerPercent(float amount)
    {
        //if (amount <= 0) return;
        secondWeaponPowerPercent += amount;
        LoadSecondWeaponPowerData();
        secondWeaponPower = (int)(secondWeaponPower * secondWeaponPowerPercent);
        //Debug.LogError("secondWeaponPowerPercent: " + secondWeaponPowerPercent + "----secondWeaponPower:" + secondWeaponPower);
        Debug.Log("<color=#00fff9>" +
      "secondWeaponPowerPercent: " + secondWeaponPowerPercent + "----secondWeaponPower:" + secondWeaponPower
      + "</color>");
    }
    public void ChangeSecondWeaponSpeedPercent(float amount)
    {
        //if (amount <= 0) return;
        secondWeaponSpeedPercent += amount;
        LoadSecondWeaponSpeedData();
        secondWeaponSpeed = secondWeaponSpeed * secondWeaponSpeedPercent;

        for (int i = 0; i < listSecondWeaponUbhSetData.Length; i++)
        {
            listSecondWeaponUbhSetData[i].m_bulletSpeed = secondWeaponSpeed;
        }
        Debug.Log("<color=#00fff9>" +
        "secondWeaponSpeedPercent: " + secondWeaponSpeedPercent + "----secondWeaponSpeed:" + secondWeaponSpeed
        + "</color>");
    }

    //---get sprite sử dụng

    void GetSpriteMainWeaponUse()
    {
        int indexGetSprite = (int)(mainWeaponPowerLevel / 15);

        if (indexGetSprite >= listSpriteMainWeaponBullet.Count)
        {
            indexGetSprite = listSpriteMainWeaponBullet.Count - 1;
            Debug.Log("indexGetValue get Sprite Bullet quá count ");
        }
        if (listSpriteMainWeaponBullet[indexGetSprite] != null)
        {
            mainWeaponSprite = listSpriteMainWeaponBullet[indexGetSprite];
        }
        //Debug.LogError("mainWeaponPowerLevel:" + mainWeaponPowerLevel + "---indexGetSprite:" + indexGetSprite + "---mainWeaponSprite:" + mainWeaponSprite);
        Debug.Log("<color=#00fff9>" +
        "mainWeaponPowerLevel:" + mainWeaponPowerLevel + "---indexGetSprite:" + indexGetSprite + "---mainWeaponSprite:" + mainWeaponSprite
        + "</color>");
    }

    void GetSpriteSecondWeaponUse()
    {
        int levelGet = secondWeaponPowerLevel - 150;
        if (levelGet < 0) levelGet = 0;
        int indexGetSprite = (int)(levelGet / 15);

        if (indexGetSprite >= listSpriteSecondWeaponBullet.Count)
        {
            indexGetSprite = listSpriteSecondWeaponBullet.Count - 1;
            Debug.Log("indexGetValue get Sprite Bullet quá count ");
        }
        if (listSpriteSecondWeaponBullet[indexGetSprite] != null)
        {
            secondWeaponSprite = listSpriteSecondWeaponBullet[indexGetSprite];
        }
    }

    //--- số lần sử dụng skill
    public void ChangeUsingSkillNumber(int amount)
    {
        usingSkillNumber += amount;

        if (onUsingSkillNumberChange != null) onUsingSkillNumberChange(usingSkillNumber, amount);

    }

    public void ChangeUsingSkillTime(float amount)
    {
        usingSkillTime += amount;
        //Debug.LogError("usingSkillTime:" + usingSkillTime);
        Debug.Log("<color=#00fff9>" +
        "usingSkillTime:" + usingSkillTime
        + "</color>");
    }

    public void ChangeUsingSkillCountdownTime(float amount)
    {
        usingSkillCountdownTime += amount;
        //Debug.LogError("usingSkillCountdownTime:" + usingSkillCountdownTime);
        Debug.Log("<color=#00fff9>" +
        "usingSkillCountdownTime:" + usingSkillCountdownTime
        + "</color>");
    }



    //--------

    protected abstract void LoadMainWeaponPowerData();
    protected abstract void LoadMainWeaponQuantityData();
    protected abstract void LoadMainWeaponSpeedData();

    protected abstract void LoadSecondWeaponPowerData();
    protected abstract void LoadSecondWeaponSpeedData();

    public abstract void UseActiveSkill();
    public abstract void StopTimeScaleSkill();

    public abstract void ShootMainWeapon();
    public abstract void ShootSecondWeapon();
    public abstract void ShootUltimateWeapon();

    public abstract void StopShootMainWeapon();
    public abstract void StopShootSecondWeapon();
    public abstract void StopShootUltimateWeapon();



    public bool HasSkillRank(Rank rank)
    {
        return (int)this.rank >= (int)rank;
    }


    #region  //------------------- Collision ------------------------------------------------------

    //------------------- Collision ------------------------------------------------------


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Item")
        {
            ItemController itemControllerIsn = col.GetComponent<ItemController>();
            itemControllerIsn.PlayerTriggerItem(this);
            itemControllerIsn.Despawn();
        }

        if (!MainScene.Instance.gameFinished && !aircraftIsReloading && !aircraftUsingActiveShield && !MainScene.Instance.gameStopping && LevelManager.Instance.GameState == GameStateType.Playing && !MainScene.Instance.bossDead)
        {
            switch (col.tag)
            {
                case "EnemyBullet":
                    Debug.Log("Player Trigger EnemyBullet");
                    //Pool_EffectPlayerDie();
                    playerControllerFalconScript.PlayerDie();
                    //
                    UbhBullet bulletDespawn = col.transform.parent.GetComponent<UbhBullet>();
                    if (bulletDespawn != null)
                    {
                        UbhObjectPool.instance.ReleaseBullet(bulletDespawn);
                    }
                    break;
                case "BulletEnemyDestroyByWing":
                    Debug.Log("Player Trigger BulletEnemyDestroyByWing");
                    playerControllerFalconScript.PlayerDie();
                    //
                    break;
                case "BulletEnemyUndestroy":
                    Debug.Log("Player Trigger BulletEnemyUndestroy");
                    //Pool_EffectPlayerDie();
                    playerControllerFalconScript.PlayerDie();
                    //
                    break;
                case "Enemy":
                    Debug.Log("Player Trigger Enemy");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss1":
                    Debug.Log("Player Trigger Boss1");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss2":
                    Debug.Log("Player Trigger Boss2");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss3":
                    Debug.Log("Player Trigger Boss3");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss4":
                    Debug.Log("Player Trigger Boss4");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss5":
                    Debug.Log("Player Trigger Boss5");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss7":
                    Debug.Log("Player Trigger Boss7");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss8":
                    Debug.Log("Player Trigger Boss8");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss9":
                    Debug.Log("Player Trigger Boss9");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss10":
                    Debug.Log("Player Trigger Boss10");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "LazeNhen":
                    Debug.Log("Player Trigger LazeNhen");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Hazzard":
                    Debug.Log("Player Trigger Hazzard");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss2MissileExplosion":
                    Debug.Log("Player Trigger Boss2MissileExplosion");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss4SpiderSilk":
                    Debug.Log("Player Trigger Boss4SpiderSilk");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "FXEnemy305_Bonangluong":
                    Debug.Log("Player Trigger FXEnemy305_Bonangluong");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "BulletBoss7_Stone":
                    Debug.Log("Player Trigger BulletBoss7_Stone");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "EnemyBulletBoomerang":
                    Debug.Log("Player Trigger EnemyBulletBoomerang");
                    playerControllerFalconScript.PlayerDie();
                    break;
                case "Boss10VeTinh":
                    Debug.Log("Player Trigger Boss10VeTinh");
                    playerControllerFalconScript.PlayerDie();
                    break;


            }

        }

    }

    #endregion

    //---------------------Fx -----------------------

    public void Pool_EffectPlayerDie()
    {
        if (EffectList.Instance.playerDie != null)
        {
            Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.playerDie, transform.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
        }
    }
}

