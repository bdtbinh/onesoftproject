﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CacheGame : MonoBehaviour
{

    //public static int numItemPowerUpUse = 0;
    //public static int numUsingSkill = 0;            //Số lần sử dụng skill trong 1 level
    //public static int numLifeUpUse = 0;
    public const string NUM_ITEM_POWERUP_USING = "NUM_ITEM_POWERUP_USING";
    public const string NUM_ITEM_LIFE_USING = "NUM_ITEM_LIFE_USING";
    public const string NUM_ITEM_SKILL_USING = "NUM_ITEM_SKILL_USING";
    public const string CACHE_POPUP_CARD_SEELING = "PopupCardPackIsSelling"; //=1 nếu popup đang trong thời gian bán


    public static int numItemPowerUpUse
    {
        get
        {
            return PlayerPrefs.GetInt(NUM_ITEM_POWERUP_USING, 0);
        }
        set
        {
            PlayerPrefs.SetInt(NUM_ITEM_POWERUP_USING, value);
        }
    }
    public static int numUsingSkill
    {
        get
        {
            return PlayerPrefs.GetInt(NUM_ITEM_SKILL_USING, 0);
        }
        set
        {
            PlayerPrefs.SetInt(NUM_ITEM_SKILL_USING, value);
        }
    }
    public static int numLifeUpUse
    {
        get
        {
            return PlayerPrefs.GetInt(NUM_ITEM_LIFE_USING, 0);
        }
        set
        {
            PlayerPrefs.SetInt(NUM_ITEM_LIFE_USING, value);
        }
    }

    public static int PopupCardPackIsSelling //=1 nếu đang trong thời gian bán
    {
        get
        {
            return PlayerPrefs.GetInt(CACHE_POPUP_CARD_SEELING, 0);
        }
        set
        {
            PlayerPrefs.SetInt(CACHE_POPUP_CARD_SEELING, value);
        }
    }

    //-----------------   Endless  ----------------------

    //--- biến này dùng để reset data của user sau mỗi ngày chơi 
    public static void SetLastDayOpenGame(int day)
    {
        AntiCheat.SetIntConverted("NgayCuoiCungBatGame", day);
    }

    public static int GetLastDayOpenGame()
    {
        return AntiCheat.GetIntConverted("LastDayOpenGame", "NgayCuoiCungBatGame", 0);
    }

    //-------
    public static void SetTotalPlayEndless(int numPlay)
    {
        //PlayerPrefs.SetInt("SetTotalPlayEndless", numPlay);

        AntiCheat.SetIntConverted("TongLanChoi_CheDo_VoTan", numPlay);
    }

    public static int GetTotalPlayEndless()
    {
        //return PlayerPrefs.GetInt("SetTotalPlayEndless", 5);

        return AntiCheat.GetIntConverted("SetTotalPlayEndless", "TongLanChoi_CheDo_VoTan", 5);
    }

    //----
    public static void SetMaxWavesEndlessReached(int maxWave)
    {
        //PlayerPrefs.SetInt("SetMaxWavesEndlessReached", maxWave);
        AntiCheat.SetIntConverted("CapDo_EnNet_CaoNhat_DaChoi", maxWave);
    }

    public static int GetMaxWavesEndlessReached()
    {
        //return PlayerPrefs.GetInt("SetMaxWavesEndlessReached",0);

        return AntiCheat.GetIntConverted("SetMaxWavesEndlessReached", "CapDo_EnNet_CaoNhat_DaChoi", 0);
    }



    //----------------------------------------------------------------------------------------------------------------------------------------

    //-------
    public static void SetNumShowStarterPackInLose(int num)
    {
        //PlayerPrefs.SetInt("NumShowStarterPackInLose", num);
        AntiCheat.SetIntConverted("CapDo_EnNet_CaoNhat_DaChoi", num);
    }

    public static int GetNumShowStarterPackInLose()
    {
        //return PlayerPrefs.GetInt("NumShowStarterPackInLose");
        return AntiCheat.GetIntConverted("NumShowStarterPackInLose", "SoLanHienThiStarterPack_KhiThua", 0);
    }



    //-------Cache lần đầu tiên mở mở game lên thì cho vào thẳng Main
    // - =1 là đã vào game hơn 1 lần
    public static void SetFirtSessionGame(int first)
    {
        PlayerPrefs.SetInt("SetFirtSessionGame", first);
    }

    public static int GetFirtSessionGame()
    {
        return PlayerPrefs.GetInt("SetFirtSessionGame");
    }

    public static bool HasFirstSessionGame()
    {
        return PlayerPrefs.HasKey("SetFirtSessionGame");
    }

    // - =1 là đã chơi tutorial rồi
    public static void SetTutorialGame(int isTutorial)
    {
        PlayerPrefs.SetInt("userchoiTutorial", isTutorial);
    }

    public static int GetTutorialGame()
    {
        return PlayerPrefs.GetInt("userchoiTutorial");
    }

    //
    // Set số lượng star đã đạt được ở mỗi level
    public static void SetNumStarLevel(int level, int indexStar, int hideShow, string difficult, GameContext.LevelCampaignMode levelCampaignMode)
    {
        //1 la` da~ nhan dc star nay`
        //PlayerPrefs.SetInt("NumStarLevel_" + level + "_" + indexStar + difficult, hideShow);
        if (levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            AntiCheat.SetIntConverted("SoLuongSao_1CapDo" + level + "_" + indexStar + difficult + "DaNhanDuoc", hideShow);
            PvpUtil.SendUpdatePlayer("SetNumStarLevel");
        }
        else
        {
            AntiCheat.SetInt("LevelExtra_StarNum_" + level + "_" + indexStar + difficult, hideShow);
            PvpUtil.SendUpdatePlayer("SetNumStarLevelExtra");
        }
    }

    public static void SetNumStarLevelNamNX(int level, int indexStar, int hideShow, string difficult, GameContext.LevelCampaignMode levelCampaignMode = GameContext.LevelCampaignMode.Normal)
    {
        //1 la` da~ nhan dc star nay`
        //PlayerPrefs.SetInt("NumStarLevel_" + level + "_" + indexStar + difficult, hideShow);
        if (levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            AntiCheat.SetIntConverted("SoLuongSao_1CapDo" + level + "_" + indexStar + difficult + "DaNhanDuoc", hideShow);
        }
        else
        {
            AntiCheat.SetInt("LevelExtra_StarNum_" + level + "_" + indexStar + difficult, hideShow);
        }

    }

    public static int GetNumStarLevel(int level, int indexStar, string difficult, GameContext.LevelCampaignMode levelCampaignMode)
    {
        //return PlayerPrefs.GetInt("NumStarLevel_" + level + "_" + indexStar + difficult,0);
        if (levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            return AntiCheat.GetIntConverted("NumStarLevel_" + level + "_" + indexStar + difficult, "SoLuongSao_1CapDo" + level + "_" + indexStar + difficult + "DaNhanDuoc", 0);
        }
        else
        {
            return AntiCheat.GetInt("LevelExtra_StarNum_" + level + "_" + indexStar + difficult, 0);
        }
    }
    //

    public static void SetPassedLevelExtra(int level, string difficult, int passed)
    {
        //1 la` passed level này
        AntiCheat.SetInt("PassedLevelExtra" + level + "_" + difficult, passed);
        PvpUtil.SendUpdatePlayer("SetPassedLevelExtra");
    }

    public static int GetPassedLevelExtra(int level, string difficult)
    {
        return AntiCheat.GetInt("PassedLevelExtra" + level + "_" + difficult, 0);
    }
    // Set CurLevel đang chơi


    public static void SetCurrentLevel(int curLevel)
    {
        //PlayerPrefs.SetInt("CurentLevelPlay", curLevel);
        AntiCheat.SetIntConverted("CapDoDangChoi", curLevel);
    }

    public static int GetCurrentLevel()
    {
        //return PlayerPrefs.GetInt("CurentLevelPlay",1);
        return AntiCheat.GetIntConverted("CurentLevelPlay", "CapDoDangChoi", 1);
    }

    // Set Cur World Play

    public static int GetCurrentWorld()
    {
        return Mathf.Min((int)(((CacheGame.GetMaxLevel3Difficult() - 1) / 7) + 1), (int)((GameContext.TOTAL_LEVEL / 7)));
    }


    // set max level lon' nhat' trong ca? 3 do kho'

    public static void SetMaxLevel3Difficult(int max)
    {
        ////PlayerPrefs.SetInt("MaxLevel3Difficult", max);
        //AntiCheat.SetIntConverted("CapDo_Lon_Nhat_3DoKho", max);
    }

    public static int GetMaxLevel3Difficult()
    {
        //return PlayerPrefs.GetInt("MaxLevel3Difficult",1);
        int maxLevel = 0;
        maxLevel = Mathf.Max(Mathf.Max(GetMaxLevel(GameContext.DIFFICULT_NOMAL), GetMaxLevel(GameContext.DIFFICULT_HARD), GetMaxLevel(GameContext.DIFFICULT_HELL)));
        AntiCheat.SetIntConverted("CapDo_Lon_Nhat_3DoKho", maxLevel);

        return AntiCheat.GetIntConverted("MaxLevel3Difficult", "CapDo_Lon_Nhat_3DoKho", 1);
    }



    public static void SetMaxLevel(string difficult, int max)
    {
        //PlayerPrefs.SetInt("MaxLevel" + difficult, max);
        AntiCheat.SetIntConverted("CapDo_Lon_Nhat_Tung_DoKho" + AntiCheat.GetString3Difficult(difficult), max);
    }

    public static int GetMaxLevel(string difficult)
    {
        //return PlayerPrefs.GetInt("MaxLevel" + difficult, 1);
        return AntiCheat.GetIntConverted("MaxLevel" + difficult, "CapDo_Lon_Nhat_Tung_DoKho" + AntiCheat.GetString3Difficult(difficult), 1);
    }



    // set độ khó đang chọn là normal,hard hay hell

    public static void SetDifficultCampaign(string difficult)
    {
        PlayerPrefs.SetString("SetDifficultCampaign", difficult);
    }

    public static string GetDifficultCampaign()
    {
        return PlayerPrefs.GetString("SetDifficultCampaign", GameContext.DIFFICULT_NOMAL);
    }
    //

    //set xem da dung inapp hay chưa. Để loại  bỏ admob

    public static void SetUsedInapp(int inapp)
    {
        // = 1 la da dung inapp
        //PlayerPrefs.SetInt("UsedInapp", inapp);
        AntiCheat.SetIntConverted("DaNapTien_HayChua", inapp);
    }

    public static int GetUsedInapp()
    {
        //return PlayerPrefs.GetInt("UsedInapp");
        return AntiCheat.GetIntConverted("UsedInapp", "DaNapTien_HayChua", 0);
    }

    //check user mua gold  lần đâu tiên. Nhân đôi gold cho các user đó
    public static void SetInappGemOneTime(int indexPack, int buyed)
    {
        // inappTrue = 1 la da dung inapp
        AntiCheat.SetInt("UsedInappGemFirst" + indexPack, buyed);
    }

    public static int GetInappGemOneTime(int indexPack)
    {
        // inappTrue = 1 la da dung inapp
        // index pack từ 2usd=1 5usd=2 10usd=3 20usd=5 50usd=5 2usd=1đến 5
        return AntiCheat.GetInt("UsedInappGemFirst" + indexPack, 0);
    }

    public static void SetInappGoldOneTime(int indexPack, int buyed)
    {
        // inappTrue = 1 la da dung inapp
        AntiCheat.SetInt("UsedInappGoldFirst" + indexPack, buyed);
    }

    public static int GetInappGoldOneTime(int indexPack)
    {
        // inappTrue = 1 la da dung inapp
        // index pack từ 2usd=1 5usd=2 10usd=3 20usd=5 50usd=5 2usd=1đến 5
        return AntiCheat.GetInt("UsedInappGoldFirst" + indexPack, 0);
    }

    public static void SetBuyInfinityPack(int inappTrue)
    {
        // = 1 la da dung inapp
        //PlayerPrefs.SetInt("UsedInapp", inapp);
        AntiCheat.SetIntConverted("DaNapTien_Goi_Infi", inappTrue);
    }

    public static int GetBuyInfinityPack()
    {
        //return PlayerPrefs.GetInt("UsedInapp");
        //=1 là đã nạp
        return AntiCheat.GetIntConverted("UsedInappGoldFirstInfi", "DaNapTien_Goi_Infi", 0);
    }


    //-----LuckyWheel------------------------------------

    public static void SetDayUseVideoLuckyWheel(int day)
    {
        // ngày cuối cùng sửa dụng vòng quay may mắn
        AntiCheat.SetIntConverted("NgayUseVideoQuayMayMan", day);
    }

    public static int GetDayUseVideoLuckyWheel()
    {
        // ngày cuối cùng sửa dụng vòng quay may mắn
        return AntiCheat.GetIntConverted("NgayUseVideoQuayMayManOld", "NgayUseVideoQuayMayMan", 0);
    }


    public static void SetDayUseFreeLuckyWheel(int day)
    {
        // ngày cuối cùng sửa dụng vòng quay may mắn
        AntiCheat.SetIntConverted("NgayUseFreeQuayMayMan", day);
    }

    public static int GetDayUseFreeLuckyWheel()
    {
        // ngày cuối cùng sửa dụng vòng quay may mắn
        return AntiCheat.GetIntConverted("NgayUseFreeQuayMayManOlds", "NgayUseFreeQuayMayMan", 0);
    }



    //------------------------ gold and item --------------------------------------------------------


    // tổng lượng gold  trong game
    //, string whyLogFirebase, string whereLogFirebase
    public static void SetTotalCoin(int coin, string whyLogFirebase, string whereLogFirebase)
    {
        //PlayerPrefs.SetInt("TongSoVangDangCo", coin);
        int curCoin = GetTotalCoin();


        AntiCheat.SetIntConverted("TongSoVangDangCo", coin);

        PvpUtil.OnSetTotalCoin(curCoin, coin, whyLogFirebase, whereLogFirebase);

        //if (!GameContext.isInGameCampaign && curCoin != coin)
        //{
        //    PlayerPrefs.Save();
        //}
        if (coin - curCoin > 10)
        {
            FirebaseLogSpaceWar.LogGoldIn(coin - curCoin, whyLogFirebase, whereLogFirebase);
        }
        else if (coin - curCoin < 0)
        {
            FirebaseLogSpaceWar.LogGoldOut(coin - curCoin, whyLogFirebase, whereLogFirebase);
        }
    }

    public static void SetTotalCoinNamNX(int coin)
    {
        int curCoin = GetTotalCoin();
        AntiCheat.SetIntConverted("TongSoVangDangCo", coin);

    }

    public static int GetTotalCoin()
    {
        //return PlayerPrefs.GetInt("TotalCoinInGame", 0);
        return AntiCheat.GetIntConverted("TotalCoinInGame", "TongSoVangDangCo", 1000);
    }

    // tổng lượng gem trong game

    public static void SetTotalGem(int gem, string whyLogFirebase, string whereLogFirebase)
    {
        if (gem == GetTotalGem())
        {
            AntiCheat.SetIntConverted("TongSoGemmDangCo", gem);
        }
        else
        {
            if (gem - GetTotalGem() > 2) // nếu thay đổi nhỏ hơn 5 gem là nó nhặt được trong màn chơi => không log 
            {
                FirebaseLogSpaceWar.LogGemIn(gem - GetTotalGem(), whyLogFirebase, whereLogFirebase);
            }
            else if (gem - GetTotalGem() < -2)
            {
                FirebaseLogSpaceWar.LogGemOut(gem - GetTotalGem(), whyLogFirebase, whereLogFirebase);
            }

            AntiCheat.SetIntConverted("TongSoGemmDangCo", gem);

            PvpUtil.SendUpdatePlayer("SetTotalGem", whyLogFirebase + "--" + whereLogFirebase);

        }
    }

    public static void SetTotalGemNamNX(int gem)
    {
        //PlayerPrefs.SetInt("TotalCoinInGame", coin);
        AntiCheat.SetIntConverted("TongSoGemmDangCo", gem);
    }

    public static int GetTotalGem()
    {
        //return PlayerPrefs.GetInt("TotalCoinInGame", 0);
        return AntiCheat.GetIntConverted("TotalGemInGame", "TongSoGemmDangCo", 100);
    }

    //Energy
    public static void SetTotalEnergy(int energy, string whyLogFirebase, string whereLogFirebase)
    {
        if (energy == GetTotalEnergy())
        {
            AntiCheat.SetInt("TotalEnergyInGame", energy);
        }
        else
        {
            //if (energy - GetTotalEnergy() > 2) // nếu thay đổi nhỏ hơn 5 gem là nó nhặt được trong màn chơi => không log 
            //{
            //    FirebaseLogSpaceWar.LogGemIn(energy - GetTotalGem(), whyLogFirebase, whereLogFirebase);
            //}
            //else if (energy - GetTotalGem() < -2)
            //{
            //    FirebaseLogSpaceWar.LogGemOut(energy - GetTotalGem(), whyLogFirebase, whereLogFirebase);
            //}

            AntiCheat.SetInt("TotalEnergyInGame", energy);

            PvpUtil.SendUpdatePlayer("SetTotalEnergy", whyLogFirebase + "--" + whereLogFirebase);
        }
    }

    public static void SetTotalEnergyNamNX(int energy)
    {
        AntiCheat.SetInt("TotalEnergyInGame", energy);
    }

    public static int GetTotalEnergy()
    {
        return AntiCheat.GetInt("TotalEnergyInGame", 0);
    }


    // Số Energy đã nhận được trong 1 ngày-------------------

    public static void SetNumEnergyGetToday(int energy)
    {
        AntiCheat.SetInt("TotalEnergyGetPerDay", energy);
    }

    public static int GetNumEnergyGetToday()
    {
        return AntiCheat.GetInt("TotalEnergyGetPerDay", 0);
    }


    // Số gem đã nhận được trong 1 ngày-------------------

    public static void SetNumGemGetToday(int gem)
    {
        //PlayerPrefs.SetInt("TotalCoinInGame", coin);
        AntiCheat.SetIntConverted("TongSoGemmRoiRaHomNay", gem);
    }

    public static int GetNumGemGetToday()
    {
        //return PlayerPrefs.GetInt("TotalCoinInGame", 0);
        return AntiCheat.GetIntConverted("SoGemmRoiRaHomNay", "TongSoGemmRoiRaHomNay", 0);
    }

    // Số CARD đã nhận được trong 1 ngày-------------------

    public static void SetNumCardGetToday(int gem)
    {
        //PlayerPrefs.SetInt("TotalCoinInGame", coin);
        AntiCheat.SetIntConverted("TongSoCardRoiRaHomNay", gem);
    }

    public static int GetNumCardGetToday()
    {
        //return PlayerPrefs.GetInt("TotalCoinInGame", 0);
        return AntiCheat.GetIntConverted("SoCardRoiRaHomNay", "TongSoCardRoiRaHomNay", 0);
    }

    //---------tổng số item life có-------------------------
    public static void SetTotalItemLife(int numItem)
    {

        //PlayerPrefs.SetInt("TotalItemLife", numItem);
        AntiCheat.SetIntConverted("Tong_So_VatPham_Mang", numItem);
    }

    public static int GetTotalItemLife()
    {
        //return PlayerPrefs.GetInt("TotalItemLife");
        return AntiCheat.GetIntConverted("TotalItemLife", "Tong_So_VatPham_Mang", 0);
    }

    //----------tổng số item power up

    public static void SetTotalItemPowerUp(int numItem)
    {
        //PlayerPrefs.SetInt("TotalItemPowerUp", numItem);
        AntiCheat.SetIntConverted("Tong_So_VatPham_SucManh", numItem);
    }

    public static int GetTotalItemPowerUp()
    {
        //return PlayerPrefs.GetInt("TotalItemPowerUp");
        return AntiCheat.GetIntConverted("TotalItemPowerUp", "Tong_So_VatPham_SucManh", 0);
    }

    //----------tổng số item timeSphere

    public static void SetTotalItemActiveSkill(int numItem)
    {
        //PlayerPrefs.SetInt("TotalItemTimeSphere", numItem);
        AntiCheat.SetIntConverted("Tong_So_VatPham_LamCham", numItem);
    }

    public static int GetTotalItemActiveSkill()
    {
        //return PlayerPrefs.GetInt("TotalItemTimeSphere");
        return AntiCheat.GetIntConverted("TotalItemTimeSphere", "Tong_So_VatPham_LamCham", 0);
    }

    //----  danh` cho sensivity
    public static void SetSpeedPlayer(float speed)
    {

        PlayerPrefs.SetFloat("SpeedPlayer_Scrollbar", speed);
    }

    public static float GetSpeedPlayer()
    {

        return PlayerPrefs.GetFloat("SpeedPlayer_Scrollbar", 0);

    }


    //--------------------Shop upgrade-------------------------------------------------------------------------------------------------------------------------

    public static void SetCurrentLvShopUpgrade(string item, string typeUpgrade, int level)
    {

        //PlayerPrefs.SetInt("Cur_" + item + "_" + typeUpgrade, level);
        AntiCheat.SetIntConverted("CapDo" + item + "LoaiVuKhi" + "themTiChoDai" + typeUpgrade + "DkmThangHach", level);
    }

    public static int GetCurrentLvShopUpgrade(string item, string typeUpgrade)
    {
        //
        //return PlayerPrefs.GetInt("Cur_" + item + "_" + typeUpgrade, 0);
        return AntiCheat.GetIntConverted("Cur_" + item + "_" + typeUpgrade, "CapDo" + item + "LoaiVuKhi" + "themTiChoDai" + typeUpgrade + "DkmThangHach", 0);
    }

    //-------------------------------------------------------
    public static void SetPowerShopUpgradeBackup(int power)
    {
        PlayerPrefs.SetInt("PowerShopUpgradeBackup", power);
    }

    public static int GetPowerShopUpgradeBackup()
    {
        return PlayerPrefs.GetInt("PowerShopUpgradeBackup", 0);
    }


    //--------------------Set data achievement-------------------------------------------------------------------------------------------------------------------------


    public static void SetCurrentLevelAchievement(string nameItem, int level)
    {
        //PlayerPrefs.SetInt("Cur_Level_Achi_" + nameItem, level);
        AntiCheat.SetIntConverted("HienTai" + nameItem + "SoNhiemVu", level);
    }

    public static int GetCurrentLevelAchievement(string nameItem)
    {
        //
        //return PlayerPrefs.GetInt("Cur_Level_Achi_" + nameItem, 0);
        return AntiCheat.GetIntConverted("Cur_Level_Achi_" + nameItem, "HienTai" + nameItem + "SoNhiemVu", 0);
    }

    // set so' luong enemy, bosss... da giet' duoc so voi'  nhiem vu la bao nhieu
    public static void SetNumberAchievementAchieved(string nameItem, int level)
    {
        if (SceneManager.GetActiveScene().name != "LevelTryPlane")
        {
            //PlayerPrefs.SetInt("NumberAchiCompleteInMission_" + nameItem, level);
            AntiCheat.SetIntConverted("SoLuongAchi" + nameItem + "DaHoanThanh", level);
        }

    }

    public static int GetNumberAchievementAchieved(string nameItem)
    {
        //
        //return PlayerPrefs.GetInt("NumberAchiCompleteInMission_" + nameItem, 0);
        return AntiCheat.GetIntConverted("NumberAchiCompleteInMission_" + nameItem, "SoLuongAchi" + nameItem + "DaHoanThanh", 0);
    }


    //----------
    public static void SetCompleteAllLevelAchievement(string nameItem, int trueFalse)
    {
        //--- 1=true , 0 = false
        //PlayerPrefs.SetInt("CompleteAllLevelItemAchievement_" + nameItem, trueFalse);
        AntiCheat.SetIntConverted("DaHoanThanh" + nameItem + "ToanBoNhiemVuNew", trueFalse);
    }

    public static int GetCompleteAllLevelAchievement(string nameItem)
    {
        //
        //return PlayerPrefs.GetInt("CompleteAllLevelItemAchievement_" + nameItem, 0);
        return AntiCheat.GetIntConverted("CompleteAllLevelItemAchievement_" + nameItem, "DaHoanThanh" + nameItem + "ToanBoNhiemVuNew", 0);
    }


    //--------------------Set data Daily Quest------------------------------------------------------------------------------------------------------------



    // set so' luong enemy, bosss... da giet' duoc so voi'  nhiem vu la bao nhieu
    public static void SetNumberDailyQuestAchieved(string nameItem, int level)
    {
        if (SceneManager.GetActiveScene().name != "LevelTryPlane")
        {
            //PlayerPrefs.SetInt("NumberDailyQuest_" + nameItem, level);
            AntiCheat.SetIntConverted("DaHoanThanh" + nameItem + "TrenMoiNhiemVu", level);
        }
    }

    public static int GetNumberDailyQuestAchieved(string nameItem)
    {
        //return PlayerPrefs.GetInt("NumberDailyQuest_" + nameItem, 0);
        return AntiCheat.GetIntConverted("NumberDailyQuest_" + nameItem, "DaHoanThanh" + nameItem + "TrenMoiNhiemVu", 0);
    }

    //
    //----  Set ngay` dang show dailyQuest . Neu qua ngay` moi' se random cac' nhiem vu moi'

    public static void SetDateOpenDailyQuest(int date)
    {
        //PlayerPrefs.SetInt("DateOpenDailyQuest", date);
        AntiCheat.SetIntConverted("NgayDangSuDung_NhiemVuHangNgay", date);
    }

    public static int GetDateOpenDailyQuest()
    {
        //
        //return PlayerPrefs.GetInt("DateOpenDailyQuest", 0);
        return AntiCheat.GetIntConverted("DateOpenDailyQuest", "NgayDangSuDung_NhiemVuHangNgay", 0);
    }

    // xét xem item đó có được lựa  chọn  đê show trong ngày  hay ko .  =1  là show  , còn lại là ko show

    public static void SetItemDailyQuestChooseShow(string nameItem, int level)
    {
        AntiCheat.SetIntConverted("NhiemVu" + nameItem + "CoDuocSuDungKhong", level);
    }

    public static int GetItemDailyQuestChooseShow(string nameItem)
    {
        return AntiCheat.GetIntConverted("ItemDailyQuestChooseShow_" + nameItem, "NhiemVu" + nameItem + "CoDuocSuDungKhong", 0);
    }

    // xét xem item đó có đa duoc hoan` thanh` hay chua

    public static void SetCompleteItemDailyQuest(string nameItem, int trueFalse)
    {
        //--- 1=true , 2 = false
        AntiCheat.SetIntConverted("DaHoanThanh" + nameItem + "1NhiemVuHangNgay", trueFalse);
    }

    public static int GetCompleteItemDailyQuest(string nameItem)
    {
        return AntiCheat.GetIntConverted("CompleteItemDailyQuest_" + nameItem, "DaHoanThanh" + nameItem + "1NhiemVuHangNgay", 0);
    }


    ////typeShow=1  :box chua duoc nhan qua  ; typeShow=2  :box da san sang nhan qua  ; typeShow=3  :box da nhan qua

    public static void SetTypeShowGiftBoxDailyQuest(int indexBox, int typeShow)
    {
        //typeShow=1  :box chua duoc nhan qua  ; typeShow=2  :box da san sang nhan qua  ; typeShow=3  :box da nhan qua
        AntiCheat.SetIntConverted("TrangThai" + indexBox + "HopQua", typeShow);
    }

    public static int GetTypeShowGiftBoxDailyQuest(int indexBox)
    {
        //
        //return PlayerPrefs.GetInt("TypeShowGiftBoxDailyQuest_" + indexBox, 0);
        return AntiCheat.GetIntConverted("TypeShowGiftBoxDailyQuest_" + indexBox, "TrangThai" + indexBox + "HopQua", 0);
    }



    //-----cache lại số lần đã die ở 1 level đề điều chỉnh power rơi ra hoặc show pack

    public static void SetNumLoseInOneLevel(int level, string difficult, int numLose)
    {
        //PlayerPrefs.SetInt("NumLoseToGetMaxItemPower_" + level + difficult, numLose);
        AntiCheat.SetIntConverted("SoLanThua" + level + "1CapDo" + difficult, numLose);
    }

    public static int GetNumLoseInOneLevel(int level, string difficult)
    {
        //
        //return PlayerPrefs.GetInt("NumLoseToGetMaxItemPower_" + level + difficult, 0);
        return AntiCheat.GetIntConverted("NumLoseToGetMaxItemPower_" + level + difficult, "SoLanThua" + level + "1CapDo" + difficult, 0);
    }



    //------------------------------------Panel unlock SpaceShip------------------------------------------------------

    public static void SetSpaceShipIsUnlocked(int indexSpaceShip, int unlocked)
    {

        //=1 la` da unlock
        //PlayerPrefs.SetInt("SpaceShipIsUnlocked_" + indexSpaceShip, unlocked);
        AntiCheat.SetIntConverted("MoKhoa" + indexSpaceShip + "1MayBay", unlocked);
        PvpUtil.SendUpdatePlayer("SetSpaceShipIsUnlocked");
    }

    public static void SetSpaceShipIsUnlockedNamNX(int indexSpaceShip, int unlocked)
    {

        //=1 la` da unlock
        //PlayerPrefs.SetInt("SpaceShipIsUnlocked_" + indexSpaceShip, unlocked);
        AntiCheat.SetIntConverted("MoKhoa" + indexSpaceShip + "1MayBay", unlocked);
    }

    public static int GetSpaceShipIsUnlocked(int indexSpaceShip)
    {
        if (indexSpaceShip == 1)
            return 1;
        //return PlayerPrefs.GetInt("SpaceShipIsUnlocked_" + indexSpaceShip);
        return AntiCheat.GetIntConverted("SpaceShipIsUnlocked_" + indexSpaceShip, "MoKhoa" + indexSpaceShip + "1MayBay", 0);

    }


    //------------------------------------Panel unlock SpaceShip------------------------------------------------------
    public static void SetWingIsUnlocked(int index, int unlocked)
    {
        //=1 la` da unlock
        AntiCheat.SetInt("UnlockWing_" + index, unlocked);
        PvpUtil.SendUpdatePlayer("SetSpaceShipIsUnlocked");
    }

    public static int GetWingIsUnlocked(int index)
    {
        return AntiCheat.GetInt("UnlockWing_" + index, 0);
    }

    //-------------------Lưu lại rank và level của spaceship
    public static void SetSpaceShipRank(AircraftTypeEnum type, Rank rank)
    {
        AntiCheat.SetInt("1maybayrank" + type + "capdonangcap", (int)rank);
    }

    public static Rank GetSpaceShipRank(AircraftTypeEnum type)
    {
        return (Rank)AntiCheat.GetInt("1maybayrank" + type + "capdonangcap", AircraftSheet.Get((int)type).begin_rank);
    }

    public static void SetSpaceShipLevel(AircraftTypeEnum type, int level)
    {
        AntiCheat.SetInt(type + "_Aicraft_Level", level);
    }

    public static int GetSpaceShipLevel(AircraftTypeEnum type)
    {
        return AntiCheat.GetInt(type + "_Aicraft_Level", 0);
    }

    //-------------------Lưu lại rank và level của wingman
    public static void SetWingmanRank(WingmanTypeEnum type, Rank rank)
    {
        AntiCheat.SetInt(type + "_Wingman_Rank", (int)rank);
    }

    public static Rank GetWingmanRank(WingmanTypeEnum type)
    {
        return (Rank)AntiCheat.GetInt(type + "_Wingman_Rank", WingmanSheet.Get((int)type).begin_rank);
    }

    public static void SetWingmanLevel(WingmanTypeEnum type, int level)
    {
        AntiCheat.SetInt(type + "_Wingman_Level", level);
    }

    public static int GetWingmanLevel(WingmanTypeEnum type)
    {
        return AntiCheat.GetInt(type + "_Wingman_Level", 0);
    }

    //-------------------Lưu lại rank và level của wing
    public static void SetWingRank(WingTypeEnum type, Rank rank)
    {
        AntiCheat.SetInt("WingRank_" + type, (int)rank);
    }

    public static Rank GetWingRank(WingTypeEnum type)
    {
        return (Rank)AntiCheat.GetInt("WingRank_" + type, WingSheet.Get((int)type).begin_rank);
    }

    public static void SetWingLevel(WingTypeEnum type, int level)
    {
        AntiCheat.SetInt("WingLevel_" + type, level);
    }

    public static int GetWingLevel(WingTypeEnum type)
    {
        return AntiCheat.GetInt("WingLevel_" + type, 0);
    }

    //-------------------Lưu lại space ship ma User chọn để mang vào chơi

    public static void SetSpaceShipUserSelected(int indexSpaceShipSelect)
    {
        //PlayerPrefs.SetInt("SpaceShipUserSelected", indexSpaceShipSelect);
        AntiCheat.SetIntConverted("MayBayMangVaoChoi", indexSpaceShipSelect);
    }

    public static int GetSpaceShipUserSelected()
    {
        //return PlayerPrefs.GetInt("SpaceShipUserSelected", 1);
        return AntiCheat.GetIntConverted("SpaceShipUserSelected", "MayBayMangVaoChoi", 1);
    }

    //-------------------Lưu lại space ship ma User chọn để mang vào chơi Extra Level

    public static void SetSpaceShipUserSelectedExtraLevel(int indexSpaceShipSelect)
    {
        AntiCheat.SetInt("MayBayMangVaoChoi_ExtraLevel", indexSpaceShipSelect);
    }

    public static int GetSpaceShipUserSelectedExtraLevel()
    {
        return AntiCheat.GetInt("MayBayMangVaoChoi_ExtraLevel", 1);
    }


    //-------------------Lưu lại wing ma User chọn để mang vào chơi
    public static void SetWingUserSelected(int index)
    {
        AntiCheat.SetInt("WingUserSelected", index);
    }

    public static int GetWingUserSelected()
    {
        return AntiCheat.GetInt("WingUserSelected", 0);
    }

    //-------------------Cache lai  space ship ma User select choi thu

    public static void SetSpaceShipUserTry(int indexSpaceShipTry)
    {
        //PlayerPrefs.SetInt("SpaceShipUserTry", indexSpaceShipTry);
        AntiCheat.SetIntConverted("NguoiChoiThuMayBayNao", indexSpaceShipTry);
    }


    //Bằng 0 là không có máy bay nào được thử
    public static int GetSpaceShipUserTry()
    {
        //return PlayerPrefs.GetInt("SpaceShipUserTry",0);
        return AntiCheat.GetIntConverted("SpaceShipUserTry", "NguoiChoiThuMayBayNao", 0);
    }

    //-------------------Cache lai  wing ma User select choi thu
    public static void SetWingUserTry(int index)
    {
        AntiCheat.SetInt("WingUserTry", index);
    }

    //Bằng 0 là không có wing nào được thử
    public static int GetWingUserTry()
    {
        return AntiCheat.GetInt("WingUserTry", 0);
    }

    //-------------------Cache lai  space ship ma User select choi Tournament

    public static void SetSpaceShipTournament(int indexSpaceShipTournament)
    {
        AntiCheat.SetIntConverted("SetSpaceShipTournament", indexSpaceShipTournament);
    }

    public static int GetSpaceShipTournament()
    {
        return AntiCheat.GetInt("SetSpaceShipTournament", 1);
    }

    //---------
    public static void SetWingTournament(int indexWing)
    {
        AntiCheat.SetIntConverted("SetWingTournament", indexWing);
    }

    public static int GetWingTournament()
    {
        return AntiCheat.GetInt("SetWingTournament", 0);
    }

    //------------------------------------WingMan------------------------------------------------------
    //1 Galting
    //2 Auto Galting
    //3 Laser
    //
    //-------------------Lưu lại WingMan Đã unlock hay chưa

    public static void SetWingManIsUnlocked(int indexWingMan, int unlocked)
    {

        //=1 la` da unlock
        //PlayerPrefs.SetInt("SetWingManIsUnlocked_" + indexWingMan, unlocked);
        AntiCheat.SetIntConverted("DaMoKhoaMayBayPhu" + indexWingMan + "HayChua", unlocked);
        PvpUtil.SendUpdatePlayer("SetWingManIsUnlocked");
    }

    public static void SetWingManIsUnlockedNamNX(int indexWingMan, int unlocked)
    {

        //=1 la` da unlock
        //PlayerPrefs.SetInt("SetWingManIsUnlocked_" + indexWingMan, unlocked);
        AntiCheat.SetIntConverted("DaMoKhoaMayBayPhu" + indexWingMan + "HayChua", unlocked);
    }

    public static int GetWingManIsUnlocked(int indexWingMan)
    {
        if (indexWingMan == 1)
            return 1;
        //return PlayerPrefs.GetInt("SetWingManIsUnlocked_" + indexWingMan);
        return AntiCheat.GetIntConverted("SetWingManIsUnlocked_" + indexWingMan, "DaMoKhoaMayBayPhu" + indexWingMan + "HayChua", 0);
    }


    //-------------------Lưu lại WingMan mà User chọn để mang vào chơi
    public static void SetWingManLeftUserSelected(int indexWingManSelect)
    {

        //PlayerPrefs.SetInt("SetWingManUserSelected", indexWingManSelect);
        AntiCheat.SetIntConverted("MayBayphuTraiNguoiChoiSuDung", indexWingManSelect);
    }

    public static int GetWingManLeftUserSelected()
    {
        //return PlayerPrefs.GetInt("SetWingManUserSelected", 1);
        return AntiCheat.GetIntConverted("SetWingManUserSelectedTrai", "MayBayphuTraiNguoiChoiSuDung", 0);
    }


    public static void SetWingManRightUserSelected(int indexWingManSelect)
    {

        //PlayerPrefs.SetInt("SetWingManUserSelected", indexWingManSelect);
        AntiCheat.SetIntConverted("MayBayphuPhaiNguoiChoiSuDung", indexWingManSelect);
    }

    public static int GetWingManRightUserSelected()
    {
        //return PlayerPrefs.GetInt("SetWingManUserSelected", 1);
        return AntiCheat.GetIntConverted("SetWingManUserSelectedPhai", "MayBayphuPhaiNguoiChoiSuDung", 0);
    }

    //-------------------Cache lai  tWingMan ma User select choi thu

    public static void SetWingManUserTry(int indextWingManTry)
    {

        AntiCheat.SetIntConverted("MayBayphuChonChoiThu", indextWingManTry);
    }

    public static int GetWingManUserTry()
    {
        return AntiCheat.GetIntConverted("SettWingManUserTry", "MayBayphuChonChoiThu", 0);
    }



    //-------------------Lưu lại WingMan mà User chọn để mang vào chơi
    public static void SetWingManLeftTournament(int indexWingManTournament)
    {

        AntiCheat.SetIntConverted("SetWingManLeftTournament", indexWingManTournament);
    }

    public static int GetWingManLeftTournament()
    {
        return AntiCheat.GetInt("SetWingManLeftTournament", 0);
    }


    public static void SetWingManRightTournament(int indexWingManTournament)
    {

        AntiCheat.SetIntConverted("SetWingManRightTournament", indexWingManTournament);
    }

    public static int GetWingManRightTournament()
    {
        return AntiCheat.GetInt("SetWingManRightTournament", 0);
    }


    //------------------------------------Secondary Weapon------------------------------------------------------
    //1 Burst shot
    //2 Chainsaw

    //-------------------Lưu lại SecondaryWeapon Đã unlock hay chưa
    public static void SetSecondaryWeaponIsUnlocked(int indexSecondaryWeapon, int unlocked)
    {
        //=1 la` da unlock
        //PlayerPrefs.SetInt("SetSecondaryWeaponIsUnlocked" + indexSecondaryWeapon, unlocked);
        AntiCheat.SetIntConverted("VuKhiThu2MoKhoa" + indexSecondaryWeapon + "HayChua", unlocked);
        PvpUtil.SendUpdatePlayer("SetSecondaryWeaponIsUnlocked");
    }

    public static void SetSecondaryWeaponIsUnlockedNamNX(int indexSecondaryWeapon, int unlocked)
    {
        //=1 la` da unlock
        //PlayerPrefs.SetInt("SetSecondaryWeaponIsUnlocked" + indexSecondaryWeapon, unlocked);
        AntiCheat.SetIntConverted("VuKhiThu2MoKhoa" + indexSecondaryWeapon + "HayChua", unlocked);
    }

    public static int GetSecondaryWeaponIsUnlocked(int indexSecondaryWeapon)
    {
        //return PlayerPrefs.GetInt("SetSecondaryWeaponIsUnlocked" + indexSecondaryWeapon);
        return AntiCheat.GetIntConverted("SetSecondaryWeaponIsUnlocked" + indexSecondaryWeapon, "VuKhiThu2MoKhoa" + indexSecondaryWeapon + "HayChua", 0);
    }
    //-------------------Lưu lại Secondary Weapon mà User chọn để mang vào chơi

    public static void SetSecondaryWeaponSelected(int indexSecondaryWeaponSelect)
    {
        //PlayerPrefs.SetInt("SetSecondaryWeaponSelected", indexSecondaryWeaponSelect);
        AntiCheat.SetIntConverted("VuKhiThu2DangSuDung", indexSecondaryWeaponSelect);
    }

    public static int GetSecondaryWeaponSelected()
    {
        //return PlayerPrefs.GetInt("SetSecondaryWeaponSelected", 0);
        return AntiCheat.GetIntConverted("SetSecondaryWeaponSelected", "VuKhiThu2DangSuDung", 0);
    }

    //-------------------Cache lai  tWingMan ma User select choi thu

    public static void SetSecondaryWeaponUserTry(int indextSecondaryWeaponTry)
    {
        //PlayerPrefs.SetInt("SettSecondaryWeaponUserTry", indextSecondaryWeaponTry);
        AntiCheat.SetIntConverted("ThuVuKhiThu2Nao", indextSecondaryWeaponTry);
    }

    public static int GetSecondaryWeaponUserTry()
    {
        //return PlayerPrefs.GetInt("SettSecondaryWeaponUserTry", 0);
        return AntiCheat.GetIntConverted("SettSecondaryWeaponUserTry", "ThuVuKhiThu2Nao", 0);
    }

    //------------------------------------------------------------------------------------------

    //-----------Cache lai so lan` da~ hien thi x3 gold o popup win trong 1 ngay

    public static void SetTotalShowX3Coin(int number)
    {
        //PlayerPrefs.SetInt("TotalShowX3Coin", number);
        AntiCheat.SetIntConverted("SoLanHienThiX3Vang", number);
    }

    public static int GetTotalShowX3Coin()
    {
        //return PlayerPrefs.GetInt("TotalShowX3Coin", 0);
        return AntiCheat.GetIntConverted("TotalShowX3Coin", "SoLanHienThiX3Vang", 0);
    }


    //-----------Cache lai so lan` da~ hien thi xem video hàng ngày trong 1 ngày

    public static void SetNumUseHourlyRewardOneDay(int numberUse)
    {
        //PlayerPrefs.SetInt("NumUseHourlyRewardOneDay", numberUse);
        AntiCheat.SetIntConverted("SoLanSuDungVideoHangNgay_MoiNgay", numberUse);
    }

    public static int GetNumUseHourlyRewardOneDay()
    {
        //return PlayerPrefs.GetInt("NumUseHourlyRewardOneDay",0);
        return AntiCheat.GetIntConverted("NumUseHourlyRewardOneDay", "SoLanSuDungVideoHangNgay_MoiNgay", 0);
    }


    //-----------Cache lại số lần đã hiển thị video End Game trong 1 ngày---------------------------------------------

    public static void SetNumUseVideoEndGameOneDay(int numberUse)
    {
        AntiCheat.SetIntConverted("SoLanDaSuDungVideoEndGameMoiNgay", numberUse);
    }

    public static int GetNumUseVideoEndGameOneDay()
    {
        return AntiCheat.GetInt("SoLanDaSuDungVideoEndGameMoiNgay", 0);
    }

    //--- random index để get quà trong data lần đầu tiên
    public static void SetIndexRandomVideoEndGameFirst(int index)
    {
        AntiCheat.SetIntConverted("ValueRandomVideoEndGameFirst", index);
    }

    public static int GetIndexRandomVideoEndGameFirst()
    {
        return AntiCheat.GetInt("ValueRandomVideoEndGameFirst", 1);
    }



    //-----------Cache lai ti le man` hinh` de scale popup


    public static void SetRatioScreen(float ratio)
    {
        PlayerPrefs.SetFloat("RatioScreen", ratio);
    }

    public static float GetRatioScreen()
    {

        return PlayerPrefs.GetFloat("RatioScreen", (float)Screen.height / (float)Screen.width);
    }


    //	-----------------------------Daily LogIn----------------------------------------------------------------------------------------

    ////status=1  :box chua duoc nhan qua  ; status=2  :box da san sang nhan qua  ; status=3  :box da nhan qua

    public static void SetStatusClaimDailyLogin(int indexDay, int status)
    {
        //PlayerPrefs.SetInt("StatusClaimDailyLogin_" + indexDay, status);
        AntiCheat.SetIntConverted("TrangThaiNhanQuaDangNhapMoiNgay" + indexDay, status);
    }

    public static int GetStatusClaimDailyLogin(int indexDay)
    {
        //
        //return PlayerPrefs.GetInt("StatusClaimDailyLogin_" + indexDay, 1);
        return AntiCheat.GetIntConverted("StatusClaimDailyLogin_" + indexDay, "TrangThaiNhanQuaDangNhapMoiNgay" + indexDay, 1);
    }


    //--- last day login dùng ở daily login
    public static void SetLastDayLogin(int day)
    {
        //PlayerPrefs.SetInt("LastDayLogin_DailyLogin", day);
        AntiCheat.SetIntConverted("NgayCuoiCungDangNhap", day);
    }

    public static int GetLastDayLogin()
    {
        //return PlayerPrefs.GetInt("LastDayLogin_DailyLogin",0);
        return AntiCheat.GetIntConverted("LastDayLogin_DailyLogin", "NgayCuoiCungDangNhap", 0);
    }

    //--- tổng số ngày login
    public static void SetTotalDayLogin(int numDay)
    {
        //PlayerPrefs.SetInt("TotalDayLogin", numDay);
        AntiCheat.SetIntConverted("TongSoNgayDangNhap", numDay);
    }

    public static int GetTotalDayLogin()
    {
        //return PlayerPrefs.GetInt("TotalDayLogin",0);
        return AntiCheat.GetIntConverted("TotalDayLogin", "TongSoNgayDangNhap", 0);
    }


    //--- last day login : =1 la` da~ nhan het qua`
    public static void SetClaimedAllGiftDailyLogin(int trueFalse)
    {
        //PlayerPrefs.SetInt("ClaimedAllGiftDailyLogin", trueFalse);
        AntiCheat.SetIntConverted("DaNhanHetQua7Ngay", trueFalse);
    }

    public static int GetClaimedAllGiftDailyLogin()
    {

        //return PlayerPrefs.GetInt("ClaimedAllGiftDailyLogin");
        return AntiCheat.GetIntConverted("ClaimedAllGiftDailyLogin", "DaNhanHetQua7Ngay", 0);
    }


    //	-----------------------------Starter Pack----------------------------------------------------------------------------------------


    public static void SetPurchasedStarterPack(int purchased)
    {
        //PlayerPrefs.SetInt("PurchasedStarterPack", purchased);
        AntiCheat.SetIntConverted("DaMuaGoiKhoiDau", purchased);
    }

    public static int GetPurchasedStarterPack()
    {
        //return PlayerPrefs.GetInt("PurchasedStarterPack");
        return AntiCheat.GetIntConverted("PurchasedStarterPack", "DaMuaGoiKhoiDau", 0);
    }

    //	-----------------------------Premium Pack----------------------------------------------------------------------------------------

    // vì gói này có thể mua lại sau 7 ngày nên biến này chỉ có tác dụng để dùng cho việc set  cho user buy pack bằng xem ads
    //public static void SetPurchasedPremiumPack(int purchased)
    //{
    //    AntiCheat.SetIntConverted("DaMuaGoiPreHayChua", purchased);
    //}

    //public static int GetPurchasedPremiumPack()
    //{
    //    return AntiCheat.GetIntConverted("DaMuaGoiPreHayChuaOld", "DaMuaGoiPreHayChua", 0);
    //}


    //	-----------------------------VIP Pack----------------------------------------------------------------------------------------


    public static void SetPurchasedVIPPack(int purchased)
    {
        //PlayerPrefs.SetInt("PurchasedVIPPack", purchased);
        AntiCheat.SetIntConverted("DaMuaGoiVipThanhCong", purchased);
    }

    public static int GetPurchasedVIPPack()
    {
        //return PlayerPrefs.GetInt("PurchasedVIPPack");
        return AntiCheat.GetIntConverted("PurchasedVIPPack", "DaMuaGoiVipThanhCong", 0);
    }


    //	-----------------------------sỐ lần user đã xem video ads để hiện thị lên cái---------------------------------------------------

    public static void SetNumberWatchAdsToGetPack(int num)
    {
        //PlayerPrefs.SetInt("PurchasedVIPPack", purchased);
        AntiCheat.SetIntConverted("SoLanDaXemAdsDeNhanGoiKhuyenMai", num);
    }

    public static int GetNumberWatchAdsToGetPack()
    {
        //return PlayerPrefs.GetInt("PurchasedVIPPack");



        return AntiCheat.GetIntConverted("SoLanDaXemAdsDeNhanGoiKhuyenMaiOld", "SoLanDaXemAdsDeNhanGoiKhuyenMai", 0);
    }

    //	-----------------------------sỐ lần tối đa user được xem ads để mua pack còn lại mỗi ngày---------------------------------------------------

    public static void SetRemainWatchAdsToGetPackToday(int num)
    {
        //PlayerPrefs.SetInt("PurchasedVIPPack", purchased);
        AntiCheat.SetIntConverted("SoLanDuocXemAdNhanGoiKhuyenMai1Ngay", num);
    }

    public static int GetRemainWatchAdsToGetPackToday()
    {
        //return PlayerPrefs.GetInt("PurchasedVIPPack");
        return AntiCheat.GetIntConverted("SoLanDuocXemAdNhanGoiKhuyenMai1NgayOld", "SoLanDuocXemAdNhanGoiKhuyenMai1Ngay", 5);
    }

    //	-----------------------------cache lại giá trị của server gửi về để change độ khó mỗi level ---------------------------------------------------

    public static void SetValueLevelModifiServer(int level, float coeff)
    {
        PlayerPrefs.SetFloat("DoKhothaydoimoileveltusever" + level, coeff);
    }

    public static float GetValueLevelModifiServer(int level)
    {
        //return PlayerPrefs.GetFloat("DoKhothaydoimoileveltusever" + level, 1f);
        if (PlayerPrefs.HasKey("DoKhothaydoimoileveltusever" + level))
        {
            return PlayerPrefs.GetFloat("DoKhothaydoimoileveltusever" + level, 1f);
        }
        else
        {
            return 1f;
        }
    }

    const string KEY_VALUE_BUTTONSKILL = "Value_Position_ButtonSkill";
    public static int ValueColtrolButtonSkill
    {
        get
        {
            return PlayerPrefs.GetInt(KEY_VALUE_BUTTONSKILL, 1);
        }
        set
        {
            PlayerPrefs.SetInt(KEY_VALUE_BUTTONSKILL, value);
        }
    }


    // vì gói combo của event black friday chỉ được mua 1 lần 
    const string KEY_BUYED_STANDARD_COMBO = "KEY_BUYED_STANDARD_COMBO";
    public static int ValueBuyedStandardCombo
    {
        get
        {
            return PlayerPrefs.GetInt(KEY_BUYED_STANDARD_COMBO, 0);
        }
        set
        {
            PlayerPrefs.SetInt(KEY_BUYED_STANDARD_COMBO, value);
        }
    }

    const string KEY_BUYED_MEGA_COMBO = "KEY_BUYED_MEGA_COMBO";
    public static int ValueBuyedMegaCombo
    {
        get
        {
            return PlayerPrefs.GetInt(KEY_BUYED_MEGA_COMBO, 0);
        }
        set
        {
            PlayerPrefs.SetInt(KEY_BUYED_MEGA_COMBO, value);
        }
    }
}