﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CacheFireBase
{

    public const string FALCON_ANDROID_SPEND = "falcon_android_spend_";
    public const string FALCON_ANDROID_NOT_SPEND = "falcon_android_not_spend_";
    public const string FALCON_AB_VIP_PACKAGE = "falcon_ab_price_";
    public const string FALCON_AB_VIP_PACKAGE_20USD = "20usd_";
    public const string FALCON_AB_VIP_PACKAGE_40USD = "40usd_";
    public const string FALCON_AB_VIP_PACKAGE_PREDICTION = "prediction_";


    public const string FALCON_URL_PVP = "falcon_url_pvp_";
    public const string FALCON_CAN_PLAY_PVP = "falcon_can_play_pvp_";
    public const string FALCON_LEVEL_UNLOCK_PVP = "falcon_level_unlock_pvp_";
    public const string FALCON_LEVEL_UNLOCK_PLAYERINFO = "falcon_level_unlock_playerinfo_";

    public const string FALCON_URL_PVP_DDOS = "falcon_url_pvp_servers_";
    public const string FALCON_URL_TXT_INDEX_DDOS = "falcon_url_txt_index_ddos_";

    public const string FALCON_URL_ASSET_BUNDLE = "falcon_url_asset_bundle_";

    public const string FALCON_URL_OFFERWALL = "falcon_url_offerwall_";
    public const string FALCON_LEVEL_UNLOCK_OFFERWALL = "falcon_level_unlock_offerwall_";
    public const string FALCON_VERSION_UNLOCK_OFFERWALL = "falcon_version_unlock_offerwall_";
    public const string FALCON_AB_STARTER_PACKAGE = "falcon_starter_pack_price_";
    public const string FALCON_AB_STARTER_PACKAGE_1USD = "1usd_";
    public const string FALCON_AB_STARTER_PACKAGE_2USD = "2usd_";
    public const string FALCON_AB_STARTER_PACKAGE_PREDICTION = "prediction_";
    public const string FALCON_URL_GIFTCODE = "falcon_url_giftcode_";
    public const string FALCON_MAX_GEM_DROP_1DAY = "falcon_max_gem_drop_1day_";
    public const string FALCON_MAX_CARD_DROP_1DAY = "falcon_max_card_drop_1day_";
    public const string FALCON_NUMLEVEL_WHEN_GET_POWERUP = "falcon_numLevel_When_Get_PowerUp_";
    public const string FALCON_TEST_14LEVEL_FIRST = "falcon_test_14level_first_";
    public const string FALCON_TEST_LEVEL_BOSS = "falcon_test_level_boss_";
    public const string FALCON_TEST_BUY_GOLD_BY_GEM = "falcon_test_buy_gold_by_gem_";
    public const string FALCON_TEST_VIDEO_ENDLEVEL = "falcon_test_video_endlevel_";
    public const string FALCON_USE_RATE_IN_QUEST = "falcon_use_rate_in_quest_";
    public const string FALCON_TEST_USE_LEVEL_UNLOCK_ARICRAFT = "falcon_test_unlock_aricraft_";


    public const string FALCON_DAY_START_EVENT = "_falcon_day_start_event_summer2019";
    public const string FALCON_DAY_FINISH_EVENT = "_falcon_day_finish_event_summer2019";

    public const string FALCON_IS_REVIEW_VERSION = "falcon_is_review_version_china_";
    public const string FALCON_DAY_FINISH_PROMOTE_EVENT = "falcon_day_finish_promote_event_build228_";

    public const string TEST_SHOW_ROYAL_PACK = "falcon_test_show_royal_pack_";

    public const string ADMOB_HIGHER_PRICE = "falcon_admob_higher_price_";



    public static string GetSeverPvp
    {
        get
        {
            return PlayerPrefs.GetString(FALCON_URL_PVP_DDOS, PvpUtil.LIST_SERVERS_URL_PVP_DDOS);
        }
        set
        {
            PlayerPrefs.SetString(FALCON_URL_PVP_DDOS, value);
        }
    }

    //0;1;2 : 0,1,2 bi chet
    public static string GetUrlTxtDdos
    {
        get
        {
            return PlayerPrefs.GetString(FALCON_URL_TXT_INDEX_DDOS, PvpUtil.URL_TXT_INDEX_DDOS);
        }
        set
        {
            PlayerPrefs.SetString(FALCON_URL_TXT_INDEX_DDOS, value);
        }
    }

    public static string GetUrlGiftCode
    {
        get
        {
            return PlayerPrefs.GetString(FALCON_URL_GIFTCODE, GameContext.FALCON_URL_GIFTCODE_DF);
        }
        set
        {
            PlayerPrefs.SetString(FALCON_URL_GIFTCODE, value);
        }
    }

    public static string GetUrlAssetBundle
    {
        get
        {
            return PlayerPrefs.GetString(FALCON_URL_ASSET_BUNDLE, GameContext.FALCON_URL_ASSET_BUNDLE_DF);
        }
        set
        {
            PlayerPrefs.SetString(FALCON_URL_ASSET_BUNDLE, value);
        }
    }



    public static int GetLevelUnlockOfferWall
    {
        get
        {
            return PlayerPrefs.GetInt(FALCON_LEVEL_UNLOCK_OFFERWALL, GameContext.LEVEL_UNLOCK_OFFERWALL_DF);
        }
        set
        {
            PlayerPrefs.SetInt(FALCON_LEVEL_UNLOCK_OFFERWALL, value);
        }
    }

    public static int GetVersionUnlockOfferWall
    {
        get
        {
            return PlayerPrefs.GetInt(FALCON_VERSION_UNLOCK_OFFERWALL, 100);
        }
        set
        {
            PlayerPrefs.SetInt(FALCON_VERSION_UNLOCK_OFFERWALL, value);
        }
    }


    public static int GetMaxGemDropOneDay
    {
        get
        {
            return PlayerPrefs.GetInt(FALCON_MAX_GEM_DROP_1DAY, GameContext.MAX_GEM_DROP_ONEDAY_DF);
        }
        set
        {
            PlayerPrefs.SetInt(FALCON_MAX_GEM_DROP_1DAY, value);
        }
    }

    public static int GetMaxCardDropOneDay
    {
        get
        {
            return PlayerPrefs.GetInt(FALCON_MAX_CARD_DROP_1DAY, GameContext.MAX_CARD_DROP_ONEDAY_DF);
        }
        set
        {
            PlayerPrefs.SetInt(FALCON_MAX_CARD_DROP_1DAY, value);
        }
    }

    public static int GetNumLevelAdd_When_GetPowerUp
    {
        get
        {
            return PlayerPrefs.GetInt(FALCON_NUMLEVEL_WHEN_GET_POWERUP, GameContext.NUM_LEVEL_WHEN_GET_POWERUP);
        }
        set
        {
            PlayerPrefs.SetInt(FALCON_NUMLEVEL_WHEN_GET_POWERUP, value);
        }
    }

    public static string GetType14LevelFirst
    {
        get
        {
            return PlayerPrefs.GetString(FALCON_TEST_14LEVEL_FIRST, GameContext.TYPE_TEST_14LEVEL_FIRST_DF);
        }
        set
        {
            PlayerPrefs.SetString(FALCON_TEST_14LEVEL_FIRST, value);
        }
    }

    public static string GetTypeTestLevelBoss
    {
        get
        {
            return PlayerPrefs.GetString(FALCON_TEST_LEVEL_BOSS, GameContext.TYPE_TEST_LEVEL_BOSS_DF);
        }
        set
        {
            PlayerPrefs.SetString(FALCON_TEST_LEVEL_BOSS, value);
        }
    }

    //1- mua bằng inapp , 2- mua bằng gem
    public static int GetTypeBuyGold
    {
        get
        {
            return PlayerPrefs.GetInt(FALCON_TEST_BUY_GOLD_BY_GEM, 1);
        }
        set
        {
            PlayerPrefs.SetInt(FALCON_TEST_BUY_GOLD_BY_GEM, value);
        }
    }


    //public static string GetDayFinishEvent
    //{
    //    get
    //    {
    //        return PlayerPrefs.GetString(FALCON_DAY_FINISH_EVENT, GameContext.FALCON_DAY_FINISH_EVENT_DF);
    //    }
    //    set
    //    {
    //        PlayerPrefs.SetString(FALCON_DAY_FINISH_EVENT, value);
    //    }
    //}



    public static int GetIsReviewVersion
    {
        get
        {
            return PlayerPrefs.GetInt(FALCON_IS_REVIEW_VERSION, 0);
        }
        set
        {
            PlayerPrefs.SetInt(FALCON_IS_REVIEW_VERSION, value);
        }
    }

    public static string GetAdmobHigherPriceIDs
    {
        get
        {
            return PlayerPrefs.GetString(ADMOB_HIGHER_PRICE, GameContext.ADMOB_HIGHER_PRICE_DEFAULT_ID);
        }
        set
        {
            PlayerPrefs.SetString(ADMOB_HIGHER_PRICE, value);
        }
    }


    public static string GetDayStartEvent
    {
        get
        {
            return PlayerPrefs.GetString(FALCON_DAY_START_EVENT, GameContext.FALCON_DAY_START_EVENT_DF);
        }
        set
        {
            PlayerPrefs.SetString(FALCON_DAY_START_EVENT, value);
        }
    }
    public static string GetDayFinishEvent
    {
        get
        {
            return PlayerPrefs.GetString(FALCON_DAY_FINISH_EVENT, GameContext.FALCON_DAY_FINISH_EVENT_DF);
        }
        set
        {
            PlayerPrefs.SetString(FALCON_DAY_FINISH_EVENT, value);
        }
    }


    public static string GetDayFinishPromoteEvent
    {
        get
        {
            return PlayerPrefs.GetString(FALCON_DAY_FINISH_PROMOTE_EVENT, GameContext.FALCON_DAY_FINISH_PROMOTE_EVENT_DF);
        }
        set
        {
            PlayerPrefs.SetString(FALCON_DAY_FINISH_PROMOTE_EVENT, value);
        }
    }

    public static int GetTestShowVideoEndLevel
    {
        get
        {
            return PlayerPrefs.GetInt(FALCON_TEST_VIDEO_ENDLEVEL, 1);
        }
        set
        {
            PlayerPrefs.SetInt(FALCON_TEST_VIDEO_ENDLEVEL, value);
        }
    }

    //=1 là có show, = 0 là tắt royalpack
    public static int GetTestShowRoyalPack
    {
        get
        {
            return PlayerPrefs.GetInt(TEST_SHOW_ROYAL_PACK, 1);
        }
        set
        {
            PlayerPrefs.SetInt(TEST_SHOW_ROYAL_PACK, value);
        }
    }


    //=1 là sẽ sử dụng rate và share facebook trong daily quest
    public static int GetUseRateInQuest
    {
        get
        {
            return PlayerPrefs.GetInt(FALCON_USE_RATE_IN_QUEST, 1);
        }
        set
        {
            PlayerPrefs.SetInt(FALCON_USE_RATE_IN_QUEST, value);
        }
    }

    // =1 là có sử dụng điều kiện unlock máy bay là phải pass qua level nào đó
    public static int GetUseLevelUnlockAircraft
    {
        get
        {
            return PlayerPrefs.GetInt(FALCON_TEST_USE_LEVEL_UNLOCK_ARICRAFT, 1);
        }
        set
        {
            PlayerPrefs.SetInt(FALCON_TEST_USE_LEVEL_UNLOCK_ARICRAFT, value);
        }
    }
}
