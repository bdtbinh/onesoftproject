﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour 
{
	public static T ParserDataFromTxtFile<T>(string from_file_txt) {
		TextAsset txt = Resources.Load (from_file_txt, typeof(TextAsset)) as TextAsset;

		#if UNITY_WP8
		T result = Pathfinding.Serialization.JsonFx.JsonReader.Deserialize<T>(txt.text);
		#else
		T result = JsonFx.Json.JsonReader.Deserialize<T>(txt.text);
		#endif
		return result;
	}

    //--------------------------------- BATTLE HUNGXD -------------------------------------------------------

    public static string ExportDataString<T>(T inputDataObj) 
    {
#if UNITY_WP8
		return Pathfinding.Serialization.JsonFx.JsonWriter.Serialize(inputDataObj); 
#else
        return JsonFx.Json.JsonWriter.Serialize(inputDataObj);
#endif

    }

    public static T ParserDataFromJsonString<T>(string json)   
    {
#if UNITY_WP8
		T result = Pathfinding.Serialization.JsonFx.JsonReader.Deserialize<T>(json);
#else
        T result = JsonFx.Json.JsonReader.Deserialize<T>(json);
#endif


        return result;
    }
}