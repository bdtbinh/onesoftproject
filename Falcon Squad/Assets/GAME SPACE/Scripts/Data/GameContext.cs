﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EasyMobile;
using DG.Tweening;
using UnityEngine.Purchasing;
using TCore;
using FalconSDK;

public class GameContext
{
  
    public const bool IS_CHINA_VERSION = false;

    public const string ADMOB_HIGHER_PRICE_DEFAULT_ID = "";

    //Sau này khi có thêm event thì viết thêm xuống dưới chứ k xóa dòng này đi.
    public const string EVENT_HALLOWEEN_2018 = "Halloween_2018";
    public const string EVENT_XMAS_2018 = "XMas_2018";
    public const string EVENT_SUMMER_HOLIDAY_2019 = "Summer_Holiday_2019";


    public const string FALCON_DAY_START_EVENT_DF = "4/7/2019";
    public const string FALCON_DAY_FINISH_EVENT_DF = "5/11/2019";

    public const string FALCON_DAY_FINISH_PROMOTE_EVENT_DF = "3/18/2019";


    public const string FALCON_URL_GIFTCODE_DF = "http://falconsquad.net:8686/falcon-backend";
    public const string FALCON_URL_ASSET_BUNDLE_DF = "https://storage.googleapis.com/data.falconsquad.net/";
    public const string FALCON_URL_OFFERWALL_DF = "http://35.187.253.118:9090/gamebackend-restapi/v1/rest";


    public const int LEVEL_UNLOCK_OFFERWALL_DF = 100;
    public const int VERSION_OFFERWALL_NOW = 2;

    public const int numLifeInitAircraftFirst = 3;
    public const int numLifeInitAircraftBackup = 1;

    public static int numChangeSpeedPlayer = 1;

    public static int numItemPowerGet = 0;
    public static int numItemPowerDrop = 0;
    //
    public const int MAX_GEM_DROP_ONEDAY_DF = 20;
    public const int MAX_CARD_DROP_ONEDAY_DF = 10;
    public const int TOTAL_LEVEL = 98;

    public const int TOTAL_AIRCRAFT = 10;
    public const int TOTAL_WINGMAN = 6;
    public const int TOTAL_WING = 3;



    public const int LEVEL_UNLOCK_ENDLESS_MODE = 6;
    public const int NUM_LEVEL_WHEN_GET_POWERUP = 3;

    public const int CALLING_BACKUP_TIMES = 1;

    public const string TYPE_TEST_14LEVEL_FIRST_DF = "b";
    public const string TYPE_TEST_LEVEL_BOSS_DF = "a";

    //Thời gian hết hạn của items trong pack (tính bằng ngày)
    public const int EXPIRED_TIME_PREMIUM_PACK = 7;
    //

    #region--- tra thuong--------------------

    public enum TypeItemInPopupItemReward
    {
        unknown,
        Gold,
        Gem,
        PowerUp,
        EMP,
        Life,
        AircraftGeneralCard,
        Aircraft1Card,
        Aircraft2Card,
        Aircraft3Card,
        Aircraft6Card,
        Aircraft7Card,
        Aircraft8Card,
        Aircraft9Card,
        DroneGeneralCard,
        Drone1Card,
        Drone2Card,
        Drone3Card,
        Drone4Card,
        Drone5Card,
        Drone6Card,
        WingGeneralCard,
        Wing1Card,
        Wing2Card,
        Wing3Card,
        Energy,
        Aircraft10Card,
    }

    public static TypeItemInPopupItemReward typeItemPopupItemReward;
    public static int numItemPopupItemReward;

    //displayImmediately là hiển thị quà lên text ngay khi được nhận thưởng

    public static void AddGiftToPlayer(string giftType, int numGift, bool displayImmediately, string why, string where = "")
    {
        numItemPopupItemReward = numGift;

        switch (giftType)
        {
            case "Gold":
                if (displayImmediately)
                {
                    if (PanelCoinGem.Instance != null)
                    {
                        PanelCoinGem.Instance.AddCoinTop(numGift, why);
                    }
                }
                else
                {
                    CacheGame.SetTotalCoin(CacheGame.GetTotalCoin() + numGift, why, "");
                    FirebaseLogSpaceWar.LogGoldIn(numGift, why, "");
                }
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = TypeItemInPopupItemReward.Gold;
                break;
            case "Gem":
                if (displayImmediately)
                {
                    if (PanelCoinGem.Instance != null)
                    {
                        PanelCoinGem.Instance.AddGemTop(numGift, why);
                    }
                }
                else
                {
                    CacheGame.SetTotalGem(CacheGame.GetTotalGem() + numGift, why, "");
                    //FirebaseLogSpaceWar.LogGemIn(numGift, why, "");
                }
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = TypeItemInPopupItemReward.Gem;
                break;
            case "Energy":
                if (displayImmediately)
                {
                    if (PanelCoinGem.Instance != null)
                    {
                        PanelCoinGem.Instance.AddEnergyTop(numGift, why);
                    }
                }
                else
                {
                    CacheGame.SetTotalEnergy(CacheGame.GetTotalEnergy() + numGift, why, "");
                    //FirebaseLogSpaceWar.LogGemIn(numGift, why, "");
                }
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = TypeItemInPopupItemReward.Energy;
                Debug.LogError("GameContext: " + typeItemPopupItemReward);
                break;
            case "PowerUp":
                CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + numGift);
                SoundManager.PlayClickButton();
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = TypeItemInPopupItemReward.PowerUp;
                break;
            case "ActiveSkill":
                CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + numGift);
                SoundManager.PlayClickButton();
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = TypeItemInPopupItemReward.EMP;
                break;
            case "Life":
                CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + numGift);
                SoundManager.PlayClickButton();
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + numGift + " " + giftType;
                typeItemPopupItemReward = TypeItemInPopupItemReward.Life;
                break;

            case "AircraftGeneralCard":
                MinhCacheGame.SetSpaceShipGeneralCards(MinhCacheGame.GetSpaceShipGeneralCards() + numGift, why, "");
                typeItemPopupItemReward = TypeItemInPopupItemReward.AircraftGeneralCard;
                break;
            case "Aircraft1Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.BataFD, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD) + numGift, why, "", AircraftTypeEnum.BataFD.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Aircraft1Card;
                break;

            case "Aircraft2Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.SkyWraith, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith) + numGift, why, "", AircraftTypeEnum.SkyWraith.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Aircraft2Card;
                break;

            case "Aircraft3Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.FuryOfAres, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres) + numGift, why, "", AircraftTypeEnum.FuryOfAres.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Aircraft3Card;
                break;

            case "Aircraft6Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.MacBird, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird) + numGift, why, "", AircraftTypeEnum.MacBird.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Aircraft6Card;
                break;

            case "Aircraft7Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.TwilightX, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX) + numGift, why, "", AircraftTypeEnum.TwilightX.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Aircraft7Card;
                break;

            case "Aircraft8Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + numGift, why, "", AircraftTypeEnum.StarBomb.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Aircraft8Card;
                break;

            case "Aircraft9Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.IceShard, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.IceShard) + numGift, why, "", AircraftTypeEnum.IceShard.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Aircraft9Card;
                break;
            case "Aircraft10Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.ThunderBolt, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.ThunderBolt) + numGift, why, "", AircraftTypeEnum.ThunderBolt.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Aircraft10Card;
                break;
            case "DroneGeneralCard":
                MinhCacheGame.SetWingmanGeneralCards(MinhCacheGame.GetWingmanGeneralCards() + numGift, why, "");
                typeItemPopupItemReward = TypeItemInPopupItemReward.DroneGeneralCard;
                break;
            case "Drone1Card":
                MinhCacheGame.SetWingmanCards(WingmanTypeEnum.GatlingGun, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.GatlingGun) + numGift, why, "", WingmanTypeEnum.GatlingGun.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Drone1Card;
                break;
            case "Drone2Card":
                MinhCacheGame.SetWingmanCards(WingmanTypeEnum.AutoGatlingGun, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.AutoGatlingGun) + numGift, why, "", WingmanTypeEnum.AutoGatlingGun.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Drone2Card;
                break;
            case "Drone3Card":
                MinhCacheGame.SetWingmanCards(WingmanTypeEnum.Lazer, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Lazer) + numGift, why, "", WingmanTypeEnum.Lazer.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Drone3Card;
                break;
            case "Drone4Card":
                MinhCacheGame.SetWingmanCards(WingmanTypeEnum.DoubleGalting, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.DoubleGalting) + numGift, why, "", WingmanTypeEnum.DoubleGalting.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Drone4Card;
                break;
            case "Drone5Card":
                MinhCacheGame.SetWingmanCards(WingmanTypeEnum.HomingMissile, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile) + numGift, why, "", WingmanTypeEnum.HomingMissile.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Drone5Card;
                break;

            case "Drone6Card":
                MinhCacheGame.SetWingmanCards(WingmanTypeEnum.Splasher, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Splasher) + numGift, why, "", WingmanTypeEnum.Splasher.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Drone6Card;
                break;

            case "WingGeneralCard":
                MinhCacheGame.SetWingGeneralCards(MinhCacheGame.GetWingGeneralCards() + numGift, why, "");
                typeItemPopupItemReward = TypeItemInPopupItemReward.WingGeneralCard;
                break;

            case "Wing1Card":
                MinhCacheGame.SetWingCards(WingTypeEnum.WingOfJustice, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfJustice) + numGift, why, "", WingTypeEnum.WingOfJustice.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Wing1Card;
                break;

            case "Wing2Card":
                MinhCacheGame.SetWingCards(WingTypeEnum.WingOfRedemption, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfRedemption) + numGift, why, "", WingTypeEnum.WingOfRedemption.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Wing2Card;
                break;

            case "Wing3Card":
                MinhCacheGame.SetWingCards(WingTypeEnum.WingOfResolution, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfResolution) + numGift, why, "", WingTypeEnum.WingOfResolution.ToString());
                typeItemPopupItemReward = TypeItemInPopupItemReward.Wing3Card;
                break;

            case "BoxCard1":
                GetCardInBox.GetCardInBoxByRate(why, "BoxCard1");
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
            case "BoxCard2":
                GetCardInBox.GetCardInBoxByRate(why, "BoxCard2");
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
            case "BoxCard3":
                GetCardInBox.GetCardInBoxByRate(why, "BoxCard3");
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
            case "BoxCard4":
                GetCardInBox.GetCardInBoxByRate(why, "BoxCard4");
                //listTextGiftToast = listTextGiftToast + "" + Environment.NewLine + "+" + GetCardInBox.textCardShowToast;
                typeItemPopupItemReward = GetCardInBox.typeItemPopupItemReward;
                numItemPopupItemReward = GetCardInBox.numItemPopupItemReward;
                break;
        }
    }

    #endregion

    #region--- get string name Gift Clear Reward--------------------

    public static string getNameSpriteGift(string typeGift)
    {
        switch (typeGift)
        {
            case "Gold":
                return "icon_coin";
                break;
            case "Gem":
                return "icon_daily_quest_gem";
                break;
            case "PowerUp":
                return "icon_dailyquest_powerup";
                break;
            case "ActiveSkill":
                return "icon_dailyquest_time";
                break;
            case "Life":
                return "icon_dailyquest_life";
                break;
            case "AircraftGeneralCard":
                return "card_A0";
                break;
            case "Aircraft1Card":
                return "card_A1";
                break;
            case "Aircraft2Card":
                return "card_A2";
                break;
            case "Aircraft3Card":
                return "card_A3";
                break;
            case "Aircraft6Card":
                return "card_A6";
                break;
            case "Aircraft7Card":
                return "card_A7";
                break;
            case "Aircraft8Card":
                return "card_A8";
                break;
            case "Aircraft9Card":
                return "card_A9";
                break;
            case "Aircraft10Card":
                return "card_A10";
                break;
            case "WingmanGeneralCard":
                return "card_D0";
                break;
            case "Wingman1Card":
                return "card_D1";
                break;
            case "Wingman2Card":
                return "card_D2";
                break;
            case "Wingman3Card":
                return "card_D3";
                break;
            case "Wingman4Card":
                return "card_D4";
                break;
            case "Wingman5Card":
                return "card_D5";
                break;
            default:
                return " ";
                break;
        }


    }
    #endregion

    public static bool isInGameCampaign = false;
    //
    public static bool isLoadedHome = false;
    //
    public static bool serverX2Purchase = false;
    public static bool serverSaleVipPack = false;


    public const string DIFFICULT_NOMAL = "Normal";
    public const string DIFFICULT_HARD = "Hard";
    public const string DIFFICULT_HELL = "Hell";
    //
    public const string GLOW_BTN_UPGRADE_EVENT = "GlowBtnUpgradeEvent";

    /// <summary>
    public static bool showPopupDailyLogin = true;

    // dùng để kiểm tra mỗi ngày chỉ show popup DailyLogin một lần
    public static bool showPopupNotifiFinishEvent = true;
    
    public static bool showPackInHomeScene = true;
    //
    public static bool showChooseEndLessMode = false;
    //
    public static int numItemPowerUpUse = 0;
    public static int numUsingSkill = 0;            //Số lần sử dụng skill trong 1 level
    public static int numLifeUpUse = 0;
    public static bool readyClickShowPopup = false;

    //Check xem level extra hay level thường:
    public enum LevelCampaignMode
    {
        Normal,
        Extra
    }

    public static LevelCampaignMode levelCampaignMode = LevelCampaignMode.Normal;

    //--
    //Thử wingman trái hay phải:
    public enum TypeTryWingman
    {
        Left,
        Right
    }
    public static TypeTryWingman typeTryWingman = TypeTryWingman.Left;

    //Check xem vào select level từ scene nào để show bar list item level
    public enum TypeShowBarFromScene
    {
        Main,
        Home
    }

    public static TypeShowBarFromScene typeShowBarFromScene = TypeShowBarFromScene.Home;


    //Check data để show popup khi quay lại từ try máy bay
    public enum TypeOpenSceneFromTry
    {
        SelectLevel,
        Home,
        NotShow,
        Tournament
    }

    public static TypeOpenSceneFromTry typeOpenSceneFromTry = TypeOpenSceneFromTry.NotShow;

    ////Check để show popup select item từ:
    //public enum TypeOpenSelectItemFrom
    //{
    //    None,
    //    TouchToChangeButton,
    //    EndlessButton,
    //    LevelButton,
    //    CoOpButton
    //}

    //public static TypeOpenSelectItemFrom typeOpenSelectItemFrom = TypeOpenSelectItemFrom.None;


    public enum TypeOpenPopupFromTry
    {
        Aircraft,
        Wingman,
        Wing

    }

    public static TypeOpenPopupFromTry typeOpenPopupFromTry = TypeOpenPopupFromTry.Aircraft;

    //--check data Để show popup select level khi vào scene select level

    public enum TypeLoadPopupSelectCampaign
    {
        Main_Click_Next,
        Main_Click_Retry,
        NotShow
    }

    public static TypeLoadPopupSelectCampaign typeLoadPopupSelectCampaign = TypeLoadPopupSelectCampaign.NotShow;

    //--check data Để show popup select level khi vào scene select Home
    public enum TypeLoadPopupSelectEndless
    {
        Main_Click_Retry,
        NotShow
    }

    public static TypeLoadPopupSelectEndless typeLoadPopupSelectEndless = TypeLoadPopupSelectEndless.NotShow;

    //--check data Để show popup select level khi vào scene select Home
    public enum TypeLoadPopupSelectEvent
    {
        Main_Click_Retry,
        NotShow
    }

    public static TypeLoadPopupSelectEvent typeLoadPopupSelectEvent = TypeLoadPopupSelectEvent.NotShow;


    //--check data mode chơi game

    public enum ModeGamePlay
    {
        Campaign,
        EndLess,
        pvp,
        TryPlane,
        Events,
        Tournament,
        PVP2vs2
    }

    public static ModeGamePlay modeGamePlay = ModeGamePlay.Campaign;


    //Kiểm tra ID quảng cáo admob có nhiều tiền không:
    public static bool IsAdmobHigherPrice()
    {
#if UNITY_ANDROID 
        if(CacheFireBase.GetAdmobHigherPriceIDs == "")
        {
            return false;
        }

        if (MediationInfo.GetAvailableAdsInformation() == null || string.IsNullOrEmpty( MediationInfo.GetAvailableAdsInformation().Id) )
            return false;
        
        Debug.LogError("Quang: " + MediationInfo.GetAvailableAdsInformation().Id);

        string[] ids = CacheFireBase.GetAdmobHigherPriceIDs.Split(';');

        for (int i = 0; i < ids.Length; i++)
        {
            Debug.LogError(ids[i]);
            if (MediationInfo.GetAvailableAdsInformation().Id.Contains(ids[i]))
            {
                Debug.LogError("Return true: " + ids[i]);
                return true;
            }
        }

        return false;
#endif
        return false;
    }

    //---------------------------DATA EXCEL------------------------------------------------------------------------------------------------------------

    ///-------------SHOP upgrade-------------------------------------------------
    /// </summary>
    public const string MAIN_WEAPON = "MainWeapon";
    public const string LIFE = "Life";
    public const string LASER = "Laser";
    public const string WINDSLASH = "WindSlash";
    public const string MISSILE = "Missile";
    public const string SUPER_WEAPON = "SuperWeapon";
    public const string GOLD = "Gold";
    public const string WINGMAN = "Wingman";
    public const string SECONDARY_WEAPON = "SecondaryWeapon";

    //---

    public const string POWER = "Power";
    public const string QUANTITY = "Quantity";
    public const string SPEED = "Speed";
    public const string DURATION = "Duration";
    public const string DROP_CHANCE = "DropChance";
    //---

    public const string PRICE = "Price";
    public const string VALUE = "Value";



    ///-------------Achievements-------------------------------------------------
    //

    public const string VALUE_ACHIEVEMENTS = "Value";
    public const string TYPEGIFT_ACHIEVEMENTS = "TypeGift";
    public const string NUMGIFT_ACHIEVEMENTS = "NumGift";

    public const int TOTAL_TYPE_ACHIEVEMENTS = 6;
    public enum TypeItemAchievements
    {
        GetGold = 1,
        GetUpgrade,
        KillEnemy,
        KillBoss,
        CollectItem,
        UseTimeSphere
    }
    //--type Achievements
    //public const string GET_GOLD_ACHIEVEMENTS = "GetGold";
    //public const string GET_UPGRADE_ACHIEVEMENTS = "GetUpgrade";
    //public const string KILL_ENEMY_ACHIEVEMENTS = "KillEnemy";
    //public const string KILL_BOSS_ACHIEVEMENTS = "KillBoss";
    //public const string COLLECT_ITEM_ACHIEVEMENTS = "CollectItem";
    //public const string USE_ACTIVESKILL_ACHIEVEMENTS = "UseTimeSphere";


    ///-------------DailyQuest-------------------------------------------------
    //11 type
    public const int TOTAL_TYPE_DAILYQUEST = 22;
    public enum TypeItemDailyQuest
    {
        GetGoldEasy = 1,
        GetGoldHard,
        UseTimeSphereEasy,
        UsePowerUpEasy,
        UsePowerUpHard,
        KillBossEasy,
        KillBossHard,
        PassLevelEasy,
        PassLevelHard,
        KillEnemyEasy,
        KillEnemyHard,
        ShareFacebook,
        RateGame,
        PlayPvpEasy,
        PlayPvpHard,
        WinPvpEasy,
        WinPvpHard,
        PlayEndlessEasy,
        PlayEndlessHard,
        LuckyWheelEasy,
        LuckyWheelHard,
        UseTimeSphereHard
    }

    //public static TypeItemDailyQuest TypeItemDailyQuest = ModeGamePlay.Campaign;

    //public const string GETGOLD_EASY_DAILYQUEST = "GetGoldEasy";
    //public const string GETGOLD_HARD_DAILYQUEST = "GetGoldHard";
    //public const string USE_ACTIVESKILL_EASY_DAILYQUEST = "UseTimeSphereEasy";
    //public const string USE_POWERUP_EASY_DAILYQUEST = "UsePowerUpEasy";
    //public const string USE_POWERUP_HARD_DAILYQUEST = "UsePowerUpHard";
    //public const string KILLBOSS_EASY_DAILYQUEST = "KillBossEasy";
    //public const string KILLBOSS_HARD_DAILYQUEST = "KillBossHard";
    //public const string PASSLEVEL_EASY_DAILYQUEST = "PassLevelEasy";
    //public const string PASSLEVEL_HARD_DAILYQUEST = "PassLevelHard";
    //public const string KILLENEMY_EASY_DAILYQUEST = "KillEnemyEasy";
    //public const string KILLENEMY_HARD_DAILYQUEST = "KillEnemyHard";


    ///-------------Format 3 số 0-------------------------------------------------


    public static string FormatNumber(int number)
    {
        string str = string.Format("{0:n0}", number);
        str = str.Replace(',', '.');

        return str;
    }


}
