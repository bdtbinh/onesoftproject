﻿using System.Collections.Generic;
using Firebase.Analytics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirebaseLogSpaceWar
{
    public static string PROPERTY_CURRENT_USER_STATUS = "CurrentLevel";

    public static string PROPERTY_MAX_LEVEL_STATUS = "MaxLevel";


    //-cache lại để check sau khi mua inapp thì user dùng để mua gói nào
    public const string KEY_CACHE_BUY_INAPP = "Inapp_for_check_buy_item";



    public static string FormatLevelName(string level)
    {
        if (SceneManager.GetActiveScene().name == "LevelTutorial")
        {
            level = "Tutorial";
        }
        else if (GameContext.modeGamePlay != GameContext.ModeGamePlay.Campaign)
        {
            level = "000";
        }
        return level;
    }

    #region check init trước khi log
    public static void LogEvent(string name, params Parameter[] parameters)
    {
        if (FireBaseRemote.Instance.isFirebaseInitialized)
        {
            FirebaseAnalytics.SetUserProperty(PROPERTY_CURRENT_USER_STATUS, "COMPLETE_LEVEL_" + CacheGame.GetCurrentLevel());
            if (CacheGame.GetMaxLevel3Difficult() <= GameContext.TOTAL_LEVEL + 1)
            {
                FirebaseAnalytics.SetUserProperty(PROPERTY_MAX_LEVEL_STATUS, "MAX_LEVEL_" + CacheGame.GetMaxLevel3Difficult());
            }
            //FirebaseAnalytics.LogEvent(name, parameters);
        }
    }
    #endregion

    public static string FormatModePlaying(string typeDifficult)
    {
        var modePlayingReturn = "";
        switch (GameContext.modeGamePlay)
        {
            case GameContext.ModeGamePlay.Campaign:
                modePlayingReturn = typeDifficult;
                break;
            case GameContext.ModeGamePlay.EndLess:
                modePlayingReturn = "EndLess";
                break;
            case GameContext.ModeGamePlay.TryPlane:
                modePlayingReturn = "TryPlane";
                break;
            case GameContext.ModeGamePlay.pvp:
            case GameContext.ModeGamePlay.Tournament:
            case GameContext.ModeGamePlay.PVP2vs2:
                modePlayingReturn = "pvp";
                break;
            default:
                modePlayingReturn = "unknow";
                break;
        }

        return modePlayingReturn;
    }

    public static FalconFirebaseLogger.DifficultyLevel FormatModePlayingQuang(string typeDifficult)
    {
        switch (typeDifficult)
        {
            case GameContext.DIFFICULT_NOMAL:
                return FalconFirebaseLogger.DifficultyLevel.Normal;
            case GameContext.DIFFICULT_HARD:
                return FalconFirebaseLogger.DifficultyLevel.Hard;
            case GameContext.DIFFICULT_HELL:
                return FalconFirebaseLogger.DifficultyLevel.Hardest;
            default:
                return FalconFirebaseLogger.DifficultyLevel.Other;

        }

    }

    //--------------------------------------------------------------------

    public static void LogLevelStart()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            FalconFirebaseLogger.LevelStart(FormatModePlayingQuang(CacheGame.GetDifficultCampaign()), CacheGame.GetCurrentLevel());
        }
    }


    //---------------
    public static void LogLevelFail()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            FalconFirebaseLogger.LevelFail(FormatModePlayingQuang(CacheGame.GetDifficultCampaign()), CacheGame.GetCurrentLevel());
        }

    }


    //---------------

    public static void LogLevelComplete()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            FalconFirebaseLogger.LevelComplete(FormatModePlayingQuang(CacheGame.GetDifficultCampaign()), CacheGame.GetCurrentLevel());
        }

    }


    public static void LogLevelCompleteLv5()
    {
        if (CacheGame.GetCurrentLevel() == 5)
        {
            FalconFirebaseLogger.LogEvent("Gameplay_Pass_Lv5");
        }
    }


    public static void LogLevelCompleteLv10()
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            if (CacheGame.GetCurrentLevel() == 10)
            {
                FalconFirebaseLogger.LogEvent("Gameplay_Pass_Lv10");
            }
        }

    }


    public static void LogUserDead(string waveName)
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            FalconFirebaseLogger.PlayerDied(FormatModePlayingQuang(CacheGame.GetDifficultCampaign()), CacheGame.GetCurrentLevel(), waveName);
        }
    }

    public static void LogTimeLoadAssetBundle(string time)
    {
        FalconFirebaseLogger.LogEvent("TimeLoadAssetBundle", new Parameter("Time", "" + time));
    }

    public static void LogInappPurchase(string productName, int value)
    {

        if (PlayerPrefs.GetInt("FirstInappFalcon", 0) >= 1 && FalconFirebaseLogger.InAppCount == 0)
        {
            FalconFirebaseLogger.InAppCount = 1;
        }
        FalconFirebaseLogger.InappPurchase(productName, value);

        //LogEvent("InappPurchaseFalcon", new Parameter("productName", "" + productName), new Parameter("Value", value));
        //PlayerPrefs.SetInt(KEY_CACHE_BUY_INAPP, 1);
    }



    public static void LogTimePlayLevel(string level, int time)
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            FalconFirebaseLogger.LogEvent("TimePlayLevel", new Parameter("Level", FormatLevelName(level)), new Parameter("Time", time));
        }
        else
        {
            FalconFirebaseLogger.LogEvent("TimePlayLevelCampaignExtra", new Parameter("Level", FormatLevelName(level)), new Parameter("Time", time));
        }
    }



    public static void LogClickButton(string nameButton)
    {
        if (FireBaseRemote.Instance.isFirebaseInitialized)
        {
            //FirebaseAnalytics.LogEvent("Click", new Parameter("Button", nameButton));
            FalconFirebaseLogger.LogEvent("ClickBtn_" + nameButton);
        }
    }

    #region log vòng đời 1 quảng cáo

    public const string EVENT_INTERTITIAL_STATUS = "intertitial_show_event";
    public const string PARAM_INTERTITIAL_STATUS = "intertitial_show_status";
    public const string INTERTITIAL_IRONSRC = "ironsrc";
    public const string INTERTITIAL_ADMOB = "admob";
    public const string INTERTITIAL_NOTHING = "nothing";
    public static void LogShowIntertitialStatus(string status)
    {
        FalconFirebaseLogger.LogEvent(EVENT_INTERTITIAL_STATUS, new Parameter(PARAM_INTERTITIAL_STATUS, status));
        //FirebaseAnalytics.LogEvent(
        //EVENT_INTERTITIAL_STATUS,
        //new Parameter(PARAM_INTERTITIAL_STATUS, status)

        //);
    }

    //FirebaseLogSpaceWar
    public static void LogOtherEvent(string eventName)
    {
        if (FireBaseRemote.Instance.isFirebaseInitialized)
        {
            FirebaseAnalytics.LogEvent(eventName);
        }
    }

    #endregion

    #region log thay đổi tiền , card, gem
    //--------------------- log thay đổi tiền , card, gem ----------------------------------------------------
    public const string Mission_why = "Mission";
    public const string FirstTimeMission_why = "FirstTimeMission";
    public const string LuckyWheel_why = "LuckyWheel";
    public const string MysticChest_why = "MysticChest";
    public const string SocialReward_why = "SocialReward";
    public const string PvP_why = "PvP";
    public const string Endless_why = "Endless";
    public const string Achievement_why = "Achievement";
    public const string DailyQuest_why = "DailyQuest";
    public const string DailyLogin_why = "DailyLogin";
    public const string IAP_why = "IAP";
    public const string GiftCode_why = "GiftCode";
    public const string X2VideoMission_why = "X2VideoMission";
    public const string X4VideoMission_why = "X4VideoMission";
    public const string VideoTop_why = "VideoTop";
    public const string VideoEndGame_why = "VideoEndGame";
    public const string Event_why = "Event";
    public const string ServerPush_why = "ServerPush";
    public const string UpgradeAircraft_why = "UpgradeAircraft";
    public const string UpgradeDrone_why = "UpgradeDrone";
    public const string PurchaseAircraft_why = "PurchaseAircraft";
    public const string PurchaseDrone_why = "PurchaseDrone";
    public const string PurchaseWing_why = "PurchaseWing";
    public const string EvolveAircraft_why = "EvolveAircraft";
    public const string EvolveDrone_why = "EvolveDrone";
    public const string EvolveWing_why = "EvolveWing";
    public const string BuyGold_why = "BuyGold";
    public const string GloryChest_why = "GloryChest";
    public const string PopupMail_why = "PopupMail";
    public const string Tournament_why = "Tournament";
    public const string Unknown_why = "unknown";
    public const string DropIngame_Why = "DropIngame";
    public const string StarChest_Why = "StarChest";
    public const string CreateClan_Why = "CreateClan";
    public const string RandomCardShop_Why = "RandomCardShop";

    public const string UpgradeWing_why = "UpgradeWing";


    public static void LogGoldIn(int value, string why, string where)
    {
        FalconFirebaseLogger.LogEvent("GoldIn", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where));
    }
    public static void LogGoldOut(int value, string why, string where)
    {
        FalconFirebaseLogger.LogEvent("GoldOut", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where));
    }
    //-------
    public static void LogGemIn(int value, string why, string where)
    {
        FalconFirebaseLogger.LogEvent("GemIn", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where));
    }
    public static void LogGemOut(int value, string why, string where)
    {
        FalconFirebaseLogger.LogEvent("GemOut", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where));
    }
    //-------
    public static void LogCardIn(int value, string why, string where, string typeCard)
    {
        FalconFirebaseLogger.LogEvent("CardIn", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where), new Parameter("typeCard", typeCard));
    }
    public static void LogCardOut(int value, string why, string where, string typeCard)
    {
        FalconFirebaseLogger.LogEvent("CardOut", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where), new Parameter("typeCard", typeCard));
    }
    //-------
    #endregion


}
