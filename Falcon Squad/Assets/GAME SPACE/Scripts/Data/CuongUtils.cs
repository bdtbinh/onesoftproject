﻿using EasyMobile;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;
using System;

public static class CuongUtils
{
    static string packagename = "invaders.os.galaxy.space.shooter.attack.classic";

    public static void TrackFacebookPurchase(string package_sku)
    {
        if (GameContext.IS_CHINA_VERSION) return;

        // priceCurrency is a string containing the 3-letter ISO code for the currency
        // that the user spent, and priceAmount is a float containing the quantity spent;
        // packageName is a string containing your SKU code for the thing they bought
        var iapParameters = new Dictionary<string, object>();
        iapParameters[packagename] = package_sku;
        //FB.LogPurchase(priceAmount, priceCurrency, iapParameters);
        FB.LogPurchase(GetLocalizedPrice(package_sku), GetCurrencyCode(package_sku), iapParameters);

        //var iapParameters = new Dictionary<string, object>();
        //iapParameters[bundleID] = bundleID;
        //iapParameters[product.definition.id] = product.definition.id;
        //iapParameters["isoCurrencyCode"] = productMetaData.isoCurrencyCode;
        //iapParameters["localizedPrice"] = localizedPrice;
        //FB.LogAppEvent(AppEventName.Purchased, null, iapParameters);
    }


    public static string ad_type_video = "VIDEO";
    public static string ad_type_inter = "FULLSCREEN";
    public static void TrackAppflyerAds(string ad_type)
    {
        if (GameContext.IS_CHINA_VERSION) return;

        Dictionary<string, string> adWatchedEvent = new Dictionary<string, string>();
        adWatchedEvent.Add("af_type", ad_type);
        adWatchedEvent.Add("af_CurrencyCode", GetCurrencyCode(ad_type));
        AppsFlyer.trackRichEvent("af_ad_watched", adWatchedEvent);
    }

    static string publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp6ecxSVOAo18qbmh6mGSh6SnmxrUVXRhK2U5PifI9d0Gmf7CtcEJFlvWEJWK80rjyG9FwUXqX9OA2zGAfYDlxGIW65dtDtaDaciVDcYFMX3RM5N3J36gtRSFwVp5tJfc9D3dkTcN4yNd+S0hh+H3k2EX83rAysdN8Fggbrr+vNqM0j1D0RGIFtL54/o9vM0Mqzv/R3FNVada0gv7AAuCUe9MOP6iFQl7ZtnhDmR0BDj8ZvD2c92Zgvwb3tU4IC29lePiA8jfDmb8/JHdTexh37x9n/jQuD9l9T0+6P/N2jxSnguGhDHZ3KIiCZMSE7a7WeM2H2ZOkHPFGiWtiTevhQIDAQAB";
    public static void TrackAppflyerPurchase(string sku)
    {
        if (GameContext.IS_CHINA_VERSION) return;

        Debug.LogError("sku: " + sku);
        var product = InAppPurchasing.GetProduct(sku);
        if (product != null)
        {
#if UNITY_ANDROID
            var receipt = product.receipt;
            Debug.LogError("receipt: " + receipt);
            if (receipt != null)
            {
                var googleReceipt = GooglePurchase.FromJson(receipt);
                var ReceiptJson = googleReceipt.PayloadData.json;
                // Pass in the signature
                var Signature = googleReceipt.PayloadData.signature;
                Debug.LogError("googleReceipt: " + googleReceipt);
                Debug.LogError("ReceiptJson: " + ReceiptJson);
                Debug.LogError("Signature: " + Signature);
                AppsFlyer.validateReceipt(publicKey, ReceiptJson, Signature, GetLocalizedPrice(sku).ToString(), GetCurrencyCode(sku), null);
            }
#endif

#if UNITY_IOS
            var transactionID = product.transactionID;
            if (transactionID != null)
            {
                AppsFlyer.validateReceipt(sku, GetLocalizedPrice(sku).ToString(), GetCurrencyCode(sku), transactionID, null);
            }
#endif
        }
        //AppsFlyer.validateReceipt(sku, GetLocalizedPrice(sku).ToString(), GetCurrencyCode(sku), transactionId,null);


        //public static void validateReceipt(string publicKey, string purchaseData, string signature, string price, string currency, Dictionary<string, string> extraParams) { }
        //public static void validateReceipt(string productIdentifier, string price, string currency, string transactionId, Dictionary<string, string> additionalParametes) { }
    }

    public static void TrackAppflyerPurchaseOld(string sku)
    {
        if (GameContext.IS_CHINA_VERSION) return;

        //string purchaseId
        Dictionary<string, string> eventValue = new Dictionary<string, string>();
        try
        {
            eventValue.Add(AFInAppEvents.REVENUE, GetLocalizedPrice(sku).ToString());
            eventValue.Add(AFInAppEvents.CONTENT_ID, sku);
            eventValue.Add(AFInAppEvents.CURRENCY, GetCurrencyCode(sku));
            eventValue.Add(AFInAppEvents.QUANTITY, "1");
            AppsFlyer.trackRichEvent(AFInAppEvents.PURCHASE, eventValue);
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }
    }


    public static float GetLocalizedPrice(string sku)
    {
        if (InAppPurchasing.GetProductLocalizedData(sku) != null)
        {
            decimal price = InAppPurchasing.GetProductLocalizedData(sku).localizedPrice;
            return ((float)price) * 0.72f;
        }
        return 0;
    }

    public static string GetCurrencyCode(string sku)
    {
        if (InAppPurchasing.GetProductLocalizedData(sku) != null)
        {
            return InAppPurchasing.GetProductLocalizedData(sku).isoCurrencyCode;
        }
        return "USD";
    }


    public static int GetMaxPlaneUnlock()
    {
        for (int i = 7; i > 0; i--)
        {
            if (CacheGame.GetSpaceShipIsUnlocked(i) == 1)
            {
                return i;
            }
        }
        return 1;
    }


    public static int ChangeDifficultCampaignToInt()
    {
        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.Campaign)
        {
            return 000;
        }
        else
        {
            switch (CacheGame.GetDifficultCampaign())
            {
                case "Normal":
                    return 0;
                    break;
                case "Hard":
                    return 1;
                    break;
                case "Hell":
                    return 2;
                    break;
                default:
                    return 000;
                    break;
            }
        }
    }


    const string KEY_CACHE_PRICE_IAP = "cache_price_iap";
    public static string GetIapLocalizedPrice(string sku)
    {
        if (InAppPurchasing.GetProductLocalizedData(sku) != null)
        {
            PlayerPrefs.SetString(KEY_CACHE_PRICE_IAP + sku, "" + InAppPurchasing.GetProductLocalizedData(sku).localizedPriceString);
            return "" + InAppPurchasing.GetProductLocalizedData(sku).localizedPriceString;
        }
        else
        {
            return PlayerPrefs.GetString(KEY_CACHE_PRICE_IAP + sku, "not ready");
        }
        //Price
        //return "";

    }
}

// The following classes are used to deserialize JSON results provided by IAP Service
// Please, note that Json fields are case-sensetive and should remain fields to support Unity Deserialization via JsonUtilities
public class JsonData
{
    // Json Fields, ! Case-sensetive

    public string orderId;
    public string packageName;
    public string productId;
    public long purchaseTime;
    public int purchaseState;
    public string purchaseToken;
}

public class PayloadData
{
    public JsonData JsonData;

    // Json Fields, ! Case-sensetive
    public string signature;
    public string json;

    public static PayloadData FromJson(string json)
    {
        var payload = JsonUtility.FromJson<PayloadData>(json);
        payload.JsonData = JsonUtility.FromJson<JsonData>(payload.json);
        return payload;
    }
}

public class GooglePurchase
{
    public PayloadData PayloadData;

    // Json Fields, ! Case-sensetive
    public string Store;
    public string TransactionID;
    public string Payload;

    public static GooglePurchase FromJson(string json)
    {
        var purchase = JsonUtility.FromJson<GooglePurchase>(json);
        purchase.PayloadData = PayloadData.FromJson(purchase.Payload);
        return purchase;
    }

}


