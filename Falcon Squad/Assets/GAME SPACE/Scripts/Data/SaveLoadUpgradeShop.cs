﻿using System;
using System.Collections;
using System.Collections.Generic;
using OneSoftGame.Tools;
using Sirenix.OdinInspector;
//using UnityEditor;
using UnityEngine;

public class SaveLoadUpgradeShop : PersistentSingleton<SaveLoadUpgradeShop>
{
	public List<int> test;
	public UpgradeInShop upgradeShopData;


	public UpgradeInShop GetData ()
	{
		return upgradeShopData;
	}


	private static SaveLoadUpgradeShop mInstance;

	public static SaveLoadUpgradeShop Instance {
		get {
			if (mInstance == null)
				mInstance = GameObject.FindObjectOfType<SaveLoadUpgradeShop> ();
			return mInstance;
		}
	}

	//	public void SaveData ()
	//	{
	//		SaveDataToDisk ();
	//	}
	//
	//	private void SaveDataToDisk ()
	//	{
	//		ES2.Save<UpgradeInShop> (upgradeShopData, "UpgradeInShop");
	//	}

	// Use this for initialization
	void Start ()
	{
//		LoadData ();
		LoadPriceData (GameContext.MAIN_WEAPON, GameContext.SPEED, 2);
//		print ("dataUpgrade_" + LoadPriceData (GameContex.mainWeapon, GameContex.speed, 2));

//		print ("totalLevel" + LoadTotalLevelData (GameContex.mainWeapon, GameContex.speed));
	}



	int curPriceUse = 0;
	int curValueUse = 0;
	int curTotalLevelUse = 0;

	public int LoadPriceData (string nameItem, string typeUpgrade, int levelIndex)
	{

		string curNameItem, curTypeUpgrade;
		int curLevelIndex;

		foreach (var child_ItemUpgrade in upgradeShopData.listItemUpgrade) {
			curNameItem = child_ItemUpgrade.nameItem;

			foreach (var child_TypeUpgradeInItem in child_ItemUpgrade.listTypeUpgradeInItem) {
				curTypeUpgrade = child_TypeUpgradeInItem.nameTypeUpgradeItem;

				foreach (var child_LevelKeyValue in child_TypeUpgradeInItem.listLevelKeyValue) {
					curLevelIndex = child_LevelKeyValue.indexLevel;

					if (curNameItem == nameItem && curTypeUpgrade == typeUpgrade && curLevelIndex == levelIndex) {
						curPriceUse = child_LevelKeyValue.price;
					}
				}
			}
		}
		return curPriceUse;
	}


	public int LoadValueData (string nameItem, string typeUpgrade, int levelIndex)
	{

		string curNameItem, curTypeUpgrade;
		int curLevelIndex;

		foreach (var child_ItemUpgrade in upgradeShopData.listItemUpgrade) {
			curNameItem = child_ItemUpgrade.nameItem;

			foreach (var child_TypeUpgradeInItem in child_ItemUpgrade.listTypeUpgradeInItem) {
				curTypeUpgrade = child_TypeUpgradeInItem.nameTypeUpgradeItem;

				foreach (var child_LevelKeyValue in child_TypeUpgradeInItem.listLevelKeyValue) {
					curLevelIndex = child_LevelKeyValue.indexLevel;

					if (curNameItem == nameItem && curTypeUpgrade == typeUpgrade && curLevelIndex == levelIndex) {
						curValueUse = child_LevelKeyValue.value;
					}
				}
			}
		}
		return curValueUse;
	}



	public int LoadTotalLevelData (string nameItem, string typeUpgrade)
	{

		string curNameItem, curTypeUpgrade;
//		int curLevelIndex;

		foreach (var child_ItemUpgrade in upgradeShopData.listItemUpgrade) {
			curNameItem = child_ItemUpgrade.nameItem;

			foreach (var child_TypeUpgradeInItem in child_ItemUpgrade.listTypeUpgradeInItem) {
				curTypeUpgrade = child_TypeUpgradeInItem.nameTypeUpgradeItem;
				if (curNameItem == nameItem && curTypeUpgrade == typeUpgrade) {
					curTotalLevelUse = child_TypeUpgradeInItem.totalLevel;
				}
			}
		}
		return curTotalLevelUse;
	}

}









//
//  Class Shop Upgrade
//



[Serializable]
public class OneLevelKeyValue
{
	public int indexLevel;
	//	[Header ("----")]
	public int price;
	public int value;
}



[Serializable]
public class TypeUpgradeInItem
{
	public string nameTypeUpgradeItem;
	public int totalLevel;
	//	public int curLevel;
	public List<OneLevelKeyValue> listLevelKeyValue;


}


[Serializable]
public class ItemUpgrade
{

	[Header ("------------------------------------------------")]
	[PropertyOrder (-1)]
	public string nameItem;
	public List<TypeUpgradeInItem> listTypeUpgradeInItem;


}

[Serializable]
public class UpgradeInShop
{
	//	public int version;

	public List<ItemUpgrade> listItemUpgrade;

}


