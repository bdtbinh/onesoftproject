﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using OneSoftGame.Tools;
//using Sirenix.OdinInspector;
//using UnityEditor;
//using UnityEngine;
//
//public class SaveLoadManager : PersistentSingleton<SaveLoadManager> {
//
//	public PlayerData playerData;
//
//	public PlayerData GetPlayerData()
//	{
//		return playerData;
//	}
//
//	public void SavePlayerData()
//	{
//		ES2.Save<PlayerData>(playerData,"PlayerData");
//	}
//	
//	// Use this for initialization
//	void Start () {
//		LoadPlayerData();
//	}	
//   private void LoadPlayerData()
//    {
//        var isFirstLoad = true;
//		/* Check that there is data to load */
//		if(ES2.Exists("FirstLoad"))
//    		isFirstLoad = ES2.Load<bool>("FirstLoad");
//		if (isFirstLoad)
//		{
//			LoadDefaultValue();
//		}else
//		{
//			LoadFromDisk();
//		}
//    }
//
//	[Button]
//    private void LoadFromDisk()
//    {
//		print("Load playerData From Disk");
//		playerData = ES2.Load<PlayerData>("PlayerData");
//		print("data" +playerData.HighScore);
//    }
//
//	[Button]
//    private void LoadDefaultValue()
//    {
//		print("LoadDefaultValue, playerData get from inspector");
//		SavePlayerData();
//		
//		ES2.Save<bool>(false,"FirstLoad");
//		
//		print("data" +playerData.HighScore);
//		//print("dataManagerName:" + data.dataManagerName);
//
//    }
//}
//
//[Serializable]
//public class PlayerData
//{
//	public bool soundOn;
//	public bool musicOn;
//
//	public int HighScore;
//}
