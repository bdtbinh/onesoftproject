﻿using Firebase.Analytics;
using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

//public class Parameter
//{
//    public readonly string name;
//    public readonly string value;
//    public readonly Firebase.Analytics.Parameter firebaseParameter;
//    public Parameter(string parameterName, string parameterValue)
//    {
//        firebaseParameter = new Firebase.Analytics.Parameter(parameterName, parameterValue);
//        name = parameterName;
//        value = parameterValue.ToString();
//    }
//    public Parameter(string parameterName, long parameterValue)
//    {
//        firebaseParameter = new Firebase.Analytics.Parameter(parameterName, parameterValue);
//        name = parameterName;
//        value = parameterValue.ToString();
//    }
//    public Parameter(string parameterName, double parameterValue)
//    {
//        firebaseParameter = new Firebase.Analytics.Parameter(parameterName, parameterValue);
//        name = parameterName;
//        value = parameterValue.ToString();
//    }
//}


public static class FalconFirebaseLogger
{


    public static void LogEvent(string name, params Parameter[] parameters)
    {
        if (FireBaseRemote.Instance.isFirebaseInitialized)
        {
            FirebaseAnalytics.LogEvent(name, parameters);
        }
    }


    //public static void LogEvent(string name, params Parameter[] parameters)
    //{
    //    Firebase.Analytics.Parameter[] paramsFB = new Firebase.Analytics.Parameter[parameters.Length];

    //    if (GameContext.IS_CHINA_VERSION)
    //    {
    //        var dict = new Dictionary<string, string>();
    //        for (int i = 0; i < parameters.Length; i++)
    //        {
    //            string dicKey = parameters[i].name;
    //            string dicValue = parameters[i].value;
    //            dict[dicKey] = dicValue;

    //            paramsFB[i] = new Firebase.Analytics.Parameter(parameters[i].name, parameters[i].value);
    //        }
    //        //GA.Event(name, dict);
    //    }

    //    if (FireBaseRemote.Instance.isFirebaseInitialized)
    //    {
    //        FirebaseAnalytics.LogEvent(name, paramsFB);
    //    }
    //}


    public enum ExtraGameMode
    {
        EndLess,
        PvP,
        CoOp,
        TryPlane,
        Tutorial,
        Boss,
        Other
    }

    public enum DifficultyLevel
    {
        Normal,
        Hard,
        Hardest,
        Other
    }

    #region Các sự khiện ExtraGame
    public static void ExtraGameStart(ExtraGameMode gameMode, DifficultyLevel difficultyLevel = DifficultyLevel.Normal, int level = -1)
    {
        LogEvent("ExtraGameStart", new Parameter(gameMode + "_" + difficultyLevel, level));
    }

    public static void ExtraGameFail(ExtraGameMode gameMode, DifficultyLevel difficultyLevel = DifficultyLevel.Normal, int level = -1)
    {
        LogEvent("ExtraGameFail", new Parameter(gameMode + "_" + difficultyLevel, level));
    }

    public static void ExtraGameComplete(ExtraGameMode gameMode, DifficultyLevel difficultyLevel = DifficultyLevel.Normal, int level = -1)
    {
        LogEvent("ExtraGameComplete", new Parameter(gameMode + "_" + difficultyLevel, level));
    }

    /// <summary>
    /// Log khi player chết trong các chế độ khác
    /// </summary>    
    /// <param name="waveName">Tên gameObject của wave</param>
    public static void ExtraGamePlayerDied(ExtraGameMode gameMode, DifficultyLevel difficultyLevel, int level, string waveName)
    {
        LogEvent("ExtraGamePlayerDied", new Parameter(gameMode + "_" + difficultyLevel, level), new Parameter("Wave", waveName));
    }
    #endregion 

    #region Các sự khiện Campaign
    public static void LevelStart(DifficultyLevel difficultyLevel, int level)
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            LogEvent("LevelStart", new Parameter(difficultyLevel.ToString(), level));
            if (difficultyLevel == DifficultyLevel.Normal)
            {
                if (MaxLevel == level)
                {
                    LogEvent("MaxLevelStart", new Parameter(difficultyLevel.ToString(), level));
                }
                else if (MaxLevel < level)
                {
                    MaxLevel = level;
                    MaxLevelFailCount = 0;
                }
            }
        }
        else
        {
            if (difficultyLevel == DifficultyLevel.Normal)
            {
                LogEvent("LevelCampaignExtraStart", new Parameter(difficultyLevel.ToString(), level));
            }
        }
    }

    public static void LevelFail(DifficultyLevel difficultyLevel, int level)
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            LogEvent("LevelFail", new Parameter(difficultyLevel.ToString(), level));
            if (difficultyLevel == DifficultyLevel.Normal)
            {
                if (MaxLevel == level)
                {
                    MaxLevelFailCount++;
                    LogEvent("MaxLevelFail", new Parameter(difficultyLevel.ToString(), level));
                }
            }
        }
        else
        {
            if (difficultyLevel == DifficultyLevel.Normal)
            {
                LogEvent("LevelCampaignExtraFail", new Parameter(difficultyLevel.ToString(), level));
            }
        }
    }

    public static void LevelComplete(DifficultyLevel difficultyLevel, int level)
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            LogEvent("LevelComplete", new Parameter(difficultyLevel.ToString(), level));
            if (difficultyLevel == DifficultyLevel.Normal)
            {
                if (MaxLevel <= level)
                {
                    LogEvent("MaxLevelComplete",
                        new Parameter(difficultyLevel.ToString(), level),
                        new Parameter("MaxLevelFailCount", MaxLevelFailCount)
                        );
                    MaxLevel = level + 1;
                    MaxLevelFailCount = 0;
                }
            }
        }
        else
        {
            if (difficultyLevel == DifficultyLevel.Normal)
            {
                LogEvent("LevelCampaignExtraComplete", new Parameter(difficultyLevel.ToString(), level));
            }
        }
    }



    /// <summary>
    /// Log khi player chết trong Campaign
    /// </summary>    
    /// <param name="waveName">Tên gameObject của wave</param>
    public static void PlayerDied(DifficultyLevel difficultyLevel, int level, string waveName)
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Normal)
        {
            LogEvent("PlayerDied", new Parameter(difficultyLevel.ToString(), level), new Parameter("Wave", waveName));
        }
        else
        {
            LogEvent("PlayerDiedLevelCampaignExtra", new Parameter(difficultyLevel.ToString(), level), new Parameter("Wave", waveName));
        }
    }
    #endregion

    public static void InappPurchase(string productName, int value)
    {
        LogEvent("Inapp", new Parameter("ProductName", "" + productName), new Parameter("Value", value));
        if (InAppCount <= 0)
        {
            LogEvent("FirstInapp", new Parameter("ProductName", "" + productName), new Parameter("Value", "" + value), new Parameter("MaxLevelFailCount", "" + MaxLevelFailCount));
        }
        InAppCount++;
    }

    #region Log thay đổi tiền , card, gem
    public const string Mission_why = "Mission";
    public const string LevelClearRewards_why = "LevelClearRewards";
    public const string LevelClearRewardsExtra_why = "LevelClearRewardsExtra";
    public const string LuckyWheel_why = "LuckyWheel";
    public const string MysticChest_why = "MysticChest";
    public const string SocialReward_why = "SocialReward";
    public const string PvP_why = "PvP";
    public const string Endless_why = "Endless";
    public const string Achievement_why = "Achievement";
    public const string DailyQuest_why = "DailyQuest";
    public const string DailyLogin_why = "DailyLogin";
    public const string IAP_why = "IAP";
    public const string GiftCode_why = "GiftCode";
    public const string X2VideoMission_why = "X2VideoMission";
    public const string X4VideoMission_why = "X4VideoMission";
    public const string VideoTop_why = "VideoTop";
    public const string ServerPush_why = "ServerPush";
    public const string UpgradeAircraft_why = "UpgradeAircraft";
    public const string UpgradeDrone_why = "UpgradeDrone";
    public const string PurchaseAircraft_why = "PurchaseAircraft";
    public const string PurchaseDrone_why = "PurchaseDrone";
    public const string EvolveAircraft_why = "EvolveAircraft";
    public const string EvolveDrone_why = "EvolveDrone";
    //Gold
    public static void LogGoldIn(int value, string why, string where)
    {
        LogEvent("GoldIn", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where));
    }
    public static void LogGoldOut(int value, string why, string where)
    {
        LogEvent("GoldOut", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where));
    }
    //Gem
    public static void LogGemIn(int value, string why, string where)
    {
        LogEvent("GemIn", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where));
    }
    public static void LogGemOut(int value, string why, string where)
    {
        LogEvent("GemOut", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where));
    }
    //Card
    public static void LogCardIn(int value, string why, string where, string typeCard)
    {
        LogEvent("CardIn", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where), new Parameter("typeCard", typeCard));
    }
    public static void LogCardOut(int value, string why, string where, string typeCard)
    {
        LogEvent("CardOut", new Parameter("value", "" + value), new Parameter("why", why), new Parameter("where", where), new Parameter("typeCard", typeCard));
    }
    #endregion

    #region FirebaseCache
    private static void Save(string key, int value)
    {
        PlayerPrefs.SetInt("FirebaseCache_" + key, value);
    }
    private static void Save(string key, string value)
    {
        PlayerPrefs.SetString("FirebaseCache_" + key, value);
    }
    private static int Load(string key)
    {
        return PlayerPrefs.GetInt("FirebaseCache_" + key, 0);
    }
    private static string LoadString(string key)
    {
        return PlayerPrefs.GetString("FirebaseCache_" + key, "EMPTY");
    }
    public static int InAppCount
    {
        get { return Load("InAppCount"); }
        set { Save("InAppCount", value); }
    }
    private static int MaxLevelFailCount
    {
        get { return Load("MaxLevelFailCount"); }
        set { Save("MaxLevelFailCount", value); }
    }
    public static int MaxLevel
    {
        get { return Load("MaxLevel"); }
        set
        {
            if (FireBaseRemote.Instance.isFirebaseInitialized) FirebaseAnalytics.SetUserProperty("MaxLevel", "MAX_LEVEL_" + value);
            Save("MaxLevel", value);
        }
    }
    public static string UserID
    {
        get { return LoadString("UserID"); }
        set
        {
            if (FireBaseRemote.Instance.isFirebaseInitialized) FirebaseAnalytics.SetUserProperty("UserID", value);
            Save("UserID", value);
        }
    }
    #endregion

    public static void SetDeviceUID()
    {
        if (FireBaseRemote.Instance.isFirebaseInitialized) FirebaseAnalytics.SetUserProperty("DeviceUID", SystemInfo.deviceUniqueIdentifier);
    }
}

