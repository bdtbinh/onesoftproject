﻿using OneSoftGame.Tools;
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Analytics;

public class UnityRemoteValue : PersistentSingleton<UnityRemoteValue>
{

    /// setup ads
    [Title ("Setup ads", bold: true, horizontalLine: true)]
    public float percentChooseTypeAds = 50f;

    public int levelStartShowInterAds = 3;

    public float timeWaitForShowInterAdsAgain = 61f;


    /// setup SocialManager
    [Title("Setup SocialManager", bold: true, horizontalLine: true)]
    public string facebookFanpageID;

    public string facebookGroupID;

    public string instagramUserName;

    public string twitterUserName;
    public string twitterID;


    /// setup pvp
    [Title("Setup pvp", bold: true, horizontalLine: true)]
    public string urlPvp = "";
    public int requireLevelPvp;
    public int canPlayPvp;


    //void Start () {

    //    Debug.LogError("percentChooseTypeAds:" + percentChooseTypeAds);
    //    Debug.LogError("levelStartShowInterAds:" + levelStartShowInterAds);
    //    Debug.LogError("timeWaitForShowInterAdsAgain:" + timeWaitForShowInterAdsAgain);
    //    Debug.LogError("facebookFanpageID:" + facebookFanpageID);
    //    Debug.LogError("facebookGroupID:" + facebookGroupID);
    //    Debug.LogError("instagramUserName:" + instagramUserName);
    //    Debug.LogError("twitterUserName:" + twitterUserName);
    //    Debug.LogError("twitterID:" + twitterID);
    //    Debug.LogError("urlPvp:" + urlPvp);
    //    Debug.LogError("requireLevelPvp:" + requireLevelPvp);
    //    Debug.LogError("canPlayPvp:" + canPlayPvp);


        //CachePvp.ServerUrl = urlPvp;
        //CachePvp.RequireLevel = requireLevelPvp;
        //CachePvp.CanPlayPvp = canPlayPvp;

    //}

	
}
