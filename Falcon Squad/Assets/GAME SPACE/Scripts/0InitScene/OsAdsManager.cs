﻿using EasyMobile;
using OneSoftGame.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TCore;
using UnityEngine.SceneManagement;
using Mp.Pvp;

public class OsAdsManager : PersistentSingleton<OsAdsManager>
{
    public string IRON_KEY_ANDROID = "6c88390d";
    public string IRON_KEY_IOS = "6dcfe19d";
    public string IRON_KEY_ANDROID_CHINA = "6c88390d";
    public string IRON_KEY_IOS_CHINA = "6dcfe19d";

    string Iron_Key_Android_Use = "6c88390d";
    string Iron_Key_Ios_Use = "6dcfe19d";


    //public string INTER_ADMOB_BACKUP_ANDROID = "ca-app-pub-9457878244675693/4470396501";
    //public string INTER_ADMOB_BACKUP_IOS = "ca-app-pub-3940256099942544/4411468910";

    public float valueAdsRemote = 50f;
    public int levelStartShowInter = 3;
    public float timeWaitForShowInterAgain = 61f;
    //
    #region----- vị trí click quảng cáo------------------------------------------------
    public LocationClickVideo locationClickVideo = LocationClickVideo.unknown;
    public enum LocationClickVideo
    {
        unknown,
        btn_video_Top,
        btn_video_Revival,
        btn_video_X2CoinWin,
        btn_video_X2CoinLose,
        btn_video_LuckyWheel,
        btn_video_BuyPack,
        btn_video_Sever,
        btn_video_EndGame
    }
    //
    public LocationClickInterstitial locationClickInterstitial = LocationClickInterstitial.unknown;

    public enum LocationClickInterstitial
    {
        unknown,
        btn_Inter_RetryWin,
        btn_Inter_NextWin,
        btn_Inter_RetryRevival,
        btn_Inter_RetryLose,
        btn_Inter_RetryPause,
        btn_Inter_InterstitialSever
    }
    #endregion

    //InterstitialAd admobIntertitial;

    void Start()
    {
        SetupChinaVersion();
        InitIronSource();
    }

    void SetupChinaVersion()
    {
        if (GameContext.IS_CHINA_VERSION)
        {
            Iron_Key_Ios_Use = IRON_KEY_IOS_CHINA;
            Iron_Key_Android_Use = IRON_KEY_ANDROID_CHINA;
        }
        else
        {
            Iron_Key_Ios_Use = IRON_KEY_IOS;
            Iron_Key_Android_Use = IRON_KEY_ANDROID;
        }
    }

    void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(isPaused);

    }

    void InitIronSource()
    {
        Debug.Log("USE IronSource");

        IronSourceConfig.Instance.setClientSideCallbacks(true);
#if UNITY_ANDROID
        IronSource.Agent.init(Iron_Key_Android_Use);
#else
		    IronSource.Agent.init(Iron_Key_Ios_Use);
#endif

        IronSource.Agent.validateIntegration();
        IronSource.Agent.setAdaptersDebug(true);
        Debug.Log("init IronSource");
        //
        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;

        //

        IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent;
        IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
        //IronSource.Agent.loadInterstitial();
        ReLoadloadInter();


//#if UNITY_ANDROID
//        string adUnitId = INTER_ADMOB_BACKUP_ANDROID;
//#elif UNITY_IPHONE
//        string adUnitId = INTER_ADMOB_BACKUP_IOS;
//#else
//        string adUnitId = "unexpected_platform";
//#endif

        // Initialize an InterstitialAd.

        //admobIntertitial = new InterstitialAd(adUnitId);
        //admobIntertitial.OnAdClosed += HandleOnAdmobIntertitialClosed;
        //admobIntertitial.OnAdFailedToLoad += HandleOnAdmobFailedToLoad;
        //admobIntertitial.OnAdOpening += HandleOnAdmobOpened;
        //admobIntertitial.OnAdLeavingApplication += HandleOnAdmobClick;
        //Advertising.InterstitialAdCompleted
        // Create an empty ad request.
        //AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        //admobIntertitial.LoadAd(request);
    }

    void HandleOnAdmobOpened(object sender, EventArgs args)
    {
        InterstitialAdShowSucceededEvent();
    }

    void HandleOnAdmobClick(object sender, EventArgs args)
    {
        InterstitialAdClickedEvent();
    }

    //------------------------Big Banner----------------------------------------------------------------------------------------
    private string adsId;
    private string where;
    public void ShowInterstitialAds(LocationClickInterstitial locationClick)
    {
        if (CachePvp.myVipPoint > 0) return;

        locationClickInterstitial = locationClick;

        if (CacheGame.GetMaxLevel3Difficult() > levelStartShowInter && CacheGame.GetUsedInapp() != 1 && isRationShowInterAds() && readyShowInterAgain)
        {
            if (IronSource.Agent.isInterstitialReady())
            {
                adsId = PvpUtil.getRandomString();
                where = locationClick.ToString();
                StartCoroutine(WaitForShowInterAgain());
                IronSource.Agent.showInterstitial();
                FirebaseLogSpaceWar.LogShowIntertitialStatus(FirebaseLogSpaceWar.INTERTITIAL_IRONSRC);
            }
            else
            {
                IronSource.Agent.loadInterstitial();
                //AdRequest request = new AdRequest.Builder().Build();
                //admobIntertitial.LoadAd(request);
                FirebaseLogSpaceWar.LogShowIntertitialStatus(FirebaseLogSpaceWar.INTERTITIAL_NOTHING);
            }
        }
    }

    private void ReLoadloadInter()
    {
        if (!IronSource.Agent.isInterstitialReady() && CacheGame.GetMaxLevel3Difficult() > levelStartShowInter && CacheGame.GetUsedInapp() != 1 && isRationShowInterAds() && readyShowInterAgain)
        {
            IronSource.Agent.loadInterstitial();
        }
    }


    private bool readyShowInterAgain = true;
    IEnumerator WaitForShowInterAgain()
    {
        readyShowInterAgain = false;
        yield return new WaitForSecondsRealtime(CachePvp.sCClientConfig.intertitialCountDown);
        readyShowInterAgain = true;
    }

    public bool isRationShowInterAds()
    {
        int valueRandom = UnityEngine.Random.Range(1, 101);

        if (valueRandom < CachePvp.sCClientConfig.intertitiRatio)
        {
            return true;
        }
        return false;

    }

    //------------------------Video Ads----------------------------------------------------------------------------------------
    public bool isRewardedVideoAvailable()
    {
        bool isReady = IronSource.Agent.isRewardedVideoAvailable();
        return isReady;
    }


    //----------------
    public void ShowRewardedAds(LocationClickVideo locationClick)
    {
        adsId = PvpUtil.getRandomString();
        where = locationClick.ToString();
        IronSource.Agent.showRewardedVideo();
        locationClickVideo = locationClick;
        //sau khi xem video ads thì cũng đếm ngược để không show inter ngay sau đó 
        StartCoroutine(WaitForShowInterAgain());
    }

    //----------------------------------------------------------------


    public UnityEvent VideoClosedEvent;
    public UnityEvent VideoRewardedEvent;

    public void AddRewardedListenner(UnityAction callback)
    {
        VideoRewardedEvent.RemoveAllListeners();
        VideoRewardedEvent.AddListener(callback);
    }

    public void AddClosedListenner(UnityAction callback)
    {
        VideoClosedEvent.RemoveAllListeners();
        VideoClosedEvent.AddListener(callback);
    }


    //-----------------------call back video admob-----------------------------------------------------




    void RewardedAdCompletedHandler(RewardedAdNetwork network, AdLocation location)
    {
        Debug.Log("Rewarded ad has completed. The user should be rewarded now.");
        VideoRewardedEvent.Invoke();
    }

    void RewardedAdClosedHandler(RewardedAdNetwork network, AdLocation location)
    {
        //Advertising.LoadRewardedAd();
        VideoClosedEvent.Invoke();
        Debug.Log("Rewarded ad has completed. The user should be rewarded now.");
    }


    //------------------call back video IronSource --------------------------------------------------

#if !UNITY_ANDROID
	bool isRewardedVideo = false;
#endif

    //Invoked when the RewardedVideo ad view has opened.
    //Your Activity will lose focus. Please avoid performing heavy
    //tasks till the video ad will be closed.
    void RewardedVideoAdOpenedEvent()
    {
        Debug.Log("RewardedVideoAdOpenedEvent.");
        new CSLogAds(adsId, CSLogAds.TYPE_VIDEO_ADS, where, CSLogAds.ADS_STATUS_SHOWED).Send();
    }
    //Invoked when the RewardedVideo ad view is about to be closed.
    //Your activity will now regain its focus.
    void RewardedVideoAdClosedEvent()
    {
        Debug.Log("RewardedVideoAdClosedEvent.");
#if !UNITY_ANDROID
//		print ("RewardedVideoAdClosedEvent " + isRewardedVideo);

		if(SceneManager.GetActiveScene ().name == "SelectLevel" || SceneManager.GetActiveScene ().name == "Home")
		{
			SoundManager.PlayHomeBG ();
		}
		else{
			//SoundManager.PlayMainBG ();
			SoundManager.PlayMain2BG ();
		}

		if (isRewardedVideo)
			VideoRewardedEvent.Invoke ();
#endif
        VideoClosedEvent.Invoke();
        CuongUtils.TrackAppflyerAds(CuongUtils.ad_type_video);
    }

    //Invoked when there is a change in the ad availability status.
    //@param - available - value will change to true when rewarded videos are available.
    //You can then show the video by calling showRewardedVideo().
    //Value will change to false when no videos are available.
    void RewardedVideoAvailabilityChangedEvent(bool available)
    {
        Debug.Log("RewardedVideoAvailabilityChangedEvent.");
        //Change the in-app 'Traffic Driver' state according to availability.
        bool rewardedVideoAvailability = available;
    }
    //Invoked when the video ad starts playing.
    void RewardedVideoAdStartedEvent()
    {
        Debug.Log("RewardedVideoAdStartedEvent.");
        new CSLogAds(adsId, CSLogAds.TYPE_VIDEO_ADS, where, CSLogAds.ADS_STATUS_VIDEO_PLAYING).Send();
#if !UNITY_ANDROID
//		print ("RewardedVideoAdStartedEvent");
		SoundManager.StopHomeBG ();
		SoundManager.StopMainBG ();
		SoundManager.StopMain2BG ();

		isRewardedVideo = false;
#endif
    }
    //Invoked when the video ad finishes playing.
    void RewardedVideoAdEndedEvent()
    {
        //		print ("RewardedVideoAdEndedEvent");

    }
    //Invoked when the user completed the video and should be rewarded.
    //If using server-to-server callbacks you may ignore this events and wait for
    //the callback from the ironSource server.
    //@param - placement - placement object which contains the reward data
    void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {
        Debug.Log("RewardedVideoAdRewardedEvent.");
        new CSLogAds(adsId, CSLogAds.TYPE_VIDEO_ADS, where, CSLogAds.ADS_STATUS_VIDEO_REWARDED).Send();
#if !UNITY_ANDROID
//		print ("RewardedVideoAdRewardedEvent");

		isRewardedVideo = true;
#else
        VideoRewardedEvent.Invoke();
#endif
        //CachePvp.Instance.LogAds("VideoAds", locationClickVideo.ToString());
    }

    void RewardedVideoAdClickedEvent(IronSourcePlacement placement)
    {
        Debug.Log("RewardedVideoAdClickedEvent.");
        new CSLogAds(adsId, CSLogAds.TYPE_VIDEO_ADS, where, CSLogAds.ADS_STATUS_CLICKED).Send();
    }
    //Invoked when the Rewarded Video failed to show
    //@param description - string - contains information about the failure.
    void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
        Debug.Log("RewardedVideoAdShowFailedEvent.");
        //		print ("RewardedVideoAdShowFailedEvent");

    }


    //------------------call back Interstitial IronSource --------------------------------------------------

    //Invoked when the initialization process has failed.
    void InterstitialAdLoadFailedEvent(IronSourceError error)
    {
        //if (!IronSource.Agent.isInterstitialReady())
        //{
        //    IronSource.Agent.loadInterstitial();
        //}
        ReLoadloadInter();
        //AdRequest request = new AdRequest.Builder().Build();
        //admobIntertitial.LoadAd(request);
    }
    //Invoked right before the Interstitial screen is about to open.
    void InterstitialAdShowSucceededEvent()
    {
        new CSLogAds(adsId, CSLogAds.TYPE_INTERTITIAL, where, CSLogAds.ADS_STATUS_SHOWED).Send();

#if !UNITY_ANDROID
 Time.timeScale = 0f;
        SoundManager.StopHomeBG();
        SoundManager.StopMainBG();
        SoundManager.StopMain2BG();
#else


#endif


    }
    //Invoked when the ad fails to show.
    void InterstitialAdShowFailedEvent(IronSourceError error)
    {
        Debug.Log("InterstitialAdShowFailedEvent");
    }
    // Invoked when end user clicked on the interstitial ad
    void InterstitialAdClickedEvent()
    {
        new CSLogAds(adsId, CSLogAds.TYPE_INTERTITIAL, where, CSLogAds.ADS_STATUS_CLICKED).Send();
    }
    //Invoked when the interstitial ad closed and the user goes back to the application screen.
    void InterstitialAdClosedEvent()
    {
        //if (!IronSource.Agent.isInterstitialReady())
        //{
        //    IronSource.Agent.loadInterstitial();
        //}
        ReLoadloadInter();

        //AdRequest request = new AdRequest.Builder().Build();
        //admobIntertitial.LoadAd(request);

        CuongUtils.TrackAppflyerAds(CuongUtils.ad_type_inter);
#if !UNITY_ANDROID
		Time.timeScale = 1f;

//		Debug.Log ("Inerstitial TimeScale = 1");

		if(SceneManager.GetActiveScene ().name == "SelectLevel" || SceneManager.GetActiveScene ().name == "Home")
		{
			SoundManager.PlayHomeBG ();
		}
		else{
			//SoundManager.PlayMainBG ();
			SoundManager.PlayMain2BG ();
		}
#endif
    }
    //Invoked when the Interstitial is Ready to shown after load function is called
    void InterstitialAdReadyEvent()
    {
        Debug.Log("InterstitialAdReadyEvent");
    }
    //Invoked when the Interstitial Ad Unit has opened
    void InterstitialAdOpenedEvent()
    {
        Debug.Log("InterstitialAdOpenedEvent");
    }

}
