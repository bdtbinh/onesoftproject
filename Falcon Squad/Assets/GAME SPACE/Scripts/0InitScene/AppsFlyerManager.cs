﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OneSoftGame.Tools;
using SkyGameKit;

public class AppsFlyerManager : PersistentSingleton<AppsFlyerManager>
{
    public string devKey = "Gio8zFFVzAZJGr9sbHXoVb";

    public string androidAppID = "invaders.os.galaxy.space.shooter.attack.classic";
    public string iosAppID = "IDFromItunesConnect";


    void Start()
    {
        if (!GameContext.IS_CHINA_VERSION)
        {
            InitAppsFlyer();
        }

    }


    void InitAppsFlyer()
    {
        try
        {
            //Application.runInBackground = true;
#if UNITY_IOS
            AppsFlyer.setAppID(iosAppID);
            AppsFlyer.setAppsFlyerKey(devKey);
            AppsFlyer.trackAppLaunch();
            AppsFlyer.getConversionData();
#elif UNITY_ANDROID
            AppsFlyer.setAppID(androidAppID);
            AppsFlyer.init(devKey);
            AppsFlyer.loadConversionData("StartUp");
            AppsFlyer.getConversionData();
            AppsFlyer.trackAppLaunch();

            Debug.Log("init AppsFlyer complete");
            //For Android Uninstall, fix soon
            //AppsFlyer.enableUninstallTracking ("YOUR_GCM_PROJECT_NUMBER");
#endif
        }
        catch (System.Exception ex)
        {
            Debug.LogError("AppsFlyer: " + ex.Message + " at " + ex.StackTrace);
        }
    }


    //    void Test()
    //    {

    //#if UNITY_IOS
    //        /* Mandatory - set your apple app ID
    //           NOTE: You should enter the number only and not the "ID" prefix */
    //        AppsFlyer.setAppID(Config.Apple_App_ID);
    //        AppsFlyer.trackAppLaunch();
    //        AppsFlyer.getConversionData();
    //        UnityEngine.iOS.NotificationServices.RegisterForNotifications(UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Sound);

    //#elif UNITY_ANDROID
    //        /// Mandatory - set your Android package name /
    //        AppsFlyer.setAppID(Config.package_name);
    //        /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
    //        AppsFlyer.init(Config.Appflyer_Dev_Key, "AppsFlyerTrackerCallbacks");
    //        AppsFlyer.createValidateInAppListener("AppsFlyerTrackerCallbacks", "onInAppBillingSuccess", "onInAppBillingFailure");
    //#endif


    //    }
}
