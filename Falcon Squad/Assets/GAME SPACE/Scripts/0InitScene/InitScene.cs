﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TCore;
using SkyGameKit;
using com.ootii.Messages;
using UnityEngine.SceneManagement;
using Mp.Pvp;
using System;

public class InitScene : MonoBehaviour
{

    private void Awake()
    {
        MessageDispatcher.AddListener(EventName.SyncData.SyncFromClientToServer.ToString(), OnSyncFromClientToServer, true);
        MessageDispatcher.AddListener(EventName.SyncData.SyncFromServerToClient.ToString(), OnSyncFromServerToClient, true);
        MessageDispatcher.AddListener(EventName.SyncData.NoSync.ToString(), OnNoSync, true);
        MessageDispatcher.AddListener(EventName.SyncData.StopCoroutineWait.ToString(), OnStopCoroutineWait, true);
        VipBonusValue.GetVipBonusValue();
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            LoadHome();
        }
        else
        {
            StartCoroutine(Wait5s());
        }
    }

    void OnStopCoroutineWait(IMessage msg)
    {
        Debug.Log("StopAllCoroutines");
        StopAllCoroutines();
    }

    IEnumerator Wait5s()
    {
        yield return new WaitForSeconds(10);
        Debug.Log("Load Home after coroutine");
        //new CSGetToken().Send();
        LoadHome();
    }
    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.SyncData.SyncFromClientToServer.ToString(), OnSyncFromClientToServer, true);
        MessageDispatcher.RemoveListener(EventName.SyncData.SyncFromServerToClient.ToString(), OnSyncFromServerToClient, true);
        MessageDispatcher.RemoveListener(EventName.SyncData.NoSync.ToString(), OnNoSync, true);
        MessageDispatcher.RemoveListener(EventName.SyncData.StopCoroutineWait.ToString(), OnStopCoroutineWait, true);
    }

    void OnNoSync(IMessage msg)
    {
        Debug.Log("OnNoSync");
        LoadHome();
    }

    void OnSyncFromServerToClient(IMessage msg)
    {
        Debug.Log("OnSyncFromServerToClient");
        DataSync data = msg.Data as DataSync;
        Debug.Log("CachePvp.SetPlayerData");
        CachePvp.SetPlayerData(data.player, data.token);
        Debug.Log("CSValidateToken");
        if (GameContext.IS_CHINA_VERSION)
        {
            new CSValidateToken(data.token, data.player.code, CachePvp.deviceId, CachePvp.GamecenterId).Send();
        }
        else
        {
            new CSValidateToken(data.token, data.player.code, CachePvp.deviceId, CachePvp.FacebookId).Send();
        }
        LoadHome();
    }

    void OnSyncFromClientToServer(IMessage msg)
    {
        Debug.Log("OnSyncFromClientToServer");
        if (GameContext.IS_CHINA_VERSION)
        {
            new CSGetToken(CachePvp.deviceId, CachePvp.GamecenterId).Send();
        }
        else
        {
            new CSGetToken(CachePvp.deviceId, CachePvp.FacebookId).Send();
        }

        LoadHome();
    }

    void Start()
    {
        //Firebase.Analytics.FirebaseAnalytics.LogEvent("OpenLoadStartScene");
        Time.timeScale = 1;
        DOTween.Init(true, true, LogBehaviour.Verbose).SetCapacity(330, 50);
        float ftest = 0;
        DOTween.To(() => ftest, x => ftest = x, 1f, 0.5f).SetUpdate(true).OnComplete(() =>
        {

        }).SetUpdate(true);

        //VipBonusValue.GetVipBonusValue();

        //StartCoroutine("LoadHome");
        //Invoke("LoadHome", 0.5f);
        //LoadHome();
    }


    void LoadHome()
    {

        //Debug.LogError("CacheGame.GetTutorialGame:-" + CacheGame.GetTutorialGame() + "  CacheGame.GetMaxLevel3Difficult:-" + CacheGame.GetMaxLevel3Difficult());
        if (CacheGame.GetTutorialGame() != 1 && CacheGame.GetMaxLevel3Difficult() < 2)
        {
            StartCoroutine(LoadLevelLevelTutorial());
        }
        else if (CacheGame.GetMaxLevel3Difficult() < 3)
        {
            StartCoroutine(LoadSelectLevelScene());
        }
        else
        {
            Application.LoadLevel("Home");
        }
    }


    IEnumerator LoadLevelLevelTutorial()
    {
        yield return new WaitUntil(() => ConfigLoaderInit.Instance.loadDataExcelCompleted == true);
        GameContext.modeGamePlay = GameContext.ModeGamePlay.Campaign;
        CacheGame.SetCurrentLevel(1);
        Application.LoadLevel("LevelTutorial");
    }

    IEnumerator LoadSelectLevelScene()
    {
        yield return new WaitUntil(() => ConfigLoaderInit.Instance.loadDataExcelCompleted == true);
        GameContext.modeGamePlay = GameContext.ModeGamePlay.Campaign;
        CacheGame.SetCurrentLevel(1);
        Application.LoadLevel("SelectLevel");
    }
}