﻿using OneSoftGame.Tools;
using SkyGameKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigLoaderInit : PersistentSingleton<ConfigLoaderInit>
{

    public bool loadDataExcelCompleted;
    // Use this for initialization
    void Awake()
    {
        base.Awake();
        ConfigLoader.Load();
		SerializableSet set;
        //====== Minh load data excel
		if (CachePvp.assetBundleEnable == 1 && CachePvp.serializableSet != null){
			set = CachePvp.serializableSet;
		}else{
			set = Resources.Load<SerializableSet>("SerializableSet");
		}
          
        Deserializer.Deserialize(set);
        Resources.UnloadUnusedAssets();

        loadDataExcelCompleted = true;

        Debug.Log("SerializableSet complete:" + AircraftSheet.GetDictionary().Count);
        //Debug.LogError(RankSheet.GetDictionary().Count);
        //Debug.LogError(AirC1BataFDSheet.GetDictionary().Count);

    }
}
