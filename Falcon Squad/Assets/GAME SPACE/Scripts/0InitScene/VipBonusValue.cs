﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VipBonusValue
{

    public static int ticketLuckyWheel = 0; // theo số lượng
    public static int itemLife = 0; // số lượng
    public static int itemPowerUp = 0; // số lượng
    public static float goldDropChangeRate = 0; //theo %
    public static float damePlaneRate = 0; //theo %
    public static float dameDroneRate = 0; //theo %
    public static int rewardsDailyTask = 0; //theo %


    //void Start()
    //{
    //    GetVipBonusValue();
    //}


    public static void GetVipBonusValue()
    {
        //CachePvp.myVip = 9;

        ticketLuckyWheel = 0;
        itemLife = 0;
        itemPowerUp = 0;
        goldDropChangeRate = 0;
        damePlaneRate = 0;
        dameDroneRate = 0;
        rewardsDailyTask = 0;

        int valueTest = 10;
        switch (CachePvp.myVip)
        //switch (valueTest)
        {
            case 1:
                BonusVip1();
                break;
            case 2:
                BonusVip1();
                BonusVip2();
                break;
            case 3:
                BonusVip1();
                BonusVip2();
                BonusVip3();
                break;
            case 4:
                BonusVip1();
                BonusVip2();
                BonusVip3();
                BonusVip4();
                break;
            case 5:
                BonusVip1();
                BonusVip2();
                BonusVip3();
                BonusVip4();
                BonusVip5();
                break;
            case 6:
                BonusVip1();
                BonusVip2();
                BonusVip3();
                BonusVip4();
                BonusVip5();
                BonusVip6();
                break;
            case 7:
                BonusVip1();
                BonusVip2();
                BonusVip3();
                BonusVip4();
                BonusVip5();
                BonusVip6();
                BonusVip7();
                break;
            case 8:
                BonusVip1();
                BonusVip2();
                BonusVip3();
                BonusVip4();
                BonusVip5();
                BonusVip6();
                BonusVip7();
                BonusVip8();
                break;
            case 9:
                BonusVip1();
                BonusVip2();
                BonusVip3();
                BonusVip4();
                BonusVip5();
                BonusVip6();
                BonusVip7();
                BonusVip8();
                BonusVip9();
                break;
            case 10:
                BonusVip1();
                BonusVip2();
                BonusVip3();
                BonusVip4();
                BonusVip5();
                BonusVip6();
                BonusVip7();
                BonusVip8();
                BonusVip9();
                BonusVip10();
                break;
            default:
                break;
        }

        //Debug.LogError("vip ticketLuckyWheel: "+ ticketLuckyWheel);
        //Debug.LogError("vip itemLife: " + itemLife);
        //Debug.LogError("vip itemPowerUp: " + itemPowerUp);
        //Debug.LogError("vip goldDropChangeRate: " + goldDropChangeRate);
        //Debug.LogError("vip damePlaneRate: " + damePlaneRate);
        //Debug.LogError("vip dameDroneRate: " + dameDroneRate);
        //Debug.LogError("vip rewardsDailyTask: " + rewardsDailyTask);
        Debug.Log("GetVipBonusValue");
    }



    public static void BonusVip1()
    {
        ticketLuckyWheel += 1;
    }

    public static void BonusVip2()
    {
        itemLife += 1;
        itemPowerUp += 1;
    }

    public static void BonusVip3()
    {
        goldDropChangeRate = 0.3f;
    }

    public static void BonusVip4()
    {
        goldDropChangeRate = 0.5f;
    }

    public static void BonusVip5()
    {
        goldDropChangeRate = 1f;
    }

    public static void BonusVip6()
    {
        damePlaneRate = 0.02f;
    }

    public static void BonusVip7()
    {
        damePlaneRate = 0.05f;
    }

    public static void BonusVip8()
    {
        damePlaneRate = 0.1f;
    }

    public static void BonusVip9()
    {
        dameDroneRate = 0.2f;
    }

    public static void BonusVip10()
    {
        rewardsDailyTask = 1;
    }


}
