﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneSignalManager : MonoBehaviour
{

    [SerializeField]
    private string appID = "0a97761c-3102-4916-9ab8-26182fe82af2";

    void Start()
    {
        //StartCoroutine(InitOneSignal());
        InitOneSignal();
    }


    void InitOneSignal()
    {
        //yield return new WaitUntil(() => GameContext.isLoadedHome == true);

        // Enable line below to enable logging if you are having issues setting up OneSignal. (logLevel, visualLogLevel)
        //OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.INFO, OneSignal.LOG_LEVEL.INFO);

        if (appID != "")
        {
            OneSignal.StartInit(appID)
          .HandleNotificationOpened(HandleNotificationOpened)
          .EndInit();

            OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;

        }
        else
        {
            Debug.LogError("Chua dien OneSignal App ID");
        }



        // Call syncHashedEmail anywhere in your app if you have the user's email.
        // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        // OneSignal.syncHashedEmail(userEmail);
    }


    // Gets called when the player opens the notification.
    private static void HandleNotificationOpened(OSNotificationOpenedResult result)
    {
    }
}
