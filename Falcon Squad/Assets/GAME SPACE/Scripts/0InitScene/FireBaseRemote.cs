﻿using Firebase.Analytics;
using OneSoftGame.Tools;
using System;
using System.Collections;
using System.Threading.Tasks;
//using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;


public class FireBaseRemote : PersistentSingleton<FireBaseRemote>
{


    private Vector2 controlsScrollViewVector = Vector2.zero;
    private Vector2 scrollViewVector = Vector2.zero;
    bool UIEnabled = true;
    private string logText = "";
    const int kMaxLogSize = 16382;
    Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
    public bool isFirebaseInitialized = false;

    // When the app starts, check to make sure that we have
    // the required dependencies to use Firebase, and if not,
    // add them if possible.

    public const string FALCON_ANDROID_SPEND = "falcon_android_spend";
    public const string FALCON_ANDROID_NOT_SPEND = "falcon_android_not_spend";
    public const string FALCON_AB_VIP_PACKAGE = "falcon_ab_price";
    public const string FALCON_AB_VIP_PACKAGE_20USD = "20usd";
    public const string FALCON_AB_VIP_PACKAGE_40USD = "40usd";
    public const string FALCON_AB_VIP_PACKAGE_PREDICTION = "prediction";



    public const string FALCON_URL_PVP = "falcon_url_pvp";
    public const string FALCON_CAN_PLAY_PVP = "falcon_can_play_pvp";
    public const string FALCON_LEVEL_UNLOCK_PVP = "falcon_level_unlock_pvp";
    public const string FALCON_LEVEL_UNLOCK_2VS2 = "falcon_level_unlock_2v2";

    public const string FALCON_LEVEL_UNLOCK_PLAYERINFO = "falcon_level_unlock_playerinfo";

    public const string FALCON_URL_PVP_DDOS = "falcon_url_pvp_servers";
    public const string FALCON_URL_TXT_INDEX_DDOS = "falcon_url_txt_index_ddos";

    public const string FALCON_URL_ASSET_BUNDLE = "falcon_url_asset_bundle";


    public const string FALCON_URL_OFFERWALL = "falcon_url_offerwall";
    public const string FALCON_LEVEL_UNLOCK_OFFERWALL = "falcon_level_unlock_offerwall";
    public const string FALCON_VERSION_UNLOCK_OFFERWALL = "falcon_version_unlock_offerwall";

    public const string FALCON_AB_STARTER_PACKAGE = "falcon_starter_pack_price";
    public const string FALCON_AB_STARTER_PACKAGE_1USD = "1usd";
    public const string FALCON_AB_STARTER_PACKAGE_2USD = "2usd";
    public const string FALCON_AB_STARTER_PACKAGE_PREDICTION = "prediction";

    public const string FALCON_URL_GIFTCODE = "falcon_url_giftcode";

    public const string FALCON_MAX_GEM_DROP_1DAY = "falcon_max_gem_drop_1day";
    public const string FALCON_MAX_CARD_DROP_1DAY = "falcon_max_card_drop_1day";

    public const string FALCON_NUMLEVEL_WHEN_GET_POWERUP = "falcon_numLevel_When_Get_PowerUp";

    public const string FALCON_TEST_14LEVEL_FIRST = "falcon_test_14level_first";
    public const string FALCON_TEST_LEVEL_BOSS = "falcon_test_level_boss";
    public const string FALCON_TEST_BUY_GOLD_BY_GEM = "falcon_test_buy_gold_by_gem";

    public const string FALCON_IS_REVIEW_VERSION = "falcon_is_review_version_china";
    public const string FALCON_TEST_VIDEO_ENDLEVEL = "falcon_test_video_endlevel";

    public const string FALCON_TEST_USE_LEVEL_UNLOCK_ARICRAFT = "falcon_test_use_unlock_aricraft";
    public const string FALCON_USE_RATE_IN_QUEST = "falcon_use_rate_in_quest";

    public const string FALCON_DAY_START_EVENT = "falcon_day_start_event_summer2019";
    public const string FALCON_DAY_FINISH_EVENT = "falcon_day_finish_event_summer2019";

    public const string FALCON_DAY_FINISH_PROMOTE_EVENT = "falcon_day_finish_promote_event_build228";

    public const string TEST_SHOW_ROYAL_PACK = "falcon_test_show_royal_pack";

    public const string ADMOB_HIGHER_PRICE = "falcon_admob_higher_price";
    



    //float t1, t2, t3;
    protected virtual void Awake()
    {
        base.Awake();
        //#if UNITY_ANDROID
        //        t1 = Time.realtimeSinceStartup;
        //#endif
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {

            dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                InitializeFirebase();

                //#if UNITY_ANDROID
                //                t2 = Time.realtimeSinceStartup;
                //                Debug.Log("<color=#00ff00>" + "InitializeFirebase time:" + +(t2 - t1) + "</color>");
                //#endif

                //Debug.LogError("InitializeFirebase:");
            }
            else
            {
                CachePvp.isFetchFirebase = true;
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });



        //StartCoroutine(GetUrlOfferWall());

        //Debug.LogError("bo cai nay di");
    }

    // Initialize remote config, and set the default values.
    void InitializeFirebase()
    {
        System.Collections.Generic.Dictionary<string, object> defaults =
          new System.Collections.Generic.Dictionary<string, object>();


        defaults.Add(FALCON_ANDROID_SPEND, false);
        defaults.Add(FALCON_ANDROID_NOT_SPEND, false);

        defaults.Add(FALCON_AB_VIP_PACKAGE, "20usd");
        defaults.Add(FALCON_AB_STARTER_PACKAGE, "1usd");


        defaults.Add(FALCON_URL_ASSET_BUNDLE, GameContext.FALCON_URL_ASSET_BUNDLE_DF);

        defaults.Add(FALCON_URL_OFFERWALL, GameContext.FALCON_URL_OFFERWALL_DF);
        defaults.Add(FALCON_LEVEL_UNLOCK_OFFERWALL, GameContext.LEVEL_UNLOCK_OFFERWALL_DF);
        defaults.Add(FALCON_VERSION_UNLOCK_OFFERWALL, 100);

        defaults.Add(FALCON_URL_PVP, PvpUtil.SERVER_URL_PVP);
        defaults.Add(FALCON_CAN_PLAY_PVP, PvpUtil.CAN_PLAY_PVP_VALUE);
        defaults.Add(FALCON_LEVEL_UNLOCK_PVP, PvpUtil.REQUIRE_LEVEL_PLAY_PVP);
        defaults.Add(FALCON_LEVEL_UNLOCK_2VS2, PvpUtil.REQUIRE_LEVEL_PLAY_2V2);

        defaults.Add(FALCON_LEVEL_UNLOCK_PLAYERINFO, PvpUtil.REQUIRE_LEVEL_UNLOCK_PLAYERINFO);

        defaults.Add(FALCON_URL_PVP_DDOS, PvpUtil.LIST_SERVERS_URL_PVP_DDOS);
        defaults.Add(FALCON_URL_TXT_INDEX_DDOS, PvpUtil.URL_TXT_INDEX_DDOS);

        defaults.Add(FALCON_URL_GIFTCODE, GameContext.FALCON_URL_GIFTCODE_DF);

        defaults.Add(FALCON_MAX_GEM_DROP_1DAY, GameContext.MAX_GEM_DROP_ONEDAY_DF);
        defaults.Add(FALCON_MAX_CARD_DROP_1DAY, GameContext.MAX_CARD_DROP_ONEDAY_DF);

        defaults.Add(FALCON_NUMLEVEL_WHEN_GET_POWERUP, GameContext.NUM_LEVEL_WHEN_GET_POWERUP);

        defaults.Add(FALCON_TEST_14LEVEL_FIRST, GameContext.TYPE_TEST_14LEVEL_FIRST_DF);
        defaults.Add(FALCON_TEST_LEVEL_BOSS, GameContext.TYPE_TEST_LEVEL_BOSS_DF);

        defaults.Add(FALCON_TEST_BUY_GOLD_BY_GEM, 1);
        defaults.Add(FALCON_IS_REVIEW_VERSION, 1);
        defaults.Add(FALCON_TEST_VIDEO_ENDLEVEL, 0);
        defaults.Add(FALCON_USE_RATE_IN_QUEST, 1);
        defaults.Add(FALCON_TEST_USE_LEVEL_UNLOCK_ARICRAFT, 1);
        defaults.Add(ADMOB_HIGHER_PRICE, GameContext.ADMOB_HIGHER_PRICE_DEFAULT_ID);

        defaults.Add(FALCON_DAY_START_EVENT, GameContext.FALCON_DAY_START_EVENT_DF);
        defaults.Add(FALCON_DAY_FINISH_EVENT, GameContext.FALCON_DAY_FINISH_EVENT_DF);

        defaults.Add(FALCON_DAY_FINISH_PROMOTE_EVENT, GameContext.FALCON_DAY_FINISH_PROMOTE_EVENT_DF);

        defaults.Add(TEST_SHOW_ROYAL_PACK, 1);


        Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
        Debug.Log("RemoteConfig configured and ready!");
        isFirebaseInitialized = true;
        FetchDataAsync();
    }



    public void DisplayData()
    {
        //Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_ANDROID_SPEND).BooleanValue);
        //Firebase.Analytics.FirebaseAnalytics.LogEvent("Prediction_Spend_" + Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_ANDROID_SPEND).BooleanValue, new Parameter("Purchased:", "" + CacheGame.GetUsedInapp()), new Parameter("MaxLevel:", "" + CacheGame.GetMaxLevel3Difficult()));
        //Firebase.Analytics.FirebaseAnalytics.LogEvent("Prediction_Not_Spend_" + Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_ANDROID_NOT_SPEND).BooleanValue, new Parameter("Purchased:", "" + CacheGame.GetUsedInapp()), new Parameter("MaxLevel:", "" + CacheGame.GetMaxLevel3Difficult()));

        SetNewData();
        GetUrlOfferWall();

        FalconFirebaseLogger.MaxLevel = CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL);
        FalconFirebaseLogger.SetDeviceUID();


        FirebaseLogSpaceWar.LogOtherEvent("InitFirebaseCompleted");

        //#if UNITY_ANDROID
        //        t3 = Time.realtimeSinceStartup;
        //        Debug.Log("<color=#00ff00>" + "InitializeFirebase DisplayData:" + +(t3 - t1) + "</color>");
        //#endif
        //Debug.LogError("DisplayData FireBaseRemote:");
    }

    void SetNewData()
    {
        CachePvp.RequireLevelPlayPvp = GetLevelUnlockModePvP();
        CachePvp.RequireLevelPlay2v2 = GetLevelUnlockMode2vs2();

        CachePvp.CanPlayPvp = GetPvpCanPlay();
        CachePvp.RequireLevelPlayerInfo = GetLevelUnlockPlayerInfo();
        CacheFireBase.GetSeverPvp = GetSeverPvp();
        CacheFireBase.GetUrlTxtDdos = GetUrlTxtDdos();

        CacheFireBase.GetUrlGiftCode = GetUrlGiftCode();
        CacheFireBase.GetUrlAssetBundle = GetUrlAssetBundle();
        CacheFireBase.GetLevelUnlockOfferWall = GetLevelUnlockOfferWall();
        CacheFireBase.GetVersionUnlockOfferWall = GetVersionUnlockOfferWall();
        CacheFireBase.GetMaxGemDropOneDay = GetMaxGemDropOneDay();
        CacheFireBase.GetMaxCardDropOneDay = GetMaxCardDropOneDay();
        CacheFireBase.GetNumLevelAdd_When_GetPowerUp = GetNumLevelAdd_When_GetPowerUp();
        CacheFireBase.GetType14LevelFirst = GetType14LevelFirst();
        CacheFireBase.GetTypeTestLevelBoss = GetTypeTestLevelBoss();
        CacheFireBase.GetTypeBuyGold = GetTypeBuyGold();

        CacheFireBase.GetIsReviewVersion = GetIsReviewVersion();

        CacheFireBase.GetTestShowVideoEndLevel = GetTestShowVideoEndLevel();

        CacheFireBase.GetUseRateInQuest = GetUseRateInQuest();

        CacheFireBase.GetUseLevelUnlockAircraft = GetUseLevelUnlockAircraft();

        CacheFireBase.GetDayStartEvent = GetDayStartEvent();
        CacheFireBase.GetDayFinishEvent = GetDayFinishEvent();

        CacheFireBase.GetDayFinishPromoteEvent = GetDayFinishPromoteEvent();

        CacheFireBase.GetTestShowRoyalPack = GetTestShowRoyalPack();

        CacheFireBase.GetAdmobHigherPriceIDs = GetAdmobHigherPriceIDs();



        PlayerPrefs.Save();
        //CachePvp.ServerUrl = "http://123.30.186.226:9988/falcon-squad/";
        //Debug.LogError("bo cai nay di");

        //Debug.Log("<color=#00ff00>" + "uri:" + CachePvp.ServerUrl + "</color>");
        Debug.Log("<color=#00ff00>" + "canPlayPvp:" + CachePvp.CanPlayPvp + "</color>");
        Debug.Log("<color=#00ff00>" + "RequireLevelPlayPvp:" + CachePvp.RequireLevelPlayPvp + "</color>");
        Debug.Log("<color=#00ff00>" + "RequireLevelPlayerInfo:" + CachePvp.RequireLevelPlayerInfo + "</color>");
    }


    //"http://api.falconsquad.net:9988/falcon-squad/;http://35.188.16.223:9988/falcon-squad/"

    public int GetIsReviewVersion()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_IS_REVIEW_VERSION).StringValue))
        {
            return 0;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_IS_REVIEW_VERSION).StringValue);
    }

    public string GetAdmobHigherPriceIDs()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(ADMOB_HIGHER_PRICE).StringValue))
        {
            return GameContext.ADMOB_HIGHER_PRICE_DEFAULT_ID;
        }
        return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(ADMOB_HIGHER_PRICE).StringValue;
    }



    public string GetDayStartEvent()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_DAY_START_EVENT).StringValue))
        {
            return GameContext.FALCON_DAY_START_EVENT_DF;
        }
        return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_DAY_START_EVENT).StringValue;
    }

    public string GetDayFinishEvent()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_DAY_FINISH_EVENT).StringValue))
        {
            return GameContext.FALCON_DAY_FINISH_EVENT_DF;
        }
        return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_DAY_FINISH_EVENT).StringValue;
    }

    public string GetDayFinishPromoteEvent()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_DAY_FINISH_PROMOTE_EVENT).StringValue))
        {
            return GameContext.FALCON_DAY_FINISH_PROMOTE_EVENT_DF;
        }
        return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_DAY_FINISH_PROMOTE_EVENT).StringValue;
    }



    public int GetTestShowVideoEndLevel()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_TEST_VIDEO_ENDLEVEL).StringValue))
        {
            return 0;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_TEST_VIDEO_ENDLEVEL).StringValue);
    }

    public int GetUseRateInQuest()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_USE_RATE_IN_QUEST).StringValue))
        {
            return 1;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_USE_RATE_IN_QUEST).StringValue);
    }

    public int GetUseLevelUnlockAircraft()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_TEST_USE_LEVEL_UNLOCK_ARICRAFT).StringValue))
        {
            return 1;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_TEST_USE_LEVEL_UNLOCK_ARICRAFT).StringValue);
    }



    public string GetSeverPvp()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_URL_PVP_DDOS).StringValue))
        {
            return PvpUtil.LIST_SERVERS_URL_PVP_DDOS;
        }
        return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_URL_PVP_DDOS).StringValue;
    }

    //0;1;2 : 0,1,2 bi chet
    public string GetUrlTxtDdos()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_URL_TXT_INDEX_DDOS).StringValue))
        {
            return PvpUtil.URL_TXT_INDEX_DDOS;
        }
        return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_URL_TXT_INDEX_DDOS).StringValue;
    }


    public string GetUrlGiftCode()
    {

        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_URL_GIFTCODE).StringValue))
        {
            return GameContext.FALCON_URL_GIFTCODE_DF;
        }
        return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_URL_GIFTCODE).StringValue;
    }


    public string GetUrlAssetBundle()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_URL_ASSET_BUNDLE).StringValue))
        {
            return GameContext.FALCON_URL_ASSET_BUNDLE_DF;
        }
        return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_URL_ASSET_BUNDLE).StringValue;
    }

    public string GetType14LevelFirst()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_TEST_14LEVEL_FIRST).StringValue))
        {
            return GameContext.TYPE_TEST_14LEVEL_FIRST_DF;
        }
        return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_TEST_14LEVEL_FIRST).StringValue;
    }

    public string GetTypeTestLevelBoss()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_TEST_LEVEL_BOSS).StringValue))
        {
            return GameContext.TYPE_TEST_LEVEL_BOSS_DF;
        }
        return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_TEST_LEVEL_BOSS).StringValue;
    }


    public int GetTypeBuyGold()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_TEST_BUY_GOLD_BY_GEM).StringValue))
        {
            return 1;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_TEST_BUY_GOLD_BY_GEM).StringValue);
    }

    public int GetPvpCanPlay()
    {
        //Debug.LogError("FALCON_CAN_PLAY_PVP: " + Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_CAN_PLAY_PVP).StringValue);
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_CAN_PLAY_PVP).StringValue))
        {
            return PvpUtil.CAN_PLAY_PVP_VALUE;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_CAN_PLAY_PVP).StringValue);
    }

    public int GetTestShowRoyalPack()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(TEST_SHOW_ROYAL_PACK).StringValue))
        {
            return 1;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(TEST_SHOW_ROYAL_PACK).StringValue);
    }




    public int GetLevelUnlockModePvP()
    {
        //return 0;
        //Debug.LogError("FALCON_LEVEL_UNLOCK_PVP: " + Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_LEVEL_UNLOCK_PVP).StringValue);
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_LEVEL_UNLOCK_PVP).StringValue))
        {
            return PvpUtil.REQUIRE_LEVEL_PLAY_PVP;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_LEVEL_UNLOCK_PVP).StringValue);
    }

    public int GetLevelUnlockMode2vs2()
    {
        //return 0;
        //Debug.LogError("FALCON_LEVEL_UNLOCK_PVP: " + Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_LEVEL_UNLOCK_PVP).StringValue);
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_LEVEL_UNLOCK_2VS2).StringValue))
        {
            return PvpUtil.REQUIRE_LEVEL_PLAY_2V2;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_LEVEL_UNLOCK_2VS2).StringValue);
    }

    void GetUrlOfferWall()
    {
        //yield return new WaitUntil(() => CachePvp.isFetchFirebase == true);
        //Debug.LogError("GetUrlOfferWall FirebaseRomote" + Time.frameCount);
        string urlRemote;
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_URL_OFFERWALL).StringValue))
        {
            urlRemote = GameContext.FALCON_URL_OFFERWALL_DF;
        }
        else
        {
            urlRemote = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_URL_OFFERWALL).StringValue;
        }
        //RestAPIClient.Instance.UpdateBaseUrl(urlRemote);
    }


    public int GetLevelUnlockOfferWall()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_LEVEL_UNLOCK_OFFERWALL).StringValue))
        {
            return GameContext.LEVEL_UNLOCK_OFFERWALL_DF;
        }

        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_LEVEL_UNLOCK_OFFERWALL).StringValue);
    }

    public int GetVersionUnlockOfferWall()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_VERSION_UNLOCK_OFFERWALL).StringValue))
        {
            return 100;
        }

        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_VERSION_UNLOCK_OFFERWALL).StringValue);
    }

    public int GetLevelUnlockPlayerInfo()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_LEVEL_UNLOCK_PLAYERINFO).StringValue))
        {
            return PvpUtil.REQUIRE_LEVEL_UNLOCK_PLAYERINFO;
        }

        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_LEVEL_UNLOCK_PLAYERINFO).StringValue);
    }


    public int GetMaxGemDropOneDay()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_MAX_GEM_DROP_1DAY).StringValue))
        {
            return GameContext.MAX_GEM_DROP_ONEDAY_DF;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_MAX_GEM_DROP_1DAY).StringValue);
    }

    public int GetMaxCardDropOneDay()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_MAX_CARD_DROP_1DAY).StringValue))
        {
            return GameContext.MAX_CARD_DROP_ONEDAY_DF;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_MAX_CARD_DROP_1DAY).StringValue);
    }

    public int GetNumLevelAdd_When_GetPowerUp()
    {
        if (string.IsNullOrEmpty(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_NUMLEVEL_WHEN_GET_POWERUP).StringValue))
        {
            return GameContext.NUM_LEVEL_WHEN_GET_POWERUP;
        }
        return int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_NUMLEVEL_WHEN_GET_POWERUP).StringValue);
    }



    public int GetVipPackType()
    {
        return 1;
        String promotedBundle = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_AB_VIP_PACKAGE).StringValue;
        bool willSpend = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_ANDROID_SPEND).BooleanValue;
        if (promotedBundle.Equals(FALCON_AB_VIP_PACKAGE_PREDICTION) && willSpend)
        {
            return 2;
        }
        else if (promotedBundle.Equals(FALCON_AB_VIP_PACKAGE_PREDICTION))
        {
            return 1;
        }

        if (promotedBundle.Equals(FALCON_AB_VIP_PACKAGE_40USD))
            return 2;

        return 1;

    }


    public int GetStarterPackType()
    {
        return 1;
        String promotedBundle = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_AB_STARTER_PACKAGE).StringValue;
        bool willSpend = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(FALCON_ANDROID_SPEND).BooleanValue;
        if (promotedBundle.Equals(FALCON_AB_STARTER_PACKAGE_PREDICTION) && willSpend)
        {
            return 2;
        }
        else if (promotedBundle.Equals(FALCON_AB_STARTER_PACKAGE_PREDICTION))
        {
            return 1;
        }

        if (promotedBundle.Equals(FALCON_AB_STARTER_PACKAGE_2USD))
            return 2;

        return 1;

    }


    //-----------------------------------//-------------------------------------------------------------------------------------
    public void DisplayAllKeys()
    {
        Debug.Log("Current Keys:");
        System.Collections.Generic.IEnumerable<string> keys =
            Firebase.RemoteConfig.FirebaseRemoteConfig.Keys;
        foreach (string key in keys)
        {
            Debug.Log("    " + key);
        }
        Debug.Log("GetKeysByPrefix(\"config_test_s\"):");
        keys = Firebase.RemoteConfig.FirebaseRemoteConfig.GetKeysByPrefix("config_test_s");
        foreach (string key in keys)
        {
            Debug.Log("    " + key);
        }
    }

    // Start a fetch request.
    public Task FetchDataAsync()
    {
        Debug.Log("Fetching data...");
        // FetchAsync only fetches new data if the current data is older than the provided
        // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
        // By default the timespan is 12 hours, and for production apps, this is a good
        // number.  For this example though, it's set to a timespan of zero, so that
        // changes in the console will always show up immediately.
        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(
            TimeSpan.Zero);
        return fetchTask.ContinueWith(FetchComplete);
    }

    void FetchComplete(Task fetchTask)
    {
        if (fetchTask.IsCanceled)
        {
            Debug.Log("Fetch canceled.");
        }
        else if (fetchTask.IsFaulted)
        {
            Debug.Log("Fetch encountered an error.");
        }
        else if (fetchTask.IsCompleted)
        {
            Debug.Log("Fetch completed successfully!");
        }

        var info = Firebase.RemoteConfig.FirebaseRemoteConfig.Info;
        switch (info.LastFetchStatus)
        {
            case Firebase.RemoteConfig.LastFetchStatus.Success:
                Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();
                Debug.Log(String.Format("Remote data loaded and ready (last fetch time {0}).",
                                       info.FetchTime));
                DisplayData();
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Failure:
                switch (info.LastFetchFailureReason)
                {
                    case Firebase.RemoteConfig.FetchFailureReason.Error:
                        Debug.Log("Fetch failed for unknown reason");
                        break;
                    case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                        Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                        break;
                }
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Pending:
                Debug.Log("Latest Fetch call still pending.");
                break;
        }
        //Debug.LogError(" CachePvp.isFetchFirebase = true; " + Time.frameCount);
        CachePvp.isFetchFirebase = true;
    }



}