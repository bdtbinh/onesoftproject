﻿using UnityEngine;
using Sirenix.OdinInspector;

public class VersionManager : MonoBehaviour
{
    [SerializeField]
    [MinValue(1)]
    private int version;

    private void Awake()
    {

#if UNITY_IOS
        Application.targetFrameRate = 60;
#endif

        if(MinhCacheGame.GetVersion() < 0)
        {
            if (CacheGame.HasFirstSessionGame())
            {
                MinhCacheGame.SetVersion(0);
            }
            else
            {
                MinhCacheGame.SetVersion(version);
            }
        }
    }
}
