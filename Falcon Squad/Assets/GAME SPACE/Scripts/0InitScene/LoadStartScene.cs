﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadStartScene : MonoBehaviour
{
    void Start()
    {
        //Debug.unityLogger.logEnabled = false;

        //Debug.LogError(Application.identifier);
        Time.timeScale = 1;
        InitSdkChina();

        DOTween.Init(true, true, LogBehaviour.ErrorsOnly).SetCapacity(330, 50);
        float ftest = 0;
        DOTween.To(() => ftest, x => ftest = x, 1f, 0.5f).SetUpdate(true).OnComplete(() =>
        {
        }).SetUpdate(true);

        Application.LoadLevel("InitScene");
    }


    void InitSdkChina()
    {
        //GA.StartWithAppKeyAndReportPolicyAndChannelId("58ca7198a325110fee00139a", ReportPolicy.BATCH, "App Store");
    }
}
