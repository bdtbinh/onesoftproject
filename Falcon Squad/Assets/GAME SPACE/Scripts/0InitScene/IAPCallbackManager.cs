﻿using OneSoftGame.Tools;
using EasyMobile;
using System.Collections;
using UnityEngine;
using UnityEngine.Purchasing.Security;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class IAPCallbackManager : PersistentSingleton<IAPCallbackManager>
{
    public event Action<string> OnPurchaseSuccessed;

    private bool isReadyReadReceipt = false;
    private bool isClickBtnPurchase = false;

    int vipPointTrack;

    public enum WherePurchase
    {
        unknown,
        push_server, // mua khi server push về
        shop, // mua khi tự mở popup lên mua 
        shop_sale // mua vàng và gem trong lúc đang được x2

    }
    [HideInInspector]
    public string wherePurchaseLog;
    [HideInInspector]
    public string nameItemPurchase;

    public bool isServerPush = false; // biẾn này dùng để check xem 1 thằng nó mua hàng khi cái popup push từ server hiện lên hay mua theo luồng click bình thường

    #region ------Dictionary : Get Vip Point By Product Name-------------

    public Dictionary<string, int> GetPointByProductName = new Dictionary<string, int> {

        { EM_IAPConstants.Product_inapp_gold_1usd, 1 },
        { EM_IAPConstants.Product_inapp_2usd, 2 },
        { EM_IAPConstants.Product_inapp_gold_5usd, 5 },
        { EM_IAPConstants.Product_inapp_10usd, 10 },
        { EM_IAPConstants.Product_inapp_20usd, 20 },
        { EM_IAPConstants.Product_inapp_gold_50usd, 50 },
        { EM_IAPConstants.Product_inapp_100usd, 100 },

        { EM_IAPConstants.Product_inapp_starterpack_v2, 3 },
        { EM_IAPConstants.Product_inapp_starterpack_v3, 2 },

        { EM_IAPConstants.Product_inapp_premiumpack, 10 },
        { EM_IAPConstants.Product_inapp_premiumpack_sale2, 10 },

        { EM_IAPConstants.Product_inapp_vippack_beforesale, 60 },
        { EM_IAPConstants.Product_inapp_vippack, 40 },
        { EM_IAPConstants.Product_inapp_vippack_sale_lv3, 20 },
        { EM_IAPConstants.Product_inapp_vippack_sale_lv2, 20 },

        { EM_IAPConstants.Product_inapp_luckywheel, 2 },

        { EM_IAPConstants.Product_inapp_gem_1usd,1 },
        { EM_IAPConstants.Product_inapp_gem_2usd,2 },
        { EM_IAPConstants.Product_inapp_gem_5usd, 5 },
        { EM_IAPConstants.Product_inapp_gem_10usd, 10 },
        { EM_IAPConstants.Product_inapp_gem_20usd, 20 },
        { EM_IAPConstants.Product_inapp_gem_50usd, 50 },
        { EM_IAPConstants.Product_inapp_gem_100usd, 100 },
        { EM_IAPConstants.Product_inapp_gold_infinitypack_new, 10 },

        { EM_IAPConstants.Product_inapp_event_black_1usd, 1 },
        { EM_IAPConstants.Product_inapp_event_black_2usd, 2 },
        { EM_IAPConstants.Product_inapp_event_black_5usd, 5 },
        { EM_IAPConstants.Product_inapp_event_black_10usd, 10 },
        { EM_IAPConstants.Product_inapp_event_black_15usd, 15 },
        { EM_IAPConstants.Product_inapp_event_black_20usd, 20 },
        { EM_IAPConstants.Product_inapp_event_black_50usd, 50 },
        { EM_IAPConstants.Product_inapp_event_black_100usd, 100 },

        { EM_IAPConstants.Product_card_great_offer_5, 5 },
        { EM_IAPConstants.Product_card_great_offer_10, 10 },
        { EM_IAPConstants.Product_card_great_offer_20, 20 },

        { EM_IAPConstants.Product_unlock_aircraft_2, 10 },
        { EM_IAPConstants.Product_unlock_aircraft_3, 20 },
        { EM_IAPConstants.Product_unlock_aircraft_6, 20 },
        { EM_IAPConstants.Product_unlock_aircraft_7, 30 },
        { EM_IAPConstants.Product_unlock_aircraft_8, 20 },
        { EM_IAPConstants.Product_unlock_aircraft_9, 20 },

        { EM_IAPConstants.Product_unlock_drone_2, 5 },
        { EM_IAPConstants.Product_unlock_drone_3, 10 },
        { EM_IAPConstants.Product_unlock_drone_4, 5 },
        { EM_IAPConstants.Product_unlock_drone_5, 10 },
        { EM_IAPConstants.Product_unlock_drone_6, 15 },

        { EM_IAPConstants.Product_unlock_wing_1, 5 },
        { EM_IAPConstants.Product_unlock_wing_2, 20 },
        { EM_IAPConstants.Product_unlock_wing_3, 10 },


        { EM_IAPConstants.Product_aircraft_2_sale_25, 7 },
        { EM_IAPConstants.Product_aircraft_3_sale_25, 15 },
        { EM_IAPConstants.Product_aircraft_6_sale_25, 15 },
        { EM_IAPConstants.Product_aircraft_7_sale_25, 22 },
        { EM_IAPConstants.Product_aircraft_8_sale_25, 15 },
        { EM_IAPConstants.Product_aircraft_9_sale_25, 15 },

        { EM_IAPConstants.Product_aircraft_2_sale_50, 5 },
        { EM_IAPConstants.Product_aircraft_3_sale_50, 10 },
        { EM_IAPConstants.Product_aircraft_6_sale_50, 10 },
        { EM_IAPConstants.Product_aircraft_7_sale_50, 15 },
        { EM_IAPConstants.Product_aircraft_8_sale_50, 10 },
        { EM_IAPConstants.Product_aircraft_9_sale_50, 10 },

        { EM_IAPConstants.Product_inapp_royalpack, 5 },
    };
    #endregion

    private void Start()
    {
        InAppPurchasing.InitializeSucceeded += OnIAPInitSuccessed;
    }

    private void OnIAPInitSuccessed()
    {
        InAppPurchasing.PurchaseCompleted += PurchaseCompletedHandler;
        InAppPurchasing.PurchaseFailed += PurchaseFailedHandler;
        StartCoroutine("WaitForReadyReadReceipt");
    }

    IEnumerator WaitForReadyReadReceipt()
    {
        yield return new WaitForSecondsRealtime(8f);
        isReadyReadReceipt = true;
    }

    public void BuyProduct(string productName, WherePurchase wherePurchase, string nameItemBuy = "")
    {
        if (isClickBtnPurchase)
            return;

        if (!isReadyReadReceipt)
            return;

        coWaitForClickBtnPurchase = WaitForClickBtnPurchase();
        StartCoroutine(coWaitForClickBtnPurchase);

        InAppPurchasing.Purchase(productName);
        wherePurchaseLog = wherePurchase.ToString();
        nameItemPurchase = nameItemBuy;
#if !UNITY_EDITOR
        PopupManagerCuong.Instance.ShowPopupLoadingPurchase();
#endif
    }

    IEnumerator coWaitForClickBtnPurchase;

    IEnumerator WaitForClickBtnPurchase()
    {
        isClickBtnPurchase = true;

#if UNITY_ANDROID
        yield return new WaitForSecondsRealtime(Constant.ANDROID_CLICK_PURCHASE_WAITING_TIME);
#else
		yield return new WaitForSecondsRealtime (Constant.IOS_CLICK_PURCHASE_WAITING_TIME);
#endif
        isClickBtnPurchase = false;
    }

    private void PurchaseFailedHandler(IAPProduct product)
    {
        Debug.Log("Purchase failed! -- " + product.Id);
        PopupManagerCuong.Instance.HidePopupLoadingPurchase();
    }


    private void PurchaseCompletedHandler(IAPProduct product)
    {
        Debug.Log("Purchase Completed! -- " + product.Id);
        //Debug.LogError("IAPCallbackManager PurchaseCompletedHandler product:" + product);
        if (!IOSRestorePurchase.clickIOSRestorePurchase)
        {
            if (product != null && product.Name != null)
            {
                ReadGooglePlayReceipt(product.Name);
            }
        }
        PopupManagerCuong.Instance.HidePopupLoadingPurchase();
    }




    void ReadGooglePlayReceipt(string productName)
    {
#if EM_UIAP
        //Debug.LogError("step1:" + productName);
#if UNITY_ANDROID
        GooglePlayReceipt receipt = InAppPurchasing.GetGooglePlayReceipt(productName);
#else
//		AppleInAppPurchaseReceipt receipt = IAPManager.GetAppleIAPReceipt (productName);
		AppleInAppPurchaseReceipt receipt = InAppPurchasing.GetAppleIAPReceipt (productName);
#endif
        //Debug.LogError("step2:" + productName);
        if (receipt != null)
        {
            Debug.Log(isReadyReadReceipt + " * " + SceneManager.GetActiveScene().name);

            if (isReadyReadReceipt && SceneManager.GetActiveScene().name != "InitScene")
            {
                //Debug.LogError("step3:" + productName);
                CacheGame.SetUsedInapp(1);
                //Debug.LogError("step4:" + productName);
                if (OnPurchaseSuccessed != null)
                    OnPurchaseSuccessed(productName);
                //Debug.LogError("step5:" + productName);
                //Debug.LogError("receipt.productID:" + receipt.productID);
                //Debug.LogError("receipt.transactionID:" + receipt.transactionID);
                //Debug.LogError("receipt.packageName:" + receipt.packageName);
                //Debug.LogError("receipt.purchaseToken:" + receipt.purchaseToken);
                //Debug.LogError("receipt.purchaseDate.ToString():" + receipt.purchaseDate.ToString());
                CuongUtils.TrackAppflyerPurchaseOld(productName);
                //#if UNITY_ANDROID
                //                CuongUtils.TrackAppflyerPurchase(productName);
                //#else
                //				CuongUtils.TrackAppflyerPurchaseOld(productName);
                //#endif
                //CuongUtils.TrackFacebookPurchase(productName);
                if (GetPointByProductName.ContainsKey(productName) && GetPointByProductName[productName] != null)
                {
                    vipPointTrack = GetPointByProductName[productName];
                    CachePvp.myVipPoint += vipPointTrack;
                    Debug.Log("Vip Point Purchase Add:" + vipPointTrack);

#if UNITY_ANDROID
                    CachePvp.Instance.LogIAPToServer(nameItemPurchase, CuongUtils.GetLocalizedPrice(productName).ToString(), CuongUtils.GetCurrencyCode(productName), wherePurchaseLog, vipPointTrack, receipt.productID, receipt.transactionID, receipt.packageName, receipt.purchaseToken, receipt.purchaseDate.ToString());
#else
					CachePvp.Instance.LogIAPToServer (nameItemPurchase, CuongUtils.GetLocalizedPrice (productName).ToString (), CuongUtils.GetCurrencyCode (productName), wherePurchaseLog, vipPointTrack, receipt.productID, "IOStransactionID", "IOSpackageName", "IOSpurchaseToken", receipt.purchaseDate.ToString ());
#endif
                    FirebaseLogSpaceWar.LogInappPurchase(productName, vipPointTrack);
                }

                PvpUtil.SendUpdatePlayer("ReadGooglePlayReceipt");
            }
        }

#endif
    }


}
