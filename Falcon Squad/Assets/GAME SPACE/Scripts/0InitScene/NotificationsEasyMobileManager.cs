﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;
using EasyMobile.MiniJSON;
using OneSoftGame.Tools;
using EasyMobile;
using I2.Loc;

public class NotificationsEasyMobileManager : PersistentSingleton<NotificationsEasyMobileManager>
{


    //public const string 

    //public const string[] MESS_1DAY = "Lycky Wheel is available FREE now. Check it out!";
    //public const string MESS_3DAY = "Don't miss something new in Falcon Squad. Join now!";
    //public const string MESS_7DAY = "Long time no see. We have new features for you. Let's check it!";


    public string categoryId;

    //public InputField idInputField;
    //public DemoUtils demoUtils;

    string orgNotificationListText;

    void Start()
    {
        Init();
        CancelAllPendingLocalNotifications();
    }


    public void OnApplicationQuit()
    {
        //ScheduleLocalNotification("Quit" + MESS_1DAY, 26);
        //ScheduleLocalNotification("Quit" + MESS_3DAY, 72);
        //ScheduleLocalNotification("Quit" + MESS_7DAY, 168);
        int maxIndex1Day = 3;
        int maxIndex3Day = 3;
        int maxIndex7Day = 3;

        int index1Day = AntiCheat.GetInt("Notifi_User_1_Day_Offline", 1);
        int index3Day = AntiCheat.GetInt("Notifi_User_3_Day_Offline", 1);
        int index7Day = AntiCheat.GetInt("Notifi_User_7_Day_Offline", 1);

        ScheduleLocalNotification(GetMess1Day(index1Day), 24);
        AntiCheat.SetInt("Notifi_User_1_Day_Offline", index1Day >= maxIndex1Day ? 1 : index1Day + 1);

        ScheduleLocalNotification(GetMess3Day(index3Day), 72);
        AntiCheat.SetInt("Notifi_User_3_Day_Offline", index3Day >= maxIndex3Day ? 1 : index3Day + 1);

        ScheduleLocalNotification(GetMess7Day(index7Day), 168);
        AntiCheat.SetInt("Notifi_User_7_Day_Offline", index7Day >= maxIndex7Day ? 1 : index7Day + 1);
    }

    private string GetMess1Day(int index)
    {
        switch (index)
        {
            case 1:
                return ScriptLocalization.MESS_1DAY_1;

            case 2:
                return ScriptLocalization.MESS_1DAY_2;

            case 3:
                return ScriptLocalization.MESS_1DAY_3;

            default:
                return ScriptLocalization.MESS_1DAY_1;
        }
    }

    private string GetMess3Day(int index)
    {
        switch (index)
        {
            case 1:
                return ScriptLocalization.MESS_3DAY_1;

            case 2:
                return ScriptLocalization.MESS_3DAY_2;

            case 3:
                return ScriptLocalization.MESS_3DAY_3;

            default:
                return ScriptLocalization.MESS_3DAY_1;
        }
    }

    private string GetMess7Day(int index)
    {
        switch (index)
        {
            case 1:
                return ScriptLocalization.MESS_7DAY_1;

            case 2:
                return ScriptLocalization.MESS_7DAY_2;

            case 3:
                return ScriptLocalization.MESS_7DAY_3;

            default:
                return ScriptLocalization.MESS_7DAY_1;
        }
    }


    public void Init()
    {
        if (Notifications.IsInitialized())
        {
            //NativeUI.Alert("Already Initialized", "Notification module is already initalized.");
            return;
        }

        Notifications.Init();
    }

    public void ScheduleLocalNotification(string contentBody, int timedelay)
    {
        if (!InitCheck())
            return;

        NotificationContent content = PrepareNotificationContent(contentBody);

        DateTime triggerDate = DateTime.Now + new TimeSpan(timedelay, 0, 0);

        //NativeUI.Alert("Notifications", "Notification module is already initalized.");
        string id = Notifications.ScheduleLocalNotification(triggerDate, content);
    }




    NotificationContent PrepareNotificationContent(string contentBody)
    {
        NotificationContent content = new NotificationContent();

        // Provide the notification title.
        content.title = "Falcon Squad";

        // You can optionally provide the notification subtitle, which is visible on iOS only.
        content.subtitle = "Protectors Of The Galaxy";

        // Provide the notification message.
        content.body = contentBody;

        // You can optionally attach custom user information to the notification
        // in form of a key-value dictionary.
        content.userInfo = new Dictionary<string, object>();
        content.userInfo.Add("string", "OK");
        content.userInfo.Add("number", 3);
        content.userInfo.Add("bool", true);
        content.userInfo.Add("newUpdate", true);
        content.categoryId = categoryId;

        //content.smallIcon = "YOUR_CUSTOM_SMALL_ICON";
        //content.largeIcon = "YOUR_CUSTOM_LARGE_ICON";

        return content;
    }

    public void CancelAllPendingLocalNotifications()
    {
        if (!InitCheck())
            return;
        Notifications.CancelAllPendingLocalNotifications();
        //NativeUI.Alert("Alert", "Canceled all pending local notifications of this app.");
    }

    public void RemoveAllDeliveredNotifications()
    {
        Notifications.ClearAllDeliveredNotifications();
        //NativeUI.Alert("Alert", "Cleared all shown notifications of this app.");
    }

    bool InitCheck()
    {
        bool isInit = Notifications.IsInitialized();

        if (!isInit)
        {
            //NativeUI.Alert("Alert", "Please initialize first.");
        }
        Debug.Log("isInit Notifications.IsInitialized: " + isInit);
        return isInit;
    }


}
