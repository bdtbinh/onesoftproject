﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChinaStartScene : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        StartCoroutine(OpenLoadStartScene());
    }

    IEnumerator OpenLoadStartScene()
    {
        yield return new WaitForSeconds(6);
        Application.LoadLevel("LoadStart");
    }
}
