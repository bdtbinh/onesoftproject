﻿using System.Collections.Generic;

[System.Serializable]
public class AirC9IceShardSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;

    /// <summary>
    /// mainweapon_quantity
    /// </summary>
    public int mainweapon_quantity;

    /// <summary>
    /// mainweapon_speed
    /// </summary>
    public float mainweapon_speed;

    /// <summary>
    /// secondweapon_power
    /// </summary>
    public int secondweapon_power;

    /// <summary>
    /// secondweapon_speed
    /// </summary>
    public float secondweapon_speed;


    private static Dictionary<int, AirC9IceShardSheet> dictionary = new Dictionary<int, AirC9IceShardSheet>();

    /// <summary>
    /// 通过level获取AirC9IceShardSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>AirC9IceShardSheet的实例</returns>
    public static AirC9IceShardSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, AirC9IceShardSheet> GetDictionary()
    {
        return dictionary;
    }
}
