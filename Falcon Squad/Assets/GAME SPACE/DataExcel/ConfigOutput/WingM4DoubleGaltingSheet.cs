﻿using System.Collections.Generic;

[System.Serializable]
public class WingM4DoubleGaltingSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;


    private static Dictionary<int, WingM4DoubleGaltingSheet> dictionary = new Dictionary<int, WingM4DoubleGaltingSheet>();

    /// <summary>
    /// 通过level获取WingM4DoubleGaltingSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingM4DoubleGaltingSheet的实例</returns>
    public static WingM4DoubleGaltingSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingM4DoubleGaltingSheet> GetDictionary()
    {
        return dictionary;
    }
}
