﻿using System.Collections.Generic;

[System.Serializable]
public class WingM5HomingMissileSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;


    private static Dictionary<int, WingM5HomingMissileSheet> dictionary = new Dictionary<int, WingM5HomingMissileSheet>();

    /// <summary>
    /// 通过level获取WingM5HomingMissileSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingM5HomingMissileSheet的实例</returns>
    public static WingM5HomingMissileSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingM5HomingMissileSheet> GetDictionary()
    {
        return dictionary;
    }
}
