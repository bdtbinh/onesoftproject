﻿using System.Collections.Generic;

[System.Serializable]
public class DailyLogin30Sheet
{
    /// <summary>
    /// index
    /// </summary>
    public int index;

    /// <summary>
    /// name
    /// </summary>
    public string name;

    /// <summary>
    /// amount
    /// </summary>
    public int amount;

    /// <summary>
    /// phần thưởng giá trị (1 = giá trị, 0 = không giá trị)
    /// </summary>
    public int valuable;


    private static Dictionary<int, DailyLogin30Sheet> dictionary = new Dictionary<int, DailyLogin30Sheet>();

    /// <summary>
    /// 通过index获取DailyLogin30Sheet的实例
    /// </summary>
    /// <param name="index">索引</param>
    /// <returns>DailyLogin30Sheet的实例</returns>
    public static DailyLogin30Sheet Get(int index)
    {
        return dictionary[index];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, DailyLogin30Sheet> GetDictionary()
    {
        return dictionary;
    }
}
