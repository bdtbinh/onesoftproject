﻿using System.Collections.Generic;

[System.Serializable]
public class WingMConvertPowerSheet
{
    /// <summary>
    /// levelOld
    /// </summary>
    public int levelOld;

    /// <summary>
    /// levelNew
    /// </summary>
    public int levelNew;


    private static Dictionary<int, WingMConvertPowerSheet> dictionary = new Dictionary<int, WingMConvertPowerSheet>();

    /// <summary>
    /// 通过levelOld获取WingMConvertPowerSheet的实例
    /// </summary>
    /// <param name="levelOld">索引</param>
    /// <returns>WingMConvertPowerSheet的实例</returns>
    public static WingMConvertPowerSheet Get(int levelOld)
    {
        return dictionary[levelOld];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingMConvertPowerSheet> GetDictionary()
    {
        return dictionary;
    }
}
