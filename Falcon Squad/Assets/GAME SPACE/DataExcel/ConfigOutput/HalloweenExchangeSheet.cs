﻿using System.Collections.Generic;

[System.Serializable]
public class HalloweenExchangeSheet
{
    /// <summary>
    /// index
    /// </summary>
    public int index;

    /// <summary>
    /// name
    /// </summary>
    public string name;

    /// <summary>
    /// amount
    /// </summary>
    public int amount;

    /// <summary>
    /// quantity
    /// </summary>
    public int quantity;

    /// <summary>
    /// requirement
    /// </summary>
    public int requirement;


    private static Dictionary<int, HalloweenExchangeSheet> dictionary = new Dictionary<int, HalloweenExchangeSheet>();

    /// <summary>
    /// 通过index获取HalloweenExchangeSheet的实例
    /// </summary>
    /// <param name="index">索引</param>
    /// <returns>HalloweenExchangeSheet的实例</returns>
    public static HalloweenExchangeSheet Get(int index)
    {
        return dictionary[index];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, HalloweenExchangeSheet> GetDictionary()
    {
        return dictionary;
    }
}
