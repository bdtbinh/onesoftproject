﻿using System.Collections.Generic;

[System.Serializable]
public class WingOfRedemption2Sheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;


    private static Dictionary<int, WingOfRedemption2Sheet> dictionary = new Dictionary<int, WingOfRedemption2Sheet>();

    /// <summary>
    /// 通过level获取WingOfRedemption2Sheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingOfRedemption2Sheet的实例</returns>
    public static WingOfRedemption2Sheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingOfRedemption2Sheet> GetDictionary()
    {
        return dictionary;
    }
}
