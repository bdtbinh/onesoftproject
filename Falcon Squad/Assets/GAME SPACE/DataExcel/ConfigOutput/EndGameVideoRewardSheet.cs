﻿using System.Collections.Generic;

[System.Serializable]
public class EndGameVideoRewardSheet
{
    /// <summary>
    /// key
    /// </summary>
    public string NAME_ID;

    /// <summary>
    /// list type gift
    /// </summary>
    public string[] typeGiftList;

    /// <summary>
    /// list num gift
    /// </summary>
    public int[] numGiftList;


    private static Dictionary<string, EndGameVideoRewardSheet> dictionary = new Dictionary<string, EndGameVideoRewardSheet>();

    /// <summary>
    /// 通过NAME_ID获取EndGameVideoRewardSheet的实例
    /// </summary>
    /// <param name="NAME_ID">索引</param>
    /// <returns>EndGameVideoRewardSheet的实例</returns>
    public static EndGameVideoRewardSheet Get(string NAME_ID)
    {
        return dictionary[NAME_ID];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<string, EndGameVideoRewardSheet> GetDictionary()
    {
        return dictionary;
    }
}
