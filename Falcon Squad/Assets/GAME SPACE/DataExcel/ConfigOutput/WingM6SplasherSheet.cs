﻿using System.Collections.Generic;

[System.Serializable]
public class WingM6SplasherSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;


    private static Dictionary<int, WingM6SplasherSheet> dictionary = new Dictionary<int, WingM6SplasherSheet>();

    /// <summary>
    /// 通过level获取WingM6SplasherSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingM6SplasherSheet的实例</returns>
    public static WingM6SplasherSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingM6SplasherSheet> GetDictionary()
    {
        return dictionary;
    }
}
