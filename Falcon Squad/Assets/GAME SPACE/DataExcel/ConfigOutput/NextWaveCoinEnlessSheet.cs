﻿using System.Collections.Generic;

[System.Serializable]
public class NextWaveCoinEnlessSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int numWave;

    /// <summary>
    /// Resources Get Khi Pass Wave
    /// </summary>
    public int NumResourcesGet;


    private static Dictionary<int, NextWaveCoinEnlessSheet> dictionary = new Dictionary<int, NextWaveCoinEnlessSheet>();

    /// <summary>
    /// 通过numWave获取NextWaveCoinEnlessSheet的实例
    /// </summary>
    /// <param name="numWave">索引</param>
    /// <returns>NextWaveCoinEnlessSheet的实例</returns>
    public static NextWaveCoinEnlessSheet Get(int numWave)
    {
        return dictionary[numWave];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, NextWaveCoinEnlessSheet> GetDictionary()
    {
        return dictionary;
    }
}
