﻿using System.Collections.Generic;

[System.Serializable]
public class WingmanSheet
{
    /// <summary>
    /// 4
    /// </summary>
    public int index;

    /// <summary>
    /// name
    /// </summary>
    public string name;

    /// <summary>
    /// C = 1,B = 2,A = 3,S = 4,SS = 5,SSS = 6
    /// </summary>
    public int begin_rank;

    /// <summary>
    /// index_rank
    /// </summary>
    public string index_rank;

    /// <summary>
    /// using_skill_number
    /// </summary>
    public int using_skill_number;

    /// <summary>
    /// card_unlock
    /// </summary>
    public int card_unlock;

    /// <summary>
    /// level_unlock
    /// </summary>
    public int level_unlock;

    /// <summary>
    /// gem_unlock
    /// </summary>
    public int gem_unlock;

    /// <summary>
    /// gold_unlock
    /// </summary>
    public int gold_unlock;

    /// <summary>
    /// purchase_unlock
    /// </summary>
    public string purchase_id;


    private static Dictionary<int, WingmanSheet> dictionary = new Dictionary<int, WingmanSheet>();

    /// <summary>
    /// 通过index获取WingmanSheet的实例
    /// </summary>
    /// <param name="index">索引</param>
    /// <returns>WingmanSheet的实例</returns>
    public static WingmanSheet Get(int index)
    {
        return dictionary[index];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingmanSheet> GetDictionary()
    {
        return dictionary;
    }
}
