﻿using System.Collections.Generic;

[System.Serializable]
public class StarChestSheet
{
    /// <summary>
    /// .
    /// </summary>
    public string level;

    /// <summary>
    /// 1
    /// </summary>
    public string TypeGift1;

    /// <summary>
    /// 1
    /// </summary>
    public int ValueGift1;

    /// <summary>
    /// 2
    /// </summary>
    public string TypeGift2;

    /// <summary>
    /// 2
    /// </summary>
    public int ValueGift2;

    /// <summary>
    /// 3
    /// </summary>
    public string TypeGift3;

    /// <summary>
    /// 3
    /// </summary>
    public int ValueGift3;


    private static Dictionary<string, StarChestSheet> dictionary = new Dictionary<string, StarChestSheet>();

    /// <summary>
    /// 通过level获取StarChestSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>StarChestSheet的实例</returns>
    public static StarChestSheet Get(string level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<string, StarChestSheet> GetDictionary()
    {
        return dictionary;
    }
}
