﻿using System.Collections.Generic;

[System.Serializable]
public class RankSheet
{
    /// <summary>
    /// index
    /// </summary>
    public int index;

    /// <summary>
    /// rank
    /// </summary>
    public string rank;

    /// <summary>
    /// max_level
    /// </summary>
    public int max_level;

    /// <summary>
    /// card_evolve
    /// </summary>
    public int card_evolve;

    /// <summary>
    /// gem_evolve
    /// </summary>
    public int gem_evolve;


    private static Dictionary<int, RankSheet> dictionary = new Dictionary<int, RankSheet>();

    /// <summary>
    /// 通过index获取RankSheet的实例
    /// </summary>
    /// <param name="index">索引</param>
    /// <returns>RankSheet的实例</returns>
    public static RankSheet Get(int index)
    {
        return dictionary[index];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, RankSheet> GetDictionary()
    {
        return dictionary;
    }
}
