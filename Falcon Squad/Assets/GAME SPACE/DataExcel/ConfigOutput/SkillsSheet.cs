﻿using System.Collections.Generic;

[System.Serializable]
public class SkillsSheet
{
    /// <summary>
    /// index
    /// </summary>
    public int index;

    /// <summary>
    /// name
    /// </summary>
    public string name;


    private static Dictionary<int, SkillsSheet> dictionary = new Dictionary<int, SkillsSheet>();

    /// <summary>
    /// 通过index获取SkillsSheet的实例
    /// </summary>
    /// <param name="index">索引</param>
    /// <returns>SkillsSheet的实例</returns>
    public static SkillsSheet Get(int index)
    {
        return dictionary[index];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, SkillsSheet> GetDictionary()
    {
        return dictionary;
    }
}
