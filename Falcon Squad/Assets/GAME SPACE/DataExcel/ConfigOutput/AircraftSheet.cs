﻿using System.Collections.Generic;

[System.Serializable]
public class AircraftSheet
{
    /// <summary>
    /// index
    /// </summary>
    public int index;

    /// <summary>
    /// name
    /// </summary>
    public string name;

    /// <summary>
    /// C = 1,B = 2,A = 3,S = 4,SS = 5,SSS = 6
    /// </summary>
    public int begin_rank;

    /// <summary>
    /// index_rank
    /// </summary>
    public string index_rank;

    /// <summary>
    /// card_unlock
    /// </summary>
    public int card_unlock;

    /// <summary>
    /// level_unlock
    /// </summary>
    public int level_unlock;

    /// <summary>
    /// gem_unlock
    /// </summary>
    public int gem_unlock;

    /// <summary>
    /// gold_unlock
    /// </summary>
    public int gold_unlock;

    /// <summary>
    /// purchase unlock
    /// </summary>
    public string purchase_id;

    /// <summary>
    /// purchase unlock_sale25
    /// </summary>
    public string purchase_sale25;

    /// <summary>
    /// purchase unlock_sale50
    /// </summary>
    public string purchase_sale50;

    /// <summary>
    /// skill_rank
    /// </summary>
    public int skill_rank_c;

    /// <summary>
    /// 
    /// </summary>
    public int skill_rank_b;

    /// <summary>
    /// 
    /// </summary>
    public int skill_rank_a;

    /// <summary>
    /// 
    /// </summary>
    public int skill_rank_s;

    /// <summary>
    /// 
    /// </summary>
    public int skill_rank_ss;

    /// <summary>
    /// 
    /// </summary>
    public int skill_rank_sss;

    /// <summary>
    /// using_skill_number
    /// </summary>
    public int using_skill_number;

    /// <summary>
    /// using_skill_time
    /// </summary>
    public float using_skill_time;

    /// <summary>
    /// using_skill_cooldown_time
    /// </summary>
    public float using_skill_cooldown_time;

    /// <summary>
    /// activevalue_rank_c
    /// </summary>
    public int activevalue_rank_c;

    /// <summary>
    /// 
    /// </summary>
    public int activevalue_rank_b;

    /// <summary>
    /// 
    /// </summary>
    public int activevalue_rank_a;

    /// <summary>
    /// 
    /// </summary>
    public int activevalue_rank_s;

    /// <summary>
    /// 
    /// </summary>
    public int activevalue_rank_ss;

    /// <summary>
    /// 
    /// </summary>
    public int activevalue_rank_sss;


    private static Dictionary<int, AircraftSheet> dictionary = new Dictionary<int, AircraftSheet>();

    /// <summary>
    /// 通过index获取AircraftSheet的实例
    /// </summary>
    /// <param name="index">索引</param>
    /// <returns>AircraftSheet的实例</returns>
    public static AircraftSheet Get(int index)
    {
        return dictionary[index];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, AircraftSheet> GetDictionary()
    {
        return dictionary;
    }
}
