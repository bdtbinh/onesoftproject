﻿using System.Collections.Generic;

[System.Serializable]
public class WingM2AutoGaltingSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;


    private static Dictionary<int, WingM2AutoGaltingSheet> dictionary = new Dictionary<int, WingM2AutoGaltingSheet>();

    /// <summary>
    /// 通过level获取WingM2AutoGaltingSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingM2AutoGaltingSheet的实例</returns>
    public static WingM2AutoGaltingSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingM2AutoGaltingSheet> GetDictionary()
    {
        return dictionary;
    }
}
