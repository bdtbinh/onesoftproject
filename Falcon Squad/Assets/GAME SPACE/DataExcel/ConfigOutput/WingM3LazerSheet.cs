﻿using System.Collections.Generic;

[System.Serializable]
public class WingM3LazerSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;


    private static Dictionary<int, WingM3LazerSheet> dictionary = new Dictionary<int, WingM3LazerSheet>();

    /// <summary>
    /// 通过level获取WingM3LazerSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingM3LazerSheet的实例</returns>
    public static WingM3LazerSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingM3LazerSheet> GetDictionary()
    {
        return dictionary;
    }
}
