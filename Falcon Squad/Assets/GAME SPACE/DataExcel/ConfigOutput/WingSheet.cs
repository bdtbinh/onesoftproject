﻿using System.Collections.Generic;

[System.Serializable]
public class WingSheet
{
    /// <summary>
    /// index
    /// </summary>
    public int index;

    /// <summary>
    /// name
    /// </summary>
    public string name;

    /// <summary>
    /// C = 1,B = 2,A = 3,S = 4,SS = 5,SSS = 6
    /// </summary>
    public int begin_rank;

    /// <summary>
    /// index_rank
    /// </summary>
    public string index_rank;

    /// <summary>
    /// card_unlock
    /// </summary>
    public int card_unlock;

    /// <summary>
    /// level_unlock
    /// </summary>
    public int level_unlock;

    /// <summary>
    /// gem_unlock
    /// </summary>
    public int gem_unlock;

    /// <summary>
    /// gold_unlock
    /// </summary>
    public int gold_unlock;

    /// <summary>
    /// purchase unlock
    /// </summary>
    public string purchase_id;

    /// <summary>
    /// skill_rank
    /// </summary>
    public int skill_rank_c;

    /// <summary>
    /// 
    /// </summary>
    public int skill_rank_b;

    /// <summary>
    /// 
    /// </summary>
    public int skill_rank_a;

    /// <summary>
    /// 
    /// </summary>
    public int skill_rank_s;

    /// <summary>
    /// 
    /// </summary>
    public int skill_rank_ss;

    /// <summary>
    /// 
    /// </summary>
    public int skill_rank_sss;

    /// <summary>
    /// using_skill_number
    /// </summary>
    public int using_skill_number;

    /// <summary>
    /// using_skill_time
    /// </summary>
    public float using_skill_time;

    /// <summary>
    /// using_skill_cooldown_time
    /// </summary>
    public float using_skill_cooldown_time;


    private static Dictionary<int, WingSheet> dictionary = new Dictionary<int, WingSheet>();

    /// <summary>
    /// 通过index获取WingSheet的实例
    /// </summary>
    /// <param name="index">索引</param>
    /// <returns>WingSheet的实例</returns>
    public static WingSheet Get(int index)
    {
        return dictionary[index];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingSheet> GetDictionary()
    {
        return dictionary;
    }
}
