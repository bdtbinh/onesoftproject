﻿using System.Collections.Generic;

[System.Serializable]
public class XMasAccumulatedSheet
{
    /// <summary>
    /// index
    /// </summary>
    public int index;

    /// <summary>
    /// name
    /// </summary>
    public string name;

    /// <summary>
    /// amount
    /// </summary>
    public int amount;

    /// <summary>
    /// requirement
    /// </summary>
    public int requirement;


    private static Dictionary<int, XMasAccumulatedSheet> dictionary = new Dictionary<int, XMasAccumulatedSheet>();

    /// <summary>
    /// 通过index获取XMasAccumulatedSheet的实例
    /// </summary>
    /// <param name="index">索引</param>
    /// <returns>XMasAccumulatedSheet的实例</returns>
    public static XMasAccumulatedSheet Get(int index)
    {
        return dictionary[index];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, XMasAccumulatedSheet> GetDictionary()
    {
        return dictionary;
    }
}
