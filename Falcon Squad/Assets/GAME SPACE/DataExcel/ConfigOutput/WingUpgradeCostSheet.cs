﻿using System.Collections.Generic;

[System.Serializable]
public class WingUpgradeCostSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// Đk nâng cấp
    /// </summary>
    public int gem;

    /// <summary>
    /// Đk nâng cấp
    /// </summary>
    public int gold;

    /// <summary>
    /// Đk nâng cấp
    /// </summary>
    public int energy;


    private static Dictionary<int, WingUpgradeCostSheet> dictionary = new Dictionary<int, WingUpgradeCostSheet>();

    /// <summary>
    /// 通过level获取WingUpgradeCostSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingUpgradeCostSheet的实例</returns>
    public static WingUpgradeCostSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingUpgradeCostSheet> GetDictionary()
    {
        return dictionary;
    }
}
