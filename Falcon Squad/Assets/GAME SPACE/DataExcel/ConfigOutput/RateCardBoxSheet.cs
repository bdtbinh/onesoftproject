﻿using System.Collections.Generic;

[System.Serializable]
public class RateCardBoxSheet
{
    /// <summary>
    /// get theo key
    /// </summary>
    public int KeyGet;

    /// <summary>
    /// 
    /// </summary>
    public string Type;

    /// <summary>
    /// Index
    /// </summary>
    public int Index;

    /// <summary>
    /// tên máy bay
    /// </summary>
    public string Name;

    /// <summary>
    /// 
    /// </summary>
    public int BoxCard1;

    /// <summary>
    /// số lượng card add
    /// </summary>
    public int BoxCard1Num;

    /// <summary>
    /// 
    /// </summary>
    public int BoxCard2;

    /// <summary>
    /// số lượng card add
    /// </summary>
    public int BoxCard2Num;

    /// <summary>
    /// 
    /// </summary>
    public int BoxCard3;

    /// <summary>
    /// số lượng card add
    /// </summary>
    public int BoxCard3Num;

    /// <summary>
    /// 
    /// </summary>
    public int BoxCard4;

    /// <summary>
    /// số lượng card add
    /// </summary>
    public int BoxCard4Num;


    private static Dictionary<int, RateCardBoxSheet> dictionary = new Dictionary<int, RateCardBoxSheet>();

    /// <summary>
    /// 通过KeyGet获取RateCardBoxSheet的实例
    /// </summary>
    /// <param name="KeyGet">索引</param>
    /// <returns>RateCardBoxSheet的实例</returns>
    public static RateCardBoxSheet Get(int KeyGet)
    {
        return dictionary[KeyGet];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, RateCardBoxSheet> GetDictionary()
    {
        return dictionary;
    }
}
