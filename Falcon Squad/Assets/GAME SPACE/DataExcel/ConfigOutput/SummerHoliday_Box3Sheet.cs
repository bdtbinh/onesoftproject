﻿using System.Collections.Generic;

[System.Serializable]
public class SummerHoliday_Box3Sheet
{
    /// <summary>
    /// index
    /// </summary>
    public int index;

    /// <summary>
    /// name
    /// </summary>
    public string name;

    /// <summary>
    /// amount
    /// </summary>
    public int amount;

    /// <summary>
    /// Duy nhập% vào đây
    /// </summary>
    public float percent;

    /// <summary>
    /// Minh dùng để code
    /// </summary>
    public float total_percent;

    /// <summary>
    /// phần thưởng giá trị (1 = giá trị, 0 = không giá trị)
    /// </summary>
    public int valuable;

    /// <summary>
    /// Cần tag event: 1, không cần tag event: 0
    /// </summary>
    public int event_tag;


    private static Dictionary<int, SummerHoliday_Box3Sheet> dictionary = new Dictionary<int, SummerHoliday_Box3Sheet>();

    /// <summary>
    /// 通过index获取SummerHoliday_Box3Sheet的实例
    /// </summary>
    /// <param name="index">索引</param>
    /// <returns>SummerHoliday_Box3Sheet的实例</returns>
    public static SummerHoliday_Box3Sheet Get(int index)
    {
        return dictionary[index];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, SummerHoliday_Box3Sheet> GetDictionary()
    {
        return dictionary;
    }
}
