﻿using System.Collections.Generic;

[System.Serializable]
public class AirCConvertPowerSheet
{
    /// <summary>
    /// levelOld
    /// </summary>
    public int levelOld;

    /// <summary>
    /// levelNew
    /// </summary>
    public int levelNew;


    private static Dictionary<int, AirCConvertPowerSheet> dictionary = new Dictionary<int, AirCConvertPowerSheet>();

    /// <summary>
    /// 通过levelOld获取AirCConvertPowerSheet的实例
    /// </summary>
    /// <param name="levelOld">索引</param>
    /// <returns>AirCConvertPowerSheet的实例</returns>
    public static AirCConvertPowerSheet Get(int levelOld)
    {
        return dictionary[levelOld];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, AirCConvertPowerSheet> GetDictionary()
    {
        return dictionary;
    }
}
