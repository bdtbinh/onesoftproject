﻿using UnityEngine;
using Sirenix.OdinInspector;
using com.ootii.Messages;
using TCore;

public class PopupXMas : MonoBehaviour {

    //[SerializeField]
    //private GameObject glowBtnCandy;

    //[SerializeField]
    //private GameObject glowBtnPumpkin;

    [Button]
    public void AddXMasBells()
    {
        MinhCacheGame.AddRemoveEventResources(GameContext.EVENT_XMAS_2018, 1500, true);
        MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, GameContext.EVENT_XMAS_2018, 0);
    }

    [SerializeField]
    private GameObject panelLeaderboard;

    [SerializeField]
    private GameObject paneAccumulated;

    private void OnEnable()
    {
        paneAccumulated.SetActive(true);
        panelLeaderboard.SetActive(false);
        //glowBtnCandy.SetActive(true);
        //glowBtnPumpkin.SetActive(false);
    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
    }

    public void OnClickBtnBell()
    {
        SoundManager.PlayClickButton();
        paneAccumulated.SetActive(true);
        panelLeaderboard.SetActive(false);

        //glowBtnCandy.SetActive(true);
        //glowBtnPumpkin.SetActive(false);
    }

    public void OnClickBtnLeaderboard()
    {
        SoundManager.PlayClickButton();
        paneAccumulated.SetActive(false);
        panelLeaderboard.SetActive(true);

        //glowBtnCandy.SetActive(false);
        //glowBtnPumpkin.SetActive(true);
    }

    public void OnClickBtnPlay()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowSelectLevelPopup();
    }

    public void OnClickBtnAdd()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowBuyEventItemPopup();
    }
}
