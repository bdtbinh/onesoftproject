﻿using System.Collections.Generic;

[System.Serializable]
public class WingOfResolution3Sheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;


    private static Dictionary<int, WingOfResolution3Sheet> dictionary = new Dictionary<int, WingOfResolution3Sheet>();

    /// <summary>
    /// 通过level获取WingOfResolution3Sheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingOfResolution3Sheet的实例</returns>
    public static WingOfResolution3Sheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingOfResolution3Sheet> GetDictionary()
    {
        return dictionary;
    }
}
