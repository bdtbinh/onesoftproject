﻿using System.Collections.Generic;

[System.Serializable]
public class WingOfJustice1Sheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;


    private static Dictionary<int, WingOfJustice1Sheet> dictionary = new Dictionary<int, WingOfJustice1Sheet>();

    /// <summary>
    /// 通过level获取WingOfJustice1Sheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingOfJustice1Sheet的实例</returns>
    public static WingOfJustice1Sheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingOfJustice1Sheet> GetDictionary()
    {
        return dictionary;
    }
}
