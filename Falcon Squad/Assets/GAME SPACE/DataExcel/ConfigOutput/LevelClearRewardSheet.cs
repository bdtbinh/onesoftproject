﻿using System.Collections.Generic;

[System.Serializable]
public class LevelClearRewardSheet
{
    /// <summary>
    /// get theo level
    /// </summary>
    public int level;

    /// <summary>
    /// 1
    /// </summary>
    public string TypeGift1;

    /// <summary>
    /// 1
    /// </summary>
    public int ValueGift1;

    /// <summary>
    /// 2
    /// </summary>
    public string TypeGift2;

    /// <summary>
    /// 2
    /// </summary>
    public int ValueGift2;

    /// <summary>
    /// 3
    /// </summary>
    public string TypeGift3;

    /// <summary>
    /// 3
    /// </summary>
    public int ValueGift3;

    /// <summary>
    /// 4
    /// </summary>
    public string TypeGift4;

    /// <summary>
    /// 4
    /// </summary>
    public int ValueGift4;

    /// <summary>
    /// 5
    /// </summary>
    public string TypeGift5;

    /// <summary>
    /// 5
    /// </summary>
    public int ValueGift5;

    /// <summary>
    /// 6
    /// </summary>
    public string TypeGift6;

    /// <summary>
    /// 6
    /// </summary>
    public int ValueGift6;


    private static Dictionary<int, LevelClearRewardSheet> dictionary = new Dictionary<int, LevelClearRewardSheet>();

    /// <summary>
    /// 通过level获取LevelClearRewardSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>LevelClearRewardSheet的实例</returns>
    public static LevelClearRewardSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, LevelClearRewardSheet> GetDictionary()
    {
        return dictionary;
    }
}
