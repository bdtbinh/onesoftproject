﻿using System.Collections.Generic;

[System.Serializable]
public class LuckyBoxGoldSheet
{
    /// <summary>
    /// index
    /// </summary>
    public int index;

    /// <summary>
    /// name
    /// </summary>
    public string name;

    /// <summary>
    /// amount
    /// </summary>
    public int amount;

    /// <summary>
    /// Bình nhập% vào đây
    /// </summary>
    public float percent;

    /// <summary>
    /// Minh dùng để code
    /// </summary>
    public float total_percent;

    /// <summary>
    /// phần thưởng giá trị (1 = giá trị, 0 = không giá trị)
    /// </summary>
    public int valuable;

    /// <summary>
    /// Số lượng đặc biệt (dùng cho event)
    /// </summary>
    public int special_amount;

    /// <summary>
    /// Số thứ tự vật phẩm đặc biệt
    /// </summary>
    public int special_item_order;


    private static Dictionary<int, LuckyBoxGoldSheet> dictionary = new Dictionary<int, LuckyBoxGoldSheet>();

    /// <summary>
    /// 通过index获取LuckyBoxGoldSheet的实例
    /// </summary>
    /// <param name="index">索引</param>
    /// <returns>LuckyBoxGoldSheet的实例</returns>
    public static LuckyBoxGoldSheet Get(int index)
    {
        return dictionary[index];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, LuckyBoxGoldSheet> GetDictionary()
    {
        return dictionary;
    }
}
