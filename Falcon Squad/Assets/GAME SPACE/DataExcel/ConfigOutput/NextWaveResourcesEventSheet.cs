﻿using System.Collections.Generic;

[System.Serializable]
public class NextWaveResourcesEventSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int numWave;

    /// <summary>
    /// Resources Get Khi Pass Wave
    /// </summary>
    public int NumResourcesGet;


    private static Dictionary<int, NextWaveResourcesEventSheet> dictionary = new Dictionary<int, NextWaveResourcesEventSheet>();

    /// <summary>
    /// 通过numWave获取NextWaveResourcesEventSheet的实例
    /// </summary>
    /// <param name="numWave">索引</param>
    /// <returns>NextWaveResourcesEventSheet的实例</returns>
    public static NextWaveResourcesEventSheet Get(int numWave)
    {
        return dictionary[numWave];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, NextWaveResourcesEventSheet> GetDictionary()
    {
        return dictionary;
    }
}
