﻿using System.Collections.Generic;

[System.Serializable]
public class WingM1GaltingSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;


    private static Dictionary<int, WingM1GaltingSheet> dictionary = new Dictionary<int, WingM1GaltingSheet>();

    /// <summary>
    /// 通过level获取WingM1GaltingSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingM1GaltingSheet的实例</returns>
    public static WingM1GaltingSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingM1GaltingSheet> GetDictionary()
    {
        return dictionary;
    }
}
