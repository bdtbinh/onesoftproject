﻿using System.Collections.Generic;

[System.Serializable]
public class AirC10ThunderBoltSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// mainweapon_power
    /// </summary>
    public int mainweapon_power;

    /// <summary>
    /// mainweapon_quantity
    /// </summary>
    public int mainweapon_quantity;

    /// <summary>
    /// mainweapon_speed
    /// </summary>
    public float mainweapon_speed;

    /// <summary>
    /// secondweapon_power
    /// </summary>
    public int secondweapon_power;

    /// <summary>
    /// secondweapon_speed
    /// </summary>
    public float secondweapon_speed;


    private static Dictionary<int, AirC10ThunderBoltSheet> dictionary = new Dictionary<int, AirC10ThunderBoltSheet>();

    /// <summary>
    /// 通过level获取AirC10ThunderBoltSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>AirC10ThunderBoltSheet的实例</returns>
    public static AirC10ThunderBoltSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, AirC10ThunderBoltSheet> GetDictionary()
    {
        return dictionary;
    }
}
