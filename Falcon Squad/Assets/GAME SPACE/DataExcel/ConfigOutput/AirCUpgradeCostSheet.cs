﻿using System.Collections.Generic;

[System.Serializable]
public class AirCUpgradeCostSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// Đk nâng cấp
    /// </summary>
    public int gem;

    /// <summary>
    /// Đk nâng cấp
    /// </summary>
    public int gold;


    private static Dictionary<int, AirCUpgradeCostSheet> dictionary = new Dictionary<int, AirCUpgradeCostSheet>();

    /// <summary>
    /// 通过level获取AirCUpgradeCostSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>AirCUpgradeCostSheet的实例</returns>
    public static AirCUpgradeCostSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, AirCUpgradeCostSheet> GetDictionary()
    {
        return dictionary;
    }
}
