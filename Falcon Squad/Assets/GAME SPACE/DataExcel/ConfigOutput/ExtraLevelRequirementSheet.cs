﻿using System.Collections.Generic;

[System.Serializable]
public class ExtraLevelRequirementSheet
{
    /// <summary>
    /// level extra
    /// </summary>
    public int level;

    /// <summary>
    /// index cua aircraft, bat dau tu 1
    /// </summary>
    public int aircraft_index;

    /// <summary>
    /// C = 1,B = 2,A = 3,S = 4,SS = 5,SSS = 6
    /// </summary>
    public int rank;


    private static Dictionary<int, ExtraLevelRequirementSheet> dictionary = new Dictionary<int, ExtraLevelRequirementSheet>();

    /// <summary>
    /// 通过level获取ExtraLevelRequirementSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>ExtraLevelRequirementSheet的实例</returns>
    public static ExtraLevelRequirementSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, ExtraLevelRequirementSheet> GetDictionary()
    {
        return dictionary;
    }
}
