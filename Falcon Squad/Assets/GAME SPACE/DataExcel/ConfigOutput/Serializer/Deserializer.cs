﻿public class Deserializer
{
    public static void Deserialize(SerializableSet set)
    {
       
        for (int i = 0, l = set.Ranks.Length; i < l; i++)
        {
            RankSheet.GetDictionary().Add(set.Ranks[i].index, set.Ranks[i]);
        }
       
        for (int i = 0, l = set.Skillss.Length; i < l; i++)
        {
            SkillsSheet.GetDictionary().Add(set.Skillss[i].index, set.Skillss[i]);
        }
       
        for (int i = 0, l = set.AirC10ThunderBolts.Length; i < l; i++)
        {
            AirC10ThunderBoltSheet.GetDictionary().Add(set.AirC10ThunderBolts[i].level, set.AirC10ThunderBolts[i]);
        }
       
        for (int i = 0, l = set.AirC1BataFDs.Length; i < l; i++)
        {
            AirC1BataFDSheet.GetDictionary().Add(set.AirC1BataFDs[i].level, set.AirC1BataFDs[i]);
        }
       
        for (int i = 0, l = set.AirC2SkyWraiths.Length; i < l; i++)
        {
            AirC2SkyWraithSheet.GetDictionary().Add(set.AirC2SkyWraiths[i].level, set.AirC2SkyWraiths[i]);
        }
       
        for (int i = 0, l = set.AirC3FuryOfAress.Length; i < l; i++)
        {
            AirC3FuryOfAresSheet.GetDictionary().Add(set.AirC3FuryOfAress[i].level, set.AirC3FuryOfAress[i]);
        }
       
        for (int i = 0, l = set.AirC6MacBirds.Length; i < l; i++)
        {
            AirC6MacBirdSheet.GetDictionary().Add(set.AirC6MacBirds[i].level, set.AirC6MacBirds[i]);
        }
       
        for (int i = 0, l = set.AirC7TwilightXs.Length; i < l; i++)
        {
            AirC7TwilightXSheet.GetDictionary().Add(set.AirC7TwilightXs[i].level, set.AirC7TwilightXs[i]);
        }
       
        for (int i = 0, l = set.AirC8Starbombs.Length; i < l; i++)
        {
            AirC8StarbombSheet.GetDictionary().Add(set.AirC8Starbombs[i].level, set.AirC8Starbombs[i]);
        }
       
        for (int i = 0, l = set.AirC9IceShards.Length; i < l; i++)
        {
            AirC9IceShardSheet.GetDictionary().Add(set.AirC9IceShards[i].level, set.AirC9IceShards[i]);
        }
       
        for (int i = 0, l = set.AirCConvertPowers.Length; i < l; i++)
        {
            AirCConvertPowerSheet.GetDictionary().Add(set.AirCConvertPowers[i].levelOld, set.AirCConvertPowers[i]);
        }
       
        for (int i = 0, l = set.Aircrafts.Length; i < l; i++)
        {
            AircraftSheet.GetDictionary().Add(set.Aircrafts[i].index, set.Aircrafts[i]);
        }
       
        for (int i = 0, l = set.AirCUpgradeCosts.Length; i < l; i++)
        {
            AirCUpgradeCostSheet.GetDictionary().Add(set.AirCUpgradeCosts[i].level, set.AirCUpgradeCosts[i]);
        }
       
        for (int i = 0, l = set.DailyLogin30s.Length; i < l; i++)
        {
            DailyLogin30Sheet.GetDictionary().Add(set.DailyLogin30s[i].index, set.DailyLogin30s[i]);
        }
       
        for (int i = 0, l = set.EndGameVideoRewards.Length; i < l; i++)
        {
            EndGameVideoRewardSheet.GetDictionary().Add(set.EndGameVideoRewards[i].NAME_ID, set.EndGameVideoRewards[i]);
        }
       
        for (int i = 0, l = set.ExtraLevelRequirements.Length; i < l; i++)
        {
            ExtraLevelRequirementSheet.GetDictionary().Add(set.ExtraLevelRequirements[i].level, set.ExtraLevelRequirements[i]);
        }
       
        for (int i = 0, l = set.HalloweenExchanges.Length; i < l; i++)
        {
            HalloweenExchangeSheet.GetDictionary().Add(set.HalloweenExchanges[i].index, set.HalloweenExchanges[i]);
        }
       
        for (int i = 0, l = set.LevelClearRewards.Length; i < l; i++)
        {
            LevelClearRewardSheet.GetDictionary().Add(set.LevelClearRewards[i].level, set.LevelClearRewards[i]);
        }
       
        for (int i = 0, l = set.LuckyBoxGems.Length; i < l; i++)
        {
            LuckyBoxGemSheet.GetDictionary().Add(set.LuckyBoxGems[i].index, set.LuckyBoxGems[i]);
        }
       
        for (int i = 0, l = set.LuckyBoxGolds.Length; i < l; i++)
        {
            LuckyBoxGoldSheet.GetDictionary().Add(set.LuckyBoxGolds[i].index, set.LuckyBoxGolds[i]);
        }
       
        for (int i = 0, l = set.NextWaveCoinEnlesss.Length; i < l; i++)
        {
            NextWaveCoinEnlessSheet.GetDictionary().Add(set.NextWaveCoinEnlesss[i].numWave, set.NextWaveCoinEnlesss[i]);
        }
       
        for (int i = 0, l = set.NextWaveResourcesEvents.Length; i < l; i++)
        {
            NextWaveResourcesEventSheet.GetDictionary().Add(set.NextWaveResourcesEvents[i].numWave, set.NextWaveResourcesEvents[i]);
        }
       
        for (int i = 0, l = set.RateCardBoxs.Length; i < l; i++)
        {
            RateCardBoxSheet.GetDictionary().Add(set.RateCardBoxs[i].KeyGet, set.RateCardBoxs[i]);
        }
       
        for (int i = 0, l = set.StarChests.Length; i < l; i++)
        {
            StarChestSheet.GetDictionary().Add(set.StarChests[i].level, set.StarChests[i]);
        }
       
        for (int i = 0, l = set.XMasAccumulateds.Length; i < l; i++)
        {
            XMasAccumulatedSheet.GetDictionary().Add(set.XMasAccumulateds[i].index, set.XMasAccumulateds[i]);
        }
       
        for (int i = 0, l = set.SummerHoliday_Box1s.Length; i < l; i++)
        {
            SummerHoliday_Box1Sheet.GetDictionary().Add(set.SummerHoliday_Box1s[i].index, set.SummerHoliday_Box1s[i]);
        }
       
        for (int i = 0, l = set.SummerHoliday_Box2s.Length; i < l; i++)
        {
            SummerHoliday_Box2Sheet.GetDictionary().Add(set.SummerHoliday_Box2s[i].index, set.SummerHoliday_Box2s[i]);
        }
       
        for (int i = 0, l = set.SummerHoliday_Box3s.Length; i < l; i++)
        {
            SummerHoliday_Box3Sheet.GetDictionary().Add(set.SummerHoliday_Box3s[i].index, set.SummerHoliday_Box3s[i]);
        }
       
        for (int i = 0, l = set.Wings.Length; i < l; i++)
        {
            WingSheet.GetDictionary().Add(set.Wings[i].index, set.Wings[i]);
        }
       
        for (int i = 0, l = set.WingOfJustice1s.Length; i < l; i++)
        {
            WingOfJustice1Sheet.GetDictionary().Add(set.WingOfJustice1s[i].level, set.WingOfJustice1s[i]);
        }
       
        for (int i = 0, l = set.WingOfRedemption2s.Length; i < l; i++)
        {
            WingOfRedemption2Sheet.GetDictionary().Add(set.WingOfRedemption2s[i].level, set.WingOfRedemption2s[i]);
        }
       
        for (int i = 0, l = set.WingOfResolution3s.Length; i < l; i++)
        {
            WingOfResolution3Sheet.GetDictionary().Add(set.WingOfResolution3s[i].level, set.WingOfResolution3s[i]);
        }
       
        for (int i = 0, l = set.WingUpgradeCosts.Length; i < l; i++)
        {
            WingUpgradeCostSheet.GetDictionary().Add(set.WingUpgradeCosts[i].level, set.WingUpgradeCosts[i]);
        }
       
        for (int i = 0, l = set.WingM1Galtings.Length; i < l; i++)
        {
            WingM1GaltingSheet.GetDictionary().Add(set.WingM1Galtings[i].level, set.WingM1Galtings[i]);
        }
       
        for (int i = 0, l = set.WingM2AutoGaltings.Length; i < l; i++)
        {
            WingM2AutoGaltingSheet.GetDictionary().Add(set.WingM2AutoGaltings[i].level, set.WingM2AutoGaltings[i]);
        }
       
        for (int i = 0, l = set.WingM3Lazers.Length; i < l; i++)
        {
            WingM3LazerSheet.GetDictionary().Add(set.WingM3Lazers[i].level, set.WingM3Lazers[i]);
        }
       
        for (int i = 0, l = set.WingM4DoubleGaltings.Length; i < l; i++)
        {
            WingM4DoubleGaltingSheet.GetDictionary().Add(set.WingM4DoubleGaltings[i].level, set.WingM4DoubleGaltings[i]);
        }
       
        for (int i = 0, l = set.WingM5HomingMissiles.Length; i < l; i++)
        {
            WingM5HomingMissileSheet.GetDictionary().Add(set.WingM5HomingMissiles[i].level, set.WingM5HomingMissiles[i]);
        }
       
        for (int i = 0, l = set.WingM6Splashers.Length; i < l; i++)
        {
            WingM6SplasherSheet.GetDictionary().Add(set.WingM6Splashers[i].level, set.WingM6Splashers[i]);
        }
       
        for (int i = 0, l = set.Wingmans.Length; i < l; i++)
        {
            WingmanSheet.GetDictionary().Add(set.Wingmans[i].index, set.Wingmans[i]);
        }
       
        for (int i = 0, l = set.WingMConvertPowers.Length; i < l; i++)
        {
            WingMConvertPowerSheet.GetDictionary().Add(set.WingMConvertPowers[i].levelOld, set.WingMConvertPowers[i]);
        }
       
        for (int i = 0, l = set.WingMUpgradeCosts.Length; i < l; i++)
        {
            WingMUpgradeCostSheet.GetDictionary().Add(set.WingMUpgradeCosts[i].level, set.WingMUpgradeCosts[i]);
        }


    }
}
