﻿[System.Serializable]
public class SerializableSet : UnityEngine.ScriptableObject
{
    public RankSheet[] Ranks;
    public SkillsSheet[] Skillss;
    public AirC10ThunderBoltSheet[] AirC10ThunderBolts;
    public AirC1BataFDSheet[] AirC1BataFDs;
    public AirC2SkyWraithSheet[] AirC2SkyWraiths;
    public AirC3FuryOfAresSheet[] AirC3FuryOfAress;
    public AirC6MacBirdSheet[] AirC6MacBirds;
    public AirC7TwilightXSheet[] AirC7TwilightXs;
    public AirC8StarbombSheet[] AirC8Starbombs;
    public AirC9IceShardSheet[] AirC9IceShards;
    public AirCConvertPowerSheet[] AirCConvertPowers;
    public AircraftSheet[] Aircrafts;
    public AirCUpgradeCostSheet[] AirCUpgradeCosts;
    public DailyLogin30Sheet[] DailyLogin30s;
    public EndGameVideoRewardSheet[] EndGameVideoRewards;
    public ExtraLevelRequirementSheet[] ExtraLevelRequirements;
    public HalloweenExchangeSheet[] HalloweenExchanges;
    public LevelClearRewardSheet[] LevelClearRewards;
    public LuckyBoxGemSheet[] LuckyBoxGems;
    public LuckyBoxGoldSheet[] LuckyBoxGolds;
    public NextWaveCoinEnlessSheet[] NextWaveCoinEnlesss;
    public NextWaveResourcesEventSheet[] NextWaveResourcesEvents;
    public RateCardBoxSheet[] RateCardBoxs;
    public StarChestSheet[] StarChests;
    public XMasAccumulatedSheet[] XMasAccumulateds;
    public SummerHoliday_Box1Sheet[] SummerHoliday_Box1s;
    public SummerHoliday_Box2Sheet[] SummerHoliday_Box2s;
    public SummerHoliday_Box3Sheet[] SummerHoliday_Box3s;
    public WingSheet[] Wings;
    public WingOfJustice1Sheet[] WingOfJustice1s;
    public WingOfRedemption2Sheet[] WingOfRedemption2s;
    public WingOfResolution3Sheet[] WingOfResolution3s;
    public WingUpgradeCostSheet[] WingUpgradeCosts;
    public WingM1GaltingSheet[] WingM1Galtings;
    public WingM2AutoGaltingSheet[] WingM2AutoGaltings;
    public WingM3LazerSheet[] WingM3Lazers;
    public WingM4DoubleGaltingSheet[] WingM4DoubleGaltings;
    public WingM5HomingMissileSheet[] WingM5HomingMissiles;
    public WingM6SplasherSheet[] WingM6Splashers;
    public WingmanSheet[] Wingmans;
    public WingMConvertPowerSheet[] WingMConvertPowers;
    public WingMUpgradeCostSheet[] WingMUpgradeCosts;


}
