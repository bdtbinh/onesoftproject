﻿using System.Collections.Generic;

[System.Serializable]
public class WingMUpgradeCostSheet
{
    /// <summary>
    /// level
    /// </summary>
    public int level;

    /// <summary>
    /// Đk nâng cấp
    /// </summary>
    public int gem;

    /// <summary>
    /// Đk nâng cấp
    /// </summary>
    public int gold;


    private static Dictionary<int, WingMUpgradeCostSheet> dictionary = new Dictionary<int, WingMUpgradeCostSheet>();

    /// <summary>
    /// 通过level获取WingMUpgradeCostSheet的实例
    /// </summary>
    /// <param name="level">索引</param>
    /// <returns>WingMUpgradeCostSheet的实例</returns>
    public static WingMUpgradeCostSheet Get(int level)
    {
        return dictionary[level];
    }
    
    /// <summary>
    /// 获取字典
    /// </summary>
    /// <returns>字典</returns>
    public static Dictionary<int, WingMUpgradeCostSheet> GetDictionary()
    {
        return dictionary;
    }
}
