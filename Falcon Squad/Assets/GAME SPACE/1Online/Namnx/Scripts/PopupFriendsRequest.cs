﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;

public class PopupFriendsRequest : MonoBehaviour
{
    public GameObject gFriends;
    public GameObject gClan;
    public GameObject gInvite;
    public UISprite sBackgroundInviteMember;
    public ItemFindMemberClan itemInviteMember;
    public GameObject buttonFindInviteMember;
    public GameObject buttonFindInviteMemberDeactive;
    public UIInput inputFindMember;

    public GameObject buttonAddFriends;
    public GameObject buttonAddFriendsDeactive;
    public UILabel labelAddFriendsDeactive;

    public UISprite sBtnFriends;
    public UISprite sBtnClan;

    public Transform listWrap;
    public GameObject fxloading;
    public GameObject lNoFriends;
    public GameObject lNoClan;

    public GameObject backgroundValueRequest;
    public UILabel labelBackgroundValueRequest;

    private const string TAB_ACTIVE = "2vs2_list_tab_1";
    private const string TAB_DEACTIVE = "2vs2_list_tab_0";

    private void OnEnable()
    {
        readyClickBack = true;
        gInvite.SetActive(false);

        if (CachePvp.FriendshipStats.numRequested <= 0)
        {
            backgroundValueRequest.SetActive(false);
        }
        else
        {
            backgroundValueRequest.SetActive(true);
            labelBackgroundValueRequest.text = CachePvp.FriendshipStats.numRequested.ToString();
        }

        for (int i = 0; i < listWrap.childCount; i++)
        {
            listWrap.GetChild(i).gameObject.SetActive(false);
        }
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadedListFriends.ToString(), OnLoadedListFriends, true);
        MessageDispatcher.AddListener(EventName.Clan.PlayerInfo.ToString(), OnPlayerInfo, true);
        MessageDispatcher.AddListener(EventName.Clan.ClanMember.ToString(), OnClanMember, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadFriendshipStatus.ToString(), OnLoadFriendshipStatus, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.RequestFriend.ToString(), OnRequestFriend, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadFriendRequest.ToString(), OnLoadFriendRequest, true);

        sBtnFriends.spriteName = TAB_DEACTIVE;
        sBtnClan.spriteName = TAB_DEACTIVE;
        bFriends();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadFriendRequest.ToString(), OnLoadFriendRequest, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadedListFriends.ToString(), OnLoadedListFriends, true);
        MessageDispatcher.RemoveListener(EventName.Clan.PlayerInfo.ToString(), OnPlayerInfo, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ClanMember.ToString(), OnClanMember, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadFriendshipStatus.ToString(), OnLoadFriendshipStatus, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RequestFriend.ToString(), OnRequestFriend, true);
    }

    private void OnLoadFriendRequest(IMessage rMessage)
    {
        SCMyRequests mr = rMessage.Data as SCMyRequests;
        if (mr.status == SCMyRequests.SUCCESS)
        {
            fxloading.SetActive(false);
            if (mr.players == null || mr.players.Count == 0)
            {
                lNoClan.SetActive(true);
            }
            else
            {
                MessageDispatcher.SendMessage(gameObject, EventName.LoadProfile.LoadedFriendRequest.ToString(), mr.players, 0);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(mr.message, false, 1.5f);
        }
        fxloading.SetActive(false);
    }

    private void OnRequestFriend(IMessage rMessage)
    {
        SCRequestFriend rf = rMessage.Data as SCRequestFriend;

        if (rf.status == SCRequestFriend.SUCCESS)
        {
            if (rf.type == CSRequestFriend.REQUEST)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.request_sent, false, 1.5f);
                buttonAddFriends.SetActive(false);
                buttonAddFriendsDeactive.SetActive(true);
                labelAddFriendsDeactive.text = I2.Loc.ScriptLocalization.request_sent;
            }
            else if (rf.type == CSRequestFriend.ACCEPT)
            {
                buttonAddFriends.SetActive(false);
                buttonAddFriendsDeactive.SetActive(true);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.success, true, 1.5f);
                labelAddFriendsDeactive.text = I2.Loc.ScriptLocalization.friend;
            }
            StartCoroutine(RefreshNumberRequest());
        }
        else if (rf.status == SCRequestFriend.BE_FRIEND)
        {
            buttonAddFriends.SetActive(false);
            buttonAddFriendsDeactive.SetActive(true);
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.success, true, 1.5f);
            labelAddFriendsDeactive.text = I2.Loc.ScriptLocalization.friend;
            StartCoroutine(RefreshNumberRequest());
        }
        else
        {
            buttonAddFriends.SetActive(true);
            buttonAddFriendsDeactive.SetActive(false);
            PopupManagerCuong.Instance.ShowTextNotifiToast(rf.message, false, 1.5f);
        }
    }

    IEnumerator RefreshNumberRequest()
    {
        yield return null;
        if (CachePvp.FriendshipStats.numRequested <= 0)
        {
            backgroundValueRequest.SetActive(false);
        }
        else
        {
            backgroundValueRequest.SetActive(true);
            labelBackgroundValueRequest.text = CachePvp.FriendshipStats.numRequested.ToString();
        }
    }

    Player playerFound;

    private void OnPlayerInfo(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (gInvite.activeInHierarchy)
        {
            if (status == SCPlayerInfo.SUCCESS)
            {
                if (rMessage.Data != null)
                {
                    isFriend = false;
                    playerFound = rMessage.Data as Player;
                    itemInviteMember.SetData(playerFound);
                    sBackgroundInviteMember.height = 514;
                    itemInviteMember.gameObject.SetActive(true);
                    buttonFindInviteMember.SetActive(false);
                    buttonFindInviteMemberDeactive.SetActive(false);
                    buttonAddFriends.SetActive(false);
                    buttonAddFriendsDeactive.SetActive(false);
                    new CSFriendshipStatus(playerFound.code).Send();
                }
                else
                {
                    sBackgroundInviteMember.height = 377;
                    itemInviteMember.gameObject.SetActive(false);
                    buttonFindInviteMember.SetActive(true);
                    buttonFindInviteMemberDeactive.SetActive(false);
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_player_not_exist, false, 1.5f);
                }
            }
            else
            {
                sBackgroundInviteMember.height = 377;
                itemInviteMember.gameObject.SetActive(false);
                buttonFindInviteMember.SetActive(true);
                buttonFindInviteMemberDeactive.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.error, false, 1.5f);
            }
        }
    }

    bool isFriend;

    private void OnLoadFriendshipStatus(IMessage rMessage)
    {
        SCFriendshipStatus fs = rMessage.Data as SCFriendshipStatus;
        if (fs.status == SCFriendshipStatus.SUCCESS && !PopupManager.Instance.IsProfilePopupOnTopActive())
        {
            if (playerFound.code.Equals(fs.friendshipStatus.code) && !playerFound.code.Equals(CachePvp.Code))
            {
                if (fs.friendshipStatus.isFriend)
                {
                    buttonAddFriends.SetActive(false);
                    buttonAddFriendsDeactive.SetActive(true);
                    isFriend = true;
                    labelAddFriendsDeactive.text = I2.Loc.ScriptLocalization.friend;
                }
                else if (fs.friendshipStatus.isRequesting)
                {
                    buttonAddFriends.SetActive(false);
                    buttonAddFriendsDeactive.SetActive(true);
                    isFriend = false;
                    labelAddFriendsDeactive.text = I2.Loc.ScriptLocalization.request_sent;
                }
                else
                {
                    buttonAddFriends.SetActive(true);
                    buttonAddFriendsDeactive.SetActive(false);
                }
            }
        }
    }

    private void OnClanMember(IMessage rMessage)
    {
        List<Player> list = rMessage.Data as List<Player>;
        fxloading.SetActive(false);
        if (list.Count == 0)
        {
            lNoFriends.SetActive(true);
        }
        else
        {
            lNoFriends.SetActive(false);
        }
        //MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedClanInfo.ToString(), rMessage.Data as List<Player>, 0);
    }

    private void OnLoadedListFriends(IMessage rMessage)
    {
        SCMyFriends mf = rMessage.Data as SCMyFriends;
        if (mf.status == SCMyFriends.SUCCESS)
        {
            fxloading.SetActive(false);
            if (mf.total == 0)
            {
                lNoFriends.SetActive(true);
            }
            else
            {
                lNoFriends.SetActive(false);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(mf.message, false, 1.5f);
        }
    }

    public void bAddFriend()
    {
        if (string.IsNullOrEmpty(CachePvp.Name))
        {
            PopupManager.Instance.ShowRegisterPopup();
            return;
        }
        buttonAddFriends.SetActive(false);
        buttonAddFriendsDeactive.SetActive(true);
        new CSRequestFriend(CSRequestFriend.REQUEST, playerFound.code).Send();
    }

    public void bRequestSent()
    {
        if (isFriend)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.title_is_friend, true, 1.5f);
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.request_sent, true, 1.5f);
        }
    }

    public void bSend()
    {
        if (!gInvite.activeInHierarchy)
        {
            sBackgroundInviteMember.height = 377;
            gInvite.SetActive(true);
            inputFindMember.value = "";
            itemInviteMember.gameObject.SetActive(false);
            buttonFindInviteMember.SetActive(true);
            buttonFindInviteMemberDeactive.SetActive(false);
        }
    }

    public void bBack()
    {
        if (gInvite.activeInHierarchy)
        {
            gInvite.SetActive(false);
        }
        else
        {
            PopupManager.Instance.HideFriendsRequest();
        }
    }

    public void bFriends()
    {
        if (sBtnFriends.spriteName.Equals(TAB_DEACTIVE))
        {
            for (int i = 0; i < gFriends.transform.GetChild(0).childCount; i++)
            {
                gFriends.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            }
            lNoClan.SetActive(false);
            lNoFriends.SetActive(false);
            fxloading.SetActive(false);
            gFriends.SetActive(true);
            gClan.SetActive(false);
            sBtnFriends.spriteName = TAB_ACTIVE;
            sBtnClan.spriteName = TAB_DEACTIVE;
            if (!HasChild(gFriends.transform.GetChild(0)))
            {
                fxloading.SetActive(true);
                new CSMyFriends().Send();
            }
        }
    }

    public void OnChangeInputInviteMember()
    {
        sBackgroundInviteMember.height = 377;
        itemInviteMember.gameObject.SetActive(false);
        buttonFindInviteMember.SetActive(true);
        buttonFindInviteMemberDeactive.SetActive(false);
    }

    public void bFindMemberInvite()
    {
        //sBackgroundInviteMember.height = 514;
        itemInviteMember.gameObject.SetActive(false);
        buttonFindInviteMember.SetActive(false);
        buttonFindInviteMemberDeactive.SetActive(true);
        if (!string.IsNullOrEmpty(inputFindMember.value))
        {
            new CSPlayerInfo(inputFindMember.value).Send();
        }
        else
        {
            sBackgroundInviteMember.height = 377;
            itemInviteMember.gameObject.SetActive(false);
            buttonFindInviteMember.SetActive(true);
            buttonFindInviteMemberDeactive.SetActive(false);
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_code_invalid, false, 1.5f);
        }
    }

    public void bClan()
    {
        if (sBtnClan.spriteName.Equals(TAB_DEACTIVE))
        {
            for (int i = 0; i < gClan.transform.GetChild(0).childCount; i++)
            {
                gClan.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            }
            lNoFriends.SetActive(false);
            lNoClan.SetActive(false);
            fxloading.SetActive(true);
            gFriends.SetActive(false);
            gClan.SetActive(true);
            sBtnClan.spriteName = TAB_ACTIVE;
            sBtnFriends.spriteName = TAB_DEACTIVE;

            //if (CachePvp.TypeMemberClan == CachePvp.HAS_NO_CLAN)
            //{
            //    lNoClan.SetActive(true);
            //}
            //else
            //{
            //if (!HasChild(gClan.transform.GetChild(0)))
            //{
            //    fxloading.SetActive(true);
            new CSMyRequests().Send();
            //}
            //}
        }
    }

    bool HasChild(Transform t)
    {
        if (t.childCount == 0)
        {
            return false;
        }
        for (int i = 0; i < t.childCount; i++)
        {
            if (t.GetChild(i).gameObject.activeInHierarchy)
            {
                return true;
            }
        }
        return false;
    }

    private bool readyClickBack;

    IEnumerator ReadyClickBack()
    {
        readyClickBack = false;
        yield return new WaitForSecondsRealtime(0.38f);
        readyClickBack = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && readyClickBack)
        {
            StartCoroutine(ReadyClickBack());
            bBack();
        }
    }
}
