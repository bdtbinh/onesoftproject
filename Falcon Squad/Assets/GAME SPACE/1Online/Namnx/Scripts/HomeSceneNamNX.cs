﻿using com.ootii.Messages;
using Mp.Pvp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class HomeSceneNamNX : MonoBehaviour
{

    public GameObject buttonWarningButtonExtra;
    public GameObject buttonWarningButtonPVP;

    private static HomeSceneNamNX _instance = null;

    public static HomeSceneNamNX Instance
    {
        get
        {
            return _instance;
        }
    }

    public void ShowOfferWallIronSource()
    {
        IronSource.Agent.showOfferwall();
    }

    private void Awake()
    {
        Debug.Log("=============device model" + SystemInfo.deviceModel);
        _instance = this;
        buttonWarningButtonExtra.SetActive(false);
        buttonWarningButtonPVP.SetActive(false);
        MessageDispatcher.AddListener(EventName.SyncData.SyncFromClientToServer.ToString(), OnSyncFromClientToServer, true);
        MessageDispatcher.AddListener(EventName.SyncData.SyncFromServerToClient.ToString(), OnSyncFromServerToClient, true);
        MessageDispatcher.AddListener(EventName.PVP.SessionRewardedInfo.ToString(), OnSessionRewardedInfo, true);
        //MessageDispatcher.AddListener(EventName.PVP.SessionRewardedClaim.ToString(), OnSessionRewardedClaim, true);
        MessageDispatcher.AddListener(EventName.LoadMailBox.LoadedMailBox.ToString(), LoadedMailBox, true);
        if (CachePvp.timeSinceStartUp == 0)
        {
            CachePvp.timeSinceStartUp = (long)(Time.realtimeSinceStartup * 1000);
        }
        PopupManager.Instance.ShowChatWorldPopup();
        Debug.Log("iden : " + Application.identifier);
        if (CachePvp.isConnectServer)
        {
            if (!CachePvp.isHomeOK)
            {
                CachePvp.isHomeOK = true;

                new CSHomeOK(PvpUtil.PVP_VERSION).Send();
            }
            if (!CachePvp.isSendLoadingTime)
            {
                CachePvp.isSendLoadingTime = true;
                long time = CachePvp.timeSinceStartUp;
                string deviceId = CachePvp.deviceId;
                string deviceName = SystemInfo.deviceModel;
                string code = CachePvp.Code;
                string platform = "";
#if UNITY_IPHONE
                platform = "ios";
#elif UNITY_ANDROID
                platform = "android";
#endif
                new CSLoadingTime(time, deviceId, deviceName, platform, code, Application.identifier).Send();
            }
        }

        if (CachePvp.needSync)
        {
            CachePvp.needSync = false;
            if (string.IsNullOrEmpty(CachePvp.deviceId))
            {
#if UNITY_EDITOR
                if (SystemInfo.deviceModel.Equals("MS-7971 (MSI)"))
                {
                    CachePvp.deviceId = "editornamnx";
                    new CSValidateDeviceId(CachePvp.deviceId).Send();
                }
                else if (SystemInfo.deviceModel.Equals("MS-7A70 (MSI)"))
                {
                    CachePvp.deviceId = "editorminhddv2";
                    new CSValidateDeviceId(CachePvp.deviceId).Send();
                }
                else
                {
                    new CSGetToken(CachePvp.deviceId, CachePvp.FacebookId).Send();
                    MessageDispatcher.SendMessage(EventName.SyncData.NoSync.ToString());
                }
#else
                bool check = Application.RequestAdvertisingIdentifierAsync((string advertisingId, bool trackingEnabled, string error) =>
                {
                    CachePvp.deviceId = advertisingId;
                    new CSValidateDeviceId(advertisingId).Send();
                });
                if (!check)
                {
                    new CSGetToken(CachePvp.deviceId, CachePvp.FacebookId).Send();
                    MessageDispatcher.SendMessage(EventName.SyncData.NoSync.ToString());
                }
#endif
            }
            else
            {
                new CSValidateDeviceId(CachePvp.deviceId).Send();
            }
        }

        //MissionHandler.OnRewardClaimed += OnRewardClaimed;

        IronSourceEvents.onOfferwallClosedEvent += OfferwallClosedEvent;
        IronSourceEvents.onOfferwallOpenedEvent += OfferwallOpenedEvent;
        IronSourceEvents.onOfferwallShowFailedEvent += OfferwallShowFailedEvent;
        IronSourceEvents.onOfferwallAdCreditedEvent += OfferwallAdCreditedEvent;
        IronSourceEvents.onGetOfferwallCreditsFailedEvent += GetOfferwallCreditsFailedEvent;
        IronSourceEvents.onOfferwallAvailableEvent += OfferwallAvailableEvent;

        //List<ItemVolumeFalconMail> list = new List<ItemVolumeFalconMail>();
        //for (int i = 0; i < 3; i++)
        //{
        //    ItemVolumeFalconMail item = new ItemVolumeFalconMail();
        //    if (i == 0)
        //    {
        //        item.key = FalconMail.ITEM_GOLD;
        //        item.value = 10;
        //    }
        //    else if (i == 1)
        //    {
        //        item.key = FalconMail.ITEM_GEM;
        //        item.value = 20;
        //    }
        //    else if (i == 2)
        //    {
        //        item.key = FalconMail.ITEM_CARD_PLANE_GENERAL;
        //        item.value = 30;
        //    }
        //    list.Add(item);
        //}
        //PopupManager.Instance.ShowClaimPopup(list, EventName.PVP_REWARD, true);
    }

    private void OnSessionRewardedInfo(IMessage rMessage)
    {
        buttonWarningButtonExtra.SetActive(true);
        buttonWarningButtonPVP.SetActive(true);
    }

    private void LoadedMailBox(IMessage rMessage)
    {
        DataJsonMail djm = DataJsonMail.ToJsonObject(CachePvp.FalconMailJson);
        for (int i = 0; i < djm.listFalconMail.Count; i++)
        {
            if (djm.listFalconMail[i].url.Equals("debug"))
            {
                Debug.unityLogger.logEnabled = true;
                SRDebug.Init();
                Rect r = Screen.safeArea;
                Debug.Log(r);
                Debug.Log(Screen.width + " - " + Screen.height);
                break;
            }
        }
    }

    /**
* Invoked when there is a change in the Offerwall availability status.
* @param - available - value will change to YES when Offerwall are available. 
* You can then show the video by calling showOfferwall(). Value will change to NO when Offerwall isn't available.
*/
    void OfferwallAvailableEvent(bool canShowOfferwall)
    {
        Debug.Log("OfferwallAvailableEvent : " + canShowOfferwall);
    }
    /**
     * Invoked when the Offerwall successfully loads for the user.
     */
    void OfferwallOpenedEvent()
    {
        Debug.Log("OfferwallOpenedEvent");
    }
    /**
     * Invoked when the method 'showOfferWall' is called and the OfferWall fails to load.  
    *@param desc - A string which represents the reason of the failure.
     */
    void OfferwallShowFailedEvent(IronSourceError error)
    {
        Debug.Log("OfferwallShowFailedEvent : " + error);
    }
    /**
      * Invoked each time the user completes an offer.
      * Award the user with the credit amount corresponding to the value of the ‘credits’ 
      * parameter.
      * @param dict - A dictionary which holds the credits and the total credits.   
      */
    void OfferwallAdCreditedEvent(Dictionary<string, object> dict)
    {
        //Debug.Log("I got OfferwallAdCreditedEvent, current credits = "dict["credits"] + "totalCredits = " + dict["totalCredits"]);
        Debug.Log("OfferwallAdCreditedEvent");
        foreach (var item in dict)
        {
            Debug.Log(item.Key + " - " + item.Value);
        }
        int goldMore = (int)dict["credits"];
        if (goldMore != 0)
        {
            int gold = CacheGame.GetTotalCoin();
            CacheGame.SetTotalCoin(gold + goldMore, "OfferwallAdCreditedEvent", "OfferwallAdCreditedEvent");
            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.AddCoinTop();
            }
        }
    }
    /**
      * Invoked when the method 'getOfferWallCredits' fails to retrieve 
      * the user's credit balance info.
      * @param desc -string object that represents the reason of the  failure. 
      */
    void GetOfferwallCreditsFailedEvent(IronSourceError error)
    {
        Debug.Log("GetOfferwallCreditsFailedEvent : " + error);
    }
    /**
      * Invoked when the user is about to return to the application after closing 
      * the Offerwall.
      */
    void OfferwallClosedEvent()
    {
        IronSource.Agent.getOfferwallCredits();
        Debug.Log("OfferwallClosedEvent");
    }

    private void OnDestroy()
    {
        //MissionHandler.OnRewardClaimed -= OnRewardClaimed;
        MessageDispatcher.RemoveListener(EventName.SyncData.SyncFromClientToServer.ToString(), OnSyncFromClientToServer, true);
        MessageDispatcher.RemoveListener(EventName.SyncData.SyncFromServerToClient.ToString(), OnSyncFromServerToClient, true);
        MessageDispatcher.RemoveListener(EventName.LoadMailBox.LoadedMailBox.ToString(), LoadedMailBox, true);
        MessageDispatcher.RemoveListener(EventName.PVP.SessionRewardedInfo.ToString(), OnSessionRewardedInfo, true);
        //MessageDispatcher.RemoveListener(EventName.PVP.SessionRewardedClaim.ToString(), OnSessionRewardedClaim, true);

        IronSourceEvents.onOfferwallClosedEvent -= OfferwallClosedEvent;
        IronSourceEvents.onOfferwallOpenedEvent -= OfferwallOpenedEvent;
        IronSourceEvents.onOfferwallShowFailedEvent -= OfferwallShowFailedEvent;
        IronSourceEvents.onOfferwallAdCreditedEvent -= OfferwallAdCreditedEvent;
        IronSourceEvents.onGetOfferwallCreditsFailedEvent -= GetOfferwallCreditsFailedEvent;
        IronSourceEvents.onOfferwallAvailableEvent -= OfferwallAvailableEvent;

        PopupManager.Instance.HideChatWorldPopup();
    }

    private void OnRewardClaimed(string obj)
    {
        //5000 Gold
        //10000 Gold
        Debug.Log("OnRewardClaimed : " + obj);
        string[] arr = obj.Split(' ');
        if (arr.Length == 2)
        {
            if (arr[1].Equals("Gold"))
            {
                int goldMore = int.Parse(arr[0]);
                int gold = CacheGame.GetTotalCoin();
                CacheGame.SetTotalCoin(gold + goldMore, "OnRewardClaimed", "HomeSceneNamNX");
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddCoinTop();
                }
            }
        }
    }

    public void ShowMailBox()
    {
        PopupManager.Instance.ShowMailPopup();
    }

    void OnSyncFromServerToClient(IMessage msg)
    {
        DataSync data = msg.Data as DataSync;
        CachePvp.SetPlayerData(data.player, data.token);
        if (CachePvp.facebookUser != null)
        {
            new CSFacebookConnect(SocialManager.Instance.FBGetUserToken(), CachePvp.facebookUser.id, CachePvp.facebookUser.name, 2).Send();
        }
    }

    void OnSyncFromClientToServer(IMessage msg)
    {
        if (GameContext.IS_CHINA_VERSION)
        {
            if (Social.localUser.authenticated)
            {
                new CSAppCenterConnect("", Social.localUser.id, Social.localUser.userName, 1).Send();
            }
        }
        else
        {
            if (CachePvp.facebookUser != null)
            {
                new CSFacebookConnect(SocialManager.Instance.FBGetUserToken(), CachePvp.facebookUser.id, CachePvp.facebookUser.name, 1).Send();
            }
        }
    }

    public void Promotion()
    {
        PopupManager.Instance.ShowPromotionPopup();
    }

    public void ShowClan()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        if (CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL) <= 10)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", "10"), false, 1.5f);
            return;
        }
        if (!CachePvp.sCVersionConfig.canClan)
        {
            PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainClan);
            return;
        }
        PopupManager.Instance.ShowClanPopup();
        FirebaseLogSpaceWar.LogClickButton("Clan");
    }

    public void SetLevel()
    {
        CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, 16);
        for (int i = 1; i <= 16; i++)
        {
            CacheGame.SetNumStarLevel(i, 1, 1, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal);
        }
    }

    public void TestGetRewardVideo()
    {
        long c = 0;
        int f = 0;

        for (int i = 0; i < 100000; i++)
        {
            ItemVolumeFalconMail item = CachePvp.GetRewardFromVideo();
            if (item.key == FalconMail.ITEM_GOLD)
            {
                c += item.value;
                f++;
            }
        }
        Debug.Log("Gold : " + (c * 1.0f / f));

        c = 0;
        f = 0;
        for (int i = 0; i < 100000; i++)
        {
            ItemVolumeFalconMail item = CachePvp.GetRewardFromVideo();
            if (item.key == FalconMail.ITEM_GEM)
            {
                c += item.value;
                f++;
            }
        }
        Debug.Log("Gem : " + (c * 1.0f / f));

        c = 0;
        f = 0;
        for (int i = 0; i < 100000; i++)
        {
            ItemVolumeFalconMail item = CachePvp.GetRewardFromVideo();
            if (item.key == FalconMail.ITEM_CARD_PLANE_1)
            {
                c += item.value;
                f++;
            }
        }
        Debug.Log("Card : " + (c * 1.0f / f));

        c = 0;
        f = 0;
        for (int i = 0; i < 100000; i++)
        {
            ItemVolumeFalconMail item = CachePvp.GetRewardFromVideo();
            if (item.key == FalconMail.ITEM_ACTIVESKILL)
            {
                c += item.value;
                f++;
            }
        }
        Debug.Log("Item : " + (c * 1.0f / f));
    }

    public void ShowTextRunning()
    {
        //string text = "Đây là một đoạn text test rất là dài loằng ngoằng Đây là một đoạn text test rất là dài loằng ngoằng Đây là một đoạn text test rất là dài loằng ngoằng";
        string text = "Bạn Min đã nhận được 1 trái tim từ bạn BB, xin chúc hai bạn hạnh phúc!";
        PopupManager.Instance.ShowTextRunningPopup(text, 360, 0, "http://bongdaplus.vn");
    }

    public void ShowLiveScore()
    {
        PopupManager.Instance.ShowLiveScorePopup();
    }

    public void ShowGloryChest()
    {
        if (CachePvp.TypeMemberClan == CachePvp.HAS_NO_CLAN)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_join_team_to_open, false, 1.5f);
            return;
        }
        PopupManager.Instance.ShowGloryChestPopup();
    }

    public void InviteTest()
    {
        //new CSPvPInvite("147422", CacheGame.GetSpaceShipUserSelected(), PvPDataHelper.Instance.GetMyWaveCacheIndex(), PvPDataHelper.Instance.GetMyDataString()).Send();
        PopupManager.Instance.ShowSelectCardTypePopup();
    }

    public void AddFriend1()
    {
        new CSRequestFriend(CSRequestFriend.REQUEST, "14543861").Send();
    }

    public void AddFriend2()
    {
        new CSRequestFriend(CSRequestFriend.REQUEST, "147422").Send();
    }

    public void AddFriend3()
    {
        new CSRequestFriend(CSRequestFriend.REQUEST, "17312525").Send();
    }

    public void ShowTournament()
    {
        PopupManager.Instance.ShowTournamentPopup();
    }
}

[System.Serializable]
public class ItemVolumeFalconMail
{
    public int key;
    public int value;
}

[System.Serializable]
public class DataJsonMail
{
    public List<FalconMail> listFalconMail;

    public string ToJson()
    {
        CachePvp.NeedReloadMailBox = true;
        return JsonUtility.ToJson(this);
    }

    public static DataJsonMail ToJsonObject(string jsonString)
    {
        DataJsonMail djm = JsonUtility.FromJson<DataJsonMail>(jsonString);
        return djm;
    }
}