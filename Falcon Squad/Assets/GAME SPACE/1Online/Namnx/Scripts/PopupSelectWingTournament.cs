﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupSelectWingTournament : MonoBehaviour
{
    [SerializeField]
    private ElementSelectWingTournament[] listElements;

    [SerializeField]
    private UISprite btnConfirm;

    private ElementSelectWingTournament selectedWingTournament;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventID.ON_SELECTED_CALLING_BACKUP, OnSelectedCallingBackup, true);

        InitListElements();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_SELECTED_CALLING_BACKUP, OnSelectedCallingBackup, true);
    }

    private void OnSelectedCallingBackup(IMessage msg)
    {
        btnConfirm.spriteName = "PVP_btn_fight";

        if (selectedWingTournament != null)
        {
            selectedWingTournament.SetAsDeselected();
        }

        selectedWingTournament = (ElementSelectWingTournament)msg.Sender;
    }

    private void InitListElements()
    {
        int totalWing = System.Enum.GetValues(typeof(WingTypeEnum)).Length - 1;
        btnConfirm.spriteName = "PVP_btn_fight_d";
        selectedWingTournament = null;
        for (int i = 0; i < listElements.Length; i++)
        {
            listElements[i].CreateItem((WingTypeEnum)(i + 1));
        }
        int selected = CacheGame.GetWingTournament();
        if (selected != 0)
        {
            listElements[selected - 1].SetAsSelected();
        }
        else
        {
            listElements[0].SetAsSelected();
        }
    }

    public void OnClickBtnConfirm()
    {
        if (selectedWingTournament != null)
        {
            CacheGame.SetWingTournament((int)selectedWingTournament.WingType);
            PopupManager.Instance.HideSelectWingPopup();
            MessageDispatcher.SendMessage(EventName.Tournament.RefreshSelected.ToString());
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_use_another_aircraft, false, 1.5f);
        }
    }

    public void OnSkip()
    {
        PopupManager.Instance.HideSelectWingPopup();
    }
}
