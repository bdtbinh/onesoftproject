﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class GlobalObject : MonoBehaviour
{

    public UILabel lGlobal;
    public UILabel lName;
    public UILabel lNameClan;
    public UILabel lElo;
    public UILabel lWinRate;
    public UILabel lMedalRank;
    public UISprite sMedalRank;
    public UILabel lBonusMedal;
    public UISprite sBonusMedal;
    public UISprite[] sMedals;
    public UI2DSprite sAvatar;

    public UISprite sVipIcon;

    string facebookIdToShow = "";

    private const string MEDAL_ENABLE_SPRITE = "PVP_rank_medal_1";
    private const string MEDAL_DISABLE_SPRITE = "PVP_rank_medal_0";

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.ChangeProperties.UpdatePlayerInfo.ToString(), OnUpdatePlayerInfo, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.ChangeProperties.UpdatePlayerInfo.ToString(), OnUpdatePlayerInfo, true);
    }

    void OnUpdatePlayerInfo(IMessage msg)
    {
        SetPlayerElo();
        SetPlayerWinRate();
        SetUserMedalTitle();
        SetUserMedalIcon();
        SetUserMedalNumber();
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdToShow))
        {
            sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdToShow];
        }
    }

    // Use this for initialization
    void Start()
    {
        SetAvatar();
        SetPlayerName();
        SetPlayerElo();
        SetPlayerWinRate();
        SetUserMedalTitle();
        SetUserMedalIcon();
        SetUserMedalNumber();
    }

    void SetAvatar()
    {
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(CachePvp.GamecenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.GamecenterId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.GamecenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.GamecenterId);
                    if (t != null)
                    {
                        facebookIdToShow = CachePvp.GamecenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.GamecenterId, t);
                    }
                    else
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                    }
                }
            }
            else
            {
                sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(CachePvp.FacebookId))
            {
                sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            }
            else
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
                }
                else
                {
                    facebookIdToShow = CachePvp.FacebookId;
                    string query = "https://graph.facebook.com/" + CachePvp.FacebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.FacebookId);
                }
            }
        }
        if (CachePvp.myVip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (CachePvp.myVip - 1);
        }
    }

    Vector3 tempVector3;
    void SetPlayerName()
    {
        lName.text = string.IsNullOrEmpty(CachePvp.Name) ? CachePvp.Code : CachePvp.Name;
        tempVector3.x = lNameClan.transform.localPosition.x;
        tempVector3.z = lNameClan.transform.localPosition.z;
        if (!string.IsNullOrEmpty(CachePvp.PlayerClan.name))
        {
            lNameClan.text = CachePvp.PlayerClan.name;
            tempVector3.y = 24;
        }
        else
        {
            lNameClan.text = "";
            tempVector3.y = 44;
        }
        lNameClan.transform.localPosition = tempVector3;
    }

    void SetPlayerElo()
    {
        lElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.PlayerLevel.elo);
    }

    void SetPlayerWinRate()
    {

        lWinRate.text = I2.Loc.ScriptLocalization.win_rate.Replace("%{percent_number}", CachePvp.GetWinrateText());

    }

    void SetUserMedalTitle()
    {
        //MedalRankTitle rankTitle = (MedalRankTitle)(CachePvp.Medal / 30);
        //int medalSurplus = CachePvp.Medal % 30;
        //MedalRankSymbol rankSymbol = (MedalRankSymbol)(medalSurplus / 6);

        //lMedalRank.text = rankTitle + " " + rankSymbol;
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lMedalRank.text = rankTitle + " " + CachePvp.ConvertChina(CachePvp.PlayerLevel.name);
    }

    void SetUserMedalIcon()
    {
        //sMedalRank.spriteName = "PVP_rank_" + (CachePvp.Medal / 30 + 1);
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code];
        sMedalRank.spriteName = "PVP_rank_" + rank;
    }

    void SetUserMedalNumber()
    {
        lBonusMedal.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.PlayerLevel.elo);
        //sBonusMedal.fillAmount = (CachePvp.Win % 10) / 10f;
        string currentRank = CachePvp.PlayerLevel.code;
        int tempStartRank = CachePvp.dictionaryRank[currentRank];
        int number = 3;
        int currentStar = CachePvp.PlayerLevel.medal;
        if (currentRank == "BRONZE")
        {
            number = 3;
        }
        else if (currentRank == "SILVER")
        {
            number = 4;
        }
        else
        {
            number = 5;
        }

        for (int i = 0; i < sMedals.Length; i++)
        {
            if (i < number)
            {
                sMedals[i].gameObject.SetActive(true);
                if (i < currentStar)
                {
                    sMedals[i].spriteName = MEDAL_ENABLE_SPRITE;
                }
                else
                {
                    sMedals[i].spriteName = MEDAL_DISABLE_SPRITE;
                }
            }
            else
            {
                sMedals[i].gameObject.SetActive(false);
            }
        }

        sBonusMedal.fillAmount = (CachePvp.PlayerLevel.elo % 10) / 10f;
    }
}
