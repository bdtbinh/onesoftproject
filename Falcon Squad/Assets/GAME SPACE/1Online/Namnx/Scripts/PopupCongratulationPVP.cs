﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OneSoftGame.Tools;
using Mp.Pvp;

public class PopupCongratulationPVP : PersistentSingleton<PopupCongratulationPVP>
{
    public UILabel lCongratulation;
    public UISprite sRank;
    public UILabel lRank;
    public UILabel lMessage;

    public void ShowCongratulation()
    {
        if (CachePvp.MyResult != null)
        {
            //MedalRankTitle rankTitle = (MedalRankTitle)(CachePvp.Medal / 30);
            //int medalSurplus = CachePvp.Medal % 30;
            //MedalRankSymbol rankSymbol = (MedalRankSymbol)(medalSurplus / 6);

            //lRank.text = rankTitle + " " + rankSymbol;
            //sRank.spriteName = "PVP_rank_" + (CachePvp.Medal / 30 + 1);
            int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code];
            MedalRankTitle rankTitle = (MedalRankTitle)(rank);
            lRank.text = rankTitle + " " + CachePvp.ConvertChina(CachePvp.PlayerLevel.name);
            sRank.spriteName = "PVP_rank_" + rank;

            if (CachePvp.MyResult.result == PlayerResult.WIN)
            {
                lMessage.text = I2.Loc.ScriptLocalization.msg_pvp_rank_up;
                lCongratulation.text = I2.Loc.ScriptLocalization.title_pvp_rank_up; ;
            }
            else if (CachePvp.MyResult.result == PlayerResult.LOSE)
            {
                lMessage.text = I2.Loc.ScriptLocalization.msg_pvp_rank_down;
                lCongratulation.text = I2.Loc.ScriptLocalization.title_pvp_rank_down;
            }
        }
    }

    public void OnClickBack()
    {
        PopupManager.Instance.HideCongratulation();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnClickBack();
        }
    }
}
