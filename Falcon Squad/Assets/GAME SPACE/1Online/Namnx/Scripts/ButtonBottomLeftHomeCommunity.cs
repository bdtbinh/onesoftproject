﻿using com.ootii.Messages;
using UnityEngine;

public class ButtonBottomLeftHomeCommunity : MonoBehaviour
{

    private void Awake()
    {
        MessageDispatcher.AddListener(EventName.UIHome.SetTurnOnTeamSCClientConfig.ToString(), OnSetTurnOnTeamSCClientConfig, true);
        RefreshUI();
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.UIHome.SetTurnOnTeamSCClientConfig.ToString(), OnSetTurnOnTeamSCClientConfig, true);
    }

    private void OnSetTurnOnTeamSCClientConfig(IMessage rMessage)
    {
        RefreshUI();
    }

    void RefreshUI()
    {
        if (CachePvp.sCClientConfig.turnOnTeam == 0)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
