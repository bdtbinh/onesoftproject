﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class ElementSelectWingmanTournament : MonoBehaviour
{
    [SerializeField]
    private UISprite sIcon;

    [SerializeField]
    private UISprite sRank;

    [SerializeField]
    private UILabel lLevel;

    [SerializeField]
    private GameObject sSelected;

    public WingmanTypeEnum WingmanType { get; private set; }

    public UISprite sPosition;

    const string SPRITE_LEFT = "Item_laber_L";
    const string SPRITE_RIGHT = "Item_laber_R";

    public void CreateItem(WingmanTypeEnum wingmanType)
    {
        this.WingmanType = wingmanType;
        Rank rank = Constant.MAX_RANK_APPLY;
        sSelected.SetActive(false);
        sPosition.gameObject.SetActive(false);
        if ((int)wingmanType == CacheGame.GetWingManLeftTournament())
        {
            sPosition.gameObject.SetActive(true);
            sPosition.spriteName = SPRITE_LEFT;
        }
        else if ((int)wingmanType == CacheGame.GetWingManRightTournament())
        {
            sPosition.gameObject.SetActive(true);
            sPosition.spriteName = SPRITE_RIGHT;
        }
        sIcon.spriteName = HangarValue.WingmanIconSpriteName(wingmanType, rank);

        Vector2 localScale = sIcon.transform.localScale;
        sIcon.MakePixelPerfect();
        sIcon.transform.localScale = localScale;

        sRank.spriteName = HangarValue.RankSpriteName[rank];
        sRank.MakePixelPerfect();
        int newMaxlevel = RankSheet.Get((int)rank).max_level;
        lLevel.text = newMaxlevel.ToString();
    }

    public void SetAsSelected()
    {
        sSelected.SetActive(true);
        MessageDispatcher.SendMessage(this, EventID.ON_SELECTED_CALLING_BACKUP, WingmanType, 0);
    }

    public void SetAsDeselected()
    {
        sSelected.SetActive(false);
    }

    public void OnClickAircraft()
    {
        if (sSelected.activeInHierarchy)
            return;
        SetAsSelected();
    }
}
