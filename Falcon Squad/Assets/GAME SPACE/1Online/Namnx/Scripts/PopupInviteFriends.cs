﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;

public class PopupInviteFriends : MonoBehaviour
{
    public Transform listWrap;
    public GameObject fxloading;
    public GameObject lNoFriends;

    private void OnEnable()
    {
        lNoFriends.SetActive(false);
        fxloading.SetActive(true);
        CachePvp.Opponent = null;
        for (int i = 0; i < listWrap.childCount; i++)
        {
            listWrap.GetChild(i).gameObject.SetActive(false);
        }
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadedListFriends.ToString(), OnLoadedListFriends, true);
        new CSMyFriends().Send();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadedListFriends.ToString(), OnLoadedListFriends, true);
    }

    private void OnLoadedListFriends(IMessage rMessage)
    {
        SCMyFriends mf = rMessage.Data as SCMyFriends;
        if (mf.status == SCMyFriends.SUCCESS)
        {
            fxloading.SetActive(false);
            if (mf.total == 0)
            {
                lNoFriends.SetActive(true);
            }
            else
            {
                lNoFriends.SetActive(false);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(mf.message, false, 1.5f);
        }
    }

    public void bSend()
    {
        if (CachePvp.Opponent != null)
        {
            new CSPvPInvite(CachePvp.Opponent.code, CacheGame.GetSpaceShipUserSelected(), PvPDataHelper.Instance.GetMyWaveCacheIndex(), PvPDataHelper.Instance.GetMyDataString()).Send();
        }
    }

    public void bBack()
    {
        PopupManager.Instance.HideInviteFriendsPopup();
    }
}
