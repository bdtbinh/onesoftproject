﻿using com.ootii.Messages;
using Mp.Pvp;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupMailBox : MonoBehaviour
{
    public GameObject panelClanInfo;
    public UISprite avatarClanInfo;
    public UILabel nameClanInfo;
    public UILabel memberClanInfo;
    public UILabel scoreValueClanInfo;
    public UILabel typeValueClanInfo;
    public UILabel levelRequiredValueClanInfo;

    public UISprite buttonJoinClanInfo;
    public GameObject buttonJoinDeactiveClanInfo;
    private void OnEnable()
    {
        readyClickBack = true;
        panelClanInfo.SetActive(false);
        MessageDispatcher.AddListener(EventName.Clan.ClanInfo.ToString(), OnClanInfo, true);
        MessageDispatcher.AddListener(EventName.Clan.AcceptClanInvitation.ToString(), OnAcceptClanInvitation, true);
        DataJsonMail djm;
        if (!string.IsNullOrEmpty(CachePvp.FalconMailJson))
        {
            djm = DataJsonMail.ToJsonObject(CachePvp.FalconMailJson);
        }
        else
        {
            djm = new DataJsonMail();
            djm.listFalconMail = new List<FalconMail>();
            CachePvp.FalconMailJson = djm.ToJson();
        }
        for (int i = djm.listFalconMail.Count - 1; i >= 0; i--)
        {
            long time = djm.listFalconMail[i].eventTime;
            DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            double dou = (double)time;
            DateTime dt = unixEpoch.AddMilliseconds(dou);
            string dText = dt.ToString("dd/MM/yyyy");
            if (dText.Equals("06/03/2019"))
            {
                djm.listFalconMail.RemoveAt(i);
            }
            CachePvp.FalconMailJson = djm.ToJson();
        }
        StartCoroutine(WaitEndFrame(djm));
        CloseSendMail();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Clan.ClanInfo.ToString(), OnClanInfo, true);
        MessageDispatcher.RemoveListener(EventName.Clan.AcceptClanInvitation.ToString(), OnAcceptClanInvitation, true);
    }

    IEnumerator WaitEndFrame(DataJsonMail djm)
    {
        yield return new WaitForEndOfFrame();
        MessageDispatcher.SendMessage(gameObject, EventName.LoadMailBox.LoadedMailBox.ToString(), djm, 0);
    }

    public void ButtonJoinClan()
    {
        buttonJoinClanInfo.gameObject.SetActive(false);
        buttonJoinDeactiveClanInfo.SetActive(true);
        new CSClanInvitationAccept(m_ciView.id).Send();
    }

    private void OnAcceptClanInvitation(IMessage rMessage)
    {
        int status = (int)rMessage.Data;
        switch (status)
        {
            case SCClanInvitationAccept.SUCCESS:
                CachePvp.TypeMemberClan = CachePvp.CLAN_MEMBER;
                panelClanInfo.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.success, true, 1.5f);
                break;
            case SCClanInvitationAccept.FULL_MEMBER:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_have_full_member, false, 1.5f);
                break;
            case SCClanInvitationAccept.CLAN_NOT_FOUND:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_not_exist, false, 1.5f);
                break;
            case SCClanInvitationAccept.INVITATION_NOT_EXIST:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_invitation_not_exist, false, 1.5f);
                break;
            case SCClanInvitationAccept.JOINED_CLAN:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_had_in_clan, false, 1.5f);
                break;
            default:
                break;
        }
    }

    public void ButtonClose()
    {
        if (panelClanInfo.activeInHierarchy)
        {
            panelClanInfo.SetActive(false);
        }
        else if (PopupManager.Instance.IsClanPopupActive())
        {

        }
        else if (sendMailObject.activeInHierarchy)
        {
            sendMailObject.SetActive(false);
        }
        else
        {
            PopupManager.Instance.HideMailPopup();
        }
    }

    private bool readyClickBack;

    IEnumerator ReadyClickBack()
    {
        readyClickBack = false;
        yield return new WaitForSecondsRealtime(0.38f);
        readyClickBack = true;
    }

    public GameObject sendMailObject;
    public UIInput iMail;
    public GameObject buttonSendActive;
    public GameObject buttonSendDeactive;

    public void ButtonSendMail()
    {
        if (string.IsNullOrEmpty(iMail.value.Trim()))
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_content_cant_empty, false, 1.5f);
            return;
        }
        buttonSendActive.SetActive(false);
        buttonSendDeactive.SetActive(true);
        new CSMailToServer(iMail.value).Send();
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_thank_send_mail, true, 3f);
        CloseSendMail();
    }

    public void CloseSendMail()
    {
        sendMailObject.SetActive(false);
    }

    public void OpenSendMail()
    {
        iMail.value = "";
        buttonSendActive.SetActive(true);
        buttonSendDeactive.SetActive(false);
        sendMailObject.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && readyClickBack)
        {
            StartCoroutine(ReadyClickBack());
            ButtonClose();
        }
    }

    private void OnClanInfo(IMessage rMessage)
    {
        ClanInfo ci = rMessage.Data as ClanInfo;
        ShowClanInfo(ci);
    }

    ClanInfo m_ciView;

    void ShowClanInfo(ClanInfo ci_view)
    {
        m_ciView = ci_view;
        panelClanInfo.SetActive(true);
        if ((ci_view.avatar + 2) < 10)
        {
            avatarClanInfo.spriteName = "avt_Avt_0" + (ci_view.avatar + 2);
        }
        else
        {
            avatarClanInfo.spriteName = "avt_Avt_" + (ci_view.avatar + 2);
        }

        scoreValueClanInfo.text = GameContext.FormatNumber((int)ci_view.score);
        typeValueClanInfo.text = ci_view.type == 1 ? I2.Loc.ScriptLocalization.close : I2.Loc.ScriptLocalization.open;
        levelRequiredValueClanInfo.text = ci_view.requiredLevel.ToString();
        nameClanInfo.text = ci_view.name;
        memberClanInfo.text = ci_view.totalMember + "/" + ci_view.size;

        if (CachePvp.TypeMemberClan != CachePvp.HAS_NO_CLAN || ci_view.totalMember == ci_view.size || ci_view.requiredLevel > CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL))
        {
            buttonJoinClanInfo.height = 0;
            buttonJoinClanInfo.color = Color.clear;
            buttonJoinClanInfo.gameObject.SetActive(false);
            buttonJoinDeactiveClanInfo.SetActive(false);
        }
        else
        {
            buttonJoinClanInfo.MakePixelPerfect();
            buttonJoinClanInfo.color = Color.white;
            if (CachePvp.listClanIdRequest.Contains(ci_view.id))
            {
                buttonJoinClanInfo.gameObject.SetActive(false);
                buttonJoinDeactiveClanInfo.SetActive(true);
            }
            else
            {
                buttonJoinClanInfo.gameObject.SetActive(true);
                buttonJoinDeactiveClanInfo.SetActive(false);
            }
        }
        List<Player> list = ci_view.members;
        for (int i = 0; i < ci_view.viceMaster.Count; i++)
        {
            list.Insert(0, ci_view.viceMaster[i]);
        }
        list.Insert(0, ci_view.master);
        MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedClanInfo.ToString(), list, 0);
    }
}