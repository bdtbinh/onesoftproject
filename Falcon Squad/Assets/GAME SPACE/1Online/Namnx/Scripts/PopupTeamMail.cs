﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;

public class PopupTeamMail : MonoBehaviour
{
    public InfiniteListTeamMail listTeamMail;
    public Transform gWrapper;
    public GameObject lNoClanMember;
    public GameObject gItemContentPrefab;

    public UISprite sSelectAll;

    private const string CHECK = "wolrdchat_checkbox_1";
    private const string UNCHECK = "wolrdchat_checkbox_0";

    private const string DEACTIVE = "wolrdchat_conten_BG_0";
    private const string ACTIVE = "wolrdchat_conten_BG_1";
    [HideInInspector]
    public bool isCheckAll;

    List<UISprite> listSpriteContent = new List<UISprite>();

    private void OnEnable()
    {
        if (CachePvp.clanInfo == null)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_member_in_clan, false);
            bBack();
            return;
        }
        if (CachePvp.clanInfo.totalMember <= 1)
        {
            lNoClanMember.SetActive(true);
        }
        else
        {
            lNoClanMember.SetActive(false);
        }

        typeContent = 1;
        isCheckAll = true;
        ButtonCheckAll();
        StartCoroutine(Wait1Frame());

        if (gWrapper.childCount == 0)
        {
            Vector3 v3 = Vector3.zero;
            float padding = 285;
            listSpriteContent.Clear();
            for (int i = 0; i < 6; i++)
            {
                GameObject item = Instantiate(gItemContentPrefab);
                item.SetActive(true);
                item.transform.SetParent(gWrapper);
                v3.x = padding * i;
                v3.y = 0;
                v3.z = 0;
                item.transform.localPosition = v3;
                v3.x = 1;
                v3.y = 1;
                v3.z = 1;
                item.transform.localScale = v3;
                item.GetComponent<ItemContentTeamMail>().SetData(i);
                listSpriteContent.Add(item.transform.GetChild(0).GetChild(1).GetComponent<UISprite>());
            }
        }
        else
        {
            for (int i = 0; i < listSpriteContent.Count; i++)
            {
                if (i == 0)
                {
                    listSpriteContent[i].spriteName = ACTIVE;
                }
                else
                {
                    listSpriteContent[i].spriteName = DEACTIVE;
                }
            }
        }
        MessageDispatcher.AddListener(EventName.TeamMail.ClickContent.ToString(), OnClickContent, true);
    }

    int typeContent;

    private IEnumerator Wait1Frame()
    {
        yield return null;
        MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedMyClan.ToString(), CachePvp.clanInfo, 0);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.TeamMail.ClickContent.ToString(), OnClickContent, true);
    }

    private void OnClickContent(IMessage rMessage)
    {
        int id = (int)rMessage.Data;
        typeContent = id + 1;
        for (int i = 0; i < listSpriteContent.Count; i++)
        {
            if (i == id)
            {
                listSpriteContent[i].spriteName = ACTIVE;
            }
            else
            {
                listSpriteContent[i].spriteName = DEACTIVE;
            }
        }
    }

    List<string> list = new List<string>();
    public void bSend()
    {
        list.Clear();
        foreach (var item in listTeamMail.dictionaryType)
        {
            if (item.Value == 1)
            {
                list.Add(item.Key);
            }
        }
        if (list.Count > 0 && typeContent >= 1 && typeContent <= 6)
        {
            new CSClanMailNotify(typeContent, list).Send();
        }
    }

    public void CheckSelectAll()
    {
        if (isCheckAll)
        {
            isCheckAll = false;
            sSelectAll.spriteName = UNCHECK;
        }
    }

    public void ButtonCheckAll()
    {
        if (isCheckAll)
        {
            isCheckAll = false;
            listTeamMail.UnCheckAll();
            sSelectAll.spriteName = UNCHECK;
        }
        else
        {
            isCheckAll = true;
            listTeamMail.CheckAll();
            sSelectAll.spriteName = CHECK;
        }
    }

    public void bBack()
    {
        PopupManager.Instance.HideTeamMailPopup();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            bBack();
        }
    }
}
