﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;
using Mp.Pvp;

public class ItemContentTeamMail : MonoBehaviour
{

    public UILabel lStatus;
    public UISprite sprite;

    private const string DEACTIVE = "wolrdchat_conten_BG_0";
    private const string ACTIVE = "wolrdchat_conten_BG_1";

    int id;

    public void SetData(int id)
    {
        this.id = id;
        switch (id)
        {
            case 0:
                lStatus.text = I2.Loc.ScriptLocalization.title_encourage;
                break;
            case 1:
                lStatus.text = I2.Loc.ScriptLocalization.title_warning;
                break;
            case 2:
                lStatus.text = I2.Loc.ScriptLocalization.title_threat;
                break;
            case 3:
                lStatus.text = I2.Loc.ScriptLocalization.title_team_mission;
                break;
            case 4:
                lStatus.text = I2.Loc.ScriptLocalization.title_post_war_boost;
                break;
            case 5:
                lStatus.text = I2.Loc.ScriptLocalization.title_war_notify;
                break;
            default:
                break;
        }
        if (id == 0)
        {
            sprite.spriteName = ACTIVE;
        }
        else
        {
            sprite.spriteName = DEACTIVE;
        }
    }

    public void ClickButton()
    {
        MessageDispatcher.SendMessage(gameObject, EventName.TeamMail.ClickContent.ToString(), id, 0);
    }
}
