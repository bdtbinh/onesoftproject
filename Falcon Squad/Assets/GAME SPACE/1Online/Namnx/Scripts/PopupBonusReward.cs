﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mp.Pvp;
using System;

public class PopupBonusReward : MonoBehaviour
{
    public UILabel lTitle;
    public GameObject itemBonusRewardPrefab;
    public Transform gWrapperGloryPoint;

    public List<UISprite> listSpriteLosses;
    public UILabel lCurrentWin;

    public GameObject gBonus;
    public GameObject gButtonUnlock;
    public UILabel lButtonUnlock;
    public GameObject gLabelRewardUnlock;

    public GameObject gFree;

    private const string RADIO_RED = "radio_btn_red";
    private const string RADIO_BLACK = "radio_btn_black";

    bool m_bonus;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Tournament.MegaTournamentUnlockBonus.ToString(), OnMegaTournamentUnlockBonus, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Tournament.MegaTournamentUnlockBonus.ToString(), OnMegaTournamentUnlockBonus, true);
    }

    private void OnMegaTournamentUnlockBonus(IMessage rMessage)
    {
        SCMegaTournamentUnlockBonus mtub = rMessage.Data as SCMegaTournamentUnlockBonus;
        if (mtub.status == SCMegaTournamentUnlockBonus.SUCCESS)
        {
            if (m_bonus)
            {
                gButtonUnlock.SetActive(false);
                gLabelRewardUnlock.SetActive(true);
            }
        }
    }

    public void SetData(bool bonus)
    {
        m_bonus = bonus;
        RefreshInfo(bonus);
    }

    public void UnlockBonus()
    {
        if (CachePvp.GetGem() < CachePvp.megaTournamentInfo.info.unlockBonusGem)
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
            return;
        }
        new CSMegaTournamentUnlockBonus().Send();
    }

    public void OnClose()
    {
        PopupManager.Instance.HideBonusRewardPopup();
    }

    int GetCountReward(bool bonus = true)
    {
        int cnt = 0;
        for (int i = 0; i < CachePvp.megaTournamentInfo.configRewards.Count; i++)
        {
            if (bonus && CachePvp.megaTournamentInfo.configRewards[i].bonus != null)
            {
                cnt++;
            }
            else if (!bonus && CachePvp.megaTournamentInfo.configRewards[i].free != null)
            {
                cnt++;
            }
        }
        return cnt;
    }

    int GetCountActive()
    {
        int cnt = 0;
        for (int i = 0; i < gWrapperGloryPoint.childCount; i++)
        {
            if (gWrapperGloryPoint.GetChild(i).gameObject.activeSelf)
            {
                cnt++;
            }
        }
        return cnt;
    }

    GameObject GetGOFromPool()
    {
        GameObject item;
        for (int i = 0; i < gWrapperGloryPoint.childCount; i++)
        {
            if (!gWrapperGloryPoint.GetChild(i).gameObject.activeSelf)
            {
                return gWrapperGloryPoint.GetChild(i).gameObject;
            }
        }
        item = Instantiate(itemBonusRewardPrefab);
        return item;
    }

    public void RefreshInfo(bool bonus = true)
    {
        if (bonus)
        {
            lTitle.text = I2.Loc.ScriptLocalization.title_bonus_reward;
            gBonus.SetActive(true);
            if (CachePvp.megaTournamentInfo != null)
            {
                if (CachePvp.megaTournamentInfo.info.unlocked)
                {
                    gButtonUnlock.SetActive(false);
                    gLabelRewardUnlock.SetActive(true);
                }
                else
                {
                    gButtonUnlock.SetActive(true);
                    lButtonUnlock.text = GameContext.FormatNumber(CachePvp.megaTournamentInfo.info.unlockBonusGem);
                    gLabelRewardUnlock.SetActive(false);
                }
            }
            gFree.SetActive(false);
        }
        else
        {
            lTitle.text = I2.Loc.ScriptLocalization.title_free_reward;
            gFree.SetActive(true);
            gBonus.SetActive(false);
        }
        lCurrentWin.text = I2.Loc.ScriptLocalization.current_win + ": " + CachePvp.megaTournamentInfo.info.win;

        int lose = 0;
        if (CachePvp.megaTournamentInfo != null)
        {
            lose = CachePvp.megaTournamentInfo.info.lose;
            if (GetCountActive() != GetCountReward(bonus))
            {
                for (int i = 0; i < gWrapperGloryPoint.childCount; i++)
                {
                    gWrapperGloryPoint.GetChild(i).gameObject.SetActive(false);
                }
                Vector3 v3 = Vector3.zero;
                int cnt = 0;
                for (int i = 0; i < CachePvp.megaTournamentInfo.configRewards.Count; i++)
                {
                    if (bonus && CachePvp.megaTournamentInfo.configRewards[i].bonus != null)
                    {
                        GameObject item = GetGOFromPool();
                        item.SetActive(true);
                        item.transform.SetParent(gWrapperGloryPoint);
                        if (cnt % 4 == 0)
                        {
                            v3.x = -250;
                        }
                        else if (cnt % 4 == 1)
                        {
                            v3.x = -82.5f;
                        }
                        else if (cnt % 4 == 2)
                        {
                            v3.x = 82.5f;
                        }
                        else
                        {
                            v3.x = 250f;
                        }

                        v3.y = 200 - (cnt / 4) * 170;
                        v3.z = 0;
                        item.transform.localPosition = v3;
                        v3.x = 1;
                        v3.y = 1;
                        v3.z = 1;
                        item.transform.localScale = v3;
                        item.GetComponent<ItemBonusReward>().SetData(i, true);
                        cnt++;
                    }
                    else if (!bonus && CachePvp.megaTournamentInfo.configRewards[i].free != null)
                    {
                        GameObject item = GetGOFromPool();
                        item.SetActive(true);
                        item.transform.SetParent(gWrapperGloryPoint);
                        if (cnt % 4 == 0)
                        {
                            v3.x = -250;
                        }
                        else if (cnt % 4 == 1)
                        {
                            v3.x = -82.5f;
                        }
                        else if (cnt % 4 == 2)
                        {
                            v3.x = 82.5f;
                        }
                        else
                        {
                            v3.x = 250f;
                        }

                        v3.y = 200 - (cnt / 4) * 170;
                        v3.z = 0;
                        item.transform.localPosition = v3;
                        v3.x = 1;
                        v3.y = 1;
                        v3.z = 1;
                        item.transform.localScale = v3;
                        item.GetComponent<ItemBonusReward>().SetData(i);
                        cnt++;
                    }
                }
            }
        }

        for (int i = 0; i < listSpriteLosses.Count; i++)
        {
            if (i < lose)
            {
                listSpriteLosses[i].spriteName = RADIO_RED;
            }
            else
            {
                listSpriteLosses[i].spriteName = RADIO_BLACK;
            }
        }
    }
}
