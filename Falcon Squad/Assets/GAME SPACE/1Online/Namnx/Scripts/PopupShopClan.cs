﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OneSoftGame.Tools;
using com.ootii.Messages;
using Mp.Pvp;
using Facebook.Unity;
using System;

public class PopupShopClan : MonoBehaviour
{
    public UILabel lMedalTop;

    public UILabel lResetTime;
    public UILabel lPriceReset;
    public UILabel lButtonReset;
    public UISprite sButtonResetPrice;

    public List<UISprite> listSpriteItem;
    public List<UILabel> listLabelQuantity;
    public List<UISprite> listSpriteQuantity;
    public List<UISprite> listSpriteBackgroundPrice;
    public List<UILabel> listLabelPrice;
    public List<UILabel> listLabelPriceSoldOut;
    public List<UISprite> listSpritePrice;

    List<string> listSpriteName = new List<string>()
    {
        "Enegy_p1", "Enegy_p2","Enegy_p3","Enegy_p4","Enegy_p5",
        "Cardwing1_P1", "Cardwing1_P2" , "Cardwing1_P3", "Cardwing1_P4", "Cardwing1_P5",
        "Cardwing2_P1", "Cardwing2_P2" , "Cardwing2_P3", "Cardwing2_P4", "Cardwing2_P5"
    };

    private const string CARD = "icon_aircraft_Pieces";
    private const string ENERGY = "Icon_enegy1";

    private const string BTN_RED = "IAP_btn_dollar";
    private const string BTN_GREY = "IAP_btn_dollar_d";
    Shop m_shop;

    private void OnEnable()
    {
        lMedalTop.text = GameContext.FormatNumber(CachePvp.Medal);
        MessageDispatcher.AddListener(EventName.Shop.MyShop.ToString(), OnMyShop, true);
        MessageDispatcher.AddListener(EventName.Shop.ShopReset.ToString(), OnShopReset, true);
        MessageDispatcher.AddListener(EventName.Shop.ShopBuyItem.ToString(), OnShopBuyItem, true);
        new CSMyShop().Send();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Shop.MyShop.ToString(), OnMyShop, true);
        MessageDispatcher.RemoveListener(EventName.Shop.ShopReset.ToString(), OnShopReset, true);
        MessageDispatcher.RemoveListener(EventName.Shop.ShopBuyItem.ToString(), OnShopBuyItem, true);
    }

    int shopResetPrice;

    private void OnShopBuyItem(IMessage rMessage)
    {
        SCShopBuyItem sbi = rMessage.Data as SCShopBuyItem;
        switch (sbi.status)
        {
            case SCShopBuyItem.SUCCESS:
                //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.success, true, 1.5f);
                m_shop = sbi.shop;
                timeRemaining = (int)m_shop.timeToReset;
                if (countDown != null)
                {
                    StopCoroutine(countDown);
                }
                if (timeRemaining > 0)
                {
                    countDown = CountDown();
                    StartCoroutine(countDown);
                }
                ReloadShop(m_shop);
                break;
            case SCShopBuyItem.NOT_ENOUGH_MEDAL:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_medal, false, 1.5f);
                break;
            case SCShopBuyItem.ITEM_NOT_IN_SHOP:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_item_not_in_shop, false, 1.5f);
                break;
            case SCShopBuyItem.ITEM_SOLD_OUT:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_item_sold_out, false, 1.5f);
                break;
            default:
                PopupManagerCuong.Instance.ShowTextNotifiToast(sbi.message, false, 1.5f);
                break;
        }
    }

    private void OnShopReset(IMessage rMessage)
    {
        SCShopReset sr = rMessage.Data as SCShopReset;
        switch (sr.status)
        {
            case SCShopReset.SUCCESS:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.success, true, 1.5f);
                m_shop = sr.shop;
                timeRemaining = (int)m_shop.timeToReset;
                if (countDown != null)
                {
                    StopCoroutine(countDown);
                }
                if (timeRemaining > 0)
                {
                    countDown = CountDown();
                    StartCoroutine(countDown);
                }
                ReloadShop(m_shop);
                break;
            case SCShopReset.NOT_ENOUGH_MEDAL:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_medal, false, 1.5f);
                break;
            case SCShopReset.NOT_ENOUGH_RESET_QUOTA:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_reset_quota, false, 1.5f);
                break;
            default:
                PopupManagerCuong.Instance.ShowTextNotifiToast(sr.message, false, 1.5f);
                break;
        }
    }

    Vector3 tempVector3;

    void ReloadShop(Shop shop)
    {
        CachePvp.Medal = shop.medal;
        lMedalTop.text = GameContext.FormatNumber(CachePvp.Medal);
        shopResetPrice = shop.resetPrice;
        lPriceReset.text = GameContext.FormatNumber(shop.resetPrice);
        sButtonResetPrice.color = Color.white;
        lButtonReset.text = I2.Loc.ScriptLocalization.or_reset_now;
        if (shop.resetRemain == 0)
        {
            lPriceReset.text = "";
            sButtonResetPrice.color = Color.clear;
            lButtonReset.text = I2.Loc.ScriptLocalization.out_of_reset;
        }
        for (int i = 0; i < 6; i++)
        {
            listSpriteItem[i].spriteName = listSpriteName[CachePvp.listShopItem[i].id - 1];
            listLabelQuantity[i].text = GameContext.FormatNumber(CachePvp.listShopItem[i].quantity);
            if (CachePvp.listShopItem[i].id <= 5)
            {
                listSpriteQuantity[i].spriteName = ENERGY;
                listSpriteQuantity[i].MakePixelPerfect();
                tempVector3.x = 1f;
                tempVector3.y = 1f;
                tempVector3.z = 1f;
                listSpriteQuantity[i].transform.localScale = tempVector3;
            }
            else
            {
                listSpriteQuantity[i].spriteName = CARD;
                listSpriteQuantity[i].MakePixelPerfect();
                tempVector3.x = 0.5f;
                tempVector3.y = 0.5f;
                tempVector3.z = 0.5f;
                listSpriteQuantity[i].transform.localScale = tempVector3;
            }
            if (CachePvp.listShopItem[i].soldOut)
            {
                listLabelPrice[i].text = "";
                listLabelPriceSoldOut[i].text = I2.Loc.ScriptLocalization.sold_out;
                listSpritePrice[i].enabled = false;
                listSpriteBackgroundPrice[i].spriteName = BTN_GREY;
            }
            else
            {
                listLabelPrice[i].text = GameContext.FormatNumber(CachePvp.listShopItem[i].price);
                listLabelPriceSoldOut[i].text = "";
                listSpritePrice[i].enabled = true;
                listSpriteBackgroundPrice[i].spriteName = BTN_RED;
            }
        }
    }

    IEnumerator countDown;
    int target;
    int timeRemaining;

    private void OnMyShop(IMessage rMessage)
    {
        SCMyShop ms = rMessage.Data as SCMyShop;
        if (ms.status == SCMyShop.SUCCESS)
        {
            m_shop = ms.shop;
            timeRemaining = (int)m_shop.timeToReset;
            if (countDown != null)
            {
                StopCoroutine(countDown);
            }
            if (timeRemaining > 0)
            {
                countDown = CountDown();
                StartCoroutine(countDown);
            }
            else
            {
                Debug.Log("timeRemaining : " + timeRemaining);
                lResetTime.text = I2.Loc.ScriptLocalization.reset_in.Replace("%{time}", "[FFFF00FF]00:00:00[-]");
                new CSMyShop().Send();
            }
            ReloadShop(m_shop);
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ms.message, false, 1.5f);
        }
    }

    private IEnumerator CountDown()
    {
        TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        target = (int)span.TotalSeconds + timeRemaining;
        while (timeRemaining >= 0)
        {
            lResetTime.text = I2.Loc.ScriptLocalization.reset_in.Replace("%{time}", "[FFFF00FF]" + (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00") + "[-]");
            TimeSpan temp_span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            timeRemaining = target - (int)temp_span.TotalSeconds;
            yield return new WaitForSecondsRealtime(1);
        }
        yield return new WaitForSecondsRealtime(1);
        new CSMyShop().Send();
    }

    public void ButtonClose()
    {
        PopupManager.Instance.HideShopClanPopup();
    }

    public void ButtonReset()
    {
        if (m_shop.resetRemain == 0)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_have_used_reset, false, 1.5f);
            return;
        }
        PopupManager.Instance.ShowResetShopClanPopup(shopResetPrice);
    }

    public void BuyItem1()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        if (CachePvp.listShopItem != null)
        {
            if (CachePvp.Medal < CachePvp.listShopItem[0].price)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_medal, false, 1.5f);
                return;
            }
            new CSShopBuyItem(CachePvp.listShopItem[0].id).Send();
        }
    }

    public void BuyItem2()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        if (CachePvp.listShopItem != null)
        {
            if (CachePvp.Medal < CachePvp.listShopItem[1].price)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_medal, false, 1.5f);
                return;
            }
            new CSShopBuyItem(CachePvp.listShopItem[1].id).Send();
        }
    }

    public void BuyItem3()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        if (CachePvp.listShopItem != null)
        {
            if (CachePvp.Medal < CachePvp.listShopItem[2].price)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_medal, false, 1.5f);
                return;
            }
            new CSShopBuyItem(CachePvp.listShopItem[2].id).Send();
        }
    }

    public void BuyItem4()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        if (CachePvp.listShopItem != null)
        {
            if (CachePvp.Medal < CachePvp.listShopItem[3].price)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_medal, false, 1.5f);
                return;
            }
            new CSShopBuyItem(CachePvp.listShopItem[3].id).Send();
        }
    }

    public void BuyItem5()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        if (CachePvp.listShopItem != null)
        {
            if (CachePvp.Medal < CachePvp.listShopItem[4].price)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_medal, false, 1.5f);
                return;
            }
            new CSShopBuyItem(CachePvp.listShopItem[4].id).Send();
        }
    }

    public void BuyItem6()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        if (CachePvp.listShopItem != null)
        {
            if (CachePvp.Medal < CachePvp.listShopItem[5].price)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_medal, false, 1.5f);
                return;
            }
            new CSShopBuyItem(CachePvp.listShopItem[5].id).Send();
        }
    }
}
