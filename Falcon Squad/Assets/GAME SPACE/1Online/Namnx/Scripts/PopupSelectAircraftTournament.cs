﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupSelectAircraftTournament : MonoBehaviour
{
    [SerializeField]
    private ElementSelectAircraftTournament[] listElements;

    [SerializeField]
    private UISprite btnConfirm;

    private ElementSelectAircraftTournament selectedAircraftTournament;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventID.ON_SELECTED_CALLING_BACKUP, OnSelectedCallingBackup, true);

        InitListElements();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_SELECTED_CALLING_BACKUP, OnSelectedCallingBackup, true);
    }

    private void OnSelectedCallingBackup(IMessage msg)
    {
        btnConfirm.spriteName = "PVP_btn_fight";

        if (selectedAircraftTournament != null)
        {
            selectedAircraftTournament.SetAsDeselected();
        }

        selectedAircraftTournament = (ElementSelectAircraftTournament)msg.Sender;
    }

    private void InitListElements()
    {
        int totalAircraft = System.Enum.GetValues(typeof(AircraftTypeEnum)).Length;
        btnConfirm.spriteName = "PVP_btn_fight_d";
        selectedAircraftTournament = null;
        int j;
        for (int i = 0; i < listElements.Length; i++)
        {
            j = i;
            if (i >= 3) j = i + 2;
            listElements[i].CreateItem((AircraftTypeEnum)(j + 1));
        }
        int selected = CacheGame.GetSpaceShipTournament();
        if (selected < 4) //1,2,3
        {
            listElements[selected - 1].SetAsSelected();
        }
        else
        {
            listElements[selected - 3].SetAsSelected();
        }
    }

    public void OnClickBtnConfirm()
    {
        if (selectedAircraftTournament != null)
        {
            CacheGame.SetSpaceShipTournament((int)selectedAircraftTournament.AircraftType);
            PopupManager.Instance.HideSelectAircraftPopup();
            MessageDispatcher.SendMessage(EventName.Tournament.RefreshSelected.ToString());
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_use_another_aircraft, false, 1.5f);
        }
    }

    public void OnSkip()
    {
        PopupManager.Instance.HideSelectAircraftPopup();
    }
}
