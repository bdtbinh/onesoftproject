﻿using UnityEngine;
using System.Collections;

public class EventName
{
    public const string VIDEO_GIFT = "video_gift";
    public const string GLORY_CHEST = "glory_chest";
    public const string POPUP_MAIL = "popup_mail";
    public const string POPUP_TOURNAMENT = "popup_tournament";
    public const string PVP_REWARD = "pvp_reward";
    public const string GIFT_CODE = "gift_code";
    public const string CLAN_DONATE = "clan_donate";
    public const string CLAN_SHOP_BUY_ITEM = "clan_shop_buy_item";
    public const string PROMOTE_EVENT_NAME = "promote_event_ver1_drone6";
    public const string POPUP_TOURNAMENT_2V2 = "popup_tournament_2v2";

    public enum LoadLeaderboard
    {
        LoadWorldRank,
        LoadedWorldRank,
        LoadedCacheWorldRank,
        LoadLocalRank,
        LoadedLocalRank,
        LoadedCacheLocalRank,
        LoadFacebookRank,
        LoadedFacebookRank,
        LoadedCacheFacebookRank,
        LoadWorldMegaRank,
        LoadedWorldMegaRank,
        LoadedCacheWorldMegaRank,
        LoadLocalMegaRank,
        LoadedLocalMegaRank,
        LoadedCacheLocalMegaRank,
        LoadFacebookMegaRank,
        LoadedFacebookMegaRank,
        LoadedCacheFacebookMegaRank
    }

    public enum LiveScore
    {
        LoadListRoom,
        LoadedListRoom,
        LoadedRoomInfo
    }

    public enum Clan
    {
        ChangeNewMemberClan,
        LoadJoinRequest,
        LoadedJoinRequest,
        LoadSearchClan,
        LoadedSearchClan,
        LoadChat,
        LoadedChat,
        LoadedChatError,
        InputChatSubmitClan,
        InputChatSubmitWorld,
        LoadMyClan,
        LoadedMyClan,
        LoadedSuggestClan,
        LoadedSuggestClanForList,
        RequestedJoinClan,
        RequestedJoinClanError,
        CreatedClan,
        LeaveClan,
        KickClan,
        ConfirmJoinRequest,
        ConfirmJoinRequestError,
        LoadClanInvite,
        LoadedClanInvite,
        LoadedClanInfo,
        AcceptClanInvitation,
        AcceptedClanInvitation,
        RejectClanInvitation,
        RejectedClanInvitation,
        MyClanRequest,
        ClanInvitation,
        PlayerInfo,
        GrantViceMaster,
        RevokeViceMaster,
        GrantMaster,
        LoadListClanTopWorld,
        LoadedListClanTopWorld,
        EditedClan,
        ClanMember,
        ClanInfo,
        RequestItem,
        DonateRequest,
        CSDonate,
        SCDonate,
        DonateLog,
        DonateNotify,
        DonateQueue,
        ShowButtonRequestChat,
        ChangeTopChat,
        ChangeBottomChat,
        ChangeMedal
    }

    public enum LoadMailBox
    {
        LoadedMailBox,
        ChangeNewMailNumber
    }

    public enum SyncData
    {
        NoSync,
        SyncFromClientToServer,
        SyncFromServerToClient,
        StopCoroutineWait
    }

    public enum LoadProfile
    {
        LoadProfile,
        LoadProfileLocal,
        LoadedProfile,
        RefreshAvatar,
        LoadListFriends,
        LoadedListFriends,
        LoadFriendshipStatus,
        LoadFriendRequest,
        LoadFollow,
        LoadedFriendRequest,
        LoadedFollow,
        RequestFriend,
        FollowFriend,
        UnfollowFriend,
        LoadFriendshipStats,
        Unfriend
    }

    public enum InvitePopup
    {
        ShowInviteButton,
        PressInviteButton,
        ShowInvitePopup
    }

    public enum LoginFacebook
    {
        LoggedFacebook,
        CheckFriends
    }

    public enum ChangeProperties
    {
        UpdatePlayerInfo,
        //		ReconnectFromServer,
        DisconnectFromServer,
        ChangeVip
    }

    public enum PopupExtra
    {
        GoToPVP,
        DisablePopupExtra
    }

    public enum WatchVideo
    {
        WatchVideo
    }

    public enum GloryChest
    {
        CloseSelectLevel,
        TournamentInfo,
        TournamentClaim,
        LoadTournamentInfo
    }

    public enum PVP
    {
        StartGamePVP,
        InvitePVP,
        InvitePVPConfirm,
        SessionRewardedInfo,
        SessionRewardedClaim,
        CoopRoomInfo,
        CSCoopPVPInvite,
        SCCoopPVPInvite,
        CSCoopPvPConfirm,
        SCCoopPvPConfirm,
        CSCoopPvPReady,
        SCCoopPvPReady,
        CSCoopPvPOut,
        SCCoopPvPOut,
        CSCoopPvPDied,
        SCCoopPvPDied,
        StartGamePVP2v2,
        SCCoopPvPRematch,
        CoopPvPTournamentInfo,
        CoopPvPSeasonRewardedInfo,
        CoopPvPSeasonRewardedClaim,
        CoopPvPEnemyDie
    }

    public enum UIHome
    {
        SetTurnOnTeamSCClientConfig,
        GiftCode
    }

    public enum Tournament
    {
        MegaTournament,
        MegaTournamentInfo,
        MegaTournamentClaim,
        MegaTournamentUnlockBonus,
        RefreshSelected
    }

    public enum SelectLevelEvent
    {
        SelectLvEvent,
        SelectLvEventInfo,
        SelectLvEventTicket
    }

    public enum Shop
    {
        MyShop,
        ShopReset,
        ShopBuyItem
    }

    public enum RandomShop
    {
        MyRandomShop,
        RandomShopReset,
        RandomShopBuyItem
    }

    public enum RoyaltyPack
    {
        BuyRoyaltyPack,
        GetRoyaltyPackInfo,
        RoyaltyPackOpenChest
    }

    public enum UDP
    {
        ActiveSkill2v2,
        AddPowerUp
    }

    public enum TeamMail
    {
        ClickContent,
        SentMail
    }
}