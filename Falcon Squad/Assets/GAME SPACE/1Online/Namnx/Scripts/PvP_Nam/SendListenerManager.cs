﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using Mp.Pvp;
using System;

public class SendListenerManager : MonoBehaviour
{

    private string loadRank = "";

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadWorldRank.ToString(), OnLoadWorldRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadLocalRank.ToString(), OnLoadLocalRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadFacebookRank.ToString(), OnLoadFacebookRank, true);

        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadWorldMegaRank.ToString(), OnLoadWorldMegaRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadLocalMegaRank.ToString(), OnLoadLocalMegaRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadFacebookMegaRank.ToString(), OnLoadFacebookMegaRank, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadWorldRank.ToString(), OnLoadWorldRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadLocalRank.ToString(), OnLoadLocalRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadFacebookRank.ToString(), OnLoadFacebookRank, true);

        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadWorldMegaRank.ToString(), OnLoadWorldMegaRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadLocalMegaRank.ToString(), OnLoadLocalMegaRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadFacebookMegaRank.ToString(), OnLoadFacebookMegaRank, true);
    }

    //WORLD(0), COUNTRY(1), FRIEND(2), WORLD_OFFLINE(3), COUNTRY_OFFLINE(4), FRIEND_OFFLINE(5);

    void OnLoadWorldRank(IMessage msg)
    {
        if ((msg.Data != null && (int)msg.Data == 1))
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                LoadWorld(true);
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                LoadWorldPVP(true);
            }
        }
        else
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                if (!loadRank.Equals(CachePvp.RANK_WORLD))
                {
                    LoadWorld();
                }
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                if (!loadRank.Equals(CachePvp.RANK_WORLD_PVP))
                {
                    LoadWorldPVP();
                }
            }
        }
    }

    void OnLoadWorldMegaRank(IMessage msg)
    {
        if ((msg.Data != null && (int)msg.Data == 1))
        {
            LoadWorldPVP(true);
        }
        else
        {
            if (!loadRank.Equals(CachePvp.RANK_WORLD_PVP))
            {
                LoadWorldPVP();
            }
        }
    }

    void LoadWorldPVP(bool force = false)
    {
        loadRank = CachePvp.RANK_WORLD_PVP;
        if (CachePvp.LoadMegaRank)
        {
            if (PopupManager.Instance.GetAvailableClickWorldPVP())
            {
                new CSLeaderboard(6).Send();
                PopupManager.Instance.ResetClickWorldPVP();
                Debug.Log("Load World PVP Mega Rank");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickWorldPassedPVP();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(6).Send();
                    PopupManager.Instance.ResetClickWorldPVP();
                    Debug.Log("Load World PVP Mega Rank");
                }
                else
                {
                    if (force)
                    {
                        MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadedCacheWorldMegaRank.ToString(), 1, 0);
                    }
                    else
                    {
                        MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheWorldMegaRank.ToString());
                    }
                    Debug.Log("Load Cache World PVP Mega Rank");
                }
            }
        }
        else
        {
            if (PopupManager.Instance.GetAvailableClickWorldPVP())
            {
                new CSLeaderboard(0).Send();
                PopupManager.Instance.ResetClickWorldPVP();
                Debug.Log("Load World PVP Rank");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickWorldPassedPVP();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(0).Send();
                    PopupManager.Instance.ResetClickWorldPVP();
                    Debug.Log("Load World PVP Rank");
                }
                else
                {
                    if (force)
                    {
                        MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadedCacheWorldRank.ToString(), 1, 0);
                    }
                    else
                    {
                        MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheWorldRank.ToString());
                    }
                    Debug.Log("Load Cache World PVP Rank");
                }
            }
        }
    }

    void LoadWorld(bool force = false)
    {
        loadRank = CachePvp.RANK_WORLD;
        if (PopupManager.Instance.typeRank == 0)
        {
            if (PopupManager.Instance.GetAvailableClickWorld())
            {
                new CSLeaderboard(3).Send();
                PopupManager.Instance.ResetClickWorld();
                Debug.Log("Load World Rank");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickWorldPassed();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(3).Send();
                    PopupManager.Instance.ResetClickWorld();
                    Debug.Log("Load World Rank");
                }
                else
                {
                    if (force)
                    {
                        MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadedCacheWorldRank.ToString(), 1, 0);
                    }
                    else
                    {
                        MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheWorldRank.ToString());
                    }
                    Debug.Log("Load Cache World Rank");
                }
            }
        }
        else if (PopupManager.Instance.typeRank == 2)
        {
            if (PopupManager.Instance.GetAvailableClickWorld2v2())
            {
                new CSLeaderboard(9).Send();
                PopupManager.Instance.ResetClickWorld2v2();
                Debug.Log("Load World Rank 2v2");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickWorldPassed2v2();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(9).Send();
                    PopupManager.Instance.ResetClickWorld2v2();
                    Debug.Log("Load World Rank 2v2");
                }
                else
                {
                    if (force)
                    {
                        MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadedCacheWorldRank.ToString(), 1, 0);
                    }
                    else
                    {
                        MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheWorldRank.ToString());
                    }
                    Debug.Log("Load Cache World Rank 2v2");
                }
            }
        }
    }

    void OnLoadLocalRank(IMessage msg)
    {
        if ((msg.Data != null && (int)msg.Data == 1))
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                LoadLocal(true);
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                LoadLocalPVP(true);
            }
        }
        else
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                if (!loadRank.Equals(CachePvp.RANK_LOCAL))
                {
                    LoadLocal();
                }
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                if (!loadRank.Equals(CachePvp.RANK_LOCAL_PVP))
                {
                    LoadLocalPVP();
                }
            }
        }
    }

    void OnLoadLocalMegaRank(IMessage msg)
    {
        if ((msg.Data != null && (int)msg.Data == 1))
        {
            LoadLocalPVP(true);
        }
        else
        {
            if (!loadRank.Equals(CachePvp.RANK_LOCAL_PVP))
            {
                LoadLocalPVP();
            }
        }
    }

    void LoadLocal(bool force = false)
    {
        loadRank = CachePvp.RANK_LOCAL;
        if (PopupManager.Instance.typeRank == 0)
        {
            if (PopupManager.Instance.GetAvailableClickLocal())
            {
                new CSLeaderboard(4).Send();
                PopupManager.Instance.ResetClickLocal();
                Debug.Log("Load Local Rank");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickLocalPassed();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(4).Send();
                    PopupManager.Instance.ResetClickLocal();
                    Debug.Log("Load Local Rank");
                }
                else
                {
                    if (force)
                    {
                        MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadedCacheLocalRank.ToString(), 1, 0);
                    }
                    else
                    {
                        MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheLocalRank.ToString());
                    }
                    Debug.Log("Load Cache Local Rank");
                }
            }
        }
        else if (PopupManager.Instance.typeRank == 2)
        {
            if (PopupManager.Instance.GetAvailableClickLocal2v2())
            {
                new CSLeaderboard(10).Send();
                PopupManager.Instance.ResetClickLocal2v2();
                Debug.Log("Load Local Rank 2v2");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickLocalPassed2v2();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(10).Send();
                    PopupManager.Instance.ResetClickLocal2v2();
                    Debug.Log("Load Local Rank 2v2");
                }
                else
                {
                    if (force)
                    {
                        MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadedCacheLocalRank.ToString(), 1, 0);
                    }
                    else
                    {
                        MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheLocalRank.ToString());
                    }
                    Debug.Log("Load Cache Local Rank 2v2");
                }
            }
        }
    }

    void LoadLocalPVP(bool force = false)
    {
        loadRank = CachePvp.RANK_LOCAL_PVP;
        if (CachePvp.LoadMegaRank)
        {
            if (PopupManager.Instance.GetAvailableClickLocalPVP())
            {
                new CSLeaderboard(7).Send();
                PopupManager.Instance.ResetClickLocalPVP();
                Debug.Log("Load Local PVP Mega Rank");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickLocalPassedPVP();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(7).Send();
                    PopupManager.Instance.ResetClickLocalPVP();
                    Debug.Log("Load Local PVP Mega Rank");
                }
                else
                {
                    if (force)
                    {
                        MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadedCacheLocalMegaRank.ToString(), 1, 0);
                    }
                    else
                    {
                        MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheLocalMegaRank.ToString());
                    }
                    Debug.Log("Load Cache Local PVP Mega Rank");
                }
            }
        }
        else
        {
            if (PopupManager.Instance.GetAvailableClickLocalPVP())
            {
                new CSLeaderboard(1).Send();
                PopupManager.Instance.ResetClickLocalPVP();
                Debug.Log("Load Local PVP Rank");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickLocalPassedPVP();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(1).Send();
                    PopupManager.Instance.ResetClickLocalPVP();
                    Debug.Log("Load Local PVP Rank");
                }
                else
                {
                    if (force)
                    {
                        MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadedCacheLocalRank.ToString(), 1, 0);
                    }
                    else
                    {
                        MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheLocalRank.ToString());
                    }
                    Debug.Log("Load Cache Local PVP Rank");
                }
            }
        }
    }

    void OnLoadFacebookRank(IMessage msg)
    {
        if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
        {
            if (!loadRank.Equals(CachePvp.RANK_FACEBOOK))
            {
                LoadFacebook();
            }
        }
        else if (PopupManager.Instance.typeRank == 1)
        {
            if (!loadRank.Equals(CachePvp.RANK_FACEBOOK_PVP))
            {
                LoadFacebookPVP();
            }
        }
    }

    void OnLoadFacebookMegaRank(IMessage msg)
    {
        if (!loadRank.Equals(CachePvp.RANK_FACEBOOK_PVP))
        {
            LoadFacebookPVP();
        }
    }

    void LoadFacebook()
    {

        loadRank = CachePvp.RANK_FACEBOOK;
        if (PopupManager.Instance.typeRank == 0)
        {
            if (PopupManager.Instance.GetAvailableClickFacebook())
            {
                new CSLeaderboard(5).Send();
                PopupManager.Instance.ResetClickFacebook();
                Debug.Log("Load Facebook Rank");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickFacebookPassed();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(5).Send();
                    PopupManager.Instance.ResetClickFacebook();
                    Debug.Log("Load Facebook Rank");
                }
                else
                {
                    MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheFacebookRank.ToString());
                    Debug.Log("Load Cache Facebook Rank");
                }
            }
        }
        else if (PopupManager.Instance.typeRank == 2)
        {
            if (PopupManager.Instance.GetAvailableClickFacebook2v2())
            {
                new CSLeaderboard(11).Send();
                PopupManager.Instance.ResetClickFacebook2v2();
                Debug.Log("Load Facebook Rank 2v2");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickFacebookPassed2v2();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(11).Send();
                    PopupManager.Instance.ResetClickFacebook2v2();
                    Debug.Log("Load Facebook Rank 2v2");
                }
                else
                {
                    MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheFacebookRank.ToString());
                    Debug.Log("Load Cache Facebook Rank 2v2");
                }
            }
        }
    }

    void LoadFacebookPVP()
    {
        loadRank = CachePvp.RANK_FACEBOOK_PVP;
        if (CachePvp.LoadMegaRank)
        {
            if (PopupManager.Instance.GetAvailableClickFacebookPVP())
            {
                new CSLeaderboard(8).Send();
                PopupManager.Instance.ResetClickFacebookPVP();
                Debug.Log("Load Facebook PVP Mega Rank");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickFacebookPassedPVP();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(8).Send();
                    PopupManager.Instance.ResetClickFacebookPVP();
                    Debug.Log("Load Facebook PVP Mega Rank");
                }
                else
                {
                    MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheFacebookMegaRank.ToString());
                    Debug.Log("Load Cache Facebook PVP Mega Rank");
                }
            }
        }
        else
        {
            if (PopupManager.Instance.GetAvailableClickFacebookPVP())
            {
                new CSLeaderboard(2).Send();
                PopupManager.Instance.ResetClickFacebookPVP();
                Debug.Log("Load Facebook PVP Rank");
            }
            else
            {
                double totalSeconds = PopupManager.Instance.GetSecondsClickFacebookPassedPVP();
                if (totalSeconds > 10)
                {
                    new CSLeaderboard(2).Send();
                    PopupManager.Instance.ResetClickFacebookPVP();
                    Debug.Log("Load Facebook PVP Rank");
                }
                else
                {
                    MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadedCacheFacebookRank.ToString());
                    Debug.Log("Load Cache Facebook PVP Rank");
                }
            }
        }
    }
}
