﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonTopRightHome : MonoBehaviour
{
    private GroupButtonTopRightHome group;
    public bool canShow = true;
    private bool _showFirst;
    public bool showFirst
    {
        get
        {
            return _showFirst;
        }
        set
        {
            _showFirst = value;
            if (group != null)
            {
                if (value || !gameObject.activeInHierarchy)
                {
                    group.RePosition();
                }
            }
        }
    }

    private void Awake()
    {
        group = transform.parent.GetComponent<GroupButtonTopRightHome>();
        if (name.Equals("B_Community") && CacheGame.GetMaxLevel3Difficult() <= 21 && !GameContext.IS_CHINA_VERSION)
        {
            showFirst = true;
        }
    }
}
