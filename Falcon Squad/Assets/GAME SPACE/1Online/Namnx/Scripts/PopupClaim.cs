﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;
using Mp.Pvp;

public class PopupClaim : MonoBehaviour
{
    public List<GameObject> listRewardParticle;
    public List<UISprite> listRewardSprite;
    public List<UILabel> listRewardLabel;
    public List<TweenPosition> listTweenPosition;
    public List<TweenAlpha> listTweenAlpha;

    public GameObject background;
    public GameObject title;

    private List<ItemVolumeFalconMail> promotions;
    bool clickConfirm;

    string why = "";

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.PVP.SessionRewardedClaim.ToString(), OnSessionRewardedClaim, true);
        MessageDispatcher.AddListener(EventName.PVP.CoopPvPSeasonRewardedClaim.ToString(), OnCoopPvPSeasonRewardedClaim, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.PVP.SessionRewardedClaim.ToString(), OnSessionRewardedClaim, true);
        MessageDispatcher.RemoveListener(EventName.PVP.CoopPvPSeasonRewardedClaim.ToString(), OnCoopPvPSeasonRewardedClaim, true);
    }

    private void OnCoopPvPSeasonRewardedClaim(IMessage rMessage)
    {
        SCCoopPvPSeasonRewardedClaim c = rMessage.Data as SCCoopPvPSeasonRewardedClaim;
        if (c.status == SCCoopPvPSeasonRewardedClaim.SUCCESS)
        {
            clickConfirm = false;
            rewardFromPVP2v2 = false;
            GetRewardConfirm();
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(c.message, false, 1.5f);
            PopupManager.Instance.HideClaimPopup();
        }
    }

    private void OnSessionRewardedClaim(IMessage rMessage)
    {
        SCPvPSessionRewardedClaim c = rMessage.Data as SCPvPSessionRewardedClaim;
        if (c.status == SCPvPSessionRewardedClaim.SUCCESS)
        {
            clickConfirm = false;
            rewardFromPVP = false;
            GetRewardConfirm();
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(c.message, false, 1.5f);
            PopupManager.Instance.HideClaimPopup();
        }
    }

    bool rewardFromPVP;
    bool rewardFromPVP2v2;

    public void GetReward(List<ItemVolumeFalconMail> promotions, string why, bool rewardFromPVP = false, bool rewardFromPVP2v2 = false)
    {
        this.why = why;
        this.rewardFromPVP = rewardFromPVP;
        this.rewardFromPVP2v2 = rewardFromPVP2v2;
        clickConfirm = false;
        this.promotions = promotions;
        if (promotions != null)
        {
            Debug.Log("promotions ! null");
            int cnt = 0;
            for (int i = 0; i < listRewardParticle.Count; i++)
            {
                listRewardParticle[i].SetActive(false);
            }
            foreach (var item in promotions)
            {
                if (cnt < 3)
                {
                    listTweenPosition[cnt].Play(true);
                    listTweenPosition[cnt].ResetToBeginning();
                    listTweenPosition[cnt].enabled = false;
                    listTweenAlpha[cnt].Play(true);
                    listTweenAlpha[cnt].ResetToBeginning();
                    listTweenAlpha[cnt].enabled = false;
                    listRewardParticle[cnt].SetActive(true);
                    background.SetActive(false);
                    title.SetActive(false);
                    listRewardSprite[cnt].spriteName = FalconMail.GetSpriteName(item.key, item.value);
                    switch (item.key)
                    {
                        case FalconMail.ITEM_GOLD:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_GEM:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_ENERGY:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_POWER_UP:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_ACTIVESKILL:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_LIFE:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_PLANE:
                            listRewardLabel[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_WINGMAN:
                            listRewardLabel[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_WING:
                            listRewardLabel[cnt].gameObject.SetActive(false);
                            break;
                        //case FalconMail.ITEM_SECONDARY_WEAPON:
                        //    listRewardLabel[cnt].gameObject.SetActive(false);
                        //    switch (item.value)
                        //    {
                        //        case 0:
                        //            listRewardSprite[cnt].spriteName = SUPER_WEAPON_1_GIFT_SPRITE_NAME;
                        //            break;
                        //        case 1:
                        //            listRewardSprite[cnt].spriteName = SUPER_WEAPON_2_GIFT_SPRITE_NAME;
                        //            break;
                        //        case 2:
                        //            listRewardSprite[cnt].spriteName = SUPER_WEAPON_3_GIFT_SPRITE_NAME;
                        //            break;
                        //        case 3:
                        //            listRewardSprite[cnt].spriteName = SUPER_WEAPON_4_GIFT_SPRITE_NAME;
                        //            break;
                        //        default:
                        //            break;
                        //    }
                        //    break;
                        case FalconMail.ITEM_CARD_PLANE_GENERAL:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_1:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_2:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_3:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_4:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_5:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_6:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_7:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_8:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_GENERAL:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_1:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_2:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_3:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_4:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_5:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_6:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_GENERAL:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_1:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_2:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_3:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_VIP_POINT:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_LUCKY_WHEEL_TICKET:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_PREMIUM_PACK:
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_INFINITY_PACK:
                            listRewardLabel[cnt].gameObject.SetActive(false);
                            break;
                        default:
                            break;
                    }
                    cnt++;
                }
            }
            if (cnt == 1)
            {
                listRewardParticle[0].transform.localPosition = Vector3.zero;
            }
            else if (cnt == 2)
            {
                listRewardParticle[0].transform.localPosition = new Vector3(-75, 0, 0);
                listRewardParticle[1].transform.localPosition = new Vector3(75, 0, 0);
            }
            else if (cnt == 3)
            {
                listRewardParticle[0].transform.localPosition = new Vector3(-150, 0, 0);
                listRewardParticle[1].transform.localPosition = Vector3.zero;
                listRewardParticle[2].transform.localPosition = new Vector3(150, 0, 0);
            }
            else if (cnt == 0)
            {
                PopupManager.Instance.HideClaimPopup();
            }
        }
        else
        {
            Debug.Log("promotions == null");
            PopupManager.Instance.HideClaimPopup();
        }
    }

    public void GetRewardConfirm()
    {
        if (string.IsNullOrEmpty(why))
        {
            why = FirebaseLogSpaceWar.PvP_why;
        }
        if (clickConfirm)
        {
            return;
        }
        if (!clickConfirm)
        {
            clickConfirm = true;
        }
        if (rewardFromPVP)
        {
            new CSPvPSessionRewardedClaim().Send();
            return;
        }
        if (rewardFromPVP2v2)
        {
            new CSCoopPvPSeasonRewardedClaim().Send();
            return;
        }
        if (promotions != null)
        {
            bool oldInGame = GameContext.isInGameCampaign;
            GameContext.isInGameCampaign = true;
            Debug.Log("promotions ! null");
            int cnt = 0;
            foreach (var item in promotions)
            {
                if (cnt < 3)
                {
                    Debug.Log("Popup claim ----- key : " + item.key + " - value : " + item.value);
                    listTweenPosition[cnt].enabled = true;
                    listTweenAlpha[cnt].enabled = true;
                    background.SetActive(false);
                    title.SetActive(false);
                    switch (item.key)
                    {
                        case FalconMail.ITEM_GOLD:
                            if (PanelCoinGem.Instance != null)
                            {
                                PanelCoinGem.Instance.AddCoinTop(item.value, why);
                            }
                            break;
                        case FalconMail.ITEM_GEM:
                            if (PanelCoinGem.Instance != null)
                            {
                                PanelCoinGem.Instance.AddGemTop(item.value, why);
                            }
                            break;
                        case FalconMail.ITEM_ENERGY:
                            if (PanelCoinGem.Instance != null)
                            {
                                PanelCoinGem.Instance.AddEnergyTop(item.value, why);
                            }
                            break;
                        case FalconMail.ITEM_POWER_UP:
                            CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + item.value);
                            break;
                        case FalconMail.ITEM_ACTIVESKILL:
                            CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + item.value);
                            break;
                        case FalconMail.ITEM_LIFE:
                            CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + item.value);
                            break;
                        case FalconMail.ITEM_PLANE:
                            CacheGame.SetSpaceShipIsUnlocked(item.value + 1, 1);
                            CacheGame.SetSpaceShipUserSelected(item.value + 1);
                            MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI);
                            break;
                        case FalconMail.ITEM_WINGMAN:
                            CacheGame.SetWingManIsUnlocked(item.value + 1, 1);
                            CacheGame.SetWingManLeftUserSelected(item.value + 1);
                            MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI);
                            break;
                        case FalconMail.ITEM_WING:
                            CacheGame.SetWingIsUnlocked(item.value + 1, 1);
                            MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI);
                            break;
                        //case FalconMail.ITEM_SECONDARY_WEAPON:
                        //    CacheGame.SetSecondaryWeaponIsUnlocked(item.value, 1);
                        //    CacheGame.SetSecondaryWeaponSelected(item.value);
                        //    MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI);
                        //    break;
                        case FalconMail.ITEM_CARD_PLANE_GENERAL:
                            MinhCacheGame.SetSpaceShipGeneralCards(MinhCacheGame.GetSpaceShipGeneralCards() + item.value, why, "");
                            Debug.Log("ITEM_CARD_PLANE_GENERAL : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_1:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.BataFD, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD) + item.value, why, "", AircraftTypeEnum.BataFD.ToString());
                            Debug.Log("ITEM_CARD_PLANE_1 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_2:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.SkyWraith, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith) + item.value, why, "", AircraftTypeEnum.SkyWraith.ToString());
                            Debug.Log("ITEM_CARD_PLANE_2 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_3:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.FuryOfAres, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres) + item.value, why, "", AircraftTypeEnum.FuryOfAres.ToString());
                            Debug.Log("ITEM_CARD_PLANE_3 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_4:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.MacBird, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird) + item.value, why, "", AircraftTypeEnum.MacBird.ToString());
                            Debug.Log("ITEM_CARD_PLANE_4 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_5:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.TwilightX, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX) + item.value, why, "", AircraftTypeEnum.TwilightX.ToString());
                            Debug.Log("ITEM_CARD_PLANE_5 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_6:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + item.value, why, "", AircraftTypeEnum.StarBomb.ToString());
                            Debug.Log("ITEM_CARD_PLANE_6 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_7:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.IceShard, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.IceShard) + item.value, why, "", AircraftTypeEnum.IceShard.ToString());
                            Debug.Log("ITEM_CARD_PLANE_7 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_8:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.ThunderBolt, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.ThunderBolt) + item.value, why, "", AircraftTypeEnum.ThunderBolt.ToString());
                            Debug.Log("ITEM_CARD_PLANE_8 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_GENERAL:
                            MinhCacheGame.SetWingmanGeneralCards(MinhCacheGame.GetWingmanGeneralCards() + item.value, why, "");
                            Debug.Log("ITEM_CARD_WINGMAN_GENERAL : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_1:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.GatlingGun, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.GatlingGun) + item.value, why, "", WingmanTypeEnum.GatlingGun.ToString());
                            Debug.Log("ITEM_CARD_WINGMAN_1 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_2:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.AutoGatlingGun, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.AutoGatlingGun) + item.value, why, "", WingmanTypeEnum.AutoGatlingGun.ToString());
                            Debug.Log("ITEM_CARD_WINGMAN_2 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_3:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.Lazer, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Lazer) + item.value, why, "", WingmanTypeEnum.Lazer.ToString());
                            Debug.Log("ITEM_CARD_WINGMAN_3 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_4:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.DoubleGalting, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.DoubleGalting) + item.value, why, "", WingmanTypeEnum.DoubleGalting.ToString());
                            Debug.Log("ITEM_CARD_WINGMAN_4 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_5:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.HomingMissile, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile) + item.value, why, "", WingmanTypeEnum.HomingMissile.ToString());
                            Debug.Log("ITEM_CARD_WINGMAN_5 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_6:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.Splasher, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Splasher) + item.value, why, "", WingmanTypeEnum.Splasher.ToString());
                            Debug.Log("ITEM_CARD_WINGMAN_6 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_GENERAL:
                            MinhCacheGame.SetWingGeneralCards(MinhCacheGame.GetWingGeneralCards() + item.value, why, "");
                            Debug.Log("ITEM_CARD_WING_GENERAL : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_1:
                            MinhCacheGame.SetWingCards(WingTypeEnum.WingOfJustice, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfJustice) + item.value, why, "", WingTypeEnum.WingOfJustice.ToString());
                            Debug.Log("ITEM_CARD_WING_1 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_2:
                            MinhCacheGame.SetWingCards(WingTypeEnum.WingOfRedemption, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfRedemption) + item.value, why, "", WingTypeEnum.WingOfRedemption.ToString());
                            Debug.Log("ITEM_CARD_WING_2 : " + item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_3:
                            MinhCacheGame.SetWingCards(WingTypeEnum.WingOfResolution, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfResolution) + item.value, why, "", WingTypeEnum.WingOfResolution.ToString());
                            Debug.Log("ITEM_CARD_WING_3 : " + item.value);
                            break;
                        case FalconMail.ITEM_VIP_POINT:
                            CachePvp.myVipPoint += item.value;
                            Debug.Log("ITEM_VIP_POINT : " + item.value);
                            break;
                        case FalconMail.ITEM_LUCKY_WHEEL_TICKET:
                            MinhCacheGame.AddSpinTickets(item.value);
                            Debug.Log("ITEM_LUCKY_WHEEL_TICKET : " + item.value);
                            break;
                        case FalconMail.ITEM_PREMIUM_PACK:
                            MinhCacheGame.SetAlreadyPurchasePremiumPack(true);
                            MinhCacheGame.SetPremiumPackExpiredTime(DateTime.Now.AddDays(GameContext.EXPIRED_TIME_PREMIUM_PACK));
                            Debug.Log("ITEM_PREMIUM_PACK : " + item.value);
                            break;
                        case FalconMail.ITEM_INFINITY_PACK:
                            CacheGame.SetBuyInfinityPack(1);
                            Debug.Log("ITEM_INFINITY_PACK : " + item.value);
                            break;
                        default:
                            break;
                    }
                    MessageDispatcher.SendMessage(this, item.key, EventID.ON_REWARD_SUCCESSED, item.value, 0);
                    cnt++;
                }
            }
            GameContext.isInGameCampaign = oldInGame;
            PvpUtil.SendUpdatePlayer(why);
            Debug.Log("cnt : " + cnt);
            if (cnt == 0)
            {
                PopupManager.Instance.HideClaimPopup();
            }
            if (cnt != 0)
            {
                StartCoroutine(Wait());
            }
        }
        else
        {
            Debug.Log("promotions == null");
            PopupManager.Instance.HideClaimPopup();
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSecondsRealtime(3);
        PopupManager.Instance.HideClaimPopup();
    }
}
