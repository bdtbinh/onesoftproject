﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupRankReward2v2 : MonoBehaviour
{
    public UILabel lSeasonScore;
    public UILabel lTimeValue;

    public List<UILabel> listChestName; //0 -> 4
    public List<UILabel> listChestRequire; //0 -> 4
    public List<UILabel> listChestWin; //0 -> 4
    public List<UILabel> listChestLose; //0 -> 4
    public List<UILabel> listChestReward; //0 -> 14
    public List<UISprite> listChestSprite; //0 -> 4

    private void OnEnable()
    {
        lSeasonScore.text = I2.Loc.ScriptLocalization.season_score + ": [ffff00]" + GameContext.FormatNumber(CachePvp.PvP2vs2SeasonData.score) + "[-]";
        int day, hour;
        if (CachePvp.coopPvPTournamentInfo.info.state == PvP2vs2TournamentInfo.END)
        {
            day = (int)(CachePvp.coopPvPTournamentInfo.info.startTimeRemain / 60 / 60 / 24);
            hour = (int)(CachePvp.coopPvPTournamentInfo.info.startTimeRemain / 60 / 60) - (day * 24);
            lTimeValue.text = I2.Loc.ScriptLocalization.info_season_start.Replace("%{total_day}", "[44B52E]" + day).Replace("%{total_hour}", hour.ToString()) + "[-]";
        }
        else
        {
            day = (int)(CachePvp.coopPvPTournamentInfo.info.endTimeRemain / 60 / 60 / 24);
            hour = (int)(CachePvp.coopPvPTournamentInfo.info.endTimeRemain / 60 / 60) - (day * 24);
            lTimeValue.text = I2.Loc.ScriptLocalization.info_season_end.Replace("%{total_day}", "[44B52E]" + day).Replace("%{total_hour}", hour.ToString()) + "[-]";
        }

        for (int i = 0; i < listChestName.Count; i++)
        {
            if (CachePvp.coopPvPTournamentInfo.info.rewards.Count > i)
            {
                listChestName[i].text = CachePvp.MedalRank2v2Title(CachePvp.coopPvPTournamentInfo.info.rewards[i].name);
                listChestSprite[i].spriteName = CachePvp.GetSpriteNameChest(CachePvp.coopPvPTournamentInfo.info.rewards[i].name);
                listChestRequire[i].text = I2.Loc.ScriptLocalization.label_require + " [ffff00]" + CachePvp.coopPvPTournamentInfo.info.rewards[i].scoreReward.ToString() + "[-]";
                listChestWin[i].text = I2.Loc.ScriptLocalization.win + ": [ffff00]" + CachePvp.coopPvPTournamentInfo.info.rewards[i].scoreWin.ToString() + "[-]";
                listChestLose[i].text = I2.Loc.ScriptLocalization.lose + ": [ffff00]" + CachePvp.coopPvPTournamentInfo.info.rewards[i].scoreLose.ToString() + "[-]";

                listChestReward[i * 3].text = GameContext.FormatNumber(CachePvp.coopPvPTournamentInfo.info.rewards[i].gold);
                listChestReward[i * 3 + 1].text = GameContext.FormatNumber(CachePvp.coopPvPTournamentInfo.info.rewards[i].gem);
                listChestReward[i * 3 + 2].text = GameContext.FormatNumber(CachePvp.coopPvPTournamentInfo.info.rewards[i].wing);
            }
        }
    }

    public void bClose()
    {
        PopupManager.Instance.HideRankRewardPopup2v2();
    }
}
