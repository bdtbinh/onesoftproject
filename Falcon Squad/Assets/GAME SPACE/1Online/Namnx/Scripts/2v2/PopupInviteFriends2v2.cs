﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;

public class PopupInviteFriends2v2 : MonoBehaviour
{
    public GameObject gFriends;
    public GameObject gClan;

    public UISprite sBtnFriends;
    public UILabel lBtnFriends;
    public UISprite sBtnClan;
    public UILabel lBtnClan;

    public Transform listWrap;
    public GameObject fxloading;
    public GameObject lNoFriends;
    public GameObject lNoClan;

    private const string TAB_ACTIVE = "2vs2_list_tab_1";
    private const string TAB_DEACTIVE = "2vs2_list_tab_0";

    private void OnEnable()
    {
        CachePvp.Opponent = null;
        for (int i = 0; i < listWrap.childCount; i++)
        {
            listWrap.GetChild(i).gameObject.SetActive(false);
        }
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadedListFriends.ToString(), OnLoadedListFriends, true);
        MessageDispatcher.AddListener(EventName.Clan.ClanMember.ToString(), OnClanMember, true);
        sBtnFriends.spriteName = TAB_DEACTIVE;
        sBtnClan.spriteName = TAB_DEACTIVE;
        bFriends();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadedListFriends.ToString(), OnLoadedListFriends, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ClanMember.ToString(), OnClanMember, true);
    }

    private void OnClanMember(IMessage rMessage)
    {
        List<Player> list = rMessage.Data as List<Player>;
        fxloading.SetActive(false);
        if (list.Count == 0)
        {
            lNoFriends.SetActive(true);
        }
        else
        {
            lNoFriends.SetActive(false);
        }
        //MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedClanInfo.ToString(), rMessage.Data as List<Player>, 0);
    }

    private void OnLoadedListFriends(IMessage rMessage)
    {
        SCMyFriends mf = rMessage.Data as SCMyFriends;
        if (mf.status == SCMyFriends.SUCCESS)
        {
            fxloading.SetActive(false);
            if (mf.total == 0)
            {
                lNoFriends.SetActive(true);
            }
            else
            {
                lNoFriends.SetActive(false);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(mf.message, false, 1.5f);
        }
    }

    public void bSend()
    {
        if (CachePvp.Opponent != null)
        {
            new CSCoopPvPInvite(CachePvp.Opponent.code, CacheGame.GetSpaceShipUserSelected(), MinhCacheGame.GetBettingAmountPvP2v2()).Send();
        }
    }

    public void bBack()
    {
        PopupManager.Instance.HideInviteFriends2v2Popup();
    }

    public void bFriends()
    {
        if (sBtnFriends.spriteName.Equals(TAB_DEACTIVE))
        {
            for (int i = 0; i < gFriends.transform.GetChild(0).childCount; i++)
            {
                gFriends.transform.GetChild(0).GetChild(i).GetChild(1).gameObject.SetActive(false);
            }
            CachePvp.Opponent = null;
            lNoClan.SetActive(false);
            lNoFriends.SetActive(false);
            fxloading.SetActive(false);
            gFriends.SetActive(true);
            gClan.SetActive(false);
            sBtnFriends.spriteName = TAB_ACTIVE;
            lBtnFriends.color = Color.white;
            lBtnFriends.fontSize = 30;
            sBtnClan.spriteName = TAB_DEACTIVE;
            lBtnClan.color = Color.grey;
            lBtnClan.fontSize = 20;

            if (!HasChild(gFriends.transform.GetChild(0)))
            {
                fxloading.SetActive(true);
                new CSMyFriends().Send();
            }
        }
    }

    public void bClan()
    {
        if (sBtnClan.spriteName.Equals(TAB_DEACTIVE))
        {
            for (int i = 0; i < gClan.transform.GetChild(0).childCount; i++)
            {
                gClan.transform.GetChild(0).GetChild(i).GetChild(1).gameObject.SetActive(false);
            }
            CachePvp.Opponent = null;
            lNoFriends.SetActive(false);
            lNoClan.SetActive(false);
            fxloading.SetActive(false);
            gFriends.SetActive(false);
            gClan.SetActive(true);
            sBtnClan.spriteName = TAB_ACTIVE;
            lBtnClan.color = Color.white;
            lBtnClan.fontSize = 30;
            sBtnFriends.spriteName = TAB_DEACTIVE;
            lBtnFriends.color = Color.grey;
            lBtnFriends.fontSize = 20;
            if (CachePvp.TypeMemberClan == CachePvp.HAS_NO_CLAN)
            {
                lNoClan.SetActive(true);
            }
            else
            {
                if (!HasChild(gClan.transform.GetChild(0)))
                {
                    fxloading.SetActive(true);
                    new CSClanMember(CachePvp.PlayerClan.id).Send();
                }
            }
        }
    }

    bool HasChild(Transform t)
    {
        if (t.childCount == 0)
        {
            return false;
        }
        for (int i = 0; i < t.childCount; i++)
        {
            if (t.GetChild(i).gameObject.activeInHierarchy)
            {
                return true;
            }
        }
        return false;
    }
}
