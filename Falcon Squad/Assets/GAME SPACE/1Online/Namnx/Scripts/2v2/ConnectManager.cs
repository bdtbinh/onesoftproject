﻿using SkyGameKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class ConnectManager : MonoBehaviour
{
    public static Dictionary<int, BaseEnemy> dictionaryEnemy = new Dictionary<int, BaseEnemy>();
    public static Dictionary<int, int> dictionaryCurrentHPEnemy = new Dictionary<int, int>();
    public static Dictionary<int, int> hashSetIdDieEnemey = new Dictionary<int, int>();
    public UdpClient client;

    public string hostIp;
    public int hostPort;
    private IPAddress serverIp;
    private IPEndPoint hostEndPoint;

    private IPEndPoint localIpPort;
    private Thread thread;

    private static ConnectManager _instance;

    public static ConnectManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ConnectManager>();
                if (_instance == null)
                {
                    GameObject obj = new GameObject();
                    _instance = obj.AddComponent<ConnectManager>();
                }
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (this != _instance)
            {
                Destroy(gameObject);
            }
        }
        destroy = false;
        ConnectedToServer();
    }

    bool destroy = false;

    private void OnDestroy()
    {
        if (thread != null)
        {
            destroy = true;
            thread.Abort();
        }
    }

    public void ConnectedToServer()
    {
        try
        {
            serverIp = IPAddress.Parse(hostIp);
            hostEndPoint = new IPEndPoint(serverIp, hostPort);

            client = new UdpClient();
            client.Connect(hostEndPoint);
            client.Client.Blocking = false;
            client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 2000);

            //lấy thông tin local, ( nhớ gửi cho server qua bản tin cs - gửi address, port property của localIpPort )
            localIpPort = new IPEndPoint(IPAddress.Any, ((IPEndPoint)client.Client.LocalEndPoint).Port);
            //Debug.Log(localIpPort.Address + " - " + localIpPort.Port);
            thread = new Thread(new ThreadStart(ReceiveMessage));
            thread.IsBackground = true;
            thread.Start();
        }
        catch (Exception)
        {

        }
    }

    public void SendData(int type, Vector3 vector3, int id = 0, int score = 0, int hp = 0)
    {
        switch (type)
        {
            case CachePvp.PLAYER_MOVE:
                SendPosition(vector3);
                break;
            case CachePvp.PLAYER_HP:
                PlayerDie(id);
                break;
            case CachePvp.ENEMY_DIE:
                EnemyDie(id, score, hp);
                break;
            case CachePvp.ACTIVE_SKILL:
                ActiveSkill(id);
                break;
            case CachePvp.PLAYER_BULLET:
                AddPowerUp(id);
                break;
            case CachePvp.ENEMY_HP:
                EnemyHp(id, score, hp);
                break;
            default:
                break;
        }
    }

    private void AddPowerUp(int amount)
    {
        MemoryStream memory = new MemoryStream();
        byte[] dgram;
        using (BinaryWriter writer = new BinaryWriter(memory))
        {
            writer.Write(EndianUtilities.Swap(CachePvp.PLAYER_BULLET));
            if (CachePvp.MyCode == 0)
            {
                CachePvp.MyCode = int.Parse(CachePvp.Code);
            }
            writer.Write(EndianUtilities.Swap(CachePvp.MyCode));
            writer.Write(EndianUtilities.Swap(amount));
            dgram = memory.ToArray();
        }
        client.Send(dgram, dgram.Length);
    }

    void ActiveSkill(int type)
    {
        MemoryStream memory = new MemoryStream();
        byte[] dgram;
        using (BinaryWriter writer = new BinaryWriter(memory))
        {
            writer.Write(EndianUtilities.Swap(CachePvp.ACTIVE_SKILL));
            if (CachePvp.MyCode == 0)
            {
                CachePvp.MyCode = int.Parse(CachePvp.Code);
            }
            writer.Write(EndianUtilities.Swap(CachePvp.MyCode));
            writer.Write(EndianUtilities.Swap(type));
            dgram = memory.ToArray();
        }
        client.Send(dgram, dgram.Length);
    }

    private void EnemyDie(int id, int score, int hp)
    {
        MemoryStream memory = new MemoryStream();
        byte[] dgram;
        using (BinaryWriter writer = new BinaryWriter(memory))
        {
            writer.Write(EndianUtilities.Swap(CachePvp.ENEMY_DIE));
            if (CachePvp.MyCode == 0)
            {
                CachePvp.MyCode = int.Parse(CachePvp.Code);
            }
            writer.Write(EndianUtilities.Swap(CachePvp.MyCode));

            writer.Write(EndianUtilities.Swap(id));
            writer.Write(EndianUtilities.Swap(score));
            writer.Write(EndianUtilities.Swap(hp));
            dgram = memory.ToArray();
        }
        client.Send(dgram, dgram.Length);
    }

    private void EnemyHp(int id, int score, int hp)
    {
        MemoryStream memory = new MemoryStream();
        byte[] dgram;
        using (BinaryWriter writer = new BinaryWriter(memory))
        {
            writer.Write(EndianUtilities.Swap(CachePvp.ENEMY_HP));
            if (CachePvp.MyCode == 0)
            {
                CachePvp.MyCode = int.Parse(CachePvp.Code);
            }
            writer.Write(EndianUtilities.Swap(CachePvp.MyCode));

            writer.Write(EndianUtilities.Swap(id));
            writer.Write(EndianUtilities.Swap(score));
            writer.Write(EndianUtilities.Swap(hp));
            dgram = memory.ToArray();
        }
        client.Send(dgram, dgram.Length);
    }

    public void HandShake()
    {
        try
        {
            MemoryStream memory = new MemoryStream();
            byte[] dgram;
            using (BinaryWriter writer = new BinaryWriter(memory))
            {
                writer.Write(EndianUtilities.Swap(CachePvp.HANDSHAKE));
                if (CachePvp.MyCode == 0)
                {
                    CachePvp.MyCode = int.Parse(CachePvp.Code);
                }
                writer.Write(EndianUtilities.Swap(CachePvp.MyCode));
                dgram = memory.ToArray();
            }
            client.Send(dgram, dgram.Length);
        }
        catch (Exception)
        {
            ConnectedToServer();
        }
    }

    void SendPosition(Vector3 position)
    {
        MemoryStream memory = new MemoryStream();
        byte[] dgram;
        using (BinaryWriter writer = new BinaryWriter(memory))
        {
            writer.Write(EndianUtilities.Swap(CachePvp.PLAYER_MOVE));
            if (CachePvp.MyCode == 0)
            {
                CachePvp.MyCode = int.Parse(CachePvp.Code);
            }
            writer.Write(EndianUtilities.Swap(CachePvp.MyCode));

            writer.Write(EndianUtilities.Swap(position.x));
            writer.Write(EndianUtilities.Swap(position.y));
            dgram = memory.ToArray();
        }
        client.Send(dgram, dgram.Length);
    }

    void PlayerDie(int currentHP)
    {
        MemoryStream memory = new MemoryStream();
        byte[] dgram;
        using (BinaryWriter writer = new BinaryWriter(memory))
        {
            writer.Write(EndianUtilities.Swap(CachePvp.PLAYER_HP));
            if (CachePvp.MyCode == 0)
            {
                CachePvp.MyCode = int.Parse(CachePvp.Code);
            }
            writer.Write(EndianUtilities.Swap(CachePvp.MyCode));
            writer.Write(EndianUtilities.Swap(currentHP));
            dgram = memory.ToArray();
        }
        client.Send(dgram, dgram.Length);
    }

    private void ReceiveMessage()
    {
        while (!destroy)
        {
            try
            {
                if (client.Available > 0)
                {
                    byte[] content = client.Receive(ref localIpPort);
                    MemoryStream memory = new MemoryStream(content);
                    using (BinaryReader reader = new BinaryReader(memory))
                    {
                        int command = EndianUtilities.Swap(reader.ReadInt32());
                        int code = EndianUtilities.Swap(reader.ReadInt32());
                        switch (command)
                        {
                            case CachePvp.PLAYER_MOVE:
                                if (code == CachePvp.MyTeammateCode)
                                {
                                    float x = EndianUtilities.Swap(reader.ReadSingle());
                                    float y = EndianUtilities.Swap(reader.ReadSingle());
                                    if (AllPlayerManager.Instance != null)
                                    {
                                        AllPlayerManager.Instance.playerCoop.playerControllerScript.PlayerCoOpMove(x, y);
                                    }
                                }
                                break;
                            case CachePvp.PLAYER_HP:
                                int currentHP = EndianUtilities.Swap(reader.ReadInt32());
                                InGamePvP.Instance.PlayerDie(code.ToString(), currentHP);
                                break;
                            case CachePvp.ENEMY_DIE:
                                if (code == CachePvp.MyTeammateCode)
                                {
                                    int id = EndianUtilities.Swap(reader.ReadInt32());
                                    int score = EndianUtilities.Swap(reader.ReadInt32());
                                    int hp = EndianUtilities.Swap(reader.ReadInt32());
                                    if (!hashSetIdDieEnemey.ContainsKey(id))
                                    {
                                        hashSetIdDieEnemey.Add(id, id);
                                    }
                                    if (dictionaryEnemy.ContainsKey(id))
                                    {
                                        //BaseEnemy.dictionaryEnemy[id].enemyCollisionBase.TakeDamage(BaseEnemy.dictionaryEnemy[id].currentHP);
                                        dictionaryEnemy[id].SetDie();
                                    }
                                }
                                break;
                            case CachePvp.ENEMY_HP:
                                if (code == CachePvp.MyTeammateCode)
                                {
                                    int id = EndianUtilities.Swap(reader.ReadInt32());
                                    int score = EndianUtilities.Swap(reader.ReadInt32());
                                    int hp = EndianUtilities.Swap(reader.ReadInt32());
                                    if (dictionaryCurrentHPEnemy.ContainsKey(id))
                                    {
                                        dictionaryCurrentHPEnemy[id] = score;
                                    }
                                    else
                                    {
                                        dictionaryCurrentHPEnemy.Add(id, score);
                                    }
                                    if (dictionaryEnemy.ContainsKey(id))
                                    {
                                        if (dictionaryCurrentHPEnemy[id] < dictionaryEnemy[id].currentHP)
                                        {
                                            dictionaryEnemy[id].currentHP = dictionaryCurrentHPEnemy[id];
                                        }
                                        dictionaryEnemy[id].SetHPFromServer(hp);
                                    }
                                }
                                break;
                            case CachePvp.ACTIVE_SKILL:
                                if (code == CachePvp.MyTeammateCode)
                                {
                                    int type = EndianUtilities.Swap(reader.ReadInt32());
                                    InGamePvP.Instance.ActiveSkill(type);
                                }
                                break;
                            case CachePvp.PLAYER_BULLET:
                                if (code == CachePvp.MyTeammateCode)
                                {
                                    int amount = EndianUtilities.Swap(reader.ReadInt32());
                                    InGamePvP.Instance.AddPowerUp(amount);
                                }
                                break;
                            case CachePvp.SCORE:
                                int scoreTeam1 = EndianUtilities.Swap(reader.ReadInt32());
                                int scoreTeam2 = EndianUtilities.Swap(reader.ReadInt32());
                                InGamePvP.Instance.UpdateScore(scoreTeam1, scoreTeam2);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
        }
    }
}