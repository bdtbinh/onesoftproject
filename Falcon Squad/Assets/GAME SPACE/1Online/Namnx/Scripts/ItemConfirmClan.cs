﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemConfirmClan : MonoBehaviour
{
    public UI2DSprite sAvatar;
    public UILabel lName;
    public UILabel lId;
    public UISprite sRank;
    public UISprite sFlag;
    public UILabel lRank;

    public UILabel lScore;
    public UILabel lStatus; //online ; offline
    public UISprite sStatus;
    public string facebookID;

    public UISprite sVipIcon;
    void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);

        if (CachePvp.memberPlayer == null || CachePvp.GetVipFromVipPoint(CachePvp.memberPlayer.vip) == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (CachePvp.GetVipFromVipPoint(CachePvp.memberPlayer.vip) - 1);
        }

        lId.text = I2.Loc.ScriptLocalization.elo_key + ": " + CachePvp.memberPlayer.code;
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        sFlag.spriteName = CachePvp.memberPlayer.country.ToLower();

        int rank = CachePvp.dictionaryRank[CachePvp.memberPlayer.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lRank.text = rankTitle + " " + CachePvp.ConvertChina(CachePvp.memberPlayer.playerLevel.name);
        sRank.spriteName = "PVP_rank_" + rank;
        lScore.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.memberPlayer.playerLevel.elo);
        lStatus.text = CachePvp.memberPlayer.offline ? I2.Loc.ScriptLocalization.offline : I2.Loc.ScriptLocalization.online;
        sStatus.spriteName = CachePvp.memberPlayer.offline ? "status_offline" : "status_online";

        if (CachePvp.memberPlayer != null && !string.IsNullOrEmpty(CachePvp.memberPlayer.code))
        {
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(CachePvp.memberPlayer.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.memberPlayer.appCenterId))
                    {
                        showImageFB = false;
                        facebookID = CachePvp.memberPlayer.appCenterId;
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.memberPlayer.appCenterId];
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.memberPlayer.appCenterId);
                        if (t != null)
                        {
                            showImageFB = false;
                            facebookID = CachePvp.memberPlayer.appCenterId;
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.memberPlayer.appCenterId, t);
                        }
                    }
                }
            }
            if (showImageFB)
            {
                facebookID = CachePvp.memberPlayer.facebookId;
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(CachePvp.memberPlayer.facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.memberPlayer.facebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.memberPlayer.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + CachePvp.memberPlayer.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.memberPlayer.facebookId);
                    }
                }
            }
            lName.text = string.IsNullOrEmpty(CachePvp.memberPlayer.name) ? CachePvp.memberPlayer.code : CachePvp.memberPlayer.name;
        }
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    public void OnClick()
    {
        PopupManager.Instance.ShowProfilePopup(CachePvp.memberPlayer);
    }
}
