﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class CacheAvatarManager : MonoBehaviour
{

    public Dictionary<string, Sprite> dictImageDownloaded = new Dictionary<string, Sprite>();
    public Sprite defaultAvatar;

    protected static CacheAvatarManager _instance;
    protected bool _enabled;

    public static CacheAvatarManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<CacheAvatarManager>();
                if (_instance == null)
                {
                    GameObject obj = new GameObject();
                    //obj.hideFlags = HideFlags.HideAndDontSave;
                    _instance = obj.AddComponent<CacheAvatarManager>();
                }
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (!Application.isPlaying)
        {
            return;
        }

        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this as CacheAvatarManager;
            DontDestroyOnLoad(transform.gameObject);
            _enabled = true;
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public void DownloadImageFormUrl(string imageLink, string id, Texture2D texture = null)
    {
        if (texture != null)
        {
            Sprite sr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            if (!dictImageDownloaded.ContainsKey(id))
            {
                dictImageDownloaded.Add(id, sr);
            }
            MessageDispatcher.SendMessage(EventName.LoadProfile.RefreshAvatar.ToString());
            return;
        }
        StartCoroutine(DownloadImage(imageLink, id));
    }

    IEnumerator DownloadImage(string imageLink, string id)
    {
        WWW www = new WWW(imageLink);
        yield return www;
        var texture = www.texture;
        if (texture != null)
        {
            Sprite sr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            if (!dictImageDownloaded.ContainsKey(id))
            {
                dictImageDownloaded.Add(id, sr);
            }
            MessageDispatcher.SendMessage(EventName.LoadProfile.RefreshAvatar.ToString());
        }
    }

    public Texture2D GetTextureByGamecenterId(string id)
    {
        return null;
        if (Social.localUser.authenticated)
        {
            if (Social.localUser.id.Equals(id))
            {
                return Social.localUser.image;
            }
            for (int i = 0; i < Social.localUser.friends.Length; i++)
            {
                if (Social.localUser.friends[i].id.Equals(id))
                {
                    return Social.localUser.friends[i].image;
                }
            }
        }
        return null;
    }
}
