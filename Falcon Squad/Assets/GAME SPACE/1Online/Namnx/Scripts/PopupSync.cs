﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using Mp.Pvp;

public class PopupSync : MonoBehaviour
{
    DataSync data;
    public GameObject popupSync;
    public GameObject popupBottomSync;
    public GameObject popupSyncConfirm;
    public GameObject popupBottomConfirm;
    int typeSync;
    public UILabel lTextStatusAccount;

    public UISprite sVipIcon;
    public UI2DSprite sAvatar;
    public UISprite sFlag;
    public UILabel lId;
    public UILabel lName;
    public UILabel lLevel;
    public UILabel lStars;
    public UISprite sRank;
    public UILabel lRank;

    public UILabel lGold;
    public UILabel lGem;
    public UILabel lElo;
    public AircraftAnimations aa;

    public UISprite sVipIconOld;
    public UI2DSprite sAvatarOld;
    public UISprite sFlagOld;
    public UILabel lNameOld;
    public UILabel lLevelOld;
    public UILabel lStarsOld;
    public UISprite sRankOld;
    public UILabel lRankOld;
    public UILabel lIdOld;

    public UILabel lGoldOld;
    public UILabel lGemOld;
    public UILabel lEloOld;
    public AircraftAnimations aaOld;

    public UISprite sVipIconConfirm;
    public UI2DSprite sAvatarConfirm;
    public UISprite sFlagConfirm;
    public UILabel lIdConfirm;
    public UILabel lNameConfirm;
    public UILabel lLevelConfirm;
    public UILabel lStarsConfirm;
    public UISprite sRankConfirm;
    public UILabel lRankConfirm;

    public UILabel lGoldConfirm;
    public UILabel lGemConfirm;
    public UILabel lEloConfirm;
    public AircraftAnimations aaConfirm;

    private void OnEnable()
    {
        facebookIdOld = "";
        facebookIdConfirm = "";
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    private void OnDisable()
    {
        facebookIdOld = "";
        facebookIdConfirm = "";
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (GameContext.IS_CHINA_VERSION)
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.GamecenterId))
            {
                sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.GamecenterId];
            }
        }
        else
        {
            if (CachePvp.FacebookId != null)
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
                }
            }
        }
        if (!string.IsNullOrEmpty(facebookIdOld))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdOld))
            {
                sAvatarOld.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdOld];
            }
        }
        if (!string.IsNullOrEmpty(facebookIdConfirm))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdConfirm))
            {
                sAvatarConfirm.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdConfirm];
            }
        }
    }

    public void SetData(Player player, string token)
    {
        data = new DataSync();
        data.player = player;
        data.token = token;
        LoadUICurrent(CachePvp.MyInfo);
        LoadUIOld(player);
        popupSync.SetActive(true);
        popupBottomSync.SetActive(true);
        popupSyncConfirm.SetActive(false);
        popupBottomConfirm.SetActive(false);
    }

    string facebookIdOld = "";
    string facebookIdConfirm = "";

    void LoadUIOld(Player player)
    {
        Debug.Log("UI Old vip point : " + player.vip);
        int vip = CachePvp.GetVipFromVipPoint(player.vip);
        Debug.Log("player.vip : " + vip);
        if (vip == 0)
        {
            sVipIconOld.gameObject.SetActive(false);
        }
        else
        {
            sVipIconOld.gameObject.SetActive(true);
            sVipIconOld.spriteName = "Icon__VIP_" + (vip - 1);
        }
        if (!string.IsNullOrEmpty(player.country))
        {
            sFlagOld.spriteName = player.country.ToLower();
        }
        else
        {
            sFlagOld.spriteName = "unknown_flag";
        }

        if (!string.IsNullOrEmpty(player.name))
        {
            lNameOld.text = player.name;
        }
        else
        {
            lNameOld.text = "????????????????";
        }

        if (!string.IsNullOrEmpty(player.code))
        {
            lIdOld.text = player.code;
        }
        else
        {
            lIdOld.text = "????????????????";
        }
        PlayerDataUtil.PlayerProfileData profile = PlayerDataUtil.StringToObject(player.data);
        int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);
        int[] starExtra = PvpUtil.StarExtra(profile);

        int level1 = Mathf.Max(0, Mathf.Min(levelValue[0] - 1, GameContext.TOTAL_LEVEL));
        int level2 = Mathf.Max(0, Mathf.Min(levelValue[2] - 1, GameContext.TOTAL_LEVEL));
        int level3 = Mathf.Max(0, Mathf.Min(levelValue[4] - 1, GameContext.TOTAL_LEVEL));

        lLevelOld.text = I2.Loc.ScriptLocalization.level + ": " + level1 + ", " + level2 + ", " + level3;
        lStarsOld.text = I2.Loc.ScriptLocalization.star + ": " + Mathf.Min(levelValue[1] + starExtra[0], PvpUtil.MaxStar(level1)) + ", " + Mathf.Min(levelValue[3] + starExtra[1], PvpUtil.MaxStar(level2)) + ", " + Mathf.Min(levelValue[5] + starExtra[2], PvpUtil.MaxStar(level3));

        int rank = CachePvp.dictionaryRank[player.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lRankOld.text = rankTitle + " " + CachePvp.ConvertChina(player.playerLevel.name);
        sRankOld.spriteName = "PVP_rank_" + rank;

        lEloOld.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(player.playerLevel.elo);

        AircraftTypeEnum aircraftType;
        if (profile.listRankPlane != null)
        {
            aircraftType = (AircraftTypeEnum)profile.selectedShip;
            aaOld.PlayAnimations(aircraftType, (Rank)profile.listRankPlane[profile.selectedShip - 1]);
        }
        else
        {
            aircraftType = (AircraftTypeEnum)1;
            aaOld.PlayAnimations(aircraftType, (Rank)AircraftSheet.Get((int)aircraftType).begin_rank);
        }

        lGoldOld.text = I2.Loc.ScriptLocalization.gold + ": " + player.gold;
        lGemOld.text = I2.Loc.ScriptLocalization.gem + ": " + player.diamond;
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(player.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                {
                    showImageFB = false;
                    facebookIdOld = player.appCenterId;
                    sAvatarOld.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        facebookIdOld = player.appCenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(player.facebookId))
            {
                facebookIdOld = player.facebookId;
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                {
                    sAvatarOld.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                }
            }
            else
            {
                sAvatarOld.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            }
        }
    }

    void LoadUICurrent(Player player)
    {
        Debug.Log("UI current vip point : " + player.vip);
        int vip = CachePvp.GetVipFromVipPoint(player.vip);
        Debug.Log("player.vip : " + vip);
        if (vip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (vip - 1);
        }
        Debug.Log("player.country : " + player.country);
        if (!string.IsNullOrEmpty(player.country))
        {
            sFlag.spriteName = player.country.ToLower();
        }
        else
        {
            sFlag.spriteName = "unknown_flag";
        }
        Debug.Log("player.name : " + player.name);
        if (!string.IsNullOrEmpty(player.name))
        {
            lName.text = player.name;
        }
        else
        {
            lName.text = "????????????????";
        }
        Debug.Log("player.code : " + player.code);
        if (!string.IsNullOrEmpty(player.code))
        {
            lId.text = player.code;
        }
        else
        {
            lId.text = "????????????????";
        }
        Debug.Log("player.levelValue : " + player.levelValue);
        PlayerDataUtil.PlayerProfileData profile = PlayerDataUtil.StringToObject(player.data);
        int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);
        int[] starExtra = PvpUtil.StarExtra(profile);

        int level1 = Mathf.Max(0, Mathf.Min(levelValue[0] - 1, GameContext.TOTAL_LEVEL));
        int level2 = Mathf.Max(0, Mathf.Min(levelValue[2] - 1, GameContext.TOTAL_LEVEL));
        int level3 = Mathf.Max(0, Mathf.Min(levelValue[4] - 1, GameContext.TOTAL_LEVEL));

        lLevel.text = I2.Loc.ScriptLocalization.level + ": " + level1 + ", " + level2 + ", " + level3;
        lStars.text = I2.Loc.ScriptLocalization.star + ": " + Mathf.Min(levelValue[1] + starExtra[0], PvpUtil.MaxStar(level1)) + ", " + Mathf.Min(levelValue[3] + starExtra[1], PvpUtil.MaxStar(level2)) + ", " + Mathf.Min(levelValue[5] + starExtra[2], PvpUtil.MaxStar(level3));

        Debug.Log("lLevel.text : " + lLevel.text);
        Debug.Log("lStars.text : " + lStars.text);
        lElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(player.playerLevel.elo);

        int rank = CachePvp.dictionaryRank[player.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lRank.text = rankTitle + " " + CachePvp.ConvertChina(player.playerLevel.name);
        sRank.spriteName = "PVP_rank_" + rank;
        Debug.Log("lRank.text : " + lRank.text);
        Debug.Log("rank : " + rank);

        AircraftTypeEnum aircraftType;
        if (profile.listRankPlane != null)
        {
            aircraftType = (AircraftTypeEnum)profile.selectedShip;
            aa.PlayAnimations(aircraftType, (Rank)profile.listRankPlane[profile.selectedShip - 1]);
        }
        else
        {
            aircraftType = (AircraftTypeEnum)1;
            aa.PlayAnimations(aircraftType, (Rank)AircraftSheet.Get((int)aircraftType).begin_rank);
        }
        lGold.text = I2.Loc.ScriptLocalization.gold + ": " + player.gold;
        lGem.text = I2.Loc.ScriptLocalization.gem + ": " + player.diamond;
        Debug.Log("lGold.text : " + lGold.text);
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(player.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                {
                    showImageFB = false;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(player.facebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                }
            }
            else
            {
                sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            }
        }
    }

    public void ButtonYes()
    {
        typeSync = 1;
        popupSync.SetActive(false);
        popupBottomSync.SetActive(false);
        popupSyncConfirm.SetActive(true);
        popupBottomConfirm.SetActive(true);
        SetDataConfirm(typeSync);
    }

    public void ButtonNo()
    {
        typeSync = 0;
        popupSync.SetActive(false);
        popupBottomSync.SetActive(false);
        popupSyncConfirm.SetActive(true);
        popupBottomConfirm.SetActive(true);
        SetDataConfirm(typeSync);
    }

    void SetDataConfirm(int typeSync)
    {
        Player player;
        if (typeSync == 0)
        {
            //use current data
            player = CachePvp.MyInfo;
            lTextStatusAccount.text = I2.Loc.ScriptLocalization.title_pvp_current_account;
        }
        else
        {
            //use old data
            player = data.player;
            lTextStatusAccount.text = I2.Loc.ScriptLocalization.title_pvp_old_account;
        }
        int vip = CachePvp.GetVipFromVipPoint(player.vip);
        if (vip == 0)
        {
            sVipIconConfirm.gameObject.SetActive(false);
        }
        else
        {
            sVipIconConfirm.gameObject.SetActive(true);
            sVipIconConfirm.spriteName = "Icon__VIP_" + (vip - 1);
        }
        if (!string.IsNullOrEmpty(player.country))
        {
            sFlagConfirm.spriteName = player.country.ToLower();
        }
        else
        {
            sFlagConfirm.spriteName = "unknown_flag";
        }

        if (!string.IsNullOrEmpty(player.name))
        {
            lNameConfirm.text = player.name;
        }
        else
        {
            lNameConfirm.text = "????????????????";
        }

        if (!string.IsNullOrEmpty(player.code))
        {
            lIdConfirm.text = player.code;
        }
        else
        {
            lIdConfirm.text = "????????????????";
        }
        PlayerDataUtil.PlayerProfileData profile = PlayerDataUtil.StringToObject(player.data);
        int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);
        int[] starExtra = PvpUtil.StarExtra(profile);

        int level1 = Mathf.Max(0, Mathf.Min(levelValue[0] - 1, GameContext.TOTAL_LEVEL));
        int level2 = Mathf.Max(0, Mathf.Min(levelValue[2] - 1, GameContext.TOTAL_LEVEL));
        int level3 = Mathf.Max(0, Mathf.Min(levelValue[4] - 1, GameContext.TOTAL_LEVEL));

        lLevelConfirm.text = I2.Loc.ScriptLocalization.level + ": " + level1 + ", " + level2 + ", " + level3;
        lStarsConfirm.text = I2.Loc.ScriptLocalization.star + ": " + Mathf.Min(levelValue[1] + starExtra[0], PvpUtil.MaxStar(level1)) + ", " + Mathf.Min(levelValue[3] + starExtra[1], PvpUtil.MaxStar(level2)) + ", " + Mathf.Min(levelValue[5] + starExtra[2], PvpUtil.MaxStar(level3));

        int rank = CachePvp.dictionaryRank[player.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lRankConfirm.text = rankTitle + " " + CachePvp.ConvertChina(player.playerLevel.name);
        sRankConfirm.spriteName = "PVP_rank_" + rank;

        AircraftTypeEnum aircraftType;
        if (profile.listRankPlane != null)
        {
            aircraftType = (AircraftTypeEnum)profile.selectedShip;
            aaConfirm.PlayAnimations(aircraftType, (Rank)profile.listRankPlane[profile.selectedShip - 1]);
        }
        else
        {
            aircraftType = (AircraftTypeEnum)1;
            aaConfirm.PlayAnimations(aircraftType, (Rank)AircraftSheet.Get((int)aircraftType).begin_rank);
        }
        lGoldConfirm.text = I2.Loc.ScriptLocalization.gold + ": " + GameContext.FormatNumber(player.gold);
        lGemConfirm.text = I2.Loc.ScriptLocalization.gem + ": " + player.diamond;
        lEloConfirm.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(player.playerLevel.elo);
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(player.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                {
                    showImageFB = false;
                    facebookIdConfirm = player.appCenterId;
                    sAvatarConfirm.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        facebookIdConfirm = player.appCenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(player.facebookId))
            {
                facebookIdConfirm = player.facebookId;
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                {
                    sAvatarConfirm.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                }
            }
            else
            {
                sAvatarConfirm.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            }
        }
    }

    public void ButtonYesConfirm()
    {
        if (typeSync == 0)
        {
            MessageDispatcher.SendMessage(gameObject, EventName.SyncData.SyncFromClientToServer.ToString(), this.data, 0);
        }
        else if (typeSync == 1)
        {
            MessageDispatcher.SendMessage(gameObject, EventName.SyncData.SyncFromServerToClient.ToString(), this.data, 0);
        }
        PopupManager.Instance.HideSyncPopup();
    }

    public void ButtonNoConfirm()
    {
        popupSync.SetActive(true);
        popupBottomSync.SetActive(true);
        popupSyncConfirm.SetActive(false);
        popupBottomConfirm.SetActive(false);
    }
}

public class DataSync
{
    public string token { get; set; }
    public Player player { get; set; }
}