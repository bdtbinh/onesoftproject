﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupRankReward : MonoBehaviour
{
    //public UILabel lRankValue;
    public UILabel lTimeValue;
    public UILabel lYourRank;

    private void OnEnable()
    {
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        //lRankValue.text = rankTitle + " " + CachePvp.PlayerLevel.name;

        lYourRank.text = I2.Loc.ScriptLocalization.msg_your_rank.Replace("%{rank_number}", "[EE9D44FF]" + rankTitle + " " + CachePvp.ConvertChina(CachePvp.PlayerLevel.name) + "[-]");
        int day = (int)(CachePvp.timeEndSession / 60 / 60 / 24);
        int hour = (int)(CachePvp.timeEndSession / 60 / 60) - (day * 24);
        lTimeValue.text = I2.Loc.ScriptLocalization.info_season_end.Replace("%{total_day}", "[44B52E]" + day).Replace("%{total_hour}", hour.ToString()) + "[-]";
    }

    public void bClose()
    {
        PopupManager.Instance.HideRankRewardPopup();
    }
}
