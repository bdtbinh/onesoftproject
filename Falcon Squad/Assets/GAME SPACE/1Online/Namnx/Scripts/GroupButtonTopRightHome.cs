﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupButtonTopRightHome : MonoBehaviour
{
    private List<ButtonTopRightHome> listButton = new List<ButtonTopRightHome>();
    private bool isOpen;
    public int space = 105;

    private void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            listButton.Add(transform.GetChild(i).GetComponent<ButtonTopRightHome>());
            listButton[i].transform.localPosition = Vector3.zero;
            listButton[i].gameObject.SetActive(true);
        }

        LeanTween.delayedCall(0.1f, () =>
        {
            int cnt = 0;
            for (int i = 0; i < listButton.Count; i++)
            {
                if (listButton[i].showFirst)
                {
                    listButton[i].gameObject.SetActive(true);
                    listButton[i].transform.localPosition = new Vector3(listButton[i].transform.localPosition.x, -space * ++cnt, listButton[i].transform.localPosition.z);
                }
                else
                {
                    listButton[i].gameObject.SetActive(false);
                }
            }
        }).setIgnoreTimeScale(true);
    }

    public void bClick()
    {
        if (isOpen)
        {
            isOpen = false;
            for (int i = 0; i < listButton.Count; i++)
            {
                MoveLocalBack(i);
            }
        }
        else
        {
            isOpen = true;
            int cnt = 0;
            for (int i = 0; i < listButton.Count; i++)
            {
                if (!listButton[i].canShow || (listButton[i].name.Equals("B_Community") && (CachePvp.sCClientConfig.turnOnTeam == 0 || GameContext.IS_CHINA_VERSION)) || (listButton[i].name.Equals("B_Glory")))
                {
                    continue;
                }
                if (listButton[i].showFirst)
                {
                    MoveLocal(i, cnt++);
                }
            }
            for (int i = 0; i < listButton.Count; i++)
            {
                if (!listButton[i].canShow || (listButton[i].name.Equals("B_Glory")) || (listButton[i].name.Equals("B_Community") && (CachePvp.sCClientConfig.turnOnTeam == 0 || GameContext.IS_CHINA_VERSION)))
                {
                    continue;
                }
                if (!listButton[i].showFirst)
                {
                    MoveLocal(i, cnt++);
                }
            }
        }
    }

    public void RePosition()
    {
        if (!isOpen)
        {
            int cnt = 0;
            for (int i = 0; i < listButton.Count; i++)
            {
                if (listButton[i].showFirst)
                {
                    listButton[i].gameObject.SetActive(true);
                    listButton[i].transform.localPosition = new Vector3(listButton[i].transform.localPosition.x, -space * ++cnt, listButton[i].transform.localPosition.z);
                }
            }
        }
    }

    void MoveLocalBack(int i)
    {
        LeanTween.cancel(listButton[i].gameObject);
        LeanTween.moveLocalY(listButton[i].gameObject, 0, 0.1f).setIgnoreTimeScale(true).setOnComplete(() =>
        {
            listButton[i].gameObject.SetActive(false);
        });
    }

    void MoveLocal(int i, int cnt)
    {
        listButton[i].gameObject.SetActive(true);
        LeanTween.cancel(listButton[i].gameObject);
        LeanTween.moveLocalY(listButton[i].gameObject, -space * (cnt + 1), 0.1f).setIgnoreTimeScale(true);
    }
}
