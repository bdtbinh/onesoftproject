﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class ElementSelectWingTournament : MonoBehaviour
{
    [SerializeField]
    private UISprite sIcon;

    [SerializeField]
    private UISprite sRank;

    [SerializeField]
    private UILabel lLevel;

    [SerializeField]
    private GameObject sSelected;

    public WingTypeEnum WingType { get; private set; }

    public void CreateItem(WingTypeEnum wingType)
    {
        this.WingType = wingType;
        Rank rank = Constant.MAX_RANK_APPLY;

        sSelected.SetActive(false);

        sIcon.spriteName = HangarValue.WingIconSpriteName(wingType, rank);
        Vector2 localScale = sIcon.transform.localScale;
        sIcon.MakePixelPerfect();
        sIcon.transform.localScale = localScale;

        sRank.spriteName = HangarValue.RankSpriteName[rank];
        sRank.MakePixelPerfect();

        int newMaxlevel = RankSheet.Get((int)rank).max_level;
        lLevel.text = newMaxlevel.ToString();
    }

    public void SetAsSelected()
    {
        sSelected.SetActive(true);
        MessageDispatcher.SendMessage(this, EventID.ON_SELECTED_CALLING_BACKUP, WingType, 0);
    }

    public void SetAsDeselected()
    {
        sSelected.SetActive(false);
    }

    public void OnClickAircraft()
    {
        if (sSelected.activeInHierarchy)
            return;
        SetAsSelected();
    }
}
