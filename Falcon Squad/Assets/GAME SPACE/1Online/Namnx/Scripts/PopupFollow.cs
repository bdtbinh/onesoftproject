﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using Mp.Pvp;
using System;

public class PopupFollow : MonoBehaviour
{

    public GameObject loading;
    public GameObject lNoFollow;
    public Transform scrollWrapper;

    private static PopupFollow _instance;
    public static PopupFollow Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    void OnEnable()
    {
        for (int i = 0; i < scrollWrapper.childCount; i++)
        {
            scrollWrapper.GetChild(i).gameObject.SetActive(false);
        }
        lNoFollow.SetActive(false);
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadFollow.ToString(), OnLoadFollow, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadFollow.ToString(), OnLoadFollow, true);
    }

    private void OnLoadFollow(IMessage rMessage)
    {
        SCMyFollow mr = rMessage.Data as SCMyFollow;
        if (mr.status == SCMyRequests.SUCCESS)
        {
            loading.SetActive(false);
            if (mr.players == null || mr.players.Count == 0)
            {
                lNoFollow.SetActive(true);
            }
            else
            {
                MessageDispatcher.SendMessage(gameObject, EventName.LoadProfile.LoadedFollow.ToString(), mr.players, 0);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(mr.message, false, 1.5f);
        }
    }

    public void ShowFollow()
    {
        loading.SetActive(true);
        new CSMyFollows().Send();
    }

    public void OnClickClosePopup()
    {
        PopupManager.Instance.HideFollowPopup();
    }
}
