﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;
using Mp.Pvp;
using UnityEngine.SceneManagement;

public class PopupNotify : MonoBehaviour
{
    string url = "";
    string urlImage = "";
    long id;
    int type;
    List<PromotionItem> promotions;
    public UILabel lMessage;
    public UI2DSprite sImage;
    public static Action actionSale;
    public GameObject urlNull;
    public GameObject urlNotNull;
    public List<GameObject> listRewardParticle;
    public List<UISprite> listRewardSprite;
    public List<UILabel> listRewardLabel;
    public List<TweenPosition> listTweenPosition;
    public List<TweenAlpha> listTweenAlpha;

    public GameObject background;
    public GameObject title;

    const int CLICK_OK = 1;
    const int CLICK_CANCEL = 0;

    bool clickYes;

    private void OnEnable()
    {
        clickYes = false;
        background.SetActive(true);
        title.SetActive(true);
    }

    public void ButtonYes()
    {
        if (!clickYes)
        {
            LeanTween.delayedCall(6, () =>
            {
                clickYes = false;
            });
            clickYes = true;
            if (!string.IsNullOrEmpty(url))
            {
                string[] arr;
                int x;
                int y;
                int caseSplit;
                switch (url.Length)
                {
                    case 1:
                        switch (url)
                        {
                            case "1":
                                if (actionSale != null)
                                {
                                    actionSale();
                                }
                                //GameContext.serverX2Purchase = true;
                                if (PopupShop.Instance != null)
                                {
                                    PanelCoinGem.Instance.ShowGlowButtonIapGold();
                                }
                                PopupManagerCuong.Instance.ShowShopPopup();
                                if (PopupShop.Instance != null)
                                {
                                    PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.IapGold);
                                }
                                break;
                            case "2": // Mở profile
                                PopupManager.Instance.ShowProfilePopup();
                                break;
                            case "3": // Mở upgrade
                                PopupManagerCuong.Instance.ShowShopPopup();
                                if (PopupShop.Instance != null)
                                {
                                    PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.Upgrade);
                                }
                                break;
                            case "5": //Mở ranking campain
                                PopupManager.Instance.ShowRanking();
                                break;
                            case "6": //Mở ranking 
                                PopupManager.Instance.ShowRankingPVP();
                                break;
                            case "7": //Mở gói VIP
                                if (CacheGame.GetPurchasedVIPPack() != 1)
                                {
                                    PopupManagerCuong.Instance.Show3PackPopup();
                                    Popup3Pack.Instance.ShowPackInPopup(TypePackShowInPopup3Pack.VipPack);
                                    PopupManagerCuong.Instance.ChangeDepth3PackPopup();
                                    IAPCallbackManager.Instance.isServerPush = true;
                                }
                                break;
                            case "8": //Mở gói Premium
                                PopupManagerCuong.Instance.Show3PackPopup();
                                Popup3Pack.Instance.ShowPackInPopup(TypePackShowInPopup3Pack.PremiumPack);
                                PopupManagerCuong.Instance.ChangeDepth3PackPopup();
                                IAPCallbackManager.Instance.isServerPush = true;
                                break;
                            case "9": //Mở gói Starter
                                if (CacheGame.GetPurchasedStarterPack() != 1)
                                {
                                    PopupManagerCuong.Instance.Show3PackPopup();
                                    Popup3Pack.Instance.ShowPackInPopup(TypePackShowInPopup3Pack.StarterPack);
                                    PopupManagerCuong.Instance.ChangeDepth3PackPopup();
                                    IAPCallbackManager.Instance.isServerPush = true;
                                }

                                break;
                            default:
                                break;
                        }
                        break;
                    case 2:
                        switch (url)
                        {
                            case "-1":
                                Debug.Log("khong lam gi ca");
                                break;
                            case "10": //Mở màn hình chọn chiến cơ
                                PopupManagerCuong.Instance.ShowSelectAircraftPopup(SelectDeviceHandler.OpenSelectFromType.FromHome, false);
                                PopupManagerCuong.Instance.ChangeDepthSelectAircraftPopup();
                                Debug.LogError("case10: " + url);
                                break;
                            case "11": //Mở màn hình chọn chiến cơ phụ
                                PopupManagerCuong.Instance.ShowSelectWingmanPopup(WingmanPosType.LeftWingman, SelectDeviceHandler.OpenSelectFromType.FromHome, false);
                                PopupManagerCuong.Instance.ChangeDepthSelectWingmanPopup();
                                Debug.LogError("case11: " + url);
                                break;
                            case "12": //Mở gói Royal_Greatoffer
                                PopupManagerCuong.Instance.ShowRoyaltyPopup();
                                break;
                            case "13": //Mở vòng quay may mắn
                                break;
                            case "14": //Mở Community
                                break;
                            case "15": //Mở Daily Task 
                                break;
                            case "16": //Mở Popup Register 
                                PopupManager.Instance.ShowRegisterPopup();
                                PopupManager.Instance.ChangeDepthRegisterPopup();
                                break;
                            case "17": //Mở màn hình PvP
                                SceneManager.LoadScene("PVPMain");
                                break;
                            case "18": //Mở màn hình Endless
                                break;
                            case "19": //Mở màn hình yêu cầu Rating
                                PopupManagerCuong.Instance.ShowRatePopup();
                                PopupManagerCuong.Instance.ChangeDepthRatePopup();
                                break;
                            case "20": //Hiện quảng cáo Intertisal
                                OsAdsManager.Instance.ShowInterstitialAds(OsAdsManager.LocationClickInterstitial.btn_Inter_InterstitialSever);
                                break;
                            case "21": //Hiện quảng cáo Rewards Video
                                if (OsAdsManager.Instance.isRewardedVideoAvailable())
                                {
                                    OsAdsManager.Instance.AddRewardedListenner(CallBackFinishVideoAds);
                                    OsAdsManager.Instance.AddClosedListenner(CallBackClosedVideoAds);
                                    OsAdsManager.Instance.ShowRewardedAds(OsAdsManager.LocationClickVideo.btn_video_Sever);
                                }
                                else
                                {
                                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_ads_not_available, false, 1.5f);
                                }
                                break;
                            case "22": // Mở popup IAP Gold
                                PopupManagerCuong.Instance.ShowShopPopup();
                                if (PopupShop.Instance != null)
                                {
                                    PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.IapGold);
                                }
                                break;
                            case "23": // Dis
                                GameContext.serverSaleVipPack = true;
                                PopupManagerCuong.Instance.Show3PackPopup();
                                Popup3Pack.Instance.ShowPackInPopup(TypePackShowInPopup3Pack.VipPack);
                                PopupManagerCuong.Instance.ChangeDepth3PackPopup();
                                break;
                            case "24": // Mở popup IAP Gem
                                PopupManagerCuong.Instance.ShowShopPopup();
                                if (PopupShop.Instance != null)
                                {
                                    PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.IapGem);
                                }
                                break;
                            case "27": // OPEN_INFINITY_PACK
                                if (!PopupManagerCuong.Instance.IsActiveShopPopup())
                                {
                                    PopupManagerCuong.Instance.ShowPopupPushInfinityPack();
                                }
                                break;
                            case "28": // OPEN_SALE_TICKET_PACK 
                                PopupManagerCuong.Instance.ShowPopupPushTicketLuckyPack();
                                break;
                            default:
                                break;
                        }
                        break;
                    case 3:
                        arr = url.Split(',');
                        x = int.Parse(arr[1]);
                        //mở level x
                        break;
                    case 4:
                        arr = url.Split(',');
                        x = int.Parse(arr[1]);
                        caseSplit = int.Parse(arr[0]);
                        switch (caseSplit)
                        {
                            case 25:
                                //Mở popup khuyến mãi x2 gem cho gói x (x = 1,2,3,4,5,6)
                                if (!PopupManagerCuong.Instance.IsActiveShopPopup())
                                {
                                    PopupManagerCuong.Instance.ShowPopupSaleGoldGem("gem", x);
                                }
                                break;
                            case 26:
                                //Mở popup khuyến mãi x2 vàng cho gói x (x = 1,2,3,4,5,6)
                                if (!PopupManagerCuong.Instance.IsActiveShopPopup())
                                {
                                    PopupManagerCuong.Instance.ShowPopupSaleGoldGem("gold", x);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case 6:
                    case 7:
                    case 8:
                        arr = url.Split(',');
                        x = int.Parse(arr[1]);
                        y = int.Parse(arr[2]);
                        caseSplit = int.Parse(arr[0]);
                        switch (caseSplit)
                        {
                            case 29:
                                //OPEN_SELL_PACK_CARD_AIRCRAFT = "29,x,y"; 
                                //x = gói sell
                                //y = index máy bay sell
                                //Debug.LogError("case 29: " + CacheGame.PopupCardPackIsSelling + " x" + x + " y" + y);
                                if (CacheGame.PopupCardPackIsSelling != 1)
                                {
                                    PopupManagerCuong.Instance.ShowCardPackPopup();
                                    PopupManagerCuong.Instance.SetDataCardPackPopup(x, y);
                                    IAPCallbackManager.Instance.isServerPush = true;
                                }
                                break;
                            case 30:
                                //OPEN_SALE_AIRCRAFT= "30,x,y"; 
                                //x= %Sale = 0,25,50
                                //y = index máy bay sell
                                //Debug.LogError("case 30: " + "unlocked " + CacheGame.GetSpaceShipIsUnlocked(y) + " x" + x + " y" + y);

                                if (CacheGame.GetSpaceShipIsUnlocked(y) != 1)
                                {
                                    PopupManagerCuong.Instance.ShowPopupSaleAircraft(x, y);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        Application.OpenURL(url);
                        break;
                }
                if (!url.Equals("21"))
                {
                    GetReward();
                }
            }
        }
    }

    void CallBackFinishVideoAds()
    {
        GetReward();
    }

    void CallBackClosedVideoAds()
    {
        Debug.Log("CallBackClosedVideoAds");
    }

    void GetReward()
    {
        lMessage.gameObject.SetActive(false);
        sImage.gameObject.SetActive(false);
        urlNull.SetActive(false);
        urlNotNull.SetActive(false);
        if (promotions != null)
        {
            int cnt = 0;
            for (int i = 0; i < listRewardParticle.Count; i++)
            {
                listRewardParticle[i].SetActive(false);
            }
            foreach (var item in promotions)
            {
                if (cnt < 3)
                {
                    listRewardParticle[cnt].SetActive(true);
                    listTweenPosition[cnt].Play(true);
                    listTweenPosition[cnt].ResetToBeginning();
                    listTweenAlpha[cnt].Play(true);
                    listTweenAlpha[cnt].ResetToBeginning();
                    background.SetActive(false);
                    title.SetActive(false);
                    listRewardSprite[cnt].spriteName = FalconMail.GetSpriteName(item.itemIndex, item.volume);
                    switch (item.itemIndex)
                    {
                        case FalconMail.ITEM_GOLD:
                            if (PanelCoinGem.Instance != null)
                            {
                                PanelCoinGem.Instance.AddCoinTop(item.volume, FirebaseLogSpaceWar.PvP_why);
                            }
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_GEM:
                            if (PanelCoinGem.Instance != null)
                            {
                                PanelCoinGem.Instance.AddGemTop(item.volume, FirebaseLogSpaceWar.PvP_why);
                            }
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_ENERGY:
                            if (PanelCoinGem.Instance != null)
                            {
                                PanelCoinGem.Instance.AddEnergyTop(item.volume, FirebaseLogSpaceWar.PvP_why);
                            }
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_POWER_UP:
                            CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + item.volume);
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_ACTIVESKILL:
                            CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + item.volume);
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_LIFE:
                            CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + item.volume);
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_PLANE:
                            CacheGame.SetSpaceShipIsUnlocked(item.volume, 1);
                            CacheGame.SetSpaceShipUserSelected(item.volume);
                            MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI);
                            listRewardLabel[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_WINGMAN:
                            CacheGame.SetWingManIsUnlocked(item.volume, 1);
                            CacheGame.SetWingManLeftUserSelected(item.volume);
                            MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI);
                            listRewardLabel[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_WING:
                            CacheGame.SetWingIsUnlocked(item.volume, 1);
                            MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI);
                            listRewardLabel[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_SECONDARY_WEAPON:
                            CacheGame.SetSecondaryWeaponIsUnlocked(item.volume, 1);
                            CacheGame.SetSecondaryWeaponSelected(item.volume);
                            MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI);
                            listRewardLabel[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_GENERAL:
                            MinhCacheGame.SetSpaceShipGeneralCards(MinhCacheGame.GetSpaceShipGeneralCards() + item.volume, FirebaseLogSpaceWar.PvP_why, "");
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_1:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.BataFD, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD) + item.volume, FirebaseLogSpaceWar.PvP_why, "", AircraftTypeEnum.BataFD.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_2:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.SkyWraith, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith) + item.volume, FirebaseLogSpaceWar.PvP_why, "", AircraftTypeEnum.SkyWraith.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_3:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.FuryOfAres, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres) + item.volume, FirebaseLogSpaceWar.PvP_why, "", AircraftTypeEnum.FuryOfAres.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_4:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.MacBird, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird) + item.volume, FirebaseLogSpaceWar.PvP_why, "", AircraftTypeEnum.MacBird.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_5:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.TwilightX, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX) + item.volume, FirebaseLogSpaceWar.PvP_why, "", AircraftTypeEnum.TwilightX.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_6:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + item.volume, FirebaseLogSpaceWar.PvP_why, "", AircraftTypeEnum.StarBomb.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_7:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.IceShard, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.IceShard) + item.volume, FirebaseLogSpaceWar.PvP_why, "", AircraftTypeEnum.IceShard.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_8:
                            MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.ThunderBolt, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.ThunderBolt) + item.volume, FirebaseLogSpaceWar.PvP_why, "", AircraftTypeEnum.ThunderBolt.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_GENERAL:
                            MinhCacheGame.SetWingmanGeneralCards(MinhCacheGame.GetWingmanGeneralCards() + item.volume, FirebaseLogSpaceWar.PvP_why, "");
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_1:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.GatlingGun, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.GatlingGun) + item.volume, FirebaseLogSpaceWar.PvP_why, "", WingmanTypeEnum.GatlingGun.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_2:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.AutoGatlingGun, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.AutoGatlingGun) + item.volume, FirebaseLogSpaceWar.PvP_why, "", WingmanTypeEnum.AutoGatlingGun.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_3:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.Lazer, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Lazer) + item.volume, FirebaseLogSpaceWar.PvP_why, "", WingmanTypeEnum.Lazer.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_4:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.DoubleGalting, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.DoubleGalting) + item.volume, FirebaseLogSpaceWar.PvP_why, "", WingmanTypeEnum.DoubleGalting.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_5:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.HomingMissile, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile) + item.volume, FirebaseLogSpaceWar.PvP_why, "", WingmanTypeEnum.HomingMissile.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_6:
                            MinhCacheGame.SetWingmanCards(WingmanTypeEnum.Splasher, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Splasher) + item.volume, FirebaseLogSpaceWar.PvP_why, "", WingmanTypeEnum.Splasher.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WING_GENERAL:
                            MinhCacheGame.SetWingGeneralCards(MinhCacheGame.GetWingGeneralCards() + item.volume, FirebaseLogSpaceWar.PvP_why, "");
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WING_1:
                            MinhCacheGame.SetWingCards(WingTypeEnum.WingOfJustice, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfJustice) + item.volume, FirebaseLogSpaceWar.PvP_why, "", WingTypeEnum.WingOfJustice.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WING_2:
                            MinhCacheGame.SetWingCards(WingTypeEnum.WingOfRedemption, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfRedemption) + item.volume, FirebaseLogSpaceWar.PvP_why, "", WingTypeEnum.WingOfRedemption.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_CARD_WING_3:
                            MinhCacheGame.SetWingCards(WingTypeEnum.WingOfResolution, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfResolution) + item.volume, FirebaseLogSpaceWar.PvP_why, "", WingTypeEnum.WingOfResolution.ToString());
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_VIP_POINT:
                            CachePvp.myVipPoint += item.volume;
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_LUCKY_WHEEL_TICKET:
                            MinhCacheGame.AddSpinTickets(item.volume);
                            listRewardLabel[cnt].gameObject.SetActive(true);
                            listRewardLabel[cnt].text = GameContext.FormatNumber(item.volume);
                            break;
                        case FalconMail.ITEM_PREMIUM_PACK:
                            MinhCacheGame.SetAlreadyPurchasePremiumPack(true);
                            MinhCacheGame.SetPremiumPackExpiredTime(DateTime.Now.AddDays(GameContext.EXPIRED_TIME_PREMIUM_PACK));
                            listRewardLabel[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_INFINITY_PACK:
                            CacheGame.SetBuyInfinityPack(1);
                            listRewardLabel[cnt].gameObject.SetActive(false);
                            break;
                        default:
                            break;
                    }
                    cnt++;
                }
            }
            Debug.Log("cnt : " + cnt);
            if (cnt == 1)
            {
                listRewardParticle[0].transform.localPosition = Vector3.zero;
            }
            else if (cnt == 2)
            {
                listRewardParticle[0].transform.localPosition = new Vector3(-75, 0, 0);
                listRewardParticle[1].transform.localPosition = new Vector3(75, 0, 0);
            }
            else if (cnt == 3)
            {
                listRewardParticle[0].transform.localPosition = new Vector3(-150, 0, 0);
                listRewardParticle[1].transform.localPosition = Vector3.zero;
                listRewardParticle[2].transform.localPosition = new Vector3(150, 0, 0);
            }
            else if (cnt == 0)
            {
                PopupManager.Instance.HideNotifyPopup();
            }
            if (cnt != 0)
            {
                LeanTween.delayedCall(4, () =>
                {
                    PopupManager.Instance.HideNotifyPopup();
                }).setIgnoreTimeScale(true);
            }
        }
        else
        {
            PopupManager.Instance.HideNotifyPopup();
        }
    }

    public void ButtonClose()
    {
        GetReward();
    }

    public void ButtonNo()
    {
        PopupManager.Instance.HideNotifyPopup();
        //GameContext.serverSaleVipPack = true;
        if (url.Equals("1"))
        {
            //GameContext.serverX2Purchase = true;
            if (PopupShop.Instance != null)
            {
                PanelCoinGem.Instance.ShowGlowButtonIapGold();
            }
        }
        else if (url.Equals("23"))
        {
            GameContext.serverSaleVipPack = true;
        }
        new CSNotifyClick(id, CLICK_CANCEL).Send();
    }

    public void SetUrl(long id, string url, string message, string urlImage, int type, List<PromotionItem> promotions)
    {
        this.id = id;
        this.url = url;
        this.type = type;
        this.promotions = promotions;
        this.urlImage = urlImage;

        MessageDispatcher.SendMessage(EventName.PopupExtra.DisablePopupExtra.ToString());
        if (string.IsNullOrEmpty(url))
        {
            urlNull.SetActive(true);
            urlNotNull.SetActive(false);
        }
        else
        {
            urlNotNull.SetActive(true);
            urlNull.SetActive(false);
        }
        if (string.IsNullOrEmpty(urlImage))
        {
            lMessage.gameObject.SetActive(true);
            sImage.gameObject.SetActive(false);
            lMessage.text = message;
        }
        else
        {
            lMessage.gameObject.SetActive(false);
            sImage.gameObject.SetActive(true);
            StartCoroutine(DownloadImage(urlImage));
        }
        for (int i = 0; i < listRewardParticle.Count; i++)
        {
            listRewardParticle[i].SetActive(false);
        }
        if (type == 1)
        {
            ButtonYes();
            urlNull.SetActive(false);
            urlNotNull.SetActive(false);
        }
    }

    IEnumerator DownloadImage(string imageLink)
    {
        WWW www = new WWW(imageLink);
        yield return www;
        var texture = www.texture;
        if (texture != null)
        {
            Sprite sr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            sImage.sprite2D = sr;
        }
    }

    public void CallbackReward()
    {
        PopupManager.Instance.HideNotifyPopup();
        new CSNotifyClick(id, CLICK_OK).Send();
    }

}
