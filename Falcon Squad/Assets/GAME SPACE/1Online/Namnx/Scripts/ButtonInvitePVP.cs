﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OneSoftGame.Tools;
using Mp.Pvp;

public class ButtonInvitePVP : PersistentSingleton<ButtonInvitePVP>
{

    public void ButtonInvite()
    {
        CachePvp.typePVP = CSFindOpponent.TYPE_PVP;
        PopupManager.Instance.ShowInvitePopup();
        PopupManager.Instance.HideInviteButton();
    }

    public void ButtonInviteTournament()
    {
        CachePvp.typePVP = CSFindOpponent.TYPE_MEGA;
        PopupManager.Instance.ShowInvitePopupTournament();
        PopupManager.Instance.HideInviteButtonTournament();
    }

    public void ButtonInvite2v2()
    {
        CachePvp.typePVP = CSFindOpponent.TYPE_2V2;
        PopupManager.Instance.ShowInvitePopup(CachePvp.TYPE_2v2);
        PopupManager.Instance.HideInviteButton();
    }

    void OnDestroy()
    {
        PopupManager.Instance.DestroyInviteButton();
        PopupManager.Instance.DestroyInviteButtonTournament();
    }
}
