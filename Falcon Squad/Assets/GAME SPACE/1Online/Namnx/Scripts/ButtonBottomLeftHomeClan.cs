﻿using UnityEngine;
using com.ootii.Messages;

public class ButtonBottomLeftHomeClan : MonoBehaviour
{

    private void Awake()
    {
        MessageDispatcher.AddListener(EventName.UIHome.SetTurnOnTeamSCClientConfig.ToString(), OnSetTurnOnTeamSCClientConfig, true);
        RefreshUI();
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.UIHome.SetTurnOnTeamSCClientConfig.ToString(), OnSetTurnOnTeamSCClientConfig, true);
    }

    private void OnSetTurnOnTeamSCClientConfig(IMessage rMessage)
    {
        RefreshUI();
    }

    void RefreshUI()
    {
        if (CachePvp.sCClientConfig.turnOnTeam == 1)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
