﻿using com.ootii.Messages;
using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFindMemberClan : MonoBehaviour
{
    public UI2DSprite sAvatar;
    public UILabel lName;
    public UILabel lId;
    public UISprite sRank;
    public UISprite sFlag;
    public UILabel lRank;

    public UILabel lElo;
    public string facebookID;

    public UISprite sVipIcon;

    public UILabel lNameClan;
    public UISprite sOnline;
    public UILabel lOnline;

    Player player;
    void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    public void SetData(Player p)
    {
        this.player = p;
        if (p == null || CachePvp.GetVipFromVipPoint(p.vip) == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (CachePvp.GetVipFromVipPoint(p.vip) - 1);
        }

        lId.text = I2.Loc.ScriptLocalization.elo_key + ": " + p.code;
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        sFlag.spriteName = p.country.ToLower();

        int rank = CachePvp.dictionaryRank[p.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(p.playerLevel.elo);
        lRank.text = rankTitle + " " + CachePvp.ConvertChina(p.playerLevel.name);
        sRank.spriteName = "PVP_rank_" + rank;
        lOnline.text = player.offline ? I2.Loc.ScriptLocalization.offline : I2.Loc.ScriptLocalization.online;
        sOnline.spriteName = player.offline ? "status_offline" : "status_online";
        if (player.playerClan != null)
        {
            lNameClan.text = player.playerClan.name;
        }
        else
        {
            lNameClan.text = "";
        }
        if (p != null && !string.IsNullOrEmpty(p.code))
        {
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(p.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(p.appCenterId))
                    {
                        showImageFB = false;
                        facebookID = p.appCenterId;
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[p.appCenterId];
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(p.appCenterId);
                        if (t != null)
                        {
                            showImageFB = false;
                            facebookID = p.appCenterId;
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", p.appCenterId, t);
                        }
                    }
                }
            }
            if (showImageFB)
            {
                facebookID = p.facebookId;
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(p.facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(p.facebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[p.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + p.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, p.facebookId);
                    }
                }
            }
            if (string.IsNullOrEmpty(p.name))
            {
                lName.text = p.code.ToLower();
            }
            else
            {
                lName.text = p.name;
            }
        }
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    public void OnClick()
    {
        Debug.Log("onclick");
        Debug.Log(player.code);
        if (player != null)
        {
            PopupManager.Instance.ShowProfilePopupOnTop(player);
        }
    }
}
