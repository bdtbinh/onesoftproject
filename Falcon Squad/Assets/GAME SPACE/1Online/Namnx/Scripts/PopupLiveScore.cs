﻿using com.ootii.Messages;
using Mp.Pvp;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupLiveScore : MonoBehaviour
{
    private void OnEnable()
    {
        if (CachePvp.LoadLiveScore == 1)
        {
            new CSLiveScoreJoin(CSLiveScoreJoin.TYPE_MEGA).Send();
        }
        else if (CachePvp.LoadLiveScore == 0)
        {
            new CSLiveScoreJoin(CSLiveScoreJoin.TYPE_PVP).Send();
        }
        else if (CachePvp.LoadLiveScore == 2)
        {
            new CSLiveScoreJoin(CSLiveScoreJoin.TYPE_PVP_2VS2).Send();
        }
        MessageDispatcher.AddListener(EventName.LiveScore.LoadListRoom.ToString(), OnLoadListRoom, true);
        MessageDispatcher.AddListener(EventName.Clan.PlayerInfo.ToString(), OnLoadPlayerInfo, true);
    }

    private void OnDisable()
    {
        if (CachePvp.LoadLiveScore == 1)
        {
            new CSLiveScoreLeave(CSLiveScoreJoin.TYPE_MEGA).Send();
        }
        else if (CachePvp.LoadLiveScore == 0)
        {
            new CSLiveScoreLeave(CSLiveScoreJoin.TYPE_PVP).Send();
        }
        else if (CachePvp.LoadLiveScore == 2)
        {
            new CSLiveScoreLeave(CSLiveScoreJoin.TYPE_PVP_2VS2).Send();
        }
        MessageDispatcher.RemoveListener(EventName.LiveScore.LoadListRoom.ToString(), OnLoadListRoom, true);
        MessageDispatcher.RemoveListener(EventName.Clan.PlayerInfo.ToString(), OnLoadPlayerInfo, true);
    }

    private void OnLoadPlayerInfo(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCPlayerInfo.SUCCESS)
        {
            if (!PopupManager.Instance.IsProfilePopupActive())
            {
                PopupManager.Instance.ShowProfilePopup(rMessage.Data as Player);
            }
        }
    }

    private void OnLoadListRoom(IMessage rMessage)
    {
        SCLiveScoreJoin lsj = rMessage.Data as SCLiveScoreJoin;

        if (lsj.status == SCLiveScoreJoin.SUCCESS)
        {
            MessageDispatcher.SendMessage(gameObject, EventName.LiveScore.LoadedListRoom.ToString(), lsj, 0);
        }
    }

    public void ButtonClose()
    {
        PopupManager.Instance.HideLiveScorePopup();
    }
}