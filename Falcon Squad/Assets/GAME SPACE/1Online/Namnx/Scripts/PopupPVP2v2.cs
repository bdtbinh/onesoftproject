﻿using Mp.Pvp;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using com.ootii.Messages;

public class PopupPVP2v2 : MonoBehaviour
{
    public UILabel lTime;
    public UI2DSprite sAvatar;
    public UILabel lName;
    public UILabel lNameClan;
    public UILabel lWinrate;
    public UILabel lSeasonScore;

    public UISprite sChest;
    public UILabel lChest;

    public List<UISprite> listSpriteReward;
    public List<UILabel> listLabelReward;

    public UISprite sBettingMin;
    public UILabel lBettingMin;
    public UISprite sBettingMiddle;
    public UILabel lBettingMiddle;
    public UISprite sBettingMax;
    public UILabel lBettingMax;

    private const string BTN_BETTING_CHOSEN_SPRITE = "IAP_btn_dollar";
    private const string BTN_BETTING_UNCHOSEN_SPRITE = "IAP_btn_dollar_d";

    private void OnDisable()
    {
        if (countDown2Minutes != null)
        {
            StopCoroutine(countDown2Minutes);
        }
        MessageDispatcher.RemoveListener(EventName.PVP.CoopPvPTournamentInfo.ToString(), OnCoopPvPTournamentInfo, true);
        MessageDispatcher.RemoveListener(EventName.PVP.CoopPvPSeasonRewardedInfo.ToString(), OnCoopPvPSeasonRewardedInfo, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
        {
            sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
        }
    }

    private void OnEnable()
    {
        readyClickBack = true;
        MessageDispatcher.AddListener(EventName.PVP.CoopPvPTournamentInfo.ToString(), OnCoopPvPTournamentInfo, true);
        MessageDispatcher.AddListener(EventName.PVP.CoopPvPSeasonRewardedInfo.ToString(), OnCoopPvPSeasonRewardedInfo, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        LeanTween.delayedCall(0.1f, () =>
        {
            new CSSceneLoaded(CSSceneLoaded.SCENE_COOP_PVP_MAIN_2v2).Send();
        });
        if (MinhCacheGame.GetBettingAmountPvP2v2() != GetBettingMin() && MinhCacheGame.GetBettingAmountPvP2v2() != GetBettingMiddle() && MinhCacheGame.GetBettingAmountPvP2v2() != GetBettingMax())
        {
            MinhCacheGame.SetBettingAmountPvP2v2(GetBettingMin());
        }
        if (MinhCacheGame.GetBettingAmountPvP2v2() == GetBettingMin())
        {
            sBettingMin.spriteName = BTN_BETTING_CHOSEN_SPRITE;
        }
        else if (MinhCacheGame.GetBettingAmountPvP2v2() == GetBettingMiddle())
        {
            sBettingMiddle.spriteName = BTN_BETTING_CHOSEN_SPRITE;
        }
        else if (MinhCacheGame.GetBettingAmountPvP2v2() == GetBettingMax())
        {
            sBettingMax.spriteName = BTN_BETTING_CHOSEN_SPRITE;
        }
        lTime.text = "";
        lBettingMin.text = GameContext.FormatNumber(GetBettingMin()) + "";
        lBettingMiddle.text = GameContext.FormatNumber(GetBettingMiddle()) + "";
        lBettingMax.text = GameContext.FormatNumber(GetBettingMax()) + "";

        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(CachePvp.GamecenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.GamecenterId))
                {
                    showImageFB = false;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.GamecenterId];
                }
            }
        }
        if (showImageFB)
        {
            if (!string.IsNullOrEmpty(CachePvp.FacebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + CachePvp.FacebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.FacebookId);
                }
            }
        }

        lName.text = string.IsNullOrEmpty(CachePvp.Name) ? CachePvp.Code : CachePvp.Name;
        lNameClan.text = string.IsNullOrEmpty(CachePvp.PlayerClan.name) ? "" : CachePvp.PlayerClan.name;

        float winrate = 0;
        if (CachePvp.PvP2vs2SeasonData == null || CachePvp.PvP2vs2SeasonData.win == 0)
        {
            winrate = 0;
        }
        else
        {
            winrate = (float)CachePvp.PvP2vs2SeasonData.win * 100 / (CachePvp.PvP2vs2SeasonData.win + CachePvp.PvP2vs2SeasonData.lose);
        }
        lWinrate.text = winrate.ToString("0.00") + "%";
        lSeasonScore.text = GameContext.FormatNumber(CachePvp.PvP2vs2SeasonData.score);
        if (CachePvp.coopPvPTournamentInfo != null)
        {
            if (CachePvp.coopPvPTournamentInfo.info.state == PvP2vs2TournamentInfo.START)
            {
                timeRemaining = (int)CachePvp.coopPvPTournamentInfo.info.endTimeRemain;
                isEnd = false;
            }
            else if (CachePvp.coopPvPTournamentInfo.info.state == PvP2vs2TournamentInfo.END)
            {
                timeRemaining = (int)CachePvp.coopPvPTournamentInfo.info.startTimeRemain;
                isEnd = true;
            }
            if (countDown2Minutes != null)
            {
                StopCoroutine(countDown2Minutes);
            }
            if (timeRemaining > 0)
            {
                countDown2Minutes = CountDown2Minutes();
                StartCoroutine(countDown2Minutes);
            }

            if (!string.IsNullOrEmpty(CachePvp.coopPvPTournamentInfo.info.chestName))
            {
                lChest.text = CachePvp.MedalRank2v2Title(CachePvp.coopPvPTournamentInfo.info.chestName);
                sChest.spriteName = CachePvp.GetSpriteNameChest(CachePvp.coopPvPTournamentInfo.info.chestName);
                sChest.enabled = true;
                for (int i = 0; i < listSpriteReward.Count; i++)
                {
                    listSpriteReward[i].transform.parent.gameObject.SetActive(true);
                }
            }
            else
            {
                lChest.text = "";
                sChest.enabled = false;
                for (int i = 0; i < listSpriteReward.Count; i++)
                {
                    listSpriteReward[i].transform.parent.gameObject.SetActive(false);
                }
            }

            listSpriteReward[0].spriteName = FalconMail.GetSpriteName(FalconMail.ITEM_GOLD);
            listSpriteReward[1].spriteName = FalconMail.GetSpriteName(FalconMail.ITEM_GEM);
            listSpriteReward[2].spriteName = FalconMail.GetSpriteName(FalconMail.ITEM_CARD_WING_GENERAL);

            listLabelReward[0].text = GameContext.FormatNumber(CachePvp.coopPvPTournamentInfo.info.gold);
            listLabelReward[1].text = GameContext.FormatNumber(CachePvp.coopPvPTournamentInfo.info.gem);
            listLabelReward[2].text = GameContext.FormatNumber(CachePvp.coopPvPTournamentInfo.info.wingGeneral);

            RefreshInfo();
        }
        if (CachePvp.isConnectServer)
        {
            ConnectManager.Instance.HandShake();
        }
        PvpUtil.SendUpdatePlayer();
    }

    public void OnClickBtnBetMin()
    {
        sBettingMin.spriteName = BTN_BETTING_CHOSEN_SPRITE;
        sBettingMiddle.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;
        sBettingMax.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;

        SetBettingAmount(GetBettingMin());
    }

    public void OnClickBtnBetMiddle()
    {
        sBettingMin.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;
        sBettingMiddle.spriteName = BTN_BETTING_CHOSEN_SPRITE;
        sBettingMax.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;

        SetBettingAmount(GetBettingMiddle());
    }

    public void OnClickBtnBetMax()
    {
        sBettingMin.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;
        sBettingMiddle.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;
        sBettingMax.spriteName = BTN_BETTING_CHOSEN_SPRITE;

        SetBettingAmount(GetBettingMax());
    }

    void SetBettingAmount(int amount)
    {
        MinhCacheGame.SetBettingAmountPvP2v2(amount);
    }

    int GetBettingMin()
    {
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code] - 1; //from 0 -> 7
        if (CachePvp.listGoldBet2vs2.Count > 3 * rank + 2)
        {
            return CachePvp.listGoldBet2vs2[3 * rank];
        }
        else
        {
            if (CachePvp.listGoldBet2vs2.Count >= 3)
            {
                return CachePvp.listGoldBet2vs2[CachePvp.listGoldBet2vs2.Count - 3];
            }
            else
            {
                return Constant.BETTING_AMOUNT_MIN;
            }
        }
    }

    int GetBettingMiddle()
    {
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code] - 1; //from 0 -> 7
        if (CachePvp.listGoldBet2vs2.Count > 3 * rank + 2)
        {
            return CachePvp.listGoldBet2vs2[3 * rank + 1];
        }
        else
        {
            if (CachePvp.listGoldBet2vs2.Count >= 3)
            {
                return CachePvp.listGoldBet2vs2[CachePvp.listGoldBet2vs2.Count - 2];
            }
            else
            {
                return Constant.BETTING_AMOUNT_MIDDLE;
            }
        }
    }

    int GetBettingMax()
    {
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code] - 1; //from 0 -> 7
        if (CachePvp.listGoldBet2vs2.Count > 3 * rank + 2)
        {
            return CachePvp.listGoldBet2vs2[3 * rank + 2];
        }
        else
        {
            if (CachePvp.listGoldBet2vs2.Count >= 3)
            {
                return CachePvp.listGoldBet2vs2[CachePvp.listGoldBet2vs2.Count - 1];
            }
            else
            {
                return Constant.BETTING_AMOUNT_MAX;
            }
        }
    }

    private void OnCoopPvPSeasonRewardedInfo(IMessage rMessage)
    {
        SCCoopPvPSeasonRewardedInfo ci = rMessage.Data as SCCoopPvPSeasonRewardedInfo;
        if (ci.status == SCCoopPvPTournamentInfo.SUCCESS)
        {
            List<ItemVolumeFalconMail> list = new List<ItemVolumeFalconMail>();
            for (int i = 0; i < ci.items.Count; i++)
            {
                ItemVolumeFalconMail item = new ItemVolumeFalconMail();
                item.key = ci.items[i].itemIndex;
                item.value = ci.items[i].volume;
                list.Add(item);
            }
            PopupManager.Instance.ShowClaimPopup(list, EventName.POPUP_TOURNAMENT_2V2, false, true);
        }
    }

    private void OnCoopPvPTournamentInfo(IMessage rMessage)
    {
        SCCoopPvPTournamentInfo ci = rMessage.Data as SCCoopPvPTournamentInfo;
        if (ci.status == SCCoopPvPTournamentInfo.SUCCESS)
        {
            if (ci.info.state == PvP2vs2TournamentInfo.START)
            {
                timeRemaining = (int)ci.info.endTimeRemain;
                isEnd = false;
            }
            else if (ci.info.state == PvP2vs2TournamentInfo.END)
            {
                timeRemaining = (int)ci.info.startTimeRemain;
                isEnd = true;
            }
            if (countDown2Minutes != null)
            {
                StopCoroutine(countDown2Minutes);
            }
            if (timeRemaining > 0)
            {
                countDown2Minutes = CountDown2Minutes();
                StartCoroutine(countDown2Minutes);
            }

            if (!string.IsNullOrEmpty(ci.info.chestName))
            {
                lChest.text = CachePvp.MedalRank2v2Title(ci.info.chestName);
                sChest.spriteName = CachePvp.GetSpriteNameChest(ci.info.chestName);
                sChest.enabled = true;
                for (int i = 0; i < listSpriteReward.Count; i++)
                {
                    listSpriteReward[i].transform.parent.gameObject.SetActive(true);
                }
            }
            else
            {
                lChest.text = "";
                sChest.enabled = false;
                for (int i = 0; i < listSpriteReward.Count; i++)
                {
                    listSpriteReward[i].transform.parent.gameObject.SetActive(false);
                }
            }

            listSpriteReward[0].spriteName = FalconMail.GetSpriteName(FalconMail.ITEM_GOLD);
            listSpriteReward[1].spriteName = FalconMail.GetSpriteName(FalconMail.ITEM_GEM);
            listSpriteReward[2].spriteName = FalconMail.GetSpriteName(FalconMail.ITEM_CARD_WING_GENERAL);

            listLabelReward[0].text = GameContext.FormatNumber(ci.info.gold);
            listLabelReward[1].text = GameContext.FormatNumber(ci.info.gem);
            listLabelReward[2].text = GameContext.FormatNumber(ci.info.wingGeneral);

            RefreshInfo();
        }
    }

    IEnumerator countDown2Minutes;
    int target;
    int timeRemaining;
    private bool isEnd;

    IEnumerator CountDown2Minutes()
    {
        TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        target = (int)span.TotalSeconds + timeRemaining;
        while (timeRemaining >= 0)
        {
            if (isEnd)
            {
                lTime.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", "[be3439]" + (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00") + "[-]");
            }
            else
            {
                lTime.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", "[be3439]" + (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00") + "[-]");
            }
            TimeSpan temp_span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            timeRemaining = target - (int)temp_span.TotalSeconds;
            yield return new WaitForSecondsRealtime(1);
        }
        yield return new WaitForSecondsRealtime(1);
        new CSCoopPvPTournamentInfo().Send();
    }

    void RefreshInfo()
    {
        if (isEnd)
        {
            lTime.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", "[be3439]" + (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00") + "[-]");
        }
        else
        {
            lTime.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", "[be3439]" + (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00") + "[-]");
        }
    }

    public void bStart()
    {
        if (MinhCacheGame.GetBettingAmountPvP2v2() > CachePvp.GetGold())
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
            return;
        }
        new CSCoopPvPMatching(MinhCacheGame.GetBettingAmountPvP2v2(), CacheGame.GetSpaceShipUserSelected()).Send();
    }

    public void btnBack()
    {
        PopupManager.Instance.HidePVP2V2Popup();
    }

    public void ShowLeaderboard()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        //if (!CachePvp.sCVersionConfig.canRank)
        //{
        //    PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainRank);
        //    return;
        //}
        PopupManager.Instance.ShowRanking(false, true);
    }

    public void ShowLiveScore2v2()
    {
        CachePvp.LoadLiveScore = 2;
        PopupManager.Instance.ShowLiveScorePopup();
    }

    public void bFindFriends()
    {
        if (MinhCacheGame.GetBettingAmountPvP2v2() > CachePvp.GetGold())
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
            return;
        }
        if (CachePvp.coopPvPTournamentInfo.info.state == PvP2vs2TournamentInfo.END)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_tourmanet_not_start_yet, false);
            return;
        }
        PopupManager.Instance.ShowInviteFriends2v2Popup();
    }

    public void bChest()
    {
        PopupManager.Instance.ShowRankRewardPopup2v2();
    }

    private bool readyClickBack;

    IEnumerator ReadyClickBack()
    {
        readyClickBack = false;
        yield return new WaitForSecondsRealtime(0.38f);
        readyClickBack = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && readyClickBack)
        {
            StartCoroutine(ReadyClickBack());
            if (PopupManager.Instance.IsInviteFriends2v2PopupActive())
            {
                PopupManager.Instance.HideInviteFriends2v2Popup();
            }
            else if (PopupManagerCuong.Instance.IsActiveShopPopup())
            {
                PopupManagerCuong.Instance.HideShopPopup();
            }
            else if (PopupManager.Instance.IsVideoRewardPopupActive())
            {
                PopupManager.Instance.HideVideoRewardPopup();
            }
            else if (PopupManager.Instance.IsProfilePopupActive())
            {

            }
            else if (PopupManager.Instance.IsRankRewardPopupActive2v2())
            {
                PopupManager.Instance.HideRankRewardPopup2v2();
            }
            else if (PopupManager.Instance.IsRankingPopupActive())
            {
                PopupManager.Instance.HideRankingPopup();
            }
            else if (PopupManager.Instance.IsLiveScorePopupActive())
            {
                PopupManager.Instance.HideLiveScorePopup();
            }
            else if (PopupManager.Instance.IsClaimPopupActive())
            {

            }
            else
            {
                btnBack();
            }
        }
    }
}