﻿using OneSoftGame.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonTextRunning : PersistentSingleton<ButtonTextRunning>
{
    public UILabel lTextRunning;
    string currentText = "";
    float currentPositionX;
    float timeRemaining;
    float timePassed;
    string url = "";
    bool isRunning;

    float timeStart;

    public void SetData(string text, bool isRunning, float startPosition = 360, float timePassed = 0, string url = "")
    {
        if (string.IsNullOrEmpty(text))
        {
            return;
        }
        this.isRunning = isRunning;
        if (isRunning)
        {
            currentText += " " + text;
            timePassed += Time.realtimeSinceStartup - timeStart;
        }
        else
        {
            currentText = text;
            currentPositionX = startPosition;
        }
        this.url = url;
        this.timePassed = timePassed;
        lTextRunning.text = currentText;
        lTextRunning.transform.localPosition = new Vector3(currentPositionX, lTextRunning.transform.localPosition.y, lTextRunning.transform.localPosition.z);
        StartCoroutine(Wait1Frame());
    }

    public void OpenUrl()
    {
        if (!string.IsNullOrEmpty(url))
        {
            Application.OpenURL(url);
        }
    }

    Vector3 tempVector3 = Vector3.zero;

    private IEnumerator Wait1Frame()
    {
        yield return null;
        float width = lTextRunning.width;
        if (isRunning)
        {
            LeanTween.cancel(lTextRunning.gameObject);
        }
        timeRemaining = lTextRunning.text.Length / 2.0f - timePassed;
        LeanTween.value(lTextRunning.gameObject, currentPositionX, -(width + 360), timeRemaining).setOnUpdate((float value) =>
        {
            tempVector3.x = value;
            tempVector3.y = lTextRunning.transform.localPosition.y;
            tempVector3.z = lTextRunning.transform.localPosition.z;
            lTextRunning.transform.localPosition = tempVector3;
            currentPositionX = value;
        }).setOnComplete(() =>
        {
            currentPositionX = 360;
            currentText = "";
            PopupManager.Instance.HideTextRunningPopup();
        }).setOnStart(() =>
        {
            timeStart = Time.realtimeSinceStartup;
        }).setIgnoreTimeScale(true);
    }

    private void OnDestroy()
    {
        LeanTween.cancel(lTextRunning.gameObject);
        PopupManager.Instance.RunningTextAgain(lTextRunning.text, currentPositionX, Time.realtimeSinceStartup - timeStart + timePassed);
    }
}
