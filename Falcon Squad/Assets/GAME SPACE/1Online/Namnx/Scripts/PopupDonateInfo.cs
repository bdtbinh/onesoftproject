﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using Mp.Pvp;
using System;
using System.Globalization;

public class PopupDonateInfo : MonoBehaviour
{

    public GameObject lNoInfo;
    public List<GameObject> listWrapperDonate;
    public List<UILabel> listLabelDate;
    public List<UILabel> listLabelStatus;
    public List<UILabel> listLabelProgress;
    public List<UISprite> listSpriteItem;
    public List<UILabel> listLabelName1;
    public List<UILabel> listLabelName2;
    public List<UILabel> listLabelName3;
    public List<GameObject> listGameObjectNoOneDonateYet;

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Clan.DonateLog.ToString(), OnDonateLog, true);
    }

    void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Clan.DonateLog.ToString(), OnDonateLog, true);
        for (int i = 0; i < listWrapperDonate.Count; i++)
        {
            listWrapperDonate[i].SetActive(false);
        }
        lNoInfo.SetActive(false);
        new CSDonateLog(CachePvp.clanId).Send();
    }

    private void OnDonateLog(IMessage rMessage)
    {
        SCDonateLog dl = rMessage.Data as SCDonateLog;
        if (dl.logs.Count == 0)
        {
            lNoInfo.SetActive(true);
        }
        else
        {
            lNoInfo.SetActive(false);
        }
        for (int i = 0; i < listWrapperDonate.Count; i++)
        {
            if (i < dl.logs.Count)
            {
                listWrapperDonate[i].SetActive(true);
                //time
                DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                double dou = dl.logs[i].startTime;
                DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                listLabelDate[i].text = m_dateTime.ToString("dd-MM-yyyy");
                //status
                if (dl.logs[i].received == dl.logs[i].maxCanReceive)
                {
                    listLabelStatus[i].text = I2.Loc.ScriptLocalization.completed;
                }
                else
                {
                    listLabelStatus[i].text = I2.Loc.ScriptLocalization.ongoing;
                }
                //progress
                listLabelProgress[i].text = dl.logs[i].received + "/" + dl.logs[i].maxCanReceive;
                //sprite item
                listSpriteItem[i].spriteName = FalconMail.GetSpriteName(dl.logs[i].cardType, 1);
                //donater
                if (dl.logs[i].received == 0)
                {
                    listGameObjectNoOneDonateYet[i].SetActive(true);
                }
                else
                {
                    listGameObjectNoOneDonateYet[i].SetActive(false);
                }
                if (i == 0)
                {
                    for (int j = 0; j < listLabelName1.Count; j++)
                    {
                        if (j < dl.logs[i].donaters.Count)
                        {
                            listLabelName1[j].text = (j + 1) + "." + dl.logs[i].donaters[j];
                        }
                        else
                        {
                            listLabelName1[j].text = "";
                        }
                    }
                }
                else if (i == 1)
                {
                    for (int j = 0; j < listLabelName2.Count; j++)
                    {
                        if (j < dl.logs[i].donaters.Count)
                        {
                            listLabelName2[j].text = (j + 1) + "." + dl.logs[i].donaters[j];
                        }
                        else
                        {
                            listLabelName2[j].text = "";
                        }
                    }
                }
                else if (i == 2)
                {
                    for (int j = 0; j < listLabelName3.Count; j++)
                    {
                        if (j < dl.logs[i].donaters.Count)
                        {
                            listLabelName3[j].text = (j + 1) + "." + dl.logs[i].donaters[j];
                        }
                        else
                        {
                            listLabelName3[j].text = "";
                        }
                    }
                }
            }
            else
            {
                listWrapperDonate[i].SetActive(false);
            }
        }
    }

    public void OnClickClosePopup()
    {
        PopupManager.Instance.HideDonateInfoPopup();
    }
}
