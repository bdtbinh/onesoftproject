﻿using OSNet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonNamnx : MonoBehaviour
{

    public void ButtonRanking()
    {

        if (CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL) <= CachePvp.RequireLevelPlayerInfo)
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", CachePvp.RequireLevelPlayerInfo + ""));
            return;
        }

        if (!NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }

        if (CachePvp.Name == "")
        {
            Debug.Log("Show Register");
            PopupManager.Instance.ShowRegisterPopup("ranking");
        }
        else
        {
            Debug.Log("Show Ranking");
            if (!CachePvp.sCVersionConfig.canRank)
            {
                PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainRank);
                return;
            }
            PopupManager.Instance.ShowRanking();
        }
        FirebaseLogSpaceWar.LogClickButton("Ranking");
        //		PopupManager.Instance.ShowInviteButton (null, 0);
    }

    public void ButtonAddCoin()
    {
        CacheGame.SetTotalCoin(2000000, "test", "test");
    }
}
