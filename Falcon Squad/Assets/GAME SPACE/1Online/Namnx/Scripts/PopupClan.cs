﻿using com.ootii.Messages;
using Mp.Pvp;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupClan : MonoBehaviour
{
    public GameObject panelLoading;

    public GameObject buttonTeamMail;

    public GameObject gPanelMedalTop;
    public UILabel lNumMedal;

    private const string SELECTED_SPRITE_NAME = "PVP_local_titler_a";
    private const string UNSELECTED_SPRITE_NAME = "PVP_local_titler_d";
    public UISprite sBtnMyClan;
    public UISprite sBtnChat;
    public UISprite sBtnWorld;

    public GameObject btnChatClan;
    public GameObject btnChatWorld;
    public UISprite sBtnChatClan;
    public UISprite sBtnChatWorld;
    public UILabel lDontHaveClan;

    public GameObject confirmSaveChange;

    public UILabel lTeamType;
    public UILabel lTeamTypeClanObject;
    public UILabel lTeamTypeConfirm;
    public UILabel lRequiredLevel;
    public UILabel lRequiredLevelClanObject;
    public UILabel lRequiredLevelConfirm;
    public GameObject bLeftTypeClanObject;
    public GameObject bLeftLevelRequiredClanObject;
    public GameObject bRightTypeClanObject;
    public GameObject bRightLevelRequiredClanObject;

    public GameObject gRequestChat;
    public GameObject gRequestChatOffTime;
    public GameObject gShopClan;
    public UILabel lStatusTimeDonate;

    public GameObject gTopChat;
    public UILabel lTopChat;
    public GameObject gBottomChat;
    public UILabel lBottomChat;

    public GameObject panelClanInfo;
    public UISprite avatarClanInfo;
    public UILabel nameClanInfo;
    public UILabel memberClanInfo;
    public UILabel scoreValueClanInfo;
    public UILabel typeValueClanInfo;
    public UILabel levelRequiredValueClanInfo;

    public UISprite buttonJoinClanInfo;
    public GameObject buttonJoinDeactiveClanInfo;

    public GameObject buttonDenyInvite;

    public UILabel lNeedLevel;
    public UILabel lNeedLevel2;
    public UILabel lNeedGem;
    public UILabel lNeedGem2;


    private static PopupClan _instance;
    public static PopupClan Instance
    {
        get
        {
            return _instance;
        }
    }

    private int index;

    public void DeactiveListButtonChoose(int index)
    {
        for (int i = 0; i < listButtonChooseAvatar.Count; i++)
        {
            if (index == i)
            {
                listButtonChooseAvatar[i].transform.GetChild(1).gameObject.SetActive(true);
                listButtonChooseAvatar[i].transform.GetChild(2).gameObject.SetActive(true);
            }
            else
            {
                listButtonChooseAvatar[i].transform.GetChild(1).gameObject.SetActive(false);
                listButtonChooseAvatar[i].transform.GetChild(2).gameObject.SetActive(false);
            }
        }
        this.index = index;
        Debug.Log("Index : " + index);
    }

    public GameObject contentMyClan;
    public GameObject first;
    public GameObject createClan;
    public GameObject bgChooseAvatar;
    public UISprite sAvatarClan;

    public List<GameObject> listButtonChooseAvatar;

    private int typeJoinRoomChat; //0 : no join, 1: chat clan, 2: chat world

    private void Awake()
    {
        _instance = this;
    }

    public InfiniteListMyClanPopulator listMyClanPopulator;

    public UIInput inputName;

    public GameObject fxLoadingScrollClanNear;

    public UIPanel scrollClanNear;
    public GameObject buttonStart;
    public GameObject buttonFinish;
    public GameObject buttonFinishDeactive;
    public GameObject buttonBackFinish;
    public GameObject buttonBackClan;
    public GameObject fxLoading;

    public GameObject panelRequest;
    public GameObject buttonRequestActive;
    public GameObject buttonRequestDeactive;

    public GameObject panelClanInvite;
    public GameObject buttonInviteClan;
    public GameObject buttonGloryChest;
    public UILabel lStatusTime;

    public UI2DSprite clanObject;
    public UILabel lIndexClan;
    public UISprite avatarClan;
    public UILabel lNameClan;
    public UILabel lMemberClan;
    public UILabel lScoreClan;

    public GameObject panelInvite;
    public UISprite sBackgroundInviteMember;
    public ItemFindMemberClan itemInviteMember;
    public GameObject buttonFindInviteMember;
    public GameObject buttonFindInviteMemberDeactive;
    public GameObject buttonSendInviteMember;
    public GameObject buttonSendInviteMemberDeactive;
    public UIInput inputFindMember;

    public GameObject worldObject;
    public UIPanel scrollWorld;
    public UIScrollView scrollviewWorld;

    public UILabel lResearch;
    public UILabel lSearchValue;
    public UIInput buttonSearch;
    public UIInput inputButtonChat;

    private bool isOpen;
    public UIPanel scrollMyClan;
    public UIScrollView scrollviewMyClan;
    public GameObject groupButtonClanObject;

    private Vector2 tempVector2;
    private Vector4 tempVector4;

    public GameObject popupHaveToSetMaster;
    public GameObject popupConfirm;
    public UILabel textConfirm;
    public UILabel textConfirmMember;

    public GameObject chatObject;
    public GameObject buttonChat;
    public GameObject buttonLog;
    public UIPanel scrollViewChatClan;
    public UIPanel scrollViewChatWorld;

    public GameObject lNoClanSuggest;

    public UILabel textPopupOk;
    public UILabel lNoClanTopWorld;

    //public List<ItemClanSuggest> listItemClanSuggest;

    private void OnEnable()
    {
        panelLoading.SetActive(false);
        lNeedLevel.text = "- " + I2.Loc.ScriptLocalization.pass_level.Replace("%{level}", "14");
        lNeedLevel2.text = lNeedLevel.text;
        readyClickBack = true;
        listClanTopWorld.Clear();
        ci = null;
        for (int i = 0; i < scrollMyClan.transform.GetChild(0).childCount; i++)
        {
            scrollMyClan.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < scrollWorld.transform.GetChild(0).childCount; i++)
        {
            scrollWorld.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        //for (int i = 0; i < listItemClanSuggest.Count; i++)
        //{
        //    listItemClanSuggest[i].gameObject.SetActive(false);
        //}
        for (int i = 0; i < scrollClanNear.transform.GetChild(0).childCount; i++)
        {
            scrollClanNear.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        buttonChat.SetActive(false);
        buttonLog.SetActive(false);
        gBottomChat.SetActive(false);
        gTopChat.SetActive(false);
        fromInvite = false;
        confirmSaveChange.SetActive(false);
        fxLoading.SetActive(false);
        lNoClanTopWorld.gameObject.SetActive(false);
        buttonFindInviteMemberDeactive.SetActive(false);
        buttonSendInviteMemberDeactive.SetActive(false);
        panelInvite.SetActive(false);
        buttonRequestActive.SetActive(false);
        buttonRequestDeactive.SetActive(true);
        bgChooseAvatar.SetActive(false);
        fxLoadingScrollClanNear.SetActive(false);
        buttonBackClan.SetActive(true);
        buttonBackFinish.SetActive(false);
        buttonStart.SetActive(true);
        buttonFinish.SetActive(false);
        buttonFinishDeactive.SetActive(false);
        contentMyClan.SetActive(false);
        panelClanInfo.SetActive(false);
        clanObject.gameObject.SetActive(false);
        DeactiveListButtonChoose(0);
        bCloseSetMasterPopup();
        isOpen = true;
        buttonJoinDeactiveClanInfo.SetActive(false);
        buttonJoinClanInfo.gameObject.SetActive(false);

        panelClanInvite.SetActive(false);
        buttonInviteClan.SetActive(false);
        buttonGloryChest.SetActive(false);
        lStatusTime.text = "00:00:00";
        lDontHaveClan.gameObject.SetActive(false);
        sBtnChat.spriteName = UNSELECTED_SPRITE_NAME;
        loadedMyClan = false;
        needReloadMyClan = true;
        loadedWorldClan = false;
        needReloadWorldClan = true;
        gRequestChat.SetActive(false);
        gRequestChatOffTime.SetActive(false);
        gShopClan.SetActive(false);
        gPanelMedalTop.SetActive(false);
        bChat();
        lNoClanSuggest.SetActive(false);
        lResearch.text = I2.Loc.ScriptLocalization.search_for + ": ";
        CachePvp.TypeMemberClan = CachePvp.HAS_NO_CLAN;
        new CSMyClan().Send();
        MessageDispatcher.AddListener(EventName.Clan.LoadChat.ToString(), OnLoadChat, true);
        MessageDispatcher.AddListener(EventName.Clan.LoadedChatError.ToString(), OnLoadedChatError, true);
        MessageDispatcher.AddListener(EventName.Clan.LoadMyClan.ToString(), OnLoadMyClan, true);
        MessageDispatcher.AddListener(EventName.Clan.LoadedSuggestClan.ToString(), OnLoadedSuggestClan, true);
        MessageDispatcher.AddListener(EventName.Clan.CreatedClan.ToString(), OnCreatedClan, true);
        MessageDispatcher.AddListener(EventName.Clan.LeaveClan.ToString(), OnLeaveClan, true);
        MessageDispatcher.AddListener(EventName.Clan.LoadClanInvite.ToString(), OnLoadClanInvite, true);
        MessageDispatcher.AddListener(EventName.Clan.LoadJoinRequest.ToString(), OnLoadJoinRequest, true);
        MessageDispatcher.AddListener(EventName.Clan.AcceptedClanInvitation.ToString(), OnAcceptedClanInvitation, true);
        MessageDispatcher.AddListener(EventName.Clan.RejectedClanInvitation.ToString(), OnRejectedClanInvitation, true);
        MessageDispatcher.AddListener(EventName.Clan.KickClan.ToString(), OnKickClan, true);
        MessageDispatcher.AddListener(EventName.Clan.MyClanRequest.ToString(), OnMyClanRequest, true);
        MessageDispatcher.AddListener(EventName.Clan.PlayerInfo.ToString(), OnPlayerInfo, true);
        MessageDispatcher.AddListener(EventName.Clan.ClanInvitation.ToString(), OnClanInvitation, true);
        MessageDispatcher.AddListener(EventName.Clan.GrantViceMaster.ToString(), OnGrantViceMaster, true);
        MessageDispatcher.AddListener(EventName.Clan.GrantMaster.ToString(), OnGrantMaster, true);
        MessageDispatcher.AddListener(EventName.Clan.RevokeViceMaster.ToString(), OnRevokeViceMaster, true);
        MessageDispatcher.AddListener(EventName.Clan.LoadListClanTopWorld.ToString(), OnLoadListClanTopWorld, true);
        MessageDispatcher.AddListener(EventName.Clan.LoadedSearchClan.ToString(), OnLoadedSearchClan, true);
        MessageDispatcher.AddListener(EventName.Clan.RequestedJoinClan.ToString(), OnRequestJoinClan, true);
        MessageDispatcher.AddListener(EventName.Clan.EditedClan.ToString(), OnEditedClan, true);
        MessageDispatcher.AddListener(EventName.Clan.ClanMember.ToString(), OnClanMember, true);
        MessageDispatcher.AddListener(EventName.GloryChest.TournamentInfo.ToString(), OnTournamentInfo, true);
        MessageDispatcher.AddListener(EventName.Clan.ShowButtonRequestChat.ToString(), OnShowButtonRequestChat, true);
        MessageDispatcher.AddListener(EventName.Clan.ChangeTopChat.ToString(), OnChangeTopChat, true);
        MessageDispatcher.AddListener(EventName.Clan.ChangeBottomChat.ToString(), OnChangeBottomChat, true);
        MessageDispatcher.AddListener(EventName.Clan.ChangeMedal.ToString(), OnChangeMedal, true);
        MessageDispatcher.AddListener(EventName.Clan.CSDonate.ToString(), OnCSDonate, true);
        MessageDispatcher.AddListener(EventName.Clan.SCDonate.ToString(), OnSCDonate, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Clan.LoadChat.ToString(), OnLoadChat, true);
        MessageDispatcher.RemoveListener(EventName.Clan.LoadedChatError.ToString(), OnLoadedChatError, true);
        MessageDispatcher.RemoveListener(EventName.Clan.LoadMyClan.ToString(), OnLoadMyClan, true);
        MessageDispatcher.RemoveListener(EventName.Clan.LoadedSuggestClan.ToString(), OnLoadedSuggestClan, true);
        MessageDispatcher.RemoveListener(EventName.Clan.CreatedClan.ToString(), OnCreatedClan, true);
        MessageDispatcher.RemoveListener(EventName.Clan.LeaveClan.ToString(), OnLeaveClan, true);
        MessageDispatcher.RemoveListener(EventName.Clan.LoadClanInvite.ToString(), OnLoadClanInvite, true);
        MessageDispatcher.RemoveListener(EventName.Clan.LoadJoinRequest.ToString(), OnLoadJoinRequest, true);
        MessageDispatcher.RemoveListener(EventName.Clan.AcceptedClanInvitation.ToString(), OnAcceptedClanInvitation, true);
        MessageDispatcher.RemoveListener(EventName.Clan.KickClan.ToString(), OnKickClan, true);
        MessageDispatcher.RemoveListener(EventName.Clan.MyClanRequest.ToString(), OnMyClanRequest, true);
        MessageDispatcher.RemoveListener(EventName.Clan.PlayerInfo.ToString(), OnPlayerInfo, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ClanInvitation.ToString(), OnClanInvitation, true);
        MessageDispatcher.RemoveListener(EventName.Clan.GrantViceMaster.ToString(), OnGrantViceMaster, true);
        MessageDispatcher.RemoveListener(EventName.Clan.GrantMaster.ToString(), OnGrantMaster, true);
        MessageDispatcher.RemoveListener(EventName.Clan.RevokeViceMaster.ToString(), OnRevokeViceMaster, true);
        MessageDispatcher.RemoveListener(EventName.Clan.LoadListClanTopWorld.ToString(), OnLoadListClanTopWorld, true);
        MessageDispatcher.RemoveListener(EventName.Clan.LoadedSearchClan.ToString(), OnLoadedSearchClan, true);
        MessageDispatcher.RemoveListener(EventName.Clan.RejectedClanInvitation.ToString(), OnRejectedClanInvitation, true);
        MessageDispatcher.RemoveListener(EventName.Clan.RequestedJoinClan.ToString(), OnRequestJoinClan, true);
        MessageDispatcher.RemoveListener(EventName.Clan.EditedClan.ToString(), OnEditedClan, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ClanMember.ToString(), OnClanMember, true);
        MessageDispatcher.RemoveListener(EventName.GloryChest.TournamentInfo.ToString(), OnTournamentInfo, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ShowButtonRequestChat.ToString(), OnShowButtonRequestChat, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ChangeTopChat.ToString(), OnChangeTopChat, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ChangeBottomChat.ToString(), OnChangeBottomChat, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ChangeMedal.ToString(), OnChangeMedal, true);
        MessageDispatcher.RemoveListener(EventName.Clan.CSDonate.ToString(), OnCSDonate, true);
        MessageDispatcher.RemoveListener(EventName.Clan.SCDonate.ToString(), OnSCDonate, true);
    }

    private void OnSCDonate(IMessage rMessage)
    {
        if (wait5s != null)
        {
            StopCoroutine(wait5s);
        }
        panelLoading.SetActive(false);
    }

    IEnumerator wait5s;

    private void OnCSDonate(IMessage rMessage)
    {
        panelLoading.SetActive(true);
        if (wait5s != null)
        {
            StopCoroutine(wait5s);
        }
        wait5s = Wait5s();
        StartCoroutine(wait5s);
    }

    IEnumerator Wait5s()
    {
        yield return new WaitForSecondsRealtime(5);
        if (wait5s != null)
        {
            StopCoroutine(wait5s);
        }
        panelLoading.SetActive(false);
    }

    private void OnChangeMedal(IMessage rMessage)
    {
        lNumMedal.text = GameContext.FormatNumber(CachePvp.Medal);
    }

    private void OnChangeBottomChat(IMessage rMessage)
    {
        if (CachePvp.bottomChat > 0)
        {
            gBottomChat.SetActive(true);
            lBottomChat.text = CachePvp.bottomChat.ToString();
        }
        else
        {
            gBottomChat.SetActive(false);
        }
    }

    private void OnChangeTopChat(IMessage rMessage)
    {
        if (CachePvp.topChat > 0)
        {
            gTopChat.SetActive(true);
            lTopChat.text = CachePvp.topChat.ToString();
        }
        else
        {
            gTopChat.SetActive(false);
        }
    }

    private int timeRemainingDonate;
    IEnumerator countDown2MinutesDonate;
    int targetDonate;

    private void OnShowButtonRequestChat(IMessage rMessage)
    {
        int time = (int)rMessage.Data;
        if (time <= 0)
        {
            gRequestChatOffTime.SetActive(true);
            gRequestChat.SetActive(false);
            if (countDown2MinutesDonate != null)
            {
                StopCoroutine(countDown2MinutesDonate);
            }
        }
        else
        {
            gRequestChat.SetActive(true);
            gRequestChatOffTime.SetActive(false);
            timeRemainingDonate = time;
            string t = (timeRemainingDonate / 3600).ToString("00") + ":" + ((timeRemainingDonate % 3600) / 60).ToString("00") + ":" + (timeRemainingDonate % 60).ToString("00");
            lStatusTimeDonate.text = t;
            if (countDown2MinutesDonate != null)
            {
                StopCoroutine(countDown2MinutesDonate);
            }
            if (timeRemainingDonate > 0)
            {
                countDown2MinutesDonate = CountDown2MinutesDonate();
                StartCoroutine(countDown2MinutesDonate);
            }
        }
    }

    IEnumerator CountDown2MinutesDonate()
    {
        TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        targetDonate = (int)span.TotalSeconds + timeRemainingDonate;
        while (timeRemainingDonate >= 0)
        {
            lStatusTimeDonate.text = (timeRemainingDonate / 3600).ToString("00") + ":" + ((timeRemainingDonate % 3600) / 60).ToString("00") + ":" + (timeRemainingDonate % 60).ToString("00");
            TimeSpan temp_span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            timeRemainingDonate = targetDonate - (int)temp_span.TotalSeconds;
            yield return new WaitForSecondsRealtime(1);
        }
        lStatusTimeDonate.text = "00:00:00";
        gRequestChat.SetActive(false);
        gRequestChatOffTime.SetActive(true);
    }

    private int timeRemaining;
    IEnumerator countDown2Minutes;
    int target;

    private void OnTournamentInfo(IMessage rMessage)
    {
        SCTournamentInfo tournament = rMessage.Data as SCTournamentInfo;
        if (tournament.status == SCTournamentInfo.SUCCESS)
        {
            timeRemaining = (int)tournament.remainTime;
            string time = (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00");
            lStatusTime.text = time;
            if (countDown2Minutes != null)
            {
                StopCoroutine(countDown2Minutes);
            }
            if (timeRemaining > 0)
            {
                countDown2Minutes = CountDown2Minutes();
                StartCoroutine(countDown2Minutes);
            }
        }
    }

    public void ShowGloryChest()
    {
        if (CachePvp.TypeMemberClan == CachePvp.HAS_NO_CLAN)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_join_team_to_open, false, 1.5f);
            return;
        }
        PopupManager.Instance.ShowGloryChestPopup();
    }

    IEnumerator CountDown2Minutes()
    {
        TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        target = (int)span.TotalSeconds + timeRemaining;
        while (timeRemaining >= 0)
        {
            lStatusTime.text = (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00");
            TimeSpan temp_span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            timeRemaining = target - (int)temp_span.TotalSeconds;
            yield return new WaitForSecondsRealtime(1);
        }
        lStatusTime.text = "00:00:00";
    }

    private void OnLoadedSearchClan(IMessage rMessage)
    {
        List<ClanInfo> clans = rMessage.Data as List<ClanInfo>;
        if (clans.Count == 0)
        {
            lNoClanTopWorld.gameObject.SetActive(true);
        }
        else
        {
            lNoClanTopWorld.gameObject.SetActive(false);
        }
    }

    private void OnClanMember(IMessage rMessage)
    {
        long clanId = (long)rMessage.Recipient;
        if (clanId == m_ci_view.id)
        {
            fxLoading.SetActive(false);
            MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedClanInfo.ToString(), rMessage.Data as List<Player>, 0);
        }
    }

    private void OnEditedClan(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCClanEdit.SUCCESS)
        {
            ClanInfo thisci = rMessage.Data as ClanInfo;
            ci.type = thisci.type;
            ci.requiredLevel = thisci.requiredLevel;
            oldIsPrivateTeamType = isPrivateTeamType;
            oldLevelRequired = levelRequired;
            confirmSaveChange.SetActive(false);
        }
        else
        {
            if (status == SCClanEdit.CLAN_NOT_EXIST)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_not_exist, false, 1.5f);
            }
            else if (status == SCClanEdit.NOT_A_MASTER_CLAN)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_permission_error, false, 1.5f);
            }
            isPrivateTeamType = oldIsPrivateTeamType;
            levelRequired = oldLevelRequired;
            confirmSaveChange.SetActive(false);
        }
    }

    public const int JOINED_CLAN = 3;
    public const int JOIN_REQUEST_EXISTED = 4;
    public const int CLAN_NOT_EXIST = 10;
    public const int CLAN_FULL_MEMBER = 11;
    public const int NOT_ENOUGH_LEVEL = 20;

    private void OnRequestJoinClan(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        long id = (long)rMessage.Data;
        switch (status)
        {
            case SCClanJoinRequest.SUCCESS:
                if (!CachePvp.listClanIdRequest.Contains(id))
                {
                    CachePvp.listClanIdRequest.Add(id);
                }
                //for (int i = 0; i < listItemClanSuggest.Count; i++)
                //{
                //    if (listItemClanSuggest[i].m_clanInfo.id == id)
                //    {
                //        listItemClanSuggest[i].RefreshData();
                //        break;
                //    }
                //}
                break;
            case SCClanJoinRequest.CLAN_FULL_MEMBER:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_have_full_member, false, 1.5f);
                if (panelClanInfo.activeInHierarchy)
                {
                    buttonJoinClanInfo.gameObject.SetActive(true);
                    buttonJoinDeactiveClanInfo.SetActive(false);
                }
                break;
            case SCClanJoinRequest.JOINED_CLAN:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_had_in_clan, false, 1.5f);
                if (panelClanInfo.activeInHierarchy)
                {
                    buttonJoinClanInfo.gameObject.SetActive(true);
                    buttonJoinDeactiveClanInfo.SetActive(false);
                }
                break;
            case SCClanJoinRequest.JOIN_REQUEST_EXISTED:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_had_requested, false, 1.5f);
                if (panelClanInfo.activeInHierarchy)
                {
                    buttonJoinClanInfo.gameObject.SetActive(true);
                    buttonJoinDeactiveClanInfo.SetActive(false);
                }
                break;
            case SCClanJoinRequest.CLAN_NOT_EXIST:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_not_exist, false, 1.5f);
                if (panelClanInfo.activeInHierarchy)
                {
                    buttonJoinClanInfo.gameObject.SetActive(true);
                    buttonJoinDeactiveClanInfo.SetActive(false);
                }
                break;
            case SCClanJoinRequest.NOT_ENOUGH_LEVEL:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_level, false, 1.5f);
                if (panelClanInfo.activeInHierarchy)
                {
                    buttonJoinClanInfo.gameObject.SetActive(true);
                    buttonJoinDeactiveClanInfo.SetActive(false);
                }
                break;
            default:
                if (panelClanInfo.activeInHierarchy)
                {
                    buttonJoinClanInfo.gameObject.SetActive(true);
                    buttonJoinDeactiveClanInfo.SetActive(false);
                }
                break;
        }
    }

    private void OnLoadedChatError(IMessage rMessage)
    {
        fxLoading.SetActive(false);
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_load_chat_history_error, false, 1.5f);
    }

    private void OnRejectedClanInvitation(IMessage rMessage)
    {
        long clanId = (long)rMessage.Data;
        if (panelClanInfo.activeInHierarchy && m_ci_view.id == clanId)
        {
            panelClanInfo.SetActive(false);
        }
        for (int i = 0; i < listClanInvite.Count; i++)
        {
            if (listClanInvite[i].id == clanId)
            {
                listClanInvite.RemoveAt(i);
                break;
            }
        }
        if (listClanInvite.Count == 0)
        {
            buttonInviteClan.SetActive(false);
        }
        else
        {
            buttonInviteClan.SetActive(true);
        }
        MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedClanInvite.ToString(), listClanInvite, 0);
        //}
        //else
        //{
        //    PopupManagerCuong.Instance.ShowTextNotifiToast("Reject Error", false, 1.5f);
        //}
    }

    List<ClanInfo> listClanTopWorld = new List<ClanInfo>();

    private void OnLoadListClanTopWorld(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCClanListTopWorld.SUCCESS)
        {
            listClanTopWorld = rMessage.Data as List<ClanInfo>;
            if (sBtnWorld.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                StartCoroutine(Wait1FrameAfterLoadListTopWorld());
            }
            else
            {
                loadedWorldClan = true;
            }
        }
    }

    public void bJoinClan()
    {
        buttonJoinClanInfo.gameObject.SetActive(false);
        buttonJoinDeactiveClanInfo.SetActive(true);
        if (fromInvite)
        {
            new CSClanInvitationAccept(m_ci_view.id).Send();
        }
        else
        {
            new CSClanJoinRequest(m_ci_view.id).Send();
        }
    }

    private IEnumerator Wait1FrameAfterLoadListTopWorld()
    {
        yield return null;
        Debug.Log("Wait1FrameAfterLoadListTopWorld");
        MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedSearchClan.ToString(), listClanTopWorld, 0);
        fxLoading.SetActive(false);
        if (listClanTopWorld.Count == 0)
        {
            lNoClanTopWorld.gameObject.SetActive(true);
        }
        else
        {
            lNoClanTopWorld.gameObject.SetActive(false);
        }
    }

    private void OnRevokeViceMaster(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCClanRevokeViceMaster.SUCCESS)
        {
            string code = rMessage.Data.ToString();
            if (ci.viceMaster != null)
            {
                for (int i = 0; i < ci.viceMaster.Count; i++)
                {
                    if (ci.viceMaster[i].code.Equals(code))
                    {
                        ci.viceMaster[i].playerClan.type = CachePvp.CLAN_MEMBER;
                        ci.members.Add(ci.viceMaster[i]);
                        ci.viceMaster.RemoveAt(i);
                        break;
                    }
                }
            }

            if (sBtnMyClan.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                StartCoroutine(Wait1Frame());
            }
            else
            {
                needReloadMyClan = true;
            }
        }
    }

    private void OnGrantMaster(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCClanGrantMaster.SUCCESS)
        {
            Player tempP = ci.master;
            string code = rMessage.Data.ToString();
            if (ci.members != null)
            {
                for (int i = 0; i < ci.members.Count; i++)
                {
                    if (ci.members[i].code.Equals(code))
                    {
                        ci.members[i].playerClan.type = CachePvp.CLAN_MASTER;
                        ci.master = ci.members[i];
                        ci.members.RemoveAt(i);
                        break;
                    }
                }
            }
            if (ci.viceMaster != null)
            {
                for (int i = 0; i < ci.viceMaster.Count; i++)
                {
                    if (ci.viceMaster[i].code.Equals(code))
                    {
                        ci.viceMaster[i].playerClan.type = CachePvp.CLAN_MASTER;
                        ci.master = ci.viceMaster[i];
                        ci.viceMaster.RemoveAt(i);
                        break;
                    }
                }
            }
            tempP.playerClan.type = CachePvp.CLAN_MEMBER;
            ci.members.Add(tempP);
            CachePvp.TypeMemberClan = CachePvp.CLAN_MEMBER;
            if (sBtnMyClan.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                StartCoroutine(Wait1Frame());
            }
            else
            {
                needReloadMyClan = true;
            }
        }
    }

    private void OnGrantViceMaster(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCClanGrantViceMaster.SUCCESS)
        {
            string code = rMessage.Data.ToString();
            for (int i = 0; i < ci.members.Count; i++)
            {
                if (ci.members[i].code.Equals(code))
                {
                    ci.members[i].playerClan.type = CachePvp.CLAN_VICE_MASTER;
                    ci.viceMaster.Add(ci.members[i]);
                    ci.members.RemoveAt(i);
                    break;
                }
            }

            if (sBtnMyClan.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                StartCoroutine(Wait1Frame());
            }
            else
            {
                needReloadMyClan = true;
            }
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.success, true, 1.5f);
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.error, true, 1.5f);
        }
    }

    private void OnClanInvitation(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        switch (status)
        {
            case SCClanInvitation.SUCCESS:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.success, true, 1.5f);
                break;
            case SCClanInvitation.NOT_IN_CLAN:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_had_in_clan, false, 1.5f);
                break;
            case SCClanInvitation.IS_MEMBER:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_player_was_member_your_clan, false, 1.5f);
                break;
            case SCClanInvitation.MEMBER_HAS_CLAN:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_player_joined_other_clan, false, 1.5f);
                break;
            case SCClanInvitation.MEMBER_NOT_EXIST:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_player_not_exist, false, 1.5f);
                break;
            case SCClanInvitation.FULL_MEMBER:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_have_full_member, false, 1.5f);
                break;
            case SCClanInvitation.UNAUTHORITY:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_permission_error, false, 1.5f);
                break;
            default:
                break;
        }
    }

    Player playerFound;

    private void OnPlayerInfo(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (panelInvite.activeInHierarchy)
        {
            if (status == SCPlayerInfo.SUCCESS)
            {
                if (rMessage.Data != null)
                {
                    playerFound = rMessage.Data as Player;
                    itemInviteMember.SetData(playerFound);
                    sBackgroundInviteMember.height = 514;
                    itemInviteMember.gameObject.SetActive(true);
                    buttonFindInviteMember.SetActive(false);
                    buttonFindInviteMemberDeactive.SetActive(false);
                    buttonSendInviteMember.SetActive(true);
                    buttonSendInviteMemberDeactive.SetActive(false);
                }
                else
                {
                    sBackgroundInviteMember.height = 377;
                    itemInviteMember.gameObject.SetActive(false);
                    buttonFindInviteMember.SetActive(true);
                    buttonFindInviteMemberDeactive.SetActive(false);
                    buttonSendInviteMember.SetActive(false);
                    buttonSendInviteMemberDeactive.SetActive(false);
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_player_not_exist, false, 1.5f);
                }
            }
            else
            {
                sBackgroundInviteMember.height = 377;
                itemInviteMember.gameObject.SetActive(false);
                buttonFindInviteMember.SetActive(true);
                buttonFindInviteMemberDeactive.SetActive(false);
                buttonSendInviteMember.SetActive(false);
                buttonSendInviteMemberDeactive.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.error, false, 1.5f);
            }
        }
        else if (scrollViewChatClan.gameObject.activeInHierarchy || scrollViewChatWorld.gameObject.activeInHierarchy)
        {
            if (status == SCPlayerInfo.SUCCESS)
            {
                if (rMessage.Data != null)
                {
                    if (!PopupManager.Instance.IsProfilePopupActive())
                    {
                        PopupManager.Instance.ShowProfilePopup(rMessage.Data as Player);
                    }
                }
            }
        }
    }

    private void OnMyClanRequest(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCMyClanRequest.SUCCESS)
        {
            List<ClanInfo> list = rMessage.Data as List<ClanInfo>;
            CachePvp.listClanIdRequest.Clear();
            for (int i = 0; i < list.Count; i++)
            {
                CachePvp.listClanIdRequest.Add(list[i].id);
            }
        }
    }

    private void OnKickClan(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        switch (status)
        {
            case SCClanKickMember.SUCCESS:
                string code = rMessage.Data.ToString();
                if (ci.viceMaster != null)
                {
                    for (int i = 0; i < ci.viceMaster.Count; i++)
                    {
                        if (ci.viceMaster[i].code.Equals(code))
                        {
                            ci.viceMaster.RemoveAt(i);
                            ci.totalMember--;
                            break;
                        }
                    }
                }
                if (ci.members != null)
                {
                    for (int i = 0; i < ci.members.Count; i++)
                    {
                        if (ci.members[i].code.Equals(code))
                        {
                            ci.members.RemoveAt(i);
                            ci.totalMember--;
                            break;
                        }
                    }
                }
                if (sBtnMyClan.spriteName.Equals(SELECTED_SPRITE_NAME))
                {
                    StartCoroutine(Wait1Frame());
                }
                else
                {
                    needReloadMyClan = true;
                }
                break;
            case SCClanKickMember.CLAN_NOT_EXIST:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_not_exist, false, 1.5f);
                break;
            case SCClanKickMember.UNAUTHORITY:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_permission_error, false, 1.5f);
                break;
            case SCClanKickMember.NOT_CLAN_MEMBER:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_member_in_clan, false, 1.5f);
                break;
            default:
                break;
        }
    }

    private void OnAcceptedClanInvitation(IMessage rMessage)
    {
        Debug.Log("OnAcceptedClanInvitation");
        panelClanInvite.SetActive(false);
        if (sBtnMyClan.spriteName.Equals(SELECTED_SPRITE_NAME))
        {
            bBackFinish();
            clanObject.gameObject.SetActive(false);
            scrollMyClan.gameObject.SetActive(false);
            panelRequest.SetActive(false);
            popupConfirm.SetActive(false);
            contentMyClan.SetActive(false);
            bgChooseAvatar.SetActive(false);
            fxLoading.SetActive(true);
            fxLoadingScrollClanNear.SetActive(false);
            buttonInviteClan.SetActive(false);
        }
        new CSMyClan().Send();
    }

    List<Player> listPlayerJoinRequest = new List<Player>();

    public void ConfirmJoinRequest(string code)
    {
        for (int i = 0; i < listPlayerJoinRequest.Count; i++)
        {
            if (listPlayerJoinRequest[i].code.Equals(code))
            {
                listPlayerJoinRequest.RemoveAt(i);
                break;
            }
        }
        if (listPlayerJoinRequest.Count != 0)
        {
            buttonRequestDeactive.SetActive(false);
            buttonRequestActive.SetActive(true);
        }
        else
        {
            buttonRequestDeactive.SetActive(true);
            buttonRequestActive.SetActive(false);
        }
    }

    private void OnLoadJoinRequest(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCClanMemberRequest.SUCCESS)
        {
            if (sBtnMyClan.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                listPlayerJoinRequest = rMessage.Data as List<Player>;
                StartCoroutine(Wait1FrameAfterJoinRequest(listPlayerJoinRequest));
            }
        }
    }

    private IEnumerator Wait1FrameAfterJoinRequest(List<Player> list)
    {
        yield return null;
        Debug.Log("Wait1FrameAfterJoinRequest");
        MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedJoinRequest.ToString(), list, 0);
        if (list.Count != 0)
        {
            buttonRequestDeactive.SetActive(false);
            buttonRequestActive.SetActive(true);
        }
        else
        {
            buttonRequestDeactive.SetActive(true);
            buttonRequestActive.SetActive(false);
        }
    }

    List<ClanInfo> listClanInvite = new List<ClanInfo>();

    private void OnLoadClanInvite(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCMyClanInvitation.SUCCESS)
        {
            listClanInvite.Clear();
            List<ClanInfo> list = rMessage.Data as List<ClanInfo>;
            for (int i = 0; i < list.Count; i++)
            {
                listClanInvite.Add(list[i]);
            }
            if (list.Count == 0)
            {
                buttonInviteClan.SetActive(false);
            }
            else
            {
                buttonInviteClan.SetActive(true);
                //MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedClanInvite.ToString(), list, 0);
            }
        }
    }

    private void OnLeaveClan(IMessage rMessage)
    {
        int status = (int)rMessage.Data;
        switch (status)
        {
            case SCClanLeave.SUCCESS:
                CachePvp.TypeMemberClan = CachePvp.HAS_NO_CLAN;
                bgChooseAvatar.SetActive(false);
                buttonBackClan.SetActive(true);
                buttonBackFinish.SetActive(false);
                buttonStart.SetActive(true);
                buttonFinish.SetActive(false);
                buttonFinishDeactive.SetActive(false);
                needReloadMyClan = true;
                DeactiveListButtonChoose(0);
                bCloseSetMasterPopup();
                isOpen = true;
                sBtnMyClan.spriteName = UNSELECTED_SPRITE_NAME;
                bMyClan();
                lNoClanSuggest.SetActive(false);
                fxLoadingScrollClanNear.SetActive(true);
                //for (int i = 0; i < listItemClanSuggest.Count; i++)
                //{
                //    listItemClanSuggest[i].gameObject.SetActive(false);
                //}
                for (int i = 0; i < scrollClanNear.transform.GetChild(0).childCount; i++)
                {
                    scrollClanNear.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
                }
                new CSClanSuggestion().Send();
                break;
            case SCClanLeave.CLAN_NOT_EXIST:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_not_exist, false, 1.5f);
                break;
            case SCClanLeave.GRANT_MASTER_BEFORE_LEAVE:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_need_set_to_master, false, 1.5f);
                break;
            case SCClanLeave.NOT_A_MEMBER:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_not_member_in_clan, false, 1.5f);
                break;
            default:
                break;
        }
    }

    private void OnCreatedClan(IMessage rMessage)
    {
        //throw new NotImplementedException();
        int status = (int)rMessage.Recipient;
        if (status == SCClanCreate.SUCCESS)
        {
            CachePvp.SetGem(CachePvp.GetGem() - CachePvp.sCClientConfig.gemToCreateTeam, FirebaseLogSpaceWar.CreateClan_Why, FirebaseLogSpaceWar.Unknown_why);
            //CachePvp.Gem -= CachePvp.sCClientConfig.gemToCreateTeam;
            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.AddGemTop();
            }
            ci = rMessage.Data as ClanInfo;
            CachePvp.TypeMemberClan = CachePvp.CLAN_MASTER;
            buttonTeamMail.SetActive(true);
            PopupManager.Instance.ShowClanCongratulationPopup(ci.avatar, ci.name);
            new CSMyClan().Send();
            if (sBtnMyClan.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                bBackFinish();
                clanObject.gameObject.SetActive(false);
                scrollMyClan.gameObject.SetActive(false);
                panelRequest.SetActive(false);
                popupConfirm.SetActive(false);
                contentMyClan.SetActive(false);
                bgChooseAvatar.SetActive(false);
                fxLoading.SetActive(false);
                fxLoadingScrollClanNear.SetActive(false);
                buttonInviteClan.SetActive(false);
            }
            needReloadWorldClan = true;
        }
        else
        {
            switch (status)
            {
                case SCClanCreate.NAME_IS_EMPTY:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_pvp_character.Replace("%{number_character}", "3"), false, 1.5f);
                    break;
                case SCClanCreate.LENGTH_NAME_INVALID:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_name_invalid, false, 1.5f);
                    break;
                case SCClanCreate.JOINED_CLAN:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_had_in_clan, false, 1.5f);
                    break;
                case SCClanCreate.NOT_ENOUGHT_LEVEL:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", "14"), false, 1.5f);
                    break;
                case SCClanCreate.NOT_ENOUGHT_GEM:
                    //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_gem, false, 1.5f);
                    PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
                    break;
                default:
                    string message = rMessage.Recipient as string;
                    PopupManagerCuong.Instance.ShowTextNotifiToast(message, false, 1.5f);
                    break;
            }
            buttonFinish.SetActive(true);
            buttonFinishDeactive.SetActive(false);
        }
    }

    private void OnLoadedSuggestClan(IMessage rMessage)
    {
        fxLoadingScrollClanNear.SetActive(false);
        List<ClanInfo> listClans = rMessage.Data as List<ClanInfo>;
        MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedSuggestClanForList.ToString(), listClans, 0);
        //for (int i = 0; i < listClans.Count; i++)
        //{
        //    listItemClanSuggest[i].gameObject.SetActive(true);
        //    listItemClanSuggest[i].SetData(listClans[i]);
        //}
        //scrollClanNear.GetComponent<UIScrollView>().ResetPosition();
        if (listClans.Count == 0)
        {
            lNoClanSuggest.SetActive(true);
        }
        else
        {
            lNoClanSuggest.SetActive(false);
        }
    }

    public void bInviteClan()
    {
        panelClanInvite.SetActive(true);
        if (listClanInvite.Count > 0)
        {
            MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedClanInvite.ToString(), listClanInvite, 0);
        }
    }

    public void bBackClanInvite()
    {
        panelClanInvite.SetActive(false);
    }

    void RefreshInfoClan()
    {
        lIndexClan.text = ci.rank.ToString();
        if ((ci.avatar + 2) < 10)
        {
            avatarClan.spriteName = "avt_Avt_0" + (ci.avatar + 2);
        }
        else
        {
            avatarClan.spriteName = "avt_Avt_" + (ci.avatar + 2);
        }
        lNameClan.text = ci.name.ToString();
        lMemberClan.text = ci.totalMember + "/" + ci.size;
        lScoreClan.text = GameContext.FormatNumber((int)ci.score);
    }

    ClanInfo ci;

    private void OnLoadMyClan(IMessage rMessage)
    {
        Debug.Log("LoadedMyClan");
        ci = rMessage.Data as ClanInfo;
        CachePvp.clanInfo = ci;
        if (ci != null)
        {
            CachePvp.clanId = ci.id;
            oldIsPrivateTeamType = ci.type == 1 ? true : false; //0: open, 1: close
            oldLevelRequired = ci.requiredLevel;
            isPrivateTeamType = oldIsPrivateTeamType;
            levelRequired = oldLevelRequired;

            lTeamType.text = isPrivateTeamType ? I2.Loc.ScriptLocalization.close : I2.Loc.ScriptLocalization.open;
            lTeamTypeClanObject.text = lTeamType.text;
            lTeamTypeConfirm.text = lTeamType.text;

            lRequiredLevel.text = levelRequired.ToString();
            lRequiredLevelClanObject.text = lRequiredLevel.text;
            lRequiredLevelConfirm.text = lRequiredLevel.text;
            CachePvp.TypeMemberClan = CachePvp.CLAN_MEMBER;
            buttonGloryChest.SetActive(true);
            buttonTeamMail.SetActive(false);
            if (CachePvp.Code.Equals(ci.master.code))
            {
                CachePvp.TypeMemberClan = CachePvp.CLAN_MASTER;
            }
            else
            {
                if (ci.viceMaster != null)
                {
                    for (int i = 0; i < ci.viceMaster.Count; i++)
                    {
                        if (ci.viceMaster[i].code.Equals(CachePvp.Code))
                        {
                            CachePvp.TypeMemberClan = CachePvp.CLAN_VICE_MASTER;
                            break;
                        }
                    }
                }
            }
            new CSTournamentInfo().Send();
            Debug.Log("CachePvp.TypeMemberClan : " + CachePvp.TypeMemberClan);
            if (CachePvp.TypeMemberClan == CachePvp.CLAN_MASTER || CachePvp.TypeMemberClan == CachePvp.CLAN_VICE_MASTER)
            {
                new CSClanMemberRequest(ci.id).Send();
                buttonTeamMail.SetActive(true);
            }
            RefreshInfoClan();
            if (sBtnMyClan.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                buttonInviteClan.SetActive(false);
                panelClanInfo.SetActive(false);
                panelClanInvite.SetActive(false);
                fxLoading.SetActive(false);
                scrollMyClan.gameObject.SetActive(true);
                panelRequest.SetActive(false);
                popupConfirm.SetActive(false);
                contentMyClan.SetActive(false);
                bgChooseAvatar.SetActive(false);
                clanObject.gameObject.SetActive(true);
                clanObject.height = 95;
                isOpen = false;
                groupButtonClanObject.SetActive(false);
                StartCoroutine(Wait1Frame());
                StartCoroutine(WaitLoadHeight());
            }
            else if (sBtnChat.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                if (lDontHaveClan.gameObject.activeInHierarchy)
                {
                    sBtnChat.spriteName = UNSELECTED_SPRITE_NAME;
                    bChat();
                }
            }
            else
            {
                loadedMyClan = true;
            }
        }
        else
        {
            if (sBtnMyClan.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                contentMyClan.SetActive(true);
                fxLoading.SetActive(false);
                fxLoadingScrollClanNear.SetActive(true);
                StartCoroutine(WaitLoadHeight());
            }
            CachePvp.TypeMemberClan = CachePvp.HAS_NO_CLAN;
            buttonGloryChest.SetActive(false);
            gShopClan.SetActive(false);
            buttonChat.SetActive(false);
            buttonLog.SetActive(false);
            //for (int i = 0; i < listItemClanSuggest.Count; i++)
            //{
            //    listItemClanSuggest[i].gameObject.SetActive(false);
            //}
            lNoClanSuggest.SetActive(false);
            for (int i = 0; i < scrollClanNear.transform.GetChild(0).childCount; i++)
            {
                scrollClanNear.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            }
            new CSMyClanRequest().Send();
            new CSClanSuggestion().Send();
            new CSMyClanInvitation().Send();
        }
    }

    bool isChatClan;

    private void LoadedMemberClan(IMessage rMessage)
    {
        fxLoading.SetActive(false);
    }

    public void LoadedChat()
    {
        fxLoading.SetActive(false);
    }

    private void OnLoadChat(IMessage rMessage)
    {
        for (int i = 0; i < scrollViewChatClan.transform.GetChild(0).childCount; i++)
        {
            scrollViewChatClan.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < scrollViewChatWorld.transform.GetChild(0).childCount; i++)
        {
            scrollViewChatWorld.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        //fxLoading.SetActive(false);
        int type = (int)rMessage.Recipient;
        List<Chat> list = rMessage.Data as List<Chat>;
        MessageDispatcher.SendMessage(gameObject, type, EventName.Clan.LoadedChat.ToString(), list, 0);
        if (sBtnChat.spriteName.Equals(SELECTED_SPRITE_NAME))
        {
            buttonLog.SetActive(true);
            gShopClan.SetActive(true);
            buttonChat.SetActive(true);
        }
    }

    private void LoadedChatWorld(IMessage rMessage)
    {
        fxLoading.SetActive(false);
    }

    public void bRequest()
    {
        panelRequest.SetActive(true);
        StartCoroutine(Wait1FrameAfterJoinRequest(listPlayerJoinRequest));
    }

    public void OnChangeInputInviteMember()
    {
        sBackgroundInviteMember.height = 377;
        itemInviteMember.gameObject.SetActive(false);
        buttonFindInviteMember.SetActive(true);
        buttonFindInviteMemberDeactive.SetActive(false);
        buttonSendInviteMember.SetActive(false);
        buttonSendInviteMemberDeactive.SetActive(false);
    }

    public void bFindMemberInvite()
    {
        //sBackgroundInviteMember.height = 514;
        itemInviteMember.gameObject.SetActive(false);
        buttonFindInviteMember.SetActive(false);
        buttonFindInviteMemberDeactive.SetActive(true);
        buttonSendInviteMember.SetActive(false);
        buttonSendInviteMemberDeactive.SetActive(false);
        if (!string.IsNullOrEmpty(inputFindMember.value))
        {
            new CSPlayerInfo(inputFindMember.value).Send();
        }
        else
        {
            sBackgroundInviteMember.height = 377;
            itemInviteMember.gameObject.SetActive(false);
            buttonFindInviteMember.SetActive(true);
            buttonFindInviteMemberDeactive.SetActive(false);
            buttonSendInviteMember.SetActive(false);
            buttonSendInviteMemberDeactive.SetActive(false);
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_code_invalid, false, 1.5f);
        }
    }

    public void bCancelInviteMember()
    {
        panelInvite.SetActive(false);
    }

    public void bSendMemberInvite()
    {
        buttonSendInviteMemberDeactive.SetActive(true);
        buttonSendInviteMember.SetActive(false);
        new CSClanInvitation(playerFound.code).Send();
    }

    public void bInvite()
    {
        if (!panelInvite.activeInHierarchy)
        {
            sBackgroundInviteMember.height = 377;
            panelInvite.SetActive(true);
            inputFindMember.value = "";
            itemInviteMember.gameObject.SetActive(false);
            buttonFindInviteMember.SetActive(true);
            buttonFindInviteMemberDeactive.SetActive(false);
            buttonSendInviteMember.SetActive(false);
            buttonSendInviteMemberDeactive.SetActive(false);
        }
    }

    public void ShowSetMasterPopup(int type)
    {
        switch (type)
        {
            case 0:
                //cant kick master
                textPopupOk.text = I2.Loc.ScriptLocalization.msg_you_have_to_set_master;
                break;
            case 1:
                //clan cant have vice > 2
                textPopupOk.text = I2.Loc.ScriptLocalization.msg_clan_cant_have_more_vice.Replace("%{numbers}", "2");
                break;
            default:
                break;
        }
        popupHaveToSetMaster.SetActive(true);
    }

    public void bQuit()
    {
        if (CachePvp.TypeMemberClan == CachePvp.CLAN_MASTER && ci.totalMember > 1)
        {
            ShowSetMasterPopup(0);
        }
        else
        {
            CachePvp.memberPlayer = CachePvp.MyInfo;
            CachePvp.typeActionMemberClan = 4;
            ShowPopupConfirm();
        }
    }

    public void bCloseSetMasterPopup()
    {
        popupHaveToSetMaster.SetActive(false);
    }

    public void bOkConfirm()
    {
        //set to vice : 0
        //set to member : 1
        //set to master : 2
        //kick : 3
        //quit : 4
        switch (CachePvp.typeActionMemberClan)
        {
            case 0:
                //textConfirm.text = "set to vice";
                //listMyClanPopulator.SetedToViceClan();
                new CSClanGrantViceMaster(CachePvp.memberPlayer.code).Send();
                break;
            case 1:
                //textConfirm.text = "set to member";
                //listMyClanPopulator.SetedToMemberClan();
                new CSClanRevokeViceMaster(CachePvp.memberPlayer.code).Send();
                break;
            case 2:
                //textConfirm.text = "set to master";
                //listMyClanPopulator.SetedToMasterClan();
                new CSClanGrantMaster(CachePvp.memberPlayer.code).Send();
                break;
            case 3:
                //textConfirm.text = "kick";
                //listMyClanPopulator.KickedMemberClan();
                new CSClanKickMember(CachePvp.memberPlayer.code).Send();
                break;
            case 4:
                //textConfirm.text = "quit";
                //listMyClanPopulator.QuitedClan();
                new CSClanLeave().Send();
                break;
            default:
                break;
        }
        popupConfirm.SetActive(false);
    }

    public void ShowPopupConfirm()
    {
        //set to vice : 0
        //set to member : 1
        //set to master : 2
        //kick : 3
        //quit : 4
        popupConfirm.SetActive(true);
        switch (CachePvp.typeActionMemberClan)
        {
            case 0:
                textConfirm.text = I2.Loc.ScriptLocalization.msg_upgrading_member;
                textConfirmMember.text = "[177e18]" + I2.Loc.ScriptLocalization.clan_vice + "[-]";
                break;
            case 1:
                textConfirm.text = I2.Loc.ScriptLocalization.msg_downgrading_member;
                textConfirmMember.text = I2.Loc.ScriptLocalization.clan_member;
                break;
            case 2:
                textConfirm.text = I2.Loc.ScriptLocalization.msg_upgrading_member;
                textConfirmMember.text = "[e28d26]" + I2.Loc.ScriptLocalization.clan_master + "[-]";
                break;
            case 3:
                textConfirm.text = I2.Loc.ScriptLocalization.msg_kick_member.Replace("%{member}", string.IsNullOrEmpty(CachePvp.memberPlayer.name) ? CachePvp.memberPlayer.code : CachePvp.memberPlayer.name);
                textConfirmMember.text = "";
                break;
            case 4:
                textConfirm.text = I2.Loc.ScriptLocalization.msg_quit_clan;
                textConfirmMember.text = "";
                break;
            default:
                break;
        }
    }

    public void bBackPanelJoinRequest()
    {
        panelRequest.SetActive(false);
    }

    public void bNoConfirm()
    {
        popupConfirm.SetActive(false);
    }

    bool needReloadMyClan = true;
    bool loadedMyClan;

    bool needReloadWorldClan = true;
    bool loadedWorldClan;

    float firstHeightMyClan;
    float firstCenterScrollNear;

    IEnumerator Wait1Frame()
    {
        yield return null;
        MessageDispatcher.SendMessage(gameObject, EventName.Clan.LoadedMyClan.ToString(), ci, 0);
        //StartCoroutine(WaitLoadHeight());
        yield return new WaitForSecondsRealtime(0.17f);
        isOpen = true;
        bSettingClan();
    }

    public void bSettingClan()
    {
        if (isOpen)
        {
            clanObject.height = 95;
            isOpen = false;
            tempVector4.x = scrollMyClan.baseClipRegion.x;
            tempVector4.y = 234f;
            tempVector4.z = scrollMyClan.baseClipRegion.z;
            tempVector4.w = firstHeightMyClan;
            scrollMyClan.baseClipRegion = tempVector4;
            groupButtonClanObject.SetActive(false);
        }
        else
        {
            clanObject.height = 348;
            isOpen = true;
            tempVector4.x = scrollMyClan.baseClipRegion.x;
            tempVector4.y = 108f; // 234 - (348 - 96) / 2f
            tempVector4.z = scrollMyClan.baseClipRegion.z;
            tempVector4.w = firstHeightMyClan - (348 - 96);
            scrollMyClan.baseClipRegion = tempVector4;
            groupButtonClanObject.SetActive(true);
            if (CachePvp.TypeMemberClan == CachePvp.CLAN_MASTER)
            {
                bLeftTypeClanObject.SetActive(true);
                bRightTypeClanObject.SetActive(true);
                bLeftLevelRequiredClanObject.SetActive(true);
                bRightLevelRequiredClanObject.SetActive(true);
            }
            else
            {
                bLeftTypeClanObject.SetActive(false);
                bRightTypeClanObject.SetActive(false);
                bLeftLevelRequiredClanObject.SetActive(false);
                bRightLevelRequiredClanObject.SetActive(false);
            }
        }
        scrollMyClan.GetComponent<UIScrollView>().ResetPosition();
    }

    void LeaveChat()
    {
        if (typeJoinRoomChat == 1)
        {
            new CSClanLeaveChat(CSClanLeaveChat.TYPE_JOIN_ROOM_CLAN).Send();
        }
        else if (typeJoinRoomChat == 2)
        {
            new CSClanLeaveChat(CSClanLeaveChat.TYPE_JOIN_ROOM_WORLD).Send();
        }
        typeJoinRoomChat = 0;
    }

    public void bMyClan()
    {
        if (!sBtnMyClan.spriteName.Equals(SELECTED_SPRITE_NAME))
        {
            bBackFinish();
            sBtnMyClan.spriteName = SELECTED_SPRITE_NAME;
            sBtnChat.spriteName = UNSELECTED_SPRITE_NAME;
            sBtnWorld.spriteName = UNSELECTED_SPRITE_NAME;
            lNeedGem.text = "- " + CachePvp.sCClientConfig.gemToCreateTeam + " " + I2.Loc.ScriptLocalization.gem;
            lNeedGem2.text = lNeedGem.text;
            //if not join clan
            if (CachePvp.TypeMemberClan == CachePvp.CLAN_LOADING)
            {
                fxLoading.SetActive(true);
                clanObject.gameObject.SetActive(false);
                scrollMyClan.gameObject.SetActive(false);
                panelRequest.SetActive(false);
                popupConfirm.SetActive(false);
                contentMyClan.SetActive(false);
                bgChooseAvatar.SetActive(false);
                if (listClanInvite.Count != 0)
                {
                    buttonInviteClan.SetActive(true);
                }
            }
            else if (CachePvp.TypeMemberClan == CachePvp.HAS_NO_CLAN)
            {
                fxLoading.SetActive(false);
                clanObject.gameObject.SetActive(false);
                scrollMyClan.gameObject.SetActive(false);
                panelRequest.SetActive(false);
                popupConfirm.SetActive(false);
                contentMyClan.SetActive(true);
                first.SetActive(true);
                createClan.SetActive(false);
                bgChooseAvatar.SetActive(false);
                if (listClanInvite.Count != 0)
                {
                    buttonInviteClan.SetActive(true);
                }
            }
            else
            {
                //else if joined clan
                scrollMyClan.gameObject.SetActive(true);
                panelRequest.SetActive(false);
                popupConfirm.SetActive(false);
                contentMyClan.SetActive(false);
                bgChooseAvatar.SetActive(false);
                buttonInviteClan.SetActive(false);
                if (ci != null)
                {
                    clanObject.gameObject.SetActive(true);
                }
                if (isOpen)
                {
                    clanObject.height = 95;
                    isOpen = false;
                    groupButtonClanObject.SetActive(false);
                }
            }
            StartCoroutine(WaitLoadHeight());

            worldObject.SetActive(false);
            scrollWorld.gameObject.SetActive(false);
            buttonSearch.gameObject.SetActive(false);

            chatObject.SetActive(false);
            gPanelMedalTop.SetActive(false);
            scrollViewChatClan.gameObject.SetActive(false);
            scrollViewChatWorld.gameObject.SetActive(false);
            if (loadedMyClan)
            {
                loadedMyClan = false;
                StartCoroutine(Wait1Frame());
                fxLoading.SetActive(false);
            }
            else
            {
                if (needReloadMyClan)
                {
                    fxLoading.SetActive(true);
                    needReloadMyClan = false;
                    contentMyClan.SetActive(false);
                    new CSMyClan().Send();
                }
                else
                {
                    fxLoading.SetActive(false);
                    if (CachePvp.TypeMemberClan != CachePvp.HAS_NO_CLAN)
                    {
                        scrollviewMyClan.ResetPosition();
                        bool hasActive = false;
                        for (int i = 0; i < scrollMyClan.transform.GetChild(0).childCount; i++)
                        {
                            if (scrollMyClan.transform.GetChild(0).GetChild(i).gameObject.activeInHierarchy)
                            {
                                hasActive = true;
                                break;
                            }
                        }
                        if (!hasActive)
                        {
                            StartCoroutine(Wait1Frame());
                        }
                    }
                }
            }
            bool exist = false;
            for (int i = 0; i < scrollClanNear.transform.GetChild(0).childCount; i++)
            {
                if (scrollClanNear.transform.GetChild(0).GetChild(i).gameObject.activeInHierarchy)
                {
                    exist = true;
                    break;
                }
            }
            if (exist)
            {
                lNoClanSuggest.SetActive(false);
            }
            else
            {
                lNoClanSuggest.SetActive(true);
            }
            LeaveChat();
            gRequestChat.SetActive(false);
            gRequestChatOffTime.SetActive(false);
            buttonLog.SetActive(false);
            gShopClan.SetActive(false);
            buttonChat.SetActive(false);
        }
    }

    private IEnumerator WaitLoadHeight()
    {
        yield return null;
        firstHeightMyClan = scrollMyClan.baseClipRegion.w;
    }

    public void bChat()
    {
        if (!sBtnChat.spriteName.Equals(SELECTED_SPRITE_NAME))
        {
            if (CachePvp.TypeMemberClan == CachePvp.CLAN_MASTER && (oldIsPrivateTeamType != isPrivateTeamType || oldLevelRequired != levelRequired))
            {
                typeButtonClick = 0; //0: chat, 1: world, 2: exit
                confirmSaveChange.SetActive(true);
                return;
            }
            buttonBackClan.SetActive(true);
            buttonBackFinish.SetActive(false);
            buttonInviteClan.SetActive(false);
            if (CachePvp.TypeMemberClan == CachePvp.HAS_NO_CLAN)
            {
                //btnChatClan.SetActive(false);
                //btnChatWorld.SetActive(false);

                fxLoading.SetActive(false);
                sBtnMyClan.spriteName = UNSELECTED_SPRITE_NAME;
                sBtnChat.spriteName = SELECTED_SPRITE_NAME;
                sBtnWorld.spriteName = UNSELECTED_SPRITE_NAME;

                //sBtnChatClan.spriteName = SELECTED_SPRITE_NAME;
                //sBtnChatWorld.spriteName = UNSELECTED_SPRITE_NAME;

                clanObject.gameObject.SetActive(false);
                scrollMyClan.gameObject.SetActive(false);
                contentMyClan.SetActive(false);
                bgChooseAvatar.SetActive(false);
                popupConfirm.SetActive(false);
                panelRequest.SetActive(false);

                worldObject.SetActive(false);
                scrollWorld.gameObject.SetActive(false);
                buttonSearch.gameObject.SetActive(false);

                chatObject.SetActive(true);
                gPanelMedalTop.SetActive(false);
                buttonGloryChest.SetActive(false);
                scrollViewChatClan.gameObject.SetActive(false);
                scrollViewChatWorld.gameObject.SetActive(false);
                lDontHaveClan.gameObject.SetActive(true);
                gRequestChat.SetActive(false);
                gRequestChatOffTime.SetActive(false);
                gShopClan.SetActive(false);
                buttonLog.SetActive(false);
            }
            else
            {
                //btnChatClan.SetActive(true);
                //btnChatWorld.SetActive(true);
                typeJoinRoomChat = 1;
                if (sBtnChatWorld.spriteName.Equals(SELECTED_SPRITE_NAME))
                {
                    new CSClanLeaveChat(CSClanLeaveChat.TYPE_JOIN_ROOM_WORLD).Send();
                }
                fxLoading.SetActive(true);
                sBtnMyClan.spriteName = UNSELECTED_SPRITE_NAME;
                sBtnChat.spriteName = SELECTED_SPRITE_NAME;
                sBtnWorld.spriteName = UNSELECTED_SPRITE_NAME;

                //sBtnChatClan.spriteName = SELECTED_SPRITE_NAME;
                //sBtnChatWorld.spriteName = UNSELECTED_SPRITE_NAME;

                clanObject.gameObject.SetActive(false);
                scrollMyClan.gameObject.SetActive(false);
                contentMyClan.SetActive(false);
                bgChooseAvatar.SetActive(false);
                popupConfirm.SetActive(false);
                panelRequest.SetActive(false);

                worldObject.SetActive(false);
                scrollWorld.gameObject.SetActive(false);
                buttonSearch.gameObject.SetActive(false);

                chatObject.SetActive(true);
                lNumMedal.text = GameContext.FormatNumber(CachePvp.Medal);
                gPanelMedalTop.SetActive(true);
                lDontHaveClan.gameObject.SetActive(false);
                scrollViewChatClan.gameObject.SetActive(true);
                scrollViewChatClan.transform.localPosition = Vector3.zero;
                tempVector2.x = 0;
                tempVector2.y = -378.67f;
                scrollViewChatClan.clipOffset = tempVector2;
                scrollViewChatWorld.gameObject.SetActive(false);
                isChatClan = true;
                //gRequestChat.SetActive(true);
                new CSClanJoinChat(CSClanJoinChat.TYPE_JOIN_ROOM_CLAN).Send();
                for (int i = 0; i < scrollViewChatClan.transform.GetChild(0).childCount; i++)
                {
                    scrollViewChatClan.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
                }
                for (int i = 0; i < scrollViewChatWorld.transform.GetChild(0).childCount; i++)
                {
                    scrollViewChatWorld.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
                }
            }
        }
    }

    public void bChatClan()
    {
        if (!sBtnChatClan.spriteName.Equals(SELECTED_SPRITE_NAME))
        {
            typeJoinRoomChat = 1;
            if (sBtnChatWorld.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                new CSClanLeaveChat(CSClanLeaveChat.TYPE_JOIN_ROOM_WORLD).Send();
            }
            fxLoading.SetActive(true);
            sBtnChatClan.spriteName = SELECTED_SPRITE_NAME;
            sBtnChatWorld.spriteName = UNSELECTED_SPRITE_NAME;

            clanObject.gameObject.SetActive(false);
            scrollMyClan.gameObject.SetActive(false);
            contentMyClan.SetActive(false);
            bgChooseAvatar.SetActive(false);
            popupConfirm.SetActive(false);
            panelRequest.SetActive(false);

            worldObject.SetActive(false);
            scrollWorld.gameObject.SetActive(false);
            buttonSearch.gameObject.SetActive(false);

            chatObject.SetActive(true);
            gPanelMedalTop.SetActive(true);
            lNumMedal.text = GameContext.FormatNumber(CachePvp.Medal);
            scrollViewChatClan.gameObject.SetActive(true);
            scrollViewChatClan.transform.localPosition = Vector3.zero;
            tempVector2.x = 0;
            tempVector2.y = -378.67f;
            scrollViewChatClan.clipOffset = tempVector2;
            scrollViewChatWorld.gameObject.SetActive(false);
            isChatClan = true;
            new CSClanJoinChat(CSClanJoinChat.TYPE_JOIN_ROOM_CLAN).Send();
            for (int i = 0; i < scrollViewChatClan.transform.GetChild(0).childCount; i++)
            {
                scrollViewChatClan.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            }
            for (int i = 0; i < scrollViewChatWorld.transform.GetChild(0).childCount; i++)
            {
                scrollViewChatWorld.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    public void bChatWorld()
    {
        if (!sBtnChatWorld.spriteName.Equals(SELECTED_SPRITE_NAME))
        {
            typeJoinRoomChat = 2;
            if (sBtnChatClan.spriteName.Equals(SELECTED_SPRITE_NAME))
            {
                new CSClanLeaveChat(CSClanLeaveChat.TYPE_JOIN_ROOM_CLAN).Send();
            }
            fxLoading.SetActive(true);
            sBtnChatClan.spriteName = UNSELECTED_SPRITE_NAME;
            sBtnChatWorld.spriteName = SELECTED_SPRITE_NAME;

            clanObject.gameObject.SetActive(false);
            scrollMyClan.gameObject.SetActive(false);
            contentMyClan.SetActive(false);
            bgChooseAvatar.SetActive(false);
            popupConfirm.SetActive(false);
            panelRequest.SetActive(false);

            worldObject.SetActive(false);
            scrollWorld.gameObject.SetActive(false);
            buttonSearch.gameObject.SetActive(false);

            chatObject.SetActive(true);
            gPanelMedalTop.SetActive(true);
            lNumMedal.text = GameContext.FormatNumber(CachePvp.Medal);
            scrollViewChatClan.gameObject.SetActive(false);
            scrollViewChatWorld.gameObject.SetActive(true);
            scrollViewChatWorld.transform.localPosition = Vector3.zero;
            tempVector2.x = 0;
            tempVector2.y = -378.67f;
            scrollViewChatWorld.clipOffset = tempVector2;
            isChatClan = false;
            new CSClanJoinChat(CSClanJoinChat.TYPE_JOIN_ROOM_WORLD).Send();
            for (int i = 0; i < scrollViewChatClan.transform.GetChild(0).childCount; i++)
            {
                scrollViewChatClan.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            }
            for (int i = 0; i < scrollViewChatWorld.transform.GetChild(0).childCount; i++)
            {
                scrollViewChatWorld.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    public void bWorld()
    {
        if (!sBtnWorld.spriteName.Equals(SELECTED_SPRITE_NAME))
        {
            if (CachePvp.TypeMemberClan == CachePvp.CLAN_MASTER && (oldIsPrivateTeamType != isPrivateTeamType || oldLevelRequired != levelRequired))
            {
                typeButtonClick = 1; //0: chat, 1: world, 2: exit
                confirmSaveChange.SetActive(true);
                return;
            }
            buttonInviteClan.SetActive(false);
            buttonBackClan.SetActive(true);
            buttonBackFinish.SetActive(false);
            sBtnMyClan.spriteName = UNSELECTED_SPRITE_NAME;
            sBtnChat.spriteName = UNSELECTED_SPRITE_NAME;
            sBtnWorld.spriteName = SELECTED_SPRITE_NAME;

            clanObject.gameObject.SetActive(false);
            scrollMyClan.gameObject.SetActive(false);
            contentMyClan.SetActive(false);
            bgChooseAvatar.SetActive(false);
            popupConfirm.SetActive(false);
            panelRequest.SetActive(false);

            worldObject.SetActive(true);
            scrollWorld.gameObject.SetActive(true);
            scrollWorld.transform.localPosition = Vector3.zero;
            tempVector2.x = 0;
            tempVector2.y = -378.67f;
            scrollWorld.clipOffset = tempVector2;
            buttonSearch.gameObject.SetActive(true);

            chatObject.SetActive(false);
            gPanelMedalTop.SetActive(false);
            scrollViewChatClan.gameObject.SetActive(false);
            scrollViewChatWorld.gameObject.SetActive(false);
            if (loadedWorldClan)
            {
                loadedWorldClan = false;
                fxLoading.SetActive(false);
                StartCoroutine(Wait1FrameAfterLoadListTopWorld());
            }
            else
            {
                if (needReloadWorldClan)
                {
                    lSearchValue.text = "";
                    fxLoading.SetActive(true);
                    new CSClanTopWorld().Send();
                    needReloadWorldClan = false;
                }
                else
                {
                    fxLoading.SetActive(false);
                    scrollviewWorld.ResetPosition();
                    if (listClanTopWorld.Count > 0)
                    {
                        bool hasActive = false;
                        for (int i = 0; i < scrollWorld.transform.GetChild(0).childCount; i++)
                        {
                            if (scrollWorld.transform.GetChild(0).GetChild(i).gameObject.activeInHierarchy)
                            {
                                hasActive = true;
                                break;
                            }
                        }
                        if (!hasActive)
                        {
                            StartCoroutine(Wait1FrameAfterLoadListTopWorld());
                        }
                    }
                }
            }

            LeaveChat();
            gRequestChat.SetActive(false);
            gRequestChatOffTime.SetActive(false);
            buttonLog.SetActive(false);
            buttonChat.SetActive(false);
            gShopClan.SetActive(false);
        }
    }

    public void RequestItem()
    {
        PopupManager.Instance.ShowSelectCardTypePopup();
    }

    public void RequestItemDeactive()
    {
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_request_cooldown_not_over_yet, false, 1.5f);
    }

    public void bSubmitChat()
    {
        if (string.IsNullOrEmpty(inputButtonChat.value.Trim()))
        {
            return;
        }
        if (isChatClan)
        {
            //MessageDispatcher.SendMessage(gameObject, EventName.Clan.InputChatSubmitClan.ToString(), inputButtonChat.value, 0);
            new CSClanChat(CSClanChat.CHAT_CLAN, inputButtonChat.value).Send();
            inputButtonChat.value = "";
        }
        else
        {
            //MessageDispatcher.SendMessage(gameObject, EventName.Clan.InputChatSubmitWorld.ToString(), inputButtonChat.value, 0);
            new CSClanChat(CSClanChat.CHAT_WORLD, inputButtonChat.value).Send();
            inputButtonChat.value = "";
        }
    }

    public void submitSearch()
    {
        lResearch.color = new Color(1, 1, 1, 1);
        lSearchValue.text = buttonSearch.value;
        new CSClanSearch(buttonSearch.value).Send();
    }

    public void bChooseAvatar()
    {
        bgChooseAvatar.SetActive(true);
    }

    public void bAcceptAvatarClan()
    {
        if ((index + 2) < 10)
        {
            sAvatarClan.spriteName = "avt_Avt_0" + (index + 2);
        }
        else
        {
            sAvatarClan.spriteName = "avt_Avt_" + (index + 2);
        }
        bgChooseAvatar.SetActive(false);
    }

    private bool isPrivateTeamType;
    private int levelRequired;

    private bool oldIsPrivateTeamType;
    private int oldLevelRequired;

    int typeButtonClick; //0: chat, 1: world, 2: exit

    public void bLeftType()
    {
        isPrivateTeamType = !isPrivateTeamType;
        lTeamType.text = isPrivateTeamType ? I2.Loc.ScriptLocalization.close : I2.Loc.ScriptLocalization.open;
        lTeamTypeClanObject.text = lTeamType.text;
        lTeamTypeConfirm.text = lTeamType.text;
    }

    public void bRightType()
    {
        bLeftType();
    }

    public void SaveConfirmSaveChange()
    {
        lTeamType.text = isPrivateTeamType ? I2.Loc.ScriptLocalization.close : I2.Loc.ScriptLocalization.open;
        lTeamTypeClanObject.text = lTeamType.text;
        lTeamTypeConfirm.text = lTeamType.text;
        lRequiredLevel.text = levelRequired.ToString();
        lRequiredLevelClanObject.text = lRequiredLevel.text;
        lRequiredLevelConfirm.text = lRequiredLevel.text;
        new CSClanEdit(ci.id, ci.name, ci.avatar, isPrivateTeamType ? 1 : 0, levelRequired).Send();
        switch (typeButtonClick)
        {
            case 0:
                bChat();
                break;
            case 1:
                bWorld();
                break;
            case 2:
                bBackClan();
                break;
            default:
                break;
        }
    }

    ClanInfo m_ci_view;

    bool fromInvite;

    public void ShowClanInfo(ClanInfo ci_view, bool fromInvite = false)
    {
        this.fromInvite = fromInvite;
        m_ci_view = ci_view;
        panelClanInfo.SetActive(true);
        if (fromInvite)
        {
            buttonDenyInvite.SetActive(true);
        }
        else
        {
            buttonDenyInvite.SetActive(false);
        }
        if ((m_ci_view.avatar + 2) < 10)
        {
            avatarClanInfo.spriteName = "avt_Avt_0" + (m_ci_view.avatar + 2);
        }
        else
        {
            avatarClanInfo.spriteName = "avt_Avt_" + (m_ci_view.avatar + 2);
        }

        scoreValueClanInfo.text = GameContext.FormatNumber((int)m_ci_view.score);
        typeValueClanInfo.text = m_ci_view.type == 1 ? I2.Loc.ScriptLocalization.close : I2.Loc.ScriptLocalization.open;
        levelRequiredValueClanInfo.text = m_ci_view.requiredLevel.ToString();
        nameClanInfo.text = m_ci_view.name;
        memberClanInfo.text = m_ci_view.totalMember + "/" + m_ci_view.size;

        if (CachePvp.TypeMemberClan != CachePvp.HAS_NO_CLAN || ci_view.totalMember == ci_view.size || ci_view.requiredLevel > CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL))
        {
            buttonJoinClanInfo.height = 0;
            buttonJoinClanInfo.color = Color.clear;
            buttonJoinClanInfo.gameObject.SetActive(false);
            buttonJoinDeactiveClanInfo.SetActive(false);
        }
        else
        {
            buttonJoinClanInfo.MakePixelPerfect();
            buttonJoinClanInfo.color = Color.white;
            if (CachePvp.listClanIdRequest.Contains(m_ci_view.id))
            {
                buttonJoinClanInfo.gameObject.SetActive(false);
                buttonJoinDeactiveClanInfo.SetActive(true);
            }
            else
            {
                buttonJoinClanInfo.gameObject.SetActive(true);
                buttonJoinDeactiveClanInfo.SetActive(false);
            }
        }
        fxLoading.SetActive(true);
        new CSClanMember(ci_view.id).Send();
    }

    public void bDenyClan()
    {
        panelClanInfo.SetActive(false);
        new CSClanInvitationReject(m_ci_view.id).Send();
    }

    public void bBackClanInfo()
    {
        panelClanInfo.SetActive(false);
    }

    public void DontSaveConfirmSaveChange()
    {
        confirmSaveChange.SetActive(false);
        isPrivateTeamType = oldIsPrivateTeamType;
        levelRequired = oldLevelRequired;

        lTeamType.text = isPrivateTeamType ? I2.Loc.ScriptLocalization.close : I2.Loc.ScriptLocalization.open;
        lTeamTypeClanObject.text = lTeamType.text;
        lTeamTypeConfirm.text = lTeamType.text;
        lRequiredLevel.text = levelRequired.ToString();
        lRequiredLevelClanObject.text = lRequiredLevel.text;
        lRequiredLevelConfirm.text = lRequiredLevel.text;
        switch (typeButtonClick)
        {
            case 0:
                bChat();
                break;
            case 1:
                bWorld();
                break;
            case 2:
                bBackClan();
                break;
            default:
                break;
        }
    }

    public void bLeftRequiredLevel()
    {
        levelRequired -= 5;
        levelRequired = Mathf.Min(Mathf.Max(0, levelRequired), (GameContext.TOTAL_LEVEL - 1) - (GameContext.TOTAL_LEVEL - 1) % 5);
        lRequiredLevel.text = levelRequired.ToString();
        lRequiredLevelClanObject.text = lRequiredLevel.text;
        lRequiredLevelConfirm.text = lRequiredLevel.text;
    }

    public void bRightRequiredLevel()
    {
        levelRequired += 5;
        levelRequired = Mathf.Min(Mathf.Max(0, levelRequired), (GameContext.TOTAL_LEVEL - 1) - (GameContext.TOTAL_LEVEL - 1) % 5);
        lRequiredLevel.text = levelRequired.ToString();
        lRequiredLevelClanObject.text = lRequiredLevel.text;
        lRequiredLevelConfirm.text = lRequiredLevel.text;
    }

    public void bBackFinish()
    {
        first.SetActive(true);
        createClan.SetActive(false);
        buttonBackClan.SetActive(true);
        buttonBackFinish.SetActive(false);
    }

    public void bBackClan()
    {
        if (CachePvp.TypeMemberClan == CachePvp.CLAN_MASTER && (oldIsPrivateTeamType != isPrivateTeamType || oldLevelRequired != levelRequired))
        {
            typeButtonClick = 2; //0: chat, 1: world, 2: exit
            confirmSaveChange.SetActive(true);
            return;
        }
        LeaveChat();
        PopupManager.Instance.HideClanPopup();
    }

    public void bFinish()
    {
        Debug.Log("max level : " + CacheGame.GetMaxLevel3Difficult());
        if (inputName.value.Length < 3)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_pvp_character.Replace("%{number_character}", "3"), false, 1.5f);
            return;
        }
        if (CacheGame.GetMaxLevel3Difficult() <= 14)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", "14"), false, 1.5f);
            return;
        }
        if (CachePvp.GetGem() < CachePvp.sCClientConfig.gemToCreateTeam)
        {
            //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_gem, false, 1.5f);
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
            return;
        }
        buttonFinish.SetActive(false);
        buttonFinishDeactive.SetActive(true);
        new CSClanCreate(inputName.value, index, isPrivateTeamType ? 1 : 0, levelRequired).Send();
    }

    public void bStart()
    {
        first.SetActive(false);
        createClan.SetActive(true);

        buttonFinish.SetActive(true);
        buttonFinishDeactive.SetActive(false);
        buttonBackClan.SetActive(false);
        buttonBackFinish.SetActive(true);

        fxLoadingScrollClanNear.SetActive(false);
        isPrivateTeamType = true;
        bLeftType();
        levelRequired = 0;
        bLeftRequiredLevel();
    }

    public void bShopClan()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        PopupManager.Instance.ShowShopClanPopup();
    }

    private bool readyClickBack;

    IEnumerator ReadyClickBack()
    {
        readyClickBack = false;
        yield return new WaitForSecondsRealtime(0.38f);
        readyClickBack = true;
    }

    public void bLog()
    {
        PopupManager.Instance.ShowDonateInfoPopup();
    }

    public void ButtonInvite()
    {
        PopupManager.Instance.ShowTeamMailPopup();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && readyClickBack)
        {
            StartCoroutine(ReadyClickBack());
            if (panelLoading.activeInHierarchy)
            {

            }
            else if (PopupManager.Instance.IsProfilePopupOnTopActive())
            {
                PopupManager.Instance.HideProfilePopupOnTop();
            }
            else if (PopupManager.Instance.IsGloryChestPopupActive())
            {

            }
            else if (bgChooseAvatar.activeInHierarchy)
            {
                bAcceptAvatarClan();
            }
            else if (popupHaveToSetMaster.activeInHierarchy)
            {
                bCloseSetMasterPopup();
            }
            else if (popupConfirm.activeInHierarchy)
            {
                bNoConfirm();
            }
            else if (confirmSaveChange.activeInHierarchy)
            {
                DontSaveConfirmSaveChange();
            }
            else if (panelRequest.activeInHierarchy)
            {
                bBackPanelJoinRequest();
            }
            else if (panelClanInvite.activeInHierarchy)
            {
                bBackClanInvite();
            }
            else if (panelClanInfo.activeInHierarchy)
            {
                bBackClanInfo();
            }
            else if (panelInvite.activeInHierarchy)
            {
                bCancelInviteMember();
            }
            else if (buttonBackFinish.activeInHierarchy)
            {
                bBackFinish();
            }
            else if (PopupManager.Instance.IsTeamMailPopupActive())
            {

            }
            else if (PopupManager.Instance.IsSelectCardTypePopupActive() || PopupManager.Instance.IsGloryChestPopupActive())
            {

            }
            else if (PopupManager.Instance.IsResetShopClanPopupActive())
            {
                PopupManager.Instance.HideResetShopClanPopup();
            }
            else if (PopupManager.Instance.IsShopClanPopupActive())
            {
                PopupManager.Instance.HideShopClanPopup();
            }
            else if (PopupManager.Instance.IsDonateInfoPopupActive())
            {
                PopupManager.Instance.HideDonateInfoPopup();
            }
            else if (buttonBackClan.activeInHierarchy)
            {
                bBackClan();
            }
        }
    }
}
