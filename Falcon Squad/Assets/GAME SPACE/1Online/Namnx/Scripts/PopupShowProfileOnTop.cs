﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OneSoftGame.Tools;
using com.ootii.Messages;
using Mp.Pvp;
using Facebook.Unity;
using System;

public class PopupShowProfileOnTop : PersistentSingleton<PopupShowProfileOnTop>
{
    public UISprite sVipIcon;
    public UI2DSprite sAvatar;
    public UISprite sFlag;
    public UILabel lName;
    public UILabel lLevel;
    public UILabel lStars;
    public UISprite sRank;
    public UILabel lRank;
    public UILabel lId;
    public UILabel lElo;

    public UILabel lGold;
    public UILabel lGem;
    public UILabel lEnergy;

    public UILabel lPlaneValue;
    public UILabel lDroneValue;
    public UILabel lWingValue;

    public List<GameObject> groupPlane;
    public List<GameObject> groupDrone;
    public List<GameObject> groupWing;

    public GameObject buttonFacebook;
    public GameObject buttonGamecenter;
    public GameObject buttonAddFriends;
    public GameObject buttonFriend;
    public GameObject buttonAddFriendsDeactive;
    public GameObject buttonFollow;
    public GameObject buttonFollowDeactive;
    public GameObject buttonUnfollow;
    public GameObject buttonUnfollowDeactive;

    public AircraftAnimations aa;

    Player m_player;

    private void OnEnable()
    {
        SocialManager.Instance.OnFacebookLoginSuccessed += OnFacebookLoginSuccessed;
        SocialManager.Instance.OnFacebookLoginFail += OnFacebookLoginFailed;

        SocialManager.Instance.OnGCLoginSuccessed += OnGCLoginSuccessed;
        SocialManager.Instance.OnGCLoginFailed += OnGCLoginFailed;

        MessageDispatcher.AddListener(EventName.LoadProfile.LoadProfile.ToString(), OnLoadProfile, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadProfileLocal.ToString(), OnLoadProfileLocal, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.LoginFacebook.LoggedFacebook.ToString(), OnLoadProfileLocal, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadFriendshipStatus.ToString(), OnLoadFriendshipStatus, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.RequestFriend.ToString(), OnRequestFriend, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.FollowFriend.ToString(), OnFollowFriend, true);
    }

    private void OnDisable()
    {
        if (SocialManager.Instance.OnFacebookLoginSuccessed != null)
        {
            SocialManager.Instance.OnFacebookLoginSuccessed -= OnFacebookLoginSuccessed;
        }
        if (SocialManager.Instance.OnFacebookLoginFail != null)
        {
            SocialManager.Instance.OnFacebookLoginFail -= OnFacebookLoginFailed;
        }
        if (SocialManager.Instance.OnGCLoginSuccessed != null)
        {
            SocialManager.Instance.OnGCLoginSuccessed -= OnGCLoginSuccessed;
        }
        if (SocialManager.Instance.OnGCLoginFailed != null)
        {
            SocialManager.Instance.OnGCLoginFailed -= OnGCLoginFailed;
        }
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadFriendshipStatus.ToString(), OnLoadFriendshipStatus, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RequestFriend.ToString(), OnRequestFriend, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.FollowFriend.ToString(), OnFollowFriend, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadProfile.ToString(), OnLoadProfile, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadProfileLocal.ToString(), OnLoadProfileLocal, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.LoginFacebook.LoggedFacebook.ToString(), OnLoadProfileLocal, true);
    }

    private void OnFollowFriend(IMessage rMessage)
    {
        SCFollow f = rMessage.Data as SCFollow;
        if (PopupManager.Instance.IsFollowPopupActive() && m_player.code.Equals(CachePvp.Code))
        {
        }
        else
        {
            if (f.status != SCFollow.SUCCESS)
            {
                if (f.type == CSFollow.FOLLOW)
                {
                    buttonFollow.SetActive(true);
                    buttonFollowDeactive.SetActive(false);
                }
                else if (f.type == CSFollow.UNFOLLOW)
                {
                    buttonUnfollow.SetActive(true);
                    buttonUnfollowDeactive.SetActive(false);
                }
            }
            else
            {
                if (f.type == CSFollow.FOLLOW)
                {
                    buttonFollow.SetActive(false);
                    buttonFollowDeactive.SetActive(false);
                    buttonUnfollow.SetActive(true);
                    buttonUnfollowDeactive.SetActive(false);
                    if (!PopupManager.Instance.IsProfilePopupActive())
                    {
                        CachePvp.FriendshipStats.numFollow++;
                    }
                }
                else if (f.type == CSFollow.UNFOLLOW)
                {
                    buttonUnfollow.SetActive(false);
                    buttonUnfollowDeactive.SetActive(false);
                    buttonFollow.SetActive(true);
                    buttonFollowDeactive.SetActive(false);
                    if (!PopupManager.Instance.IsProfilePopupActive())
                    {
                        CachePvp.FriendshipStats.numFollow--;
                    }
                }
            }
        }
    }

    private void OnRequestFriend(IMessage rMessage)
    {
        SCRequestFriend rf = rMessage.Data as SCRequestFriend;
        if (rf.code.Equals(m_player.code))
        {
            if (rf.status == SCRequestFriend.SUCCESS)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.request_sent, false, 1.5f);
            }
            else if (rf.status == SCRequestFriend.BE_FRIEND)
            {
                buttonAddFriends.SetActive(false);
                buttonAddFriendsDeactive.SetActive(false);
                buttonFriend.SetActive(true);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.success, true, 1.5f);
            }
            else
            {
                buttonAddFriends.SetActive(true);
                buttonAddFriendsDeactive.SetActive(false);
                buttonFriend.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(rf.message, false, 1.5f);
            }
        }
    }

    private void OnLoadFriendshipStatus(IMessage rMessage)
    {
        SCFriendshipStatus fs = rMessage.Data as SCFriendshipStatus;
        if (fs.status == SCFriendshipStatus.SUCCESS)
        {
            if (m_player.code.Equals(fs.friendshipStatus.code) && !m_player.code.Equals(CachePvp.Code))
            {
                if (fs.friendshipStatus.isFriend)
                {
                    buttonAddFriends.SetActive(false);
                    buttonFriend.SetActive(true);
                }
                else
                {
                    buttonAddFriends.SetActive(true);
                    buttonFriend.SetActive(false);
                }

                if (fs.friendshipStatus.isFollowing)
                {
                    buttonFollow.SetActive(false);
                    buttonUnfollow.SetActive(true);
                }
                else
                {
                    buttonFollow.SetActive(true);
                    buttonUnfollow.SetActive(false);
                }
            }
        }
    }

    int lastIndexPlane;
    int lastIndexDrone;

    void OnRefreshAvatar(IMessage msg)
    {
        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdToShow))
        {
            sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdToShow];
        }
    }

    void OnLoadProfileLocal(IMessage msg)
    {
        int d = (int)msg.Data;
        if (d == 1)
        {
            CheckFriends();
        }
        Player player = CachePvp.MyInfo;
        PlayerDataUtil.PlayerProfileData profile = PlayerDataUtil.StringToObject(PlayerDataUtil.GetPlayerProfileData().ToString());
        SetUIProfile(profile, player);
    }

    void OnLoadProfile(IMessage msg)
    {
        Player player = msg.Data as Player;
        PlayerDataUtil.PlayerProfileData profile = PlayerDataUtil.StringToObject(player.data);
        SetUIProfile(profile, player);
    }

    string facebookIdToShow = "";

    Vector3 tempVector3;

    void SetUIProfile(PlayerDataUtil.PlayerProfileData profile, Player player)
    {
        buttonFacebook.SetActive(false);
        buttonGamecenter.SetActive(false);
        buttonAddFriends.SetActive(false);
        buttonAddFriendsDeactive.SetActive(false);
        buttonFriend.SetActive(false);
        buttonFollow.SetActive(false);
        buttonFollowDeactive.SetActive(false);
        buttonUnfollow.SetActive(false);
        buttonUnfollowDeactive.SetActive(false);
        m_player = player;
        if (player.code.Equals(CachePvp.Code))
        {
            if (GameContext.IS_CHINA_VERSION)
            {
                if (string.IsNullOrEmpty(CachePvp.GamecenterId))
                {
                    buttonGamecenter.SetActive(true);
                    sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                }
                else
                {
                    buttonGamecenter.SetActive(false);
                    facebookIdToShow = CachePvp.GamecenterId;
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.GamecenterId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.GamecenterId];
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.GamecenterId);
                        if (t != null)
                        {
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.GamecenterId, t);
                        }
                        else
                        {
                            sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                        }
                        facebookIdToShow = CachePvp.GamecenterId;
                    }
                }
            }
            else
            {
                if (!FB.IsLoggedIn && !string.IsNullOrEmpty(CachePvp.FacebookId))
                {
#if !UNITY_EDITOR
                    if (SocialManager.Instance.OnFacebookLoginSuccessed != null)
                    {
                        SocialManager.Instance.OnFacebookLoginSuccessed -= OnFacebookLoginSuccessed;
                    }
                    SocialManager.Instance.OnFacebookLoginSuccessed += OnFacebookLoginSuccessedWithFacebookId;
                    SocialManager.Instance.FBLogIn();
#endif
                }
                else if (FB.IsLoggedIn)
                {
                    CheckFriends();
                }
                if (string.IsNullOrEmpty(CachePvp.FacebookId))
                {
                    buttonFacebook.SetActive(true);
                    sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                }
                else
                {
                    buttonFacebook.SetActive(false);
                    facebookIdToShow = CachePvp.FacebookId;
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + CachePvp.FacebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.FacebookId);
                    }
                }
                if (string.IsNullOrEmpty(CachePvp.Name))
                {
                    lName.text = player.code;
                }
                else
                {
                    lName.text = CachePvp.Name;
                }
            }
        }
        else
        {
            if (player.name == null || player.name.Equals(""))
            {
                lName.text = player.code;
            }
            else
            {
                lName.text = player.name;
            }
            sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(player.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                    {
                        showImageFB = false;
                        facebookIdToShow = player.appCenterId;
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                        if (t != null)
                        {
                            showImageFB = false;
                            facebookIdToShow = player.appCenterId;
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                        }
                    }
                }
            }
            if (showImageFB)
            {
                if (!string.IsNullOrEmpty(player.facebookId))
                {
                    facebookIdToShow = player.facebookId;
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                    }
                }
            }
            new CSFriendshipStatus(player.code).Send();
        }

        int vip = CachePvp.GetVipFromVipPoint(player.vip);
        if (vip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (vip - 1);
        }
        lId.text = !string.IsNullOrEmpty(player.code) ? "id: " + player.code : "";
        int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);
        int[] starExtra = PvpUtil.StarExtra(profile);
        int level1 = Mathf.Max(0, Mathf.Min(levelValue[0] - 1, GameContext.TOTAL_LEVEL));
        int level2 = Mathf.Max(0, Mathf.Min(levelValue[2] - 1, GameContext.TOTAL_LEVEL));
        int level3 = Mathf.Max(0, Mathf.Min(levelValue[4] - 1, GameContext.TOTAL_LEVEL));

        lLevel.text = I2.Loc.ScriptLocalization.level + ": " + level1 + ", " + level2 + ", " + level3;
        lStars.text = I2.Loc.ScriptLocalization.star + ": " + Mathf.Min(levelValue[1] + starExtra[0], PvpUtil.MaxStar(level1)) + ", " + Mathf.Min(levelValue[3] + starExtra[1], PvpUtil.MaxStar(level2)) + ", " + Mathf.Min(levelValue[5] + starExtra[2], PvpUtil.MaxStar(level3));
        //lLevel.text = I2.Loc.ScriptLocalization.level + ": " + Mathf.Max(0, Mathf.Min(levelValue[0] - 1, GameContext.TOTAL_LEVEL)) + ", " + Mathf.Max(0, Mathf.Min(levelValue[2] - 1, GameContext.TOTAL_LEVEL)) + ", " + Mathf.Max(0, Mathf.Min(levelValue[4] - 1, GameContext.TOTAL_LEVEL));
        //lStars.text = I2.Loc.ScriptLocalization.star + ": " + levelValue[1] + ", " + levelValue[3] + ", " + levelValue[5];
        sFlag.spriteName = player.country.ToLower();
        lGold.text = GameContext.FormatNumber(player.gold);
        lGem.text = GameContext.FormatNumber(player.diamond);
        lEnergy.text = GameContext.FormatNumber(player.energy);
        countPlane = 0;
        countDrone = 0;
        lastIndexPlane = -1;
        lastIndexDrone = -1;
        for (int i = 0; i < groupPlane.Count; i++)
        {
            groupPlane[i].transform.GetChild(3).gameObject.SetActive(false);
            groupPlane[i].transform.GetChild(4).gameObject.SetActive(false);
        }
        for (int i = 0; i < groupDrone.Count; i++)
        {
            groupDrone[i].transform.GetChild(3).gameObject.SetActive(false);
            groupDrone[i].transform.GetChild(4).gameObject.SetActive(false);
        }
        for (int i = 0; i < groupWing.Count; i++)
        {
            groupWing[i].transform.GetChild(3).gameObject.SetActive(false);
            groupWing[i].transform.GetChild(4).gameObject.SetActive(false);
        }
        Vector3 v = new Vector3(0.66f, 0.66f, 1);
        AircraftTypeEnum aircraftType = (AircraftTypeEnum)(1);
        for (int i = 0; i < groupPlane.Count; i++)
        {
            groupPlane[i].transform.GetChild(2).gameObject.SetActive(false);
            groupPlane[i].transform.GetChild(3).gameObject.SetActive(false);
            groupPlane[i].transform.GetChild(4).gameObject.SetActive(true);
            int j;
            if (i < 3)
            {
                j = i;
            }
            else
            {
                j = i + 2;
            }
            aircraftType = (AircraftTypeEnum)(j + 1);
            UISprite s = groupPlane[i].transform.GetChild(1).GetComponent<UISprite>();
            s.spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
            s.MakePixelPerfect();
            s.transform.localScale = v;
        }
        for (int i = 0; i < groupPlane.Count; i++)
        {
            groupPlane[i].transform.GetChild(2).gameObject.SetActive(false);
            groupPlane[i].transform.GetChild(3).gameObject.SetActive(false);
            groupPlane[i].transform.GetChild(4).gameObject.SetActive(true);
        }
        for (int i = 0; i < profile.listAirCraft.Length; i++)
        {
            if (i == 3 || i == 4)
            {
                continue;
            }
            if (i < 3)
            {
                if (profile.listAirCraft[i] == 0)
                {
                    groupPlane[i].transform.GetChild(2).gameObject.SetActive(false);
                    groupPlane[i].transform.GetChild(3).gameObject.SetActive(false);
                    groupPlane[i].transform.GetChild(4).gameObject.SetActive(true);
                    aircraftType = (AircraftTypeEnum)(i + 1);
                    UISprite s = groupPlane[i].transform.GetChild(1).GetComponent<UISprite>();
                    s.spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
                    s.MakePixelPerfect();
                    s.transform.localScale = v;
                }
                else
                {
                    aircraftType = (AircraftTypeEnum)(i + 1);
                    groupPlane[i].transform.GetChild(2).gameObject.SetActive(true);
                    groupPlane[i].transform.GetChild(3).gameObject.SetActive(true);
                    groupPlane[i].transform.GetChild(4).gameObject.SetActive(false);
                    UISprite s = groupPlane[i].transform.GetChild(1).GetComponent<UISprite>();
                    UISprite sPla = groupPlane[i].transform.GetChild(2).GetComponent<UISprite>();
                    UILabel lPla = groupPlane[i].transform.GetChild(3).GetComponent<UILabel>();
                    if (profile.listRankPlane != null)
                    {
                        s.spriteName = "aircraft" + (int)aircraftType + "_e" + profile.listRankPlane[i] + "_idle_0";
                        sPla.spriteName = "hangar_rank_" + (Rank)profile.listRankPlane[i];
                        lPla.text = profile.listLevelPlane[i].ToString();
                    }
                    else
                    {
                        Array airCrafts = Enum.GetValues(typeof(AircraftTypeEnum));
                        s.spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
                        sPla.spriteName = "hangar_rank_" + AircraftSheet.Get((int)aircraftType).begin_rank;
                        int index = (int)airCrafts.GetValue(i);
                        lPla.text = CacheGame.GetSpaceShipLevel((AircraftTypeEnum)index).ToString();
                    }
                    s.MakePixelPerfect();
                    s.transform.localScale = v;
                    sPla.MakePixelPerfect();
                    countPlane++;
                }
            }
            else
            {
                //hardcode : if data group plane > 8 then don't show (7/11/2018)
                //hardcode : if data group plane > 9 then don't show (3/12/2018)
                //hardcode : if data group plane > 10 then don't show (10/5/2019)
                if (i >= 10)
                {
                    continue;
                }
                if (profile.listAirCraft[i] == 0)
                {
                    groupPlane[i - 2].transform.GetChild(2).gameObject.SetActive(false);
                    groupPlane[i - 2].transform.GetChild(3).gameObject.SetActive(false);
                    groupPlane[i - 2].transform.GetChild(4).gameObject.SetActive(true);
                    aircraftType = (AircraftTypeEnum)(i + 1);
                    UISprite s = groupPlane[i - 2].transform.GetChild(1).GetComponent<UISprite>();
                    s.spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
                    s.MakePixelPerfect();
                    s.transform.localScale = v;
                }
                else
                {
                    groupPlane[i - 2].transform.GetChild(2).gameObject.SetActive(true);
                    groupPlane[i - 2].transform.GetChild(3).gameObject.SetActive(true);
                    groupPlane[i - 2].transform.GetChild(4).gameObject.SetActive(false);
                    aircraftType = (AircraftTypeEnum)(i + 1);
                    UISprite s = groupPlane[i - 2].transform.GetChild(1).GetComponent<UISprite>();
                    UISprite sPla = groupPlane[i - 2].transform.GetChild(2).GetComponent<UISprite>();
                    UILabel lPla = groupPlane[i - 2].transform.GetChild(3).GetComponent<UILabel>();
                    if (profile.listRankPlane != null)
                    {
                        s.spriteName = "aircraft" + (int)aircraftType + "_e" + profile.listRankPlane[i] + "_idle_0";
                        sPla.spriteName = "hangar_rank_" + (Rank)profile.listRankPlane[i];
                        lPla.text = profile.listLevelPlane[i].ToString();
                    }
                    else
                    {
                        Array airCrafts = Enum.GetValues(typeof(AircraftTypeEnum));
                        s.spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
                        sPla.spriteName = "hangar_rank_" + AircraftSheet.Get((int)aircraftType).begin_rank;
                        int index = (int)airCrafts.GetValue(i);
                        lPla.text = CacheGame.GetSpaceShipLevel((AircraftTypeEnum)index).ToString();
                    }
                    s.MakePixelPerfect();
                    s.transform.localScale = v;
                    sPla.MakePixelPerfect();
                    countPlane++;
                }
            }
        }
        for (int i = 0; i < groupDrone.Count; i++)
        {
            groupDrone[i].transform.GetChild(2).gameObject.SetActive(false);
            groupDrone[i].transform.GetChild(3).gameObject.SetActive(false);
            groupDrone[i].transform.GetChild(4).gameObject.SetActive(true);
        }
        WingmanTypeEnum wingmanType = (WingmanTypeEnum)(1);
        for (int i = 0; i < profile.listWingman.Length; i++)
        {
            //hardcode : if data group drone > 5 then don't show (7/11/2018)
            //hardcode : if data group drone > 6 then don't show (12/2/2019)
            if (i >= 6)
            {
                continue;
            }
            if (profile.listWingman[i] == 0)
            {
                groupDrone[i].transform.GetChild(2).gameObject.SetActive(false);
                groupDrone[i].transform.GetChild(3).gameObject.SetActive(false);
                groupDrone[i].transform.GetChild(4).gameObject.SetActive(true);
                wingmanType = (WingmanTypeEnum)(i + 1);
                UISprite s = groupDrone[i].transform.GetChild(1).GetComponent<UISprite>();
                s.spriteName = "wingman" + (int)wingmanType + "_e" + WingmanSheet.Get((int)wingmanType).begin_rank + "_idle_0";
                s.MakePixelPerfect();
            }
            else
            {
                lastIndexDrone = i;
                groupDrone[i].transform.GetChild(2).gameObject.SetActive(true);
                groupDrone[i].transform.GetChild(3).gameObject.SetActive(true);
                groupDrone[i].transform.GetChild(4).gameObject.SetActive(false);
                wingmanType = (WingmanTypeEnum)(i + 1);
                UISprite s = groupDrone[i].transform.GetChild(1).GetComponent<UISprite>();
                UISprite sPla = groupDrone[i].transform.GetChild(2).GetComponent<UISprite>();
                UILabel lPla = groupDrone[i].transform.GetChild(3).GetComponent<UILabel>();
                if (profile.listRankWingman != null)
                {
                    s.spriteName = "wingman" + (int)wingmanType + "_e" + profile.listRankWingman[i] + "_idle_0"; ;
                    sPla.spriteName = "hangar_rank_" + (Rank)profile.listRankWingman[i];
                    lPla.text = profile.listLevelWingmanNew[i].ToString();
                }
                else
                {
                    Array wingmans = Enum.GetValues(typeof(WingmanTypeEnum));
                    s.spriteName = "wingman" + (int)wingmanType + "_e" + WingmanSheet.Get((int)wingmanType).begin_rank + "_idle_0";
                    sPla.spriteName = "hangar_rank_" + WingmanSheet.Get((int)wingmanType).begin_rank;
                    int index = (int)wingmans.GetValue(i);
                    lPla.text = CacheGame.GetWingmanLevel((WingmanTypeEnum)index).ToString();
                }
                s.MakePixelPerfect();
                sPla.MakePixelPerfect();
                countDrone++;
            }
        }
        for (int i = 0; i < groupWing.Count; i++)
        {
            groupWing[i].transform.GetChild(2).gameObject.SetActive(false);
            groupWing[i].transform.GetChild(3).gameObject.SetActive(false);
            groupWing[i].transform.GetChild(4).gameObject.SetActive(true);
        }
        int countListWing = 2; //hardcode : if data group drone > 2 then don't show (18/1/2019)
        if (profile.listWing != null)
        {
            WingTypeEnum wingType = (WingTypeEnum)(1);
            for (int i = 0; i < profile.listWing.Length; i++)
            {
                //hardcode : if data group drone > 2 then don't show (18/1/2019)
                if (i >= countListWing)
                {
                    continue;
                }
                if (profile.listWing[i] == 0)
                {
                    groupWing[i].transform.GetChild(2).gameObject.SetActive(false);
                    groupWing[i].transform.GetChild(3).gameObject.SetActive(false);
                    groupWing[i].transform.GetChild(4).gameObject.SetActive(true);
                    wingType = (WingTypeEnum)(i + 1);
                    UISprite s = groupWing[i].transform.GetChild(1).GetComponent<UISprite>();
                    s.spriteName = "wing" + (int)wingType + "_e" + WingSheet.Get((int)wingType).begin_rank + "_idle_0";
                    s.MakePixelPerfect();
                }
                else
                {
                    lastIndexDrone = i;
                    groupWing[i].transform.GetChild(2).gameObject.SetActive(true);
                    groupWing[i].transform.GetChild(3).gameObject.SetActive(true);
                    groupWing[i].transform.GetChild(4).gameObject.SetActive(false);
                    wingType = (WingTypeEnum)(i + 1);
                    UISprite s = groupWing[i].transform.GetChild(1).GetComponent<UISprite>();
                    UISprite sPla = groupWing[i].transform.GetChild(2).GetComponent<UISprite>();
                    UILabel lPla = groupWing[i].transform.GetChild(3).GetComponent<UILabel>();
                    if (profile.listRankWing != null)
                    {
                        s.spriteName = "wing" + (int)wingType + "_e" + profile.listRankWing[i] + "_idle_0"; ;
                        sPla.spriteName = "hangar_rank_" + (Rank)profile.listRankWing[i];
                        lPla.text = profile.listLevelWing[i].ToString();
                    }
                    else
                    {
                        Array wings = Enum.GetValues(typeof(WingTypeEnum));
                        s.spriteName = "wing" + (int)wingType + "_e" + WingSheet.Get((int)wingType).begin_rank + "_idle_0";
                        sPla.spriteName = "hangar_rank_" + WingSheet.Get((int)wingType).begin_rank;
                        int index = (int)wings.GetValue(i);
                        lPla.text = CacheGame.GetWingLevel((WingTypeEnum)index).ToString();
                    }
                    s.MakePixelPerfect();
                    sPla.MakePixelPerfect();
                    countWing++;
                }
            }
        }
        else
        {
            WingTypeEnum wingType = (WingTypeEnum)(1);
            for (int i = 0; i < countListWing; i++)
            {
                groupWing[i].transform.GetChild(2).gameObject.SetActive(false);
                groupWing[i].transform.GetChild(3).gameObject.SetActive(false);
                groupWing[i].transform.GetChild(4).gameObject.SetActive(true);
                wingType = (WingTypeEnum)(i + 1);
                UISprite s = groupWing[i].transform.GetChild(1).GetComponent<UISprite>();
                s.spriteName = "wing" + (int)wingType + "_e" + WingSheet.Get((int)wingType).begin_rank + "_idle_0";
                s.MakePixelPerfect();
            }
        }
        lPlaneValue.text = countPlane + "/" + (profile.listAirCraft.Length - 2);
        lDroneValue.text = countDrone + "/" + profile.listWingman.Length;
        if (profile.listWing != null)
        {
            lWingValue.text = countWing + "/" + profile.listWing.Length;
        }
        else
        {
            lWingValue.text = countWing + "/" + countListWing;
        }
        if (profile.listRankPlane != null)
        {
            int selected = CachePvp.HighestShip(profile);
            aircraftType = (AircraftTypeEnum)selected;
            aa.PlayAnimations(aircraftType, (Rank)profile.listRankPlane[selected - 1]);
        }
        else
        {
            aircraftType = (AircraftTypeEnum)1;
            aa.PlayAnimations(aircraftType, (Rank)AircraftSheet.Get((int)aircraftType).begin_rank);
        }

        if (CachePvp.LoadMegaRank)
        {
            int rank = CachePvp.dictionaryRank[player.megaPlayerLevel.code];
            MedalRankTitle rankTitle = (MedalRankTitle)(rank);
            lRank.text = rankTitle + " " + CachePvp.ConvertChina(player.megaPlayerLevel.name);
            sRank.spriteName = "PVP_rank_" + rank;
            lElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(player.megaPlayerLevel.elo);
        }
        else
        {
            int rank = CachePvp.dictionaryRank[player.playerLevel.code];
            MedalRankTitle rankTitle = (MedalRankTitle)(rank);
            lRank.text = rankTitle + " " + CachePvp.ConvertChina(player.playerLevel.name);
            sRank.spriteName = "PVP_rank_" + rank;
            lElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(player.playerLevel.elo);
        }
    }

    private void OnFacebookLoginSuccessedWithFacebookId()
    {
        CheckFriends();
    }

    void CheckFriends()
    {
        string query = "/me?fields=friends";
        FB.API(query, HttpMethod.GET, result =>
        {
            if (result.Error == null)
            {
                FBUser user = JsonFx.Json.JsonReader.Deserialize<FBUser>(result.RawResult);
                if (user.friends.data.Count != CachePvp.CountFriends)
                {
                    CachePvp.CountFriends = user.friends.data.Count;
                    JSONObject jo = "[]".ToJsonObject();
                    for (int i = 0; i < user.friends.data.Count; i++)
                    {
                        jo.AddItem(user.friends.data[i].id);
                    }
                    new CSFacebookFriends(jo.ToJson()).Send();
                }
            }
            else
            {
                Debug.Log("Error SetFriendsFacebookToServer() : " + result.Error);
            }
        });
    }

    int countPlane;
    int countDrone;
    int countWing;
    public void ButtonClose()
    {
        PopupManager.Instance.HideProfilePopupOnTop();
    }

    public void ButtonLoginFacebook()
    {
        SocialManager.Instance.FBLogIn();
    }

    private void OnFacebookLoginSuccessed()
    {
        Debug.Log("OnFacebookLoginSuccessed");
        buttonFacebook.SetActive(false);
        facebookIdToShow = SocialManager.Instance.FBGetUserID();
        string query = "/me?fields=name,picture.width(135).height(135).type(normal)";

        FB.API(query, HttpMethod.GET, result =>
        {
            Debug.Log("API query " + query);
            FBUser user = JsonFx.Json.JsonReader.Deserialize<FBUser>(result.RawResult);
            CachePvp.facebookUser = user;
            lName.text = user.name;
            new CSValidateFacebookId(user.id).Send();
        });
    }

    private void OnFacebookLoginFailed()
    {
        PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_login_facebook_fail);
    }

    public void ButtonAddFriend()
    {
        if (string.IsNullOrEmpty(CachePvp.Name))
        {
            PopupManager.Instance.ShowRegisterPopup();
            return;
        }
        buttonAddFriends.SetActive(false);
        buttonAddFriendsDeactive.SetActive(true);
        buttonFriend.SetActive(false);
        new CSRequestFriend(CSRequestFriend.REQUEST, m_player.code).Send();
    }

    public void ButtonFollow()
    {
        if (string.IsNullOrEmpty(CachePvp.Name))
        {
            PopupManager.Instance.ShowRegisterPopup();
            return;
        }
        buttonFollow.SetActive(false);
        buttonFollowDeactive.SetActive(true);
        new CSFollow(CSFollow.FOLLOW, m_player.code).Send();
    }

    public void ButtonUnfollow()
    {
        buttonUnfollow.SetActive(false);
        buttonUnfollowDeactive.SetActive(true);
        new CSFollow(CSFollow.UNFOLLOW, m_player.code).Send();
    }

    public void ButtonLoginGamecenter()
    {
        SocialManager.Instance.LoginGameCenter();
    }

    private void OnGCLoginSuccessed()
    {
        Debug.Log("OnGCLoginSuccessed");
        buttonGamecenter.SetActive(false);
        facebookIdToShow = Social.localUser.id;
        new CSValidateAppCenterId(facebookIdToShow).Send();
    }

    private void OnGCLoginFailed()
    {
        PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_login_gamecenter_fail);
    }
}
