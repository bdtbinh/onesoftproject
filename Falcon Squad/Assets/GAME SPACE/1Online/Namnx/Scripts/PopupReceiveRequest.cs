﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using Mp.Pvp;
using System;

public class PopupReceiveRequest : MonoBehaviour
{
    public UISprite sCardType;
    public UILabel lCardAmount;

    public void OnClickClosePopup()
    {
        PopupManager.Instance.HideReceiveRequestPopup();
    }

    public void SetData(int cardType, int cardAmount)
    {
        lCardAmount.text = cardAmount.ToString();
        sCardType.spriteName = FalconMail.GetSpriteName(cardType, 1);
    }
}
