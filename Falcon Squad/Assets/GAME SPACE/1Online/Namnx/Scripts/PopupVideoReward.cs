﻿using OneSoftGame.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class PopupVideoReward : PersistentSingleton<PopupVideoReward>
{

    public GameObject buttonWatchActive;
    public GameObject buttonWatchDeactive;
    public UILabel lTimeCountDown;

    public void RefreshTime(string text)
    {
        //Debug.Log("refresh time : " + text);
        if (text.Equals(" n/a"))
        {
            buttonWatchActive.SetActive(false);
            buttonWatchDeactive.SetActive(true);
            lTimeCountDown.text = "n/a";
        }
        else if (text.Equals(""))
        {
            buttonWatchActive.SetActive(true);
            buttonWatchDeactive.SetActive(false);
            lTimeCountDown.text = "";
        }
        else
        {
            buttonWatchActive.SetActive(false);
            buttonWatchDeactive.SetActive(true);
            lTimeCountDown.text = text;
        }
    }

    public void bWatch()
    {
        Debug.Log("click");
        buttonWatchActive.SetActive(false);
        buttonWatchDeactive.SetActive(true);
        //lTimeCountDown.text = "n/a";
        MessageDispatcher.SendMessage(EventName.WatchVideo.WatchVideo.ToString());
    }

    public void bClose()
    {
        PopupManager.Instance.HideVideoRewardPopup();
    }
}
