﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;

public class ButtonMailBox : MonoBehaviour
{
    public GameObject backgroundNumber;
    public UILabel lNumber;
    private ButtonTopRightHome buttonTopRightHome;

    private void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadMailBox.ChangeNewMailNumber.ToString(), OnChangeNewMailNumber, true);
        ReloadUI();
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadMailBox.ChangeNewMailNumber.ToString(), OnChangeNewMailNumber, true);
    }

    private void OnChangeNewMailNumber(IMessage rMessage)
    {
        ReloadUI();
    }

    void ReloadUI()
    {
        if (buttonTopRightHome == null)
        {
            buttonTopRightHome = GetComponent<ButtonTopRightHome>();
        }
        Debug.Log("CachePvp.NewMailNumber : " + CachePvp.NewMailNumber + " - CachePvp.NewMailNumberForce : " + CachePvp.NewMailNumberForce);
        if (CachePvp.NewMailNumber != 0 || CachePvp.NewMailNumberForce != 0)
        {
            backgroundNumber.SetActive(true);
            lNumber.text = (CachePvp.NewMailNumber + CachePvp.NewMailNumberForce).ToString();
            buttonTopRightHome.showFirst = true;
        }
        else
        {
            buttonTopRightHome.showFirst = false;
            backgroundNumber.SetActive(false);
        }
    }

    public void bClickShowMailBox()
    {
        if (!CachePvp.sCVersionConfig.canMail)
        {
            PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainMail);
            return;
        }
        PopupManager.Instance.ShowMailPopup();
        if (CachePvp.NewMailNumber != 0)
        {
            new CSMail().Send();
        }
        CachePvp.NewMailNumberForce = 0;
        FirebaseLogSpaceWar.LogClickButton("MailBox");

    }
}