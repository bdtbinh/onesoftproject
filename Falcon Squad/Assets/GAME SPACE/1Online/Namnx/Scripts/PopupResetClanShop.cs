﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;

public class PopupResetClanShop : MonoBehaviour
{
    public UILabel lPriceActive;
    public UILabel lPriceDeactive;
    public int price;
    public GameObject bSendActive;
    public GameObject bSendDeactive;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Shop.ShopReset.ToString(), OnShopReset, true);
        bSendActive.SetActive(true);
        bSendDeactive.SetActive(false);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Shop.ShopReset.ToString(), OnShopReset, true);
    }

    private void OnShopReset(IMessage rMessage)
    {
        bSendActive.SetActive(true);
        bSendDeactive.SetActive(false);
        //bBack();
        LeanTween.delayedCall(0.1f, () =>
        {
            bBack();
        });
    }

    public void SetData(int price)
    {
        lPriceActive.text = GameContext.FormatNumber(price);
        lPriceDeactive.text = GameContext.FormatNumber(price);
        this.price = price;
    }

    public void PressButtonSendActive()
    {
        if (CachePvp.Medal < price)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_medal, false, 1.5f);
            return;
        }
        new CSShopReset().Send();
        bSendActive.SetActive(false);
        bSendDeactive.SetActive(true);
    }

    public void PressButtonSendDeactive()
    {
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_wait_until_ready, false, 1.5f);
    }

    public void bBack()
    {
        PopupManager.Instance.HideResetShopClanPopup();
    }
}
