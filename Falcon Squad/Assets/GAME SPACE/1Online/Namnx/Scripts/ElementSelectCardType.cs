﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class ElementSelectCardType : MonoBehaviour
{
    [SerializeField]
    private UISprite sIcon;

    [SerializeField]
    private UISprite sRank;

    [SerializeField]
    private UILabel lLevel;

    [SerializeField]
    private GameObject sSelected;

    public GameObject sLock;
    public enum Type
    {
        Aircraft,
        Wingman,
        Wing
    }

    public Type type;

    public AircraftTypeEnum AircraftType { get; private set; }
    public WingmanTypeEnum WingmanType { get; private set; }
    public WingTypeEnum WingType { get; private set; }

    public void CreateItem(AircraftTypeEnum aircraftType)
    {
        type = Type.Aircraft;
        this.AircraftType = aircraftType;

        Rank rank = CacheGame.GetSpaceShipRank(aircraftType);

        sSelected.SetActive(false);

        sIcon.spriteName = HangarValue.AircraftIconSpriteName(aircraftType, rank);
        Vector2 localScale = sIcon.transform.localScale;
        sIcon.MakePixelPerfect();
        sIcon.transform.localScale = localScale;

        sRank.spriteName = HangarValue.RankSpriteName[rank];
        sRank.MakePixelPerfect();

        int newMaxlevel = CacheGame.GetSpaceShipLevel(aircraftType);

        lLevel.text = newMaxlevel.ToString();

        if (CacheGame.GetSpaceShipIsUnlocked((int)aircraftType) == 0)
        {
            sLock.SetActive(true);
            lLevel.gameObject.SetActive(false);
            sRank.gameObject.SetActive(false);
        }
        else
        {
            sLock.SetActive(false);
            lLevel.gameObject.SetActive(true);
            sRank.gameObject.SetActive(true);
        }
    }

    public void CreateItem(WingmanTypeEnum wingmanType)
    {
        type = Type.Wingman;
        this.WingmanType = wingmanType;
        Rank rank = CacheGame.GetWingmanRank(wingmanType);
        sSelected.SetActive(false);
        sIcon.spriteName = HangarValue.WingmanIconSpriteName(wingmanType, rank);

        Vector2 localScale = sIcon.transform.localScale;
        sIcon.MakePixelPerfect();
        sIcon.transform.localScale = localScale;

        sRank.spriteName = HangarValue.RankSpriteName[rank];
        sRank.MakePixelPerfect();
        int newMaxlevel = CacheGame.GetWingmanLevel(wingmanType);
        lLevel.text = newMaxlevel.ToString();
        if (CacheGame.GetWingManIsUnlocked((int)wingmanType) == 0)
        {
            sLock.SetActive(true);
            lLevel.gameObject.SetActive(false);
            sRank.gameObject.SetActive(false);
        }
        else
        {
            sLock.SetActive(false);
            lLevel.gameObject.SetActive(true);
            sRank.gameObject.SetActive(true);
        }
    }

    public void CreateItem(WingTypeEnum wingType)
    {
        type = Type.Wing;
        this.WingType = wingType;
        Rank rank = CacheGame.GetWingRank(wingType);
        sSelected.SetActive(false);
        sIcon.spriteName = HangarValue.WingIconSpriteName(wingType, rank);

        Vector2 localScale = sIcon.transform.localScale;
        sIcon.MakePixelPerfect();
        sIcon.transform.localScale = localScale;

        sRank.spriteName = HangarValue.RankSpriteName[rank];
        sRank.MakePixelPerfect();
        int newMaxlevel = CacheGame.GetWingLevel(wingType);
        lLevel.text = newMaxlevel.ToString();
        if (CacheGame.GetWingIsUnlocked((int)wingType) == 0)
        {
            sLock.SetActive(true);
            lLevel.gameObject.SetActive(false);
            sRank.gameObject.SetActive(false);
        }
        else
        {
            sLock.SetActive(false);
            lLevel.gameObject.SetActive(true);
            sRank.gameObject.SetActive(true);
        }
    }

    public void SetAsSelected()
    {
        sSelected.SetActive(true);
        MessageDispatcher.SendMessage(this, EventID.ON_SELECTED_CALLING_BACKUP, this, 0);
    }

    public void SetAsDeselected()
    {
        sSelected.SetActive(false);
    }

    public void OnClickAircraft()
    {
        if (sSelected.activeInHierarchy)
            return;
        SetAsSelected();
    }
}
