﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OneSoftGame.Tools;
using Mp.Pvp;

public class PopupPVPInvite : PersistentSingleton<PopupPVPInvite>
{
    public GameObject g1vs1;
    public GameObject g2vs2;
    //1v1
    public UISprite sBackground;
    public UISprite sVipIcon;
    public UI2DSprite sAvatar;
    public UILabel lTitle;
    public UILabel lName;
    public UILabel lNameClan;
    public UILabel lLevel;
    public UILabel lStars;
    public UISprite sFlag;
    public UILabel lRank;
    public UISprite sRank1v1;
    public UISprite sRank2v2;
    public UILabel lMessage;
    public UILabel lBetting;
    //2v2
    public UILabel lMessage2v2;
    public List<UISprite> listSpritePlayer2v2;
    public UILabel lStatus2v2;

    const string PVP_INVITE = "popup_bg_small";
    const string TOURNAMENT_INVITE = "popup_bg_blue";

    const string AVATAR_IN = "avt_player_in";
    const string AVATAR_OUT = "avt_player_wait";

    public void ButtonGo()
    {
        if (PopupManager.Instance.typeInvite != CachePvp.TYPE_PVP_FRIENDS)
        {
            MinhCacheGame.SetBettingAmountPvP(CachePvp.numberCoin);
            if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
            {
                PopupManager.Instance.HideInviteButton();
                PopupManager.Instance.HideInvitePopup();
                new CSConfirmOpponentFindingNotify(0, CachePvp.numberCoin, tempToken, CacheGame.GetSpaceShipUserSelected(), CachePvp.typePVP).Send();
            }
            else if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
            {
                PopupManager.Instance.HideInviteButtonTournament();
                PopupManager.Instance.HideInvitePopupTournament();
                new CSConfirmOpponentFindingNotify(0, CachePvp.numberCoin, tempToken, CacheGame.GetSpaceShipTournament(), CachePvp.typePVP).Send();
            }
            else if (CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
            {
                PopupManager.Instance.HideInviteButton2v2();
                PopupManager.Instance.HideInvitePopup();
                if (PopupManager.Instance.typeInvite == CachePvp.TYPE_FRIEND_2v2)
                {
                    new CSCoopPvPConfirm(CSCoopPvPConfirm.ACCEPT, PopupManager.Instance.cachedPlayer.code, CachePvp.numberCoin).Send();
                }
                else
                {
                    new CSCoopPvPNotifyConfirm(tempToken, CSCoopPvPConfirm.ACCEPT, CachePvp.numberCoin, CacheGame.GetSpaceShipUserSelected(), PvPDataHelper.Instance.GetMyWaveCacheIndex(CachePvp.typePVP), PvPDataHelper.Instance.GetMyDataString(CachePvp.typePVP)).Send();
                }
            }

        }
        else
        {
            MinhCacheGame.SetBettingAmountPvP(0);
            new CSPvPConfirm(PopupManager.Instance.cachedPlayer.code, CSPvPConfirm.ACCEPT, CacheGame.GetSpaceShipUserSelected(), PvPDataHelper.Instance.GetMyWaveCacheIndex(), PvPDataHelper.Instance.GetMyDataString()).Send();
        }
    }

    public void ButtonClose()
    {
        if (PopupManager.Instance.typeInvite != CachePvp.TYPE_PVP_FRIENDS)
        {
            if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
            {
                PopupManager.Instance.HideInviteButton();
                PopupManager.Instance.HideInvitePopup();
                new CSConfirmOpponentFindingNotify(1, CachePvp.numberCoin, tempToken, CacheGame.GetSpaceShipUserSelected(), CachePvp.typePVP).Send();
            }
            else if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
            {
                PopupManager.Instance.HideInviteButtonTournament();
                PopupManager.Instance.HideInvitePopupTournament();
                new CSConfirmOpponentFindingNotify(1, CachePvp.numberCoin, tempToken, CacheGame.GetSpaceShipUserSelected(), CachePvp.typePVP).Send();
            }
            else if (CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
            {
                PopupManager.Instance.HideInviteButton2v2();
                PopupManager.Instance.HideInvitePopup();
                if (PopupManager.Instance.typeInvite == CachePvp.TYPE_FRIEND_2v2)
                {
                    new CSCoopPvPConfirm(CSCoopPvPConfirm.DENIED, PopupManager.Instance.cachedPlayer.code, CachePvp.numberCoin).Send();
                }
                else
                {
                    new CSCoopPvPNotifyConfirm(tempToken, CSCoopPvPNotifyConfirm.DENIED, CachePvp.numberCoin, CacheGame.GetSpaceShipUserSelected(), PvPDataHelper.Instance.GetMyWaveCacheIndex(CachePvp.typePVP), PvPDataHelper.Instance.GetMyDataString(CachePvp.typePVP)).Send();
                }
            }
        }
        else
        {
            new CSPvPConfirm(PopupManager.Instance.cachedPlayer.code, CSPvPConfirm.DENIED, CacheGame.GetSpaceShipUserSelected(), PvPDataHelper.Instance.GetMyWaveCacheIndex(), PvPDataHelper.Instance.GetMyDataString()).Send();
            PopupManager.Instance.HideInvitePopup();
        }
    }

    string tempToken;
    Vector3 tempVector3;

    public void SetInformation(string token, Player player, int gold, int typeInvite, int type = CSFindOpponent.TYPE_PVP)
    {
        if (typeInvite != CachePvp.TYPE_2v2)
        {
            g1vs1.SetActive(true);
            g2vs2.SetActive(false);
        }
        else
        {
            g1vs1.SetActive(false);
            g2vs2.SetActive(true);
        }
        Debug.Log("typeInvite : " + typeInvite);
        lBetting.text = "";
        tempToken = token;
        sBackground.spriteName = PVP_INVITE;
        lTitle.text = I2.Loc.ScriptLocalization.title_pvp_invitation;
        if (typeInvite == CachePvp.TYPE_PVP_FRIENDS)
        {
            lMessage.text = I2.Loc.ScriptLocalization.msg_challenge_friend;
            CachePvp.numberCoin = 0;
        }
        else if (typeInvite == CachePvp.TYPE_2v2)
        {
            lTitle.text = I2.Loc.ScriptLocalization.title_2vs2_invitation;
            lMessage2v2.text = I2.Loc.ScriptLocalization.msg_2vs2_invitation.Replace("%{number}", "[e1ac34]" + gold + "[-]");
            CachePvp.numberCoin = gold;
            lStatus2v2.text = I2.Loc.ScriptLocalization.player + ": " + PopupManager.Instance.listPlayersInvite.Count + "/4";
            for (int i = 0; i < listSpritePlayer2v2.Count; i++)
            {
                if (i < PopupManager.Instance.listPlayersInvite.Count)
                {
                    listSpritePlayer2v2[i].spriteName = AVATAR_IN;
                }
                else
                {
                    listSpritePlayer2v2[i].spriteName = AVATAR_OUT;
                }
            }
        }
        else
        {
            if (type == CSFindOpponent.TYPE_PVP)
            {
                lMessage.text = I2.Loc.ScriptLocalization.msg_pvp_invitation.Replace("%{betting_gold}", gold.ToString());
            }
            else if (type == CSFindOpponent.TYPE_MEGA)
            {
                lTitle.text = I2.Loc.ScriptLocalization.tournament;
                lMessage.text = I2.Loc.ScriptLocalization.msg_challenge_tournament;
                sBackground.spriteName = TOURNAMENT_INVITE;
            }
            else if (type == CSFindOpponent.TYPE_2V2)
            {
                Debug.Log("type : " + type);
                Debug.Log("typeInvite : " + typeInvite);
                lMessage.text = I2.Loc.ScriptLocalization.invited_to_2vs2;
                Debug.Log("I2.Loc.ScriptLocalization.invited_to_2vs2 : " + I2.Loc.ScriptLocalization.invited_to_2vs2);
                if (!string.IsNullOrEmpty(I2.Loc.ScriptLocalization.title_betting))
                {
                    lBetting.text = I2.Loc.ScriptLocalization.title_betting.Replace("%{gold}", gold.ToString());
                }
                else
                {
                    lBetting.text = I2.Loc.ScriptLocalization.title_betting;
                }
                tempToken = player.code;
            }
            CachePvp.numberCoin = gold;
        }
        PopupManager.Instance.typeInvite = typeInvite;
        if (typeInvite != CachePvp.TYPE_2v2)
        {
            PopupManager.Instance.cachedPlayer = player;
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(player.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                    {
                        showImageFB = false;
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                        if (t != null)
                        {
                            showImageFB = false;
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                        }
                    }
                }
            }
            if (showImageFB)
            {
                if (string.IsNullOrEmpty(player.facebookId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                }
                else
                {
                    if (!string.IsNullOrEmpty(player.facebookId))
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                        {
                            sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                        }
                        else
                        {
                            string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                            CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                        }
                    }
                }
            }

            lName.text = player.name;
            tempVector3.x = lNameClan.transform.localPosition.x;
            tempVector3.z = lNameClan.transform.localPosition.z;
            if (player.playerClan != null)
            {
                lNameClan.text = player.playerClan.name;
                tempVector3.y = 134f;
            }
            else
            {
                lNameClan.text = "";
                tempVector3.y = 153.3f;
            }
            lNameClan.transform.localPosition = tempVector3;
            int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);

            int[] starExtra = PvpUtil.StarExtra(PlayerDataUtil.StringToObject(player.data));
            int level1 = Mathf.Max(0, Mathf.Min(levelValue[0] - 1, GameContext.TOTAL_LEVEL));
            int level2 = Mathf.Max(0, Mathf.Min(levelValue[2] - 1, GameContext.TOTAL_LEVEL));
            int level3 = Mathf.Max(0, Mathf.Min(levelValue[4] - 1, GameContext.TOTAL_LEVEL));

            lLevel.text = I2.Loc.ScriptLocalization.level + ": " + level1 + ", " + level2 + ", " + level3;
            lStars.text = I2.Loc.ScriptLocalization.star + ": " + Mathf.Min(levelValue[1] + starExtra[0], PvpUtil.MaxStar(level1)) + ", " + Mathf.Min(levelValue[3] + starExtra[1], PvpUtil.MaxStar(level2)) + ", " + Mathf.Min(levelValue[5] + starExtra[2], PvpUtil.MaxStar(level3));
            sFlag.spriteName = player.country.ToLower();

            if (typeInvite != CachePvp.TYPE_FRIEND_2v2)
            {
                int rank = CachePvp.dictionaryRank[player.playerLevel.code];
                MedalRankTitle rankTitle = (MedalRankTitle)(rank);
                lRank.text = rankTitle + " " + CachePvp.ConvertChina(player.playerLevel.name);
                sRank1v1.spriteName = "PVP_rank_" + rank;
                sRank1v1.gameObject.SetActive(true);
                sRank2v2.gameObject.SetActive(false);
            }
            else
            {
                //lRank.text = CachePvp.MedalRank2v2Title(CachePvp.coopPvPTournamentInfo.info.chestName);
                //sRank2v2.spriteName = CachePvp.GetSpriteNameChest(CachePvp.coopPvPTournamentInfo.info.chestName);
                lRank.text = "";
                sRank1v1.gameObject.SetActive(false);
                sRank2v2.gameObject.SetActive(false);
            }
            int vip = CachePvp.GetVipFromVipPoint(player.vip);
            Debug.Log("player.vip : " + vip);
            if (vip == 0)
            {
                sVipIcon.gameObject.SetActive(false);
            }
            else
            {
                sVipIcon.gameObject.SetActive(true);
                sVipIcon.spriteName = "Icon__VIP_" + (vip - 1);
            }
        }
    }
}
