﻿using OneSoftGame.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupVipInfo : PersistentSingleton<PopupVipInfo>
{
    public void OnClickBack()
    {
        PopupManager.Instance.HideVipInfo();
    }
}
