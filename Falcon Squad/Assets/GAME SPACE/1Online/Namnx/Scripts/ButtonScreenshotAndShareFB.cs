﻿using com.ootii.Messages;
using Facebook.Unity;
using Mp.Pvp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ButtonScreenshotAndShareFB : MonoBehaviour
{
    public GameObject watermarkParent;
    public GameObject watermark;
    public GameObject background;
    public BoxCollider boxCollider;
    private float speed = 150f;
    private float amount = 4f;
    private Vector2 tempVector2;
    private Vector3 tempVector3;

    private void OnEnable()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        var versionClazz = new AndroidJavaClass("android.os.Build$VERSION");
        var apiLevel = versionClazz.GetStatic<int>("SDK_INT");
        if (apiLevel >= 24)
        {
            //android 8
            gameObject.SetActive(false);
        }
#endif
        if (GameContext.IS_CHINA_VERSION)
        {
            gameObject.SetActive(false);
        }
        watermarkParent.SetActive(false);
        background.SetActive(true);
        boxCollider.enabled = true;
    }

    public void ScreenshotAndShareFBEndPVP()
    {
        tempVector3.x = 10;
        tempVector3.y = 10;
        tempVector3.z = 10;
        watermarkParent.transform.localScale = tempVector3;
        watermarkParent.SetActive(true);
        boxCollider.enabled = false;
        background.SetActive(false);
        StopAllCoroutines();
        tempVector3.x = 1;
        tempVector3.y = 1;
        tempVector3.z = 1;
        string[] str = Directory.GetFiles(Application.persistentDataPath);
        for (int i = 0; i < str.Length; i++)
        {
            if (Path.GetExtension(str[i]).Equals(".png") && Path.GetFileName(str[i]).StartsWith("screenshot"))
            {
                File.Delete(str[i]);
            }
        }
        LeanTween.scale(watermarkParent, tempVector3, 0.1f).setOnComplete(() =>
          {
              StartCoroutine(WaitUntilCapture());
              //Debug.Log("done");
              //boxCollider.enabled = true;
              //background.SetActive(true);
              //watermarkParent.SetActive(false);
          }).setIgnoreTimeScale(true);
    }

    IEnumerator WaitUntilCapture()
    {
        int countFrame = 0;
        while (countFrame < 22)
        {
            tempVector2.x = Mathf.Sin(Time.time * speed) * amount;
            tempVector2.y = Mathf.Sin(Time.time * speed) * amount;
            watermark.transform.localPosition = tempVector2;
            countFrame++;
            yield return new WaitForEndOfFrame();
        }
        //Debug.Log("done");
        //boxCollider.enabled = true;
        //background.SetActive(true);
        //watermarkParent.SetActive(false);
        //yield break;
        DateTime dt = DateTime.Now;
        int d = (int)dt.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        ScreenCapture.CaptureScreenshot("screenshot" + d + ".png");
        yield return new WaitUntil(() => File.Exists(Path.Combine(Application.persistentDataPath, "screenshot" + d + ".png")));

        SocialConnector.Share("", "http://falconsquad.net/falcon-squad.html", Path.Combine(Application.persistentDataPath, "screenshot" + d + ".png"));
        boxCollider.enabled = true;
        background.SetActive(true);
        watermarkParent.SetActive(false);
    }
}