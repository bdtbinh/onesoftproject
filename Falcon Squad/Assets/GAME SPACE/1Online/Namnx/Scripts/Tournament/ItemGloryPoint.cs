﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mp.Pvp;
using com.ootii.Messages;
using System;

public class ItemGloryPoint : MonoBehaviour
{

    public UISprite sNumberBackground;
    public UISprite sBackground;
    public UILabel lTextNumber;

    public GameObject buttonTop;
    public UISprite sRewardTop;
    public UILabel lRewardTop;
    public GameObject gLockTop;
    public GameObject gConfirmTop;
    public GameObject gFadeTop;

    public GameObject buttonBottom;
    public UISprite sRewardBottom;
    public UILabel lRewardBottom;
    public GameObject gLockBottom;
    public GameObject gConfirmBottom;
    public GameObject gFadeBottom;

    Dictionary<int, Item> dictRewardTournamentBonus = new Dictionary<int, Item>();
    Dictionary<int, Item> dictRewardTournamentFree = new Dictionary<int, Item>();

    private const string WINSTREAK_DEACTIVE = "Winstreak_point_o";
    private const string WINSTREAK_ACTIVE = "Winstreak_point_on";

    private const string GLORY_DEACTIVE = "tournament_count_BG";
    private const string GLORY_ACTIVE = "Glory_slide_filled";

    Item item_top;
    Item item_bottom;
    int m_win;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Tournament.MegaTournamentClaim.ToString(), OnMegaTournamentClaim, true);
        MessageDispatcher.AddListener(EventName.Tournament.MegaTournamentUnlockBonus.ToString(), OnMegaTournamentUnlockBonus, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Tournament.MegaTournamentClaim.ToString(), OnMegaTournamentClaim, true);
        MessageDispatcher.RemoveListener(EventName.Tournament.MegaTournamentUnlockBonus.ToString(), OnMegaTournamentUnlockBonus, true);
    }

    private void OnMegaTournamentUnlockBonus(IMessage rMessage)
    {
        SCMegaTournamentUnlockBonus mtub = rMessage.Data as SCMegaTournamentUnlockBonus;
        if (mtub.status == SCMegaTournamentUnlockBonus.SUCCESS)
        {
            gLockTop.SetActive(false);
        }
    }

    private void OnMegaTournamentClaim(IMessage rMessage)
    {
        SCMegaTournamentClaim c = rMessage.Data as SCMegaTournamentClaim;
        if (c.status == SCMegaTournamentClaim.SUCCESS)
        {
            if (c.bonus)
            {
                if (m_win == c.key)
                {
                    gConfirmTop.SetActive(true);
                }
            }
            else
            {
                if (m_win == c.key)
                {
                    gConfirmBottom.SetActive(true);
                }
            }
        }
    }

    public void SetDataTop(int win, Item item)
    {
        m_win = win;
        item_top = item;

        if (CachePvp.megaTournamentInfo.info.win >= win)
        {
            sNumberBackground.spriteName = WINSTREAK_ACTIVE;
            sBackground.spriteName = GLORY_ACTIVE;
        }
        else
        {
            sNumberBackground.spriteName = WINSTREAK_DEACTIVE;
            sBackground.spriteName = GLORY_DEACTIVE;
        }
        lTextNumber.text = (win).ToString();
        if (item == null)
        {
            buttonTop.SetActive(false);
        }
        else
        {
            buttonTop.SetActive(true);
            sRewardTop.spriteName = FalconMail.GetSpriteName(item.key, item.value);
            lRewardTop.text = GameContext.FormatNumber(item.value);
            if (item.key == FalconMail.ITEM_BOX_CARD1 || item.key == FalconMail.ITEM_BOX_CARD2 || item.key == FalconMail.ITEM_BOX_CARD3 || item.key == FalconMail.ITEM_BOX_CARD4)
            {
                lRewardTop.text = "";
            }
            if (CachePvp.megaTournamentInfo == null)
            {
                //chua bat dau tournament
                gFadeTop.SetActive(true);
                gLockTop.SetActive(true);
                gConfirmTop.SetActive(false);
            }
            else
            {
                //da bat dau tournament
                if (IsPass(win)) //da pass
                {
                    gFadeTop.SetActive(false);
                }
                else
                {
                    gFadeTop.SetActive(true);
                }
                if (CachePvp.megaTournamentInfo.info.unlocked)
                {
                    //da unlock bonus
                    gLockTop.SetActive(false);
                    bool rewarded = IsGotReward(win);
                    if (rewarded)
                    {
                        //da nhan qua
                        gConfirmTop.SetActive(true);
                    }
                    else
                    {
                        //chua nhan qua
                        gConfirmTop.SetActive(false);
                    }
                }
                else
                {
                    //chua unlock bonus
                    gLockTop.SetActive(true);
                    gConfirmTop.SetActive(false);
                }
            }
        }
    }

    bool IsPass(int win, bool top = true)
    {
        for (int i = 0; i < CachePvp.megaTournamentInfo.info.rewards.Count; i++)
        {
            if (CachePvp.megaTournamentInfo.info.rewards[i].win == win)
            {
                if (top)
                {
                    return true;
                }
                else if (!top)
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool IsGotReward(int win, bool top = true)
    {
        for (int i = 0; i < CachePvp.megaTournamentInfo.info.rewards.Count; i++)
        {
            if (CachePvp.megaTournamentInfo.info.rewards[i].win == win)
            {
                if (top && CachePvp.megaTournamentInfo.info.rewards[i].bonusRewarded)
                {
                    return true;
                }
                else if (!top && CachePvp.megaTournamentInfo.info.rewards[i].rewarded)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void SetDataBottom(int win, Item item)
    {
        item_bottom = item;
        if (item == null)
        {
            buttonBottom.SetActive(false);
        }
        else
        {
            buttonBottom.SetActive(true);
            sRewardBottom.spriteName = FalconMail.GetSpriteName(item.key, item.value);
            lRewardBottom.text = GameContext.FormatNumber(item.value);
            if (item.key == FalconMail.ITEM_BOX_CARD1 || item.key == FalconMail.ITEM_BOX_CARD2 || item.key == FalconMail.ITEM_BOX_CARD3 || item.key == FalconMail.ITEM_BOX_CARD4)
            {
                lRewardBottom.text = "";
            }
            gLockBottom.SetActive(false);
            if (CachePvp.megaTournamentInfo == null)
            {
                //chua bat dau tournament
                gFadeBottom.SetActive(true);
                gConfirmBottom.SetActive(false);
            }
            else
            {
                //da bat dau tournament
                if (IsPass(win, false))
                {
                    gFadeBottom.SetActive(false);
                }
                else
                {
                    gFadeBottom.SetActive(true);
                }
                bool rewarded = false;
                rewarded = IsGotReward(win, false);
                if (rewarded)
                {
                    //da nhan qua
                    gConfirmBottom.SetActive(true);
                }
                else
                {
                    //chua nhan qua
                    gConfirmBottom.SetActive(false);
                }
            }
        }
    }

    public void ButtonTop()
    {
        if (gFadeTop.activeInHierarchy)
        {
            if (CachePvp.megaTournamentInfo == null)
            {
                //chua start tournament
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_tourmanet_not_start_yet, false, 1.5f);
            }
            else if (m_win > CachePvp.megaTournamentInfo.info.win)
            {
                //win first
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_win_battle_first.Replace("%{battle}", m_win.ToString()), false, 1.5f);
            }
            return;
        }
        if (gLockTop.activeInHierarchy)
        {
            //chua unlock
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_bonus_reward_first, false, 1.5f);
            return;
        }
        if (gConfirmTop.activeInHierarchy)
        {
            //da nhan qua
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_claimed_reward, false, 1.5f);
            return;
        }
        new CSMegaTournamentClaim(m_win, true).Send();
    }

    public void ButtonBottom()
    {
        if (gFadeBottom.activeInHierarchy)
        {
            if (CachePvp.megaTournamentInfo == null)
            {
                //chua start tournament
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_tourmanet_not_start_yet, false, 1.5f);
            }
            else if (m_win > CachePvp.megaTournamentInfo.info.win)
            {
                //win first
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_win_battle_first.Replace("%{battle}", m_win.ToString()), false, 1.5f);
            }
            return;
        }
        if (gConfirmBottom.activeInHierarchy)
        {
            //da nhan qua
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_claimed_reward, false, 1.5f);
            return;
        }
        new CSMegaTournamentClaim(m_win, false).Send();
    }
}