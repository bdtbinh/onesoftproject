﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;
using Mp.Pvp;

public class ItemBonusReward : MonoBehaviour
{

    public UILabel lStatus;
    public UISprite sprite;
    public UILabel lValue;
    public GameObject gLock;
    public GameObject gConfirm;
    public GameObject gFade;

    private const string WINSTREAK_DEACTIVE = "Winstreak_point_o";
    private const string WINSTREAK_ACTIVE = "Winstreak_point_on";

    private const string GLORY_DEACTIVE = "tournament_count_BG";
    private const string GLORY_ACTIVE = "Glory_slide_filled";

    bool m_bonus;
    MegaTournamentReward m_megaTournamentRewardConfig;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Tournament.MegaTournamentClaim.ToString(), OnMegaTournamentClaim, true);
        MessageDispatcher.AddListener(EventName.Tournament.MegaTournamentUnlockBonus.ToString(), OnMegaTournamentUnlockBonus, true);
    }

    private void OnMegaTournamentUnlockBonus(IMessage rMessage)
    {
        SCMegaTournamentUnlockBonus mtub = rMessage.Data as SCMegaTournamentUnlockBonus;
        if (mtub.status == SCMegaTournamentUnlockBonus.SUCCESS)
        {
            gLock.SetActive(false);
        }
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Tournament.MegaTournamentClaim.ToString(), OnMegaTournamentClaim, true);
        MessageDispatcher.RemoveListener(EventName.Tournament.MegaTournamentUnlockBonus.ToString(), OnMegaTournamentUnlockBonus, true);
    }

    private void OnMegaTournamentClaim(IMessage rMessage)
    {
        SCMegaTournamentClaim c = rMessage.Data as SCMegaTournamentClaim;
        if (c.status == SCMegaTournamentClaim.SUCCESS)
        {
            if (c.bonus)
            {
                if (m_megaTournamentRewardConfig.win == c.key)
                {
                    gConfirm.SetActive(true);
                }
            }
            else
            {
                if (m_megaTournamentRewardConfig.win == c.key)
                {
                    gConfirm.SetActive(true);
                }
            }
        }
    }

    bool IsPass(int win, bool top = true)
    {
        for (int i = 0; i < CachePvp.megaTournamentInfo.info.rewards.Count; i++)
        {
            if (CachePvp.megaTournamentInfo.info.rewards[i].win == win)
            {
                if (top)
                {
                    return true;
                }
                else if (!top)
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool IsGotReward(int win, bool top = true)
    {
        for (int i = 0; i < CachePvp.megaTournamentInfo.info.rewards.Count; i++)
        {
            if (CachePvp.megaTournamentInfo.info.rewards[i].win == win)
            {
                if (top && CachePvp.megaTournamentInfo.info.rewards[i].bonusRewarded)
                {
                    return true;
                }
                else if (!top && CachePvp.megaTournamentInfo.info.rewards[i].rewarded)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void SetData(int id, bool bonus = false)
    {
        m_bonus = bonus;
        m_megaTournamentRewardConfig = CachePvp.megaTournamentInfo.configRewards[id];
        lStatus.text = m_megaTournamentRewardConfig.win + " " + I2.Loc.ScriptLocalization.win;
        if (bonus)
        {
            lValue.text = GameContext.FormatNumber(m_megaTournamentRewardConfig.bonus.value);
            sprite.spriteName = FalconMail.GetSpriteName(m_megaTournamentRewardConfig.bonus.key, m_megaTournamentRewardConfig.bonus.value);
        }
        else
        {
            lValue.text = GameContext.FormatNumber(m_megaTournamentRewardConfig.free.value);
            sprite.spriteName = FalconMail.GetSpriteName(m_megaTournamentRewardConfig.free.key, m_megaTournamentRewardConfig.free.value);
        }

        if (CachePvp.megaTournamentInfo == null)
        {
            //chua bat dau tournament
            gFade.SetActive(true);
            gLock.SetActive(true);
            gConfirm.SetActive(false);
        }
        else
        {
            //da bat dau tournament
            if (IsPass(m_megaTournamentRewardConfig.win, m_bonus))
            {
                gFade.SetActive(false);
            }
            else
            {
                gFade.SetActive(true);
            }
            if (bonus)
            {
                if (CachePvp.megaTournamentInfo.info.unlocked)
                {
                    //da unlock bonus
                    gLock.SetActive(false);
                    bool rewarded = false;
                    rewarded = IsGotReward(m_megaTournamentRewardConfig.win, true);
                    if (rewarded)
                    {
                        //da nhan qua
                        gConfirm.SetActive(true);
                    }
                    else
                    {
                        //chua nhan qua
                        gConfirm.SetActive(false);
                    }
                }
                else
                {
                    //chua unlock bonus
                    gLock.SetActive(true);
                    gConfirm.SetActive(false);
                }
            }
            else
            {
                //free
                bool rewarded = false;
                rewarded = IsGotReward(m_megaTournamentRewardConfig.win, false);
                if (rewarded)
                {
                    //da nhan qua
                    gConfirm.SetActive(true);
                }
                else
                {
                    //chua nhan qua
                    gConfirm.SetActive(false);
                }
            }
        }
        if (!bonus)
        {
            gLock.SetActive(false);
        }
    }

    public void ClickButton()
    {
        if (gFade.activeInHierarchy)
        {
            if (CachePvp.megaTournamentInfo == null)
            {
                //chua start tournament
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_tourmanet_not_start_yet, false, 1.5f);
            }
            else if (m_megaTournamentRewardConfig.win > CachePvp.megaTournamentInfo.info.win)
            {
                //win first
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_win_battle_first.Replace("%{battle}", m_megaTournamentRewardConfig.win.ToString()), false, 1.5f);
            }
            return;
        }
        if (gLock.activeInHierarchy)
        {
            //chua unlock
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_bonus_reward_first, false, 1.5f);
            return;
        }
        if (gConfirm.activeInHierarchy)
        {
            //da nhan qua
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_claimed_reward, false, 1.5f);
            return;
        }
        Item item;
        if (m_bonus)
        {
            item = m_megaTournamentRewardConfig.bonus;
        }
        else
        {
            item = m_megaTournamentRewardConfig.free;
        }
        new CSMegaTournamentClaim(m_megaTournamentRewardConfig.win, m_bonus).Send();
    }

    public int ChangeTypeToFalconMail(GameContext.TypeItemInPopupItemReward item)
    {
        switch (item)
        {
            case GameContext.TypeItemInPopupItemReward.Gold:
                return FalconMail.ITEM_GOLD;
            case GameContext.TypeItemInPopupItemReward.Gem:
                return FalconMail.ITEM_GEM;
            case GameContext.TypeItemInPopupItemReward.PowerUp:
                return FalconMail.ITEM_POWER_UP;
            case GameContext.TypeItemInPopupItemReward.EMP:
                return FalconMail.ITEM_ENERGY;
            case GameContext.TypeItemInPopupItemReward.Life:
                return FalconMail.ITEM_LIFE;
            case GameContext.TypeItemInPopupItemReward.AircraftGeneralCard:
                return FalconMail.ITEM_CARD_PLANE_GENERAL;
            case GameContext.TypeItemInPopupItemReward.Aircraft1Card:
                return FalconMail.ITEM_CARD_PLANE_1;
            case GameContext.TypeItemInPopupItemReward.Aircraft2Card:
                return FalconMail.ITEM_CARD_PLANE_2;
            case GameContext.TypeItemInPopupItemReward.Aircraft3Card:
                return FalconMail.ITEM_CARD_PLANE_3;
            case GameContext.TypeItemInPopupItemReward.Aircraft6Card:
                return FalconMail.ITEM_CARD_PLANE_4;
            case GameContext.TypeItemInPopupItemReward.Aircraft7Card:
                return FalconMail.ITEM_CARD_PLANE_5;
            case GameContext.TypeItemInPopupItemReward.Aircraft8Card:
                return FalconMail.ITEM_CARD_PLANE_6;
            case GameContext.TypeItemInPopupItemReward.Aircraft9Card:
                return FalconMail.ITEM_CARD_PLANE_7;
            case GameContext.TypeItemInPopupItemReward.Aircraft10Card:
                return FalconMail.ITEM_CARD_PLANE_8;
            case GameContext.TypeItemInPopupItemReward.DroneGeneralCard:
                return FalconMail.ITEM_CARD_WINGMAN_GENERAL;
            case GameContext.TypeItemInPopupItemReward.Drone1Card:
                return FalconMail.ITEM_CARD_WINGMAN_1;
            case GameContext.TypeItemInPopupItemReward.Drone2Card:
                return FalconMail.ITEM_CARD_WINGMAN_2;
            case GameContext.TypeItemInPopupItemReward.Drone3Card:
                return FalconMail.ITEM_CARD_WINGMAN_3;
            case GameContext.TypeItemInPopupItemReward.Drone4Card:
                return FalconMail.ITEM_CARD_WINGMAN_4;
            case GameContext.TypeItemInPopupItemReward.Drone5Card:
                return FalconMail.ITEM_CARD_WINGMAN_5;
            case GameContext.TypeItemInPopupItemReward.Drone6Card:
                return FalconMail.ITEM_CARD_WINGMAN_6;
            case GameContext.TypeItemInPopupItemReward.WingGeneralCard:
                return FalconMail.ITEM_CARD_WING_GENERAL;
            case GameContext.TypeItemInPopupItemReward.Wing1Card:
                return FalconMail.ITEM_CARD_WING_1;
            case GameContext.TypeItemInPopupItemReward.Wing2Card:
                return FalconMail.ITEM_CARD_WING_2;
            case GameContext.TypeItemInPopupItemReward.Wing3Card:
                return FalconMail.ITEM_CARD_WING_3;
            case GameContext.TypeItemInPopupItemReward.Energy:
                return FalconMail.ITEM_ENERGY;
            default:
                return 0;
        }
    }
}
