﻿using OneSoftGame.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupClanCongratulation : PersistentSingleton<PopupClanCongratulation>
{
    public UISprite sAvatarClan;
    public UILabel lNameClan;

    public void Refresh(int index, string name)
    {
        Debug.Log("refresh index : " + index);
        if ((index + 2) < 10)
        {
            sAvatarClan.spriteName = "avt_Avt_0" + (index + 2);
        }
        else
        {
            sAvatarClan.spriteName = "avt_Avt_" + (index + 2);
        }
        lNameClan.text = name;
    }

    public void OnClickOK()
    {
        PopupManager.Instance.HideClanCongratulationPopup();
        //PopupManager.Instance.HideClanPopup();
    }
}
