﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;

public class PopupBuyTicket : MonoBehaviour
{
    public UILabel lBuy1Ticket;
    public UILabel lBuy1TicketValue;

    public UILabel lBuy5Ticket;
    public UILabel lBuy5TicketValue;
    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Tournament.MegaTournamentInfo.ToString(), OnMegaTournamentInfo, true);
        if (CachePvp.megaTournamentInfo != null && CachePvp.megaTournamentInfo.ticketSales != null)
        {
            lBuy1Ticket.text = I2.Loc.ScriptLocalization.msg_buy_ticket.Replace("%{ticket}", CachePvp.megaTournamentInfo.ticketSales[0].ticket.ToString());
            lBuy1TicketValue.text = CachePvp.megaTournamentInfo.ticketSales[0].gem.ToString();
            lBuy5Ticket.text = I2.Loc.ScriptLocalization.msg_buy_ticket.Replace("%{ticket}", CachePvp.megaTournamentInfo.ticketSales[1].ticket.ToString());
            lBuy5TicketValue.text = CachePvp.megaTournamentInfo.ticketSales[1].gem.ToString();
        }
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Tournament.MegaTournamentInfo.ToString(), OnMegaTournamentInfo, true);
    }

    private void OnMegaTournamentInfo(IMessage rMessage)
    {
        if (CachePvp.megaTournamentInfo != null && CachePvp.megaTournamentInfo.ticketBought != null)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_buy_ticket_success, true, 1.5f);
            LeanTween.delayedCall(0.1f, () =>
            {
                bBack();
            });
        }
    }

    public void PressButtonBuy1Ticket()
    {
        if (CachePvp.megaTournamentInfo != null && CachePvp.megaTournamentInfo.ticketSales != null)
        {
            if (CachePvp.GetGem() < CachePvp.megaTournamentInfo.ticketSales[0].gem)
            {
                //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_gem, false, 1.5f);
                PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
                return;
            }
            new CSMegaTournamentBuyTicket(CachePvp.megaTournamentInfo.ticketSales[0].ticket).Send();
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_please_wait_from_server, false, 1.5f);
        }
    }

    public void PressButtonBuy5Ticket()
    {
        if (CachePvp.megaTournamentInfo != null && CachePvp.megaTournamentInfo.ticketSales != null)
        {
            if (CachePvp.GetGem() < CachePvp.megaTournamentInfo.ticketSales[1].gem)
            {
                //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_gem, false, 1.5f);
                PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
                return;
            }
            new CSMegaTournamentBuyTicket(CachePvp.megaTournamentInfo.ticketSales[1].ticket).Send();
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_please_wait_from_server, false, 1.5f);
        }
    }

    public void bBack()
    {
        PopupManager.Instance.HideBuyTicketPopup();
    }
}
