﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;

public class PopupChatWorld : MonoBehaviour
{
    public GameObject bgNotify;
    public UILabel lTextNotify;
    public UISprite sAnimDot;

    public GameObject gFade;
    public GameObject gHeadbar;
    public GameObject gFriends;
    public GameObject gClan;
    public GameObject gEmoji;

    public UISprite sBtnWorld;
    public UILabel lBtnWorld;
    public UISprite sBtnLocal;
    public UILabel lBtnLocal;

    public Transform listWrap;
    public GameObject fxloading;
    public GameObject lNoFriends;
    public GameObject lNoClan;

    public UIInput inputButtonChat;

    private const string TAB_ACTIVE = "2vs2_list_tab_2";
    private const string TAB_DEACTIVE = "2vs2_list_tab_0";

    private const string ANIM_DOT = "worldchat_popdot_anim-animation_";

    public bool IsShow()
    {
        return isShow;
    }

    bool isShow;

    public void bShow()
    {
        if (isShow)
        {
            isShow = false;
            Hide();
        }
        else
        {
            if (!OSNet.NetManager.Instance.IsOnline())
            {
                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
                return;
            }
            isShow = true;
            Show();
        }
    }

    public void ShowButton()
    {
        bgNotify.SetActive(false);
        gEmoji.SetActive(false);
        StartCoroutine(Wait1Frame());
    }

    IEnumerator Wait1Frame()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        vt2.x = 690;
        vt2.y = 0;
        transform.localPosition = vt2;
        StartCoroutine(AnimationDot());
    }

    IEnumerator AnimationDot()
    {
        int i = 0;
        while (true)
        {
            sAnimDot.spriteName = ANIM_DOT + i;
            i++;
            if (i == 10)
            {
                i = 0;
                yield return new WaitForSecondsRealtime(0.95f);
            }
            yield return new WaitForSecondsRealtime(0.05f);
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    void Show()
    {
        LeanTween.cancel(gameObject);
        LeanTween.moveLocalX(gameObject, 0, 0.5f).setOnComplete(() =>
        {
            gFade.SetActive(true);
            gHeadbar.SetActive(true);
        }
        ).setEase(LeanTweenType.easeOutQuart).setIgnoreTimeScale(true);

        readyClickBack = true;
        gEmoji.SetActive(false);
        for (int i = 0; i < listWrap.childCount; i++)
        {
            listWrap.GetChild(i).gameObject.SetActive(false);
        }
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadedListFriends.ToString(), OnLoadedListFriends, true);
        MessageDispatcher.AddListener(EventName.Clan.PlayerInfo.ToString(), OnLoadPlayerInfo, true);
        MessageDispatcher.AddListener(EventName.Clan.ClanInvitation.ToString(), OnClanInvitation, true);
        MessageDispatcher.AddListener(EventName.Clan.LoadChat.ToString(), OnLoadChat, true);

        sBtnWorld.spriteName = TAB_DEACTIVE;
        sBtnLocal.spriteName = TAB_DEACTIVE;
        ButtonWorld();
    }

    void Hide()
    {
        isShow = false;
        gFade.SetActive(false);
        gHeadbar.SetActive(false);
        LeanTween.cancel(gameObject);
        LeanTween.moveLocalX(gameObject, 690, 0.5f).setEase(LeanTweenType.easeInQuart).setIgnoreTimeScale(true);

        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadedListFriends.ToString(), OnLoadedListFriends, true);
        MessageDispatcher.RemoveListener(EventName.Clan.LoadChat.ToString(), OnLoadChat, true);
        MessageDispatcher.RemoveListener(EventName.Clan.PlayerInfo.ToString(), OnLoadPlayerInfo, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ClanInvitation.ToString(), OnClanInvitation, true);
    }

    private void OnClanInvitation(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCClanInvitation.SUCCESS)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_have_invited_to_friend, true);
        }
        else if (status == SCClanInvitation.UNAUTHORITY)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_permission_error, true);
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(rMessage.Data.ToString(), true);
        }
    }

    private void OnLoadPlayerInfo(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        if (status == SCPlayerInfo.SUCCESS)
        {
            Player p = rMessage.Data as Player;
            if (CachePvp.isInvite)
            {
                if (p.playerClan != null)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.title_player_already_in_a_team, false);
                }
                else
                {
                    new CSClanInvitation(p.code).Send();
                }
            }
            else
            {
                if (!PopupManager.Instance.IsProfilePopupActive())
                {
                    PopupManager.Instance.ShowProfilePopup(p);
                }
            }
        }
    }

    public void ButtonHideEmoji()
    {
        gEmoji.SetActive(false);
    }

    public void ButtonShowEmoji()
    {
        if (gEmoji.activeInHierarchy)
        {
            gEmoji.SetActive(false);
        }
        else
        {
            gEmoji.SetActive(true);
        }
    }

    public void ButtonChatEmoji1()
    {
        if (sBtnWorld.spriteName.Equals(TAB_ACTIVE))
        {
            new CSClanChat(CSClanChat.CHAT_WORLD, "/Emo1_1").Send();
        }
        else
        {
            new CSClanChat(CSClanChat.CHAT_LOCAL, "/Emo1_1").Send();
        }
        ButtonHideEmoji();
    }

    public void ButtonChatEmoji2()
    {
        if (sBtnWorld.spriteName.Equals(TAB_ACTIVE))
        {
            new CSClanChat(CSClanChat.CHAT_WORLD, "/Emo1_2").Send();
        }
        else
        {
            new CSClanChat(CSClanChat.CHAT_LOCAL, "/Emo1_2").Send();
        }
        ButtonHideEmoji();
    }

    public void ButtonChatEmoji3()
    {
        if (sBtnWorld.spriteName.Equals(TAB_ACTIVE))
        {
            new CSClanChat(CSClanChat.CHAT_WORLD, "/Emo1_3").Send();
        }
        else
        {
            new CSClanChat(CSClanChat.CHAT_LOCAL, "/Emo1_3").Send();
        }
        ButtonHideEmoji();
    }

    public void ButtonChatEmoji4()
    {
        if (sBtnWorld.spriteName.Equals(TAB_ACTIVE))
        {
            new CSClanChat(CSClanChat.CHAT_WORLD, "/Emo1_4").Send();
        }
        else
        {
            new CSClanChat(CSClanChat.CHAT_LOCAL, "/Emo1_4").Send();
        }
        ButtonHideEmoji();
    }

    public void ButtonChatEmoji5()
    {
        if (sBtnWorld.spriteName.Equals(TAB_ACTIVE))
        {
            new CSClanChat(CSClanChat.CHAT_WORLD, "/Emo1_5").Send();
        }
        else
        {
            new CSClanChat(CSClanChat.CHAT_LOCAL, "/Emo1_5").Send();
        }
        ButtonHideEmoji();
    }

    public void ButtonChatEmoji6()
    {
        if (sBtnWorld.spriteName.Equals(TAB_ACTIVE))
        {
            new CSClanChat(CSClanChat.CHAT_WORLD, "/Emo1_6").Send();
        }
        else
        {
            new CSClanChat(CSClanChat.CHAT_LOCAL, "/Emo1_6").Send();
        }
        ButtonHideEmoji();
    }

    public void ButtonChatEmoji7()
    {
        if (sBtnWorld.spriteName.Equals(TAB_ACTIVE))
        {
            new CSClanChat(CSClanChat.CHAT_WORLD, "/Emo1_7").Send();
        }
        else
        {
            new CSClanChat(CSClanChat.CHAT_LOCAL, "/Emo1_7").Send();
        }
        ButtonHideEmoji();
    }

    public void ButtonChatEmoji8()
    {
        if (sBtnWorld.spriteName.Equals(TAB_ACTIVE))
        {
            new CSClanChat(CSClanChat.CHAT_WORLD, "/Emo1_8").Send();
        }
        else
        {
            new CSClanChat(CSClanChat.CHAT_LOCAL, "/Emo1_8").Send();
        }
        ButtonHideEmoji();
    }

    public void ButtonSubmitChat()
    {
        if (string.IsNullOrEmpty(inputButtonChat.value.Trim()))
        {
            return;
        }
        if (canChat)
        {
            canChat = false;
            if (wait3seconds != null)
            {
                StopCoroutine(wait3seconds);
            }
            wait3seconds = Wait3Second();
            StartCoroutine(wait3seconds);
            if (sBtnWorld.spriteName.Equals(TAB_ACTIVE))
            {
                new CSClanChat(CSClanChat.CHAT_WORLD, inputButtonChat.value).Send();
                inputButtonChat.value = "";
            }
            else
            {
                new CSClanChat(CSClanChat.CHAT_LOCAL, inputButtonChat.value).Send();
                inputButtonChat.value = "";
            }
        }
        else
        {
            bgNotify.SetActive(true);
        }
    }

    Vector2 vt2;

    private void OnLoadChat(IMessage rMessage)
    {
        int type = (int)rMessage.Recipient;
        List<Chat> list = rMessage.Data as List<Chat>;
        MessageDispatcher.SendMessage(gameObject, type, EventName.Clan.LoadedChat.ToString(), list, 0);
        fxloading.SetActive(false);
    }

    private void OnLoadedListFriends(IMessage rMessage)
    {
        SCMyFriends mf = rMessage.Data as SCMyFriends;
        if (mf.status == SCMyFriends.SUCCESS)
        {
            fxloading.SetActive(false);
            if (mf.total == 0)
            {
                lNoFriends.SetActive(true);
            }
            else
            {
                lNoFriends.SetActive(false);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(mf.message, false, 1.5f);
        }
    }

    public void ButtonBack()
    {
        if (bgNotify.activeInHierarchy)
        {
            bgNotify.SetActive(false);
        }
        else if (gEmoji.activeInHierarchy)
        {
            gEmoji.SetActive(false);
        }
        else
        {
            Hide();
        }
    }

    public void ButtonWorld()
    {
        if (sBtnWorld.spriteName.Equals(TAB_DEACTIVE))
        {
            for (int i = 0; i < gFriends.transform.GetChild(0).childCount; i++)
            {
                gFriends.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            }
            lNoClan.SetActive(false);
            lNoFriends.SetActive(false);
            fxloading.SetActive(false);
            gFriends.SetActive(true);
            gClan.SetActive(false);
            sBtnWorld.spriteName = TAB_ACTIVE;
            lBtnWorld.color = Color.white;
            lBtnWorld.fontSize = 30;
            sBtnLocal.spriteName = TAB_DEACTIVE;
            lBtnLocal.color = Color.grey;
            lBtnLocal.fontSize = 20;
            fxloading.SetActive(true);
            new CSClanJoinChat(CSClanJoinChat.TYPE_JOIN_ROOM_WORLD).Send();
        }
    }

    public void ButtonLocal()
    {
        if (sBtnLocal.spriteName.Equals(TAB_DEACTIVE))
        {
            for (int i = 0; i < gClan.transform.GetChild(0).childCount; i++)
            {
                gClan.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            }
            lNoFriends.SetActive(false);
            lNoClan.SetActive(false);
            fxloading.SetActive(true);
            gFriends.SetActive(false);
            gClan.SetActive(true);
            sBtnLocal.spriteName = TAB_ACTIVE;
            lBtnLocal.color = Color.white;
            lBtnLocal.fontSize = 30;
            sBtnWorld.spriteName = TAB_DEACTIVE;
            lBtnWorld.color = Color.grey;
            lBtnWorld.fontSize = 20;

            new CSClanJoinChat(CSClanJoinChat.TYPE_JOIN_ROOM_LOCAL).Send();
        }
    }

    private bool readyClickBack;

    IEnumerator ReadyClickBack()
    {
        readyClickBack = false;
        yield return new WaitForSecondsRealtime(0.38f);
        readyClickBack = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && readyClickBack)
        {
            StartCoroutine(ReadyClickBack());
            ButtonBack();
        }
    }

    public void ButtonCloseNotify()
    {
        bgNotify.SetActive(false);
    }

    IEnumerator wait3seconds;
    bool canChat = true;

    IEnumerator Wait3Second()
    {
        canChat = false;
        for (int i = 3; i > 0; i--)
        {
            lTextNotify.text = I2.Loc.ScriptLocalization.title_wait_3_seconds.Replace("%{value}", i.ToString());
            yield return new WaitForSecondsRealtime(1);
        }
        canChat = true;
        ButtonCloseNotify();
    }
}
