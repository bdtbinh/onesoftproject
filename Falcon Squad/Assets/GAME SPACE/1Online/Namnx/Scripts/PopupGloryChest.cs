﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using UnityEngine.SceneManagement;

public class PopupGloryChest : MonoBehaviour
{
    public UILabel lStatusTime;

    public GameObject buttonClaim;
    public GameObject buttonClaimed;
    public GameObject buttonClaimDeactive;

    public List<UISprite> listSpriteButtonBox;

    public UISprite sBarProgress;
    public UILabel lbarProgress;

    public List<UISprite> listSpriteBackgroundGlory;
    public List<UILabel> listLabelBackgroundNumber;
    public List<UILabel> listLabelGloryPointNumber;

    public List<UILabel> listLabelInfo;

    public GameObject panelInfo;

    public UISprite sButtonClaim;

    public GameObject gLabelNotClan;

    public List<Transform> listsBarSplit;

    private const string BTN_BLUE = "btn_blue";
    private const string BTN_GREY = "btn_daily_grey";

    private const string GLORY_DEACTIVE = "Glory_slide_emty";
    private const string GLORY_ACTIVE = "Glory_slide_filled";
    private const string GLORY_GREY = "Glory_slide_filled_grey";

    private const string WINSTREAK_DEACTIVE = "Winstreak_point_o";
    private const string WINSTREAK_ACTIVE = "Winstreak_point_on";

    private const string GLORY_CHEST_GOLD_DEACTIVE = "Glory_chest_gold_d";
    private const string GLORY_CHEST_GOLD_ACTIVE = "Glory_chest_gold_w";
    private const string GLORY_CHEST_GOLD_PASS = "Glory_chest_gold";

    private const string GLORY_CHEST_DIAMOND_DEACTIVE = "Glory_chest_diamon_d";
    private const string GLORY_CHEST_DIAMOND_ACTIVE = "Glory_chest_diamon_w";
    private const string GLORY_CHEST_DIAMOND_PASS = "Glory_chest_diamon";

    private const string GLORY_CHEST_METEORITE_DEACTIVE = "Glory_chest_meteor_d";
    private const string GLORY_CHEST_METEORITE_ACTIVE = "Glory_chest_meteor_w";
    private const string GLORY_CHEST_METEORITE_PASS = "Glory_chest_meteor";

    public GameObject fxLoading;

    private int timeRemaining;
    private bool rewarded;
    private bool canReward;

    int goldReward;
    int gemReward;
    int cardReward;

    int targetChest1 = 100;
    int targetChest2 = 200;
    int targetChest3 = 300;

    List<int> maxGold = new List<int>() { 5000, 7000, 10000 };
    List<int> maxGem = new List<int>() { 10, 15, 20 };
    List<int> maxCard = new List<int>() { 5, 7, 10 };

    Color green = new Color(0.17f, 0.59f, 0.42f);
    Color yellow = new Color(0.95f, 0.81f, 0.25f);
    Color white = new Color(1, 1, 1);

    private void OnEnable()
    {
        missInfo = false;
        readyClickBack = true;
        targetChest1 = 100;
        targetChest2 = 200;
        targetChest3 = 300;
        goldReward = 0;
        gemReward = 0;
        cardReward = 0;
        timeRemaining = 3600;
        rewarded = true;
        canReward = false;
        buttonClaim.SetActive(false);
        buttonClaimed.SetActive(false);
        buttonClaimDeactive.SetActive(true);

        MessageDispatcher.AddListener(EventName.GloryChest.TournamentInfo.ToString(), OnTournamentInfo, true);
        MessageDispatcher.AddListener(EventName.GloryChest.CloseSelectLevel.ToString(), OnCloseSelectLevel, true);
        MessageDispatcher.AddListener(EventName.GloryChest.TournamentClaim.ToString(), OnTournamentClaim, true);
        RefreshInfo();
        ShowInfoReward100();
        fxLoading.SetActive(true);
        gLabelNotClan.SetActive(false);
        panelInfo.SetActive(false);
        new CSTournamentInfo().Send();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.GloryChest.TournamentInfo.ToString(), OnTournamentInfo, true);
        MessageDispatcher.RemoveListener(EventName.GloryChest.CloseSelectLevel.ToString(), OnCloseSelectLevel, true);
        MessageDispatcher.RemoveListener(EventName.GloryChest.TournamentClaim.ToString(), OnTournamentClaim, true);
    }

    private void OnTournamentClaim(IMessage rMessage)
    {
        SCTournamentClaim tc = rMessage.Data as SCTournamentClaim;
        if (tc.status == SCTournamentClaim.SUCCESS)
        {
            Chest c = tc.chest;
            if (c != null)
            {
                goldReward = c.gold;
                gemReward = c.gem;
                cardReward = c.universalCard;
                if (missInfo)
                {
                    missInfo = false;
                    bClaim();
                    clickClaim = false;
                }
            }
        }
    }

    bool missInfo;
    bool clickClaim;

    IEnumerator countDown2Minutes;
    int target;

    bool is_end;

    private void OnTournamentInfo(IMessage rMessage)
    {
        Debug.Log("OnTournamentInfo");
        SCTournamentInfo tournament = rMessage.Data as SCTournamentInfo;
        if (tournament.status == SCTournamentInfo.SUCCESS)
        {
            fxLoading.SetActive(false);
            List<Chest> chests = tournament.chests;
            for (int i = 0; i < chests.Count; i++)
            {
                if (i == 0)
                {
                    targetChest1 = chests[i].glory;
                }
                else if (i == 1)
                {
                    targetChest2 = chests[i].glory;
                }
                else if (i == 2)
                {
                    targetChest3 = chests[i].glory;
                }
                maxGold[i] = chests[i].gold;
                maxGem[i] = chests[i].gem;
                maxCard[i] = chests[i].universalCard;
            }
            if (showInfoReward == 0)
            {
                ShowInfoReward100();
            }
            else if (showInfoReward == 1)
            {
                ShowInfoReward200();
            }
            else
            {
                ShowInfoReward300();
            }
            ClanInfo ci = tournament.clanInfo;
            starCount = (int)ci.scoreTournament;
            timeRemaining = (int)tournament.remainTime;
            rewarded = tournament.rewarded;
            canReward = tournament.canReward;
            is_end = tournament.endTournament;

            for (int i = 0; i < listLabelBackgroundNumber.Count; i++)
            {
                listLabelBackgroundNumber[i].text = "(" + tournament.achievement.items[i].current + "/" + tournament.achievement.items[i].max + ") ";
                if (i == 0)
                {
                    listLabelBackgroundNumber[i].text += I2.Loc.ScriptLocalization.title_win_pvp_5_times.Replace("%{value}", tournament.achievement.items[i].max.ToString());
                }
                else if (i == 1)
                {
                    listLabelBackgroundNumber[i].text += I2.Loc.ScriptLocalization.title_open_golden_chest.Replace("%{value}", tournament.achievement.items[i].max.ToString());
                }
                else if (i == 2)
                {
                    listLabelBackgroundNumber[i].text += I2.Loc.ScriptLocalization.title_request_card_2_times.Replace("%{value}", tournament.achievement.items[i].max.ToString());
                }
                else if (i == 3)
                {
                    listLabelBackgroundNumber[i].text += I2.Loc.ScriptLocalization.title_donate_card_10_times.Replace("%{value}", tournament.achievement.items[i].max.ToString());
                }
                listLabelGloryPointNumber[i].text = "+" + tournament.achievement.items[i].gloryPoint;
                if (tournament.achievement.items[i].current == 0)
                {
                    listSpriteBackgroundGlory[i].color = white;
                }
                else if (tournament.achievement.items[i].current == tournament.achievement.items[i].max)
                {
                    listSpriteBackgroundGlory[i].color = green;
                }
                else if (tournament.achievement.items[i].current < tournament.achievement.items[i].max)
                {
                    listSpriteBackgroundGlory[i].color = yellow;
                }
            }

            string time = (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00");
            if (tournament.endTournament)
            {
                lStatusTime.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", time);
            }
            else
            {
                lStatusTime.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", time);
            }
            if (ci == null)
            {
                gLabelNotClan.SetActive(true);
            }
            else
            {
                gLabelNotClan.SetActive(false);
                MessageDispatcher.SendMessage(gameObject, EventName.GloryChest.LoadTournamentInfo.ToString(), ci, 0);
            }
            if (is_end && !rewarded && canReward)
            {
                if (tournament.chest == null)
                {
                    new CSTournamentClaim(false).Send();
                }
                else
                {
                    goldReward = tournament.chest.gold;
                    gemReward = tournament.chest.gem;
                    cardReward = tournament.chest.universalCard;
                }
            }
            if (countDown2Minutes != null)
            {
                StopCoroutine(countDown2Minutes);
            }
            if (timeRemaining > 0)
            {
                countDown2Minutes = CountDown2Minutes();
                StartCoroutine(countDown2Minutes);
            }
            RefreshInfo();
        }
        else if (tournament.status == SCTournamentInfo.CLAN_NOT_EXIST)
        {
            fxLoading.SetActive(false);
            gLabelNotClan.SetActive(true);
            starCount = 0;
            timeRemaining = 0;
            rewarded = false;
            string time = (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00");
            lStatusTime.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", time);
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(tournament.message, false, 1.5f);
        }
    }

    IEnumerator CountDown2Minutes()
    {
        TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        target = (int)span.TotalSeconds + timeRemaining;
        while (timeRemaining >= 0)
        {
            if (is_end)
            {
                lStatusTime.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00"));
            }
            else
            {
                lStatusTime.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00"));
            }
            TimeSpan temp_span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            timeRemaining = target - (int)temp_span.TotalSeconds;
            yield return new WaitForSecondsRealtime(1);
        }
        //lStatusTime.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", "00:00:00");

        yield return new WaitForSecondsRealtime(2);
        new CSTournamentClaim(false).Send();
    }

    private void OnCloseSelectLevel(IMessage rMessage)
    {
        UIPanel p = PopupManagerCuong.Instance.gameObject.GetComponent<UIPanel>();
        Destroy(p);
    }

    int starCount;
    Vector3 tempVector3;
    void RefreshInfo()
    {
        Debug.Log("star count : " + starCount);
        if (targetChest1 == 0)
        {
            targetChest1 = 100;
        }
        if (targetChest2 == 0)
        {
            targetChest2 = 200;
        }
        if (targetChest3 == 0)
        {
            targetChest3 = 300;
        }
        Debug.Log("targetChest3 : " + targetChest3);
        sBarProgress.width = Mathf.Min((int)(517 * (starCount * 1.0f / targetChest3)), 582); //0 : 0, 100% : 582-517
        float pos = Mathf.Max(0, Mathf.Min((517f / targetChest3) * targetChest1, 517));
        tempVector3.x = pos;
        tempVector3.y = listsBarSplit[0].localPosition.y;
        tempVector3.z = listsBarSplit[0].localPosition.z;
        listsBarSplit[0].localPosition = tempVector3;
        pos = Mathf.Max(0, Mathf.Min((517f / targetChest3) * targetChest2, 517));
        tempVector3.x = pos;
        tempVector3.y = listsBarSplit[1].localPosition.y;
        tempVector3.z = listsBarSplit[1].localPosition.z;
        listsBarSplit[1].localPosition = tempVector3;
        tempVector3.x = 517;
        tempVector3.y = listsBarSplit[2].localPosition.y;
        tempVector3.z = listsBarSplit[2].localPosition.z;
        listsBarSplit[2].localPosition = tempVector3;

        lbarProgress.text = starCount.ToString();
        if (starCount >= 300)
        {
            listSpriteButtonBox[0].spriteName = GLORY_CHEST_GOLD_DEACTIVE;
            listSpriteButtonBox[1].spriteName = GLORY_CHEST_DIAMOND_DEACTIVE;
            listSpriteButtonBox[2].spriteName = GLORY_CHEST_METEORITE_PASS;
        }
        else if (starCount >= 200)
        {
            listSpriteButtonBox[0].spriteName = GLORY_CHEST_GOLD_DEACTIVE;
            listSpriteButtonBox[1].spriteName = GLORY_CHEST_DIAMOND_PASS;
            listSpriteButtonBox[2].spriteName = GLORY_CHEST_METEORITE_DEACTIVE;
        }
        else if (starCount >= 100)
        {
            listSpriteButtonBox[0].spriteName = GLORY_CHEST_GOLD_PASS;
            listSpriteButtonBox[1].spriteName = GLORY_CHEST_DIAMOND_DEACTIVE;
            listSpriteButtonBox[2].spriteName = GLORY_CHEST_METEORITE_DEACTIVE;
        }
        else
        {
            listSpriteButtonBox[0].spriteName = GLORY_CHEST_GOLD_DEACTIVE;
            listSpriteButtonBox[1].spriteName = GLORY_CHEST_DIAMOND_DEACTIVE;
            listSpriteButtonBox[2].spriteName = GLORY_CHEST_METEORITE_DEACTIVE;
        }

        if (is_end)
        {
            if (canReward)
            {
                if (!rewarded)
                {
                    buttonClaim.SetActive(true);
                    buttonClaimDeactive.SetActive(false);
                    buttonClaimed.SetActive(false);
                }
                else
                {
                    buttonClaim.SetActive(false);
                    buttonClaimDeactive.SetActive(false);
                    buttonClaimed.SetActive(true);
                }
            }
            else
            {
                buttonClaim.SetActive(false);
                buttonClaimDeactive.SetActive(true);
                buttonClaimed.SetActive(false);
            }
        }
        else
        {
            buttonClaim.SetActive(false);
            buttonClaimDeactive.SetActive(true);
            buttonClaimed.SetActive(false);
        }
    }

    int showInfoReward;

    public void ShowInfoReward100()
    {
        showInfoReward = 0;
        listLabelInfo[0].text = GameContext.FormatNumber(maxGold[0]);
        listLabelInfo[1].text = maxGem[0].ToString();
        listLabelInfo[2].text = maxCard[0].ToString();
    }

    public void ShowInfoReward200()
    {
        showInfoReward = 1;
        listLabelInfo[0].text = GameContext.FormatNumber(maxGold[1]);
        listLabelInfo[1].text = maxGem[1].ToString();
        listLabelInfo[2].text = maxCard[1].ToString();
    }

    public void ShowInfoReward300()
    {
        showInfoReward = 2;
        listLabelInfo[0].text = GameContext.FormatNumber(maxGold[2]);
        listLabelInfo[1].text = maxGem[2].ToString();
        listLabelInfo[2].text = maxCard[2].ToString();
    }

    public void bExitInfo()
    {
        panelInfo.SetActive(false);
    }

    public void bExitGloryChest()
    {
        PopupManager.Instance.HideGloryChestPopup();
    }

    public void bGoDefeatBoss()
    {
        //CacheGame.SetCurrentLevel(7);
        //UIPanel p = PopupManagerCuong.Instance.gameObject.AddComponent<UIPanel>();
        //p.depth = 21;
        //p.useSortingOrder = true;
        //p.sortingOrder = 1600;
        //PopupManagerCuong.Instance.ShowSelectLevelPopup();
        SceneManager.LoadScene("PVPMain");
    }

    public void bClaim()
    {
        if (goldReward == 0 && gemReward == 0 && cardReward == 0 && !clickClaim)
        {
            clickClaim = true;
            missInfo = true;
            new CSTournamentClaim(false).Send();
            return;
        }
        List<ItemVolumeFalconMail> list = new List<ItemVolumeFalconMail>();
        for (int i = 0; i < 3; i++)
        {
            ItemVolumeFalconMail item = new ItemVolumeFalconMail();
            if (i == 0)
            {
                item.key = FalconMail.ITEM_GOLD;
                item.value = goldReward;
            }
            else if (i == 1)
            {
                item.key = FalconMail.ITEM_GEM;
                item.value = gemReward;
            }
            else if (i == 2)
            {
                item.key = FalconMail.ITEM_CARD_PLANE_GENERAL;
                item.value = cardReward;
            }
            list.Add(item);
        }
        buttonClaim.SetActive(false);
        buttonClaimDeactive.SetActive(false);
        buttonClaimed.SetActive(true);
        PopupManager.Instance.ShowClaimPopup(list, FirebaseLogSpaceWar.GloryChest_why);
        new CSTournamentClaim(true).Send();
    }

    public void bClaimed()
    {
        //da nhan phan thuong
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_claimed, false, 1.5f);
    }

    public void bClaimDeactive()
    {
        //chua den thoi gian
        //if (starCount >= 100)
        //{
        //    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_can_claim_after.Replace("%{time}", (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00")), false, 1.5f);
        //}
        //else
        //{
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_cant_claim, false, 1.5f);
        //}
    }

    public void bShowInfo()
    {
        panelInfo.SetActive(true);
    }

    private bool readyClickBack;

    IEnumerator ReadyClickBack()
    {
        readyClickBack = false;
        yield return new WaitForSecondsRealtime(0.38f);
        readyClickBack = true;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && readyClickBack)
        {
            StartCoroutine(ReadyClickBack());
            if (panelInfo.activeInHierarchy)
            {
                panelInfo.SetActive(false);
            }
            else
            {
                bExitGloryChest();
            }
        }
    }
}
