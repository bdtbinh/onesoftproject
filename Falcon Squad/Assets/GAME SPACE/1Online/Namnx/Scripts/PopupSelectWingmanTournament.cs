﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupSelectWingmanTournament : MonoBehaviour
{
    [SerializeField]
    private ElementSelectWingmanTournament[] listElements;

    [SerializeField]
    private UISprite btnConfirm;

    private ElementSelectWingmanTournament selectedWingmanTournament;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventID.ON_SELECTED_CALLING_BACKUP, OnSelectedCallingBackup, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_SELECTED_CALLING_BACKUP, OnSelectedCallingBackup, true);
    }

    bool isRight;

    public void SetData(bool isRight)
    {
        this.isRight = isRight;
        InitListElements();
    }

    private void OnSelectedCallingBackup(IMessage msg)
    {
        btnConfirm.spriteName = "PVP_btn_fight";

        if (selectedWingmanTournament != null)
        {
            selectedWingmanTournament.SetAsDeselected();
        }

        selectedWingmanTournament = (ElementSelectWingmanTournament)msg.Sender;
    }

    private void InitListElements()
    {
        int totalAircraft = System.Enum.GetValues(typeof(WingmanTypeEnum)).Length;
        btnConfirm.spriteName = "PVP_btn_fight_d";
        selectedWingmanTournament = null;
        int j;
        for (int i = 0; i < listElements.Length; i++)
        {
            j = i;
            listElements[i].CreateItem((WingmanTypeEnum)(j + 1));
        }
        int selected = 0;
        if (isRight)
        {
            selected = CacheGame.GetWingManRightTournament();
        }
        else
        {
            selected = CacheGame.GetWingManLeftTournament();
        }
        if (selected > 0)
        {
            listElements[selected - 1].SetAsSelected();
        }
        else
        {
            listElements[0].SetAsSelected();
        }
    }

    public void OnClickBtnConfirm()
    {
        if (selectedWingmanTournament != null)
        {
            if (isRight)
            {
                if ((int)selectedWingmanTournament.WingmanType == CacheGame.GetWingManLeftTournament())
                {
                    CacheGame.SetWingManLeftTournament((int)WingmanTypeEnum.None);
                }
                CacheGame.SetWingManRightTournament((int)selectedWingmanTournament.WingmanType);
                PopupManager.Instance.HideSelectWingmanPopup();
                MessageDispatcher.SendMessage(EventName.Tournament.RefreshSelected.ToString());
            }
            else
            {
                if ((int)selectedWingmanTournament.WingmanType == CacheGame.GetWingManRightTournament())
                {
                    CacheGame.SetWingManRightTournament((int)WingmanTypeEnum.None);
                }
                CacheGame.SetWingManLeftTournament((int)selectedWingmanTournament.WingmanType);
                PopupManager.Instance.HideSelectWingmanPopup();
                MessageDispatcher.SendMessage(EventName.Tournament.RefreshSelected.ToString());
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_use_another_aircraft, false, 1.5f);
        }
    }

    public void OnSkip()
    {
        PopupManager.Instance.HideSelectWingmanPopup();
    }
}
