﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mp.Pvp;
public class PopupSelectCardType : MonoBehaviour
{
    [SerializeField]
    private ElementSelectCardType[] listElements;

    [SerializeField]
    private UISprite btnConfirm;

    private ElementSelectCardType selectedCardType;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventID.ON_SELECTED_CALLING_BACKUP, OnSelectedCallingBackup, true);

        InitListElements();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_SELECTED_CALLING_BACKUP, OnSelectedCallingBackup, true);
    }

    private void OnSelectedCallingBackup(IMessage msg)
    {
        btnConfirm.spriteName = "PVP_btn_fight";

        if (selectedCardType != null)
        {
            selectedCardType.SetAsDeselected();
        }

        selectedCardType = (ElementSelectCardType)msg.Sender;
    }

    private void InitListElements()
    {
        int totalAircraft = System.Enum.GetValues(typeof(AircraftTypeEnum)).Length;
        int totalDrone = System.Enum.GetValues(typeof(WingmanTypeEnum)).Length;
        int totalWing = System.Enum.GetValues(typeof(WingTypeEnum)).Length;
        btnConfirm.spriteName = "PVP_btn_fight_d";
        selectedCardType = null;
        int i;
        int j;
        for (i = 0; i < totalAircraft - 2; i++)
        {
            j = i;
            if (i >= 3) j = i + 2;
            listElements[i].CreateItem((AircraftTypeEnum)(j + 1));
        }
        for (; i < listElements.Length - totalWing + 1; i++)
        {
            j = i - (totalAircraft - 2);
            listElements[i].CreateItem((WingmanTypeEnum)(j + 1));
        }
        for (; i < listElements.Length; i++)
        {
            j = i - (totalAircraft - 2) - (totalDrone - 1);
            listElements[i].CreateItem((WingTypeEnum)(j + 1));
        }
        listElements[0].SetAsSelected();
    }

    public void OnClickBtnConfirm()
    {
        if (selectedCardType != null)
        {
            if (selectedCardType.type == ElementSelectCardType.Type.Aircraft && CacheGame.GetSpaceShipIsUnlocked((int)selectedCardType.AircraftType) == 0)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_unlock_aircraft_first, false, 1.5f);
                return;
            }
            if (selectedCardType.type == ElementSelectCardType.Type.Wingman && CacheGame.GetWingManIsUnlocked((int)selectedCardType.WingmanType) == 0)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_unlock_wingman_first, false, 1.5f);
                return;
            }
            if (selectedCardType.type == ElementSelectCardType.Type.Wing && CacheGame.GetWingIsUnlocked((int)selectedCardType.WingType) == 0)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_unlock_wing_first, false, 1.5f);
                return;
            }
            PopupManager.Instance.HideSelectCardTypePopup();
            int type = 0;
            if (selectedCardType.type == ElementSelectCardType.Type.Aircraft)
            {
                //from 1 to 10 (ignore 4,5)
                //public const int ITEM_CARD_PLANE_1 = 10;
                //public const int ITEM_CARD_PLANE_2 = 11;
                //public const int ITEM_CARD_PLANE_3 = 12;
                //public const int ITEM_CARD_PLANE_4 = 13; //plane 6
                //public const int ITEM_CARD_PLANE_5 = 14; //plane 7
                //public const int ITEM_CARD_PLANE_6 = 24; //plane 8
                //public const int ITEM_CARD_PLANE_7 = 25; //plane 9
                //public const int ITEM_CARD_PLANE_8 = 38; //plane 10
                switch ((int)selectedCardType.AircraftType)
                {
                    case 1:
                    case 2:
                    case 3:
                        type = (int)selectedCardType.AircraftType + 9;
                        break;
                    case 6:
                    case 7:
                        type = (int)selectedCardType.AircraftType + 7;
                        break;
                    case 8:
                    case 9:
                        type = (int)selectedCardType.AircraftType + 16;
                        break;
                    case 10:
                        type = (int)selectedCardType.AircraftType + 28;
                        break;
                    default:
                        break;
                }

            }
            else if (selectedCardType.type == ElementSelectCardType.Type.Wingman)
            {
                //from 1 to 6
                //public const int ITEM_CARD_WINGMAN_1 = 16;
                //public const int ITEM_CARD_WINGMAN_2 = 17;
                //public const int ITEM_CARD_WINGMAN_3 = 18;
                //public const int ITEM_CARD_WINGMAN_4 = 19;
                //public const int ITEM_CARD_WINGMAN_5 = 20;
                //public const int ITEM_CARD_WINGMAN_6 = 32;
                switch ((int)selectedCardType.WingmanType)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        type = (int)selectedCardType.WingmanType + 15;
                        break;
                    case 6:
                        type = (int)selectedCardType.WingmanType + 26;
                        break;
                    default:
                        break;
                }
            }
            else if (selectedCardType.type == ElementSelectCardType.Type.Wing)
            {
                //from 1 to 3
                //public const int ITEM_CARD_WING_1 = 29;
                //public const int ITEM_CARD_WING_2 = 30;
                switch ((int)selectedCardType.WingType)
                {
                    case 1:
                    case 2:
                        type = (int)selectedCardType.WingType + 28;
                        break;
                    case 3:
                        type = (int)selectedCardType.WingType + 34;
                        break;
                    default:
                        break;
                }
            }
            new CSDonateRequest(type).Send();
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_use_another_aircraft, false, 1.5f);
        }
    }

    public void OnSkip()
    {
        PopupManager.Instance.HideSelectCardTypePopup();
    }
}
