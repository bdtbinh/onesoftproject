﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;
using Mp.Pvp;
using UnityEngine.SceneManagement;

public class PopupForceUpdateVersion : MonoBehaviour
{
    public GameObject gUpdate;
    public GameObject gMaintain;

    public GameObject lUpdate;
    public GameObject lMaintain;

    public void SetData(bool isMaintain)
    {
        if (isMaintain)
        {
            gUpdate.SetActive(false);
            lUpdate.SetActive(false);
            gMaintain.SetActive(true);
            lMaintain.SetActive(true);
        }
        else
        {
            gMaintain.SetActive(false);
            lMaintain.SetActive(false);
            gUpdate.SetActive(true);
            lUpdate.SetActive(true);
        }
    }

    public void ButtonYes()
    {
#if UNITY_ANDROID
        string bundleID = "" + Application.identifier;
        Application.OpenURL("market://details?id=" + bundleID);
#elif UNITY_IPHONE
        if (GameContext.IS_CHINA_VERSION)
        {
            Application.OpenURL("itms-apps://itunes.apple.com/app/id1448799684");
        }
        else
        {
            Application.OpenURL("itms-apps://itunes.apple.com/app/id1337514468");
        }
#endif
    }

    public void ButtonNo()
    {
        PopupManager.Instance.HideForceUpdatePopup();
    }

}
