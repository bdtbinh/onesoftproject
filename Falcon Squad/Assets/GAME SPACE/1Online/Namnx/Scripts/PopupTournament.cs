﻿using Mp.Pvp;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using com.ootii.Messages;

public class PopupTournament : MonoBehaviour
{
    public GameObject gNotify;

    public GameObject gTotalBonus;
    public UILabel lTotalBonus;

    public UILabel lTime;
    public UILabel lGainTicket;
    public UILabel lTimeGainTicket;
    public UILabel lStartButton;
    public AircraftAnimations aircraftAnimations;

    public UISprite wingSelectIcon;
    public WingAnimations wingAnimations;
    public UISprite rightWingmanIcon;
    public UISprite leftWingmanIcon;

    public GameObject gloryPointPrefab;
    public Transform gWrapperGloryPoint;

    public List<UISprite> listSpriteLosses;

    private const string SPRITE_WINGMAN_NOT_CHOSEN = "home_Dron_empty_slot";
    private const string SPRITE_WEAPON_NOT_CHOSEN = "home_SWeapon_empty_slot";
    private const string SPRITE_WING_NOT_CHOSEN = "home_Wing_empty_slot";

    private const string BULLET = "hangar_bullet_0";

    private const string RADIO_RED = "radio_btn_red";
    private const string RADIO_BLACK = "radio_btn_black";

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Tournament.MegaTournamentInfo.ToString(), OnMegaTournamentInfo, true);
        MessageDispatcher.AddListener(EventName.Tournament.RefreshSelected.ToString(), OnRefreshSelected, true);
        new CSMegaTournamentInfo().Send();
        AircraftTypeEnum selectedType = (AircraftTypeEnum)CacheGame.GetSpaceShipTournament();
        aircraftAnimations.PlayAnimations(selectedType, Constant.MAX_RANK_APPLY);
        gTotalBonus.SetActive(false);
        isEnd = false;
        lTime.text = "";
        lGainTicket.color = Color.clear;
        lTimeGainTicket.color = Color.clear;
        RefreshSelected();
        LeanTween.delayedCall(0.1f, () =>
        {
            new CSSceneLoaded(CSSceneLoaded.SCENE_PVP_TOURNAMENT_MAIN).Send();
        });
    }

    void RefreshSelected()
    {
        if (CachePvp.NotifyTournament == 0)
        {
            gNotify.SetActive(true);
        }
        else
        {
            gNotify.SetActive(false);
        }
        AircraftTypeEnum selectedType = (AircraftTypeEnum)CacheGame.GetSpaceShipTournament();
        aircraftAnimations.PlayAnimations(selectedType, Constant.MAX_RANK_APPLY);
        if (CacheGame.GetWingManLeftTournament() == 0)
        {
            leftWingmanIcon.spriteName = SPRITE_WINGMAN_NOT_CHOSEN;
        }
        else
        {
            leftWingmanIcon.spriteName = HangarValue.WingmanIconWithBorderSpriteName[(WingmanTypeEnum)CacheGame.GetWingManLeftTournament()];
        }
        if (CacheGame.GetWingManRightTournament() == 0)
        {
            rightWingmanIcon.spriteName = SPRITE_WINGMAN_NOT_CHOSEN;
        }
        else
        {
            rightWingmanIcon.spriteName = HangarValue.WingmanIconWithBorderSpriteName[(WingmanTypeEnum)CacheGame.GetWingManRightTournament()];
        }
        if (CacheGame.GetWingTournament() == 0)
        {
            wingSelectIcon.spriteName = SPRITE_WING_NOT_CHOSEN;
            wingAnimations.gameObject.SetActive(false);
        }
        else
        {
            wingSelectIcon.spriteName = BULLET;
            wingAnimations.gameObject.SetActive(true);
            wingAnimations.PlayAnimations((WingTypeEnum)CacheGame.GetWingTournament(), Constant.MAX_RANK_APPLY);
        }
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Tournament.MegaTournamentInfo.ToString(), OnMegaTournamentInfo, true);
        MessageDispatcher.RemoveListener(EventName.Tournament.RefreshSelected.ToString(), OnRefreshSelected, true);
    }

    private void OnRefreshSelected(IMessage rMessage)
    {
        CachePvp.NotifyTournament = 1;
        RefreshSelected();
    }

    IEnumerator countDown2Minutes;
    int target;

    IEnumerator countDownGainTicket;
    int targetGainTicket;
    int timeRemainingGainTicket;

    SCMegaTournamentInfo m;

    private void OnMegaTournamentInfo(IMessage rMessage)
    {
        m = rMessage.Data as SCMegaTournamentInfo;
        if (m.status == SCMegaTournamentInfo.SUCCESS)
        {
            if (m.info.startTimeRemain == 0)
            {
                timeRemaining = (int)m.info.endTimeRemain;
                isEnd = false;
            }
            else
            {
                timeRemaining = (int)m.info.startTimeRemain;
                isEnd = true;
            }
            if (countDown2Minutes != null)
            {
                StopCoroutine(countDown2Minutes);
            }
            if (timeRemaining > 0)
            {
                countDown2Minutes = CountDown2Minutes();
                StartCoroutine(countDown2Minutes);
            }

            timeRemainingGainTicket = (int)m.info.refillRemain;
            if (countDownGainTicket != null)
            {
                StopCoroutine(countDownGainTicket);
            }
            if (m.info.refillRemain != 0)
            {
                if (timeRemainingGainTicket > 0)
                {
                    countDownGainTicket = CountDownGainTicket();
                    StartCoroutine(countDownGainTicket);
                }
                lGainTicket.color = Color.white;
                lTimeGainTicket.color = Color.white;
            }
            else
            {
                lGainTicket.color = Color.clear;
                lTimeGainTicket.color = Color.clear;
            }

            lStartButton.text = I2.Loc.ScriptLocalization.start.ToUpper() + " x" + m.info.ticket;

            RefreshInfo();
        }
    }

    private IEnumerator CountDownGainTicket()
    {
        TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        targetGainTicket = (int)span.TotalSeconds + timeRemainingGainTicket;
        while (timeRemainingGainTicket >= 0)
        {
            lTimeGainTicket.text = "[be3439]" + (timeRemainingGainTicket / 3600).ToString("00") + ":" + ((timeRemainingGainTicket % 3600) / 60).ToString("00") + ":" + (timeRemainingGainTicket % 60).ToString("00") + "[-]";
            TimeSpan temp_span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            timeRemainingGainTicket = targetGainTicket - (int)temp_span.TotalSeconds;
            yield return new WaitForSecondsRealtime(1);
        }
        yield return new WaitForSecondsRealtime(1);
        new CSMegaTournamentInfo().Send();
    }

    IEnumerator CountDown2Minutes()
    {
        TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        target = (int)span.TotalSeconds + timeRemaining;
        while (timeRemaining >= 0)
        {
            if (isEnd)
            {
                lTime.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", "[be3439]" + (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00") + "[-]");
            }
            else
            {
                lTime.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", "[be3439]" + (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00") + "[-]");
            }
            TimeSpan temp_span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            timeRemaining = target - (int)temp_span.TotalSeconds;
            yield return new WaitForSecondsRealtime(1);
        }
    }

    int timeRemaining;
    private bool isEnd;

    void RefreshInfo()
    {
        if (isEnd)
        {
            lTime.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", "[be3439]" + (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00") + "[-]");
        }
        else
        {
            lTime.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", "[be3439]" + (timeRemaining / 3600).ToString("00") + ":" + ((timeRemaining % 3600) / 60).ToString("00") + ":" + (timeRemaining % 60).ToString("00") + "[-]");
        }

        int lose = 0;
        if (CachePvp.megaTournamentInfo != null)
        {
            if (CachePvp.megaTournamentInfo.info.unlocked)
            {
                gTotalBonus.SetActive(false);
            }
            else
            {
                int cnt = 0;
                for (int i = 0; i < CachePvp.megaTournamentInfo.info.rewards.Count; i++)
                {
                    if (CachePvp.megaTournamentInfo.info.rewards[i].bonus != null)
                    {
                        cnt++;
                    }
                }
                if (cnt > 0)
                {
                    gTotalBonus.SetActive(true);
                    lTotalBonus.text = cnt.ToString();
                }
                else
                {
                    gTotalBonus.SetActive(false);
                }
            }
            lose = CachePvp.megaTournamentInfo.info.lose;
            if (gWrapperGloryPoint.transform.childCount == 0)
            {
                Vector3 v3 = Vector3.zero;
                float padding = 115;
                for (int i = 0; i < CachePvp.megaTournamentInfo.configRewards.Count; i++)
                {
                    GameObject item = Instantiate(gloryPointPrefab);
                    item.SetActive(true);
                    item.transform.SetParent(gWrapperGloryPoint);
                    v3.x = padding * i;
                    v3.y = 0;
                    v3.z = 0;
                    item.transform.localPosition = v3;
                    v3.x = 1;
                    v3.y = 1;
                    v3.z = 1;
                    item.transform.localScale = v3;
                    item.GetComponent<ItemGloryPoint>().SetDataTop(CachePvp.megaTournamentInfo.configRewards[i].win, CachePvp.megaTournamentInfo.configRewards[i].bonus);
                    item.GetComponent<ItemGloryPoint>().SetDataBottom(CachePvp.megaTournamentInfo.configRewards[i].win, CachePvp.megaTournamentInfo.configRewards[i].free);
                }
            }
        }

        for (int i = 0; i < listSpriteLosses.Count; i++)
        {
            if (i < lose)
            {
                listSpriteLosses[i].spriteName = RADIO_RED;
            }
            else
            {
                listSpriteLosses[i].spriteName = RADIO_BLACK;
            }
        }
    }

    public void bStart()
    {
        if (CachePvp.megaTournamentInfo == null || CachePvp.megaTournamentInfo.info.startTimeRemain > 0)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_tourmanet_not_start_yet, false, 1.5f);
            return;
        }
        if (m != null && m.info.ticket == 0)
        {
            //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_ticket, false, 1.5f);
            PopupManager.Instance.ShowBuyTicketPopup();
            return;
        }
        MinhCacheGame.SetBettingAmountPvP(0);
        CachePvp.typePVP = CSFindOpponent.TYPE_MEGA;
        PopupManager.Instance.goToTournament = true;
        SceneManager.LoadScene("PVPMain");
    }

    public void bSelectAircraft()
    {
        PopupManager.Instance.ShowSelectAircraftPopup();
    }

    public void bSelectLeftWingman()
    {
        PopupManager.Instance.ShowSelectWingmanPopup(false);
    }

    public void bSelectRightWingman()
    {
        PopupManager.Instance.ShowSelectWingmanPopup(true);
    }

    public void bSelectWing()
    {
        PopupManager.Instance.ShowSelectWingPopup();
    }

    public void btnBack()
    {
        PopupManager.Instance.HideTournamentPopup();
    }

    public void ShowLeaderboardTournament()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        if (!CachePvp.sCVersionConfig.canRank)
        {
            PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainRank);
            return;
        }
        PopupManager.Instance.ShowRanking(true);
    }

    public void ShowLiveScoreTournament()
    {
        CachePvp.LoadLiveScore = 1;
        PopupManager.Instance.ShowLiveScorePopup();
    }

    public void bShowBonusPopup()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        PopupManager.Instance.ShowBonusRewardPopup();
    }

    public void bShowFreePopup()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        PopupManager.Instance.ShowBonusRewardPopup(false);
    }
}