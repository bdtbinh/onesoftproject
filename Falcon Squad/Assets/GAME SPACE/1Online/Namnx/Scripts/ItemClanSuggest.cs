﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using Mp.Pvp;
using System;

public class ItemClanSuggest : MonoBehaviour
{
    public UILabel lIndex;
    public UISprite sAvatarClan;
    public UILabel lNameClan;
    public UILabel lScoreClan;
    public UILabel lStatusMember;

    private const string AVATAR = "avt_Avt_";

    public ClanInfo m_clanInfo;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Clan.RequestedJoinClan.ToString(), OnRequestJoinClan, true);
    }

    private void OnRequestJoinClan(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        long id = (long)rMessage.Data;
        if (id == m_clanInfo.id)
        {
            switch (status)
            {
                case SCClanJoinRequest.SUCCESS:
                    lStatusMember.color = new Color(1f, 1f, 1f);
                    if (!CachePvp.listClanIdRequest.Contains(id))
                    {
                        CachePvp.listClanIdRequest.Add(id);
                    }
                    RefreshData();
                    break;
                case SCClanJoinRequest.JOINED_CLAN:
                    lStatusMember.color = new Color(1f, 1f, 1f);
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_had_in_clan, false, 1.5f);
                    break;
                case SCClanJoinRequest.JOIN_REQUEST_EXISTED:
                    lStatusMember.color = new Color(1f, 1f, 1f);
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_had_requested, false, 1.5f);
                    break;
                case SCClanJoinRequest.CLAN_NOT_EXIST:
                    lStatusMember.color = new Color(1f, 1f, 1f);
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_not_exist, false, 1.5f);
                    break;
                case SCClanJoinRequest.CLAN_FULL_MEMBER:
                    lStatusMember.color = new Color(1f, 1f, 1f);
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_have_full_member, false, 1.5f);
                    break;
                default:
                    break;
            }
        }
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Clan.RequestedJoinClan.ToString(), OnRequestJoinClan, true);
    }

    public void RequestJoinClan()
    {
        lStatusMember.color = new Color(1f, 1f, 1f);
        new CSClanJoinRequest(m_clanInfo.id).Send();
        //MessageDispatcher.SendMessage(gameObject, EventName.Clan.RequestJoinClan.ToString(), clanId, 0);
    }

    public void RefreshData()
    {
        transform.localPosition = new Vector3(0, transform.localPosition.y, transform.localPosition.z);
        lIndex.text = (transform.GetSiblingIndex() + 1).ToString();

        if (m_clanInfo.avatar < 8)
        {
            sAvatarClan.spriteName = AVATAR + "0" + (m_clanInfo.avatar + 2);
        }
        else
        {
            sAvatarClan.spriteName = AVATAR + (m_clanInfo.avatar + 2);
        }

        lNameClan.text = m_clanInfo.name;
        lScoreClan.text = I2.Loc.ScriptLocalization.score + ": " + GameContext.FormatNumber((int)m_clanInfo.score);
        lStatusMember.text = m_clanInfo.totalMember + "/" + m_clanInfo.size;
        if (m_clanInfo.totalMember == m_clanInfo.size)
        {
            lStatusMember.color = new Color(0.6f, 0.17f, 0.29f);
        }
        else
        {
            lStatusMember.color = new Color(1f, 1f, 1f);
        }
    }

    public void SetData(ClanInfo ci)
    {
        transform.localPosition = new Vector3(0, transform.localPosition.y, transform.localPosition.z);
        m_clanInfo = ci;
        lIndex.text = (transform.GetSiblingIndex() + 1).ToString();

        if (ci.avatar < 8)
        {
            sAvatarClan.spriteName = AVATAR + "0" + (ci.avatar + 2);
        }
        else
        {
            sAvatarClan.spriteName = AVATAR + (ci.avatar + 2);
        }

        lNameClan.text = ci.name;
        lScoreClan.text = I2.Loc.ScriptLocalization.score + ": " + GameContext.FormatNumber((int)ci.score);
        lStatusMember.text = ci.totalMember + "/" + ci.size;
        if (ci.totalMember == ci.size)
        {
            lStatusMember.color = new Color(0.6f, 0.17f, 0.29f);
        }
        else
        {
            lStatusMember.color = new Color(1f, 1f, 1f);
        }
    }

    bool ContainId(long id)
    {
        for (int i = 0; i < CachePvp.listClanIdRequest.Count; i++)
        {
            if (CachePvp.listClanIdRequest[i] == id)
            {
                return true;
            }
        }
        return false;
    }

    public void bView()
    {
        PopupClan.Instance.ShowClanInfo(m_clanInfo);
    }
}
