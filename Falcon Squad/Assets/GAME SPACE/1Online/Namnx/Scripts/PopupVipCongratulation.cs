﻿using OneSoftGame.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupVipCongratulation : PersistentSingleton<PopupVipCongratulation>
{
    public UISprite sRank;
    public UILabel lRank;
    public UILabel lMessage;

    private const string ICON_VIP = "Icon__VIP_";

    public void Refresh()
    {
        Debug.Log("refresh");
        //lMessage.text = "Congratulation! Now you are VIP " + CachePvp.myVip;
        lMessage.text = I2.Loc.ScriptLocalization.msg_congratulation_vip.Replace("%{total_vip}", CachePvp.myVip.ToString());
        lRank.text = "VIP " + CachePvp.myVip;
        sRank.spriteName = ICON_VIP + (CachePvp.myVip - 1);
    }

    public void OnClickBack()
    {
        PopupManager.Instance.HideCongratulationVip();
    }
}
