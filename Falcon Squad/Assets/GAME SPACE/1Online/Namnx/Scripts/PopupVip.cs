﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using System;

public class PopupVip : MonoBehaviour
{

    const string FEATURES = "FEATURES";
    const string ICON = "ICON";
    const string LUCKY_WHEEL_FREE_PER_DAY = "LUCKY_WHEEL_FREE_PER_DAY";
    const string HEART_PER_DAY = "HEART_PER_DAY";
    const string POWER_UP_PER_DAY = "POWER_UP_PER_DAY";
    const string DROP_30_CHANGE_GOLD = "X2_COINS_3_FIRST_GAMES";
    const string DROP_50_CHANGE_GOLD = "X2_COINS_PER_GAMES";
    const string DROP_100_CHANGE_GOLD = "HEART_PER_GAME";
    const string DMG2_PER_PLANES = "2DMG_PER_PLANES";
    const string DMG5_PER_PLANES = "5DMG_PER_PLANES";
    const string DMG10_PER_PLANES = "10DMG_PER_PLANES";
    const string DMG20_PER_DRONES = "20DMG_PER_DRONES";
    const string X2_REWARDS_DAILY_TASK = "X2_REWARDS_DAILY_TASK";

    const string ICON_VIP_SPRITE_NAME = "Icon__VIP_";
    const string ICON_VIP_DOT_LIST_SPRITE_NAME = "VIP_dotlist";
    const string ICON_VIP_CHECK_LIST_SPRITE_NAME = "VIP__check";

    public static bool OpenFromHome = true;

    public UISprite sVipIcon;
    public UILabel lProgress;
    public UISprite sProgressBackground;
    public UILabel lProgressValue;
    public UILabel lCurrentVip;

    public UISprite sListNeed;
    public UILabel lListNeed;
    public UILabel lVipBenefit;

    public UISprite[] sListBenefit;
    public UILabel[] lListBenefit;

    public GameObject buttonLeft;
    public GameObject buttonRight;

    public GameObject progressGameobject;

    public UISprite sBoxLeft;
    public UISprite sBoxRight;

    private List<RewardPopupVip> listReward;

    public static List<int> listTarget = new List<int>() { 1, 5, 10, 20, 50, 100, 200, 300, 500, 1000 };

    public string VipSystemDescription(string key)
    {
        switch (key)
        {
            case LUCKY_WHEEL_FREE_PER_DAY:
                return I2.Loc.ScriptLocalization.popup_vip_1_spin_per_day;

            case HEART_PER_DAY:
                return I2.Loc.ScriptLocalization.popup_vip_1_life_per_day;

            case POWER_UP_PER_DAY:
                return I2.Loc.ScriptLocalization.popup_vip_1_power_up_per_day;

            case DROP_30_CHANGE_GOLD:
                return I2.Loc.ScriptLocalization.popup_vip_30_percent_drop_gold;

            case DROP_50_CHANGE_GOLD:
                return I2.Loc.ScriptLocalization.popup_vip_50_percent_drop_gold;

            case DROP_100_CHANGE_GOLD:
                return I2.Loc.ScriptLocalization.popup_vip_100_percent_drop_gold;

            case DMG2_PER_PLANES:
                return I2.Loc.ScriptLocalization.popup_vip_2_percent_plane_damage;

            case DMG5_PER_PLANES:
                return I2.Loc.ScriptLocalization.popup_vip_5_percent_plane_damage;

            case DMG10_PER_PLANES:
                return I2.Loc.ScriptLocalization.popup_vip_10_percent_plane_damage;

            case DMG20_PER_DRONES:
                return I2.Loc.ScriptLocalization.popup_vip_20_percent_drone_damage;

            case X2_REWARDS_DAILY_TASK:
                return I2.Loc.ScriptLocalization.popup_vip_x2_reward_daily_task;

            case ICON:
                return I2.Loc.ScriptLocalization.icon;

            default:
                return "Unknow Vip System Description";
        }
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.ChangeProperties.ChangeVip.ToString(), OnChangeVip, true);
    }

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.ChangeProperties.ChangeVip.ToString(), OnChangeVip, true);
        listReward = new List<RewardPopupVip>();
        for (int i = 0; i < 10; i++)
        {
            RewardPopupVip rw = new RewardPopupVip();
            switch (i)
            {
                case 0:
                    rw.types = new List<string>() { LUCKY_WHEEL_FREE_PER_DAY };
                    break;
                case 1:
                    rw.types = new List<string>() { HEART_PER_DAY, POWER_UP_PER_DAY };
                    break;
                case 2:
                    rw.types = new List<string>() { DROP_30_CHANGE_GOLD };
                    break;
                case 3:
                    rw.types = new List<string>() { DROP_50_CHANGE_GOLD };
                    break;
                case 4:
                    rw.types = new List<string>() { DROP_100_CHANGE_GOLD };
                    break;
                case 5:
                    rw.types = new List<string>() { DMG2_PER_PLANES };
                    break;
                case 6:
                    rw.types = new List<string>() { DMG5_PER_PLANES };
                    break;
                case 7:
                    rw.types = new List<string>() { DMG10_PER_PLANES };
                    break;
                case 8:
                    rw.types = new List<string>() { DMG20_PER_DRONES };
                    break;
                case 9:
                    rw.types = new List<string>() { X2_REWARDS_DAILY_TASK };
                    break;
                default:
                    break;
            }
            listReward.Add(rw);
        }
        lCurrentVip.text = I2.Loc.ScriptLocalization.vip_key.Replace("%{number}", CachePvp.myVip.ToString());
        ShowInfo(CachePvp.myVip);
    }

    // true nếu mở popup vip lên từ button ở home
    public void ButtonInfo()
    {
        PopupManager.Instance.ShowVipInfo();
        if (!OpenFromHome)
        {
            PopupManager.Instance.ChangeVipInfoDepth(GetComponent<ChangeDepthLayer>().depthAdd);
        }
    }

    private void OnChangeVip(IMessage rMessage)
    {
        lCurrentVip.text = I2.Loc.ScriptLocalization.vip_key.Replace("%{number}", CachePvp.myVip.ToString());
        ShowInfo(CachePvp.myVip);
    }

    int currentVip = 0;

    private void ShowInfo(int vip)
    {
        vip = Mathf.Max(0, Mathf.Min(9, vip));
        currentVip = vip;
        if (vip < 9)
        {
            string vipBenefit = I2.Loc.ScriptLocalization.popup_vip_benefit;
            lVipBenefit.text = vipBenefit.Replace("%{level_vip}", (vip + 1).ToString());
        }
        else
        {
            string vipBenefit = I2.Loc.ScriptLocalization.popup_vip_benefit;
            lVipBenefit.text = vipBenefit.Replace("%{level_vip}", "10");
        }
        if (vip == 0)
        {
            buttonLeft.SetActive(false);
        }
        else
        {
            buttonLeft.SetActive(true);
        }

        if (vip == 9)
        {
            buttonRight.SetActive(false);
        }
        else
        {
            buttonRight.SetActive(true);
        }

        string vipProgress = I2.Loc.ScriptLocalization.popup_vip_progress;
        lProgress.text = vipProgress.Replace("%{level_vip}", Mathf.Min(10, vip + 1).ToString());

        if (CachePvp.myVip < 10)
        {
            sProgressBackground.fillAmount = Mathf.Min(1, CachePvp.myVipPoint * 1.0f / listTarget[vip]);
            lProgressValue.text = CachePvp.myVipPoint + "/" + listTarget[vip];
            if (CachePvp.myVip <= vip)
            {
                string moreVipPoint = I2.Loc.ScriptLocalization.popup_vip_need_more_points;
                lListNeed.text = moreVipPoint.Replace("%{vip_points}", "[ffff00]" + (listTarget[vip] - CachePvp.myVipPoint).ToString() + "[-]").Replace("%{level_vip}", "[ffff00]" + (vip + 1).ToString() + "[-]");

                lListNeed.gameObject.SetActive(true);
                sListNeed.gameObject.SetActive(true);
            }
            else
            {
                sListNeed.gameObject.SetActive(false);
                lListNeed.gameObject.SetActive(false);
            }
        }
        else
        {
            sListNeed.gameObject.SetActive(false);
            lListNeed.gameObject.SetActive(false);
            sProgressBackground.fillAmount = 1;
            lProgressValue.text = CachePvp.myVipPoint + "/" + listTarget[listTarget.Count - 1];
        }

        //if (CachePvp.myVip == vip)
        //{
        //    progressGameobject.SetActive(true);
        //}
        //else
        //{
        //    progressGameobject.SetActive(false);
        //}

        //if (CachePvp.myVip == 10)
        //{
        //    sBoxRight.gameObject.SetActive(false);
        //}
        //else
        //{
        //    sBoxRight.gameObject.SetActive(true);
        //    sBoxRight.spriteName = ICON_VIP_SPRITE_NAME + CachePvp.myVip;
        //}
        //if (CachePvp.myVip == 0)
        //{
        //    sBoxLeft.gameObject.SetActive(false);
        //}
        //else
        //{
        //    sBoxLeft.gameObject.SetActive(true);
        //    sBoxLeft.spriteName = ICON_VIP_SPRITE_NAME + (CachePvp.myVip - 1);
        //}
        sVipIcon.spriteName = ICON_VIP_SPRITE_NAME + vip;
        if (CachePvp.myVip <= vip)
        {
            for (int i = 0; i < sListBenefit.Length; i++)
            {
                sListBenefit[i].spriteName = ICON_VIP_DOT_LIST_SPRITE_NAME;
            }
        }
        else
        {
            for (int i = 0; i < sListBenefit.Length; i++)
            {
                sListBenefit[i].spriteName = ICON_VIP_CHECK_LIST_SPRITE_NAME;
            }
        }
        if (vip == 0)
        {
            for (int i = 0; i < lListBenefit.Length; i++)
            {
                if (i < listReward[vip].types.Count)
                {
                    lListBenefit[i].gameObject.SetActive(true);
                    sListBenefit[i].gameObject.SetActive(true);
                    lListBenefit[i].text = VipSystemDescription(listReward[vip].types[i]);
                }
                else if (i == listReward[vip].types.Count)
                {
                    lListBenefit[i].gameObject.SetActive(true);
                    sListBenefit[i].gameObject.SetActive(true);
                    lListBenefit[i].text = VipSystemDescription(ICON) + " " + I2.Loc.ScriptLocalization.vip_key.Replace("%{number}", (vip + 1).ToString());
                }
                else
                {
                    lListBenefit[i].gameObject.SetActive(false);
                    sListBenefit[i].gameObject.SetActive(false);
                }
            }
        }
        else
        {
            for (int i = 0; i < lListBenefit.Length; i++)
            {
                if (i == 0)
                {
                    lListBenefit[i].gameObject.SetActive(true);
                    sListBenefit[i].gameObject.SetActive(true);

                    string vipFeatures = I2.Loc.ScriptLocalization.popup_vip_features;
                    lListBenefit[i].text = vipFeatures.Replace("%{level_vip}", vip.ToString());
                }
                else
                {
                    if (i <= listReward[vip].types.Count)
                    {
                        lListBenefit[i].gameObject.SetActive(true);
                        sListBenefit[i].gameObject.SetActive(true);
                        lListBenefit[i].text = VipSystemDescription(listReward[vip].types[i - 1]);
                    }
                    else if (i == listReward[vip].types.Count + 1)
                    {
                        lListBenefit[i].gameObject.SetActive(true);
                        sListBenefit[i].gameObject.SetActive(true);
                        lListBenefit[i].text = VipSystemDescription(ICON) + " " + I2.Loc.ScriptLocalization.vip_key.Replace("%{number}", (vip + 1).ToString());
                    }
                    else
                    {
                        lListBenefit[i].gameObject.SetActive(false);
                        sListBenefit[i].gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    public void ButtonLeft()
    {
        ShowInfo(--currentVip);
    }

    public void ButtonRight()
    {
        ShowInfo(++currentVip);
    }

    public void ButtonClose()
    {
        PopupManager.Instance.HideVipPopup();
    }

}

public class RewardPopupVip
{
    public List<string> types { get; set; }
}