﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.ootii.Messages;
using Mp.Pvp;
using System.Linq;
using System;

public class InfiniteListPopupLiveScore : MonoBehaviour
{
    private UIPanel thisUIPanel;
    public UILabel lTotalMatch;
    public GameObject fxLoading;
    // events to listen to if needed...
    public delegate void InfiniteItemIsPressed(int itemDataIndex, bool isDown);

    public event InfiniteItemIsPressed InfiniteItemIsPressedEvent;

    public delegate void InfiniteItemIsClicked(int itemDataIndex);

    public event InfiniteItemIsClicked InfiniteItemIsClickedEvent;

    public bool enableLog = true;
    //Prefabs
    const string listItemTag = "listItem";
    const string listSectionTag = "listSection";
    public Transform itemPrefab;
    public Transform sectionPrefab;
    // NGUI Controllers
    public UITable table;
    public UIScrollView draggablePanel;
    //scroll indicator
    public Transform scrollIndicator;
    private int scrollCursor = 0;
    // pool
    public float cellHeight = 94f;
    // at the moment we support fixed height... insert here or measure it
    private int poolSize = 6;
    public List<Transform> itemsPool = new List<Transform>();
    public List<InfiniteItemLiveScore> itemsPoolLiveScore = new List<InfiniteItemLiveScore>();
    public Dictionary<int, bool> itemsPoolLiveScoreDone = new Dictionary<int, bool>();
    private int extraBuffer = 10;
    private int startIndex = 0;
    // where to start
    private Hashtable dataTracker = new Hashtable();
    // hashtable to keep track of what is being displayed by the pool


    //our data...using arraylist for generic types... if you want specific types just refactor it to List<T> where T is the type
    private List<RoomInfo> originalData = new List<RoomInfo>();
    private List<RoomInfo> dataList = new List<RoomInfo>();

    private List<CoopPvPRoomData> originalData2v2 = new List<CoopPvPRoomData>();
    private List<CoopPvPRoomData> dataList2v2 = new List<CoopPvPRoomData>();
    //our sections
    private int numberOfSections = 0;
    private List<int> sectionsIndices = new List<int>();

    #region Start & Update for MonoBehaviour

    List<RoomInfo> listRoomInfo = new List<RoomInfo>();
    List<CoopPvPRoomData> listRoom2v2Info = new List<CoopPvPRoomData>();
    Dictionary<string, RoomInfo> dictionaryRoomInfo = new Dictionary<string, RoomInfo>();
    Dictionary<string, CoopPvPRoomData> dictionaryRoom2v2Info = new Dictionary<string, CoopPvPRoomData>();
    WaitForSeconds delay;

    void Awake()
    {
        thisUIPanel = GetComponent<UIPanel>();
        MessageDispatcher.AddListener(EventName.LiveScore.LoadedListRoom.ToString(), OnLoadedListRoom, true);
        MessageDispatcher.AddListener(EventName.LiveScore.LoadedRoomInfo.ToString(), OnLoadedRoomInfo, true);
        delay = new WaitForSeconds(120);
    }

    private void OnEnable()
    {
        for (int i = 0; i < transform.GetChild(0).childCount; i++)
        {
            transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        lTotalMatch.text = I2.Loc.ScriptLocalization.game_match.Replace("%{total_game}", "0");
        isRefreshing = true;
        fxLoading.SetActive(true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LiveScore.LoadedListRoom.ToString(), OnLoadedListRoom, true);
        MessageDispatcher.RemoveListener(EventName.LiveScore.LoadedRoomInfo.ToString(), OnLoadedRoomInfo, true);
    }

    private void OnLoadedRoomInfo(IMessage rMessage)
    {
        SCLiveScore ls = rMessage.Data as SCLiveScore;
        if (ls.status == SCLiveScore.SUCCESS)
        {
            if (CachePvp.LoadLiveScore == 2)
            {
                CoopPvPRoomData ri = ls.roomData;
                for (int i = 0; i < itemsPoolLiveScore.Count; i++)
                {
                    if (itemsPoolLiveScore[i].gameObject.activeInHierarchy && itemsPoolLiveScore[i].m_RoomInfo2v2 != null && itemsPoolLiveScore[i].m_RoomInfo2v2.id.Equals(ri.id))
                    {
                        itemsPoolLiveScore[i].UpdateData2v2(ri);
                        break;
                    }
                }

                if (dictionaryRoom2v2Info != null && dictionaryRoom2v2Info.ContainsKey(ri.id))
                {
                    dictionaryRoom2v2Info[ri.id].state = ri.state;
                    dictionaryRoom2v2Info[ri.id].teams = ri.teams;
                    dictionaryRoom2v2Info[ri.id].remainTime = ri.remainTime;
                }
            }
            else
            {
                RoomInfo ri = ls.room;
                for (int i = 0; i < itemsPoolLiveScore.Count; i++)
                {
                    if (itemsPoolLiveScore[i].gameObject.activeInHierarchy && itemsPoolLiveScore[i] != null && itemsPoolLiveScore[i].m_RoomInfo.roomId.Equals(ri.roomId))
                    {
                        itemsPoolLiveScore[i].UpdateData(ri);
                        break;
                    }
                }

                if (dictionaryRoomInfo != null && dictionaryRoomInfo.ContainsKey(ri.roomId))
                {
                    dictionaryRoomInfo[ri.roomId].state = ri.state;
                    dictionaryRoomInfo[ri.roomId].playerResults = ri.playerResults;
                    dictionaryRoomInfo[ri.roomId].map = ri.map;
                    dictionaryRoomInfo[ri.roomId].timeRemaining = ri.timeRemaining;
                }
            }
        }
    }

    public InfiniteItemLiveScore GetItemByDataIndex(int dataIndex)
    {
        for (int i = 0; i < itemsPoolLiveScore.Count; i++)
        {
            if (itemsPoolLiveScore[i].itemDataIndex == dataIndex)
            {
                return itemsPoolLiveScore[i];
            }
        }
        return null;
    }

    public void ButtonRefresh()
    {
        if (isRefreshing)
        {
            return;
        }
        for (int i = 0; i < itemsPool.Count; i++)
        {
            itemsPool[i].gameObject.SetActive(false);
        }
        fxLoading.SetActive(true);
        if (CachePvp.LoadLiveScore == 1)
        {
            new CSLiveScoreJoin(CSLiveScoreJoin.TYPE_MEGA).Send();
        }
        else if (CachePvp.LoadLiveScore == 0)
        {
            new CSLiveScoreJoin(CSLiveScoreJoin.TYPE_PVP).Send();
        }
        else if (CachePvp.LoadLiveScore == 2)
        {
            new CSLiveScoreJoin(CSLiveScoreJoin.TYPE_PVP_2VS2).Send();
        }
        isRefreshing = true;
    }

    bool isRefreshing;

    IEnumerator wait2Minutes;

    IEnumerator Wait2Minutes()
    {
        Debug.Log(delay);
        yield return delay;
        Debug.Log("delay");
        ButtonRefresh();
    }

    void OnLoadedListRoom(IMessage msg)
    {
        SCLiveScoreJoin lsj = msg.Data as SCLiveScoreJoin;
        Debug.Log("CachePvp.LoadLiveScore : " + CachePvp.LoadLiveScore);
        fxLoading.SetActive(false);
        if (CachePvp.LoadLiveScore == 2)
        {
            listRoom2v2Info = lsj.roomDatas;
            dictionaryRoom2v2Info.Clear();
            for (int i = 0; i < listRoom2v2Info.Count; i++)
            {
                dictionaryRoom2v2Info.Add(listRoom2v2Info[i].id, listRoom2v2Info[i]);
            }
            lTotalMatch.text = I2.Loc.ScriptLocalization.game_match.Replace("%{total_game}", listRoom2v2Info.Count.ToString());
            if (isRefreshing)
            {
                InitTableView2v2(listRoom2v2Info, null, 0);
                isRefreshing = false;
                if (wait2Minutes != null)
                {
                    StopCoroutine(wait2Minutes);
                }
                wait2Minutes = Wait2Minutes();
                StartCoroutine(wait2Minutes);
            }
            else
            {
                for (int i = 0; i < itemsPoolLiveScore.Count; i++)
                {
                    if (itemsPoolLiveScore[i].gameObject.activeInHierarchy)
                    {
                        itemsPoolLiveScore[i].SetData2v2(dictionaryRoom2v2Info[itemsPoolLiveScore[i].m_RoomInfo2v2.id]);
                        break;
                    }
                }
            }
        }
        else
        {
            listRoomInfo = lsj.rooms;
            dictionaryRoomInfo.Clear();
            for (int i = 0; i < listRoomInfo.Count; i++)
            {
                dictionaryRoomInfo.Add(listRoomInfo[i].roomId, listRoomInfo[i]);
            }
            lTotalMatch.text = I2.Loc.ScriptLocalization.game_match.Replace("%{total_game}", listRoomInfo.Count.ToString());
            if (isRefreshing)
            {
                InitTableView(listRoomInfo, null, 0);
                isRefreshing = false;
                if (wait2Minutes != null)
                {
                    StopCoroutine(wait2Minutes);
                }
                wait2Minutes = Wait2Minutes();
                StartCoroutine(wait2Minutes);
            }
            else
            {
                for (int i = 0; i < itemsPoolLiveScore.Count; i++)
                {
                    if (itemsPoolLiveScore[i].gameObject.activeInHierarchy)
                    {
                        itemsPoolLiveScore[i].SetData(dictionaryRoomInfo[itemsPoolLiveScore[i].m_RoomInfo.roomId]);
                        break;
                    }
                }
            }
        }
    }

    void Start()
    {
        // check prefabs
        if (itemPrefab == null)
            Debug.LogError("InfiniteListPopulator:: itemPrefab is not assigned");
        else if (!itemPrefab.tag.Equals(listItemTag))
            Debug.LogError("InfiniteListPopulator:: itemPrefab tag should be " + listItemTag);
        if (sectionPrefab == null)
            Debug.LogError("InfiniteListPopulator:: sectionPrefab is not assigned");
        else if (!sectionPrefab.tag.Equals(listSectionTag))
            Debug.LogError("InfiniteListPopulator:: sectionPrefab tag should be " + listSectionTag);
    }
    #endregion

    #region Infinite List Data Sources calls

    /*
     * These methods are used mainly to populate 
     * the data for both sections and rows.. 
     * change to suit your implementation
     * 
     * */

    string GetTitleForSection(int i)
    {
        return "Section " + i;
    }

    void PopulateListSectionWithIndex(Transform item, int index)
    {
        item.GetComponent<InfiniteSectionLiveScore>().label.text = GetTitleForSection(index);
    }

    void PopulateListItemWithIndex(Transform item, int dataIndex, int oldIndex = -1)
    {
        if (CachePvp.LoadLiveScore == 2)
        {
            if (listRoom2v2Info.Count > dataIndex)
            {
                item.GetComponent<InfiniteItemLiveScore>().SetData2v2(listRoom2v2Info[dataIndex], oldIndex);
            }
        }
        else
        {
            if (listRoomInfo.Count > dataIndex)
            {
                item.GetComponent<InfiniteItemLiveScore>().SetData(listRoomInfo[dataIndex], oldIndex);
            }
        }
    }

    #endregion

    #region Infinite List Management & scrolling

    // set then call InitTableView
    public void SetStartIndex(int inStartIndex)
    {
        startIndex = GetJumpIndexForItem(inStartIndex);
    }

    public void SetOriginalData(List<RoomInfo> inDataList)
    {
        originalData = new List<RoomInfo>(inDataList);
    }

    public void SetSectionIndices(List<int> inSectionsIndices)
    {
        numberOfSections = inSectionsIndices.Count;
        sectionsIndices = inSectionsIndices;
    }
    // call to refresh without changing sections.. e.g. jump to specific point...
    public void RefreshTableView()
    {
        if (enableLog)
        {
            if (originalData == null || originalData.Count == 0)
                Debug.LogWarning("InfiniteListPopulator.InitTableView() trying to refresh with no data");
        }
        if (CachePvp.LoadLiveScore == 2)
        {
            InitTableView2v2(originalData2v2, sectionsIndices, startIndex);
        }
        else
        {
            InitTableView(originalData, sectionsIndices, startIndex);
        }
    }

    public void InitTableView(List<RoomInfo> inDataList, List<int> inSectionsIndices, int inStartIndex)
    {
        InitTableViewImp(inDataList, inSectionsIndices, inStartIndex);
    }

    public void InitTableView2v2(List<CoopPvPRoomData> inDataList, List<int> inSectionsIndices, int inStartIndex)
    {
        InitTableViewImp2v2(inDataList, inSectionsIndices, inStartIndex);
    }

    #endregion

    #region The private stuff... ideally you shouldn't need to call or change things directly from this region onwards

    void InitTableViewImp(List<RoomInfo> inDataList, List<int> inSectionsIndices, int inStartIndex)
    {
        RefreshPool();
        startIndex = inStartIndex;
        scrollCursor = inStartIndex;
        dataTracker.Clear();
        originalData = new List<RoomInfo>(inDataList);
        dataList = new List<RoomInfo>(inDataList);
        if (inSectionsIndices != null)
        {
            numberOfSections = inSectionsIndices.Count;
            sectionsIndices = inSectionsIndices;
        }
        else
        {
            numberOfSections = 0;
            sectionsIndices = new List<int>();
        }
        // do we have a section? then inject 'special' data inside the date list
        if (numberOfSections > 0)
        {

            for (int i = 0; i < numberOfSections; i++)
            {
                sectionsIndices[i] += i;
                if (sectionsIndices[i] < dataList.Count)
                    dataList.Insert(sectionsIndices[i], null);// testing with null data
                else
                {
                    if (enableLog)
                    {
                        Debug.LogWarning("InfiniteListPopulator.InitTableView() section index " + (sectionsIndices[i] - i) + " value is larger than last data index and is ignored");
                    }
                    sectionsIndices.RemoveAt(i);
                    numberOfSections--;
                }
            }
        }

        int j = 0;
        for (int i = startIndex; i < dataList.Count; i++)
        {
            Transform item = GetItemFromPool(j);
            if (item != null)
            {
                // is it a section index?
                if (sectionsIndices.Contains(i) && item.tag.Equals(listItemTag))
                {
                    // change item to section
                    InitSection(item, i, j);
                }
                else
                {
                    InitListItemWithIndex(item, i, j);
                }
                if (enableLog)
                {
                    Debug.Log(item.name + "::" + item.tag);
                }
                j++;

            }
            else
            { // end of pool

                break;
            }
        }

        // at the moment we are repositioning the list after a delay... repositioning immediatly messes up the table when refreshing... no clue why...
        //Invoke ("RepositionList", 0.1f);
        LeanTween.delayedCall(0.1f, () =>
        {
            RepositionList();
        }).setIgnoreTimeScale(true);
    }

    void InitTableViewImp2v2(List<CoopPvPRoomData> inDataList, List<int> inSectionsIndices, int inStartIndex)
    {
        RefreshPool();
        startIndex = inStartIndex;
        scrollCursor = inStartIndex;
        dataTracker.Clear();
        originalData2v2 = new List<CoopPvPRoomData>(inDataList);
        dataList2v2 = new List<CoopPvPRoomData>(inDataList);
        if (inSectionsIndices != null)
        {
            numberOfSections = inSectionsIndices.Count;
            sectionsIndices = inSectionsIndices;
        }
        else
        {
            numberOfSections = 0;
            sectionsIndices = new List<int>();
        }
        // do we have a section? then inject 'special' data inside the date list
        if (numberOfSections > 0)
        {

            for (int i = 0; i < numberOfSections; i++)
            {
                sectionsIndices[i] += i;
                if (sectionsIndices[i] < dataList2v2.Count)
                    dataList2v2.Insert(sectionsIndices[i], null);// testing with null data
                else
                {
                    if (enableLog)
                    {
                        Debug.LogWarning("InfiniteListPopulator.InitTableView() section index " + (sectionsIndices[i] - i) + " value is larger than last data index and is ignored");
                    }
                    sectionsIndices.RemoveAt(i);
                    numberOfSections--;
                }
            }
        }

        int j = 0;
        for (int i = startIndex; i < dataList2v2.Count; i++)
        {
            Transform item = GetItemFromPool(j);
            if (item != null)
            {
                // is it a section index?
                if (sectionsIndices.Contains(i) && item.tag.Equals(listItemTag))
                {
                    // change item to section
                    InitSection(item, i, j);
                }
                else
                {
                    InitListItemWithIndex(item, i, j);
                }
                if (enableLog)
                {
                    Debug.Log(item.name + "::" + item.tag);
                }
                j++;

            }
            else
            { // end of pool

                break;
            }
        }

        // at the moment we are repositioning the list after a delay... repositioning immediatly messes up the table when refreshing... no clue why...
        //Invoke ("RepositionList", 0.1f);
        LeanTween.delayedCall(0.1f, () =>
        {
            RepositionList();
        }).setIgnoreTimeScale(true);
    }

    public void RepositionList()
    {
        table.Reposition();
        table.transform.localPosition = new Vector3(0, table.transform.localPosition.y, table.transform.position.z);
        // make sure we have a correct poistion sequence
        draggablePanel.SetDragAmount(0, 0, false);
        thisUIPanel.clipOffset = new Vector2(0, 52);
        thisUIPanel.transform.localPosition = new Vector3(thisUIPanel.transform.localPosition.x, -261.2f, thisUIPanel.transform.localPosition.z);
        for (int i = 0; i < itemsPool.Count; i++)
        {
            Transform item = itemsPool[i];
            item.localPosition = new Vector3(0, -((cellHeight / 2) + i * cellHeight), 0);
        }
        draggablePanel.ResetPosition();
    }

    // sections
    void InitSection(Transform item, int dataIndex, int poolIndex)
    {
        Destroy(item.gameObject);
        item = Instantiate(sectionPrefab) as Transform;
        item.GetComponent<InfiniteSectionLiveScore>().itemNumber = poolIndex;

        item.name = "item" + dataIndex;
        item.parent = table.transform;
        item.GetComponent<InfiniteSectionLiveScore>().itemDataIndex = dataIndex;
        item.GetComponent<InfiniteSectionLiveScore>().listPopulator = this;
        item.GetComponent<InfiniteSectionLiveScore>().panel = draggablePanel.panel;
        PopulateListSectionWithIndex(item, sectionsIndices.IndexOf(dataIndex));

        itemsPool[poolIndex] = item;
        dataTracker.Add(itemsPool[poolIndex].GetComponent<InfiniteSectionLiveScore>().itemDataIndex, itemsPool[poolIndex].GetComponent<InfiniteSectionLiveScore>().itemNumber);

    }

    void ChangeItemToSection(Transform item, int newIndex, int oldIndex)
    {
        int j = 0;
        Vector3 lastPosition = Vector3.zero;
        if (item.tag.Equals(listItemTag))
            j = item.GetComponent<InfiniteItemLiveScore>().itemNumber;
        if (item.tag.Equals(listSectionTag))
            j = item.GetComponent<InfiniteSectionLiveScore>().itemNumber;

        lastPosition = item.localPosition;
        Destroy(item.gameObject);
        item = Instantiate(sectionPrefab) as Transform;
        item.parent = table.transform;
        item.localPosition = lastPosition;
        if (newIndex < oldIndex)
            item.localPosition += new Vector3(0, (poolSize) * cellHeight, 0);
        else
            item.localPosition -= new Vector3(0, (poolSize) * cellHeight, 0);

        item.name = "item" + (newIndex);
        item.GetComponent<InfiniteSectionLiveScore>().itemNumber = j;
        item.GetComponent<InfiniteSectionLiveScore>().itemDataIndex = newIndex;
        item.GetComponent<InfiniteSectionLiveScore>().listPopulator = this;
        item.GetComponent<InfiniteSectionLiveScore>().panel = draggablePanel.panel;
        PopulateListSectionWithIndex(item, sectionsIndices.IndexOf(newIndex));
        itemsPool[j] = item;
        dataTracker.Add(newIndex, (int)(dataTracker[oldIndex]));
        dataTracker.Remove(oldIndex);
    }

    // items
    void InitListItemWithIndex(Transform item, int dataIndex, int poolIndex)
    {
        InfiniteItemLiveScore script = item.GetComponent<InfiniteItemLiveScore>();
        script.itemDataIndex = dataIndex;
        script.listPopulator = this;
        script.panel = draggablePanel.panel;
        item.name = "item" + dataIndex;
        PopulateListItemWithIndex(item, dataIndex);
        dataTracker.Add(itemsPool[poolIndex].GetComponent<InfiniteItemLiveScore>().itemDataIndex, itemsPool[poolIndex].GetComponent<InfiniteItemLiveScore>().itemNumber);

    }

    void PrepareListItemWithIndex(Transform item, int newIndex, int oldIndex)
    {
        InfiniteItemLiveScore script = item.GetComponent<InfiniteItemLiveScore>();
        if (newIndex < oldIndex)
            item.localPosition += new Vector3(0, (poolSize) * cellHeight, 0);
        else
            item.localPosition -= new Vector3(0, (poolSize) * cellHeight, 0);
        script.itemDataIndex = newIndex;
        item.name = "item" + (newIndex);
        if (!itemsPoolLiveScoreDone.ContainsKey(newIndex))
        {
            itemsPoolLiveScoreDone.Add(newIndex, false);
        }
        script.isDone = itemsPoolLiveScoreDone[newIndex];
        PopulateListItemWithIndex(item, newIndex, oldIndex);
        dataTracker.Add(newIndex, (int)(dataTracker[oldIndex]));
        dataTracker.Remove(oldIndex);

    }

    void ChangeSectionToItem(Transform item, int newIndex, int oldIndex)
    {
        int j = 0;
        Vector3 lastPosition = Vector3.zero;
        j = item.GetComponent<InfiniteSectionLiveScore>().itemNumber;
        lastPosition = item.localPosition;
        Destroy(item.gameObject);
        item = Instantiate(itemPrefab) as Transform;
        item.parent = table.transform;
        item.localPosition = lastPosition;
        item.GetComponent<InfiniteItemLiveScore>().itemNumber = j;
        item.GetComponent<InfiniteItemLiveScore>().listPopulator = this;
        item.GetComponent<InfiniteItemLiveScore>().panel = draggablePanel.panel;
        itemsPool[j] = item;
        itemsPoolLiveScore[j] = item.GetComponent<InfiniteItemLiveScore>();

        //maxHeight = lMessage.height;
        //boxCollider.size = new Vector3(boxCollider.size.x, (maxHeight - 43) + 254, boxCollider.size.z);

        PrepareListItemWithIndex(item, newIndex, oldIndex);
    }

    // the main logic for "infinite scrolling"...
    private bool isUpdatingList = false;

    public IEnumerator ItemIsInvisible(int itemNumber)
    {
        if (isUpdatingList)
            yield return null;
        isUpdatingList = true;
        if ((CachePvp.LoadLiveScore == 2 && dataList2v2.Count > poolSize) || (CachePvp.LoadLiveScore != 2 && dataList.Count > poolSize))
        {// we need to do something "smart"... 
            Transform item = itemsPool[itemNumber];
            int itemDataIndex = 0;
            if (item.tag.Equals(listItemTag))
                itemDataIndex = item.GetComponent<InfiniteItemLiveScore>().itemDataIndex;
            if (item.tag.Equals(listSectionTag))
                itemDataIndex = item.GetComponent<InfiniteSectionLiveScore>().itemDataIndex;

            int indexToCheck = 0;
            InfiniteItemLiveScore infItem = null;
            InfiniteSectionLiveScore infSection = null;
            if (dataTracker.ContainsKey(itemDataIndex + 1))
            {
                infItem = itemsPool[(int)(dataTracker[itemDataIndex + 1])].GetComponent<InfiniteItemLiveScore>();
                infSection = itemsPool[(int)(dataTracker[itemDataIndex + 1])].GetComponent<InfiniteSectionLiveScore>();

                if ((infItem != null && infItem.verifyVisibility()) || (infSection != null && infSection.verifyVisibility()))
                {
                    // dragging upwards (scrolling down)
                    indexToCheck = itemDataIndex - (extraBuffer / 2);
                    if (dataTracker.ContainsKey(indexToCheck))
                    {
                        //do we have an extra item(s) as well?
                        for (int i = indexToCheck; i >= 0; i--)
                        {
                            if (dataTracker.ContainsKey(i))
                            {
                                infItem = itemsPool[(int)(dataTracker[i])].GetComponent<InfiniteItemLiveScore>();
                                infSection = itemsPool[(int)(dataTracker[i])].GetComponent<InfiniteSectionLiveScore>();
                                if ((infItem != null && !infItem.verifyVisibility()) || (infSection != null && !infSection.verifyVisibility()))
                                {
                                    item = itemsPool[(int)(dataTracker[i])];
                                    if (((i + poolSize < dataList.Count && CachePvp.LoadLiveScore != 2) || (i + poolSize < dataList2v2.Count && CachePvp.LoadLiveScore == 2)) && i > -1)
                                    {
                                        // is it a section index?
                                        if (sectionsIndices.Contains(i + poolSize))
                                        {
                                            // change item to section
                                            ChangeItemToSection(item, i + poolSize, i);
                                        }
                                        else if (item.tag.Equals(listSectionTag))
                                        {
                                            // change section to item
                                            ChangeSectionToItem(item, i + poolSize, i);
                                        }
                                        else
                                        {
                                            PrepareListItemWithIndex(item, i + poolSize, i);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                scrollCursor = itemDataIndex - 1;
                                break;
                            }
                        }
                    }
                }
            }
            if (dataTracker.ContainsKey(itemDataIndex - 1))
            {
                infItem = itemsPool[(int)(dataTracker[itemDataIndex - 1])].GetComponent<InfiniteItemLiveScore>();
                infSection = itemsPool[(int)(dataTracker[itemDataIndex - 1])].GetComponent<InfiniteSectionLiveScore>();

                if ((infItem != null && infItem.verifyVisibility()) || (infSection != null && infSection.verifyVisibility()))
                {
                    //dragging downwards check the item below
                    indexToCheck = itemDataIndex + (extraBuffer / 2);

                    if (dataTracker.ContainsKey(indexToCheck))
                    {
                        // if we have an extra item
                        if (CachePvp.LoadLiveScore == 2)
                        {
                            for (int i = indexToCheck; i < dataList2v2.Count; i++)
                            {
                                if (dataTracker.ContainsKey(i))
                                {
                                    infItem = itemsPool[(int)(dataTracker[i])].GetComponent<InfiniteItemLiveScore>();
                                    infSection = itemsPool[(int)(dataTracker[i])].GetComponent<InfiniteSectionLiveScore>();
                                    if ((infItem != null && !infItem.verifyVisibility()) || (infSection != null && !infSection.verifyVisibility()))
                                    {
                                        item = itemsPool[(int)(dataTracker[i])];
                                        if ((i) - poolSize > -1 && (i) < dataList2v2.Count)
                                        {
                                            // is it a section index?
                                            if (sectionsIndices.Contains(i - poolSize))
                                            {
                                                // change item to section
                                                ChangeItemToSection(item, i - poolSize, i);
                                            }
                                            else if (item.tag.Equals(listSectionTag))
                                            {
                                                // change section to item
                                                ChangeSectionToItem(item, i - poolSize, i);
                                            }
                                            else
                                            {
                                                PrepareListItemWithIndex(item, i - poolSize, i);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    scrollCursor = itemDataIndex + 1;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            for (int i = indexToCheck; i < dataList.Count; i++)
                            {
                                if (dataTracker.ContainsKey(i))
                                {
                                    infItem = itemsPool[(int)(dataTracker[i])].GetComponent<InfiniteItemLiveScore>();
                                    infSection = itemsPool[(int)(dataTracker[i])].GetComponent<InfiniteSectionLiveScore>();
                                    if ((infItem != null && !infItem.verifyVisibility()) || (infSection != null && !infSection.verifyVisibility()))
                                    {
                                        item = itemsPool[(int)(dataTracker[i])];
                                        if ((i) - poolSize > -1 && (i) < dataList.Count)
                                        {
                                            // is it a section index?
                                            if (sectionsIndices.Contains(i - poolSize))
                                            {
                                                // change item to section
                                                ChangeItemToSection(item, i - poolSize, i);
                                            }
                                            else if (item.tag.Equals(listSectionTag))
                                            {
                                                // change section to item
                                                ChangeSectionToItem(item, i - poolSize, i);
                                            }
                                            else
                                            {
                                                PrepareListItemWithIndex(item, i - poolSize, i);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    scrollCursor = itemDataIndex + 1;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        isUpdatingList = false;
    }

    #endregion

    #region items callbacks and helpers

    int GetJumpIndexForItem(int itemDataIndex)
    {
        // find real data index
        if (numberOfSections > 0 && itemDataIndex > sectionsIndices[0])
        {
            for (int index = numberOfSections - 1; index >= 0; index--)
            {
                if (itemDataIndex > sectionsIndices[index])
                {
                    itemDataIndex = itemDataIndex + (index + 1);
                    break;
                }

            }
        }
        return itemDataIndex;
    }

    public int GetRealIndexForItem(int itemDataIndex)
    {
        // find real data index
        if (numberOfSections > 0 && itemDataIndex > sectionsIndices[0])
        {

            for (int index = numberOfSections - 1; index >= 0; index--)
            {
                if (itemDataIndex > sectionsIndices[index])
                {
                    itemDataIndex = itemDataIndex - (index + 1);
                    break;
                }

            }
        }
        return itemDataIndex;
    }

    public void itemIsPressed(int itemDataIndex, bool isDown)
    {
        itemDataIndex = GetRealIndexForItem(itemDataIndex);
        if (enableLog)
        {
            Debug.Log("Pressed down item " + itemDataIndex + " " + isDown);
        }
        if (InfiniteItemIsPressedEvent != null)
            InfiniteItemIsPressedEvent(itemDataIndex, isDown);
    }

    public void itemClicked(int itemDataIndex)
    {
        itemDataIndex = GetRealIndexForItem(itemDataIndex);
        if (enableLog)
        {
            Debug.Log("Clicked item " + itemDataIndex);
        }
        if (InfiniteItemIsClickedEvent != null)
            InfiniteItemIsClickedEvent(itemDataIndex);
    }

    #endregion

    #region Pool & sections Management

    Transform GetItemFromPool(int i)
    {
        if (i >= 0 && i < poolSize)
        {
            itemsPool[i].gameObject.SetActive(true);
            return itemsPool[i];
        }
        else
            return null;
    }

    void RefreshPool()
    {
        if (CachePvp.LoadLiveScore == 2)
        {
            cellHeight = 204;
        }
        else
        {
            cellHeight = 130;
        }
        poolSize = (int)(draggablePanel.panel.baseClipRegion.w / cellHeight) + extraBuffer;
        if (enableLog)
        {
            Debug.Log("REFRESH POOL SIZE:::" + poolSize);
        }
        // destroy current items
        for (int i = 0; i < itemsPool.Count; i++)
        {
            Destroy(itemsPool[i].gameObject);
        }
        itemsPool.Clear();
        itemsPoolLiveScore.Clear();
        itemsPoolLiveScoreDone.Clear();
        for (int i = 0; i < poolSize; i++)
        { // the pool will use itemPrefab as a default
            Transform item = Instantiate(itemPrefab) as Transform;
            item.gameObject.SetActive(false);
            InfiniteItemLiveScore itemScript = item.GetComponent<InfiniteItemLiveScore>();
            itemScript.itemNumber = i;
            item.name = "item" + i;
            item.parent = table.transform;
            itemsPool.Add(item);
            itemsPoolLiveScore.Add(itemScript);
            itemsPoolLiveScoreDone.Add(i, false);
        }

    }

    #endregion
}
