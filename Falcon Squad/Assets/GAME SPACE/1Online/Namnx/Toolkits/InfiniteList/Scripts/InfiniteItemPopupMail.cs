﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using TCore;

public class InfiniteItemPopupMail : MonoBehaviour
{
    public UISprite sBackground;
    public BoxCollider boxCollider;
    public UISprite sStarSystem;
    public UISprite sSmallFrame;
    public UI2DSprite sAvatar;
    public UILabel lName;
    public UILabel lDate;

    public UISprite sVipIcon;

    public GameObject buttonReadmore;
    public GameObject buttonReadless;
    public UILabel lMessage;
    public UISprite[] sAwards;
    public GameObject[] sAwardsFade;
    public UILabel[] lAwards;

    public GameObject buttonDelete;
    public GameObject buttonClaim;
    public GameObject buttonClaimed;
    public GameObject buttonGoUrl;
    public GameObject buttonGoClanInfo;
    public GameObject buttonGoClanHome;
    public GameObject buttonGoShop;
    public GameObject buttonDoNoThing;
    public GameObject buttonYesConfirm;
    public GameObject buttonNoConfirm;

    public InfiniteListPopupMail listPopulator;

    public string facebookID;

    public UIPanel panel;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;

    public bool isOpened;
    public bool isDeleted;

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(sBackground));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    void OnClick()
    {
        listPopulator.itemClicked(itemDataIndex);
    }

    void OnPress(bool isDown)
    {
        listPopulator.itemIsPressed(itemDataIndex, isDown);
    }

    FalconMail m_FalcomMail;

    public void SetData(FalconMail fm, int oldIndex = -1)
    {
        if (isDeleted)
        {
            gameObject.SetActive(false);
            return;
        }
        else
        {
            gameObject.SetActive(true);
        }
        if (fm.player != null)
        {
            Debug.Log("player.vip : " + CachePvp.GetVipFromVipPoint(fm.player.vip));
        }
        if (fm.player == null || CachePvp.GetVipFromVipPoint(fm.player.vip) == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (CachePvp.GetVipFromVipPoint(fm.player.vip) - 1);
        }
        //avatar
        m_FalcomMail = fm;
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        if (fm.player != null && !string.IsNullOrEmpty(fm.player.code))
        {
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(fm.player.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(fm.player.appCenterId))
                    {
                        showImageFB = false;
                        facebookID = fm.player.appCenterId;
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[fm.player.appCenterId];
                    }
                    else
                    {
                        //Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(fm.player.appCenterId);
                        //if (t != null)
                        //{
                        //    showImageFB = false;
                        //    facebookID = fm.player.appCenterId;
                        //    CacheAvatarManager.Instance.DownloadImageFormUrl("", fm.player.appCenterId, t);
                        //}
                    }
                }
            }
            if (showImageFB)
            {
                facebookID = fm.player.facebookId;
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(fm.player.facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(fm.player.facebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[fm.player.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + fm.player.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, fm.player.facebookId);
                    }
                }
            }
            lName.text = fm.player.name;
            sStarSystem.gameObject.SetActive(false);
        }
        else
        {
            facebookID = "";
            lName.text = I2.Loc.ScriptLocalization.system;
            sStarSystem.gameObject.SetActive(true);
        }
        //date
        DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
        double dou = (double)fm.eventTime;
        DateTime dt = unixEpoch.AddMilliseconds(dou);
        lDate.text = dt.ToString("dd/MM/yyyy");
        //award
        for (int i = 0; i < sAwards.Length; i++)
        {
            sAwards[i].gameObject.SetActive(false);
            lAwards[i].gameObject.SetActive(false);
        }
        if (fm.action == FalconMail.ACTION_NOTHING || fm.action == FalconMail.ACTION_GO_URL || fm.action == FalconMail.ACTION_GO_SHOP)
        {
            //button
            buttonClaim.SetActive(false);
            buttonGoUrl.SetActive(false);
            buttonGoShop.SetActive(false);
            buttonClaimed.SetActive(false);
            buttonDoNoThing.SetActive(false);
            buttonYesConfirm.SetActive(false);
            buttonNoConfirm.SetActive(false);
            buttonGoClanInfo.SetActive(false);
            buttonGoClanHome.SetActive(false);
            if (fm.action == FalconMail.ACTION_NOTHING)
            {
                buttonDoNoThing.SetActive(true);
                //buttonGoClanHome.SetActive(true);
            }
            else if (fm.action == FalconMail.ACTION_GO_URL)
            {
                buttonGoUrl.SetActive(true);
            }
            else if (fm.action == FalconMail.ACTION_GO_SHOP)
            {
                buttonGoShop.SetActive(true);
            }
        }
        else if (fm.action == FalconMail.ACTION_CLAIM || fm.action == FalconMail.ACTION_CLAIMED)
        {
            //button
            buttonClaim.SetActive(false);
            buttonGoUrl.SetActive(false);
            buttonGoShop.SetActive(false);
            buttonClaimed.SetActive(false);
            buttonDoNoThing.SetActive(false);
            buttonYesConfirm.SetActive(false);
            buttonNoConfirm.SetActive(false);
            buttonGoClanInfo.SetActive(false);
            buttonGoClanHome.SetActive(false);
            if (fm.action == FalconMail.ACTION_CLAIM)
            {
                buttonClaim.SetActive(true);
            }
            else if (fm.action == FalconMail.ACTION_CLAIMED)
            {
                buttonClaimed.SetActive(true);
            }
        }
        else if (fm.action == FalconMail.ACTION_CLAN_INFO)
        {
            //button
            buttonClaim.SetActive(false);
            buttonGoUrl.SetActive(false);
            buttonGoShop.SetActive(false);
            buttonClaimed.SetActive(false);
            buttonDoNoThing.SetActive(false);
            buttonYesConfirm.SetActive(false);
            buttonNoConfirm.SetActive(false);
            buttonGoClanInfo.SetActive(true);
            buttonGoClanHome.SetActive(false);
        }
        else if (fm.action == FalconMail.ACTION_CLAN_HOME)
        {
            //button
            buttonClaim.SetActive(false);
            buttonGoUrl.SetActive(false);
            buttonGoShop.SetActive(false);
            buttonClaimed.SetActive(false);
            buttonDoNoThing.SetActive(false);
            buttonYesConfirm.SetActive(false);
            buttonNoConfirm.SetActive(false);
            buttonGoClanInfo.SetActive(false);
            buttonGoClanHome.SetActive(true);
        }
        if (fm.hasConfirm == 1)
        {
            //button
            buttonClaim.SetActive(false);
            buttonGoUrl.SetActive(false);
            buttonGoShop.SetActive(false);
            buttonClaimed.SetActive(false);
            buttonDoNoThing.SetActive(false);
            buttonYesConfirm.SetActive(true);
            buttonNoConfirm.SetActive(true);
            buttonGoClanInfo.SetActive(false);
            buttonGoClanHome.SetActive(false);
        }
        if (fm.items != null && fm.items.Count > 0)
        {
            if (fm.hasClaimed == 0)
            {
                for (int i = 0; i < sAwardsFade.Length; i++)
                {
                    sAwardsFade[i].SetActive(false);
                }
            }
            else
            {
                for (int i = 0; i < sAwardsFade.Length; i++)
                {
                    sAwardsFade[i].SetActive(true);
                }
            }

            int cnt = 0;
            foreach (var item in fm.items)
            {
                if (cnt < 3)
                {
                    sAwards[cnt].spriteName = FalconMail.GetSpriteName(item.key, item.value);
                    switch (item.key)
                    {
                        case FalconMail.ITEM_GOLD:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_GEM:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_ENERGY:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_POWER_UP:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_ACTIVESKILL:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_LIFE:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_PLANE:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_WINGMAN:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_WING:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_SECONDARY_WEAPON:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(false);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_GENERAL:
                            //MinhCacheGame.SetSpaceShipGeneralCards(MinhCacheGame.GetSpaceShipGeneralCards() + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_1:
                            //MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.BataFD, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_2:
                            //MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.SkyWraith, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_3:
                            //MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.FuryOfAres, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_4:
                            //MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.MacBird, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_5:
                            //MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.TwilightX, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_6:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_7:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_PLANE_8:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_GENERAL:
                            //MinhCacheGame.SetWingmanGeneralCards(MinhCacheGame.GetWingmanGeneralCards() + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_1:
                            //MinhCacheGame.SetWingmanCards(WingmanTypeEnum.GatlingGun, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.GatlingGun) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_2:
                            //MinhCacheGame.SetWingmanCards(WingmanTypeEnum.AutoGatlingGun, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.AutoGatlingGun) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_3:
                            //MinhCacheGame.SetWingmanCards(WingmanTypeEnum.Lazer, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Lazer) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_4:
                            //MinhCacheGame.SetWingmanCards(WingmanTypeEnum.DoubleGalting, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.DoubleGalting) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_5:
                            //MinhCacheGame.SetWingmanCards(WingmanTypeEnum.HomingMissile, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_6:
                            //MinhCacheGame.SetWingmanCards(WingmanTypeEnum.HomingMissile, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile) + item.value);
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_GENERAL:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_1:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_2:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_CARD_WING_3:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_VIP_POINT:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_LUCKY_WHEEL_TICKET:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_PREMIUM_PACK:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].text = GameContext.FormatNumber(item.value);
                            break;
                        case FalconMail.ITEM_INFINITY_PACK:
                            sAwards[cnt].gameObject.SetActive(true);
                            lAwards[cnt].gameObject.SetActive(false);
                            break;
                        default:
                            break;
                    }
                }
                cnt++;
            }
        }
        //message
        lMessage.text = fm.message;
        //button readmore
        if (isOpened)
        {
            buttonReadmore.SetActive(false);
            buttonReadless.SetActive(true);
            lMessage.overflowMethod = UILabel.Overflow.ResizeHeight;
            StartCoroutine(Wait1Frame());
        }
        else
        {
            buttonReadmore.SetActive(true);
            buttonReadless.SetActive(false);
            lMessage.overflowMethod = UILabel.Overflow.ClampContent;
            lMessage.overflowEllipsis = true;
            lMessage.height = 43;
            boxCollider.size = new Vector3(boxCollider.size.x, 254, boxCollider.size.z);

            sBackground.height = 234;
            listPopulator.itemsPoolPopupMailHeight[itemDataIndex] = 254;
        }
    }

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(sBackground);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }

    int maxHeight = 0;

    public void ButtonReadmore()
    {
        isOpened = true;
        listPopulator.itemsPoolPopupMailClickReadmore[itemDataIndex] = isOpened;
        buttonReadmore.SetActive(false);
        buttonReadless.SetActive(true);

        lMessage.overflowMethod = UILabel.Overflow.ResizeHeight;
        StartCoroutine(Wait1Frame());
    }

    IEnumerator Wait1Frame()
    {
        yield return new WaitForEndOfFrame();
        maxHeight = Mathf.Max(43, lMessage.height);
        boxCollider.size = new Vector3(boxCollider.size.x, (maxHeight - 43) + 254, boxCollider.size.z);
        sBackground.height = 234 + (maxHeight - 43);
        listPopulator.itemsPoolPopupMailHeight[itemDataIndex] = (maxHeight - 43) + 254;
        Reposition();
    }

    void Reposition()
    {
        float posY = listPopulator.itemsPoolPopupMail[itemNumber].transform.localPosition.y;
        for (int i = itemDataIndex; i < listPopulator.itemsPoolPopupMailPositionY.Count; i++)
        {
            if (listPopulator.GetItemByDataIndex(i) != null)
            {
                listPopulator.GetItemByDataIndex(i).transform.localPosition = new Vector3(0, posY, 0);
            }
            listPopulator.itemsPoolPopupMailPositionY[i] = posY;
            posY -= listPopulator.itemsPoolPopupMailHeight[i];
        }
    }

    public void ButtonReadless()
    {
        isOpened = false;
        listPopulator.itemsPoolPopupMailClickReadmore[itemDataIndex] = isOpened;
        buttonReadmore.SetActive(true);
        buttonReadless.SetActive(false);

        lMessage.overflowMethod = UILabel.Overflow.ClampContent;
        lMessage.overflowEllipsis = true;
        lMessage.height = 43;
        boxCollider.size = new Vector3(boxCollider.size.x, 254, boxCollider.size.z);

        sBackground.height = 234;
        listPopulator.itemsPoolPopupMailHeight[itemDataIndex] = 254;
        Reposition();
    }

    public void bYesConfirmDelete()
    {
        listPopulator.DeleteFalconMailWithId(m_FalcomMail.id, itemDataIndex);
        isDeleted = true;

        StartCoroutine(WaitEndFrame());
    }

    public void bNoConfirmDelete()
    {
        m_FalcomMail.hasConfirm = 0;
        if (m_FalcomMail.action == FalconMail.ACTION_NOTHING || m_FalcomMail.action == FalconMail.ACTION_GO_URL || m_FalcomMail.action == FalconMail.ACTION_GO_SHOP)
        {
            //button
            buttonClaim.SetActive(false);
            buttonGoUrl.SetActive(false);
            buttonGoShop.SetActive(false);
            buttonClaimed.SetActive(false);
            buttonDoNoThing.SetActive(false);
            buttonYesConfirm.SetActive(false);
            buttonNoConfirm.SetActive(false);
            buttonDelete.SetActive(true);
            buttonGoClanInfo.SetActive(false);
            buttonGoClanHome.SetActive(false);
            if (m_FalcomMail.action == FalconMail.ACTION_NOTHING)
            {
                buttonDoNoThing.SetActive(true);
            }
            else if (m_FalcomMail.action == FalconMail.ACTION_GO_URL)
            {
                buttonGoUrl.SetActive(true);
            }
            else if (m_FalcomMail.action == FalconMail.ACTION_GO_SHOP)
            {
                buttonGoShop.SetActive(true);
            }
        }
        else if (m_FalcomMail.action == FalconMail.ACTION_CLAIM || m_FalcomMail.action == FalconMail.ACTION_CLAIMED)
        {
            //button
            buttonClaim.SetActive(false);
            buttonGoUrl.SetActive(false);
            buttonGoShop.SetActive(false);
            buttonClaimed.SetActive(false);
            buttonDoNoThing.SetActive(false);
            buttonYesConfirm.SetActive(false);
            buttonNoConfirm.SetActive(false);
            buttonDelete.SetActive(true);
            buttonGoClanInfo.SetActive(false);
            buttonGoClanHome.SetActive(false);
            if (m_FalcomMail.action == FalconMail.ACTION_CLAIM)
            {
                buttonClaim.SetActive(true);
            }
            else if (m_FalcomMail.action == FalconMail.ACTION_CLAIMED)
            {
                buttonClaimed.SetActive(true);
            }
        }
        else if (m_FalcomMail.action == FalconMail.ACTION_CLAN_INFO)
        {
            //button
            buttonClaim.SetActive(false);
            buttonGoUrl.SetActive(false);
            buttonGoShop.SetActive(false);
            buttonClaimed.SetActive(false);
            buttonDoNoThing.SetActive(false);
            buttonYesConfirm.SetActive(false);
            buttonNoConfirm.SetActive(false);
            buttonDelete.SetActive(true);
            buttonGoClanInfo.SetActive(true);
            buttonGoClanHome.SetActive(false);
        }
        else if (m_FalcomMail.action == FalconMail.ACTION_CLAN_HOME)
        {
            //button
            buttonClaim.SetActive(false);
            buttonGoUrl.SetActive(false);
            buttonGoShop.SetActive(false);
            buttonClaimed.SetActive(false);
            buttonDoNoThing.SetActive(false);
            buttonYesConfirm.SetActive(false);
            buttonNoConfirm.SetActive(false);
            buttonDelete.SetActive(true);
            buttonGoClanInfo.SetActive(false);
            buttonGoClanHome.SetActive(true);
        }
    }

    public void ButtonDelete()
    {
        //listPopulator.DeleteFalconMailWithId(m_FalcomMail.id, itemDataIndex);
        //isDeleted = true;

        //StartCoroutine(WaitEndFrame());
        m_FalcomMail.hasConfirm = 1;
        buttonClaim.SetActive(false);
        buttonGoUrl.SetActive(false);
        buttonGoShop.SetActive(false);
        buttonClaimed.SetActive(false);
        buttonDoNoThing.SetActive(false);
        buttonDelete.SetActive(false);
        buttonGoClanInfo.SetActive(false);
        buttonGoClanHome.SetActive(false);

        buttonYesConfirm.SetActive(true);
        buttonNoConfirm.SetActive(true);
    }

    IEnumerator WaitEndFrame()
    {
        yield return null;
        MessageDispatcher.SendMessage(gameObject, EventName.LoadMailBox.LoadedMailBox.ToString(), DataJsonMail.ToJsonObject(CachePvp.FalconMailJson), 0);
    }

    public void ButtonClaim()
    {
        listPopulator.SetClaimedFalconMailWithId(m_FalcomMail.id);
        for (int i = 0; i < sAwardsFade.Length; i++)
        {
            sAwardsFade[i].SetActive(true);
        }
        buttonClaim.SetActive(false);
        buttonClaimed.SetActive(true);
        PopupManager.Instance.ShowClaimPopup(m_FalcomMail.items, FirebaseLogSpaceWar.PopupMail_why);
    }

    public void ButtonClan()
    {
        if (m_FalcomMail != null && m_FalcomMail.clanId != 0)
        {
            new CSClanInfo(m_FalcomMail.clanId).Send();
        }
    }

    public void ButtonClanHome()
    {
        PopupManager.Instance.ShowClanPopup();
    }

    public void ButtonGoUrl()
    {
        if (m_FalcomMail.action == FalconMail.ACTION_GO_URL)
        {
            CachePvp.falconMailId = m_FalcomMail.id;
            Application.OpenURL(m_FalcomMail.url);
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            if (CachePvp.falconMailId.Equals(m_FalcomMail.id))
            {
                CachePvp.falconMailId = "";
                if (m_FalcomMail.items != null && m_FalcomMail.items.Count > 0 && m_FalcomMail.hasClaimed == 0)
                {
                    listPopulator.SetReceivedFalconMailWithId(m_FalcomMail.id);
                    for (int i = 0; i < sAwardsFade.Length; i++)
                    {
                        sAwardsFade[i].SetActive(true);
                    }
                    m_FalcomMail.hasClaimed = 1;
                    PopupManager.Instance.ShowClaimPopup(m_FalcomMail.items, FirebaseLogSpaceWar.PopupMail_why);
                }
            }
        }
    }

    public void ButtonShop()
    {
        SoundManager.PlayShowPopup();
        PopupManagerCuong.Instance.ShowShopPopup();
        if (PopupShop.Instance != null)
        {
            PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.Upgrade);
        }
        //FirebaseLogSpaceWar.LogClickButton("Upgrade");
        CachePvp.falconMailId = "";
        if (m_FalcomMail.items != null && m_FalcomMail.items.Count > 0 && m_FalcomMail.hasClaimed == 0)
        {
            listPopulator.SetReceivedFalconMailWithId(m_FalcomMail.id);
            for (int i = 0; i < sAwardsFade.Length; i++)
            {
                sAwardsFade[i].SetActive(true);
            }
            m_FalcomMail.hasClaimed = 1;
            PopupManager.Instance.ShowClaimPopup(m_FalcomMail.items, FirebaseLogSpaceWar.PopupMail_why);
        }
    }
}