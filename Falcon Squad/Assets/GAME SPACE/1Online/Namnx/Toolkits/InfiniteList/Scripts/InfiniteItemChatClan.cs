﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using TCore;
using System.Globalization;

public class InfiniteItemChatClan : MonoBehaviour
{
    public GameObject gRequest;
    public UISprite sItemRequest;
    public GameObject gButtonDonate;
    public GameObject gButtonDonateDeactive;
    public UISprite sBackgroundRequest;
    public UILabel lTextRequest;
    public UILabel lYouHaveValue;
    public UILabel lBarValue;
    public UISprite sBackgroundBar;
    public TweenAlpha tweenAlphaEffect;
    public TweenAlpha tweenAlphaEffectMedal;
    public UILabel lIncreaseMedal;
    public TweenPosition tweenPositionEffect;
    public TweenPosition tweenPositionEffectMedal;

    public GameObject gChat;
    public BoxCollider boxCollider;
    public UISprite sBackground;
    public UI2DSprite sAvatar;
    public GameObject smallFrame;
    public UILabel lText;
    public UILabel lName;

    public GameObject gSystem;
    public UILabel lTextSystem;
    public UILabel lDateSystem;

    public InfiniteListChatClanPopulator listPopulator;

    public UIPanel panel;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;
    private const string CHAT_ME = "Clanchat_Text_BG2";
    private const string CHAT_OTHER = "Clanchat_Text_BG1";

    private const string REQUEST_ME = "BG_Clan_Item_request_1";
    private const string REQUEST_OTHER = "BG_Clan_Item_request_2";

    private string facebookID = "";

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.Clan.SCDonate.ToString(), OnDonate, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.Clan.SCDonate.ToString(), OnDonate, true);
    }

    private void OnDonate(IMessage rMessage)
    {
        SCDonate d = rMessage.Data as SCDonate;
        if (d.status == SCDonate.SUCCESS)
        {
            if (m_Chat != null)
            {
                if (d.chatId.Equals(m_Chat.id))
                {
                    tweenAlphaEffect.gameObject.SetActive(true);
                    tweenAlphaEffect.transform.localPosition = new Vector3(tweenAlphaEffect.transform.localPosition.x, -2, tweenAlphaEffect.transform.localPosition.z);
                    tweenAlphaEffect.Play(true);
                    tweenAlphaEffect.ResetToBeginning();

                    tweenPositionEffect.Play(true);
                    tweenPositionEffect.ResetToBeginning();

                    lIncreaseMedal.text = "+" + d.incrementMedal;
                    tweenAlphaEffectMedal.gameObject.SetActive(true);
                    tweenAlphaEffectMedal.transform.localPosition = new Vector3(tweenAlphaEffectMedal.transform.localPosition.x, 51.5f, tweenAlphaEffectMedal.transform.localPosition.z);
                    tweenAlphaEffectMedal.Play(true);
                    tweenAlphaEffectMedal.ResetToBeginning();

                    tweenPositionEffectMedal.Play(true);
                    tweenPositionEffectMedal.ResetToBeginning();

                    CachePvp.Medal = d.medal;

                    ClanDonateData donateData = JsonFx.Json.JsonReader.Deserialize<ClanDonateData>(m_Chat.content);
                    switch (donateData.cardType)
                    {
                        case FalconMail.ITEM_CARD_PLANE_1:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD) + "[/b]/ " + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.BataFD) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_PLANE_2:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.SkyWraith) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_PLANE_3:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.FuryOfAres) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_PLANE_4:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.MacBird) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_PLANE_5:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.TwilightX) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_PLANE_6:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.StarBomb) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_PLANE_7:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.IceShard) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.IceShard) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_PLANE_8:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.ThunderBolt) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.ThunderBolt) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_1:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.GatlingGun) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.GatlingGun) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_2:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.AutoGatlingGun) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.AutoGatlingGun) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_3:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Lazer) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.Lazer) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_4:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.DoubleGalting) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.DoubleGalting) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_5:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.HomingMissile) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_WINGMAN_6:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Splasher) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.Splasher) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_WING_1:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingCards(WingTypeEnum.WingOfJustice) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingRank(WingTypeEnum.WingOfJustice) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_WING_2:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingCards(WingTypeEnum.WingOfRedemption) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingRank(WingTypeEnum.WingOfRedemption) + 1)).card_evolve;
                            break;
                        case FalconMail.ITEM_CARD_WING_3:
                            lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingCards(WingTypeEnum.WingOfResolution) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingRank(WingTypeEnum.WingOfResolution) + 1)).card_evolve;
                            break;
                        default:
                            break;
                    }
                    gButtonDonate.SetActive(false);
                    gButtonDonateDeactive.SetActive(true);
                    typeDonateDeactive = 2;
                }
                else
                {
                    if (m_Chat.contentType == Chat.CONTENT_TYPE_DONATE)
                    {
                        int youHave = 0;
                        ClanDonateData donateData = JsonFx.Json.JsonReader.Deserialize<ClanDonateData>(m_Chat.content);
                        switch (donateData.cardType)
                        {
                            case FalconMail.ITEM_CARD_PLANE_1:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD) + "[/b]/ " + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.BataFD) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD);
                                break;
                            case FalconMail.ITEM_CARD_PLANE_2:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.SkyWraith) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith);
                                break;
                            case FalconMail.ITEM_CARD_PLANE_3:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.FuryOfAres) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres);
                                break;
                            case FalconMail.ITEM_CARD_PLANE_4:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.MacBird) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird);
                                break;
                            case FalconMail.ITEM_CARD_PLANE_5:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.TwilightX) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX);
                                break;
                            case FalconMail.ITEM_CARD_PLANE_6:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.StarBomb) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb);
                                break;
                            case FalconMail.ITEM_CARD_PLANE_7:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.IceShard) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.IceShard) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.IceShard);
                                break;
                            case FalconMail.ITEM_CARD_PLANE_8:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.ThunderBolt) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.ThunderBolt) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.ThunderBolt);
                                break;
                            case FalconMail.ITEM_CARD_WINGMAN_1:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.GatlingGun) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.GatlingGun) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.GatlingGun);
                                break;
                            case FalconMail.ITEM_CARD_WINGMAN_2:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.AutoGatlingGun) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.AutoGatlingGun) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.AutoGatlingGun);
                                break;
                            case FalconMail.ITEM_CARD_WINGMAN_3:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Lazer) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.Lazer) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Lazer);
                                break;
                            case FalconMail.ITEM_CARD_WINGMAN_4:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.DoubleGalting) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.DoubleGalting) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.DoubleGalting);
                                break;
                            case FalconMail.ITEM_CARD_WINGMAN_5:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.HomingMissile) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile);
                                break;
                            case FalconMail.ITEM_CARD_WINGMAN_6:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Splasher) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.Splasher) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Splasher);
                                break;
                            case FalconMail.ITEM_CARD_WING_1:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingCards(WingTypeEnum.WingOfJustice) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingRank(WingTypeEnum.WingOfJustice) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetWingCards(WingTypeEnum.WingOfJustice);
                                break;
                            case FalconMail.ITEM_CARD_WING_2:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingCards(WingTypeEnum.WingOfRedemption) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingRank(WingTypeEnum.WingOfRedemption) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetWingCards(WingTypeEnum.WingOfRedemption);
                                break;
                            case FalconMail.ITEM_CARD_WING_3:
                                lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingCards(WingTypeEnum.WingOfResolution) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingRank(WingTypeEnum.WingOfResolution) + 1)).card_evolve;
                                youHave = MinhCacheGame.GetWingCards(WingTypeEnum.WingOfResolution);
                                break;
                            default:
                                break;
                        }
                        if (youHave <= 0 || m_Chat.isDeleted || donateData.donaters.Contains(CachePvp.Code) || donateData.received == donateData.maxCanReceive)
                        {
                            //int typeDonateDeactive; //0: can't donate yourself, 1 : not enough card, 2: have donated, 3: fully
                            if (youHave <= 0)
                            {
                                typeDonateDeactive = 1;
                            }
                            else if (donateData.donaters.Contains(CachePvp.Code))
                            {
                                typeDonateDeactive = 2;
                            }
                            else if (donateData.received == donateData.maxCanReceive)
                            {
                                typeDonateDeactive = 3;
                            }
                            gButtonDonate.SetActive(false);
                            gButtonDonateDeactive.SetActive(true);
                        }
                    }
                }
            }
        }
        else
        {
            switch (d.status)
            {
                case SCDonate.NOT_EXIST:
                case SCDonate.EXPIRED:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_request_no_longer_available, false, 1.5f);
                    break;
                case SCDonate.FULLY:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_request_fully, false, 1.5f);
                    break;
                case SCDonate.NOT_ENOUNGH_QUOTA:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_request_not_enough_quota, false, 1.5f);
                    break;
                case SCDonate.DUPLICATE:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_request_duplicate, false, 1.5f);
                    break;
                default:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(d.message, false, 1.5f);
                    break;
            }
        }
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public void ClickAvatar()
    {
        new CSPlayerInfo(m_Chat.code).Send();
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(sBackground));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    public Chat m_Chat;

    public void SetData(Chat mc, int oldIndex = -1)
    {
        gameObject.SetActive(true);
        transform.localPosition = new Vector3(-10000, transform.localPosition.y, transform.localPosition.z);
        m_Chat = mc;
        lText.text = mc.content;
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(mc.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(mc.appCenterId))
                {
                    showImageFB = false;
                    facebookID = mc.appCenterId;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[mc.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(mc.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        facebookID = mc.appCenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", mc.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (!string.IsNullOrEmpty(mc.facebookId))
            {
                facebookID = mc.facebookId;
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(mc.facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(mc.facebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[mc.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + mc.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, mc.facebookId);
                    }
                }
            }
        }
        if (listPopulator.itemsPoolChatClanChat.ContainsKey(itemDataIndex))
        {
            listPopulator.itemsPoolChatClanChat[itemDataIndex] = mc;
        }
        else
        {
            listPopulator.itemsPoolChatClanChat.Add(itemDataIndex, mc);
        }
        StartCoroutine(Wait1Frame());
    }
    int typeChat = 0; //0 : lien nhau, 1 : lien nhau cach thoi gian, 2 : cach nhau

    int typeDonateDeactive; //0: can't donate yourself, 1 : not enough card, 2: have donated, 3: fully
    IEnumerator Wait1Frame()
    {
        yield return null;

        if (m_Chat.contentType == Chat.CONTENT_TYPE_DONATE)
        {
            gChat.SetActive(false);
            gSystem.SetActive(false);
            gRequest.SetActive(true);
            int youHave = 0;
            ClanDonateData donateData = JsonFx.Json.JsonReader.Deserialize<ClanDonateData>(m_Chat.content);
            sItemRequest.spriteName = FalconMail.GetSpriteName(donateData.cardType, 1);
            switch (donateData.cardType)
            {
                case FalconMail.ITEM_CARD_PLANE_1:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD) + "[/b]/ " + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.BataFD) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD);
                    break;
                case FalconMail.ITEM_CARD_PLANE_2:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.SkyWraith) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith);
                    break;
                case FalconMail.ITEM_CARD_PLANE_3:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.FuryOfAres) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres);
                    break;
                case FalconMail.ITEM_CARD_PLANE_4:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.MacBird) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird);
                    break;
                case FalconMail.ITEM_CARD_PLANE_5:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.TwilightX) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX);
                    break;
                case FalconMail.ITEM_CARD_PLANE_6:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.StarBomb) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb);
                    break;
                case FalconMail.ITEM_CARD_PLANE_7:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.IceShard) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.IceShard) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.IceShard);
                    break;
                case FalconMail.ITEM_CARD_PLANE_8:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.ThunderBolt) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetSpaceShipRank(AircraftTypeEnum.ThunderBolt) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.ThunderBolt);
                    break;
                case FalconMail.ITEM_CARD_WINGMAN_1:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.GatlingGun) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.GatlingGun) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.GatlingGun);
                    break;
                case FalconMail.ITEM_CARD_WINGMAN_2:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.AutoGatlingGun) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.AutoGatlingGun) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.AutoGatlingGun);
                    break;
                case FalconMail.ITEM_CARD_WINGMAN_3:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Lazer) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.Lazer) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Lazer);
                    break;
                case FalconMail.ITEM_CARD_WINGMAN_4:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.DoubleGalting) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.DoubleGalting) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.DoubleGalting);
                    break;
                case FalconMail.ITEM_CARD_WINGMAN_5:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.HomingMissile) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile);
                    break;
                case FalconMail.ITEM_CARD_WINGMAN_6:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Splasher) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingmanRank(WingmanTypeEnum.Splasher) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Splasher);
                    break;
                case FalconMail.ITEM_CARD_WING_1:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingCards(WingTypeEnum.WingOfJustice) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingRank(WingTypeEnum.WingOfJustice) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetWingCards(WingTypeEnum.WingOfJustice);
                    break;
                case FalconMail.ITEM_CARD_WING_2:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingCards(WingTypeEnum.WingOfRedemption) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingRank(WingTypeEnum.WingOfRedemption) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetWingCards(WingTypeEnum.WingOfRedemption);
                    break;
                case FalconMail.ITEM_CARD_WING_3:
                    lYouHaveValue.text = "[b]" + MinhCacheGame.GetWingCards(WingTypeEnum.WingOfResolution) + "[/b]/" + RankSheet.Get(Mathf.Min((int)Constant.MAX_RANK_APPLY, (int)CacheGame.GetWingRank(WingTypeEnum.WingOfResolution) + 1)).card_evolve;
                    youHave = MinhCacheGame.GetWingCards(WingTypeEnum.WingOfResolution);
                    break;
                default:
                    break;
            }

            if (m_Chat.code.Equals(CachePvp.Code))
            {
                typeDonateDeactive = 0;
                gButtonDonate.SetActive(false);
                gButtonDonateDeactive.SetActive(true);
                sBackgroundRequest.spriteName = REQUEST_ME;
                lTextRequest.text = I2.Loc.ScriptLocalization.you_request;
            }
            else
            {
                if (youHave <= 0 || m_Chat.isDeleted || donateData.donaters.Contains(CachePvp.Code) || donateData.received == donateData.maxCanReceive)
                {
                    //int typeDonateDeactive; //0: can't donate yourself, 1 : not enough card, 2: have donated, 3: fully
                    if (youHave <= 0)
                    {
                        typeDonateDeactive = 1;
                    }
                    else if (donateData.donaters.Contains(CachePvp.Code))
                    {
                        typeDonateDeactive = 2;
                    }
                    else if (donateData.received == donateData.maxCanReceive)
                    {
                        typeDonateDeactive = 3;
                    }
                    gButtonDonate.SetActive(false);
                    gButtonDonateDeactive.SetActive(true);
                }
                else
                {
                    gButtonDonate.SetActive(true);
                    gButtonDonateDeactive.SetActive(false);
                }
                sBackgroundRequest.spriteName = REQUEST_OTHER;
                lTextRequest.text = I2.Loc.ScriptLocalization.name_request.Replace("%{name}", "[b][FFB900FF]" + (string.IsNullOrEmpty(m_Chat.name) ? m_Chat.code : m_Chat.name) + "[-][/b]");
            }
            lBarValue.text = donateData.received + "/" + donateData.maxCanReceive;
            sBackgroundBar.width = 297 * donateData.received / donateData.maxCanReceive;
            sBackground.height = 148;
            boxCollider.size = new Vector3(boxCollider.size.x, 180, boxCollider.size.z);
            boxCollider.center = new Vector3(0, -50, boxCollider.center.z);

            if (m_Chat.code.Equals(CachePvp.Code))
            {
                MessageDispatcher.SendMessage(gameObject, EventName.Clan.ShowButtonRequestChat.ToString(), (int)donateData.remainTime, 0);
            }

            listPopulator.itemsPoolChatClanHeight[itemDataIndex] = boxCollider.size.y - 20;
        }
        else if (m_Chat.contentType == Chat.CONTENT_TYPE_CHAT)
        {
            gChat.SetActive(true);
            gRequest.SetActive(false);
            gSystem.SetActive(false);

            int h = lText.height;
            //StartCoroutine(Wait1Frame());
            if (m_Chat.code.Equals(CachePvp.Code))
            {
                if (itemDataIndex > 0)
                {
                    Chat sc = listPopulator.itemsPoolChatClanChat[itemDataIndex - 1];
                    if (sc.code != null)
                    {
                        if (sc.code.Equals(m_Chat.code))
                        {
                            lName.height = 2;
                            lName.text = "";
                            if (sc.time > m_Chat.time - 45000 && sc.contentType == Chat.CONTENT_TYPE_CHAT)
                            {
                                lName.height = 2;
                                typeChat = 0;
                            }
                            else
                            {
                                lName.height = 20;
                                DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                double dou = m_Chat.time;
                                DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                                lName.text = m_dateTime.ToString("dd-MM-yyyy hh:mm");
                                typeChat = 1;
                            }
                        }
                        else
                        {
                            lName.height = 20;
                            DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                            double dou = m_Chat.time;
                            DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                            lName.text = m_dateTime.ToString("dd-MM-yyyy hh:mm");
                            typeChat = 2;
                        }
                    }
                    else
                    {
                        lName.height = 20;
                        DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        double dou = m_Chat.time;
                        DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                        lName.text = m_dateTime.ToString("dd-MM-yyyy hh:mm");
                        typeChat = 1;
                    }
                }
                else
                {
                    lName.height = 20;
                    DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    double dou = m_Chat.time;
                    DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                    lName.text = m_dateTime.ToString("dd-MM-yyyy hh:mm");
                }
                lName.alignment = NGUIText.Alignment.Right;
                lName.pivot = UIWidget.Pivot.TopRight;
                lName.transform.localPosition = new Vector3(317, lName.transform.localPosition.y, lName.transform.localPosition.z);
                sBackground.spriteName = CHAT_ME;
                smallFrame.SetActive(false);
                lText.pivot = UIWidget.Pivot.TopRight;
                lText.transform.localPosition = new Vector3(317, lText.transform.localPosition.y, lText.transform.localPosition.z);
                sBackground.pivot = UIWidget.Pivot.TopRight;
                sBackground.transform.localPosition = new Vector3(10, sBackground.transform.localPosition.y, sBackground.transform.localPosition.z);
                sBackground.height = h + 20;
                sBackground.width = lText.width + 20;
                if (lName.height == 20)
                {
                    boxCollider.size = new Vector3(boxCollider.size.x, h + 44 + 20, boxCollider.size.z);
                    boxCollider.center = new Vector3(boxCollider.center.x, (140 - boxCollider.size.y) / 2f - 20, boxCollider.center.z);
                }
                else
                {
                    boxCollider.size = new Vector3(boxCollider.size.x, sBackground.height - 4 + listPopulator.padding + 20, boxCollider.size.z);
                    boxCollider.center = new Vector3(boxCollider.center.x, (140 - boxCollider.size.y) / 2f - 20, boxCollider.center.z);
                }
                listPopulator.itemsPoolChatClanHeight[itemDataIndex] = boxCollider.size.y - 20;
            }
            else
            {
                lName.text = string.IsNullOrEmpty(m_Chat.name) ? m_Chat.code : m_Chat.name;
                DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                double dou = m_Chat.time;
                DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                lName.text += ", " + m_dateTime.ToString("dd-MM-yyyy hh:mm");
                if (itemDataIndex > 0)
                {
                    Chat sc = listPopulator.itemsPoolChatClanChat[itemDataIndex - 1];
                    if (sc.code != null)
                    {
                        if (sc.code.Equals(m_Chat.code))
                        {
                            if (sc.time > m_Chat.time - 45000 && sc.contentType == Chat.CONTENT_TYPE_CHAT)
                            {
                                typeChat = 0;
                                lName.height = 2;
                                smallFrame.SetActive(false);
                            }
                            else
                            {
                                typeChat = 1;
                                lName.height = 20;
                                smallFrame.SetActive(true);
                            }
                        }
                        else
                        {
                            if (sc.code.Equals(CachePvp.Code))
                            {
                                typeChat = 2;
                                lName.height = 20;
                                smallFrame.SetActive(true);
                            }
                            else
                            {
                                typeChat = 1;
                                lName.height = 20;
                                smallFrame.SetActive(true);
                            }
                        }
                    }
                    else
                    {
                        typeChat = 1;
                        lName.height = 20;
                        smallFrame.SetActive(true);
                    }
                }
                else
                {
                    lName.height = 20;
                    smallFrame.SetActive(true);
                }
                lName.alignment = NGUIText.Alignment.Left;
                lName.pivot = UIWidget.Pivot.TopLeft;
                lName.transform.localPosition = new Vector3(-211, lName.transform.localPosition.y, lName.transform.localPosition.z);
                sBackground.spriteName = CHAT_OTHER;
                lText.pivot = UIWidget.Pivot.TopLeft;
                lText.transform.localPosition = new Vector3(-201, lText.transform.localPosition.y, lText.transform.localPosition.z);
                sBackground.pivot = UIWidget.Pivot.TopLeft;
                sBackground.transform.localPosition = new Vector3(-10, sBackground.transform.localPosition.y, sBackground.transform.localPosition.z);
                sBackground.height = h + 20;
                sBackground.width = lText.width + 20;
                if (lName.height == 20)
                {
                    boxCollider.size = new Vector3(boxCollider.size.x, h + 44 + 20, boxCollider.size.z);
                    boxCollider.center = new Vector3(boxCollider.center.x, (140 - boxCollider.size.y) / 2f - 20, boxCollider.center.z);
                }
                else
                {
                    boxCollider.size = new Vector3(boxCollider.size.x, sBackground.height + listPopulator.padding - 4 + 20, boxCollider.size.z);
                    boxCollider.center = new Vector3(boxCollider.center.x, (140 - boxCollider.size.y) / 2f - 20, boxCollider.center.z);
                }
                listPopulator.itemsPoolChatClanHeight[itemDataIndex] = boxCollider.size.y - 20;
            }
        }
        else if (m_Chat.contentType == Chat.CONTENT_TYPE_SYSTEM)
        {
            gChat.SetActive(false);
            gRequest.SetActive(false);
            gSystem.SetActive(true);

            int h = 20;
            DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
            double dou = m_Chat.time;
            DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
            lDateSystem.text = m_dateTime.ToString("dd-MM-yyyy hh:mm");
            lTextSystem.text = m_Chat.content;

            typeChat = 2;
            lDateSystem.height = 20;

            boxCollider.size = new Vector3(boxCollider.size.x, h + 44 + 20, boxCollider.size.z);
            boxCollider.center = new Vector3(boxCollider.center.x, (140 - boxCollider.size.y) / 2f - 20, boxCollider.center.z);

            listPopulator.itemsPoolChatClanHeight[itemDataIndex] = boxCollider.size.y - 20;
        }

        //yield return null;
        Reposition();
    }

    public void Reposition()
    {
        if (listPopulator.itemsPoolChatClanPositionY.ContainsKey(itemDataIndex - 1))
        {
            float posY = listPopulator.itemsPoolChatClanPositionY[itemDataIndex - 1] - listPopulator.itemsPoolChatClanHeight[itemDataIndex - 1];
            if (lName.height == 20)
            {
                if (m_Chat.code.Equals(CachePvp.Code))
                {
                    posY -= 10;
                }
                else
                {
                    if (typeChat == 1)
                    {
                        posY -= 20;
                    }
                    else
                    {
                        posY -= 10;
                    }
                }
            }
            transform.localPosition = new Vector3(0, posY, 0);
            listPopulator.itemsPoolChatClanPositionY[itemDataIndex] = posY;
        }
        else if (itemDataIndex == 0)
        {
            transform.localPosition = new Vector3(0, -35, 0);
            listPopulator.itemsPoolChatClanPositionY[itemDataIndex] = -35;
        }
        transform.localPosition = new Vector3(0, transform.localPosition.y, transform.localPosition.z);
        StartCoroutine(CheckTop());
    }

    IEnumerator CheckTop()
    {
        yield return null;
        yield return new WaitForSeconds(0.1f);
        if (m_Chat.contentType == Chat.CONTENT_TYPE_DONATE)
        {
            bool currentVisibilty = panel.IsVisible(sBackground);
            if (currentVisibilty && oldPositionY != transform.position.y)
            {
                CachePvp.topChat--;
                oldPositionY = transform.position.y;
            }
            else
            {
                oldPositionY = transform.position.y;
            }
            isVisible = currentVisibilty;
        }
    }

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(sBackground);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                //Debug.Log("out " + itemDataIndex);
                if (m_Chat.contentType == Chat.CONTENT_TYPE_DONATE)
                {
                    if (oldPositionY > transform.position.y)
                    {
                        //di xuong
                        CachePvp.bottomChat++;
                    }
                    else
                    {
                        //di len
                        CachePvp.topChat++;
                    }
                    oldPositionY = transform.position.y;
                }
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
            else
            {
                if (m_Chat.contentType == Chat.CONTENT_TYPE_DONATE)
                {
                    if (oldPositionY > transform.position.y)
                    {
                        //di xuong
                        if (CachePvp.topChat > 0)
                        {
                            CachePvp.topChat--;
                        }
                    }
                    else
                    {
                        //di len
                        if (CachePvp.bottomChat > 0)
                        {
                            CachePvp.bottomChat--;
                        }
                    }
                    oldPositionY = transform.position.y;
                }
                //Debug.Log("in " + itemDataIndex);
            }
        }
    }

    float oldPositionY;

    public void RepositionAfterAdd()
    {
        bool currentVisibilty = panel.IsVisible(sBackground);
        isVisible = currentVisibilty;
        if (!isVisible)
        {
            StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
        }
    }

    public void ClickDonate()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server, false, 1.5f);
            return;
        }
        MessageDispatcher.SendMessage(EventName.Clan.CSDonate.ToString());
        new CSDonate(m_Chat.id).Send();
    }

    public void ClickDonateDeactive()
    {
        //int typeDonateDeactive; //0: can't donate yourself, 1 : not enough card, 2: have donated, 3: fully
        switch (typeDonateDeactive)
        {
            case 0:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_cant_donate_yourself, false, 1.5f);
                break;
            case 1:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_card, false, 1.5f);
                break;
            case 2:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_request_duplicate, false, 1.5f);
                break;
            case 3:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_request_fully, false, 1.5f);
                break;
            default:
                break;
        }
    }

    public void TweenDone()
    {
        tweenAlphaEffect.gameObject.SetActive(false);
    }
}