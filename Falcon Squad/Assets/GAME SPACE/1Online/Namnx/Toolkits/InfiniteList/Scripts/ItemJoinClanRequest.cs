﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;

public class ItemJoinClanRequest : MonoBehaviour
{
    public UISprite sVipIcon;
    public UISprite sSmallFrame;
    public UI2DSprite sAvatar;
    public UILabel lName;
    public UISprite sFlag;
    public string facebookID;
    public UILabel lId;
    public UISprite sRank;
    public UILabel lRank;

    public GameObject buttonAccept;
    public GameObject buttonDeny;
    public UILabel lStatus;
    public GameObject fxLoading;

    public UISprite backgroundSprite;
    public UIPanel panel;
    public InfiniteListPopulatorJoinClanRequest listPopulator;
    public int itemNumber;
    public int itemDataIndex;
    public Color spriteColor = new Color();
    public Color spritePressedColor = new Color();

    private bool isVisible = true;

    Player m_player;

    void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.Clan.ConfirmJoinRequest.ToString(), OnConfirmJoinRequest, true);
        MessageDispatcher.AddListener(EventName.Clan.ConfirmJoinRequestError.ToString(), OnConfirmJoinRequestError, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ConfirmJoinRequest.ToString(), OnConfirmJoinRequest, true);
        MessageDispatcher.RemoveListener(EventName.Clan.ConfirmJoinRequestError.ToString(), OnConfirmJoinRequestError, true);
    }

    private void OnConfirmJoinRequestError(IMessage rMessage)
    {
        int action = (int)rMessage.Recipient;
        string code = rMessage.Data.ToString();
        if (code.Equals(m_player.code))
        {
            buttonAccept.SetActive(true);
            buttonDeny.SetActive(true);
            fxLoading.SetActive(false);
            lStatus.text = "";
        }
    }

    private void OnConfirmJoinRequest(IMessage rMessage)
    {
        int action = (int)rMessage.Recipient;
        string code = rMessage.Data.ToString();
        if (code.Equals(m_player.code))
        {
            buttonAccept.SetActive(false);
            buttonDeny.SetActive(false);
            fxLoading.SetActive(false);
            if (action == SCClanConfirmJoinRequest.ACCEPT)
            {
                lStatus.text = I2.Loc.ScriptLocalization.accepted;
                listPopulator.dictionaryType[m_player.code] = 1;
            }
            else
            {
                lStatus.text = I2.Loc.ScriptLocalization.denied;
                listPopulator.dictionaryType[m_player.code] = 2;
            }
            PopupClan.Instance.ConfirmJoinRequest(m_player.code);
        }
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public void PressButtonAccept()
    {
        Debug.Log("accept : " + m_player.code);
        buttonAccept.SetActive(false);
        buttonDeny.SetActive(false);
        fxLoading.SetActive(true);
        lStatus.text = "";
        new CSClanConfirmJoinRequest(CSClanConfirmJoinRequest.ACCEPT, m_player.code).Send();
    }

    public void PressButtonDeny()
    {
        Debug.Log("deny : " + m_player.code);
        buttonAccept.SetActive(false);
        buttonDeny.SetActive(false);
        fxLoading.SetActive(true);
        lStatus.text = "";
        new CSClanConfirmJoinRequest(CSClanConfirmJoinRequest.REJECT, m_player.code).Send();
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(backgroundSprite));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    void OnClick()
    {
        listPopulator.itemClicked(itemDataIndex);
    }

    //	void OnDrag (Vector2 delta) {
    //		backgroundSprite.color = spriteColor;
    //	}

    void OnPress(bool isDown)
    {
        listPopulator.itemIsPressed(itemDataIndex, isDown);
        // change sprite color
        //		backgroundSprite.color = isDown ? spritePressedColor : spriteColor;				
    }

    public void SetData(Player player)
    {
        m_player = player;
        LeanTween.cancel(gameObject);
        tempVector3.x = 0;
        tempVector3.y = transform.localPosition.y;
        tempVector3.z = transform.localPosition.z;
        transform.localPosition = tempVector3;
        tempColor.a = 1;
        sSmallFrame.color = tempColor;
        sAvatar.color = tempColor;
        lName.color = tempColor;
        sFlag.color = tempColor;
        backgroundSprite.color = tempColor;
        if (listPopulator.dictionaryType[player.code] == 0)
        {
            fxLoading.SetActive(false);
            lStatus.text = "";
            buttonAccept.SetActive(true);
            buttonDeny.SetActive(true);
        }
        else if (listPopulator.dictionaryType[player.code] == 1)
        {
            fxLoading.SetActive(false);
            lStatus.text = I2.Loc.ScriptLocalization.accepted;
            buttonAccept.SetActive(false);
            buttonDeny.SetActive(false);
        }
        else if (listPopulator.dictionaryType[player.code] == 2)
        {
            fxLoading.SetActive(false);
            lStatus.text = I2.Loc.ScriptLocalization.denied;
            buttonAccept.SetActive(false);
            buttonDeny.SetActive(false);
        }
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        if (string.IsNullOrEmpty(player.name) || player.name.Trim().Equals(""))
        {
            lName.text = I2.Loc.ScriptLocalization.id_key + ": " + player.code;
        }
        else
        {
            lName.text = player.name.ToUpper();
        }
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(player.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                {
                    showImageFB = false;
                    facebookID = player.appCenterId;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        facebookID = player.appCenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (!string.IsNullOrEmpty(player.facebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                }
            }
            facebookID = player.facebookId;
        }
        lId.text = I2.Loc.ScriptLocalization.id_key + ": " + m_player.code;
        int rank = CachePvp.dictionaryRank[m_player.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lRank.text = rankTitle + " " + CachePvp.ConvertChina(m_player.playerLevel.name);
        sRank.spriteName = "PVP_rank_" + rank;
        int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);
        sFlag.spriteName = player.country.ToLower();
        int vip = CachePvp.GetVipFromVipPoint(player.vip);
        Debug.Log("player.vip : " + vip);
        if (vip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (vip - 1);
        }
    }

    Color tempColor = new Color(1, 1, 1, 1);
    Color hideColor = new Color(1, 1, 1, 0);
    Vector3 tempVector3 = Vector3.zero;
    Vector3 tempVector3Table = Vector3.zero;

    public void TweenAppear(int i, Vector3 vt3, UITable table)
    {
        tempVector3.x = -150;
        tempVector3.y = vt3.y;
        tempVector3.z = vt3.z;
        transform.localPosition = tempVector3;
        sSmallFrame.color = hideColor;
        sAvatar.color = hideColor;
        lName.color = hideColor;
        sFlag.color = hideColor;
        backgroundSprite.color = hideColor;
        LeanTween.value(gameObject, -150, 0, 0.3f).setDelay(i / 10f).setEase(LeanTweenType.easeOutBack).setOnUpdate((float obj) =>
        {
            tempVector3.x = obj;
            tempVector3.y = vt3.y;
            tempVector3.z = vt3.z;
            transform.localPosition = tempVector3;

            tempVector3Table.x = 0;
            tempVector3Table.y = table.transform.localPosition.y;
            tempVector3Table.z = table.transform.localPosition.z;

            table.transform.localPosition = tempVector3Table;
        });
        LeanTween.value(gameObject, 0, 1, 0.5f).setDelay(i / 10f).setEase(LeanTweenType.easeOutBack).setOnUpdate((float obj) =>
        {
            tempColor.a = obj;
            sSmallFrame.color = tempColor;
            sAvatar.color = tempColor;
            lName.color = tempColor;
            sFlag.color = tempColor;
            backgroundSprite.color = tempColor;
        });
    }

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(backgroundSprite);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }
}