﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;

public class ItemReceiveRequest : MonoBehaviour
{
    public UISprite sVipIcon;
    public UISprite sSmallFrame;
    public UI2DSprite sAvatar;
    public UILabel lName;
    public UILabel lNameClan;
    public UISprite sFlag;
    public string facebookID;
    public UILabel lId;
    public UISprite sRank;
    public UILabel lRank;

    public GameObject buttonAccept;
    public GameObject buttonDeny;
    public UILabel lStatus;
    public GameObject fxLoading;

    public UISprite backgroundSprite;
    public UIPanel panel;
    public InfiniteListPopulatorReceiveRequest listPopulator;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;

    Player m_player;

    void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.RequestFriend.ToString(), OnRequestFriend, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RequestFriend.ToString(), OnRequestFriend, true);
    }

    private void OnRequestFriend(IMessage rMessage)
    {
        SCRequestFriend rf = rMessage.Data as SCRequestFriend;
        if (rf.status == SCRequestFriend.SUCCESS)
        {
            if (rf.code.Equals(m_player.code))
            {
                buttonAccept.SetActive(false);
                buttonDeny.SetActive(false);
                fxLoading.SetActive(false);
                if (rf.type == CSRequestFriend.ACCEPT)
                {
                    lStatus.text = I2.Loc.ScriptLocalization.accepted;
                    listPopulator.dictionaryType[m_player.code] = 1;
                }
                else if (rf.type == CSRequestFriend.REJECT)
                {
                    lStatus.text = I2.Loc.ScriptLocalization.denied;
                    listPopulator.dictionaryType[m_player.code] = 2;
                }
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(rf.message, false, 1.5f);
            buttonAccept.SetActive(true);
            buttonDeny.SetActive(true);
            fxLoading.SetActive(false);
            lStatus.text = "";
        }
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public void PressButtonAccept()
    {
        Debug.Log("accept : " + m_player.code);
        buttonAccept.SetActive(false);
        buttonDeny.SetActive(false);
        fxLoading.SetActive(true);
        lStatus.text = "";
        new CSRequestFriend(CSRequestFriend.ACCEPT, m_player.code).Send();
    }

    public void PressButtonDeny()
    {
        Debug.Log("deny : " + m_player.code);
        buttonAccept.SetActive(false);
        buttonDeny.SetActive(false);
        fxLoading.SetActive(true);
        lStatus.text = "";
        new CSRequestFriend(CSRequestFriend.REJECT, m_player.code).Send();
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(backgroundSprite));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    void OnClick()
    {
        listPopulator.itemClicked(itemDataIndex);
    }

    void OnPress(bool isDown)
    {
        listPopulator.itemIsPressed(itemDataIndex, isDown);
    }

    public void SetData(Player player)
    {
        m_player = player;
        fxLoading.SetActive(false);
        lStatus.text = "";
        Debug.Log("SetData");
        if (listPopulator.dictionaryType[player.code] == 0)
        {
            buttonAccept.SetActive(true);
            buttonDeny.SetActive(true);
        }
        else if (listPopulator.dictionaryType[player.code] == 1)
        {
            lStatus.text = I2.Loc.ScriptLocalization.accepted;
            buttonAccept.SetActive(false);
            buttonDeny.SetActive(false);
        }
        else if (listPopulator.dictionaryType[player.code] == 2)
        {
            lStatus.text = I2.Loc.ScriptLocalization.denied;
            buttonAccept.SetActive(false);
            buttonDeny.SetActive(false);
        }
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        if (string.IsNullOrEmpty(player.name) || player.name.Trim().Equals(""))
        {
            lName.text = "ID: " + player.code;
        }
        else
        {
            lName.text = player.name.ToUpper();
        }
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(player.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                {
                    showImageFB = false;
                    facebookID = player.appCenterId;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        facebookID = player.appCenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            facebookID = player.facebookId;
            if (!string.IsNullOrEmpty(player.facebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                }
            }
        }
        tempVector3.x = lNameClan.transform.localPosition.x;
        tempVector3.z = lNameClan.transform.localPosition.z;
        if (player.playerClan != null)
        {
            lNameClan.text = player.playerClan.name;
            tempVector3.y = 15;
        }
        else
        {
            lNameClan.text = "";
            tempVector3.y = 0;
        }
        lNameClan.transform.localPosition = tempVector3;
        lId.text = "ID: " + m_player.code;
        int rank = CachePvp.dictionaryRank[m_player.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lRank.text = rankTitle + " " + m_player.playerLevel.name;
        sRank.spriteName = "PVP_rank_" + rank;
        int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);
        sFlag.spriteName = player.country.ToLower();
        int vip = CachePvp.GetVipFromVipPoint(player.vip);
        Debug.Log("player.vip : " + vip);
        if (vip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (vip - 1);
        }
    }

    Vector3 tempVector3 = Vector3.zero;

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(backgroundSprite);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }
}