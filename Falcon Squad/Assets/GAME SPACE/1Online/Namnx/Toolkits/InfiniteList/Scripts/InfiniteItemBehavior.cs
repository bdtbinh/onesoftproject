﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;

public class InfiniteItemBehavior : MonoBehaviour
{
    public UILabel lNumber;
    public UISprite sBigFrame;
    public UISprite sSmallFrame;
    public UI2DSprite sAvatar;

    public UISprite sStatusOffline;

    public UISprite sVipIcon;
    public UISprite sVipIconPVP;
    public UISprite sVipIcon2v2;

    public string facebookID;

    public UISprite backgroundSprite;
    public UIPanel panel;
    public InfiniteListPopulator listPopulator;
    public int itemNumber;
    public int itemDataIndex;
    public Color spriteColor = new Color();
    public Color spritePressedColor = new Color();

    private bool isVisible = true;
    private BoxCollider thisCollider;

    public GameObject item;
    public UILabel lName;
    public UILabel lNameClan;
    public UILabel lLevel;
    public UILabel lStars;
    public UISprite sFlag;

    public GameObject itemPVP;
    public UISprite sFlagPVP;
    public UILabel lNamePVP;
    public UILabel lNameClanPVP;
    public UILabel lElo;
    public UILabel lWinrate;
    public UILabel lMedalRank;
    public UISprite sMedalRank;
    public UISprite[] sMedals;

    public GameObject item2v2;
    public UISprite sFlag2v2;
    public UILabel lName2v2;
    public UILabel lNameClan2v2;
    public UILabel lSeasonScore;
    public UILabel lWinrate2v2;
    public UISprite sMedalRank2v2;
    public UILabel lMedalRank2v2;

    private const string PVP_TOP1_SPRITE_NAME = "PVP_top1_frame";
    private const string PVP_TOP2_SPRITE_NAME = "PVP_top2_frame";
    private const string PVP_TOP3_SPRITE_NAME = "PVP_top3_frame";

    private const string PVP_YOU_SPRITE_NAME = "player_BG_onleaderboad_you";
    private const string PVP_OTHER_SPRITE_NAME = "player_BG_onleaderboad";

    private const string MEDAL_ENABLE_SPRITE = "PVP_rank_medal_1";
    private const string MEDAL_DISABLE_SPRITE = "PVP_rank_medal_0";

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        thisCollider = GetComponent<BoxCollider>();
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(sBigFrame));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    void OnClick()
    {
        listPopulator.itemClicked(itemDataIndex);
    }

    //	void OnDrag (Vector2 delta) {
    //		backgroundSprite.color = spriteColor;
    //	}

    void OnPress(bool isDown)
    {
        listPopulator.itemIsPressed(itemDataIndex, isDown);
        // change sprite color
        //		backgroundSprite.color = isDown ? spritePressedColor : spriteColor;				
    }

    public void SetData(Player player)
    {
        if (PopupManager.Instance.typeRank == 1 || CachePvp.LoadMegaRank)
        {
            itemPVP.SetActive(true);
            item.SetActive(false);
            item2v2.SetActive(false);
        }
        else if (PopupManager.Instance.typeRank == 0)
        {
            itemPVP.SetActive(false);
            item.SetActive(true);
            item2v2.SetActive(false);
        }
        else if (PopupManager.Instance.typeRank == 2)
        {
            itemPVP.SetActive(false);
            item.SetActive(false);
            item2v2.SetActive(true);
        }
        int vip = CachePvp.GetVipFromVipPoint(player.vip);
        if (vip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
            sVipIconPVP.gameObject.SetActive(false);
            sVipIcon2v2.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIconPVP.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (vip - 1);
            sVipIconPVP.spriteName = sVipIcon.spriteName;
            sVipIcon2v2.spriteName = sVipIcon.spriteName;
        }

        LeanTween.cancel(gameObject);
        tempVector3.x = 0;
        tempVector3.y = transform.localPosition.y;
        tempVector3.z = transform.localPosition.z;
        transform.localPosition = tempVector3;
        tempColor.a = 1;

        lNumber.color = tempColor;
        sBigFrame.color = tempColor;
        sSmallFrame.color = tempColor;
        sAvatar.color = tempColor;
        sVipIcon.color = tempColor;
        sVipIcon2v2.color = tempColor;
        sVipIconPVP.color = tempColor;

        //item
        lName.color = tempColor;
        lNameClan.color = nameClanColor;
        lLevel.color = tempColor;
        lStars.color = tempColor;
        sFlag.color = tempColor;
        //itemPVP
        sFlagPVP.color = tempColor;
        lNamePVP.color = tempColor;
        lNameClanPVP.color = nameClanColor;
        lElo.color = tempColor;
        lWinrate.color = tempColor;
        lMedalRank.color = tempColor;
        sMedalRank.color = tempColor;
        sStatusOffline.color = tempColor;
        //item2v2
        sFlag2v2.color = tempColor;
        lName2v2.color = tempColor;
        lNameClan2v2.color = nameClanColor;
        lSeasonScore.color = tempColor;
        lWinrate2v2.color = tempColor;
        lMedalRank2v2.color = tempColor;
        sMedalRank2v2.color = tempColor;

        for (int i = 0; i < sMedals.Length; i++)
        {
            sMedals[i].color = tempColor;
        }
        string currentRank;
        int number = 3;
        int currentStar;
        int rank;
        MedalRankTitle rankTitle;
        if (CachePvp.LoadMegaRank)
        {
            currentRank = player.megaPlayerLevel.code;
            currentStar = player.megaPlayerLevel.medal;
            lElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(player.megaPlayerLevel.elo);

            rank = CachePvp.dictionaryRank[player.megaPlayerLevel.code];
            rankTitle = (MedalRankTitle)(rank);
            lMedalRank.text = rankTitle + " " + CachePvp.ConvertChina(player.megaPlayerLevel.name);
            lWinrate.text = "";
        }
        else
        {
            currentRank = player.playerLevel.code;
            currentStar = player.playerLevel.medal;
            lElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(player.playerLevel.elo);

            rank = CachePvp.dictionaryRank[player.playerLevel.code];
            rankTitle = (MedalRankTitle)(rank);
            lMedalRank.text = rankTitle + " " + CachePvp.ConvertChina(player.playerLevel.name);
            lWinrate.text = I2.Loc.ScriptLocalization.win + ": " + player.GetWinrateText();
        }
        if (player.pvP2vs2SeasonData != null)
        {
            lSeasonScore.text = I2.Loc.ScriptLocalization.season_score + ": " + GameContext.FormatNumber(player.pvP2vs2SeasonData.score);
            lWinrate2v2.text = I2.Loc.ScriptLocalization.win + ": " + player.GetWinrateText2v2();
            lMedalRank2v2.text = CachePvp.MedalRank2v2Title(player.pvP2vs2SeasonData.chestName);
            sMedalRank2v2.spriteName = CachePvp.GetSpriteNameChest(player.pvP2vs2SeasonData.chestName);
            if (string.IsNullOrEmpty(lMedalRank2v2.text))
            {
                sMedalRank2v2.color = hideColor;
            }
            else
            {
                tempColor.a = 1;
                sMedalRank2v2.color = tempColor;
            }
        }
        if (currentRank == "BRONZE")
        {
            number = 3;
        }
        else if (currentRank == "SILVER")
        {
            number = 4;
        }
        else
        {
            number = 5;
        }

        for (int i = 0; i < sMedals.Length; i++)
        {
            if (i < number)
            {
                sMedals[i].gameObject.SetActive(true);
                if (i < currentStar)
                {
                    sMedals[i].spriteName = MEDAL_ENABLE_SPRITE;
                }
                else
                {
                    sMedals[i].spriteName = MEDAL_DISABLE_SPRITE;
                }
            }
            else
            {
                sMedals[i].gameObject.SetActive(false);
            }
        }

        sStatusOffline.spriteName = player.offline ? "status_offline" : "status_online";

        sMedalRank.spriteName = "PVP_rank_" + rank;
        if (CachePvp.LoadMegaRank)
        {
            sMedalRank.enabled = false;
            lMedalRank.enabled = false;
        }
        else
        {
            sMedalRank.enabled = true;
            lMedalRank.enabled = true;
        }
        backgroundSprite.color = tempColor;

        if (player.ranking == 1)
        {
            lNumber.gameObject.SetActive(false);
            sBigFrame.spriteName = PVP_TOP1_SPRITE_NAME;
            sBigFrame.gameObject.SetActive(true);
            sSmallFrame.gameObject.SetActive(false);
        }
        else if (player.ranking == 2)
        {
            lNumber.gameObject.SetActive(false);
            sBigFrame.spriteName = PVP_TOP2_SPRITE_NAME;
            sBigFrame.gameObject.SetActive(true);
            sSmallFrame.gameObject.SetActive(false);
        }
        else if (player.ranking == 3)
        {
            lNumber.gameObject.SetActive(false);
            sBigFrame.spriteName = PVP_TOP3_SPRITE_NAME;
            sBigFrame.gameObject.SetActive(true);
            sSmallFrame.gameObject.SetActive(false);
        }
        else
        {
            sBigFrame.gameObject.SetActive(false);
            lNumber.gameObject.SetActive(true);
            lNumber.text = player.ranking.ToString();
            sSmallFrame.gameObject.SetActive(true);
        }
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        if (player.code.Equals(CachePvp.Code))
        {
            //me
            backgroundSprite.spriteName = PVP_YOU_SPRITE_NAME;
            if (string.IsNullOrEmpty(CachePvp.Name) || CachePvp.Name.Trim().Equals(""))
            {
                lName.text = I2.Loc.ScriptLocalization.id_key + ": " + player.code;
            }
            else
            {
                lName.text = CachePvp.Name.ToUpper();
            }
            lNamePVP.text = lName.text;
            lName2v2.text = lName.text;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(CachePvp.GamecenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.GamecenterId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.GamecenterId];
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.GamecenterId);
                        if (t != null)
                        {
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.GamecenterId, t);
                        }
                        else
                        {
                            sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                        }
                        facebookID = CachePvp.GamecenterId;
                    }
                }
                else
                {
                    facebookID = "";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(CachePvp.FacebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + CachePvp.FacebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.FacebookId);
                    }
                    facebookID = CachePvp.FacebookId;
                }
            }
        }
        else
        {
            backgroundSprite.spriteName = PVP_OTHER_SPRITE_NAME;
            if (string.IsNullOrEmpty(player.name) || player.name.Trim().Equals(""))
            {
                lName.text = I2.Loc.ScriptLocalization.id_key + ": " + player.code;
            }
            else
            {
                lName.text = player.name.ToUpper();
            }
            lNamePVP.text = lName.text;
            lName2v2.text = lName.text;
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(player.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                    {
                        showImageFB = false;
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                        facebookID = player.appCenterId;
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                        if (t != null)
                        {
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                            showImageFB = false;
                            facebookID = player.appCenterId;
                        }
                        else
                        {
                            sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                        }
                    }
                }
            }
            if (showImageFB)
            {
                if (!string.IsNullOrEmpty(player.facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                    }
                    facebookID = player.facebookId;
                }
            }
        }

        tempVector3.x = lNameClan.transform.localPosition.x;
        tempVector3.z = lNameClan.transform.localPosition.z;
        if (player.playerClan != null)
        {
            lNameClan.text = player.playerClan.name;
            tempVector3.y = 28;
        }
        else
        {
            lNameClan.text = "";
            tempVector3.y = 16;
        }
        lNameClanPVP.text = lNameClan.text;
        lNameClan2v2.text = lNameClan.text;
        lNameClan.transform.localPosition = tempVector3;
        lNameClanPVP.transform.localPosition = tempVector3;
        lNameClan2v2.transform.localPosition = tempVector3;

        int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);
        int[] starExtra = PvpUtil.StarExtra(PlayerDataUtil.StringToObject(player.data));
        int level1 = Mathf.Max(0, Mathf.Min(levelValue[0] - 1, GameContext.TOTAL_LEVEL));
        int level2 = Mathf.Max(0, Mathf.Min(levelValue[2] - 1, GameContext.TOTAL_LEVEL));
        int level3 = Mathf.Max(0, Mathf.Min(levelValue[4] - 1, GameContext.TOTAL_LEVEL));

        lLevel.text = I2.Loc.ScriptLocalization.level + ": " + level1 + ", " + level2 + ", " + level3;
        lStars.text = I2.Loc.ScriptLocalization.star + ": " + Mathf.Min(levelValue[1] + starExtra[0], PvpUtil.MaxStar(level1)) + ", " + Mathf.Min(levelValue[3] + starExtra[1], PvpUtil.MaxStar(level2)) + ", " + Mathf.Min(levelValue[5] + starExtra[2], PvpUtil.MaxStar(level3));
        sFlag.spriteName = player.country.ToLower();
        sFlagPVP.spriteName = player.country.ToLower();
        sFlag2v2.spriteName = player.country.ToLower();
    }

    Color tempColor = new Color(1, 1, 1, 1);
    Color hideColor = new Color(1, 1, 1, 0);
    Color nameClanColor = new Color(0.537f, 0.537f, 0.537f, 1);
    Vector3 tempVector3 = Vector3.zero;
    Vector3 tempVector3Table = Vector3.zero;

    public void TweenAppear(int i, Vector3 vt3, UITable table)
    {
        tempVector3.x = -150;
        tempVector3.y = vt3.y;
        tempVector3.z = vt3.z;
        transform.localPosition = tempVector3;
        sVipIcon.color = hideColor;
        sVipIcon2v2.color = hideColor;
        sVipIconPVP.color = hideColor;
        sSmallFrame.color = hideColor;
        lNumber.color = hideColor;
        sBigFrame.color = hideColor;
        sAvatar.color = hideColor;
        lName.color = hideColor;
        lNameClan.color = hideColor;
        lLevel.color = hideColor;
        lStars.color = hideColor;
        sFlag.color = hideColor;
        backgroundSprite.color = hideColor;

        sFlagPVP.color = hideColor;
        lNamePVP.color = hideColor;
        lNameClanPVP.color = hideColor;
        lElo.color = hideColor;
        lWinrate.color = hideColor;
        lMedalRank.color = hideColor;
        sMedalRank.color = hideColor;
        sStatusOffline.color = hideColor;

        sFlag2v2.color = hideColor;
        lName2v2.color = hideColor;
        lNameClan2v2.color = hideColor;
        lSeasonScore.color = hideColor;
        lWinrate2v2.color = hideColor;
        lMedalRank2v2.color = hideColor;
        sMedalRank2v2.color = hideColor;

        for (int j = 0; j < sMedals.Length; j++)
        {
            sMedals[j].color = hideColor;
        }
        LeanTween.value(gameObject, -150, 0, 0.3f).setDelay(i / 10f).setEase(LeanTweenType.easeOutBack).setOnUpdate((float obj) =>
        {
            tempVector3.x = obj;
            tempVector3.y = vt3.y;
            tempVector3.z = vt3.z;
            transform.localPosition = tempVector3;

            tempVector3Table.x = 0;
            tempVector3Table.y = table.transform.localPosition.y;
            tempVector3Table.z = table.transform.localPosition.z;

            table.transform.localPosition = tempVector3Table;
        }).setIgnoreTimeScale(true);
        LeanTween.value(gameObject, 0, 1, 0.5f).setDelay(i / 10f).setEase(LeanTweenType.easeOutBack).setOnUpdate((float obj) =>
        {
            nameClanColor.a = obj;
            lNameClan.color = nameClanColor;
            lNameClanPVP.color = nameClanColor;
            lNameClan2v2.color = nameClanColor;

            tempColor.a = obj;
            lNumber.color = tempColor;
            sBigFrame.color = tempColor;
            sSmallFrame.color = tempColor;
            sAvatar.color = tempColor;
            lName.color = tempColor;
            lLevel.color = tempColor;
            lStars.color = tempColor;
            sFlag.color = tempColor;
            backgroundSprite.color = tempColor;
            sVipIcon.color = tempColor;
            sVipIcon2v2.color = tempColor;
            sVipIconPVP.color = tempColor;

            sFlagPVP.color = tempColor;
            lNamePVP.color = tempColor;
            lElo.color = tempColor;
            lWinrate.color = tempColor;
            lMedalRank.color = tempColor;
            sMedalRank.color = tempColor;
            sStatusOffline.color = tempColor;
            for (int j = 0; j < sMedals.Length; j++)
            {
                sMedals[j].color = tempColor;
            }
            sFlag2v2.color = tempColor;
            lName2v2.color = tempColor;
            lSeasonScore.color = tempColor;
            lWinrate2v2.color = tempColor;
            if (!string.IsNullOrEmpty(lMedalRank2v2.text))
            {
                lMedalRank2v2.color = tempColor;
                sMedalRank2v2.color = tempColor;
            }
        }).setIgnoreTimeScale(true);
    }

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(backgroundSprite);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            //			thisCollider.enabled = isVisible;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }
}