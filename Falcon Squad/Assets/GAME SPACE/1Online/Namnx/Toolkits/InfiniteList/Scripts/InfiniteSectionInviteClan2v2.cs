﻿using UnityEngine;
using System.Collections;

public class InfiniteSectionInviteClan2v2 : MonoBehaviour {
	public UILabel label;
	public UIPanel panel;
	public InfiniteListInviteClan2v2 listPopulator;
	public int itemNumber;
	public int itemDataIndex;
	public bool isVisible = true;
	private BoxCollider thisCollider;

	// Use this for initialization
	void Start() 
	{
		thisCollider = GetComponent<BoxCollider>();
		transform.localScale = new Vector3(1,1,1);
	}
	void Update()
	{
		if(Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) >0)
		{
			CheckVisibilty();
		}
	}
	public bool verifyVisibility()
	{
		return(panel.IsVisible(label));
	}	

	void CheckVisibilty() 
	{
		bool currentVisibilty = panel.IsVisible(label);
		if(currentVisibilty != isVisible)
		{
			isVisible = currentVisibilty;
			thisCollider.enabled = isVisible;
			
			if(!isVisible)
			{
				listPopulator.StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
			}
		}
	}
}
