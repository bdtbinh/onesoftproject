﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;

public class InfiniteItemFriends : MonoBehaviour
{
    public UI2DSprite sAvatar;
    public UILabel lStatusOffline;
    public UISprite sStatusOffline;

    public UISprite sVipIcon;

    public string facebookID;

    public UISprite backgroundSprite;
    public UIPanel panel;
    public InfiniteListFriends listPopulator;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;

    public UISprite sFlag;
    public UILabel lName;
    public UILabel lNameClan;
    public UILabel lId;
    public UILabel lMedalRank;
    public UISprite sMedalRank;

    public GameObject buttonUnFriend;
    public GameObject buttonAccept;
    public GameObject buttonDeny;
    public GameObject unfriendSuccess;

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.Unfriend.ToString(), OnUnfriend, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.Unfriend.ToString(), OnUnfriend, true);
    }

    private void OnUnfriend(IMessage rMessage)
    {
        if (m_player != null)
        {
            SCUnFriend uf = rMessage.Data as SCUnFriend;
            if (uf.status == SCUnFriend.SUCCESS)
            {
                if (uf.code.Equals(m_player.code))
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.success, true);
                    listPopulator.dictionaryType[m_player.code] = 2;
                    buttonUnFriend.SetActive(false);
                    buttonAccept.SetActive(false);
                    buttonDeny.SetActive(false);
                    unfriendSuccess.SetActive(true);
                }
            }
            else
            {
                if (uf.code.Equals(m_player.code))
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(uf.message, false);
                    listPopulator.dictionaryType[m_player.code] = 1;
                    buttonUnFriend.SetActive(false);
                    buttonAccept.SetActive(true);
                    buttonDeny.SetActive(true);
                    unfriendSuccess.SetActive(false);
                }
            }
        }
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(backgroundSprite));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    Player m_player;

    void OnClick()
    {
        if (m_player != null)
        {
            PopupManager.Instance.ShowProfilePopupOnTop(m_player);
        }
    }

    public void SetData(Player player)
    {
        m_player = player;

        if (listPopulator.dictionaryType[player.code] == 0)
        {
            buttonUnFriend.SetActive(true);
            buttonAccept.SetActive(false);
            buttonDeny.SetActive(false);
            unfriendSuccess.SetActive(false);
        }
        else if (listPopulator.dictionaryType[player.code] == 1)
        {
            buttonUnFriend.SetActive(false);
            buttonAccept.SetActive(true);
            buttonDeny.SetActive(true);
            unfriendSuccess.SetActive(false);
        }
        else if (listPopulator.dictionaryType[player.code] == 2)
        {
            buttonUnFriend.SetActive(false);
            buttonAccept.SetActive(false);
            buttonDeny.SetActive(false);
            unfriendSuccess.SetActive(true);
        }

        int vip = CachePvp.GetVipFromVipPoint(player.vip);
        if (vip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (vip - 1);
        }

        tempVector3.x = 0;
        tempVector3.y = transform.localPosition.y;
        tempVector3.z = transform.localPosition.z;
        transform.localPosition = tempVector3;

        sStatusOffline.spriteName = player.offline ? "status_offline" : "status_online";
        lStatusOffline.text = player.offline ? I2.Loc.ScriptLocalization.offline : I2.Loc.ScriptLocalization.online;

        lId.text = I2.Loc.ScriptLocalization.id_key + ": " + player.code;
        if (player.playerClan != null)
        {
            lNameClan.text = player.playerClan.name;
        }
        else
        {
            lNameClan.text = "";
        }

        int rank = CachePvp.dictionaryRank[player.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lMedalRank.text = rankTitle + " " + player.playerLevel.name;
        sMedalRank.spriteName = "PVP_rank_" + rank;
        sMedalRank.MakePixelPerfect();
        tempVector3.x = 0.3f;
        tempVector3.y = 0.3f;
        tempVector3.z = 0.3f;
        sMedalRank.transform.localScale = tempVector3;
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        if (string.IsNullOrEmpty(player.name) || player.name.Trim().Equals(""))
        {
            lName.text = I2.Loc.ScriptLocalization.id_key + ": " + player.code;
        }
        else
        {
            lName.text = player.name.ToUpper();
        }
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(player.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                {
                    showImageFB = false;
                    facebookID = player.appCenterId;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        facebookID = player.appCenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (!string.IsNullOrEmpty(player.facebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                }
            }
            facebookID = player.facebookId;
        }
        int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);
        sFlag.spriteName = player.country.ToLower();
    }

    Vector3 tempVector3 = Vector3.zero;
    Vector3 tempVector3Table = Vector3.zero;

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(backgroundSprite);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }

    public void bUnfriend()
    {
        listPopulator.dictionaryType[m_player.code] = 1;
        buttonUnFriend.SetActive(false);
        buttonAccept.SetActive(true);
        buttonDeny.SetActive(true);
        unfriendSuccess.SetActive(false);
    }

    public void bAccept()
    {
        new CSUnFriend(m_player.code).Send();
        buttonAccept.SetActive(false);
        buttonDeny.SetActive(false);
    }

    public void bDeny()
    {
        listPopulator.dictionaryType[m_player.code] = 0;
        buttonUnFriend.SetActive(true);
        buttonAccept.SetActive(false);
        buttonDeny.SetActive(false);
        unfriendSuccess.SetActive(false);
    }
}