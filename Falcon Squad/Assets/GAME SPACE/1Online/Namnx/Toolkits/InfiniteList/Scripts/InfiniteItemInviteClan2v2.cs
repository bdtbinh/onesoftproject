﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;

public class InfiniteItemInviteClan2v2 : MonoBehaviour
{
    public UISprite sBigFrame;
    public UI2DSprite sAvatar;
    public UILabel lStatusOffline;
    public UISprite sStatusOffline;

    public UISprite sVipIcon;

    public string facebookID;

    public UISprite backgroundSprite;
    public UIPanel panel;
    public InfiniteListInviteClan2v2 listPopulator;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;

    public UISprite sFlag;
    public UILabel lName;
    public UILabel lElo;
    public UILabel lWinrate;
    public UILabel lMedalRank;
    public UISprite sMedalRank;

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(sBigFrame));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    void OnClick()
    {
        listPopulator.itemClicked(itemDataIndex);
        sBigFrame.gameObject.SetActive(true);
    }

    void OnPress(bool isDown)
    {
        listPopulator.itemIsPressed(itemDataIndex, isDown);
    }

    public void SetData(Player player)
    {
        int vip = CachePvp.GetVipFromVipPoint(player.vip);
        if (vip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (vip - 1);
        }

        tempVector3.x = 0;
        tempVector3.y = transform.localPosition.y;
        tempVector3.z = transform.localPosition.z;
        transform.localPosition = tempVector3;

        sStatusOffline.spriteName = player.offline ? "status_offline" : "status_online";
        lStatusOffline.text = player.offline ? I2.Loc.ScriptLocalization.offline : I2.Loc.ScriptLocalization.online;

        lWinrate.text = I2.Loc.ScriptLocalization.win + ": " + player.GetWinrateText();
        lElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(player.playerLevel.elo);

        int rank = CachePvp.dictionaryRank[player.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lMedalRank.text = rankTitle + " " + player.playerLevel.name;
        sMedalRank.spriteName = "PVP_rank_" + rank;

        sBigFrame.gameObject.SetActive(false);
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        if (string.IsNullOrEmpty(player.name) || player.name.Trim().Equals(""))
        {
            lName.text = I2.Loc.ScriptLocalization.id_key + ": " + player.code;
        }
        else
        {
            lName.text = player.name.ToUpper();
        }
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(player.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                {
                    showImageFB = false;
                    facebookID = player.appCenterId;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        facebookID = player.appCenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (!string.IsNullOrEmpty(player.facebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                }
            }
            facebookID = player.facebookId;
        }
        int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);
        sFlag.spriteName = player.country.ToLower();
    }

    Vector3 tempVector3 = Vector3.zero;
    Vector3 tempVector3Table = Vector3.zero;

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(backgroundSprite);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }
}