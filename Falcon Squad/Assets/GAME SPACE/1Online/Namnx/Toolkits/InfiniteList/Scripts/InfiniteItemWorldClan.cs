﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using TCore;

public class InfiniteItemWorldClan : MonoBehaviour
{
    public UISprite sBackground;
    public BoxCollider boxCollider;
    public UISprite sAvatar;
    public UILabel lName;
    public UILabel lId;
    public UILabel lTotalMember;

    public UILabel lScore;

    public InfiniteListWorldClanPopulator listPopulator;

    public UIPanel panel;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;
    private const string AVATAR = "avt_Avt_";

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Clan.RequestedJoinClan.ToString(), OnRequestJoinClan, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Clan.RequestedJoinClan.ToString(), OnRequestJoinClan, true);
    }

    private void OnRequestJoinClan(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        long id = (long)rMessage.Data;
        if (id == m_Clan.id)
        {
            switch (status)
            {
                case SCClanJoinRequest.SUCCESS:
                    //lStatusMember.color = new Color(1f, 1f, 1f);
                    if (!CachePvp.listClanIdRequest.Contains(id))
                    {
                        CachePvp.listClanIdRequest.Add(id);
                    }
                    break;
                case SCClanJoinRequest.JOINED_CLAN:
                    //lStatusMember.color = new Color(1f, 1f, 1f);
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_had_in_clan, false, 1.5f);
                    break;
                case SCClanJoinRequest.JOIN_REQUEST_EXISTED:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_had_requested, false, 1.5f);
                    break;
                case SCClanJoinRequest.CLAN_NOT_EXIST:
                    //lStatusMember.color = new Color(1f, 1f, 1f);
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_not_exist, false, 1.5f);
                    break;
                case SCClanJoinRequest.CLAN_FULL_MEMBER:
                    //lStatusMember.color = new Color(1f, 1f, 1f);
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_have_full_member, false, 1.5f);
                    break;
                case SCClanJoinRequest.NOT_ENOUGH_LEVEL:
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_level, false, 1.5f);
                    break;
                default:
                    break;
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(sBackground));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    void OnClick()
    {
        listPopulator.itemClicked(itemDataIndex);
    }

    void OnPress(bool isDown)
    {
        listPopulator.itemIsPressed(itemDataIndex, isDown);
    }

    ClanInfo m_Clan;

    Vector3 right = new Vector3(262, 1, 0);
    Vector3 center = new Vector3(96, 1, 0);

    public void RefreshData()
    {
        lId.text = (itemDataIndex + 1).ToString();
        if (m_Clan.avatar < 8)
        {
            sAvatar.spriteName = AVATAR + "0" + (m_Clan.avatar + 2);
        }
        else
        {
            sAvatar.spriteName = AVATAR + (m_Clan.avatar + 2);
        }

        lScore.text = I2.Loc.ScriptLocalization.score + ": " + GameContext.FormatNumber((int)m_Clan.score);
        lName.text = m_Clan.name;

        lTotalMember.text = m_Clan.totalMember + "/" + m_Clan.size;

        if (m_Clan.totalMember == m_Clan.size)
        {
            lTotalMember.color = new Color(0.6f, 0.17f, 0.29f);
        }
        else
        {
            lTotalMember.color = new Color(1, 1, 1);
        }
    }

    public void SetData(ClanInfo mc, int oldIndex = -1)
    {
        gameObject.SetActive(true);
        //avatar
        m_Clan = mc;
        lId.text = (itemDataIndex + 1).ToString();
        if (mc.avatar < 8)
        {
            sAvatar.spriteName = AVATAR + "0" + (mc.avatar + 2);
        }
        else
        {
            sAvatar.spriteName = AVATAR + (mc.avatar + 2);
        }

        lScore.text = I2.Loc.ScriptLocalization.score + ": " + GameContext.FormatNumber((int)mc.score);
        lName.text = mc.name;

        lTotalMember.text = mc.totalMember + "/" + mc.size;

        if (mc.totalMember == mc.size)
        {
            lTotalMember.color = new Color(0.6f, 0.17f, 0.29f);
        }
        else
        {
            lTotalMember.color = new Color(1, 1, 1);
        }
    }

    bool ContainId(long id)
    {
        for (int i = 0; i < CachePvp.listClanIdRequest.Count; i++)
        {
            if (CachePvp.listClanIdRequest[i] == id)
            {
                return true;
            }
        }
        return false;
    }

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(sBackground);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }

    public void bJoin()
    {
        PopupClan.Instance.ShowClanInfo(m_Clan);
    }
}