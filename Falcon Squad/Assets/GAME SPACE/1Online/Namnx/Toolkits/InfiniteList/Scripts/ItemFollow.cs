﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;

public class ItemFollow : MonoBehaviour
{
    public UISprite sVipIcon;
    public UISprite sSmallFrame;
    public UI2DSprite sAvatar;
    public UILabel lName;
    public UILabel lNameClan;
    public UISprite sFlag;
    public string facebookID;
    public UILabel lId;
    public UISprite sRank;
    public UILabel lRank;
    public UISprite sStatusOffline;
    public UILabel lStatusOffline;

    public GameObject buttonFollow;
    public GameObject buttonFollowDeactive;
    public GameObject buttonUnfollow;
    public GameObject buttonUnfollowDeactive;

    public UISprite backgroundSprite;
    public UIPanel panel;
    public InfiniteListPopulatorFollow listPopulator;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;

    Player m_player;

    void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.FollowFriend.ToString(), OnFollowFriend, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.FollowFriend.ToString(), OnFollowFriend, true);
    }

    private void OnFollowFriend(IMessage rMessage)
    {
        SCFollow f = rMessage.Data as SCFollow;
        if (f.status != SCFollow.SUCCESS)
        {
            if (f.code.Equals(m_player.code))
            {
                if (f.type == CSFollow.FOLLOW)
                {
                    buttonFollow.SetActive(true);
                    buttonFollowDeactive.SetActive(false);
                }
                else if (f.type == CSFollow.UNFOLLOW)
                {
                    buttonUnfollow.SetActive(true);
                    buttonUnfollowDeactive.SetActive(false);
                }
            }
        }
        else
        {
            if (f.type == CSFollow.FOLLOW)
            {
                if (f.code.Equals(m_player.code))
                {
                    buttonFollow.SetActive(false);
                    buttonFollowDeactive.SetActive(false);
                    buttonUnfollow.SetActive(true);
                    buttonUnfollowDeactive.SetActive(false);
                }
                listPopulator.dictionaryType[f.code] = 2;
            }
            else if (f.type == CSFollow.UNFOLLOW)
            {
                if (f.code.Equals(m_player.code))
                {
                    buttonUnfollow.SetActive(false);
                    buttonUnfollowDeactive.SetActive(false);
                    buttonFollow.SetActive(true);
                    buttonFollowDeactive.SetActive(false);
                }
                listPopulator.dictionaryType[f.code] = 0;
            }
        }
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public void PressButtonAccept()
    {
        buttonFollow.SetActive(false);
        buttonUnfollow.SetActive(false);
        buttonFollowDeactive.SetActive(true);
        buttonUnfollowDeactive.SetActive(false);
        listPopulator.dictionaryType[m_player.code] = 1;
        new CSFollow(CSFollow.FOLLOW, m_player.code).Send();
    }

    public void PressButtonDeny()
    {
        buttonFollow.SetActive(false);
        buttonUnfollow.SetActive(false);
        buttonUnfollowDeactive.SetActive(true);
        buttonFollowDeactive.SetActive(false);
        listPopulator.dictionaryType[m_player.code] = 3;
        new CSFollow(CSFollow.UNFOLLOW, m_player.code).Send();
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(backgroundSprite));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    void OnClick()
    {
        listPopulator.itemClicked(itemDataIndex);
    }

    void OnPress(bool isDown)
    {
        listPopulator.itemIsPressed(itemDataIndex, isDown);
    }

    public void SetData(Player player)
    {
        m_player = player;
        buttonFollow.SetActive(false);
        buttonUnfollow.SetActive(false);
        buttonFollowDeactive.SetActive(false);
        buttonUnfollowDeactive.SetActive(false);
        Debug.Log("SetData");
        if (listPopulator.dictionaryType[player.code] == 0) //unfollowed
        {
            buttonFollow.SetActive(true);
            buttonUnfollow.SetActive(false);
            buttonFollowDeactive.SetActive(false);
            buttonUnfollowDeactive.SetActive(false);
        }
        else if (listPopulator.dictionaryType[player.code] == 1) //following
        {
            buttonFollow.SetActive(false);
            buttonUnfollow.SetActive(false);
            buttonFollowDeactive.SetActive(true);
            buttonUnfollowDeactive.SetActive(false);
        }
        else if (listPopulator.dictionaryType[player.code] == 2) //followed
        {
            buttonFollow.SetActive(false);
            buttonUnfollow.SetActive(true);
            buttonFollowDeactive.SetActive(false);
            buttonUnfollowDeactive.SetActive(false);
        }
        else if (listPopulator.dictionaryType[player.code] == 3) //unfollowing
        {
            buttonFollow.SetActive(false);
            buttonUnfollow.SetActive(false);
            buttonFollowDeactive.SetActive(false);
            buttonUnfollowDeactive.SetActive(true);
        }
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        if (string.IsNullOrEmpty(player.name) || player.name.Trim().Equals(""))
        {
            lName.text = I2.Loc.ScriptLocalization.id_key + ": " + player.code;
        }
        else
        {
            lName.text = player.name.ToUpper();
        }
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(player.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                {
                    showImageFB = false;
                    facebookID = player.appCenterId;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        facebookID = player.appCenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            facebookID = player.facebookId;
            if (!string.IsNullOrEmpty(player.facebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                }
                else
                {
                    string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                }
            }
        }
        tempVector3.x = lNameClan.transform.localPosition.x;
        tempVector3.z = lNameClan.transform.localPosition.z;
        if (player.playerClan != null)
        {
            lNameClan.text = player.playerClan.name;
            tempVector3.y = 15;
        }
        else
        {
            lNameClan.text = "";
            tempVector3.y = 0;
        }
        sStatusOffline.spriteName = player.offline ? "status_offline" : "status_online";
        lStatusOffline.text = player.offline ? I2.Loc.ScriptLocalization.offline : I2.Loc.ScriptLocalization.online;
        lNameClan.transform.localPosition = tempVector3;
        lId.text = I2.Loc.ScriptLocalization.id_key + ": " + m_player.code;
        int rank = CachePvp.dictionaryRank[m_player.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lRank.text = rankTitle + " " + CachePvp.ConvertChina(m_player.playerLevel.name);
        sRank.spriteName = "PVP_rank_" + rank;
        
        int[] levelValue = PvpUtil.LevelValueDecode(player.levelValue);
        sFlag.spriteName = player.country.ToLower();
        int vip = CachePvp.GetVipFromVipPoint(player.vip);
        Debug.Log("player.vip : " + vip);
        if (vip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (vip - 1);
        }
    }

    Vector3 tempVector3 = Vector3.zero;

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(backgroundSprite);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }
}