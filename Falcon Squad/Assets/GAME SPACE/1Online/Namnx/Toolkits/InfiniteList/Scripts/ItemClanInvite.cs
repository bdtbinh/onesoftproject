﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using TCore;

public class ItemClanInvite : MonoBehaviour
{
    public UISprite sBackground;
    public BoxCollider boxCollider;
    public UISprite sAvatar;
    public UILabel lName;
    public UILabel lId;
    public UILabel lTotalMember;

    public UILabel lScore;

    public InfiniteListPopulatorClanInvite listPopulator;

    public UIPanel panel;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;
    private const string AVATAR = "avt_Avt_";

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Clan.AcceptClanInvitation.ToString(), OnAcceptClanInvitation, true);
        MessageDispatcher.AddListener(EventName.Clan.RejectClanInvitation.ToString(), OnRejectClanInvitation, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Clan.AcceptClanInvitation.ToString(), OnAcceptClanInvitation, true);
        MessageDispatcher.RemoveListener(EventName.Clan.RejectClanInvitation.ToString(), OnRejectClanInvitation, true);
    }

    private void OnRejectClanInvitation(IMessage rMessage)
    {
        int status = (int)rMessage.Recipient;
        switch (status)
        {
            case SCClanInvitationAccept.SUCCESS:
                MessageDispatcher.SendMessage(gameObject, EventName.Clan.RejectedClanInvitation.ToString(), (long)rMessage.Data, 0.05f);
                break;
            default:
                //error
                break;
        }
    }

    private void OnAcceptClanInvitation(IMessage rMessage)
    {
        int status = (int)rMessage.Data;
        switch (status)
        {
            case SCClanInvitationAccept.SUCCESS:
                CachePvp.TypeMemberClan = CachePvp.CLAN_MEMBER;
                MessageDispatcher.SendMessage(EventName.Clan.AcceptedClanInvitation.ToString(), 0.05f);
                Debug.Log("voa day");
                break;
            case SCClanInvitationAccept.FULL_MEMBER:
                lTotalMember.color = new Color(0.6f, 0.17f, 0.29f);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_have_full_member, false, 1.5f);
                break;
            case SCClanInvitationAccept.CLAN_NOT_FOUND:
                lTotalMember.color = new Color(0.6f, 0.17f, 0.29f);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_clan_not_exist, false, 1.5f);
                break;
            case SCClanInvitationAccept.INVITATION_NOT_EXIST:
                lTotalMember.color = new Color(0.6f, 0.17f, 0.29f);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_invitation_not_exist, false, 1.5f);
                break;
            case SCClanInvitationAccept.JOINED_CLAN:
                lTotalMember.color = new Color(0.6f, 0.17f, 0.29f);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_had_in_clan, false, 1.5f);
                break;
            default:
                break;
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(sBackground));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    void OnClick()
    {
        listPopulator.itemClicked(itemDataIndex);
    }

    void OnPress(bool isDown)
    {
        listPopulator.itemIsPressed(itemDataIndex, isDown);
    }

    ClanInfo m_Clan;

    public void SetData(ClanInfo mc)
    {
        gameObject.SetActive(true);
        //avatar
        m_Clan = mc;
        lId.text = (itemDataIndex + 1).ToString();
        if (mc.avatar < 8)
        {
            sAvatar.spriteName = AVATAR + "0" + (mc.avatar + 2);
        }
        else
        {
            sAvatar.spriteName = AVATAR + (mc.avatar + 2);
        }

        lScore.text = I2.Loc.ScriptLocalization.score + ": " + GameContext.FormatNumber((int)mc.score);
        lName.text = mc.name;

        lTotalMember.text = "Member: " + mc.totalMember + "/" + mc.size;

        if (mc.totalMember == mc.size)
        {
            lTotalMember.color = new Color(0.6f, 0.17f, 0.29f);
        }
        else
        {
            lTotalMember.color = new Color(1, 1, 1);
        }
    }

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(sBackground);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }

    public void bAccept()
    {
        listPopulator.AcceptClan(m_Clan.id);
    }

    public void bDeny()
    {
        listPopulator.DenyClan(m_Clan.id);
    }

    public void bView()
    {
        PopupClan.Instance.ShowClanInfo(m_Clan, true);
    }
}