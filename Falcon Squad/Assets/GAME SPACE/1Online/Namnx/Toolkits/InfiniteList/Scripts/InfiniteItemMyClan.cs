﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using TCore;

public class InfiniteItemMyClan : MonoBehaviour
{
    public UISprite sBackground;
    public UISprite sIconMember;
    public BoxCollider boxCollider;
    public UI2DSprite sAvatar;
    public UILabel lName;
    public UILabel lId;
    public UISprite sRank;
    public UISprite sFlag;
    public UILabel lRank;

    public UILabel lScore;
    public UILabel lStatus; //online ; offline
    public UISprite sStatus;

    public UISprite sVipIcon;

    public GameObject buttonReadmore;
    public GameObject buttonReadless;
    public GameObject sReadless;

    public GameObject buttonUpToMaster;
    public GameObject buttonUpToVice;
    public GameObject buttonDownToMember;
    public GameObject buttonKick;

    public InfiniteListMyClanPopulator listPopulator;

    public string facebookID;

    public UIPanel panel;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;
    private const string ICON_MASTER = "Clan_Master_laber";
    private const string ICON_VICE = "Clan_Vice_laber";

    public bool isOpened;
    public bool isDeleted;

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(sBackground));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    void OnClick()
    {
        listPopulator.itemClicked(itemDataIndex);
    }

    void OnPress(bool isDown)
    {
        listPopulator.itemIsPressed(itemDataIndex, isDown);
    }

    Player m_Player;

    public void SetData(Player mc, int oldIndex = -1)
    {
        if (isDeleted)
        {
            gameObject.SetActive(false);
            return;
        }
        else
        {
            gameObject.SetActive(true);
        }
        if (mc != null)
        {
            Debug.Log("player.vip : " + CachePvp.GetVipFromVipPoint(mc.vip));
        }
        if (mc == null || CachePvp.GetVipFromVipPoint(mc.vip) == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (CachePvp.GetVipFromVipPoint(mc.vip) - 1);
        }

        //avatar
        m_Player = mc;
        lId.text = I2.Loc.ScriptLocalization.id_key + ": " + mc.code;
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        sFlag.spriteName = mc.country.ToLower();

        int rank = CachePvp.dictionaryRank[mc.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lRank.text = rankTitle + " " + CachePvp.ConvertChina(mc.playerLevel.name);
        sRank.spriteName = "PVP_rank_" + rank;
        lScore.text = GameContext.FormatNumber((int)mc.playerClan.score);
        lStatus.text = mc.offline ? I2.Loc.ScriptLocalization.offline : I2.Loc.ScriptLocalization.online;
        sStatus.spriteName = mc.offline ? "status_offline" : "status_online";

        if (mc != null && !string.IsNullOrEmpty(mc.code))
        {
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(mc.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(mc.appCenterId))
                    {
                        showImageFB = false;
                        facebookID = mc.appCenterId;
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[mc.appCenterId];
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(mc.appCenterId);
                        if (t != null)
                        {
                            showImageFB = false;
                            facebookID = mc.appCenterId;
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", mc.appCenterId, t);
                        }
                    }
                }
            }
            if (showImageFB)
            {
                facebookID = mc.facebookId;
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(mc.facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(mc.facebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[mc.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + mc.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, mc.facebookId);
                    }
                }
            }
            if (!string.IsNullOrEmpty(mc.name))
            {
                lName.text = mc.name;
            }
            else
            {
                lName.text = mc.code;
            }
        }

        switch (mc.playerClan.type)
        {
            case CachePvp.CLAN_MASTER:
                sIconMember.gameObject.SetActive(true);
                sIconMember.spriteName = ICON_MASTER;
                break;
            case CachePvp.CLAN_VICE_MASTER:
                listPopulator.totalViceInClan++;
                sIconMember.gameObject.SetActive(true);
                sIconMember.spriteName = ICON_VICE;
                break;
            default:
                sIconMember.gameObject.SetActive(false);
                break;
        }
        sIconMember.MakePixelPerfect();

        int cnt = 0;
        if (isOpened)
        {
            buttonReadless.SetActive(true);
            buttonReadmore.SetActive(false);
            sReadless.SetActive(true);
            if (CachePvp.TypeMemberClan == CachePvp.CLAN_MASTER)
            {
                switch (m_Player.playerClan.type)
                {
                    case CachePvp.CLAN_MASTER:
                        //master
                        buttonUpToMaster.SetActive(false);
                        buttonUpToVice.SetActive(false);
                        buttonDownToMember.SetActive(false);
                        buttonKick.SetActive(false);
                        cnt = 1;
                        break;
                    case CachePvp.CLAN_VICE_MASTER:
                        //vice
                        buttonUpToMaster.SetActive(true);
                        buttonUpToVice.SetActive(false);
                        buttonDownToMember.SetActive(true);
                        buttonKick.SetActive(true);
                        cnt = 3;
                        break;
                    case CachePvp.CLAN_MEMBER:
                        //member
                        buttonUpToMaster.SetActive(true);
                        buttonUpToVice.SetActive(true);
                        buttonDownToMember.SetActive(false);
                        buttonKick.SetActive(true);
                        cnt = 3;
                        break;
                    default:
                        break;
                }
            }
            else if (CachePvp.TypeMemberClan == CachePvp.CLAN_VICE_MASTER)
            {
                switch (m_Player.playerClan.type)
                {
                    case CachePvp.CLAN_MEMBER:
                        //member
                        buttonUpToMaster.SetActive(false);
                        buttonUpToVice.SetActive(false);
                        buttonDownToMember.SetActive(false);
                        buttonKick.SetActive(true);
                        cnt = 1;
                        break;
                    default:
                        break;
                }
            }
            else if (CachePvp.TypeMemberClan == CachePvp.CLAN_MEMBER)
            {
                if (m_Player.code.Equals(CachePvp.Code))
                {
                    buttonUpToMaster.SetActive(false);
                    buttonUpToVice.SetActive(false);
                    buttonDownToMember.SetActive(false);
                    buttonKick.SetActive(false);
                    cnt = 1;
                }
            }

            if (cnt == 3)
            {
                sBackground.height = 288;
            }
            else if (cnt == 1)
            {
                sBackground.height = 180;
            }
            boxCollider.size = new Vector3(boxCollider.size.x, sBackground.height + listPopulator.padding, boxCollider.size.z);
            boxCollider.center = new Vector3(boxCollider.center.x, -(sBackground.height - listPopulator.cellHeight) / 2f, boxCollider.center.z);
            listPopulator.itemsPoolMyClanHeight[itemDataIndex] = sBackground.height + listPopulator.padding;
            Reposition();
        }
        else
        {
            sBackground.height = 110;
            boxCollider.size = new Vector3(boxCollider.size.x, listPopulator.cellHeight + listPopulator.padding, boxCollider.size.z);
            boxCollider.center = new Vector3(boxCollider.center.x, -(sBackground.height - listPopulator.cellHeight) / 2f, boxCollider.center.z);
            listPopulator.itemsPoolMyClanHeight[itemDataIndex] = listPopulator.cellHeight + listPopulator.padding;

            if (CachePvp.TypeMemberClan == CachePvp.CLAN_MASTER)
            {
                if (m_Player.playerClan.type != CachePvp.CLAN_MASTER)
                {
                    buttonReadless.SetActive(false);
                    buttonReadmore.SetActive(true);
                    sReadless.SetActive(false);
                }
                else
                {
                    buttonReadless.SetActive(false);
                    buttonReadmore.SetActive(false);
                    sReadless.SetActive(false);
                }
            }
            else if (CachePvp.TypeMemberClan == CachePvp.CLAN_VICE_MASTER)
            {
                if (m_Player.playerClan.type == CachePvp.CLAN_MASTER || m_Player.playerClan.type == CachePvp.CLAN_VICE_MASTER)
                {
                    buttonReadless.SetActive(false);
                    buttonReadmore.SetActive(false);
                    sReadless.SetActive(false);
                }
                else if (m_Player.playerClan.type == CachePvp.CLAN_MEMBER)
                {
                    buttonReadless.SetActive(false);
                    buttonReadmore.SetActive(true);
                    sReadless.SetActive(false);
                }
            }
            else if (CachePvp.TypeMemberClan == CachePvp.CLAN_MEMBER)
            {
                buttonReadless.SetActive(false);
                buttonReadmore.SetActive(false);
                sReadless.SetActive(false);
            }

            buttonUpToMaster.SetActive(false);
            buttonUpToVice.SetActive(false);
            buttonDownToMember.SetActive(false);
            buttonKick.SetActive(false);
        }
    }

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(sBackground);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }

    int maxHeight = 0;

    public void ButtonReadmore()
    {
        Debug.Log("type : " + m_Player.playerClan.type);
        Debug.Log("type cac : " + CachePvp.TypeMemberClan);
        isOpened = true;
        listPopulator.itemsPoolMyClanClickReadmore[itemDataIndex] = isOpened;
        int cnt = 0;

        buttonReadmore.SetActive(false);
        buttonReadless.SetActive(true);
        sReadless.SetActive(true);

        if (CachePvp.TypeMemberClan == CachePvp.CLAN_MASTER)
        {
            switch (m_Player.playerClan.type)
            {
                case CachePvp.CLAN_VICE_MASTER:
                    //vice
                    buttonUpToMaster.SetActive(true);
                    buttonUpToVice.SetActive(false);
                    buttonDownToMember.SetActive(true);
                    buttonKick.SetActive(true);
                    cnt = 3;
                    break;
                case CachePvp.CLAN_MEMBER:
                    //member
                    buttonUpToMaster.SetActive(true);
                    buttonUpToVice.SetActive(true);
                    buttonDownToMember.SetActive(false);
                    buttonKick.SetActive(true);
                    cnt = 3;
                    break;
                default:
                    break;
            }
        }
        else if (CachePvp.TypeMemberClan == CachePvp.CLAN_VICE_MASTER)
        {
            switch (m_Player.playerClan.type)
            {
                case CachePvp.CLAN_MEMBER:
                    //member
                    buttonUpToMaster.SetActive(false);
                    buttonUpToVice.SetActive(false);
                    buttonDownToMember.SetActive(false);
                    buttonKick.SetActive(true);
                    cnt = 1;
                    break;
                default:
                    break;
            }
        }
        if (cnt == 3)
        {
            sBackground.height = 288;
            boxCollider.size = new Vector3(boxCollider.size.x, sBackground.height + listPopulator.padding, boxCollider.size.z);
            boxCollider.center = new Vector3(boxCollider.center.x, -(sBackground.height - listPopulator.cellHeight) / 2f, boxCollider.center.z);
            listPopulator.itemsPoolMyClanHeight[itemDataIndex] = sBackground.height + listPopulator.padding;
        }
        else if (cnt == 1)
        {
            sBackground.height = 180;
            boxCollider.size = new Vector3(boxCollider.size.x, sBackground.height + listPopulator.padding, boxCollider.size.z);
            boxCollider.center = new Vector3(boxCollider.center.x, -(sBackground.height - listPopulator.cellHeight) / 2f, boxCollider.center.z);
            listPopulator.itemsPoolMyClanHeight[itemDataIndex] = sBackground.height + listPopulator.padding;
        }

        Reposition();
    }

    void Reposition()
    {
        float posY = listPopulator.itemsPoolMyClan[itemNumber].transform.localPosition.y;
        for (int i = itemDataIndex; i < listPopulator.itemsPoolMyClanPositionY.Count; i++)
        {
            if (listPopulator.GetItemByDataIndex(i) != null)
            {
                listPopulator.GetItemByDataIndex(i).transform.localPosition = new Vector3(0, posY, 0);
            }
            listPopulator.itemsPoolMyClanPositionY[i] = posY;
            posY -= listPopulator.itemsPoolMyClanHeight[i];
        }
    }

    public void ButtonReadless()
    {
        isOpened = false;
        listPopulator.itemsPoolMyClanClickReadmore[itemDataIndex] = isOpened;
        buttonReadmore.SetActive(true);
        buttonReadless.SetActive(false);
        sReadless.SetActive(false);

        buttonUpToMaster.SetActive(false);
        buttonUpToVice.SetActive(false);
        buttonDownToMember.SetActive(false);
        buttonKick.SetActive(false);

        sBackground.height = 110;
        boxCollider.size = new Vector3(boxCollider.size.x, sBackground.height + listPopulator.padding, boxCollider.size.z);
        boxCollider.center = new Vector3(boxCollider.center.x, -(sBackground.height - listPopulator.cellHeight) / 2f, boxCollider.center.z);
        listPopulator.itemsPoolMyClanHeight[itemDataIndex] = sBackground.height + listPopulator.padding;
        Reposition();
    }

    public void bKick()
    {
        listPopulator.KickMemberClan(m_Player);
    }

    public void bSetToMaster()
    {
        listPopulator.SetToMasterClan(m_Player);
    }

    public void bSetToMember()
    {
        listPopulator.SetToMemberClan(m_Player);
    }

    public void bSetToVice()
    {
        if (listPopulator.totalViceInClan >= 2)
        {
            listPopulator.CantSetVice();
        }
        else
        {
            listPopulator.SetToViceClan(m_Player);
        }
    }
}