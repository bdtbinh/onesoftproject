﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using TCore;

public class ItemGloryChest : MonoBehaviour
{
    public UISprite sBackground;
    public UISprite sIconMember;
    public BoxCollider boxCollider;
    public UI2DSprite sAvatar;
    public UILabel lName;
    public UILabel lId;
    public UISprite sRank;
    public UISprite sFlag;
    public UILabel lRank;

    public UILabel lScore;
    public UILabel lStatus; //online ; offline
    public UISprite sStatus;

    public UISprite sVipIcon;

    public InfiniteListPopulatorGloryChest listPopulator;

    public string facebookID;

    public UIPanel panel;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;
    private const string ICON_MASTER = "Clan_Master_laber";
    private const string ICON_VICE = "Clan_Vice_laber";

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(sBackground));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    void OnClick()
    {
        listPopulator.itemClicked(itemDataIndex);
    }

    void OnPress(bool isDown)
    {
        listPopulator.itemIsPressed(itemDataIndex, isDown);
    }

    Player m_Player;

    public void SetData(Player mc)
    {
        gameObject.SetActive(true);
        if (mc == null || CachePvp.GetVipFromVipPoint(mc.vip) == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (CachePvp.GetVipFromVipPoint(mc.vip) - 1);
        }
        //avatar
        m_Player = mc;
        lId.text = I2.Loc.ScriptLocalization.id_key + ": " + mc.code;
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        sFlag.spriteName = mc.country.ToLower();

        int rank = CachePvp.dictionaryRank[mc.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lRank.text = rankTitle + " " + CachePvp.ConvertChina(mc.playerLevel.name);
        sRank.spriteName = "PVP_rank_" + rank;
        lScore.text = GameContext.FormatNumber((int)mc.playerClan.scoreTournament);
        lStatus.text = mc.offline ? I2.Loc.ScriptLocalization.offline : I2.Loc.ScriptLocalization.online;
        sStatus.spriteName = mc.offline ? "status_offline" : "status_online";

        if (mc != null && !string.IsNullOrEmpty(mc.code))
        {
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(mc.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(mc.appCenterId))
                    {
                        showImageFB = false;
                        facebookID = mc.appCenterId;
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[mc.appCenterId];
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(mc.appCenterId);
                        if (t != null)
                        {
                            showImageFB = false;
                            facebookID = mc.appCenterId;
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", mc.appCenterId, t);
                        }
                    }
                }
            }
            if (showImageFB)
            {
                facebookID = mc.facebookId;
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(mc.facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(mc.facebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[mc.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + mc.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, mc.facebookId);
                    }
                }
            }
            if (!string.IsNullOrEmpty(mc.name))
            {
                lName.text = mc.name;
            }
            else
            {
                lName.text = mc.code;
            }
        }

        switch (mc.playerClan.type)
        {
            case CachePvp.CLAN_MASTER:
                sIconMember.gameObject.SetActive(true);
                sIconMember.spriteName = ICON_MASTER;
                break;
            case CachePvp.CLAN_VICE_MASTER:
                sIconMember.gameObject.SetActive(true);
                sIconMember.spriteName = ICON_VICE;
                break;
            default:
                sIconMember.gameObject.SetActive(false);
                break;
        }
        sIconMember.MakePixelPerfect();
    }

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(sBackground);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }
}