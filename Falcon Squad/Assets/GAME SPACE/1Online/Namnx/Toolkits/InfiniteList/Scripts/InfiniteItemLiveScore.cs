﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using TCore;
using System.Collections.Generic;

public class InfiniteItemLiveScore : MonoBehaviour
{
    public GameObject pvp1v1;
    public GameObject pvp2v2;

    public UISprite sBackground2v2;
    public UILabel lTimeRemaining2v2;
    public UISprite sHealthBarTeam1;
    public UISprite sHealthBarTeam2;
    public UILabel lScoreTeam1;
    public UILabel lScoreTeam2;

    public List<UI2DSprite> listAvatar2v2 = new List<UI2DSprite>();
    public List<UISprite> listFlag2v2 = new List<UISprite>();
    public List<UISprite> listVipIcon2v2 = new List<UISprite>();
    public List<UILabel> listName2v2 = new List<UILabel>();
    public List<UISprite> listShip2v2 = new List<UISprite>();
    public List<GameObject> listQuit2v2 = new List<GameObject>();
    List<string> listFacebookID2v2 = new List<string>();
    List<string> listCode2v2 = new List<string>();

    public GameObject gWinTeam1;
    public GameObject gWinTeam2;

    public GameObject gDraw2v2;

    //1v1
    public UISprite sBackground;
    public UILabel lTimeRemaining;
    public UISprite sHealthBarP1;
    public UI2DSprite sAvatarP1;
    public UISprite sFlagP1;
    public UISprite sVipIconP1;
    public UILabel lNameP1;
    public UILabel lScoreP1;

    public UISprite sRankP1;
    public UILabel lRankP1;
    public UISprite sShipP1;
    public GameObject gWinP1;
    public GameObject gQuitP1;
    public string facebookIDP1;

    public UISprite sHealthBarP2;
    public UI2DSprite sAvatarP2;
    public UISprite sFlagP2;
    public UISprite sVipIconP2;
    public UILabel lNameP2;
    public UILabel lScoreP2;

    public UISprite sRankP2;
    public UILabel lRankP2;
    public UISprite sShipP2;
    public GameObject gWinP2;
    public GameObject gQuitP2;
    public string facebookIDP2;

    public GameObject gDraw;

    public InfiniteListPopupLiveScore listPopulator;

    public UIPanel panel;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;

    public bool isDone;

    int scoreTeam1;
    int scoreTeam2;

    int scoreP1;
    int scoreP2;
    string codeP1;
    string codeP2;
    Color colorLose = new Color(0.658f, 0.658f, 0.658f, 0.93f);

    const string HEALTH_BAR_BLUE_1 = "healbar_blueplayer_1";
    const string HEALTH_BAR_BLUE_2 = "healbar_blueplayer_2";
    const string HEALTH_BAR_RED_1 = "healbar_redplayer_1";
    const string HEALTH_BAR_RED_2 = "healbar_redplayer_2";

    private bool is1v1;

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookIDP1))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIDP1))
            {
                if (sAvatarP1 != null)
                {
                    sAvatarP1.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIDP1];
                }
            }
        }
        if (!string.IsNullOrEmpty(facebookIDP2))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIDP2))
            {
                if (sAvatarP2 != null)
                {
                    sAvatarP2.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIDP2];
                }
            }
        }
        for (int i = 0; i < listFacebookID2v2.Count; i++)
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(listFacebookID2v2[i]))
            {
                listAvatar2v2[i].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[listFacebookID2v2[i]];
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(sBackground));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    public void UpdateData(RoomInfo ri)
    {
        m_RoomInfo = ri;
        lTimeRemaining.text = (m_RoomInfo.timeRemaining / 60).ToString("00") + ":" + (m_RoomInfo.timeRemaining % 60).ToString("00");
        if (m_RoomInfo.state == RoomInfo.END)
        {
            lTimeRemaining.text = "End";
        }
        else
        {
            gQuitP1.SetActive(false);
            gQuitP2.SetActive(false);
            gWinP1.SetActive(false);
            gWinP2.SetActive(false);
            gDraw.SetActive(false);
        }
        for (int i = 0; i < m_RoomInfo.playerResults.Count; i++)
        {
            if (m_RoomInfo.playerResults[i].code.Equals(codeP1))
            {
                //P1;
                //public UI2DSprite sHealthBarP1;
                scoreP1 = m_RoomInfo.playerResults[i].score;
                lScoreP1.text = GameContext.FormatNumber(m_RoomInfo.playerResults[i].score);
                if (m_RoomInfo.state == RoomInfo.END)
                {
                    if (m_RoomInfo.playerResults[i].result == PlayerResult.WIN)
                    {
                        gWinP1.SetActive(true);
                        gQuitP1.SetActive(false);
                        gDraw.SetActive(false);
                    }
                    else if (m_RoomInfo.playerResults[i].result == PlayerResult.LOSE)
                    {
                        gDraw.SetActive(false);
                        gWinP1.SetActive(false);
                        if (m_RoomInfo.playerResults[i].state == PlayerResult.OUT)
                        {
                            gQuitP1.SetActive(true);
                        }
                    }
                    else
                    {
                        gDraw.SetActive(true);
                        gWinP1.SetActive(false);
                        gWinP2.SetActive(false);
                    }
                }
            }
            else if (m_RoomInfo.playerResults[i].code.Equals(codeP2))
            {
                //P2;
                scoreP2 = m_RoomInfo.playerResults[i].score;
                lScoreP2.text = GameContext.FormatNumber(m_RoomInfo.playerResults[i].score);
                if (m_RoomInfo.state == RoomInfo.END)
                {
                    if (m_RoomInfo.playerResults[i].result == PlayerResult.WIN)
                    {
                        gDraw.SetActive(false);
                        gWinP2.SetActive(true);
                        gQuitP2.SetActive(false);
                    }
                    else if (m_RoomInfo.playerResults[i].result == PlayerResult.LOSE)
                    {
                        gDraw.SetActive(false);
                        gWinP2.SetActive(false);
                        if (m_RoomInfo.playerResults[i].state == PlayerResult.OUT)
                        {
                            gQuitP2.SetActive(true);
                        }
                    }
                }
            }
        }
        UpdateStatusHealthBar();
    }

    public void UpdateData2v2(CoopPvPRoomData ri)
    {
        m_RoomInfo2v2 = ri;
        lTimeRemaining2v2.text = (m_RoomInfo2v2.remainTime / 60).ToString("00") + ":" + (m_RoomInfo2v2.remainTime % 60).ToString("00");
        for (int i = 0; i < listQuit2v2.Count; i++)
        {
            listQuit2v2[i].SetActive(false);
        }
        gWinTeam1.SetActive(false);
        gWinTeam2.SetActive(false);
        gDraw2v2.SetActive(false);
        if (m_RoomInfo2v2.state == CoopPvPRoomData.END)
        {
            lTimeRemaining.text = "End";
        }
        for (int i = 0; i < m_RoomInfo2v2.teams.Count; i++)
        {
            if (i == 0)
            {
                lScoreTeam1.text = GameContext.FormatNumber(m_RoomInfo2v2.teams[i].score);
                scoreTeam1 = m_RoomInfo2v2.teams[i].score;
            }
            else if (i == 1)
            {
                lScoreTeam2.text = GameContext.FormatNumber(m_RoomInfo2v2.teams[i].score);
                scoreTeam2 = m_RoomInfo2v2.teams[i].score;
            }
            for (int j = 0; j < m_RoomInfo2v2.teams[i].members.Count; j++)
            {
                if (m_RoomInfo2v2.teams[i].members[j].state == CoopPvPPlayerData.STATE_OUT)
                {
                    listQuit2v2[i * 2 + j].SetActive(true);
                }
                if (m_RoomInfo2v2.state == CoopPvPRoomData.END)
                {
                    gDraw2v2.SetActive(false);
                    if (m_RoomInfo2v2.teams[0].result == CoopPvPTeamData.WIN)
                    {
                        gWinTeam1.SetActive(true);
                        gWinTeam2.SetActive(false);
                    }
                    else if (m_RoomInfo2v2.teams[1].result == CoopPvPTeamData.WIN)
                    {
                        gWinTeam1.SetActive(false);
                        gWinTeam2.SetActive(true);
                    }
                    else
                    {
                        gDraw2v2.SetActive(true);
                        gWinTeam1.SetActive(false);
                        gWinTeam2.SetActive(false);
                    }
                }
            }
        }
        UpdateStatusHealthBar();
    }

    public void ShowInfoP1()
    {
        //PopupManager.Instance.ShowProfilePopup()
        new CSPlayerInfo(codeP1).Send();
    }

    public void ShowInfoP2()
    {
        new CSPlayerInfo(codeP2).Send();
    }

    public void showInfoP1Team1()
    {
        new CSPlayerInfo(listCode2v2[0]).Send();
    }

    public void showInfoP2Team1()
    {
        new CSPlayerInfo(listCode2v2[1]).Send();
    }

    public void showInfoP1Team2()
    {
        new CSPlayerInfo(listCode2v2[2]).Send();
    }

    public void showInfoP2Team2()
    {
        new CSPlayerInfo(listCode2v2[3]).Send();
    }

    public RoomInfo m_RoomInfo;
    public CoopPvPRoomData m_RoomInfo2v2;

    public void SetData(RoomInfo ri, int oldIndex = -1)
    {
        is1v1 = true;
        pvp1v1.SetActive(true);
        pvp2v2.SetActive(false);
        gameObject.SetActive(true);
        m_RoomInfo = ri;
        lTimeRemaining.text = (m_RoomInfo.timeRemaining / 60).ToString("00") + ":" + (m_RoomInfo.timeRemaining % 60).ToString("00");
        if (m_RoomInfo.state == RoomInfo.END)
        {
            lTimeRemaining.text = "End";
        }
        else
        {
            gQuitP1.SetActive(false);
            gQuitP2.SetActive(false);
            gWinP1.SetActive(false);
            gWinP2.SetActive(false);
            gDraw.SetActive(false);
        }
        for (int i = 0; i < m_RoomInfo.playerResults.Count; i++)
        {
            if (i == 0)
            {
                //P1;
                //public UI2DSprite sHealthBarP1;
                codeP1 = m_RoomInfo.playerResults[i].code;
                sAvatarP1.sprite2D = CacheAvatarManager.Instance.defaultAvatar;

                bool showImageFB = true;
                if (GameContext.IS_CHINA_VERSION)
                {
                    if (!string.IsNullOrEmpty(m_RoomInfo.playerResults[i].appCenterId))
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(m_RoomInfo.playerResults[i].appCenterId))
                        {
                            showImageFB = false;
                            facebookIDP1 = m_RoomInfo.playerResults[i].appCenterId;
                            sAvatarP1.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[m_RoomInfo.playerResults[i].appCenterId];
                        }
                        else
                        {
                            Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(m_RoomInfo.playerResults[i].appCenterId);
                            if (t != null)
                            {
                                showImageFB = false;
                                facebookIDP1 = m_RoomInfo.playerResults[i].appCenterId;
                                CacheAvatarManager.Instance.DownloadImageFormUrl("", m_RoomInfo.playerResults[i].appCenterId, t);
                            }
                        }
                    }
                }
                if (showImageFB)
                {
                    facebookIDP1 = m_RoomInfo.playerResults[i].facebookId;
                    if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(m_RoomInfo.playerResults[i].facebookId))
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(m_RoomInfo.playerResults[i].facebookId))
                        {
                            sAvatarP1.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[m_RoomInfo.playerResults[i].facebookId];
                        }
                        else
                        {
                            string query = "https://graph.facebook.com/" + m_RoomInfo.playerResults[i].facebookId + "/picture?type=small&width=135&height=135";
                            CacheAvatarManager.Instance.DownloadImageFormUrl(query, m_RoomInfo.playerResults[i].facebookId);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(m_RoomInfo.playerResults[i].country))
                {
                    sFlagP1.spriteName = m_RoomInfo.playerResults[i].country.ToLower();
                }
                else
                {
                    sFlagP1.spriteName = "us";
                }
                if (CachePvp.GetVipFromVipPoint(m_RoomInfo.playerResults[i].vipPoint) == 0)
                {
                    sVipIconP1.gameObject.SetActive(false);
                }
                else
                {
                    sVipIconP1.gameObject.SetActive(true);
                    sVipIconP1.spriteName = "Icon__VIP_" + (CachePvp.GetVipFromVipPoint(m_RoomInfo.playerResults[i].vipPoint) - 1);
                }
                lNameP1.text = m_RoomInfo.playerResults[i].name;
                lScoreP1.text = GameContext.FormatNumber(m_RoomInfo.playerResults[i].score);
                int rank = CachePvp.dictionaryRank[m_RoomInfo.playerResults[i].playerLevel.code];
                MedalRankTitle rankTitle = (MedalRankTitle)(rank);
                lRankP1.text = rankTitle + " " + CachePvp.ConvertChina(m_RoomInfo.playerResults[i].playerLevel.name);
                sRankP1.spriteName = "PVP_rank_" + rank;
                sRankP1.MakePixelPerfect();
                sRankP1.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                scoreP1 = m_RoomInfo.playerResults[i].score;
                if (m_RoomInfo.state == RoomInfo.END)
                {
                    gDraw.SetActive(false);
                    if (m_RoomInfo.playerResults[i].result == PlayerResult.WIN)
                    {
                        gWinP1.SetActive(true);
                        gQuitP1.SetActive(false);
                    }
                    else if (m_RoomInfo.playerResults[i].result == PlayerResult.LOSE)
                    {
                        gWinP1.SetActive(false);
                        if (m_RoomInfo.playerResults[i].state == PlayerResult.OUT)
                        {
                            gQuitP1.SetActive(true);
                        }
                    }
                    else
                    {
                        gDraw.SetActive(true);
                        gWinP1.SetActive(false);
                        gWinP2.SetActive(false);
                    }
                }
                if (m_RoomInfo.playerResults[i].listRankPlane != null)
                {
                    AircraftTypeEnum aircraftType = (AircraftTypeEnum)m_RoomInfo.playerResults[i].selectedShip;
                    if (m_RoomInfo.playerResults[i].listRankPlane.Count > m_RoomInfo.playerResults[i].selectedShip - 1)
                    {
                        sShipP1.spriteName = "aircraft" + (int)aircraftType + "_e" + m_RoomInfo.playerResults[i].listRankPlane[m_RoomInfo.playerResults[i].selectedShip - 1] + "_idle_0";
                    }
                    else
                    {
                        Array airCrafts = Enum.GetValues(typeof(AircraftTypeEnum));
                        sShipP1.spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
                    }
                }
                else
                {
                    AircraftTypeEnum aircraftType = (AircraftTypeEnum)m_RoomInfo.playerResults[i].spaceship;
                    Array airCrafts = Enum.GetValues(typeof(AircraftTypeEnum));
                    sShipP1.spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
                }
                sShipP1.MakePixelPerfect();
                sShipP1.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            }
            else if (i == 1)
            {
                //P2;
                codeP2 = m_RoomInfo.playerResults[i].code;
                sAvatarP2.sprite2D = CacheAvatarManager.Instance.defaultAvatar;

                bool showImageFB = true;
                if (GameContext.IS_CHINA_VERSION)
                {
                    if (!string.IsNullOrEmpty(m_RoomInfo.playerResults[i].appCenterId))
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(m_RoomInfo.playerResults[i].appCenterId))
                        {
                            showImageFB = false;
                            facebookIDP2 = m_RoomInfo.playerResults[i].appCenterId;
                            sAvatarP2.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[m_RoomInfo.playerResults[i].appCenterId];
                        }
                        else
                        {
                            Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(m_RoomInfo.playerResults[i].appCenterId);
                            if (t != null)
                            {
                                showImageFB = false;
                                facebookIDP2 = m_RoomInfo.playerResults[i].appCenterId;
                                CacheAvatarManager.Instance.DownloadImageFormUrl("", m_RoomInfo.playerResults[i].appCenterId, t);
                            }
                        }
                    }
                }
                if (showImageFB)
                {
                    facebookIDP2 = m_RoomInfo.playerResults[i].facebookId;
                    if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(m_RoomInfo.playerResults[i].facebookId))
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(m_RoomInfo.playerResults[i].facebookId))
                        {
                            sAvatarP2.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[m_RoomInfo.playerResults[i].facebookId];
                        }
                        else
                        {
                            string query = "https://graph.facebook.com/" + m_RoomInfo.playerResults[i].facebookId + "/picture?type=small&width=135&height=135";
                            CacheAvatarManager.Instance.DownloadImageFormUrl(query, m_RoomInfo.playerResults[i].facebookId);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(m_RoomInfo.playerResults[i].country))
                {
                    sFlagP2.spriteName = m_RoomInfo.playerResults[i].country.ToLower();
                }
                else
                {
                    sFlagP2.spriteName = "us";
                }
                if (CachePvp.GetVipFromVipPoint(m_RoomInfo.playerResults[i].vipPoint) == 0)
                {
                    sVipIconP2.gameObject.SetActive(false);
                }
                else
                {
                    sVipIconP2.gameObject.SetActive(true);
                    sVipIconP2.spriteName = "Icon__VIP_" + (CachePvp.GetVipFromVipPoint(m_RoomInfo.playerResults[i].vipPoint) - 1);
                }
                lNameP2.text = m_RoomInfo.playerResults[i].name;
                lScoreP2.text = GameContext.FormatNumber(m_RoomInfo.playerResults[i].score);
                int rank = CachePvp.dictionaryRank[m_RoomInfo.playerResults[i].playerLevel.code];
                MedalRankTitle rankTitle = (MedalRankTitle)(rank);
                lRankP2.text = rankTitle + " " + CachePvp.ConvertChina(m_RoomInfo.playerResults[i].playerLevel.name);
                sRankP2.spriteName = "PVP_rank_" + rank;
                sRankP2.MakePixelPerfect();
                sRankP2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                scoreP2 = m_RoomInfo.playerResults[i].score;
                if (m_RoomInfo.state == RoomInfo.END)
                {
                    gDraw.SetActive(false);
                    if (m_RoomInfo.playerResults[i].result == PlayerResult.WIN)
                    {
                        gWinP2.SetActive(true);
                        gQuitP2.SetActive(false);
                    }
                    else if (m_RoomInfo.playerResults[i].result == PlayerResult.LOSE)
                    {
                        gWinP2.SetActive(false);
                        if (m_RoomInfo.playerResults[i].state == PlayerResult.OUT)
                        {
                            gQuitP2.SetActive(true);
                        }
                    }
                }
                if (m_RoomInfo.playerResults[i].listRankPlane != null)
                {
                    AircraftTypeEnum aircraftType = (AircraftTypeEnum)m_RoomInfo.playerResults[i].selectedShip;
                    if (m_RoomInfo.playerResults[i].listRankPlane.Count > m_RoomInfo.playerResults[i].selectedShip - 1)
                    {
                        sShipP2.spriteName = "aircraft" + (int)aircraftType + "_e" + m_RoomInfo.playerResults[i].listRankPlane[m_RoomInfo.playerResults[i].selectedShip - 1] + "_idle_0";
                    }
                    else
                    {
                        Array airCrafts = Enum.GetValues(typeof(AircraftTypeEnum));
                        sShipP2.spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
                    }
                }
                else
                {
                    AircraftTypeEnum aircraftType = (AircraftTypeEnum)m_RoomInfo.playerResults[i].spaceship;
                    Array airCrafts = Enum.GetValues(typeof(AircraftTypeEnum));
                    sShipP2.spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
                }
                sShipP2.MakePixelPerfect();
                sShipP2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            }
        }
        UpdateStatusHealthBar();
    }

    public void SetData2v2(CoopPvPRoomData ri, int oldIndex = -1)
    {
        is1v1 = false;
        pvp1v1.SetActive(false);
        pvp2v2.SetActive(true);
        gameObject.SetActive(true);
        m_RoomInfo2v2 = ri;
        lTimeRemaining2v2.text = (m_RoomInfo2v2.remainTime / 60).ToString("00") + ":" + (m_RoomInfo2v2.remainTime % 60).ToString("00");
        for (int i = 0; i < listQuit2v2.Count; i++)
        {
            listQuit2v2[i].SetActive(false);
        }
        gWinTeam1.SetActive(false);
        gWinTeam2.SetActive(false);
        gDraw2v2.SetActive(false);
        if (m_RoomInfo2v2.state == CoopPvPRoomData.END)
        {
            lTimeRemaining.text = "End";
        }
        listFacebookID2v2.Clear();
        listCode2v2.Clear();
        for (int i = 0; i < m_RoomInfo2v2.teams.Count; i++)
        {
            if (i == 0)
            {
                lScoreTeam1.text = GameContext.FormatNumber(m_RoomInfo2v2.teams[i].score);
                scoreTeam1 = m_RoomInfo2v2.teams[i].score;
            }
            else if (i == 1)
            {
                lScoreTeam2.text = GameContext.FormatNumber(m_RoomInfo2v2.teams[i].score);
                scoreTeam2 = m_RoomInfo2v2.teams[i].score;
            }
            for (int j = 0; j < m_RoomInfo2v2.teams[i].members.Count; j++)
            {
                listAvatar2v2[i * 2 + j].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                listCode2v2.Add(m_RoomInfo2v2.teams[i].members[j].player.code);
                bool showImageFB = true;
                if (GameContext.IS_CHINA_VERSION)
                {
                    if (!string.IsNullOrEmpty(m_RoomInfo2v2.teams[i].members[j].player.appCenterId))
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(m_RoomInfo2v2.teams[i].members[j].player.appCenterId))
                        {
                            showImageFB = false;
                            listFacebookID2v2.Add(m_RoomInfo2v2.teams[i].members[j].player.appCenterId);
                            listAvatar2v2[i * 2 + j].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[m_RoomInfo2v2.teams[i].members[j].player.appCenterId];
                        }
                        else
                        {
                            Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(m_RoomInfo2v2.teams[i].members[j].player.appCenterId);
                            if (t != null)
                            {
                                showImageFB = false;
                                listFacebookID2v2.Add(m_RoomInfo2v2.teams[i].members[j].player.appCenterId);
                                CacheAvatarManager.Instance.DownloadImageFormUrl("", m_RoomInfo2v2.teams[i].members[j].player.appCenterId, t);
                            }
                        }
                    }
                    else
                    {
                        listFacebookID2v2.Add("");
                    }
                }
                if (showImageFB)
                {
                    if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(m_RoomInfo2v2.teams[i].members[j].player.facebookId))
                    {
                        listFacebookID2v2.Add(m_RoomInfo2v2.teams[i].members[j].player.facebookId);
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(m_RoomInfo2v2.teams[i].members[j].player.facebookId))
                        {
                            listAvatar2v2[i * 2 + j].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[m_RoomInfo2v2.teams[i].members[j].player.facebookId];
                        }
                        else
                        {
                            string query = "https://graph.facebook.com/" + m_RoomInfo2v2.teams[i].members[j].player.facebookId + "/picture?type=small&width=135&height=135";
                            CacheAvatarManager.Instance.DownloadImageFormUrl(query, m_RoomInfo2v2.teams[i].members[j].player.facebookId);
                        }
                    }
                    else
                    {
                        listFacebookID2v2.Add("");
                    }
                }
                if (!string.IsNullOrEmpty(m_RoomInfo2v2.teams[i].members[j].player.country))
                {
                    listFlag2v2[i * 2 + j].spriteName = m_RoomInfo2v2.teams[i].members[j].player.country.ToLower();
                }
                else
                {
                    listFlag2v2[i * 2 + j].spriteName = "us";
                }
                if (CachePvp.GetVipFromVipPoint(m_RoomInfo2v2.teams[i].members[j].player.vip) == 0)
                {
                    listVipIcon2v2[i * 2 + j].gameObject.SetActive(false);
                }
                else
                {
                    listVipIcon2v2[i * 2 + j].gameObject.SetActive(true);
                    listVipIcon2v2[i * 2 + j].spriteName = "Icon__VIP_" + (CachePvp.GetVipFromVipPoint(m_RoomInfo2v2.teams[i].members[j].player.vip) - 1);
                }
                if (string.IsNullOrEmpty(m_RoomInfo2v2.teams[i].members[j].player.name))
                {
                    listName2v2[i * 2 + j].text = m_RoomInfo2v2.teams[i].members[j].player.code;
                }
                else
                {
                    listName2v2[i * 2 + j].text = m_RoomInfo2v2.teams[i].members[j].player.name;
                }
                if (m_RoomInfo2v2.teams[i].members[j].state == CoopPvPPlayerData.STATE_OUT)
                {
                    listQuit2v2[i * 2 + j].SetActive(true);
                }
                if (m_RoomInfo2v2.state == CoopPvPRoomData.END)
                {
                    gDraw2v2.SetActive(false);
                    if (m_RoomInfo2v2.teams[0].result == CoopPvPTeamData.WIN)
                    {
                        gWinTeam1.SetActive(true);
                        gWinTeam2.SetActive(false);
                    }
                    else if (m_RoomInfo2v2.teams[1].result == CoopPvPTeamData.WIN)
                    {
                        gWinTeam1.SetActive(false);
                        gWinTeam2.SetActive(true);
                    }
                    else
                    {
                        gDraw2v2.SetActive(true);
                        gWinTeam1.SetActive(false);
                        gWinTeam2.SetActive(false);
                    }
                }

                PlayerDataUtil.PlayerProfileData profile = PlayerDataUtil.StringToObject(m_RoomInfo2v2.teams[i].members[j].player.data);
                if (profile.listRankPlane != null)
                {
                    int selectedShip = CachePvp.HighestShip(profile);
                    AircraftTypeEnum aircraftType = (AircraftTypeEnum)selectedShip;
                    if (profile.listRankPlane.Length > profile.selectedShip - 1)
                    {
                        listShip2v2[i * 2 + j].spriteName = "aircraft" + (int)aircraftType + "_e" + profile.listRankPlane[selectedShip - 1] + "_idle_0";
                    }
                    else
                    {
                        listShip2v2[i * 2 + j].spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
                    }
                }
                else
                {
                    AircraftTypeEnum aircraftType = (AircraftTypeEnum)profile.selectedShip;
                    listShip2v2[i * 2 + j].spriteName = "aircraft" + (int)aircraftType + "_e" + AircraftSheet.Get((int)aircraftType).begin_rank + "_idle_0";
                }
                listShip2v2[i * 2 + j].MakePixelPerfect();
                listShip2v2[i * 2 + j].transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            }
        }
        UpdateStatusHealthBar();
    }

    void UpdateStatusHealthBar()
    {
        if (scoreP1 > scoreP2)
        {
            int sum = scoreP1 + scoreP2;
            int myPercent = Mathf.RoundToInt(scoreP1 * 240f / sum - 120);
            sHealthBarP1.width = 303 + myPercent;// Mathf.Min(409, )
            sHealthBarP1.spriteName = HEALTH_BAR_BLUE_2;
            sHealthBarP1.depth = 11;
            sHealthBarP1.color = Color.white;
            sHealthBarP2.width = 303;
            sHealthBarP2.spriteName = HEALTH_BAR_RED_1;
            sHealthBarP2.depth = 10;
            sHealthBarP2.color = colorLose;
        }
        else if (scoreP1 < scoreP2)
        {
            int sum = scoreP1 + scoreP2;
            int myPercent = Mathf.RoundToInt(scoreP2 * 240f / sum - 120);
            sHealthBarP1.width = 303;
            sHealthBarP1.spriteName = HEALTH_BAR_BLUE_1;
            sHealthBarP1.depth = 10;
            sHealthBarP1.color = colorLose;
            sHealthBarP2.width = 303 + myPercent;// Mathf.Min(303, 423)
            sHealthBarP2.spriteName = HEALTH_BAR_RED_2;
            sHealthBarP2.depth = 11;
            sHealthBarP2.color = Color.white;
        }
        else //equals
        {
            sHealthBarP1.width = 296;// Mathf.Min(409, )
            sHealthBarP1.spriteName = HEALTH_BAR_BLUE_1;
            sHealthBarP1.depth = 10;
            sHealthBarP1.color = Color.white;
            sHealthBarP2.width = 296;
            sHealthBarP2.spriteName = HEALTH_BAR_RED_1;
            sHealthBarP2.depth = 10;
            sHealthBarP2.color = Color.white;
        }

        if (scoreTeam1 > scoreTeam2)
        {
            int sum = scoreTeam1 + scoreTeam2;
            int myPercent = Mathf.RoundToInt(scoreTeam1 * 310f / sum - 155);
            sHealthBarTeam1.width = 296 + myPercent;// Mathf.Min(296, 451)
            sHealthBarTeam1.spriteName = HEALTH_BAR_BLUE_2;
            sHealthBarTeam1.depth = 11;
            sHealthBarTeam1.color = Color.white;
            sHealthBarTeam2.width = 303;
            sHealthBarTeam2.spriteName = HEALTH_BAR_RED_1;
            sHealthBarTeam2.depth = 10;
            sHealthBarTeam2.color = colorLose;
        }
        else if (scoreTeam1 < scoreTeam2)
        {
            int sum = scoreTeam1 + scoreTeam2;
            int myPercent = Mathf.RoundToInt(scoreTeam2 * 310f / sum - 155);
            sHealthBarTeam1.width = 303;
            sHealthBarTeam1.spriteName = HEALTH_BAR_BLUE_1;
            sHealthBarTeam1.depth = 10;
            sHealthBarTeam1.color = colorLose;
            sHealthBarTeam2.width = 296 + myPercent;// Mathf.Min(303, 423)
            sHealthBarTeam2.spriteName = HEALTH_BAR_RED_2;
            sHealthBarTeam2.depth = 11;
            sHealthBarTeam2.color = Color.white;
        }
        else //equals
        {
            sHealthBarTeam1.width = 296;// Mathf.Min(409, )
            sHealthBarTeam1.spriteName = HEALTH_BAR_BLUE_1;
            sHealthBarTeam1.depth = 10;
            sHealthBarTeam1.color = Color.white;
            sHealthBarTeam2.width = 296;
            sHealthBarTeam2.spriteName = HEALTH_BAR_RED_1;
            sHealthBarTeam2.depth = 10;
            sHealthBarTeam2.color = Color.white;
        }
    }

    void CheckVisibilty()
    {
        bool currentVisibilty = false;
        if (is1v1)
        {
            currentVisibilty = panel.IsVisible(sBackground);
        }
        else
        {
            currentVisibilty = panel.IsVisible(sBackground2v2);
        }
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }
}