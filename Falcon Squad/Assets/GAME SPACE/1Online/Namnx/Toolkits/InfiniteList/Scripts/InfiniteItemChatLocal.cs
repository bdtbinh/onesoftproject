﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using TCore;
using System.Globalization;

public class InfiniteItemChatLocal : MonoBehaviour
{
    public GameObject gChat;
    public BoxCollider boxCollider;
    public UISprite sBackground;
    public UI2DSprite sAvatar;
    public GameObject smallFrame;
    public UILabel lText;
    public UISprite sEmoji;
    public UILabel lName;

    public GameObject gOptionChat;
    public GameObject gOptionChatNoClan;
    public GameObject gOptionChat_v;
    public GameObject gOptionChat_vNoClan;

    public InfiniteListChatLocalPopulator listPopulator;

    public UIPanel panel;
    public int itemNumber;
    public int itemDataIndex;

    private bool isVisible = true;
    private const string CHAT_ME = "Clanchat_Text_BG2";
    private const string CHAT_OTHER = "Clanchat_Text_BG1";

    private const string REQUEST_ME = "BG_Clan_Item_request_1";
    private const string REQUEST_OTHER = "BG_Clan_Item_request_2";

    private string facebookID = "";

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (!string.IsNullOrEmpty(facebookID))
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookID))
            {
                if (sAvatar != null)
                {
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookID];
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1); // some weird scaling issues with NGUI
    }

    public void ClickAvatar()
    {
        if (CachePvp.TypeMemberClan == CachePvp.HAS_NO_CLAN)
        {
            if (!gOptionChatNoClan.activeInHierarchy)
            {
                listPopulator.HideOptionChat();
                if (itemNumber == 0)
                {
                    gOptionChat_vNoClan.SetActive(true);
                }
                else
                {
                    gOptionChatNoClan.SetActive(true);
                }
            }
        }
        else
        {
            if (!gOptionChat.activeInHierarchy)
            {
                listPopulator.HideOptionChat();
                if (itemNumber == 0)
                {
                    gOptionChat_v.SetActive(true);
                }
                else
                {
                    gOptionChat.SetActive(true);
                }
            }
        }
    }

    bool isInvite;

    public void ClickInviteToTeam()
    {
        CachePvp.isInvite = true;
        new CSPlayerInfo(m_Chat.code).Send();
    }

    public void ClickView()
    {
        CachePvp.isInvite = false;
        new CSPlayerInfo(m_Chat.code).Send();
    }

    public void ClickDrag()
    {
        listPopulator.HideOptionChat();
    }

    public bool verifyVisibility()
    {
        return (panel.IsVisible(sBackground));
    }

    void Update()
    {
        if (Mathf.Abs(listPopulator.draggablePanel.currentMomentum.y) > 0)
        {
            CheckVisibilty();
        }
    }

    public Chat m_Chat;

    public void SetData(Chat mc, int oldIndex = -1)
    {
        gOptionChat.SetActive(false);
        gOptionChatNoClan.SetActive(false);
        gOptionChat_v.SetActive(false);
        gOptionChat_vNoClan.SetActive(false);

        gameObject.SetActive(true);
        transform.localPosition = new Vector3(-10000, transform.localPosition.y, transform.localPosition.z);
        m_Chat = mc;
        lText.text = mc.content;
        sAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(mc.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(mc.appCenterId))
                {
                    showImageFB = false;
                    facebookID = mc.appCenterId;
                    sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[mc.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(mc.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        facebookID = mc.appCenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", mc.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (!string.IsNullOrEmpty(mc.facebookId))
            {
                facebookID = mc.facebookId;
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(mc.facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(mc.facebookId))
                    {
                        sAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[mc.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + mc.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, mc.facebookId);
                    }
                }
            }
        }
        if (listPopulator.itemsPoolChatLocalChat.ContainsKey(itemDataIndex))
        {
            listPopulator.itemsPoolChatLocalChat[itemDataIndex] = mc;
        }
        else
        {
            listPopulator.itemsPoolChatLocalChat.Add(itemDataIndex, mc);
        }
        StartCoroutine(Wait1Frame());
    }
    int typeChat = 0; //0 : lien nhau, 1 : lien nhau cach thoi gian, 2 : cach nhau

    IEnumerator Wait1Frame()
    {
        yield return null;

        if (m_Chat.contentType == Chat.CONTENT_TYPE_CHAT)
        {
            gChat.SetActive(true);
            int h;
            if (m_Chat.content.StartsWith("/Emo1_"))
            {
                h = 76;
                sEmoji.gameObject.SetActive(true);
                lText.gameObject.SetActive(false);
                if (m_Chat.content.Length == 7)
                {
                    sEmoji.spriteName = m_Chat.content.Remove(0, 1);
                }
            }
            else
            {
                h = lText.height;
                sEmoji.gameObject.SetActive(false);
                lText.gameObject.SetActive(true);
            }
            //StartCoroutine(Wait1Frame());
            if (m_Chat.code.Equals(CachePvp.Code))
            {
                if (itemDataIndex > 0)
                {
                    Chat sc = listPopulator.itemsPoolChatLocalChat[itemDataIndex - 1];
                    if (sc.code != null)
                    {
                        if (sc.code.Equals(m_Chat.code))
                        {
                            lName.height = 2;
                            lName.text = "";
                            if (sc.time > m_Chat.time - 45000 && sc.contentType == Chat.CONTENT_TYPE_CHAT)
                            {
                                lName.height = 2;
                                typeChat = 0;
                            }
                            else
                            {
                                lName.height = 20;
                                DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                double dou = m_Chat.time;
                                DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                                lName.text = m_dateTime.ToString("dd-MM-yyyy hh:mm");
                                typeChat = 1;
                            }
                        }
                        else
                        {
                            lName.height = 20;
                            DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                            double dou = m_Chat.time;
                            DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                            lName.text = m_dateTime.ToString("dd-MM-yyyy hh:mm");
                            typeChat = 2;
                        }
                    }
                    else
                    {
                        lName.height = 20;
                        DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        double dou = m_Chat.time;
                        DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                        lName.text = m_dateTime.ToString("dd-MM-yyyy hh:mm");
                        typeChat = 1;
                    }
                }
                else
                {
                    lName.height = 20;
                    DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    double dou = m_Chat.time;
                    DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                    lName.text = m_dateTime.ToString("dd-MM-yyyy hh:mm");
                }
                lName.alignment = NGUIText.Alignment.Right;
                lName.pivot = UIWidget.Pivot.TopRight;
                lName.transform.localPosition = new Vector3(317, lName.transform.localPosition.y, lName.transform.localPosition.z);
                sBackground.spriteName = CHAT_ME;
                smallFrame.SetActive(false);
                if (m_Chat.content.StartsWith("/Emo1_"))
                {
                    sEmoji.pivot = UIWidget.Pivot.TopRight;
                    sEmoji.transform.localPosition = new Vector3(317, sEmoji.transform.localPosition.y, sEmoji.transform.localPosition.z);
                }
                else
                {
                    lText.pivot = UIWidget.Pivot.TopRight;
                    lText.transform.localPosition = new Vector3(317, lText.transform.localPosition.y, lText.transform.localPosition.z);
                    sBackground.pivot = UIWidget.Pivot.TopRight;
                    sBackground.transform.localPosition = new Vector3(10, sBackground.transform.localPosition.y, sBackground.transform.localPosition.z);
                    sBackground.width = lText.width + 20;
                }
                sBackground.height = h + 20;
                if (lName.height == 20)
                {
                    boxCollider.size = new Vector3(boxCollider.size.x, h + 44 + 20, boxCollider.size.z);
                    boxCollider.center = new Vector3(boxCollider.center.x, (140 - boxCollider.size.y) / 2f - 20, boxCollider.center.z);
                }
                else
                {
                    boxCollider.size = new Vector3(boxCollider.size.x, sBackground.height - 4 + listPopulator.padding + 20, boxCollider.size.z);
                    boxCollider.center = new Vector3(boxCollider.center.x, (140 - boxCollider.size.y) / 2f - 20, boxCollider.center.z);
                }
                listPopulator.itemsPoolChatLocalHeight[itemDataIndex] = boxCollider.size.y - 20;
            }
            else
            {
                lName.text = string.IsNullOrEmpty(m_Chat.name) ? m_Chat.code : m_Chat.name;
                DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                double dou = m_Chat.time;
                DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
                lName.text += ", " + m_dateTime.ToString("dd-MM-yyyy hh:mm");
                if (itemDataIndex > 0)
                {
                    Chat sc = listPopulator.itemsPoolChatLocalChat[itemDataIndex - 1];
                    if (sc.code != null)
                    {
                        if (sc.code.Equals(m_Chat.code))
                        {
                            if (sc.time > m_Chat.time - 45000 && sc.contentType == Chat.CONTENT_TYPE_CHAT)
                            {
                                typeChat = 0;
                                lName.height = 2;
                                smallFrame.SetActive(false);
                            }
                            else
                            {
                                typeChat = 1;
                                lName.height = 20;
                                smallFrame.SetActive(true);
                            }
                        }
                        else
                        {
                            if (sc.code.Equals(CachePvp.Code))
                            {
                                typeChat = 2;
                                lName.height = 20;
                                smallFrame.SetActive(true);
                            }
                            else
                            {
                                typeChat = 1;
                                lName.height = 20;
                                smallFrame.SetActive(true);
                            }
                        }
                    }
                    else
                    {
                        typeChat = 1;
                        lName.height = 20;
                        smallFrame.SetActive(true);
                    }
                }
                else
                {
                    lName.height = 20;
                    smallFrame.SetActive(true);
                }
                lName.alignment = NGUIText.Alignment.Left;
                lName.pivot = UIWidget.Pivot.TopLeft;
                lName.transform.localPosition = new Vector3(-211, lName.transform.localPosition.y, lName.transform.localPosition.z);
                sBackground.spriteName = CHAT_OTHER;
                if (m_Chat.content.StartsWith("/Emo1_"))
                {
                    sEmoji.pivot = UIWidget.Pivot.TopLeft;
                    sEmoji.transform.localPosition = new Vector3(-211, sEmoji.transform.localPosition.y, sEmoji.transform.localPosition.z);
                }
                else
                {
                    lText.pivot = UIWidget.Pivot.TopLeft;
                    lText.transform.localPosition = new Vector3(-201, lText.transform.localPosition.y, lText.transform.localPosition.z);
                    sBackground.pivot = UIWidget.Pivot.TopLeft;
                    sBackground.transform.localPosition = new Vector3(-10, sBackground.transform.localPosition.y, sBackground.transform.localPosition.z);
                    sBackground.width = lText.width + 20;
                }
                sBackground.height = h + 20;
                if (lName.height == 20)
                {
                    boxCollider.size = new Vector3(boxCollider.size.x, h + 44 + 20, boxCollider.size.z);
                    boxCollider.center = new Vector3(boxCollider.center.x, (140 - boxCollider.size.y) / 2f - 20, boxCollider.center.z);
                }
                else
                {
                    boxCollider.size = new Vector3(boxCollider.size.x, sBackground.height + listPopulator.padding - 4 + 20, boxCollider.size.z);
                    boxCollider.center = new Vector3(boxCollider.center.x, (140 - boxCollider.size.y) / 2f - 20, boxCollider.center.z);
                }
                listPopulator.itemsPoolChatLocalHeight[itemDataIndex] = boxCollider.size.y - 20;
            }
        }
        Reposition();
    }

    public void Reposition()
    {
        if (listPopulator.itemsPoolChatLocalPositionY.ContainsKey(itemDataIndex - 1))
        {
            float posY = listPopulator.itemsPoolChatLocalPositionY[itemDataIndex - 1] - listPopulator.itemsPoolChatLocalHeight[itemDataIndex - 1];
            if (lName.height == 20)
            {
                if (m_Chat.code.Equals(CachePvp.Code))
                {
                    posY -= 10;
                }
                else
                {
                    if (typeChat == 1)
                    {
                        posY -= 20;
                    }
                    else
                    {
                        posY -= 10;
                    }
                }
            }
            transform.localPosition = new Vector3(0, posY, 0);
            listPopulator.itemsPoolChatLocalPositionY[itemDataIndex] = posY;
        }
        else if (itemDataIndex == 0)
        {
            transform.localPosition = new Vector3(0, -35, 0);
            listPopulator.itemsPoolChatLocalPositionY[itemDataIndex] = -35;
        }
        transform.localPosition = new Vector3(0, transform.localPosition.y, transform.localPosition.z);
    }

    void CheckVisibilty()
    {
        bool currentVisibilty = panel.IsVisible(sBackground);
        if (currentVisibilty != isVisible)
        {
            isVisible = currentVisibilty;
            if (!isVisible)
            {
                StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
            }
        }
    }

    float oldPositionY;

    public void RepositionAfterAdd()
    {
        bool currentVisibilty = panel.IsVisible(sBackground);
        isVisible = currentVisibilty;
        if (!isVisible)
        {
            StartCoroutine(listPopulator.ItemIsInvisible(itemNumber));
        }
    }
}