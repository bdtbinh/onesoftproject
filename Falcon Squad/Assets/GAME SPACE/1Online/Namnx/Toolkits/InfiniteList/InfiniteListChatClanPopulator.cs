﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.ootii.Messages;
using Mp.Pvp;
using System.Linq;
using System;

public class InfiniteListChatClanPopulator : MonoBehaviour
{
    // events to listen to if needed...
    public delegate void InfiniteItemIsPressed(int itemDataIndex, bool isDown);

    public event InfiniteItemIsPressed InfiniteItemIsPressedEvent;

    public delegate void InfiniteItemIsClicked(int itemDataIndex);

    public event InfiniteItemIsClicked InfiniteItemIsClickedEvent;

    public PopupClan popupClan;
    public UIScrollBar scrollBarChatClan;

    public bool enableLog = true;
    //Prefabs
    const string listItemTag = "listItem";
    const string listSectionTag = "listSection";
    public Transform itemPrefab;
    public Transform sectionPrefab;
    // NGUI Controllers
    public UITable table;
    public UIScrollView draggablePanel;
    //scroll indicator
    public Transform scrollIndicator;
    private int scrollCursor = 0;
    // pool
    public float cellHeight = 94f;
    public float padding = 10;
    // at the moment we support fixed height... insert here or measure it
    public int poolSize = 6;
    public List<Transform> itemsPool = new List<Transform>();
    public List<InfiniteItemChatClan> itemsPoolChatClan = new List<InfiniteItemChatClan>();
    public Dictionary<int, float> itemsPoolChatClanHeight = new Dictionary<int, float>();
    public Dictionary<int, bool> itemsPoolChatClanHeightAdded = new Dictionary<int, bool>();
    public Dictionary<int, float> itemsPoolChatClanPositionY = new Dictionary<int, float>();
    public Dictionary<int, Chat> itemsPoolChatClanChat = new Dictionary<int, Chat>();
    private int extraBuffer = 10;
    private int startIndex = 0;
    // where to start
    private Hashtable dataTracker = new Hashtable();
    // hashtable to keep track of what is being displayed by the pool


    //our data...using arraylist for generic types... if you want specific types just refactor it to List<T> where T is the type
    private List<Chat> originalData = new List<Chat>();
    private List<Chat> dataList = new List<Chat>();
    //our sections
    private int numberOfSections = 0;
    private List<int> sectionsIndices = new List<int>();

    #region Start & Update for MonoBehaviour

    List<Chat> listChatClan = new List<Chat>();

    void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.Clan.LoadedChat.ToString(), LoadedChat, true);
        MessageDispatcher.AddListener(EventName.Clan.InputChatSubmitClan.ToString(), OnInputChatSubmit, true);
        MessageDispatcher.AddListener(EventName.Clan.DonateNotify.ToString(), OnDonateNotify, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.Clan.LoadedChat.ToString(), LoadedChat, true);
        MessageDispatcher.RemoveListener(EventName.Clan.InputChatSubmitClan.ToString(), OnInputChatSubmit, true);
        MessageDispatcher.RemoveListener(EventName.Clan.DonateNotify.ToString(), OnDonateNotify, true);
    }

    private void OnDonateNotify(IMessage rMessage)
    {
        SCDonateNotify e = rMessage.Data as SCDonateNotify;
        if (e.action == SCDonateNotify.ACTION_INSERT)
        {
            Chat mc = e.data;
            oldScrollBarValue = scrollBarChatClan.value;
            listChatClan.Add(mc);
            dataList = new List<Chat>(listChatClan);
            for (int i = dataList.Count - 1; i < dataList.Count; i++)
            {
                int id = -1;
                for (int j = 0; j < itemsPool.Count; j++)
                {
                    if (!itemsPool[j].gameObject.activeInHierarchy)
                    {
                        itemsPoolChatClanHeight[j] = cellHeight + padding;
                        itemsPoolChatClanPositionY[j] = 0;
                        id = j;
                        InitListItemWithIndex(itemsPool[j], j, j);
                        break;
                    }
                }
                if (id == -1)
                {
                    int j = poolSize;
                    while (itemsPoolChatClanHeight.ContainsKey(j))
                    {
                        j++;
                    }
                    itemsPoolChatClanHeight.Add(j, cellHeight + padding);
                    itemsPoolChatClanHeightAdded.Add(j, false);
                    itemsPoolChatClanPositionY.Add(j, 0);
                }
            }

            for (int i = 0; i < itemsPoolChatClan.Count; i++)
            {
                if (itemsPoolChatClan[i].gameObject.activeInHierarchy)
                {
                    itemsPoolChatClan[i].Reposition();
                    itemsPoolChatClan[i].RepositionAfterAdd();
                }
            }
            StartCoroutine(Wait1Frame(mc));
        }
        else if (e.action == SCDonateNotify.ACTION_UPDATE || e.action == SCDonateNotify.ACTION_DELETE)
        {
            for (int i = 0; i < listChatClan.Count; i++)
            {
                if (listChatClan[i].id.Equals(e.data.id))
                {
                    if (e.action == SCDonateNotify.ACTION_DELETE)
                    {
                        e.data.isDeleted = true;
                    }
                    else
                    {
                        e.data.isDeleted = false;
                    }
                    listChatClan[i] = e.data;
                }
            }

            for (int i = 0; i < itemsPoolChatClan.Count; i++)
            {
                if (itemsPoolChatClan[i].m_Chat != null && itemsPoolChatClan[i].m_Chat.id.Equals(e.data.id))
                {
                    itemsPoolChatClan[i].SetData(e.data);
                }
            }
        }
    }

    float oldScrollBarValue;

    private void OnInputChatSubmit(IMessage rMessage)
    {
        SCClanChat mc = rMessage.Data as SCClanChat;
        if (mc.chat.type == CSClanChat.CHAT_CLAN)
        {
            oldScrollBarValue = scrollBarChatClan.value;
            listChatClan.Add(mc.chat);
            dataList = new List<Chat>(listChatClan);
            for (int i = dataList.Count - 1; i < dataList.Count; i++)
            {
                int id = -1;

                for (int j = 0; j < itemsPool.Count; j++)
                {
                    if (!itemsPool[j].gameObject.activeInHierarchy)
                    {
                        itemsPoolChatClanHeight[j] = cellHeight + padding;
                        itemsPoolChatClanPositionY[j] = 0;
                        id = j;
                        InitListItemWithIndex(itemsPool[j], j, j);
                        break;
                    }
                }
                if (id == -1)
                {
                    int j = poolSize;
                    while (itemsPoolChatClanHeight.ContainsKey(j))
                    {
                        j++;
                    }
                    itemsPoolChatClanHeight.Add(j, cellHeight + padding);
                    itemsPoolChatClanHeightAdded.Add(j, false);
                    itemsPoolChatClanPositionY.Add(j, 0);
                }
            }

            for (int i = 0; i < itemsPoolChatClan.Count; i++)
            {
                if (itemsPoolChatClan[i].gameObject.activeInHierarchy)
                {
                    itemsPoolChatClan[i].Reposition();
                    itemsPoolChatClan[i].RepositionAfterAdd();
                }
            }
            StartCoroutine(Wait1Frame(mc.chat));
        }
    }

    private IEnumerator Wait1Frame(Chat chat)
    {
        //yield return delay;
        yield return null;
        yield return null;
        if (listChatClan.Count == 1)
        {
            draggablePanel.ResetPosition();
        }
        yield return null;
        yield return null;
        if (listChatClan.Count == 1)
        {
            draggablePanel.ResetPosition();
        }
        if (listChatClan.Count != 1)
        {
            if (chat.code != null && chat.code.Equals(CachePvp.Code))
            {
                if (oldScrollBarValue >= 0.7f)
                {
                    draggablePanel.UpdateScrollbars(true);
                    if (scrollBarChatClan.barSize < 0.98f)
                    {
                        scrollBarChatClan.value = 1f;
                    }
                }
                if (scrollBarChatClan.barSize >= 0.98f)
                {
                    scrollBarChatClan.value = 0;
                }
            }
            else
            {
                if (oldScrollBarValue >= 0.95f)
                {
                    draggablePanel.UpdateScrollbars(true);
                    if (scrollBarChatClan.barSize < 0.98f)
                    {
                        scrollBarChatClan.value = 1f;
                    }
                }
                if (scrollBarChatClan.barSize >= 0.98f)
                {
                    scrollBarChatClan.value = 0;
                }
            }
        }
        //Debug.Log("listChatClan.Count : " + listChatClan.Count);
        //if (listChatClan.Count == 1)
        //{
        //    yield return new WaitForSeconds(0.3f);
        //    draggablePanel.ResetPosition();
        //}
    }

    public InfiniteItemChatClan GetItemByDataIndex(int dataIndex)
    {
        for (int i = 0; i < itemsPoolChatClan.Count; i++)
        {
            if (itemsPoolChatClan[i].itemDataIndex == dataIndex)
            {
                return itemsPoolChatClan[i];
            }
        }
        return null;
    }

    void LoadedChat(IMessage msg)
    {
        int type = (int)msg.Recipient;
        if (type == CSClanJoinChat.TYPE_JOIN_ROOM_CLAN)
        {
            List<Chat> list = msg.Data as List<Chat>;
            listChatClan.Clear();
            MessageDispatcher.SendMessage(gameObject, EventName.Clan.ShowButtonRequestChat.ToString(), 0, 0);
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].contentType == Chat.CONTENT_TYPE_DONATE)
                {
                    ClanDonateData donateData = JsonFx.Json.JsonReader.Deserialize<ClanDonateData>(list[i].content);
                    if (list[i].code.Equals(CachePvp.Code) && donateData.received == donateData.maxCanReceive)
                    {
                        MessageDispatcher.SendMessage(gameObject, EventName.Clan.ShowButtonRequestChat.ToString(), (int)donateData.remainTime, 0);
                    }
                    if (donateData.received == donateData.maxCanReceive)
                    {
                        continue;
                    }
                }
                listChatClan.Add(list[i]);
            }
            CachePvp.topChat = 0;
            CachePvp.bottomChat = 0;

            poolSize = 100;
            InitTableView(listChatClan, null, 0);
        }
    }

    void Start()
    {
        // check prefabs
        if (itemPrefab == null)
            Debug.LogError("InfiniteListPopulator:: itemPrefab is not assigned");
        else if (!itemPrefab.tag.Equals(listItemTag))
            Debug.LogError("InfiniteListPopulator:: itemPrefab tag should be " + listItemTag);
        if (sectionPrefab == null)
            Debug.LogError("InfiniteListPopulator:: sectionPrefab is not assigned");
        else if (!sectionPrefab.tag.Equals(listSectionTag))
            Debug.LogError("InfiniteListPopulator:: sectionPrefab tag should be " + listSectionTag);
    }
    #endregion

    #region Infinite List Data Sources calls

    /*
     * These methods are used mainly to populate 
     * the data for both sections and rows.. 
     * change to suit your implementation
     * 
     * */

    string GetTitleForSection(int i)
    {
        return "Section " + i;
    }

    void PopulateListSectionWithIndex(Transform item, int index)
    {
        item.GetComponent<InfiniteSectionChatClan>().label.text = GetTitleForSection(index);
    }

    void PopulateListItemWithIndex(Transform item, int dataIndex, int oldIndex = -1)
    {
        if (listChatClan.Count > dataIndex)
        {
            item.GetComponent<InfiniteItemChatClan>().SetData(listChatClan[dataIndex], oldIndex);
        }
    }

    #endregion

    #region Infinite List Management & scrolling

    // set then call InitTableView
    public void SetStartIndex(int inStartIndex)
    {
        startIndex = GetJumpIndexForItem(inStartIndex);
    }

    public void SetOriginalData(List<Chat> inDataList)
    {
        originalData = new List<Chat>(inDataList);
    }

    public void SetSectionIndices(List<int> inSectionsIndices)
    {
        numberOfSections = inSectionsIndices.Count;
        sectionsIndices = inSectionsIndices;
    }
    // call to refresh without changing sections.. e.g. jump to specific point...
    public void RefreshTableView()
    {
        if (enableLog)
        {
            if (originalData == null || originalData.Count == 0)
                Debug.LogWarning("InfiniteListPopulator.InitTableView() trying to refresh with no data");
        }
        InitTableView(originalData, sectionsIndices, startIndex);
    }

    public void InitTableView(List<Chat> inDataList, List<int> inSectionsIndices, int inStartIndex)
    {
        InitTableViewImp(inDataList, inSectionsIndices, inStartIndex);

    }

    #endregion

    #region The private stuff... ideally you shouldn't need to call or change things directly from this region onwards

    void InitTableViewImp(List<Chat> inDataList, List<int> inSectionsIndices, int inStartIndex)
    {
        RefreshPool();
        //hien button request
        startIndex = inStartIndex;
        scrollCursor = inStartIndex;
        dataTracker.Clear();
        originalData = new List<Chat>(inDataList);
        dataList = new List<Chat>(inDataList);
        if (inSectionsIndices != null)
        {
            numberOfSections = inSectionsIndices.Count;
            sectionsIndices = inSectionsIndices;
        }
        else
        {
            numberOfSections = 0;
            sectionsIndices = new List<int>();
        }
        // do we have a section? then inject 'special' data inside the date list
        if (numberOfSections > 0)
        {

            for (int i = 0; i < numberOfSections; i++)
            {
                sectionsIndices[i] += i;
                if (sectionsIndices[i] < dataList.Count)
                    dataList.Insert(sectionsIndices[i], null);// testing with null data
                else
                {
                    if (enableLog)
                    {
                        Debug.LogWarning("InfiniteListPopulator.InitTableView() section index " + (sectionsIndices[i] - i) + " value is larger than last data index and is ignored");
                    }
                    sectionsIndices.RemoveAt(i);
                    numberOfSections--;
                }
            }
        }
        int j = 0;
        for (int i = startIndex; i < dataList.Count; i++)
        {
            Transform item = GetItemFromPool(j);
            if (item != null)
            {
                // is it a section index?
                if (sectionsIndices.Contains(i) && item.tag.Equals(listItemTag))
                {
                    // change item to section
                    InitSection(item, i, j);
                }
                else
                {
                    InitListItemWithIndex(item, i, j);
                }
                if (enableLog)
                {
                    Debug.Log(item.name + "::" + item.tag);
                }
                j++;

            }
            else
            { // end of pool

                break;
            }
        }

        // at the moment we are repositioning the list after a delay... repositioning immediatly messes up the table when refreshing... no clue why...
        //Invoke ("RepositionList", 0.1f);
        LeanTween.delayedCall(0.1f, () =>
        {
            RepositionList();
        }).setIgnoreTimeScale(true);
        //LeanTween.delayedCall(0.1f, () =>
        //{
        //    RepositionList();
        //}).setIgnoreTimeScale(true);
    }

    public void RepositionList()
    {
        Debug.Log("RepositionList list");
        for (int i = 0; i < listChatClan.Count; i++)
        {
            if (listChatClan[i].contentType == Chat.CONTENT_TYPE_DONATE)
            {
                CachePvp.topChat++;
            }
        }
        table.Reposition();
        table.transform.localPosition = new Vector3(0, table.transform.localPosition.y, table.transform.position.z);
        // make sure we have a correct poistion sequence
        draggablePanel.SetDragAmount(0, 0, false);

        for (int i = 0; i < itemsPool.Count; i++)
        {
            Transform item = itemsPool[i];
            item.localPosition = new Vector3(0, -((cellHeight / 2) + i * (cellHeight + padding)), 0);
            if (itemsPoolChatClanPositionY.ContainsKey(i))
            {
                itemsPoolChatClanPositionY[i] = item.localPosition.y;
            }
            else
            {
                itemsPoolChatClanPositionY.Add(i, item.localPosition.y);
            }
        }
        for (int i = 0; i < itemsPoolChatClan.Count; i++)
        {
            if (itemsPoolChatClan[i].gameObject.activeInHierarchy)
            {
                itemsPoolChatClan[i].Reposition();
            }
        }
        popupClan.LoadedChat();
        draggablePanel.ResetPosition();
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Wait1FrameAfterReposition());
        }
        //LeanTween.delayedCall(0.1f, () =>
        //{
        //    Debug.Log(scrollBarChatClan.barSize);
        //    if (scrollBarChatClan.barSize < 0.98f)
        //        scrollBarChatClan.value = 1f;
        //});
    }

    private IEnumerator Wait1FrameAfterReposition()
    {
        yield return null;
        yield return null;
        draggablePanel.ResetPosition();
        yield return null;
        if (scrollBarChatClan.barSize < 0.98f)
            scrollBarChatClan.value = 1f;
    }

    // sections
    void InitSection(Transform item, int dataIndex, int poolIndex)
    {
        Destroy(item.gameObject);
        item = Instantiate(sectionPrefab) as Transform;
        item.GetComponent<InfiniteSectionChatClan>().itemNumber = poolIndex;

        item.name = "item" + dataIndex;
        item.parent = table.transform;
        item.GetComponent<InfiniteSectionChatClan>().itemDataIndex = dataIndex;
        item.GetComponent<InfiniteSectionChatClan>().listPopulator = this;
        item.GetComponent<InfiniteSectionChatClan>().panel = draggablePanel.panel;
        PopulateListSectionWithIndex(item, sectionsIndices.IndexOf(dataIndex));

        itemsPool[poolIndex] = item;
        dataTracker.Add(itemsPool[poolIndex].GetComponent<InfiniteSectionChatClan>().itemDataIndex, itemsPool[poolIndex].GetComponent<InfiniteSectionChatClan>().itemNumber);

    }

    void ChangeItemToSection(Transform item, int newIndex, int oldIndex)
    {
        int j = 0;
        Vector3 lastPosition = Vector3.zero;
        if (item.tag.Equals(listItemTag))
            j = item.GetComponent<InfiniteItemChatClan>().itemNumber;
        if (item.tag.Equals(listSectionTag))
            j = item.GetComponent<InfiniteSectionChatClan>().itemNumber;

        lastPosition = item.localPosition;
        Destroy(item.gameObject);
        item = Instantiate(sectionPrefab) as Transform;
        item.parent = table.transform;
        item.localPosition = lastPosition;
        if (newIndex < oldIndex)
            item.localPosition += new Vector3(0, (poolSize) * (cellHeight + padding), 0);
        else
            item.localPosition -= new Vector3(0, (poolSize) * (cellHeight + padding), 0);

        item.name = "item" + (newIndex);
        item.GetComponent<InfiniteSectionChatClan>().itemNumber = j;
        item.GetComponent<InfiniteSectionChatClan>().itemDataIndex = newIndex;
        item.GetComponent<InfiniteSectionChatClan>().listPopulator = this;
        item.GetComponent<InfiniteSectionChatClan>().panel = draggablePanel.panel;
        PopulateListSectionWithIndex(item, sectionsIndices.IndexOf(newIndex));
        itemsPool[j] = item;
        dataTracker.Add(newIndex, (int)(dataTracker[oldIndex]));
        dataTracker.Remove(oldIndex);
    }

    // items
    void InitListItemWithIndex(Transform item, int dataIndex, int poolIndex)
    {
        InfiniteItemChatClan script = item.GetComponent<InfiniteItemChatClan>();
        script.itemDataIndex = dataIndex;
        script.listPopulator = this;
        script.panel = draggablePanel.panel;
        item.name = "item" + dataIndex;
        PopulateListItemWithIndex(item, dataIndex);
        dataTracker.Add(itemsPool[poolIndex].GetComponent<InfiniteItemChatClan>().itemDataIndex, itemsPool[poolIndex].GetComponent<InfiniteItemChatClan>().itemNumber);
    }

    void InitListItemWithIndexFromPool(int dataIndex)
    {
        //InfiniteItemChatClan script = item.GetComponent<InfiniteItemChatClan>();
        //script.itemDataIndex = dataIndex;
        //script.listPopulator = this;
        //script.panel = draggablePanel.panel;
        //item.name = "item" + dataIndex;
        //PopulateListItemWithIndex(item, dataIndex);
        //dataTracker.Add(itemsPool[poolIndex].GetComponent<InfiniteItemChatClan>().itemDataIndex, itemsPool[poolIndex].GetComponent<InfiniteItemChatClan>().itemNumber);
        dataTracker.Add(dataIndex, dataIndex - poolSize);
    }

    void PrepareListItemWithIndex(Transform item, int newIndex, int oldIndex)
    {
        InfiniteItemChatClan script = item.GetComponent<InfiniteItemChatClan>();
        if (!itemsPoolChatClanHeight.ContainsKey(newIndex))
        {
            itemsPoolChatClanHeight.Add(newIndex, cellHeight + padding);
            itemsPoolChatClanHeightAdded.Add(newIndex, false);
        }
        if (!itemsPoolChatClanPositionY.ContainsKey(newIndex))
        {
            itemsPoolChatClanPositionY.Add(newIndex, itemsPoolChatClanPositionY[itemsPoolChatClanPositionY.Keys.Last()] - itemsPoolChatClanHeight[newIndex]);
        }
        item.localPosition = new Vector3(0, itemsPoolChatClanPositionY[newIndex], 0);
        script.itemDataIndex = newIndex;
        item.name = "item" + (newIndex);
        PopulateListItemWithIndex(item, newIndex, oldIndex);
        dataTracker.Add(newIndex, (int)(dataTracker[oldIndex]));
        dataTracker.Remove(oldIndex);

    }

    public void Join(ClanInfo c)
    {
        Debug.Log(c.id);
    }

    void ChangeSectionToItem(Transform item, int newIndex, int oldIndex)
    {
        int j = 0;
        Vector3 lastPosition = Vector3.zero;
        j = item.GetComponent<InfiniteSectionChatClan>().itemNumber;
        lastPosition = item.localPosition;
        Destroy(item.gameObject);
        item = Instantiate(itemPrefab) as Transform;
        item.parent = table.transform;
        item.localPosition = lastPosition;
        item.GetComponent<InfiniteItemChatClan>().itemNumber = j;
        item.GetComponent<InfiniteItemChatClan>().listPopulator = this;
        item.GetComponent<InfiniteItemChatClan>().panel = draggablePanel.panel;
        itemsPool[j] = item;
        itemsPoolChatClan[j] = item.GetComponent<InfiniteItemChatClan>();

        //maxHeight = lMessage.height;
        //boxCollider.size = new Vector3(boxCollider.size.x, (maxHeight - 43) + 254, boxCollider.size.z);

        PrepareListItemWithIndex(item, newIndex, oldIndex);
    }

    // the main logic for "infinite scrolling"...
    private bool isUpdatingList = false;
    int dataIndex;

    public IEnumerator ItemIsInvisible(int itemNumber)
    {
        if (isUpdatingList)
        {
            yield return null;
        }
        isUpdatingList = true;
        if (dataList.Count > poolSize)
        {// we need to do something "smart"... 
            Transform item = itemsPool[itemNumber];
            int itemDataIndex = 0;
            if (item.CompareTag(listItemTag))
            {
                //itemDataIndex = itemsPoolChatClan[itemNumber].itemDataIndex;
                itemDataIndex = item.GetComponent<InfiniteItemChatClan>().itemDataIndex;
            }
            //if (item.CompareTag(listSectionTag))
            //    itemDataIndex = item.GetComponent<InfiniteSectionChatClan>().itemDataIndex;
            int indexToCheck = 0;
            InfiniteItemChatClan infItem = null;
            //InfiniteSectionChatClan infSection = null;
            if (dataTracker.ContainsKey(itemDataIndex + 1))
            {
                infItem = itemsPool[(int)(dataTracker[itemDataIndex + 1])].GetComponent<InfiniteItemChatClan>();
                //dataIndex = (int)(dataTracker[itemDataIndex + 1]);
                //infItem = itemsPoolChatClan[dataIndex % poolSize];
                //infSection = itemsPool[(int)(dataTracker[itemDataIndex + 1])].GetComponent<InfiniteSectionChatClan>();
                //if ((infItem != null && infItem.verifyVisibility()) || (infSection != null && infSection.verifyVisibility()))
                if ((infItem != null && infItem.verifyVisibility()))
                {
                    // dragging upwards (scrolling down)
                    indexToCheck = itemDataIndex - (extraBuffer / 2);
                    if (dataTracker.ContainsKey(indexToCheck))
                    {
                        //do we have an extra item(s) as well?
                        //for (int i = indexToCheck; i >= 0; i--)
                        //{
                        for (int i = 0; i <= indexToCheck; i++)
                        {
                            if (dataTracker.ContainsKey(i))
                            {
                                //dataIndex = (int)(dataTracker[i]);
                                //infItem = itemsPoolChatClan[dataIndex % poolSize];
                                infItem = itemsPool[(int)(dataTracker[i])].GetComponent<InfiniteItemChatClan>();
                                //infSection = itemsPool[(int)(dataTracker[i])].GetComponent<InfiniteSectionChatClan>();
                                //if ((infItem != null && !infItem.verifyVisibility()) || (infSection != null && !infSection.verifyVisibility()))
                                if ((infItem != null && !infItem.verifyVisibility()))
                                {
                                    item = itemsPool[(int)(dataTracker[i])];
                                    if ((i) + poolSize < dataList.Count && i > -1)
                                    {
                                        // is it a section index?
                                        //if (sectionsIndices.Contains(i + poolSize))
                                        //{
                                        // change item to section
                                        //    ChangeItemToSection(item, i + poolSize, i);
                                        //}
                                        //else if (item.tag.Equals(listSectionTag))
                                        //{
                                        // change section to item
                                        //    ChangeSectionToItem(item, i + poolSize, i);
                                        //}
                                        //else
                                        //{
                                        PrepareListItemWithIndex(item, i + poolSize, i);
                                        //}
                                    }
                                }
                            }
                            //else
                            //{
                            //    scrollCursor = itemDataIndex - 1;
                            //    break;
                            //}
                        }
                    }
                }
            }
            if (dataTracker.ContainsKey(itemDataIndex - 1))
            {
                dataIndex = (int)(dataTracker[itemDataIndex - 1]);
                infItem = itemsPool[(int)(dataTracker[itemDataIndex - 1])].GetComponent<InfiniteItemChatClan>();
                //infItem = itemsPoolChatClan[dataIndex % poolSize];
                //infSection = itemsPool[(int)(dataTracker[itemDataIndex - 1])].GetComponent<InfiniteSectionChatClan>();
                //if ((infItem != null && infItem.verifyVisibility()) || (infSection != null && infSection.verifyVisibility()))
                if ((infItem != null && infItem.verifyVisibility()))
                {
                    //dragging downwards check the item below
                    indexToCheck = itemDataIndex + (extraBuffer / 2);

                    if (dataTracker.ContainsKey(indexToCheck))
                    {
                        // if we have an extra item
                        for (int i = indexToCheck; i < dataList.Count; i++)
                        {
                            if (dataTracker.ContainsKey(i))
                            {
                                //dataIndex = (int)(dataTracker[i]);
                                //infItem = itemsPoolChatClan[dataIndex % poolSize];
                                infItem = itemsPool[(int)(dataTracker[i])].GetComponent<InfiniteItemChatClan>();
                                //infSection = itemsPool[(int)(dataTracker[i])].GetComponent<InfiniteSectionChatClan>();
                                //if ((infItem != null && !infItem.verifyVisibility()) || (infSection != null && !infSection.verifyVisibility()))
                                if ((infItem != null && !infItem.verifyVisibility()))
                                {
                                    item = itemsPool[(int)(dataTracker[i])];
                                    if ((i) - poolSize > -1 && (i) < dataList.Count)
                                    {
                                        // is it a section index?
                                        //if (sectionsIndices.Contains(i - poolSize))
                                        //{
                                        // change item to section
                                        //    ChangeItemToSection(item, i - poolSize, i);
                                        //}
                                        //else if (item.tag.Equals(listSectionTag))
                                        //{
                                        // change section to item
                                        //    ChangeSectionToItem(item, i - poolSize, i);
                                        //}
                                        //else
                                        //{
                                        PrepareListItemWithIndex(item, i - poolSize, i);
                                        //}
                                    }
                                }
                            }
                            else
                            {
                                scrollCursor = itemDataIndex + 1;
                                break;
                            }
                        }
                    }
                }
            }
        }
        isUpdatingList = false;
    }

    #endregion

    #region items callbacks and helpers

    int GetJumpIndexForItem(int itemDataIndex)
    {
        // find real data index
        if (numberOfSections > 0 && itemDataIndex > sectionsIndices[0])
        {

            for (int index = numberOfSections - 1; index >= 0; index--)
            {
                if (itemDataIndex > sectionsIndices[index])
                {
                    itemDataIndex = itemDataIndex + (index + 1);
                    break;
                }

            }
        }
        return itemDataIndex;
    }

    public int GetRealIndexForItem(int itemDataIndex)
    {
        // find real data index
        if (numberOfSections > 0 && itemDataIndex > sectionsIndices[0])
        {

            for (int index = numberOfSections - 1; index >= 0; index--)
            {
                if (itemDataIndex > sectionsIndices[index])
                {
                    itemDataIndex = itemDataIndex - (index + 1);
                    break;
                }

            }
        }
        return itemDataIndex;
    }

    public void itemIsPressed(int itemDataIndex, bool isDown)
    {
        itemDataIndex = GetRealIndexForItem(itemDataIndex);
        if (enableLog)
        {
            Debug.Log("Pressed down item " + itemDataIndex + " " + isDown);
        }
        if (InfiniteItemIsPressedEvent != null)
            InfiniteItemIsPressedEvent(itemDataIndex, isDown);
    }

    public void itemClicked(int itemDataIndex)
    {
        itemDataIndex = GetRealIndexForItem(itemDataIndex);
        if (enableLog)
        {
            Debug.Log("Clicked item " + itemDataIndex);
        }
        if (InfiniteItemIsClickedEvent != null)
            InfiniteItemIsClickedEvent(itemDataIndex);
    }

    #endregion

    #region Pool & sections Management

    Transform GetItemFromPool(int i)
    {
        if (i >= 0 && i < poolSize)
        {
            itemsPool[i].gameObject.SetActive(true);
            return itemsPool[i];
        }
        else
            return null;
    }

    Transform GetItemFromPool(out int id)
    {
        for (int i = 0; i < itemsPool.Count; i++)
        {
            if (!itemsPool[i].gameObject.activeInHierarchy)
            {
                itemsPoolChatClanHeight[i] = cellHeight + padding;
                itemsPoolChatClanPositionY[i] = 0;
                id = i;
                return itemsPool[i];
            }
        }
        for (int i = 0; i < itemsPool.Count; i++)
        {
            if (!itemsPoolChatClan[i].verifyVisibility())
            {
                int j = poolSize;
                while (itemsPoolChatClanHeight.ContainsKey(j))
                {
                    j++;
                }
                itemsPoolChatClanHeight.Add(j, cellHeight + padding);
                itemsPoolChatClanHeightAdded.Add(j, false);
                itemsPoolChatClanPositionY.Add(j, 0);
                id = i;
                return itemsPool[i];
            }
        }
        id = -1;
        return null;
    }

    void RefreshPool()
    {
        //poolSize = (int)(draggablePanel.panel.baseClipRegion.w / cellHeight) + extraBuffer;
        if (enableLog)
        {
            Debug.Log("REFRESH POOL SIZE:::" + poolSize);
        }
        // destroy current items
        for (int i = 0; i < itemsPool.Count; i++)
        {
            Destroy(itemsPool[i].gameObject);
        }
        itemsPool.Clear();
        itemsPoolChatClan.Clear();
        itemsPoolChatClanHeight.Clear();
        itemsPoolChatClanHeightAdded.Clear();
        itemsPoolChatClanPositionY.Clear();
        itemsPoolChatClanChat.Clear();
        for (int i = 0; i < poolSize; i++)
        { // the pool will use itemPrefab as a default
            Transform item = Instantiate(itemPrefab) as Transform;
            item.gameObject.SetActive(false);
            InfiniteItemChatClan itemScript = item.GetComponent<InfiniteItemChatClan>();
            itemScript.itemNumber = i;
            item.name = "item" + i;
            item.parent = table.transform;
            itemsPool.Add(item);
            itemsPoolChatClan.Add(itemScript);
            itemsPoolChatClanHeight.Add(i, cellHeight + padding);
            itemsPoolChatClanHeightAdded.Add(i, false);
            itemsPoolChatClanPositionY.Add(i, 0);
        }

    }

    #endregion
}