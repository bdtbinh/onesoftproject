﻿using SkyGameKit.Multiplayer;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using System.Collections.Generic;

public class PvPUIManager : MonoBehaviour
{
    public PopupMainPvP popupMainPvP;
    public PopupRoomPvP popupRoomPvP;
    public PopupFindPvP popupFindPvP;
    public GameObject popupLoadingPvP;

    private void Awake()
    {
        _instance = this;
        new CSSceneLoaded(CSSceneLoaded.SCENE_PVP_MAIN).Send();
    }

    private static PvPUIManager _instance;

    public static PvPUIManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.PVP.SessionRewardedInfo.ToString(), OnSessionRewardedInfo, true);
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    private void OnSessionRewardedInfo(IMessage rMessage)
    {
        SCPvPSessionRewardedInfo info = rMessage.Data as SCPvPSessionRewardedInfo;
        if (info.status == SCPvPSessionRewardedInfo.SUCCESS)
        {
            List<ItemVolumeFalconMail> list = new List<ItemVolumeFalconMail>();
            for (int i = 0; i < 3; i++)
            {
                ItemVolumeFalconMail item = new ItemVolumeFalconMail();
                if (i == 0)
                {
                    item.key = FalconMail.ITEM_GOLD;
                    item.value = info.gold;
                }
                else if (i == 1)
                {
                    item.key = FalconMail.ITEM_GEM;
                    item.value = info.gem;
                }
                else if (i == 2)
                {
                    item.key = FalconMail.ITEM_CARD_PLANE_GENERAL;
                    item.value = info.card;
                }
                list.Add(item);
            }
            PopupManager.Instance.ShowClaimPopup(list, EventName.PVP_REWARD, true);
        }
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        MessageDispatcher.RemoveListener(EventName.PVP.SessionRewardedInfo.ToString(), OnSessionRewardedInfo, true);
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (PopupManager.Instance.goToPVP || PopupManager.Instance.goToTournament || PopupManager.Instance.goTo2v2)
        {
            popupMainPvP.gameObject.SetActive(false);
            popupFindPvP.gameObject.SetActive(true);
        }
    }

}
