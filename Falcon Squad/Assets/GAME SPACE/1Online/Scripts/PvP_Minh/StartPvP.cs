﻿using System;
using SkyGameKit.Multiplayer;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartPvP : MonoBehaviour
{
    public void B_GoToPvP_Click()
    {
        SceneManager.LoadScene("PVPMain");
    }
    public void B_Add_Gold_Click()
    {
        CacheGame.SetTotalCoin(2000000, "test", "test");
        CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, GameContext.TOTAL_LEVEL);
        CacheGame.SetMaxLevel3Difficult(GameContext.TOTAL_LEVEL);
    }
}
