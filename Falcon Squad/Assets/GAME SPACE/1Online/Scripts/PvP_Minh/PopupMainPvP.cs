﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PopupMainPvP : MonoBehaviour
{
    private const string MEDAL_ENABLE_SPRITE = "PVP_rank_medal_1";
    private const string MEDAL_DISABLE_SPRITE = "PVP_rank_medal_0";

    private const string BTN_BETTING_CHOSEN_SPRITE = "IAP_btn_dollar";
    private const string BTN_BETTING_UNCHOSEN_SPRITE = "IAP_btn_dollar_d";

    //Betting buttons
    [SerializeField]
    private UISprite sBettingMin;

    [SerializeField]
    private UILabel lBettingMin;

    [SerializeField]
    private UISprite sBettingMiddle;

    [SerializeField]
    private UILabel lBettingMiddle;

    [SerializeField]
    private UISprite sBettingMax;

    [SerializeField]
    private UILabel lBettingMax;

    public UILabel lSeasonEnd;

    public void OnClickBtnCreate()
    {
        PvPUIManager.Instance.popupRoomPvP.ShowPopupRoom(PopupRoomPvP.RoomType.Create);
    }

    public void OnClickBtnJoin()
    {
        PvPUIManager.Instance.popupRoomPvP.ShowPopupRoom(PopupRoomPvP.RoomType.Join);
    }

    public void OnClickBtnFight()
    {

        Debug.Log("OnClickBtnFight");
        if (CacheGame.GetTotalCoin() < GetBettingMin())
        {
            if (CachePvp.isShowPopupVideo)
            {
                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_more_gold);
            }
            else
            {
                CachePvp.isShowPopupVideo = true;
                List<PromotionItem> list = new List<PromotionItem>();
                PromotionItem pi = new PromotionItem();
                pi.itemIndex = 1;
                pi.volume = 300;
                list.Add(pi);
                PopupManager.Instance.ShowNotifyPopup(0, "21", "Xem video ngắn để nhận 300 vàng", "", 0, list);
            }
            return;
        }

        if (CacheGame.GetTotalCoin() < MinhCacheGame.GetBettingAmountPvP())
        {
            if (CachePvp.isShowPopupVideo)
            {
                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_not_enough_coin);
            }
            else
            {
                CachePvp.isShowPopupVideo = true;
                List<PromotionItem> list = new List<PromotionItem>();
                PromotionItem pi = new PromotionItem();
                pi.itemIndex = 1;
                pi.volume = 300;
                list.Add(pi);
                PopupManager.Instance.ShowNotifyPopup(0, "21", "Xem video ngắn để nhận 300 vàng", "", 0, list);
            }
            return;
        }

        //DontDestroyManager.Instance.panelCoinGemTop.SetActive(false);
        //DontDestroyManager.Instance.panelVideoTopBtn.SetActive(false);

        PopupManagerCuong.Instance.HidePanelCoinGem();
        PopupManagerCuong.Instance.HidePanelBtnVideoTop();

        CachePvp.typePVP = CSFindOpponent.TYPE_PVP;
        PvPUIManager.Instance.popupMainPvP.gameObject.SetActive(false);
        PvPUIManager.Instance.popupFindPvP.gameObject.SetActive(true);
    }

    public void ButtonReward()
    {
        PopupManager.Instance.ShowRankRewardPopup();
    }

    public void OnClickBtnBack()
    {
        //PvPUIManager.Instance.popupMainPvP.gameObject.SetActive(false);        
        SceneManager.LoadScene("Home");
    }

    public int GetBettingMin()
    {
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code] - 1; //from 0 -> 7
        if (CachePvp.listGoldBet.Count > 3 * rank + 2)
        {
            return CachePvp.listGoldBet[3 * rank];
        }
        else
        {
            if (CachePvp.listGoldBet.Count >= 3)
            {
                return CachePvp.listGoldBet[CachePvp.listGoldBet.Count - 3];
            }
            else
            {
                return Constant.BETTING_AMOUNT_MIN;
            }
        }
    }

    public int GetBettingMiddle()
    {
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code] - 1; //from 0 -> 7
        if (CachePvp.listGoldBet.Count > 3 * rank + 2)
        {
            return CachePvp.listGoldBet[3 * rank + 1];
        }
        else
        {
            if (CachePvp.listGoldBet.Count >= 3)
            {
                return CachePvp.listGoldBet[CachePvp.listGoldBet.Count - 2];
            }
            else
            {
                return Constant.BETTING_AMOUNT_MIDDLE;
            }
        }
    }

    public int GetBettingMax()
    {
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code] - 1; //from 0 -> 7
        if (CachePvp.listGoldBet.Count > 3 * rank + 2)
        {
            return CachePvp.listGoldBet[3 * rank + 2];
        }
        else
        {
            if (CachePvp.listGoldBet.Count >= 3)
            {
                return CachePvp.listGoldBet[CachePvp.listGoldBet.Count - 1];
            }
            else
            {
                return Constant.BETTING_AMOUNT_MAX;
            }
        }
    }

    private void LoadUI()
    {
        if (!PopupManager.Instance.goToPVP)
        {
            if (MinhCacheGame.GetBettingAmountPvP() == 0)
            {
                MinhCacheGame.SetBettingAmountPvP(GetBettingMin());
            }
            if (MinhCacheGame.GetBettingAmountPvP() != GetBettingMin() && MinhCacheGame.GetBettingAmountPvP() != GetBettingMiddle() && MinhCacheGame.GetBettingAmountPvP() != GetBettingMax())
            {
                MinhCacheGame.SetBettingAmountPvP(GetBettingMin());
            }
        }

        int day = (int)(CachePvp.timeEndSession / 60 / 60 / 24);
        int hour = (int)(CachePvp.timeEndSession / 60 / 60) - (day * 24);
        //lSeasonEnd.text = "SEason end in " + day + " days " + hour + " hours";
        lSeasonEnd.text = I2.Loc.ScriptLocalization.info_season_end.Replace("%{total_day}", "[ff0000]" + day.ToString() + "[-]").Replace("%{total_hour}", "[ff0000]" + hour.ToString() + "[-]");
        readyClickBack = true;
        lBettingMin.text = GameContext.FormatNumber(GetBettingMin()) + "";
        lBettingMiddle.text = GameContext.FormatNumber(GetBettingMiddle()) + "";
        lBettingMax.text = GameContext.FormatNumber(GetBettingMax()) + "";

        if (MinhCacheGame.GetBettingAmountPvP() == GetBettingMin())
        {
            sBettingMin.spriteName = BTN_BETTING_CHOSEN_SPRITE;
        }
        else if (MinhCacheGame.GetBettingAmountPvP() == GetBettingMiddle())
        {
            sBettingMiddle.spriteName = BTN_BETTING_CHOSEN_SPRITE;
        }
        else if (MinhCacheGame.GetBettingAmountPvP() == GetBettingMax())
        {
            sBettingMax.spriteName = BTN_BETTING_CHOSEN_SPRITE;
        }
    }

    public void OnClickBtnBetMin()
    {
        sBettingMin.spriteName = BTN_BETTING_CHOSEN_SPRITE;
        sBettingMiddle.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;
        sBettingMax.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;

        //SetBettingAmount(Constant.BETTING_AMOUNT_MIN);
        SetBettingAmount(GetBettingMin());
    }

    public void OnClickBtnBetMiddle()
    {
        sBettingMin.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;
        sBettingMiddle.spriteName = BTN_BETTING_CHOSEN_SPRITE;
        sBettingMax.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;

        SetBettingAmount(GetBettingMiddle());
    }

    public void OnClickBtnBetMax()
    {
        sBettingMin.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;
        sBettingMiddle.spriteName = BTN_BETTING_UNCHOSEN_SPRITE;
        sBettingMax.spriteName = BTN_BETTING_CHOSEN_SPRITE;

        SetBettingAmount(GetBettingMax());
    }

    void SetBettingAmount(int amount)
    {
        MinhCacheGame.SetBettingAmountPvP(amount);
    }

    private void OnEnable()
    {
        SoundManager.StopMainBG();
        SoundManager.StopMain2BG();
        SoundManager.PlayHomeBG();
        if (PopupManager.Instance.goToTournament || PopupManager.Instance.goTo2v2)
        {
            return;
        }
        LoadUI();
    }

    private bool readyClickBack;

    IEnumerator ReadyClickBack()
    {
        readyClickBack = false;
        yield return new WaitForSecondsRealtime(0.38f);
        readyClickBack = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && readyClickBack)
        {
            StartCoroutine(ReadyClickBack());
            if (PopupManager.Instance.IsInviteFriendsPopupActive())
            {
                PopupManager.Instance.HideInviteFriendsPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveShopPopup())
            {
                PopupManagerCuong.Instance.HideShopPopup();
            }
            else if (PopupManager.Instance.IsVideoRewardPopupActive())
            {
                PopupManager.Instance.HideVideoRewardPopup();
            }
            else if (PopupManager.Instance.IsProfilePopupActive())
            {

            }
            else if (PopupManager.Instance.IsRankRewardPopupActive())
            {
                PopupManager.Instance.HideRankRewardPopup();
            }
            else if (PopupManager.Instance.IsRankingPopupActive())
            {
                PopupManager.Instance.HideRankingPopup();
            }
            else if (PopupManager.Instance.IsLiveScorePopupActive())
            {
                PopupManager.Instance.HideLiveScorePopup();
            }
            else
            {
                OnClickBtnBack();
            }
        }
    }

    public void ButtonLeaderboard()
    {
        PopupManager.Instance.ShowRankingPVP();
    }

    public void ButtonLiveScore()
    {
        CachePvp.LoadLiveScore = 0;
        PopupManager.Instance.ShowLiveScorePopup();
    }

    public void ButtonInvite()
    {
        PopupManager.Instance.ShowInviteFriendsPopup();
    }

    public void SCFindOpponentCallback(FindState findState, int timeout)
    {
        switch (findState)
        {
            case FindState.SUCCESS:
                break;

            case FindState.ERROR:
                PopupManager.Instance.ShowToast("Server is busy now!");
                //CannotFindOpponent();
                break;

            case FindState.WAITING:
                //StartFindOpponent(timeout);
                break;

            case FindState.TIMEOUT:
                //CannotFindOpponent();
                break;
        }
    }
}
