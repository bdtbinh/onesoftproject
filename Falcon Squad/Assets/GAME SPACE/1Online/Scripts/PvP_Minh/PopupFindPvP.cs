﻿using UnityEngine;
using System.Collections;
using Mp.Pvp;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using com.ootii.Messages;
using SkyGameKit;

public class PopupFindPvP : SgkSingleton<PopupFindPvP>
{
    public GameObject pvpNormal;
    public GameObject pvp2v2;

    public List<UI2DSprite> listAvatar;
    public List<UISprite> listVipIcon;
    public List<UILabel> listPlayerName;
    public List<UILabel> listPlayerClan;
    public List<UILabel> listPlayerWinrate;
    public List<UILabel> listPlayerScore;
    public List<UILabel> listPlayerFound;
    public UILabel lConnectStatus2v2;
    public UILabel lConnectSeconds2v2;
    public UILabel lElo2v2;
    public UILabel lEloWin2v2;
    public UILabel lEloDraw2v2;
    public UILabel lEloLose2v2;
    public GameObject btnRefind2v2;

    [SerializeField]
    private UI2DSprite sPlayerAvatar;

    [SerializeField]
    private UI2DSprite sOpponentAvatar;

    [SerializeField]
    private UILabel lConnectStatus;

    [SerializeField]
    private UILabel lConnectSeconds;

    public UISprite sVipIconPlayer;
    public UISprite sVipIconOpponent;

    [SerializeField]
    private UILabel lPlayerName;
    public UILabel lPlayerNameClan;

    [SerializeField]
    private UILabel lPlayerElo;

    [SerializeField]
    private UILabel lPlayerWinRate;

    [SerializeField]
    private UILabel lOpponentName;
    public UILabel lOpponentNameClan;

    [SerializeField]
    private UILabel lOpponentElo;

    [SerializeField]
    private UILabel lOpponentWinRate;

    [SerializeField]
    private GameObject btnRefind;

    [SerializeField]
    UISprite sMedalRankPlayer;

    [SerializeField]
    UISprite sMedalRankOpponent;

    [SerializeField]
    private UILabel lMedalRankPlayer;

    [SerializeField]
    private UILabel lMedalRankOpponent;

    public UILabel lElo;
    public UILabel lEloWin;
    public UILabel lEloDraw;
    public UILabel lEloLose;

    string facebookIdPlayerP1 = "";
    string facebookIdOpponentP1 = "";
    string facebookIdPlayerP2 = "";
    string facebookIdOpponentP2 = "";

    public GameObject btnBack;

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.ChangeProperties.DisconnectFromServer.ToString(), OnDisconnectFromServer, true);
        MessageDispatcher.AddListener(EventName.ChangeProperties.UpdatePlayerInfo.ToString(), OnUpdatePlayerInfo, true);

        MessageDispatcher.AddListener(EventName.PVP.CoopRoomInfo.ToString(), OnCoopRoomInfo, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.ChangeProperties.DisconnectFromServer.ToString(), OnDisconnectFromServer, true);
        MessageDispatcher.RemoveListener(EventName.ChangeProperties.UpdatePlayerInfo.ToString(), OnUpdatePlayerInfo, true);

        MessageDispatcher.RemoveListener(EventName.PVP.CoopRoomInfo.ToString(), OnCoopRoomInfo, true);
    }

    private void OnCoopRoomInfo(IMessage rMessage)
    {
        SCCoopPvPRoomInfo ri = rMessage.Data as SCCoopPvPRoomInfo;
        if (ri.status == SCCoopPvPRoomInfo.SUCCESS)
        {
            CachePvp.PlayTime = ri.data.playTime;
            switch (ri.data.state)
            {
                case CoopPvPRoomData.WAIT_FOR_MATCHING:
                    for (int i = 1; i < 4; i++)
                    {
                        if (i >= ri.data.players.Count)
                        {
                            listVipIcon[i].gameObject.SetActive(false);
                            listPlayerName[i].text = "????????????????";
                            tempVector3.x = listPlayerName[i].transform.localPosition.x;
                            tempVector3.z = listPlayerName[i].transform.localPosition.z;
                            tempVector3.y = 42;
                            listPlayerName[i].transform.localPosition = tempVector3;
                            listPlayerClan[i].text = "";
                            listPlayerScore[i].text = "?";
                            listPlayerWinrate[i].text = "?";
                            listPlayerFound[i].text = "";
                            listAvatar[i].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                        }
                        else
                        {
                            listVipIcon[i].gameObject.SetActive(false);
                            listPlayerName[i].text = "";
                            listPlayerClan[i].text = "";
                            listPlayerScore[i].text = "";
                            listPlayerWinrate[i].text = "";
                            listPlayerFound[i].text = I2.Loc.ScriptLocalization.title_player_found;
                            listAvatar[i].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                        }
                    }
                    break;
                case CoopPvPRoomData.WAIT_FOR_READY:
                    if (CachePvp.coopPvPRoomInfo.data.waveCacheIndex != -1 && CachePvp.coopPvPRoomInfo.data.waveCacheData != null && CachePvp.coopPvPRoomInfo.data.waveCacheData.Length > 0)
                    {
                        PvPDataHelper.Instance.SetWaveCacheIndexForPvP(CachePvp.coopPvPRoomInfo.data.waveCacheIndex);
                        PvPDataHelper.Instance.SetDataForPvP(CachePvp.coopPvPRoomInfo.data.waveCacheData);
                    }
                    else
                    {
                        PvPDataHelper.Instance.SetWaveCacheIndexForPvP(PvPDataHelper.Instance.GetMyWaveCacheIndex(CSFindOpponent.TYPE_2V2));
                        PvPDataHelper.Instance.SetDataForPvP(PvPDataHelper.Instance.GetMyDataString(CSFindOpponent.TYPE_2V2));
                    }
                    LoadScene2v2();
                    break;
                case CoopPvPRoomData.DESTROY:
                    PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_room_destroyed);
                    SceneManager.LoadScene("Home");
                    break;
                default:
                    break;
            }
        }
    }

    private void LoadScene2v2()
    {
        Debug.Log("LoadScene2v2");
        CachePvp.PlayTime = CachePvp.coopPvPRoomInfo.data.playTime;
        isWaitForReady = true;
        btnBack.SetActive(false);
        Player me = null;
        Player myTeamate = null;
        Player opponent1 = null;
        Player opponent2 = null;
        for (int i = 0; i < CachePvp.coopPvPRoomInfo.data.teams.Count; i++)
        {
            for (int j = 0; j < CachePvp.coopPvPRoomInfo.data.teams[i].members.Count; j++)
            {
                if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.code.Equals(CachePvp.Code))
                {
                    CachePvp.myTeamIndex = i;
                    CachePvp.myIndex = j;
                    break;
                }
            }
        }
        me = CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].members[CachePvp.myIndex].player;
        if (CachePvp.myIndex == 0)
        {
            CachePvp.myTeammateIndex = 1;
            myTeamate = CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].members[1].player;
        }
        else
        {
            CachePvp.myTeammateIndex = 0;
            myTeamate = CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].members[0].player;
        }
        CachePvp.MyTeamate = myTeamate;
        CachePvp.MyTeammateCode = int.Parse(myTeamate.code);
        if (CachePvp.myTeamIndex == 0)
        {
            opponent1 = CachePvp.coopPvPRoomInfo.data.teams[1].members[0].player;
            opponent2 = CachePvp.coopPvPRoomInfo.data.teams[1].members[1].player;
        }
        else
        {
            opponent1 = CachePvp.coopPvPRoomInfo.data.teams[0].members[0].player;
            opponent2 = CachePvp.coopPvPRoomInfo.data.teams[0].members[1].player;
        }
        CachePvp.listPlayers2v2.Clear();
        CachePvp.dictionaryPlayers2v2.Clear();
        CachePvp.listPlayers2v2.Add(me);
        CachePvp.listPlayers2v2.Add(myTeamate);
        CachePvp.dictionaryPlayers2v2.Add(CachePvp.MyCode, me);
        CachePvp.dictionaryPlayers2v2.Add(CachePvp.MyTeammateCode, myTeamate);
        CachePvp.listPlayers2v2.Add(opponent1);
        CachePvp.listPlayers2v2.Add(opponent2);
        CachePvp.dictionaryPlayers2v2.Add(long.Parse(opponent1.code), opponent1);
        CachePvp.dictionaryPlayers2v2.Add(long.Parse(opponent2.code), opponent2);
        Player t = null;
        for (int i = 1; i < 4; i++)
        {
            if (i == 1)
            {
                t = myTeamate;
            }
            else if (i == 2)
            {
                t = opponent1;
            }
            else if (i == 3)
            {
                t = opponent2;
            }
            if (t != null)
            {
                listVipIcon[i].gameObject.SetActive(false);
                listPlayerName[i].text = string.IsNullOrEmpty(t.name) ? t.code : t.name;
                tempVector3.x = listPlayerName[i].transform.localPosition.x;
                tempVector3.z = listPlayerName[i].transform.localPosition.z;
                if (t.playerClan != null)
                {
                    listPlayerClan[i].text = t.playerClan.name;
                    tempVector3.y = 44;
                }
                else
                {
                    listPlayerClan[i].text = "";
                    tempVector3.y = 24;
                }
                listPlayerName[i].transform.localPosition = tempVector3;
                listPlayerFound[i].text = "";

                if (GameContext.IS_CHINA_VERSION)
                {
                    if (!string.IsNullOrEmpty(t.appCenterId))
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(t.appCenterId))
                        {
                            listAvatar[i].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[t.appCenterId];
                        }
                        else
                        {
                            Texture2D t2d = CacheAvatarManager.Instance.GetTextureByGamecenterId(t.appCenterId);
                            if (t2d != null)
                            {
                                CacheAvatarManager.Instance.DownloadImageFormUrl("", t.appCenterId, t2d);
                            }
                            else
                            {
                                listAvatar[i].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                            }
                            if (i == 1)
                            {
                                facebookIdPlayerP2 = t.appCenterId;
                            }
                            else if (i == 2)
                            {
                                facebookIdOpponentP1 = t.appCenterId;
                            }
                            else if (i == 3)
                            {
                                facebookIdOpponentP2 = t.appCenterId;
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(t.facebookId))
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(t.facebookId))
                        {
                            listAvatar[i].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[t.facebookId];
                        }
                        else
                        {
                            if (i == 1)
                            {
                                facebookIdPlayerP2 = t.facebookId;
                            }
                            else if (i == 2)
                            {
                                facebookIdOpponentP1 = t.facebookId;
                            }
                            else if (i == 3)
                            {
                                facebookIdOpponentP2 = t.facebookId;
                            }
                            string query = "https://graph.facebook.com/" + t.facebookId + "/picture?type=small&width=135&height=135";
                            CacheAvatarManager.Instance.DownloadImageFormUrl(query, t.facebookId);
                        }
                    }
                }
                if (t.pvP2vs2SeasonData != null)
                {
                    listPlayerScore[i].text = GameContext.FormatNumber(t.pvP2vs2SeasonData.score);
                    listPlayerWinrate[i].text = I2.Loc.ScriptLocalization.win_rate.Replace("%{percent_number}", t.GetWinrateText2v2());
                }
            }
        }
        lConnectStatus.text = "VS";
        lConnectSeconds.gameObject.SetActive(false);
        GameContext.modeGamePlay = GameContext.ModeGamePlay.PVP2vs2;
        Debug.Log("load game pvp 2v2");
        StartCoroutine(LoadScene("PVP2v2"));
    }

    void OnUpdatePlayerInfo(IMessage msg)
    {
        lPlayerElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.PlayerLevel.elo);
        lPlayerWinRate.text = I2.Loc.ScriptLocalization.win_rate.Replace("%{percent_number}", CachePvp.GetWinrateText());
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lMedalRankPlayer.text = rankTitle + " " + CachePvp.ConvertChina(CachePvp.PlayerLevel.name);
        sMedalRankPlayer.spriteName = "PVP_rank_" + rank;
    }

    void OnDisconnectFromServer(IMessage msg)
    {
        //mRoomId = "";
        //		PopupManager.Instance.ShowToast (I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
        SceneManager.LoadScene("Home");
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdPlayerP1))
        {
            sPlayerAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdPlayerP1];
            listAvatar[0].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdPlayerP1];
        }
        if (!string.IsNullOrEmpty(facebookIdPlayerP2) && CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdPlayerP2))
        {
            listAvatar[1].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdPlayerP2];
        }
        if (!string.IsNullOrEmpty(facebookIdOpponentP1) && CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdOpponentP1))
        {
            sOpponentAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdOpponentP1];
            listAvatar[2].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdOpponentP1];
        }
        if (!string.IsNullOrEmpty(facebookIdOpponentP2) && CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdOpponentP2))
        {
            listAvatar[3].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdOpponentP2];
        }
    }

    Vector3 tempVector3;

    private void LoadUI(Player opponent)
    {
        CachePvp.Opponent = opponent;
        sPlayerAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(CachePvp.GamecenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.GamecenterId))
                {
                    sPlayerAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.GamecenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.GamecenterId);
                    if (t != null)
                    {
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.GamecenterId, t);
                    }
                    else
                    {
                        sPlayerAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                    }
                    facebookIdPlayerP1 = CachePvp.GamecenterId;
                }
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(CachePvp.FacebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
                {
                    sPlayerAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
                }
                else
                {
                    facebookIdPlayerP1 = CachePvp.FacebookId;
                    string query = "https://graph.facebook.com/" + CachePvp.FacebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.FacebookId);
                }
            }
        }
        lConnectStatus.text = I2.Loc.ScriptLocalization.msg_pvp_connect_to_server;
        lConnectSeconds.gameObject.SetActive(false);
        lElo.text = "";
        lEloWin.text = "";
        lEloDraw.text = "";
        lEloLose.text = "";
        Debug.Log("CachePvp.myVip : " + CachePvp.myVip);
        if (CachePvp.myVip == 0)
        {
            sVipIconPlayer.gameObject.SetActive(false);
        }
        else
        {
            sVipIconPlayer.gameObject.SetActive(true);
            sVipIconPlayer.spriteName = "Icon__VIP_" + (CachePvp.myVip - 1);
        }

        lPlayerName.text = string.IsNullOrEmpty(CachePvp.Name) ? CachePvp.Code : CachePvp.Name;
        tempVector3.x = lPlayerName.transform.localPosition.x;
        tempVector3.z = lPlayerName.transform.localPosition.z;
        if (!string.IsNullOrEmpty(CachePvp.PlayerClan.name))
        {
            lPlayerNameClan.text = CachePvp.PlayerClan.name;
            tempVector3.y = 44;
        }
        else
        {
            tempVector3.y = 24;
            lPlayerNameClan.text = "";
        }
        lPlayerName.transform.localPosition = tempVector3;
        lPlayerElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.PlayerLevel.elo);
        lPlayerWinRate.text = I2.Loc.ScriptLocalization.win_rate.Replace("%{percent_number}", CachePvp.GetWinrateText());
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lMedalRankPlayer.text = rankTitle + " " + CachePvp.ConvertChina(CachePvp.PlayerLevel.name);
        sMedalRankPlayer.spriteName = "PVP_rank_" + rank;


        sOpponentAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        lOpponentName.text = CachePvp.Opponent.name;
        tempVector3.x = lOpponentName.transform.localPosition.x;
        tempVector3.z = lOpponentName.transform.localPosition.z;
        if (opponent.playerClan != null)
        {
            lOpponentNameClan.text = opponent.playerClan.name;
            tempVector3.y = 44;
        }
        else
        {
            lOpponentNameClan.text = "";
            tempVector3.y = 24;
        }
        lOpponentName.transform.localPosition = tempVector3;
        lOpponentElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.Opponent.playerLevel.elo);
        lOpponentWinRate.text = I2.Loc.ScriptLocalization.win_rate.Replace("%{percent_number}", CachePvp.Opponent.GetWinrateText());

        rank = CachePvp.dictionaryRank[CachePvp.Opponent.playerLevel.code];
        rankTitle = (MedalRankTitle)(rank);
        lMedalRankOpponent.text = rankTitle + " " + CachePvp.ConvertChina(CachePvp.Opponent.playerLevel.name);
        sMedalRankOpponent.spriteName = "PVP_rank_" + rank;

        btnRefind.SetActive(false);
        int vip = CachePvp.GetVipFromVipPoint(CachePvp.Opponent.vip);
        if (vip == 0)
        {
            sVipIconOpponent.gameObject.SetActive(false);
        }
        else
        {
            sVipIconOpponent.gameObject.SetActive(true);
            sVipIconOpponent.spriteName = "Icon__VIP_" + (vip - 1);
        }
    }

    private void LoadUI2v2()
    {
        isWaitForReady = false;
        btnBack.SetActive(true);
        listPlayerFound[0].text = "";
        listAvatar[0].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(CachePvp.GamecenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.GamecenterId))
                {
                    listAvatar[0].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.GamecenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.GamecenterId);
                    if (t != null)
                    {
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.GamecenterId, t);
                    }
                    else
                    {
                        listAvatar[0].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                    }
                    facebookIdPlayerP1 = CachePvp.GamecenterId;
                }
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(CachePvp.FacebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
                {
                    listAvatar[0].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
                }
                else
                {
                    facebookIdPlayerP1 = CachePvp.FacebookId;
                    string query = "https://graph.facebook.com/" + CachePvp.FacebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.FacebookId);
                }
            }
        }
        if (CachePvp.myVip == 0)
        {
            listVipIcon[0].gameObject.SetActive(false);
        }
        else
        {
            listVipIcon[0].gameObject.SetActive(true);
            listVipIcon[0].spriteName = "Icon__VIP_" + (CachePvp.myVip - 1);
        }
        lElo2v2.text = "";
        lEloWin2v2.text = "";
        lEloDraw2v2.text = "";
        lEloLose2v2.text = "";
        lConnectStatus2v2.text = I2.Loc.ScriptLocalization.msg_pvp_connect_to_server;
        lConnectSeconds2v2.gameObject.SetActive(false);

        listPlayerName[0].text = string.IsNullOrEmpty(CachePvp.Name) ? CachePvp.Code : CachePvp.Name;
        tempVector3.x = listPlayerName[0].transform.localPosition.x;
        tempVector3.z = listPlayerName[0].transform.localPosition.z;
        if (!string.IsNullOrEmpty(CachePvp.PlayerClan.name))
        {
            listPlayerClan[0].text = CachePvp.PlayerClan.name;
            tempVector3.y = 44;
        }
        else
        {
            listPlayerClan[0].text = "";
            tempVector3.y = 24;
        }
        listPlayerName[0].transform.localPosition = tempVector3;
        if (CachePvp.PvP2vs2SeasonData != null)
        {
            listPlayerScore[0].text = I2.Loc.ScriptLocalization.season_score + ": " + GameContext.FormatNumber(CachePvp.PvP2vs2SeasonData.score);
        }
        listPlayerWinrate[0].text = I2.Loc.ScriptLocalization.win_rate.Replace("%{percent_number}", CachePvp.GetWinrateText());

        for (int i = 1; i < 4; i++)
        {
            listVipIcon[i].gameObject.SetActive(false);
            listPlayerName[i].text = "????????????????";
            tempVector3.x = listPlayerName[i].transform.localPosition.x;
            tempVector3.z = listPlayerName[i].transform.localPosition.z;
            tempVector3.y = 42;
            listPlayerName[i].transform.localPosition = tempVector3;
            listPlayerClan[i].text = "";
            listPlayerScore[i].text = "?";
            listPlayerWinrate[i].text = "?";
            listPlayerFound[i].text = "";
            listAvatar[i].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        }

        btnRefind2v2.SetActive(false);

        if (CachePvp.coopPvPRoomInfo != null)
        {
            SCCoopPvPRoomInfo ri = CachePvp.coopPvPRoomInfo;
            if (ri.status == SCCoopPvPRoomInfo.SUCCESS)
            {
                for (int i = 1; i < 4; i++)
                {
                    if (i >= ri.data.players.Count)
                    {
                        listVipIcon[i].gameObject.SetActive(false);
                        listPlayerName[i].text = "????????????????";
                        tempVector3.x = listPlayerName[i].transform.localPosition.x;
                        tempVector3.z = listPlayerName[i].transform.localPosition.z;
                        tempVector3.y = 42;
                        listPlayerName[i].transform.localPosition = tempVector3;
                        listPlayerClan[i].text = "";
                        listPlayerScore[i].text = "?";
                        listPlayerWinrate[i].text = "?";
                        listPlayerFound[i].text = "";
                        listAvatar[i].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                    }
                    else
                    {
                        listVipIcon[i].gameObject.SetActive(false);
                        listPlayerName[i].text = "";
                        listPlayerClan[i].text = "";
                        listPlayerScore[i].text = "";
                        listPlayerWinrate[i].text = "";
                        listPlayerFound[i].text = I2.Loc.ScriptLocalization.title_player_found;
                        listAvatar[i].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                    }
                }
            }
        }

        countUpTime = 0;
        lConnectStatus2v2.text = I2.Loc.ScriptLocalization.msg_pvp_finding_opponents;
        coFinding2v2 = Finding2v2();
        StartCoroutine(coFinding2v2);
    }

    private void LoadUI()
    {
        isWaitForReady = false;
        btnBack.SetActive(true);
        sPlayerAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(CachePvp.GamecenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.GamecenterId))
                {
                    sPlayerAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.GamecenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.GamecenterId);
                    if (t != null)
                    {
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.GamecenterId, t);
                    }
                    else
                    {
                        sPlayerAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                    }
                    facebookIdPlayerP1 = CachePvp.GamecenterId;
                }
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(CachePvp.FacebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
                {
                    sPlayerAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
                }
                else
                {
                    facebookIdPlayerP1 = CachePvp.FacebookId;
                    string query = "https://graph.facebook.com/" + CachePvp.FacebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.FacebookId);
                }
            }
        }
        if (CachePvp.myVip == 0)
        {
            sVipIconPlayer.gameObject.SetActive(false);
        }
        else
        {
            sVipIconPlayer.gameObject.SetActive(true);
            sVipIconPlayer.spriteName = "Icon__VIP_" + (CachePvp.myVip - 1);
        }
        lElo.text = "";
        lEloWin.text = "";
        lEloDraw.text = "";
        lEloLose.text = "";
        lConnectStatus.text = I2.Loc.ScriptLocalization.msg_pvp_connect_to_server;
        lConnectSeconds.gameObject.SetActive(false);

        lPlayerName.text = string.IsNullOrEmpty(CachePvp.Name) ? CachePvp.Code : CachePvp.Name;
        tempVector3.x = lPlayerName.transform.localPosition.x;
        tempVector3.z = lPlayerName.transform.localPosition.z;
        if (!string.IsNullOrEmpty(CachePvp.PlayerClan.name))
        {
            lPlayerNameClan.text = CachePvp.PlayerClan.name;
            tempVector3.y = 44;
        }
        else
        {
            lPlayerNameClan.text = "";
            tempVector3.y = 24;
        }
        lPlayerName.transform.localPosition = tempVector3;
        lPlayerElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.PlayerLevel.elo);
        lPlayerWinRate.text = I2.Loc.ScriptLocalization.win_rate.Replace("%{percent_number}", CachePvp.GetWinrateText());
        int rank = CachePvp.dictionaryRank[CachePvp.PlayerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lMedalRankPlayer.text = rankTitle + " " + CachePvp.ConvertChina(CachePvp.PlayerLevel.name);
        sMedalRankPlayer.spriteName = "PVP_rank_" + rank;

        sVipIconOpponent.gameObject.SetActive(false);
        lOpponentName.text = "????????????????";
        tempVector3.x = lOpponentName.transform.localPosition.x;
        tempVector3.z = lOpponentName.transform.localPosition.z;
        tempVector3.y = 42;
        lOpponentName.transform.localPosition = tempVector3;
        lOpponentNameClan.text = "";
        lOpponentElo.text = "?";
        lOpponentWinRate.text = "?";
        sMedalRankOpponent.spriteName = "PVP_rank_0";
        lMedalRankOpponent.text = "?";
        sOpponentAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;

        btnRefind.SetActive(false);
    }

    public void SCFindOpponentCallback(FindState findState, int timeout, string message)
    {
        switch (findState)
        {
            case FindState.SUCCESS:
                break;

            case FindState.ERROR:
                PopupManager.Instance.ShowToast("Server is busy now!");
                CannotFindOpponent();
                break;

            case FindState.WAITING:
                StartFindOpponent(timeout);
                break;

            case FindState.TIMEOUT:
                CannotFindOpponent();
                break;
            default:
                PopupManagerCuong.Instance.ShowTextNotifiToast(message, false, 1.5f);
                break;
        }
    }

    private void ShowOpponentPlayerOnUI()
    {
        Debug.Log(CachePvp.Opponent.facebookId);
        Debug.Log(CachePvp.Opponent.name);
        Debug.Log(CachePvp.Opponent.country);

        sOpponentAvatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
        bool showImageFB = true;
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(CachePvp.Opponent.appCenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.Opponent.appCenterId))
                {
                    showImageFB = false;
                    facebookIdOpponentP1 = CachePvp.Opponent.appCenterId;
                    sOpponentAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.Opponent.appCenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.Opponent.appCenterId);
                    if (t != null)
                    {
                        showImageFB = false;
                        facebookIdOpponentP1 = CachePvp.Opponent.appCenterId;
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.Opponent.appCenterId, t);
                    }
                }
            }
        }
        if (showImageFB)
        {
            if (!string.IsNullOrEmpty(CachePvp.Opponent.facebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.Opponent.facebookId))
                {
                    sOpponentAvatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.Opponent.facebookId];
                }
                else
                {
                    facebookIdOpponentP1 = CachePvp.Opponent.facebookId;
                    string query = "https://graph.facebook.com/" + CachePvp.Opponent.facebookId + "/picture?type=small&width=135&height=135";
                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.Opponent.facebookId);
                }
            }
        }
        lOpponentName.text = CachePvp.Opponent.name;
        Debug.Log("lOpponentNameClan : " + lOpponentNameClan);
        tempVector3.x = lOpponentName.transform.localPosition.x;
        tempVector3.z = lOpponentName.transform.localPosition.z;

        if (CachePvp.Opponent.playerClan != null)
        {
            lOpponentNameClan.text = CachePvp.Opponent.playerClan.name;
            tempVector3.y = 44;
        }
        else
        {
            lOpponentNameClan.text = "";
            tempVector3.y = 24;
        }
        lOpponentName.transform.localPosition = tempVector3;
        lOpponentElo.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.Opponent.playerLevel.elo);
        lOpponentWinRate.text = I2.Loc.ScriptLocalization.win_rate.Replace("%{percent_number}", CachePvp.Opponent.GetWinrateText());
        int rank = CachePvp.dictionaryRank[CachePvp.Opponent.playerLevel.code];
        MedalRankTitle rankTitle = (MedalRankTitle)(rank);
        lMedalRankOpponent.text = rankTitle + " " + CachePvp.ConvertChina(CachePvp.Opponent.playerLevel.name);
        sMedalRankOpponent.spriteName = "PVP_rank_" + rank;

        int vip = CachePvp.GetVipFromVipPoint(CachePvp.Opponent.vip);
        Debug.Log("Opponent.vip : " + vip);
        if (vip == 0)
        {
            sVipIconOpponent.gameObject.SetActive(false);
        }
        else
        {
            sVipIconOpponent.gameObject.SetActive(true);
            sVipIconOpponent.spriteName = "Icon__VIP_" + (vip - 1);
        }
    }

    private void StartFindOpponent(int timeout)
    {
        lConnectStatus.text = I2.Loc.ScriptLocalization.msg_pvp_finding_opponents;
        coFindingCountdown = FindingCountdown(timeout);
        StartCoroutine(coFindingCountdown);
    }

    private void CannotFindOpponent()
    {
        lConnectStatus.text = I2.Loc.ScriptLocalization.msg_pvp_no_opponents;
        lConnectSeconds.gameObject.SetActive(false);
        btnRefind.SetActive(true);
    }

    string mRoomId = "";

    public void StartLoadingGameScene(int map, int time, List<Player> players, string roomId, int type = CSFindOpponent.TYPE_PVP)
    {
        Debug.Log("roomId : " + roomId);
        //this.mRoomId = roomId;
        isWaitForReady = true;
        btnBack.SetActive(false);
        foreach (var player in players)
        {
            Debug.Log("facebook ID : " + player.facebookId);
            if (!CachePvp.Code.Equals(player.code))
            {
                CachePvp.Opponent = player;
            }
        }

        Debug.Log("ShowOpponentPlayerOnUI");
        ShowOpponentPlayerOnUI();

        if (coFindingCountdown != null)
        {
            StopCoroutine(coFindingCountdown);
        }
        lConnectStatus.text = "VS";
        lConnectSeconds.gameObject.SetActive(false);
        if (type == CSFindOpponent.TYPE_PVP)
        {
            if (map > 0)
            {
                GameContext.modeGamePlay = GameContext.ModeGamePlay.pvp;
                StartCoroutine(LoadScene("PVPLevel"));
                //StartCoroutine (LoadScene ("PVPLevel" + (map % mapNumber + 1)));
            }
        }
        else if (type == CSFindOpponent.TYPE_MEGA)
        {
            GameContext.modeGamePlay = GameContext.ModeGamePlay.Tournament;
            StartCoroutine(LoadScene("Tournament"));
        }
    }

    public void StopLoadingGameScene(string roomId)
    {
        Debug.Log("StopLoadingGameScene");
        CachePvp.roomId = "";
        //Debug.Log("roomId : " + roomId + " - mRoomID : " + mRoomId);
        //if (this.mRoomId.Equals(roomId))
        //{
        //CachePvp.Gold += MinhCacheGame.GetBettingAmountPvP();
        PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_room_destroyed);
        SceneManager.LoadScene("Home");
        //}
    }

    public void OnEndGame()
    {
        SceneManager.LoadScene("PVPEndGame");
    }

    public void OpponentOut()
    {
        StopAllCoroutines();
        PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.opponent_disconnect);
        if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
        {
            SceneManager.LoadScene("Home");
        }
        else
        {
            SceneManager.LoadScene("PVPMain");
        }
    }

    bool isWaitForReady = false;

    public void OnClickBtnBack()
    {
        if (!isWaitForReady)
        {
            //chua co ai
            //DontDestroyManager.Instance.panelCoinGemTop.SetActive(true);
            //DontDestroyManager.Instance.panelVideoTopBtn.SetActive(true);
            if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
            {
                new CSCancelFindOpponent(CachePvp.typePVP).Send();
                SceneManager.LoadScene("Home");
                return;
            }
            else if (CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
            {
                new CSCoopPvPStopMatching().Send();
                SceneManager.LoadScene("Home");
                return;
            }

            PopupManagerCuong.Instance.ShowPanelBtnVideoTop();
            PopupManagerCuong.Instance.ShowPanelCoinGem();

            PvPUIManager.Instance.popupFindPvP.gameObject.SetActive(false);
            PvPUIManager.Instance.popupMainPvP.gameObject.SetActive(true);

            new CSCancelFindOpponent(CachePvp.typePVP).Send();
        }
        else
        {
            //co nguoi
            //if (!isLoaded)
            //{
            //    isWaitForReady = false;
            //    StopAllCoroutines();
            //    new CSPlayerOut().Send();
            //    if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
            //    {
            //        SceneManager.LoadScene("Home");
            //    }
            //    else
            //    {
            //        SceneManager.LoadScene("PVPMain");
            //    }
            //}
        }
    }

    public void OnClickBtnRefind()
    {
        if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
        {
            new CSFindOpponent(MinhCacheGame.GetBettingAmountPvP(), CacheGame.GetSpaceShipUserSelected(), CachePvp.typePVP).Send();
        }
        else if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
        {
            MinhCacheGame.SetBettingAmountPvP(0);
            new CSFindOpponent(MinhCacheGame.GetBettingAmountPvP(), CacheGame.GetSpaceShipTournament(), CachePvp.typePVP).Send();
        }
        btnRefind.SetActive(false);
    }

    private void OnEnable()
    {
        PopupManager.Instance.HideInviteFriendsPopup();
        if (CachePvp.isRoomDestroyed)
        {
            Debug.Log("CachePvp.isRoomDestroyed");
            CachePvp.isRoomDestroyed = false;
            if (!string.IsNullOrEmpty(CachePvp.roomId))
            {
                StopLoadingGameScene(CachePvp.roomId);
                return;
            }
        }
        Debug.Log("OnEnable findPVP");
        facebookIdOpponentP1 = "";
        isLoaded = false;
        isWaitForReady = false;
        btnBack.SetActive(true);
        if (CachePvp.typePVP != CSFindOpponent.TYPE_2V2)
        {
            pvpNormal.SetActive(true);
            pvp2v2.SetActive(false);
            if (!string.IsNullOrEmpty(CachePvp.roomId))
            {
                isWaitForReady = true;
                btnBack.SetActive(false);
                LeanTween.delayedCall(1f, () =>
                {
                    StartLoadingGameScene(CachePvp.map, CachePvp.delayTime, CachePvp.players, CachePvp.roomId, CachePvp.typePVP);
                    CachePvp.roomId = "";
                    CachePvp.delayTime = 0;
                    CachePvp.map = 0;
                    CachePvp.players = null;
                }).setIgnoreTimeScale(true);
            }
            LoadUI();
        }
        else
        {
            pvpNormal.SetActive(false);
            pvp2v2.SetActive(true);
            LoadUI2v2();
        }

        if (PopupManager.Instance.goToPVP)
        {
            PopupManager.Instance.goToPVP = false;
            LoadUI(PopupManager.Instance.cachedPlayer);
        }
        else
        {
            if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
            {
                new CSFindOpponent(MinhCacheGame.GetBettingAmountPvP(), CacheGame.GetSpaceShipUserSelected(), CachePvp.typePVP).Send();
            }
            else if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
            {
                MinhCacheGame.SetBettingAmountPvP(0);
                new CSFindOpponent(MinhCacheGame.GetBettingAmountPvP(), CacheGame.GetSpaceShipTournament(), CachePvp.typePVP).Send();
            }
            else if (CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
            {
                if (CachePvp.coopPvPRoomInfo != null && CachePvp.coopPvPRoomInfo.data.state == CoopPvPRoomData.WAIT_FOR_READY)
                {
                    if (CachePvp.coopPvPRoomInfo.data.waveCacheIndex != -1 && CachePvp.coopPvPRoomInfo.data.waveCacheData != null && CachePvp.coopPvPRoomInfo.data.waveCacheData.Length > 0)
                    {
                        PvPDataHelper.Instance.SetWaveCacheIndexForPvP(CachePvp.coopPvPRoomInfo.data.waveCacheIndex);
                        PvPDataHelper.Instance.SetDataForPvP(CachePvp.coopPvPRoomInfo.data.waveCacheData);
                    }
                    else
                    {
                        PvPDataHelper.Instance.SetWaveCacheIndexForPvP(PvPDataHelper.Instance.GetMyWaveCacheIndex(CachePvp.typePVP));
                        PvPDataHelper.Instance.SetDataForPvP(PvPDataHelper.Instance.GetMyDataString(CachePvp.typePVP));
                    }
                    LoadScene2v2();
                }
            }
        }
        PvpUtil.SendUpdatePlayer();
    }

    private void OnDisable()
    {
        if (coFindingCountdown != null)
        {
            StopCoroutine(coFindingCountdown);
        }
        if (coFinding2v2 != null)
        {
            StopCoroutine(coFinding2v2);
        }
    }

    IEnumerator coFindingCountdown;
    IEnumerator coFinding2v2;

    int countUpTime = 0;

    IEnumerator Finding2v2()
    {
        while (true)
        {
            lConnectSeconds2v2.gameObject.SetActive(true);
            lConnectSeconds2v2.text = countUpTime + "";
            countUpTime++;
            yield return new WaitForSecondsRealtime(1f);
        }
    }

    IEnumerator FindingCountdown(int seconds)
    {
        lConnectSeconds.gameObject.SetActive(true);
        for (int i = seconds; i > 0; i--)
        {
            lConnectSeconds.text = i + "";
            yield return new WaitForSecondsRealtime(1f);
        }
        //CannotFindOpponent();
    }

    private AsyncOperation asyncOperation;

    bool isLoaded = false;

    IEnumerator LoadScene(string sceneName)
    {
        asyncOperation = SceneManager.LoadSceneAsync(sceneName);
        asyncOperation.allowSceneActivation = false;
        while (!asyncOperation.isDone)
        {
            lConnectStatus.text = I2.Loc.ScriptLocalization.loading_progress.Replace("%{percent}", (asyncOperation.progress * 100).ToString("0.00"));
            if (asyncOperation.progress >= 0.9f)
            {//Không hiểu sao unity để 0.9
                lConnectStatus.text = I2.Loc.ScriptLocalization.ready;
                isLoaded = true;
                LeanTween.delayedCall(4f, () =>
                {
                    isLoaded = false;
                }).setIgnoreTimeScale(true);
                //LeanTween.delayedCall(0.3f, () =>
                //{
                //if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
                //{
                lConnectStatus.text = "";
                lConnectSeconds.text = "";
                lElo.text = I2.Loc.ScriptLocalization.elo_key;
                lEloWin.text = I2.Loc.ScriptLocalization.win + ": " + (CachePvp.eloWin > 0 ? "+" + CachePvp.eloWin.ToString() : CachePvp.eloWin.ToString());
                lEloDraw.text = I2.Loc.ScriptLocalization.draw + ": " + (CachePvp.eloDraw > 0 ? "+" + CachePvp.eloDraw.ToString() : CachePvp.eloDraw.ToString());
                lEloLose.text = I2.Loc.ScriptLocalization.lose + ": " + (CachePvp.eloLose > 0 ? "+" + CachePvp.eloLose.ToString() : CachePvp.eloLose.ToString());
                //}
                //}).setIgnoreTimeScale(true);
                LeanTween.delayedCall(0.3f, () =>
                {
                    asyncOperation.allowSceneActivation = true;
                }).setIgnoreTimeScale(true);
                break;
            }
            yield return null;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnClickBtnBack();
        }
    }
}
