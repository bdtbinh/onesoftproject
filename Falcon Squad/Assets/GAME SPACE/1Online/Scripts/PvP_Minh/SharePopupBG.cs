﻿using UnityEngine;

[ExecuteInEditMode]
public class SharePopupBG : MonoBehaviour {

    [SerializeField]
    private GameObject shareBackground;

    private void OnEnable()
    {
        shareBackground.SetActive(true);
    }

    private void OnDisable()
    {
        shareBackground.SetActive(false);
    }

}
