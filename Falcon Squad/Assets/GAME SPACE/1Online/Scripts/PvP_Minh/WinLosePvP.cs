﻿using com.ootii.Messages;
using Mp.Pvp;
using System;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLosePvP : MonoBehaviour
{
    public GameObject gNormal;
    public GameObject g2v2;
    public GameObject gBottomNormal;
    public GameObject gBottom2v2;

    //2v2
    public List<UILabel> listLabelTitleTeam;
    public List<UILabel> listLabelScoreTeam;
    public List<UILabel> listLabelName;
    public List<UILabel> listLabelNameClan;
    public List<UILabel> listLabelID;
    public List<UILabel> listLabelSeasonScore;
    public List<UILabel> listLabelScorePlayer;
    public List<UILabel> listLabelRankPlayer;

    public List<GameObject> listSpriteBackgroundMe;

    public List<UI2DSprite> listAvatarPlayers;
    public List<UISprite> listFlagPlayers;
    public List<UISprite> listVipIconPlayers;
    public List<UISprite> listBarScorePlayers;

    public List<UISprite> listSpriteStatusPlayers;
    public List<UILabel> listLabelStatusPlayers;

    public GameObject buttonRematchActive;
    public GameObject buttonRematchDeactive;

    public UILabel lTime2v2;

    //normal
    public UILabel lTimeNumber;
    public UISprite sVipIconOpponent;
    public UILabel scoreText;
    public UILabel goldText;
    public UISprite sTitle;
    public UILabel lTitle;
    public UILabel lElo;
    public UILabel lRank;
    public UISprite sRank;

    public GameObject gResult;
    public GameObject gUserRank;
    public GameObject gTournament;
    public UILabel lWinNeedValue;

    public GameObject buttonReplay;
    public GameObject buttonRank;

    public UILabel lGlory;

    public UILabel lBonusMedal;
    public UISprite sBonusMedal;

    public List<UISprite> listMedalActive;
    public List<GameObject> listMedalInactive;
    public List<ParticleSystem> listMedalEffectActive;
    public List<ParticleSystem> listMedalEffectInactive;

    public UI2DSprite sAvatarOpponent;
    public UILabel lNameOpponent;
    public UILabel lNameClanOpponent;
    public UILabel lEloOpponent;
    public UISprite sRankOpponent;
    public UILabel lRankOpponent;
    public UILabel lIdOpponent;
    public UILabel lScoreOpponent;

    private const string MEDAL_ENABLE_SPRITE = "PVP_rank_medal_1";
    private const string MEDAL_DISABLE_SPRITE = "PVP_rank_medal_0";

    private const string STATUS_READY = "status_laber_ready";
    private const string STATUS_QUIT = "status_laber_quit";

    private void Awake()
    {
        new CSSceneLoaded(CSSceneLoaded.SCENE_PVP_END).Send();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.PVP.SCCoopPvPRematch.ToString(), OnSCCoopPvPRematch, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    Vector3 tempVector3;
    string facebookIdOpponent;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.PVP.SCCoopPvPRematch.ToString(), OnSCCoopPvPRematch, true);
        SoundManager.StopMainBG();
        SoundManager.StopMain2BG();
        SoundManager.PlayHomeBG();
        readyClickBack = true;
        RefreshUI();
    }

    bool isTeammateOut;

    private void OnSCCoopPvPRematch(IMessage rMessage)
    {
        SCCoopPvPRematch cr = rMessage.Data as SCCoopPvPRematch;
        if (cr.status == SCCoopPvPRematch.SUCCESS)
        {
            if (cr.code.Equals(CachePvp.MyTeammateCode.ToString()))
            {
                string ready = I2.Loc.ScriptLocalization.ready;
                string sprite = STATUS_READY;
                if (cr.type == SCCoopPvPRematch.TYPE_NOT_REMATCH)
                {
                    ready = I2.Loc.ScriptLocalization.quit;
                    sprite = STATUS_QUIT;
                    isTeammateOut = true;
                    buttonRematchActive.SetActive(false);
                    buttonRematchDeactive.SetActive(true);
                }
                Debug.Log("my team index : " + CachePvp.myTeamIndex);
                Debug.Log("my team result : " + CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].result);

                Debug.Log("my index : " + CachePvp.myIndex);
                switch (CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].result)
                {
                    case CoopPvPTeamData.WIN:
                        if (CachePvp.myIndex == 0)
                        {
                            listSpriteStatusPlayers[1].spriteName = sprite;
                            listSpriteStatusPlayers[1].gameObject.SetActive(true);
                            listLabelStatusPlayers[1].text = ready;
                        }
                        else if (CachePvp.myIndex == 1)
                        {
                            listSpriteStatusPlayers[0].spriteName = sprite;
                            listSpriteStatusPlayers[0].gameObject.SetActive(true);
                            listLabelStatusPlayers[0].text = ready;
                        }
                        break;
                    case CoopPvPTeamData.LOSE:
                        if (CachePvp.myIndex == 0)
                        {
                            listSpriteStatusPlayers[3].spriteName = sprite;
                            listSpriteStatusPlayers[3].gameObject.SetActive(true);
                            listLabelStatusPlayers[3].text = ready;
                        }
                        else if (CachePvp.myIndex == 1)
                        {
                            listSpriteStatusPlayers[2].spriteName = sprite;
                            listSpriteStatusPlayers[2].gameObject.SetActive(true);
                            listLabelStatusPlayers[2].text = ready;
                        }
                        break;
                    case CoopPvPTeamData.DRAWN:
                        if (CachePvp.myIndex == 0)
                        {
                            listSpriteStatusPlayers[CachePvp.myTeamIndex * 2 + 1].spriteName = sprite;
                            listSpriteStatusPlayers[CachePvp.myTeamIndex * 2 + 1].gameObject.SetActive(true);
                            listLabelStatusPlayers[CachePvp.myTeamIndex * 2 + 1].text = ready;
                        }
                        else if (CachePvp.myIndex == 1)
                        {
                            listSpriteStatusPlayers[CachePvp.myTeamIndex * 2].spriteName = sprite;
                            listSpriteStatusPlayers[CachePvp.myTeamIndex * 2].gameObject.SetActive(true);
                            listLabelStatusPlayers[CachePvp.myTeamIndex * 2].text = ready;
                        }
                        break;
                    default:
                        break;
                }
            }
            else if (cr.code.Equals(CachePvp.Code))
            {
                string ready = I2.Loc.ScriptLocalization.ready;
                string sprite = STATUS_READY;
                switch (CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].result)
                {
                    case CoopPvPTeamData.WIN:
                        listSpriteStatusPlayers[CachePvp.myIndex].spriteName = sprite;
                        listSpriteStatusPlayers[CachePvp.myIndex].gameObject.SetActive(true);
                        listLabelStatusPlayers[CachePvp.myIndex].text = ready;
                        break;
                    case CoopPvPTeamData.LOSE:
                        listSpriteStatusPlayers[CachePvp.myIndex + 2].spriteName = sprite;
                        listSpriteStatusPlayers[CachePvp.myIndex + 2].gameObject.SetActive(true);
                        listLabelStatusPlayers[CachePvp.myIndex + 2].text = ready;
                        break;
                    case CoopPvPTeamData.DRAWN:
                        listSpriteStatusPlayers[CachePvp.myTeamIndex * 2 + CachePvp.myIndex].spriteName = sprite;
                        listSpriteStatusPlayers[CachePvp.myTeamIndex * 2 + CachePvp.myIndex].gameObject.SetActive(true);
                        listLabelStatusPlayers[CachePvp.myTeamIndex * 2 + CachePvp.myIndex].text = ready;
                        break;
                    default:
                        break;
                }
                buttonRematchActive.SetActive(false);
                buttonRematchDeactive.SetActive(true);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(cr.message, false, 1.5f);
            if (cr.code.Equals(CachePvp.Code))
            {
                if (!isTeammateOut)
                {
                    buttonRematchActive.SetActive(true);
                    buttonRematchDeactive.SetActive(false);
                }
            }
        }
    }

    private void RefreshUI()
    {
        GameContext.isInGameCampaign = false;
        if (CachePvp.typePVP != CSFindOpponent.TYPE_2V2)
        {
            gNormal.SetActive(true);
            gBottomNormal.SetActive(true);
            g2v2.SetActive(false);
            gBottom2v2.SetActive(false);
            CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayPvpEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayPvpEasy.ToString()) + 1);
            CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayPvpHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayPvpHard.ToString()) + 1);
            lTimeNumber.text = (CachePvp.PlayTime / 60).ToString("00") + ":" + (CachePvp.PlayTime % 60).ToString("00");

            if (CachePvp.MyResult != null)
            {
                scoreText.text = GameContext.FormatNumber(CachePvp.MyResult.score);
                goldText.text = GameContext.FormatNumber(CachePvp.MyResult.goldBet);

                if (CachePvp.MyResult.result == PlayerResult.LOSE)
                {
                    sTitle.spriteName = "ingame_title_3";
                    lTitle.text = I2.Loc.ScriptLocalization.lose;
                    goldText.text = GameContext.FormatNumber(CachePvp.MyResult.goldBet);
                    lElo.text = CachePvp.MyResult.exp.ToString();
                }
                else if (CachePvp.MyResult.result == PlayerResult.WIN)
                {
                    sTitle.spriteName = "ingame_title_1";
                    lTitle.text = I2.Loc.ScriptLocalization.win;
                    goldText.text = "+" + GameContext.FormatNumber(CachePvp.MyResult.goldBet);
                    lElo.text = "+" + CachePvp.MyResult.exp.ToString();
                    CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.WinPvpEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.WinPvpEasy.ToString()) + 1);
                    CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.WinPvpHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.WinPvpHard.ToString()) + 1);

                }
                else if (CachePvp.MyResult.result == PlayerResult.DRAWN)
                {
                    sTitle.spriteName = "ingame_title_1";
                    lTitle.text = I2.Loc.ScriptLocalization.draw;
                    goldText.text = "+0";
                    lElo.text = CachePvp.MyResult.exp.ToString();
                }
                if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
                {
                    ShowEffect(CachePvp.PlayerLevel.code, CachePvp.ConvertChina(CachePvp.PlayerLevel.name), CachePvp.MyResult.playerLevel.code, CachePvp.ConvertChina(CachePvp.MyResult.playerLevel.name), CachePvp.PlayerLevel.elo, CachePvp.MyResult.playerLevel.elo);
                }
                else
                {
                    lWinNeedValue.text = CachePvp.MyResult.tournamentInfo.nextRewardIn.ToString();
                }
            }

            if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
            {
                gResult.SetActive(true);
                gUserRank.SetActive(true);
                gTournament.SetActive(false);
                if (MinhCacheGame.GetBettingAmountPvP() == 0)
                {
                    buttonReplay.SetActive(true);
                }
                else
                {
                    buttonReplay.SetActive(false);
                }
                buttonRank.SetActive(true);
            }
            else
            {
                gResult.SetActive(false);
                gUserRank.SetActive(false);
                gTournament.SetActive(true);
                buttonReplay.SetActive(false);
                buttonRank.SetActive(false);
            }

            if (CachePvp.Opponent != null)
            {
                lNameOpponent.text = CachePvp.Opponent.name;
                tempVector3.x = lNameClanOpponent.transform.localPosition.x;
                tempVector3.z = lNameClanOpponent.transform.localPosition.z;
                if (CachePvp.Opponent.playerClan != null)
                {
                    lNameClanOpponent.text = CachePvp.Opponent.playerClan.name;
                    tempVector3.y = 24.5f;
                }
                else
                {
                    lNameClanOpponent.text = "";
                    tempVector3.y = 40.7f;
                }
                lNameClanOpponent.transform.localPosition = tempVector3;
                if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
                {
                    lEloOpponent.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.Opponent.playerLevel.elo);
                }
                else
                {
                    lEloOpponent.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.Opponent.megaPlayerLevel.elo);
                }
                if (CachePvp.OpponentResult != null)
                {
                    if (CachePvp.OpponentResult.result == PlayerResult.WIN)
                    {
                        CachePvp.Opponent.win++;
                    }
                    else if (CachePvp.OpponentResult.result == PlayerResult.LOSE)
                    {
                        CachePvp.Opponent.lose++;
                    }
                    lScoreOpponent.text = I2.Loc.ScriptLocalization.score + ": " + GameContext.FormatNumber(CachePvp.OpponentResult.score);
                }
                lIdOpponent.text = I2.Loc.ScriptLocalization.id_key + ": " + CachePvp.Opponent.code;
                int rank = CachePvp.dictionaryRank[CachePvp.Opponent.playerLevel.code];
                MedalRankTitle rankTitle = (MedalRankTitle)(rank);
                lRankOpponent.text = rankTitle + " " + CachePvp.ConvertChina(CachePvp.Opponent.playerLevel.name);
                sRankOpponent.spriteName = "PVP_rank_" + rank;

                sAvatarOpponent.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                bool showImageFB = true;
                if (GameContext.IS_CHINA_VERSION)
                {
                    if (!string.IsNullOrEmpty(CachePvp.Opponent.appCenterId))
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.Opponent.appCenterId))
                        {
                            showImageFB = false;
                            facebookIdOpponent = CachePvp.Opponent.appCenterId;
                            sAvatarOpponent.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.Opponent.appCenterId];
                        }
                        else
                        {
                            //Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.Opponent.appCenterId);
                            //if (t != null)
                            //{
                            //    showImageFB = false;
                            //    facebookIdOpponent = CachePvp.Opponent.appCenterId;
                            //    CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.Opponent.appCenterId, t);
                            //}
                        }
                    }
                }
                if (showImageFB)
                {
                    if (!string.IsNullOrEmpty(CachePvp.Opponent.facebookId))
                    {
                        facebookIdOpponent = CachePvp.Opponent.facebookId;
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.Opponent.facebookId))
                        {
                            sAvatarOpponent.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.Opponent.facebookId];
                        }
                        else
                        {
                            string query = "https://graph.facebook.com/" + CachePvp.Opponent.facebookId + "/picture?type=small&width=135&height=135";
                            CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.Opponent.facebookId);
                        }
                    }
                }
                int vip = CachePvp.GetVipFromVipPoint(CachePvp.Opponent.vip);
                Debug.Log("Opponent.vip : " + vip);
                if (vip == 0)
                {
                    sVipIconOpponent.gameObject.SetActive(false);
                }
                else
                {
                    sVipIconOpponent.gameObject.SetActive(true);
                    sVipIconOpponent.spriteName = "Icon__VIP_" + (vip - 1);
                }
            }
        }
        else
        {
            gNormal.SetActive(false);
            gBottomNormal.SetActive(false);
            g2v2.SetActive(true);
            gBottom2v2.SetActive(true);
            lTime2v2.text = (CachePvp.coopPvPRoomInfo.data.playTime / 60).ToString("00") + ":" + (CachePvp.coopPvPRoomInfo.data.playTime % 60).ToString("00");
            for (int i = 0; i < listSpriteBackgroundMe.Count; i++)
            {
                listSpriteBackgroundMe[i].SetActive(false);
            }
            //list temp for rank
            //status
            for (int i = 0; i < listSpriteStatusPlayers.Count; i++)
            {
                listSpriteStatusPlayers[i].gameObject.SetActive(false);
                listAvatarPlayers[i].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            }
            List<int> listTempInt = new List<int>();
            isTeammateOut = false;
            if (CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].members[CachePvp.myTeammateIndex].state == CoopPvPPlayerData.STATE_OUT)
            {
                isTeammateOut = true;
            }
            for (int i = 0; i < CachePvp.coopPvPRoomInfo.data.teams.Count; i++)
            {
                for (int j = 0; j < CachePvp.coopPvPRoomInfo.data.teams[i].members.Count; j++)
                {
                    listTempInt.Add(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].score);
                }
            }
            buttonRematchActive.SetActive(true);
            buttonRematchDeactive.SetActive(false);
            if (CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].result == CoopPvPTeamData.WIN)
            {
                CachePvp.SetGold(CachePvp.GetGold() + CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].members[CachePvp.myIndex].goldBet * 2, "pvp2v2", "pvp2v2");
            }
            else if (CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].result == CoopPvPTeamData.DRAWN)
            {
                CachePvp.SetGold(CachePvp.GetGold() + CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].members[CachePvp.myIndex].goldBet, "pvp2v2", "pvp2v2");
            }
            PvpUtil.SendUpdatePlayer("WinLosePVP");
            if (isTeammateOut)
            {
                string ready = I2.Loc.ScriptLocalization.quit;
                string sprite = STATUS_QUIT;
                switch (CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].result)
                {
                    case CoopPvPTeamData.WIN:
                        if (CachePvp.myIndex == 0)
                        {
                            listSpriteStatusPlayers[1].spriteName = sprite;
                            listSpriteStatusPlayers[1].gameObject.SetActive(true);
                            listLabelStatusPlayers[1].text = ready;
                        }
                        else if (CachePvp.myIndex == 1)
                        {
                            listSpriteStatusPlayers[0].spriteName = sprite;
                            listSpriteStatusPlayers[0].gameObject.SetActive(true);
                            listLabelStatusPlayers[0].text = ready;
                        }
                        break;
                    case CoopPvPTeamData.LOSE:
                        if (CachePvp.myIndex == 0)
                        {
                            listSpriteStatusPlayers[3].spriteName = sprite;
                            listSpriteStatusPlayers[3].gameObject.SetActive(true);
                            listLabelStatusPlayers[3].text = ready;
                        }
                        else if (CachePvp.myIndex == 1)
                        {
                            listSpriteStatusPlayers[2].spriteName = sprite;
                            listSpriteStatusPlayers[2].gameObject.SetActive(true);
                            listLabelStatusPlayers[2].text = ready;
                        }
                        break;
                    case CoopPvPTeamData.DRAWN:
                        if (CachePvp.myIndex == 0)
                        {
                            listSpriteStatusPlayers[CachePvp.myTeamIndex * 2 + 1].spriteName = sprite;
                            listSpriteStatusPlayers[CachePvp.myTeamIndex * 2 + 1].gameObject.SetActive(true);
                            listLabelStatusPlayers[CachePvp.myTeamIndex * 2 + 1].text = ready;
                        }
                        else if (CachePvp.myIndex == 1)
                        {
                            listSpriteStatusPlayers[CachePvp.myTeamIndex * 2].spriteName = sprite;
                            listSpriteStatusPlayers[CachePvp.myTeamIndex * 2].gameObject.SetActive(true);
                            listLabelStatusPlayers[CachePvp.myTeamIndex * 2].text = ready;
                        }
                        break;
                    default:
                        break;
                }
                buttonRematchActive.SetActive(false);
                buttonRematchDeactive.SetActive(true);
            }
            listTempInt.Sort();
            listTempInt.Reverse();
            //title + score team
            for (int i = 0; i < CachePvp.coopPvPRoomInfo.data.teams.Count; i++)
            {
                if (CachePvp.coopPvPRoomInfo.data.teams[i].result == CoopPvPTeamData.WIN)
                {
                    listLabelScoreTeam[0].text = GameContext.FormatNumber(CachePvp.coopPvPRoomInfo.data.teams[i].score);
                    listLabelTitleTeam[0].text = I2.Loc.ScriptLocalization.victory;
                    for (int j = 0; j < CachePvp.coopPvPRoomInfo.data.teams[i].members.Count; j++)
                    {
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.code.Equals(CachePvp.Code))
                        {
                            listSpriteBackgroundMe[j].SetActive(true);
                        }
                        //name player
                        if (!string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.name))
                        {
                            listLabelName[j].text = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.name;
                        }
                        else
                        {
                            listLabelName[j].text = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.code;
                        }
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.playerClan != null && !string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.playerClan.name))
                        {
                            //name clan
                            listLabelNameClan[j].text = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.playerClan.name;
                        }
                        else
                        {
                            listLabelNameClan[j].text = "";
                        }
                        //id
                        listLabelID[j].text = I2.Loc.ScriptLocalization.id_key + ": " + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.code;
                        //season score
                        string plus = "";
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].seasonScoreRewards >= 0)
                        {
                            plus = " [00ff00](+" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].seasonScoreRewards + ")[-]    ";
                        }
                        else
                        {
                            plus = " [ff0000](" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].seasonScoreRewards + ")[-]    ";
                        }
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.pvP2vs2SeasonData != null)
                        {
                            listLabelSeasonScore[j].text = I2.Loc.ScriptLocalization.season_score + ": " + GameContext.FormatNumber(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.pvP2vs2SeasonData.score) + plus + I2.Loc.ScriptLocalization.gold + ": +" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].goldBet;
                        }
                        //score
                        listLabelScorePlayer[j].text = GameContext.FormatNumber(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].score);
                        //flag
                        listFlagPlayers[j].spriteName = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.country.ToLower();
                        //vip
                        int vip = CachePvp.GetVipFromVipPoint(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.vip);
                        if (vip == 0)
                        {
                            listVipIconPlayers[j].gameObject.SetActive(false);
                        }
                        else
                        {
                            listVipIconPlayers[j].gameObject.SetActive(true);
                            listVipIconPlayers[j].spriteName = "Icon__VIP_" + (vip - 1);
                        }
                        //avatar
                        bool showImageFB = true;
                        if (GameContext.IS_CHINA_VERSION)
                        {
                            if (!string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId))
                            {
                                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId))
                                {
                                    showImageFB = false;
                                    if (j == 0)
                                    {
                                        facebookIdPlayer1 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                    }
                                    else if (j == 1)
                                    {
                                        facebookIdPlayer2 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                    }
                                    listAvatarPlayers[j].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId];
                                }
                            }
                        }
                        if (showImageFB)
                        {
                            if (!string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId))
                            {
                                if (j == 0)
                                {
                                    facebookIdPlayer1 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId;
                                }
                                else if (j == 1)
                                {
                                    facebookIdPlayer2 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId;
                                }
                                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId))
                                {
                                    listAvatarPlayers[j].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId];
                                }
                                else
                                {
                                    string query = "https://graph.facebook.com/" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId + "/picture?type=small&width=135&height=135";
                                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId);
                                }
                            }
                        }
                        //rank
                        for (int k = 0; k < listTempInt.Count; k++)
                        {
                            if (listTempInt[k] == CachePvp.coopPvPRoomInfo.data.teams[i].members[j].score)
                            {
                                switch (k)
                                {
                                    case 0:
                                        listLabelRankPlayer[j].text = I2.Loc.ScriptLocalization.rank_1st;
                                        break;
                                    case 1:
                                        listLabelRankPlayer[j].text = I2.Loc.ScriptLocalization.rank_2nd;
                                        break;
                                    case 2:
                                        listLabelRankPlayer[j].text = I2.Loc.ScriptLocalization.rank_3rd;
                                        break;
                                    case 3:
                                        listLabelRankPlayer[j].text = I2.Loc.ScriptLocalization.rank_4th;
                                        break;
                                    default:
                                        break;
                                }
                                listBarScorePlayers[j].width = (int)((listTempInt[k] - listTempInt[3]) * 1.0f / (listTempInt[0] - listTempInt[3]) * (474 - 179) + 179);
                            }
                        }
                        //((current - min) / (max - min)) * (474-179) + 179
                    }
                }
                else if (CachePvp.coopPvPRoomInfo.data.teams[i].result == CoopPvPTeamData.LOSE)
                {
                    listLabelScoreTeam[1].text = GameContext.FormatNumber(CachePvp.coopPvPRoomInfo.data.teams[i].score);
                    listLabelTitleTeam[1].text = I2.Loc.ScriptLocalization.defeat;
                    for (int j = 0; j < CachePvp.coopPvPRoomInfo.data.teams[i].members.Count; j++)
                    {
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.code.Equals(CachePvp.Code))
                        {
                            listSpriteBackgroundMe[j + 2].SetActive(true);
                        }
                        //name player
                        if (!string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.name))
                        {
                            listLabelName[j + 2].text = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.name;
                        }
                        else
                        {
                            listLabelName[j + 2].text = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.code;
                        }
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.playerClan != null && !string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.playerClan.name))
                        {
                            //name clan
                            listLabelNameClan[j + 2].text = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.playerClan.name;
                        }
                        //id
                        listLabelID[j + 2].text = I2.Loc.ScriptLocalization.id_key + ": " + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.code;
                        //season score
                        string plus = "";
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].seasonScoreRewards >= 0)
                        {
                            plus = " [00ff00](+" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].seasonScoreRewards + ")[-]    ";
                        }
                        else
                        {
                            plus = " [ff0000](" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].seasonScoreRewards + ")[-]    ";
                        }
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.pvP2vs2SeasonData != null)
                        {
                            listLabelSeasonScore[j + 2].text = I2.Loc.ScriptLocalization.season_score + ": " + GameContext.FormatNumber(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.pvP2vs2SeasonData.score) + plus + I2.Loc.ScriptLocalization.gold + ": -" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].goldBet;
                        }
                        //score
                        listLabelScorePlayer[j + 2].text = GameContext.FormatNumber(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].score);
                        //flag
                        listFlagPlayers[j + 2].spriteName = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.country.ToLower();
                        //vip
                        int vip = CachePvp.GetVipFromVipPoint(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.vip);
                        if (vip == 0)
                        {
                            listVipIconPlayers[j + 2].gameObject.SetActive(false);
                        }
                        else
                        {
                            listVipIconPlayers[j + 2].gameObject.SetActive(true);
                            listVipIconPlayers[j + 2].spriteName = "Icon__VIP_" + (vip - 1);
                        }
                        //avatar
                        bool showImageFB = true;
                        if (GameContext.IS_CHINA_VERSION)
                        {
                            if (!string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId))
                            {
                                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId))
                                {
                                    showImageFB = false;
                                    if (j == 0)
                                    {
                                        facebookIdPlayer3 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                    }
                                    else if (j == 1)
                                    {
                                        facebookIdPlayer4 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                    }
                                    listAvatarPlayers[j + 2].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId];
                                }
                            }
                        }
                        if (showImageFB)
                        {
                            if (!string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId))
                            {
                                if (j == 0)
                                {
                                    facebookIdPlayer3 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId;
                                }
                                else if (j == 1)
                                {
                                    facebookIdPlayer4 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId;
                                }
                                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId))
                                {
                                    listAvatarPlayers[j + 2].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId];
                                }
                                else
                                {
                                    string query = "https://graph.facebook.com/" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId + "/picture?type=small&width=135&height=135";
                                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId);
                                }
                            }
                        }
                        //rank
                        for (int k = 0; k < listTempInt.Count; k++)
                        {
                            if (listTempInt[k] == CachePvp.coopPvPRoomInfo.data.teams[i].members[j].score)
                            {
                                switch (k)
                                {
                                    case 0:
                                        listLabelRankPlayer[j + 2].text = I2.Loc.ScriptLocalization.rank_1st;
                                        break;
                                    case 1:
                                        listLabelRankPlayer[j + 2].text = I2.Loc.ScriptLocalization.rank_2nd;
                                        break;
                                    case 2:
                                        listLabelRankPlayer[j + 2].text = I2.Loc.ScriptLocalization.rank_3rd;
                                        break;
                                    case 3:
                                        listLabelRankPlayer[j + 2].text = I2.Loc.ScriptLocalization.rank_4th;
                                        break;
                                    default:
                                        break;
                                }
                                listBarScorePlayers[j + 2].width = (int)((listTempInt[k] - listTempInt[3]) * 1.0f / (listTempInt[0] - listTempInt[3]) * (474 - 179) + 179);
                            }
                        }
                    }
                }
                else
                {
                    listLabelScoreTeam[0].text = GameContext.FormatNumber(CachePvp.coopPvPRoomInfo.data.teams[i].score);
                    listLabelScoreTeam[1].text = GameContext.FormatNumber(CachePvp.coopPvPRoomInfo.data.teams[i].score);
                    listLabelTitleTeam[0].text = I2.Loc.ScriptLocalization.draw;
                    listLabelTitleTeam[1].text = I2.Loc.ScriptLocalization.draw;
                    for (int j = 0; j < CachePvp.coopPvPRoomInfo.data.teams[i].members.Count; j++)
                    {
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.code.Equals(CachePvp.Code))
                        {
                            listSpriteBackgroundMe[j + i * 2].SetActive(true);
                        }
                        //name player
                        if (!string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.name))
                        {
                            listLabelName[j + i * 2].text = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.name;
                        }
                        else
                        {
                            listLabelName[j + i * 2].text = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.code;
                        }
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.playerClan != null && !string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.playerClan.name))
                        {
                            //name clan
                            listLabelNameClan[j + i * 2].text = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.playerClan.name;
                        }
                        else
                        {
                            listLabelNameClan[j + i * 2].text = "";
                        }
                        //id
                        listLabelID[j + i * 2].text = I2.Loc.ScriptLocalization.id_key + ": " + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.code;
                        //season score
                        string plus = "";
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].seasonScoreRewards >= 0)
                        {
                            plus = " [00ff00](+" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].seasonScoreRewards + ")[-]    ";
                        }
                        else
                        {
                            plus = " [ff0000](" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].seasonScoreRewards + ")[-]    ";
                        }
                        if (CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.pvP2vs2SeasonData != null)
                        {
                            listLabelSeasonScore[j + i * 2].text = I2.Loc.ScriptLocalization.season_score + ": " + GameContext.FormatNumber(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.pvP2vs2SeasonData.score) + plus + I2.Loc.ScriptLocalization.gold + ": +0";
                        }
                        //score
                        listLabelScorePlayer[j + i * 2].text = GameContext.FormatNumber(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].score);
                        //flag
                        listFlagPlayers[j + i * 2].spriteName = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.country.ToLower();
                        //vip
                        int vip = CachePvp.GetVipFromVipPoint(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.vip);
                        if (vip == 0)
                        {
                            listVipIconPlayers[j + i * 2].gameObject.SetActive(false);
                        }
                        else
                        {
                            listVipIconPlayers[j + i * 2].gameObject.SetActive(true);
                            listVipIconPlayers[j + i * 2].spriteName = "Icon__VIP_" + (vip - 1);
                        }
                        //avatar
                        bool showImageFB = true;
                        if (GameContext.IS_CHINA_VERSION)
                        {
                            if (!string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId))
                            {
                                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId))
                                {
                                    showImageFB = false;
                                    if (j + i * 2 == 0)
                                    {
                                        facebookIdPlayer1 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                    }
                                    else if (j + i * 2 == 1)
                                    {
                                        facebookIdPlayer2 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                    }
                                    else if (j + i * 2 == 2)
                                    {
                                        facebookIdPlayer3 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                    }
                                    else if (j + i * 2 == 3)
                                    {
                                        facebookIdPlayer4 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                    }
                                    listAvatarPlayers[j + i * 2].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId];
                                }
                            }
                        }
                        if (showImageFB)
                        {
                            if (!string.IsNullOrEmpty(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId))
                            {
                                if (j + i * 2 == 0)
                                {
                                    facebookIdPlayer1 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                }
                                else if (j + i * 2 == 1)
                                {
                                    facebookIdPlayer2 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                }
                                else if (j + i * 2 == 2)
                                {
                                    facebookIdPlayer3 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                }
                                else if (j + i * 2 == 3)
                                {
                                    facebookIdPlayer4 = CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.appCenterId;
                                }
                                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId))
                                {
                                    listAvatarPlayers[j + i * 2].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId];
                                }
                                else
                                {
                                    string query = "https://graph.facebook.com/" + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId + "/picture?type=small&width=135&height=135";
                                    CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.facebookId);
                                }
                            }
                        }
                        //rank
                        for (int k = 0; k < listTempInt.Count; k++)
                        {
                            if (listTempInt[k] == CachePvp.coopPvPRoomInfo.data.teams[i].members[j].score)
                            {
                                switch (k)
                                {
                                    case 0:
                                        listLabelRankPlayer[j + i * 2].text = I2.Loc.ScriptLocalization.rank_1st;
                                        break;
                                    case 1:
                                        listLabelRankPlayer[j + i * 2].text = I2.Loc.ScriptLocalization.rank_2nd;
                                        break;
                                    case 2:
                                        listLabelRankPlayer[j + i * 2].text = I2.Loc.ScriptLocalization.rank_3rd;
                                        break;
                                    case 3:
                                        listLabelRankPlayer[j + i * 2].text = I2.Loc.ScriptLocalization.rank_4th;
                                        break;
                                    default:
                                        break;
                                }
                                listBarScorePlayers[j + i * 2].width = (int)((listTempInt[k] - listTempInt[3]) * 1.0f / (listTempInt[0] - listTempInt[3]) * (474 - 179) + 179);
                            }
                        }
                    }
                }
            }
        }
    }

    public void ShowInfoP1()
    {

    }

    void ShowEffect(string currentRank, string currentSymbol, string targetRank, string targetSymbol, int startScore, int endScore)
    {
        int tempStartRank = CachePvp.dictionaryRank[currentRank];
        MedalRankTitle rankTitle = (MedalRankTitle)(tempStartRank);
        sRank.spriteName = "PVP_rank_" + tempStartRank;
        lRank.text = rankTitle + " " + currentSymbol;
        int number = 3;
        int currentStar = CachePvp.PlayerLevel.medal;
        int temp = startScore - (startScore % 10);
        if (currentRank == "BRONZE")
        {
            number = 3;
        }
        else if (currentRank == "SILVER")
        {
            number = 4;
        }
        else
        {
            number = 5;
        }

        for (int i = 0; i < listMedalActive.Count; i++)
        {
            listMedalEffectActive[i].gameObject.SetActive(false);
            listMedalEffectInactive[i].gameObject.SetActive(false);
            if (i < number)
            {
                listMedalInactive[i].SetActive(true);
                if (i < currentStar)
                {
                    listMedalActive[i].gameObject.SetActive(true);
                }
                else
                {
                    listMedalActive[i].gameObject.SetActive(false);
                }
            }
            else
            {
                listMedalActive[i].gameObject.SetActive(false);
                listMedalInactive[i].SetActive(false);
            }
        }

        sBonusMedal.fillAmount = (startScore % 10) / 10f;
        lBonusMedal.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.MyResult.playerLevel.elo);
        if (endScore > startScore)
        {
            LeanTween.value(gameObject, startScore, endScore, 2).setOnComplete(() =>
            {
                Debug.Log("done");
            }).setOnUpdate((float obj) =>
            {
                if (obj >= temp + 10)
                {
                    if (currentStar >= number)
                    {
                        //int tempTargetRank = dictRank[targetRank];
                        int tempTargetRank = CachePvp.dictionaryRank[targetRank];
                        MedalRankTitle rankTitle1 = (MedalRankTitle)(tempTargetRank);
                        sRank.spriteName = "PVP_rank_" + tempTargetRank;
                        lRank.text = rankTitle1 + " " + targetSymbol;
                        Debug.Log("len cap");
                        PopupManager.Instance.ShowCongratulation();
                        temp += 10;
                        if (targetRank == "BRONZE")
                        {
                            number = 3;
                        }
                        else if (targetRank == "SILVER")
                        {
                            number = 4;
                        }
                        else
                        {
                            number = 5;
                        }
                        currentStar = 0;
                        for (int i = 0; i < listMedalActive.Count; i++)
                        {
                            if (i < number)
                            {
                                listMedalInactive[i].SetActive(true);
                                if (i < currentStar)
                                {
                                    listMedalActive[i].gameObject.SetActive(true);
                                }
                                else
                                {
                                    listMedalActive[i].gameObject.SetActive(false);
                                }
                            }
                            else
                            {
                                listMedalActive[i].gameObject.SetActive(false);
                                listMedalInactive[i].SetActive(false);
                            }
                        }
                    }
                    else
                    {
                        temp += 10;
                        listMedalActive[currentStar].gameObject.SetActive(true);
                        listMedalEffectActive[currentStar].gameObject.SetActive(true);
                        listMedalEffectActive[currentStar].Play();
                        currentStar++;
                    }
                }
                sBonusMedal.fillAmount = (obj % 10) / 10f;
            }).setIgnoreTimeScale(true).setDelay(2).setEase(LeanTweenType.easeOutQuad);
        }
        else if (endScore < startScore)
        {
            LeanTween.value(gameObject, startScore, endScore, 2).setOnComplete(() =>
            {
                Debug.Log("done");
            }).setOnUpdate((float obj) =>
            {
                if (obj < temp && temp > 1000)
                {
                    if (currentStar == 0)
                    {
                        int tempTargetRank = CachePvp.dictionaryRank[targetRank];
                        MedalRankTitle rankTitle1 = (MedalRankTitle)(tempTargetRank);
                        sRank.spriteName = "PVP_rank_" + tempTargetRank;
                        lRank.text = rankTitle1 + " " + targetSymbol;
                        Debug.Log("giam cap");
                        PopupManager.Instance.ShowCongratulation();
                        temp -= 10;
                        if (targetRank == "BRONZE")
                        {
                            number = 3;
                        }
                        else if (targetRank == "SILVER")
                        {
                            number = 4;
                        }
                        else
                        {
                            number = 5;
                        }
                        currentStar = number;
                        for (int i = 0; i < listMedalActive.Count; i++)
                        {
                            if (i < number)
                            {
                                listMedalInactive[i].SetActive(true);
                                listMedalActive[i].gameObject.SetActive(true);
                            }
                            else
                            {
                                listMedalActive[i].gameObject.SetActive(false);
                                listMedalInactive[i].SetActive(false);
                            }
                        }
                    }
                    else
                    {
                        temp -= 10;
                        currentStar--;
                        listMedalActive[currentStar].gameObject.SetActive(false);
                        listMedalEffectInactive[currentStar].gameObject.SetActive(true);
                        listMedalEffectInactive[currentStar].Play();
                    }
                }
                sBonusMedal.fillAmount = (obj % 10) / 10f;
            }).setIgnoreTimeScale(true).setDelay(2).setEase(LeanTweenType.easeInQuad);
        }
        if (CachePvp.MyResult.result == PlayerResult.WIN)
        {
            CachePvp.SetGold(CachePvp.GetGold() + CachePvp.MyResult.goldBet * 2, FirebaseLogSpaceWar.PvP_why, FirebaseLogSpaceWar.Unknown_why);
            //CachePvp.Gold += CachePvp.MyResult.goldBet * 2;
            FirebaseLogSpaceWar.LogGoldIn(MinhCacheGame.GetBettingAmountPvP() * 2, FirebaseLogSpaceWar.PvP_why, "");
        }
        else if (CachePvp.MyResult.result == PlayerResult.DRAWN)
        {
            //CachePvp.Gold += CachePvp.MyResult.goldBet;
            CachePvp.SetGold(CachePvp.GetGold() + CachePvp.MyResult.goldBet, FirebaseLogSpaceWar.PvP_why, FirebaseLogSpaceWar.Unknown_why);
            FirebaseLogSpaceWar.LogGoldIn(MinhCacheGame.GetBettingAmountPvP(), FirebaseLogSpaceWar.PvP_why, "");
        }
        PvpUtil.SendUpdatePlayer("WinLosePVP");
        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.AddCoinTop();
        }
    }

    public void ShowShop()
    {
        SoundManager.PlayShowPopup();
        PopupManagerCuong.Instance.ShowShopPopup();
        if (PopupShop.Instance != null)
        {
            PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.Upgrade);
        }
        //FirebaseLogSpaceWar.LogClickButton("Upgrade");
    }

    public void ShowInfo()
    {
        if (CachePvp.Opponent != null)
        {
            PopupManager.Instance.ShowProfilePopup(CachePvp.Opponent);
        }
    }

    string facebookIdPlayer1;
    string facebookIdPlayer2;
    string facebookIdPlayer3;
    string facebookIdPlayer4;

    void OnRefreshAvatar(IMessage msg)
    {
        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdOpponent))
        {
            sAvatarOpponent.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdOpponent];
        }

        if (!string.IsNullOrEmpty(facebookIdPlayer1) && CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdPlayer1))
        {
            listAvatarPlayers[0].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdPlayer1];
        }
        if (!string.IsNullOrEmpty(facebookIdPlayer2) && CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdPlayer2))
        {
            listAvatarPlayers[1].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdPlayer2];
        }
        if (!string.IsNullOrEmpty(facebookIdPlayer3) && CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdPlayer3))
        {
            listAvatarPlayers[2].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdPlayer3];
        }
        if (!string.IsNullOrEmpty(facebookIdPlayer4) && CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdPlayer4))
        {
            listAvatarPlayers[3].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdPlayer4];
        }
    }

    public void ShowInfoP1Team1()
    {
        for (int i = 0; i < CachePvp.coopPvPRoomInfo.data.teams.Count; i++)
        {
            if (CachePvp.coopPvPRoomInfo.data.teams[i].result == CoopPvPTeamData.WIN)
            {
                PopupManager.Instance.ShowProfilePopup(CachePvp.coopPvPRoomInfo.data.teams[i].members[0].player);
            }
            else if (CachePvp.coopPvPRoomInfo.data.teams[i].result == CoopPvPTeamData.DRAWN)
            {
                PopupManager.Instance.ShowProfilePopup(CachePvp.coopPvPRoomInfo.data.teams[0].members[0].player);
            }
        }
    }

    public void ShowInfoP2Team1()
    {
        for (int i = 0; i < CachePvp.coopPvPRoomInfo.data.teams.Count; i++)
        {
            if (CachePvp.coopPvPRoomInfo.data.teams[i].result == CoopPvPTeamData.WIN)
            {
                PopupManager.Instance.ShowProfilePopup(CachePvp.coopPvPRoomInfo.data.teams[i].members[1].player);
            }
            else if (CachePvp.coopPvPRoomInfo.data.teams[i].result == CoopPvPTeamData.DRAWN)
            {
                PopupManager.Instance.ShowProfilePopup(CachePvp.coopPvPRoomInfo.data.teams[0].members[1].player);
            }
        }
    }

    public void ShowInfoP1Team2()
    {
        for (int i = 0; i < CachePvp.coopPvPRoomInfo.data.teams.Count; i++)
        {
            if (CachePvp.coopPvPRoomInfo.data.teams[i].result == CoopPvPTeamData.LOSE)
            {
                PopupManager.Instance.ShowProfilePopup(CachePvp.coopPvPRoomInfo.data.teams[i].members[0].player);
            }
            else if (CachePvp.coopPvPRoomInfo.data.teams[i].result == CoopPvPTeamData.DRAWN)
            {
                PopupManager.Instance.ShowProfilePopup(CachePvp.coopPvPRoomInfo.data.teams[1].members[0].player);
            }
        }
    }
    public void ShowInfoP2Team2()
    {
        for (int i = 0; i < CachePvp.coopPvPRoomInfo.data.teams.Count; i++)
        {
            if (CachePvp.coopPvPRoomInfo.data.teams[i].result == CoopPvPTeamData.LOSE)
            {
                PopupManager.Instance.ShowProfilePopup(CachePvp.coopPvPRoomInfo.data.teams[i].members[1].player);
            }
            else if (CachePvp.coopPvPRoomInfo.data.teams[i].result == CoopPvPTeamData.DRAWN)
            {
                PopupManager.Instance.ShowProfilePopup(CachePvp.coopPvPRoomInfo.data.teams[1].members[1].player);
            }
        }
    }


    public void OnClickBtnBack()
    {
        if (CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
        {
            new CSCoopPvPRematch(CSCoopPvPRematch.TYPE_NOT_REMATCH).Send();
        }
        LeanTween.cancel(gameObject);
        if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA || CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
        {
            SceneManager.LoadScene("Home");
        }
        else
        {
            SceneManager.LoadScene("PVPMain");
        }
    }

    private bool readyClickBack;

    IEnumerator ReadyClickBack()
    {
        readyClickBack = false;
        yield return new WaitForSecondsRealtime(0.38f);
        readyClickBack = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && readyClickBack)
        {
            StartCoroutine(ReadyClickBack());
            if (PopupManager.Instance.IsProfilePopupActive())
            {

            }
            else if (PopupManager.Instance.IsRankingPopupActive())
            {
                PopupManager.Instance.HideRankingPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveShopPopup())
            {
                PopupManagerCuong.Instance.HideShopPopup();
            }
            else if (PopupManager.Instance.IsVideoRewardPopupActive())
            {
                PopupManager.Instance.HideVideoRewardPopup();
            }
            else
            {
                OnClickBtnBack();
            }
        }
    }

    public void OnClickButtonLeaderboardPVP()
    {
        PopupManager.Instance.ShowRankingPVP();
    }

    public void OnClickButtonReplay()
    {
        new CSPvPInvite(CachePvp.Opponent.code, CacheGame.GetSpaceShipUserSelected(), PvPDataHelper.Instance.GetMyWaveCacheIndex(), PvPDataHelper.Instance.GetMyDataString()).Send();
    }

    public void ClickButtonRematch()
    {
        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        else if (CachePvp.GetGold() < CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].members[CachePvp.myIndex].goldBet)
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
            return;
        }
        buttonRematchActive.SetActive(false);
        buttonRematchDeactive.SetActive(true);
        new CSCoopPvPRematch(CSCoopPvPRematch.TYPE_REMATCH).Send();
    }

    public void ClickButtonRematchDeactive()
    {
        //if (!OSNet.NetManager.Instance.IsOnline())
        //{
        //    PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
        //    return;
        //}
        if (CachePvp.GetGold() < CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].members[CachePvp.myIndex].goldBet)
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
            return;
        }
        if (isTeammateOut)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_your_teammate_out, false);
            return;
        }
        buttonRematchActive.SetActive(true);
        buttonRematchDeactive.SetActive(false);
        //new CSCoopPvPRematch(CSCoopPvPRematch.TYPE_REMATCH).Send();
    }
}
