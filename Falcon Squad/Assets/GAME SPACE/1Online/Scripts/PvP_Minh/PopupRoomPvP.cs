﻿using UnityEngine;

public class PopupRoomPvP : MonoBehaviour
{
    public enum RoomType
    {
        Create,
        Join
    }

    private RoomType roomType = RoomType.Create;

    [SerializeField]
    private UIInput inputRoomName;

    [SerializeField]
    private UIInput inputPassword;

    [SerializeField]
    private UILabel lTitle;

    [SerializeField]
    private UILabel lButtonName;

    public void ShowPopupRoom(RoomType roomType)
    {
        this.roomType = roomType;
        PvPUIManager.Instance.popupRoomPvP.gameObject.SetActive(true);
    }

    public void OnClickBtnCreateOrJoin()
    {
        if (inputRoomName.value == "")
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast("Please enter a valid room name!!!", false, 1.56f);
        }
        else
        {
            if(roomType == RoomType.Create)
            {
                CreateRoom(inputRoomName.value);
            }
            else
            {
                JoinRoom(inputRoomName.value);
            }
        }
    }

    public void OnClickBtnClose()
    {
        PvPUIManager.Instance.popupRoomPvP.gameObject.SetActive(false);
    }

    private void CreateRoom(string roomName)
    {
    }

    private void JoinRoom(string roomName)
    {
    }

    void LoadUI()
    {
        if (roomType == RoomType.Create)
        {
            lTitle.text = "Create";
            lButtonName.text = "Create";

            inputRoomName.value = MinhCacheGame.GetPrivateRoomNamePvP();
        }
        else
        {
            lTitle.text = "Join";
            lButtonName.text = "Join";

            inputRoomName.value = "";
        }

        inputPassword.value = "";
    }

    private void OnEnable()
    {
        LoadUI();
    }
}
