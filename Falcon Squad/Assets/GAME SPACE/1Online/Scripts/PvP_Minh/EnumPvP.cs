﻿
public enum MedalRankSymbol
{
    V,
    IV,
    III,
    II,
    I
}

public class MedalRankTitle
{
    private enum MedalTitle
    {
        Bronze = 1,
        Silver,
        Gold,
        Platinum,
        Diamond,
        Master,
        Challenger,
        Legend
    }

    string rankTitle = "unknown";

    public MedalRankTitle(int rank)
    {
        switch ((MedalTitle)rank)
        {
            case MedalTitle.Bronze:
                rankTitle = I2.Loc.ScriptLocalization.rank_pvp_bronze;
                break;

            case MedalTitle.Silver:
                rankTitle = I2.Loc.ScriptLocalization.rank_pvp_silver;
                break;

            case MedalTitle.Gold:
                rankTitle = I2.Loc.ScriptLocalization.rank_pvp_gold;
                break;

            case MedalTitle.Platinum:
                rankTitle = I2.Loc.ScriptLocalization.rank_pvp_platinum;
                break;

            case MedalTitle.Diamond:
                rankTitle = I2.Loc.ScriptLocalization.rank_pvp_diamond;
                break;

            case MedalTitle.Master:
                rankTitle = I2.Loc.ScriptLocalization.rank_pvp_master;
                break;

            case MedalTitle.Challenger:
                rankTitle = I2.Loc.ScriptLocalization.rank_pvp_challenger;
                break;

            case MedalTitle.Legend:
                rankTitle = I2.Loc.ScriptLocalization.rank_pvp_legend;
                break;

            default:
                break;
        }
    }

    public static explicit operator MedalRankTitle(int rank)
    {
        MedalRankTitle m = new MedalRankTitle(rank);
        return m;
    }

    public override string ToString()
    {
        return rankTitle;
    }
}
