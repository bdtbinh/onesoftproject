﻿using Facebook.Unity;
using SkyGameKit;
using SkyGameKit.Multiplayer;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mp.Pvp;
using OneSoftGame.Tools;
using com.ootii.Messages;

public class PopupRegisterPvP : PersistentSingleton<PopupRegisterPvP>
{
    [SerializeField]
    private UIInput inputName;

    public GameObject buttonLoginFacebook;
    public GameObject buttonLoginGamecenter;

    void Awake()
    {
        base.Awake();
        MessageDispatcher.AddListener(EventName.LoginFacebook.LoggedFacebook.ToString(), OnLoggedFacebook, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoginFacebook.LoggedFacebook.ToString(), OnLoggedFacebook, true);
    }

    void OnLoggedFacebook(IMessage msg)
    {
        ShowTargetScreen();
    }

    public void OnClickBtnGo()
    {
        if (inputName.value == "")
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_character.Replace("%{number_character}", "1"));
            return;
        }
        else
        {
            CachePvp.Name = inputName.value;
            PvpUtil.SendUpdatePlayer("PopupRegisterPvP");
            MessageDispatcher.SendMessage(this, EventName.LoadProfile.LoadedProfile.ToString(), 0, 0);
            PopupManager.Instance.HideRegisterPopup();
            ShowTargetScreen();
        }
    }

    void ShowTargetScreen()
    {
        if (targetScreen.Equals("ranking"))
        {
            if (!CachePvp.sCVersionConfig.canRank)
            {
                PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainRank);
                return;
            }
            PopupManager.Instance.ShowRanking();
            targetScreen = "";
        }
        else if (targetScreen.Equals("pvp"))
        {
            SceneManager.LoadScene("PVPMain");
        }
    }

    public void OnClickBtnLoginFacebook()
    {
        SocialManager.Instance.FBLogIn();
    }

    private void OnFacebookLoginSuccessed()
    {
        Debug.Log("OnFacebookLoginSuccessed");
        PopupManager.Instance.HideRegisterPopup();

        string query = "/me?fields=name,picture.width(135).height(135).type(normal)";
        FB.API(query, HttpMethod.GET, result =>
        {
            FBUser user = JsonFx.Json.JsonReader.Deserialize<FBUser>(result.RawResult);
            CachePvp.facebookUser = user;
            new CSValidateFacebookId(user.id).Send();
        });
    }

    public void LoginGamecenter()
    {
        SocialManager.Instance.LoginGameCenter();
    }

    private void OnGCLoginSuccessed()
    {
        Debug.Log("OnGCLoginSuccessed");
        PopupManager.Instance.HideRegisterPopup();
        new CSValidateAppCenterId(Social.localUser.id).Send();
    }

    private void OnGCLoginFailed()
    {
        PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_login_gamecenter_fail);
    }

    string targetScreen = "";

    public void SetTargetScreen(string target = "")
    {
        targetScreen = target;
    }

    private void OnFacebookLoginFailed()
    {
        PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_login_facebook_fail);
    }

    public void OnClickBtnClose()
    {
        PopupManager.Instance.HideRegisterPopup();
    }

    void LoadUI()
    {
        inputName.value = "";
        inputName.isSelected = false;
    }

    private void OnEnable()
    {
        if (GameContext.IS_CHINA_VERSION)
        {
            SocialManager.Instance.OnGCLoginSuccessed += OnGCLoginSuccessed;
            SocialManager.Instance.OnGCLoginFailed += OnGCLoginFailed;
            buttonLoginFacebook.SetActive(false);
            buttonLoginGamecenter.SetActive(true);
        }
        else
        {
            SocialManager.Instance.OnFacebookLoginSuccessed += OnFacebookLoginSuccessed;
            SocialManager.Instance.OnFacebookLoginFail += OnFacebookLoginFailed;
            buttonLoginFacebook.SetActive(true);
            buttonLoginGamecenter.SetActive(false);
        }
        LoadUI();
    }

    private void OnDisable()
    {
        if (SocialManager.Instance.OnFacebookLoginSuccessed != null)
        {
            SocialManager.Instance.OnFacebookLoginSuccessed -= OnFacebookLoginSuccessed;
        }
        if (SocialManager.Instance.OnFacebookLoginFail != null)
        {
            SocialManager.Instance.OnFacebookLoginFail -= OnFacebookLoginFailed;
        }
        if (SocialManager.Instance.OnGCLoginSuccessed != null)
        {
            SocialManager.Instance.OnGCLoginSuccessed -= OnGCLoginSuccessed;
        }
        if (SocialManager.Instance.OnGCLoginFailed != null)
        {
            SocialManager.Instance.OnGCLoginFailed -= OnGCLoginFailed;
        }
    }

}
