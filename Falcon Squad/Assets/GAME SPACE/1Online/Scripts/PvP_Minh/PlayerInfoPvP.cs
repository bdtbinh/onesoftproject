﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInfoPvP : MonoBehaviour {
    public UISprite InputChangeNameSprite;
    public UIInput InputChangeName;
    private void OnEnable() {

    }
    public void B_ChangeName_Click() {
        InputChangeNameSprite.enabled = true;
    }
    public void B_ChangeAvatar_Click() {
        CacheGame.SetTotalCoin(2000000, "test", "test");
        CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, GameContext.TOTAL_LEVEL);
        CacheGame.SetMaxLevel3Difficult(GameContext.TOTAL_LEVEL);
    }
    public void B_GoToPvP_Click() {
        SceneManager.LoadScene("PVPMain");
    }
    public void B_Close_Click() {
        string name = InputChangeName.value;
        string avatar = "";//TODO
        new CSChangeProfile(avatar, name).Send();
        gameObject.SetActive(false);
    }
}
