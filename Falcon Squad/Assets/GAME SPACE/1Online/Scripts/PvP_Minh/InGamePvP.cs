﻿using Mp.Pvp;
using PathologicalGames;
using SkyGameKit;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using DG.Tweening;
using com.ootii.Messages;
using System.Collections;

public class InGamePvP : SgkSingleton<InGamePvP>
{
    public GameObject lWaitBattleEnd;
    public GameObject lWaitReady;
    //2v2
    public GameObject pvp;
    public GameObject pvp2v2;

    public UILabel time2v2;
    public List<UISprite> listShip;
    public UI2DSprite healthbarTeam1;
    public UI2DSprite healthbarTeam2;

    public List<GameObject> listSpriteQuit;
    public List<UI2DSprite> listAvatar;
    public List<UISprite> listFlag;
    public List<UILabel> listScore;
    public List<GameObject> listLifeIcon;
    public List<int> listLifeValue = new List<int>() { 3, 3, 3, 3 };
    public List<UISprite> listVipIcon;

    //pvp
    public InGamePlayerInfoBar[] playersInfo = new InGamePlayerInfoBar[2];
    public UILabel time;
    private int score;
    private int myScore;
    private int life;
    public UISprite sPlayerShip;
    public UISprite sOpponentShip;

    public List<Sprite> listHealthBar;
    public UI2DSprite healthbarPlayer1;
    public UI2DSprite healthbarPlayer2;

    public Color colorLose;

    Queue<ObjectDataUDP> queueDataUDP = new Queue<ObjectDataUDP>();

    void Awake()
    {
        lWaitBattleEnd.SetActive(false);
        lWaitReady.SetActive(true);
        base.Awake();
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.ChangeProperties.DisconnectFromServer.ToString(), OnDisconnectFromServer, true);
        PanelUITop.Instance.onLifeChange += OnMyLifeChange;
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            pvp.SetActive(false);
            pvp2v2.SetActive(true);
            MessageDispatcher.AddListener(EventName.PVP.CoopRoomInfo.ToString(), OnCoopRoomInfo, true);
            MessageDispatcher.AddListener(EventName.PVP.SCCoopPvPDied.ToString(), OnSCCoopPvPDied, true);
            MessageDispatcher.AddListener(EventName.PVP.SCCoopPvPOut.ToString(), OnSCCoopPvPOut, true);
            MessageDispatcher.AddListener(EventName.PVP.CoopPvPEnemyDie.ToString(), OnEnemyDie, true);
        }
        else
        {
            pvp.SetActive(true);
            pvp2v2.SetActive(false);
            MessageDispatcher.AddListener(EventName.PVP.StartGamePVP.ToString(), OnStartGamePVP, true);
            PanelUITop.Instance.onScoreChange += OnMyScoreChange;
        }
        myCode = CachePvp.Code;
        myTeammateCode = CachePvp.MyTeammateCode.ToString();
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.ChangeProperties.DisconnectFromServer.ToString(), OnDisconnectFromServer, true);
        PanelUITop.Instance.onLifeChange -= OnMyLifeChange;
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            MessageDispatcher.RemoveListener(EventName.PVP.CoopRoomInfo.ToString(), OnCoopRoomInfo, true);
            MessageDispatcher.RemoveListener(EventName.PVP.SCCoopPvPDied.ToString(), OnSCCoopPvPDied, true);
            MessageDispatcher.RemoveListener(EventName.PVP.SCCoopPvPOut.ToString(), OnSCCoopPvPOut, true);
            MessageDispatcher.RemoveListener(EventName.PVP.CoopPvPEnemyDie.ToString(), OnEnemyDie, true);
        }
        else
        {
            MessageDispatcher.RemoveListener(EventName.PVP.StartGamePVP.ToString(), OnStartGamePVP, true);
            PanelUITop.Instance.onScoreChange -= OnMyScoreChange;
        }
    }

    private void OnEnemyDie(IMessage rMessage)
    {
        int e = (int)rMessage.Data;
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
        {
            if (!ConnectManager.hashSetIdDieEnemey.ContainsKey(e))
            {
                ConnectManager.hashSetIdDieEnemey.Add(e, e);
            }
            if (ConnectManager.dictionaryEnemy.ContainsKey(e))
            {
                ConnectManager.dictionaryEnemy[e].enemyCollisionBase.TakeDamage(ConnectManager.dictionaryEnemy[e].currentHP, true);
            }
        }
    }

    private void OnSCCoopPvPOut(IMessage rMessage)
    {
        SCCoopPvPPlayerOut cp = rMessage.Data as SCCoopPvPPlayerOut;
        if (cp.status == SCCoopPvPPlayerOut.SUCCESS)
        {
            string code = cp.player.code;
            if (code.Equals(myTeammateCode))
            {
                AllPlayerManager.Instance.playerCoop.Aircraft.StopTimeScaleSkill();
                AllPlayerManager.Instance.playerCoop.gameObject.SetActive(false);
                listSpriteQuit[1].SetActive(true);
            }
            else if (!code.Equals(myCode))
            {
                for (int i = 0; i < CachePvp.listPlayers2v2.Count; i++)
                {
                    if (CachePvp.listPlayers2v2[i].code.Equals(code))
                    {
                        if (i % 2 == 0)
                        {
                            listSpriteQuit[2].SetActive(true);
                        }
                        else
                        {
                            listSpriteQuit[3].SetActive(true);
                        }
                    }
                }
            }
        }
    }

    private void OnSCCoopPvPDied(IMessage rMessage)
    {
        SCCoopPvPPlayerDied cp = rMessage.Data as SCCoopPvPPlayerDied;
        if (cp.status == SCCoopPvPPlayerDied.SUCCESS)
        {
            string code = cp.code;
            if (code.Equals(CachePvp.MyTeammateCode.ToString()))
            {
                AllPlayerManager.Instance.playerCoop.Aircraft.StopTimeScaleSkill();
                AllPlayerManager.Instance.playerCoop.gameObject.SetActive(false);
            }
            else if (code.Equals(CachePvp.Code))
            {
                AllPlayerManager.Instance.playerCampaign.Aircraft.StopTimeScaleSkill();
                AllPlayerManager.Instance.playerCampaign.gameObject.SetActive(false);
            }
        }
    }

    private void OnCoopRoomInfo(IMessage rMessage)
    {
        SCCoopPvPRoomInfo ri = rMessage.Data as SCCoopPvPRoomInfo;
        if (ri.status == SCCoopPvPRoomInfo.SUCCESS)
        {
            switch (ri.data.state)
            {
                case CoopPvPRoomData.WAIT_FOR_READY:
                    lWaitBattleEnd.SetActive(false);
                    lWaitReady.SetActive(true);
                    break;
                case CoopPvPRoomData.START:
                    lWaitBattleEnd.SetActive(false);
                    lWaitReady.SetActive(false);
                    StartGame2v2();
                    break;
                case CoopPvPRoomData.END:
                    if (ri.data.teams[CachePvp.myTeamIndex].result == CoopPvPTeamData.WIN)
                    {
                        OnWin();
                    }
                    else if (ri.data.teams[CachePvp.myTeamIndex].result == CoopPvPTeamData.LOSE)
                    {
                        OnLose();
                    }
                    else if (ri.data.teams[CachePvp.myTeamIndex].result == CoopPvPTeamData.DRAWN)
                    {
                        OnDrawn();
                    }
                    break;
                case CoopPvPRoomData.DESTROY:
                    DestroyRoom();
                    break;
            }
        }
    }

    int indexPlayer;

    private void Update()
    {
        while (queueDataUDP.Count > 0)
        {
            ObjectDataUDP obj = queueDataUDP.Dequeue();
            if (obj != null)
            {
                switch (obj.type)
                {
                    case CachePvp.PLAYER_BULLET:
                        MessageDispatcher.SendMessage(gameObject, EventName.UDP.AddPowerUp.ToString(), obj.id, 0);
                        break;
                    case CachePvp.ACTIVE_SKILL:
                        MessageDispatcher.SendMessage(gameObject, EventName.UDP.ActiveSkill2v2.ToString(), obj.id, 0);
                        break;
                    default:
                        break;
                }
            }
        }
        if (flagPlayerDie)
        {
            flagPlayerDie = false;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    listLifeIcon[i * 3 + j].SetActive(true);
                    if (j >= listLifeValue[i])
                    {
                        listLifeIcon[i * 3 + j].SetActive(false);
                    }
                }
            }
            if (isTeammateDie)
            {
                AllPlayerManager.Instance.playerCoop.playerControllerScript.PlayerDie(true);
            }
        }
        if (flagUpdateScore)
        {
            flagUpdateScore = false;
            if (CachePvp.myTeamIndex == 0)
            {
                listScore[0].text = scoreTeam1.ToString();
                listScore[1].text = scoreTeam2.ToString();
                scoreMyTeam = scoreTeam1;
                scoreOpponentTeam = scoreTeam2;
            }
            else if (CachePvp.myTeamIndex == 1)
            {
                listScore[0].text = scoreTeam2.ToString();
                listScore[1].text = scoreTeam1.ToString();
                scoreMyTeam = scoreTeam2;
                scoreOpponentTeam = scoreTeam1;
            }
            if (scoreMyTeam > scoreOpponentTeam)
            {
                int sum = scoreMyTeam + scoreOpponentTeam;
                int myPercent = Mathf.RoundToInt(scoreMyTeam * 260f / sum - 130);
                healthbarTeam1.width = 184 + myPercent;// Mathf.Min(184, 314)
                healthbarTeam1.sprite2D = listHealthBar[1];
                healthbarTeam1.depth = 11;
                healthbarTeam1.color = Color.white;
                healthbarTeam2.width = 188;
                healthbarTeam2.sprite2D = listHealthBar[2];
                healthbarTeam2.depth = 10;
                healthbarTeam2.color = colorLose;
            }
            else if (scoreMyTeam < scoreOpponentTeam)
            {
                int sum = scoreMyTeam + scoreOpponentTeam;
                int myPercent = Mathf.RoundToInt(scoreOpponentTeam * 260f / sum - 130);
                healthbarTeam1.width = 188;
                healthbarTeam1.sprite2D = listHealthBar[0];
                healthbarTeam1.depth = 10;
                healthbarTeam1.color = colorLose;
                healthbarTeam2.width = 184 + myPercent;// Mathf.Min(344, 544)
                healthbarTeam2.sprite2D = listHealthBar[3];
                healthbarTeam2.depth = 11;
                healthbarTeam2.color = Color.white;
            }
            else //equals
            {
                healthbarTeam1.width = 184;// Mathf.Min(409, )
                healthbarTeam1.sprite2D = listHealthBar[0];
                healthbarTeam1.depth = 10;
                healthbarTeam1.color = Color.white;
                healthbarTeam2.width = 184;
                healthbarTeam2.sprite2D = listHealthBar[2];
                healthbarTeam2.depth = 10;
                healthbarTeam2.color = Color.white;
            }
        }
    }

    public void AddPowerUp(int amount)
    {
        ObjectDataUDP obj = new ObjectDataUDP
        {
            type = CachePvp.PLAYER_BULLET,
            id = amount
        };
        queueDataUDP.Enqueue(obj);
    }

    public void ActiveSkill(int type)
    {
        ObjectDataUDP obj = new ObjectDataUDP
        {
            type = CachePvp.ACTIVE_SKILL,
            id = type
        };
        queueDataUDP.Enqueue(obj);
    }

    int scoreMyTeam;
    int scoreOpponentTeam;

    int scoreTeam1;
    int scoreTeam2;
    bool flagUpdateScore;
    public void UpdateScore(int scoreTeam1, int scoreTeam2)
    {
        this.scoreTeam1 = scoreTeam1;
        this.scoreTeam2 = scoreTeam2;
        flagUpdateScore = true;
    }

    bool flagPlayerDie;
    bool isTeammateDie;
    string myCode;
    string myTeammateCode;

    public void PlayerDie(string playerCode, int currentHP)
    {
        isTeammateDie = false;
        if (playerCode.Equals(myTeammateCode))
        {
            indexPlayer = 1;
            isTeammateDie = true;
        }
        else if (playerCode.Equals(myCode))
        {
            indexPlayer = 0;
        }
        else
        {
            for (int i = 0; i < CachePvp.listPlayers2v2.Count; i++)
            {
                if (CachePvp.listPlayers2v2[i].code.Equals(playerCode))
                {
                    if (i % 2 == 0)
                    {
                        indexPlayer = 2;
                    }
                    else
                    {
                        indexPlayer = 3;
                    }
                }
            }
        }
        listLifeValue[indexPlayer] = currentHP;
        flagPlayerDie = true;
    }

    void StartGame2v2()
    {
        for (int i = 0; i < CachePvp.coopPvPRoomInfo.data.teams.Count; i++)
        {
            for (int j = 0; j < CachePvp.coopPvPRoomInfo.data.teams[i].members.Count; j++)
            {
                Debug.Log("CachePvp.coopPvPRoomInfo.data.teams[i].members[j].spaceship : " + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].spaceship + " - name : " + CachePvp.coopPvPRoomInfo.data.teams[i].members[j].player.name);
            }
        }

        CachePvp.SetGold(Mathf.Max(CachePvp.GetGold() - CachePvp.coopPvPRoomInfo.data.teams[CachePvp.myTeamIndex].members[CachePvp.myIndex].goldBet, 0), "pvp2v2", "pvp2v2");
        MessageDispatcher.SendMessage(EventName.PVP.StartGamePVP2v2.ToString());
        Debug.Log("OnStartGamePVP2v2");
        LevelManager.Instance.NextWaveTime();
        if (countDown2Minutes != null)
        {
            StopCoroutine(countDown2Minutes);
        }
        countDown2Minutes = CountDown2Minutes();
        StartCoroutine(countDown2Minutes);
    }

    public void DestroyRoom()
    {
        StopAllCoroutines();
        PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_room_destroyed);
        LeanTween.delayedCall(0.3f, () =>
        {
            if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA || CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
            {
                SceneManager.LoadScene("Home");
            }
            else
            {
                SceneManager.LoadScene("PVPMain");
            }
        }).setIgnoreTimeScale(true);
    }

    private void OnStartGamePVP(IMessage rMessage)
    {
        Debug.Log("OnStartGamePVP");
        //this.Delay(3f, () =>
        //{
        //    if (gameObject.activeInHierarchy)
        //    {
        //        Debug.Log("NextWave OnStartGamePVP");
        LevelManager.Instance.NextWave();
        //    }
        //});
        lWaitReady.SetActive(false);
        if (countDown2Minutes != null)
        {
            StopCoroutine(countDown2Minutes);
        }
        countDown2Minutes = CountDown2Minutes();
        StartCoroutine(countDown2Minutes);
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.PVP2vs2)
        {
            if (CachePvp.Opponent != null)
            {
                playersInfo[1].RefreshAvatar(CachePvp.Opponent);
            }
            playersInfo[0].RefreshAvatar(CachePvp.MyInfo);
        }
        else
        {
            for (int i = 0; i < CachePvp.listPlayers2v2.Count; i++)
            {
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(CachePvp.listPlayers2v2[i].facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.listPlayers2v2[i].facebookId))
                    {
                        listAvatar[i].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.listPlayers2v2[i].facebookId];
                    }
                }
            }
        }
    }

    public void OpponentOut()
    {
        if (lWaitReady.activeInHierarchy)
        {
            StopAllCoroutines();
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.opponent_disconnect);
            LeanTween.delayedCall(0.3f, () =>
            {
                if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
                {
                    SceneManager.LoadScene("Home");
                }
                else
                {
                    SceneManager.LoadScene("PVPMain");
                }
            });
        }
    }


    void OnDisconnectFromServer(IMessage msg)
    {
        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.PVP2vs2)
        {
            PlayerResult me = new PlayerResult();
            me.state = 2;
            me.score = 0;
            me.goldBet = MinhCacheGame.GetBettingAmountPvP();
            me.exp = CachePvp.Exp;
            me.result = PlayerResult.LOSE;
            CachePvp.MyResult = me;
            OnLose(true);
        }
        else
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            LeanTween.delayedCall(1.5f, () =>
            {
                SceneManager.LoadScene("Home");
            }).setIgnoreTimeScale(true);
        }
    }

    IEnumerator countDown2Minutes;

    private void Start()
    {
        if (GameContext.modeGamePlay != GameContext.ModeGamePlay.PVP2vs2)
        {
            healthbarPlayer1.width = 337;
            healthbarPlayer1.sprite2D = listHealthBar[0];
            healthbarPlayer2.width = 337;
            healthbarPlayer2.sprite2D = listHealthBar[2];
            lWaitBattleEnd.SetActive(false);
            playersInfo[0].ShowPlayer(CachePvp.MyInfo);
            if (CachePvp.Opponent != null)
            {
                playersInfo[1].ShowPlayer(CachePvp.Opponent);
            }
            timeCount = CachePvp.PlayTime;
            time.text = (timeCount / 60).ToString("00") + ":" + (timeCount % 60).ToString("00");
            score = 0;
            myScore = 0;
            opponentScore = 0;
            life = AllPlayerManager.Instance.playerCampaign.Life;
            //UpdateMyGameState();

            PlayerDataUtil.PlayerProfileData profile = PlayerDataUtil.StringToObject(PlayerDataUtil.GetPlayerProfileData().ToString());
            AircraftTypeEnum aircraftType;
            int selected;
            if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
            {
                selected = CachePvp.HighestShip(profile);
                aircraftType = (AircraftTypeEnum)selected;
                sPlayerShip.spriteName = "aircraft" + (int)aircraftType + "_e" + profile.listRankPlane[selected - 1] + "_idle_0";
            }
            else if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
            {
                selected = CachePvp.HighestShipTournament(profile);
                aircraftType = (AircraftTypeEnum)selected;
                sPlayerShip.spriteName = "aircraft" + (int)aircraftType + "_e" + (int)Constant.MAX_RANK_APPLY + "_idle_0";
            }
            sPlayerShip.MakePixelPerfect();
            if (CachePvp.Opponent != null)
            {
                profile = PlayerDataUtil.StringToObject(CachePvp.Opponent.data);
                if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP || CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
                {
                    selected = CachePvp.HighestShip(profile);
                    aircraftType = (AircraftTypeEnum)selected;
                    sOpponentShip.spriteName = "aircraft" + (int)aircraftType + "_e" + profile.listRankPlane[selected - 1] + "_idle_0";
                }
                else if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
                {
                    selected = CachePvp.HighestShipTournament(profile);
                    aircraftType = (AircraftTypeEnum)selected;
                    sOpponentShip.spriteName = "aircraft" + (int)aircraftType + "_e" + (int)Constant.MAX_RANK_APPLY + "_idle_0";
                }
                sOpponentShip.MakePixelPerfect();
            }
        }
        else
        {
            healthbarTeam1.width = 184;
            healthbarTeam1.sprite2D = listHealthBar[0];
            healthbarTeam2.width = 184;
            healthbarTeam2.sprite2D = listHealthBar[2];
            lWaitBattleEnd.SetActive(false);
            timeCount = CachePvp.PlayTime;
            time2v2.text = (timeCount / 60).ToString("00") + ":" + (timeCount % 60).ToString("00");
            for (int i = 0; i < listSpriteQuit.Count; i++)
            {
                listSpriteQuit[i].SetActive(false);
            }
            for (int i = 0; i < listScore.Count; i++)
            {
                listScore[i].text = "0";
            }
            for (int i = 0; i < CachePvp.listPlayers2v2.Count; i++)
            {
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(CachePvp.listPlayers2v2[i].facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.listPlayers2v2[i].facebookId))
                    {
                        listAvatar[i].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.listPlayers2v2[i].facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + CachePvp.listPlayers2v2[i].facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.listPlayers2v2[i].facebookId);
                    }
                }
                else
                {
                    listAvatar[i].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                }
                listFlag[i].spriteName = CachePvp.listPlayers2v2[i].country.ToLower();
                int vip = CachePvp.GetVipFromVipPoint(CachePvp.listPlayers2v2[i].vip);
                if (vip == 0)
                {
                    listVipIcon[i].gameObject.SetActive(false);
                }
                else
                {
                    listVipIcon[i].gameObject.SetActive(true);
                    listVipIcon[i].spriteName = "Icon__VIP_" + (vip - 1);
                }

                PlayerDataUtil.PlayerProfileData profile = PlayerDataUtil.StringToObject(CachePvp.listPlayers2v2[i].data);
                int selected = CachePvp.HighestShip(profile);
                AircraftTypeEnum aircraftType;
                aircraftType = (AircraftTypeEnum)selected;
                listShip[i].spriteName = "aircraft" + (int)aircraftType + "_e" + profile.listRankPlane[selected - 1] + "_idle_0";
                listShip[i].MakePixelPerfect();
                tempVector3.x = 0.25f;
                tempVector3.y = 0.25f;
                tempVector3.z = 0.25f;
                listShip[i].transform.localScale = tempVector3;
            }
            for (int i = 0; i < listLifeIcon.Count; i++)
            {
                listLifeIcon[i].SetActive(true);
            }
        }
    }

    Color redColor = new Color(1, 0, 0, 1);
    Color whiteColor = new Color(1, 1, 1, 1);
    bool flagHeartbeat = false;
    Vector3 tempVector3 = new Vector3();
    int timeCount;
    int target;

    IEnumerator CountDown2Minutes()
    {
        timeCount = CachePvp.PlayTime;
        TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        target = (int)span.TotalSeconds + timeCount;
        while (timeCount >= 0)
        {
            if (timeCount <= 10)
            {
                time.color = redColor;
                time2v2.color = redColor;
                if (!flagHeartbeat)
                {
                    flagHeartbeat = true;
                    tempVector3.x = 1.12f;
                    tempVector3.y = 1.12f;
                    tempVector3.z = 1.12f;
                    time.transform.localScale = tempVector3;
                    time2v2.transform.localScale = tempVector3;
                    EffectHeartBeat();
                }
            }
            else
            {
                tempVector3.x = 1f;
                tempVector3.y = 1f;
                tempVector3.z = 1f;
                time.transform.localScale = tempVector3;
                time2v2.transform.localScale = tempVector3;
                time.color = whiteColor;
                time2v2.color = whiteColor;
            }
            time.text = (timeCount / 60).ToString("00") + ":" + (timeCount % 60).ToString("00");
            time2v2.text = (timeCount / 60).ToString("00") + ":" + (timeCount % 60).ToString("00");
            //timeCount--;
            TimeSpan temp_span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            timeCount = target - (int)temp_span.TotalSeconds;
            yield return new WaitForSecondsRealtime(1);
        }
        yield return new WaitForSecondsRealtime(5);
        if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA || CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
        {
            Debug.Log("end time 2 minutes");
            SceneManager.LoadScene("Home");
        }
        else
        {
            SceneManager.LoadScene("PVPMain");
        }
    }

    void EffectHeartBeat()
    {
        LeanTween.value(gameObject, 1f, 1.12f, 1).setOnUpdate((float obj) =>
        {
            tempVector3.x = obj;
            tempVector3.y = obj;
            tempVector3.z = obj;
            time.transform.localScale = tempVector3;
            time2v2.transform.localScale = tempVector3;
        }).setIgnoreTimeScale(true).setEase(LeanTweenType.easeOutElastic).setLoopPingPong();
    }

    private void OnMyScoreChange(int score)
    {
        this.score = score;
        UpdateMyGameState();
        //UpdateStatusHealthBar();
    }

    private void OnMyLifeChange(int life)
    {
        Debug.LogError("ChangeNumberLife :OnMyLifeChange: " + life);
        this.life = life;
        if (life <= 0)
        {
            lWaitBattleEnd.SetActive(true);
            if (CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
            {
                new CSCoopPvPPlayerDied().Send();
            }
            else
            {
                new CSPlayerDied().Send();
            }
        }
        if (CachePvp.typePVP != CSFindOpponent.TYPE_2V2)
        {
            playersInfo[0].ShowScore(score, life);
            UpdateMyGameState();
        }
    }

    int opponentScore = 0;

    private void UpdateMyGameState()
    {
        new CSScore(score, -1, life.ToString()).Send();
        //playersInfo[0].ShowScore(score, life);
    }

    public void PlayerDied(string code)
    {
        if (CachePvp.Code.Equals(code))
        {
            playersInfo[0].ShowScore(myScore, 0);
        }
        else
        {
            playersInfo[1].ShowScore(opponentScore, 0);
        }
    }

    public void ChangeOpponentScore(int score, int life, string code)
    {
        if (CachePvp.Code.Equals(code))
        {
            playersInfo[0].ShowScore(score, life);
            myScore = score;
        }
        else
        {
            playersInfo[1].ShowScore(score, life);
            opponentScore = score;
        }
        UpdateStatusHealthBar();
    }

    void UpdateStatusHealthBar()
    {
        if (myScore > opponentScore)
        {
            int sum = myScore + opponentScore;
            int myPercent = Mathf.RoundToInt(myScore * 400f / sum - 200);
            healthbarPlayer1.width = 344 + myPercent;// Mathf.Min(409, )
            healthbarPlayer1.sprite2D = listHealthBar[1];
            healthbarPlayer1.depth = 11;
            healthbarPlayer1.color = Color.white;
            healthbarPlayer2.width = 344;
            healthbarPlayer2.sprite2D = listHealthBar[2];
            healthbarPlayer2.depth = 10;
            healthbarPlayer2.color = colorLose;
        }
        else if (myScore < opponentScore)
        {
            int sum = myScore + opponentScore;
            int myPercent = Mathf.RoundToInt(opponentScore * 400f / sum - 200);
            healthbarPlayer1.width = 344;
            healthbarPlayer1.sprite2D = listHealthBar[0];
            healthbarPlayer1.depth = 10;
            healthbarPlayer1.color = colorLose;
            healthbarPlayer2.width = 344 + myPercent;// Mathf.Min(344, 544)
            healthbarPlayer2.sprite2D = listHealthBar[3];
            healthbarPlayer2.depth = 11;
            healthbarPlayer2.color = Color.white;
        }
        else //equals
        {
            healthbarPlayer1.width = 337;// Mathf.Min(409, )
            healthbarPlayer1.sprite2D = listHealthBar[0];
            healthbarPlayer1.depth = 10;
            healthbarPlayer1.color = Color.white;
            healthbarPlayer2.width = 337;
            healthbarPlayer2.sprite2D = listHealthBar[2];
            healthbarPlayer2.depth = 10;
            healthbarPlayer2.color = Color.white;
        }
    }

    public void OnWin()
    {
        StopLevel();
        EffecAndChangeScene(1);
    }

    public void OnLose(bool idDisconnectFromServer = false)
    {
        StopLevel();
        EffecAndChangeScene(2, idDisconnectFromServer);
    }

    public void SetTimeRemaining(int time)
    {
        TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        target = (int)span.TotalSeconds + time;
    }

    public void OnDrawn()
    {
        StopLevel();
        EffecAndChangeScene(0);
    }

    private void StopLevel()
    {
        //FirebaseLogSpaceWar.LogTimePlayLevel("" + CacheGame.GetCurrentLevel(), MainScene.Instance.timePlayLevel);
        StopAllCoroutines();
        lWaitBattleEnd.SetActive(false);
        LevelManager.Instance.gameObject.SetActive(false);
    }

    Vector3 posFxVic = new Vector3(0, 2f, 0);

    public void EffecAndChangeScene(int statusWin, bool isDisconnect = false)
    {
        Debug.Log("EffecAndChangeScene");
        //status win : 0 : drawn, 1: win, 2: lose
        if (statusWin == 1)
        {
            //if (EffectList.Instance.fxVictory != null)
            //{
            //    Transform effectIns = PoolManager.Pools[SkyGameKit.Const.explosiveName].Spawn(EffectList.Instance.fxVictory, posFxVic, Quaternion.identity);
            //    effectIns.GetComponent<ParticleSystem>().Play(true);
            //}
            EffectList.Instance.fxVictory.SetActive(true);
            EffectList.Instance.fxVictory.GetComponent<ParticleSystem>().Play(true);
            this.Delay(1.5f, () =>
            {
                EffectList.Instance.fxVictory.SetActive(false);
            }, true);

            LeanTween.delayedCall(3f, () =>
            {
                SceneManager.LoadScene("PVPEndGame");
            }).setIgnoreTimeScale(true);
        }
        else if (statusWin == 2)
        {
            if (isDisconnect)
            {
                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
                LeanTween.delayedCall(1.5f, () =>
                {
                    if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA || CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
                    {
                        SceneManager.LoadScene("Home");
                    }
                    else
                    {
                        SceneManager.LoadScene("PVPMain");
                    }
                }).setIgnoreTimeScale(true);
            }
            else
            {
                LeanTween.delayedCall(1.5f, () =>
                {
                    SceneManager.LoadScene("PVPEndGame");
                }).setIgnoreTimeScale(true);
            }
        }
        else if (statusWin == 0)
        {
            //drawn
            SceneManager.LoadScene("PVPEndGame");
        }
    }

    //	private void OnDisable () {
    //		new CSPlayerOut ().Send ();
    //	}

    [Serializable]
    public class InGamePlayerInfoBar
    {
        public UI2DSprite avatar;
        public UISprite flag;
        public UILabel name;
        public UILabel score;
        [HideInInspector]
        public int scoreInt;
        public GameObject[] lifeIcon;
        public UISprite sVipIcon;

        public void RefreshAvatar(Player player)
        {
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(player.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                    {
                        showImageFB = false;
                        avatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                        if (t != null)
                        {
                            showImageFB = false;
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                        }
                    }
                }
            }
            if (showImageFB)
            {
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(player.facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                    {
                        avatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                    }
                }
                else
                {
                    avatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                }
            }
        }

        public void ShowPlayer(Player player)
        {
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(player.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.appCenterId))
                    {
                        showImageFB = false;
                        avatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.appCenterId];
                    }
                    else
                    {
                        Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(player.appCenterId);
                        if (t != null)
                        {
                            showImageFB = false;
                            CacheAvatarManager.Instance.DownloadImageFormUrl("", player.appCenterId, t);
                        }
                    }
                }
            }
            if (showImageFB)
            {
                if (CacheAvatarManager.Instance != null && !string.IsNullOrEmpty(player.facebookId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(player.facebookId))
                    {
                        avatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[player.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + player.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, player.facebookId);
                    }
                }
                else
                {
                    avatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                }
            }
            flag.spriteName = player.country.ToLower();
            name.text = "" + player.name;
            int vip = CachePvp.GetVipFromVipPoint(player.vip);
            Debug.Log("player.vip : " + vip);
            if (vip == 0)
            {
                sVipIcon.gameObject.SetActive(false);
            }
            else
            {
                sVipIcon.gameObject.SetActive(true);
                sVipIcon.spriteName = "Icon__VIP_" + (vip - 1);
            }
        }

        public void ShowScore(int score, int life)
        {
            this.score.text = GameContext.FormatNumber(score);
            OSNetMethod.SetNumberByIcon(lifeIcon, life);
            scoreInt = score;
        }
    }
}

public class ObjectDataUDP
{
    public int code;
    public int type;
    public int id;
    public int score;
    public int hp;
}