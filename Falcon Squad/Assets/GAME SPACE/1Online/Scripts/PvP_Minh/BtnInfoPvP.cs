﻿using Mp.Pvp;
using SkyGameKit;
using UnityEngine;
using com.ootii.Messages;
using Facebook.Unity;
using System;

public class BtnInfoPvP : SgkSingleton<BtnInfoPvP>
{
    public UI2DSprite avatar;
    public UISprite flag;
    public UILabel playerName;
    public UILabel lId;
    public UISprite sVipIcon;

    void Awake()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadedProfile.ToString(), OnLoadedProfile, true);
        MessageDispatcher.AddListener(EventName.ChangeProperties.ChangeVip.ToString(), OnChangeVip, true);
        MessageDispatcher.AddListener(EventName.LoginFacebook.LoggedFacebook.ToString(), OnLoadedProfile, true);
        MessageDispatcher.AddListener(EventName.SyncData.SyncFromServerToClient.ToString(), OnSyncFromServerToClient, true);
    }

    private void OnSyncFromServerToClient(IMessage rMessage)
    {
        if (!string.IsNullOrEmpty(CachePvp.Token))
        {
            ShowPlayer(CachePvp.MyInfo);
        }
    }

    private void OnChangeVip(IMessage rMessage)
    {
        if (CachePvp.myVip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (CachePvp.myVip - 1);
        }
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadedProfile.ToString(), OnLoadedProfile, true);
        MessageDispatcher.RemoveListener(EventName.ChangeProperties.ChangeVip.ToString(), OnChangeVip, true);
        MessageDispatcher.RemoveListener(EventName.LoginFacebook.LoggedFacebook.ToString(), OnLoadedProfile, true);
        MessageDispatcher.RemoveListener(EventName.SyncData.SyncFromServerToClient.ToString(), OnSyncFromServerToClient, true);
    }

    void OnLoadedProfile(IMessage msg)
    {
        Debug.Log("CachePvp.Token : " + CachePvp.Token);
        if (!string.IsNullOrEmpty(CachePvp.Token))
        {
            ShowPlayer(CachePvp.MyInfo);
        }
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (GameContext.IS_CHINA_VERSION)
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.GamecenterId))
            {
                avatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.GamecenterId];
            }
        }
        else
        {
            if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
            {
                avatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
            }
        }
        playerName.text = string.IsNullOrEmpty(CachePvp.Name) ? CachePvp.Code : CachePvp.Name;
    }

    private void OnEnable()
    {
        if (!string.IsNullOrEmpty(CachePvp.Token))
        {
            ShowPlayer(CachePvp.MyInfo);
        }
    }

    public void ShowPlayer(Player player)
    {
        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(CachePvp.GamecenterId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.GamecenterId))
                {
                    avatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.GamecenterId];
                }
                else
                {
                    Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.GamecenterId);
                    if (t != null)
                    {
                        CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.GamecenterId, t);
                    }
                    else
                    {
                        avatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                    }
                }
            }
            else
            {
                avatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(CachePvp.FacebookId))
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.FacebookId))
                {
                    avatar.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.FacebookId];
                }
                else
                {
                    CacheAvatarManager.Instance.DownloadImageFormUrl("https://graph.facebook.com/" + CachePvp.FacebookId + "/picture?type=small&width=135&height=135", CachePvp.FacebookId);
                }
            }
            else
            {
                avatar.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            }
        }
        if (!string.IsNullOrEmpty(player.country))
        {
            player.country = "VN";
        }
        flag.spriteName = player.country.ToLower();
        playerName.text = string.IsNullOrEmpty(player.name) ? player.code : player.name;
        lId.text = !string.IsNullOrEmpty(player.code) ? "id: " + player.code : "";
        if (CachePvp.myVip == 0)
        {
            sVipIcon.gameObject.SetActive(false);
        }
        else
        {
            sVipIcon.gameObject.SetActive(true);
            sVipIcon.spriteName = "Icon__VIP_" + (CachePvp.myVip - 1);
        }
    }

    public void OnClickMe()
    {
        PopupManager.Instance.ShowProfilePopup();
        FirebaseLogSpaceWar.LogClickButton("Info");

    }
}
