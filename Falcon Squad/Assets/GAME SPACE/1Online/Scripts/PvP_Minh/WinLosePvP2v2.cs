﻿using com.ootii.Messages;
using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLosePvP2v2 : MonoBehaviour
{
    public UILabel lTimeNumber;
    public UISprite sVipIconOpponent;
    public UILabel scoreText;
    public UILabel goldText;
    public UISprite sTitle;
    public UILabel lTitle;
    public UILabel lElo;
    public UILabel lRank;
    public UISprite sRank;

    public GameObject gResult;
    public GameObject gUserRank;
    public GameObject gTournament;
    public UILabel lWinNeedValue;

    public GameObject buttonReplay;
    public GameObject buttonRank;

    public UILabel lGlory;

    public UILabel lBonusMedal;
    public UISprite sBonusMedal;

    public List<UISprite> listMedalActive;
    public List<GameObject> listMedalInactive;
    public List<ParticleSystem> listMedalEffectActive;
    public List<ParticleSystem> listMedalEffectInactive;

    public UI2DSprite sAvatarOpponent;
    public UILabel lNameOpponent;
    public UILabel lNameClanOpponent;
    public UILabel lEloOpponent;
    public UISprite sRankOpponent;
    public UILabel lRankOpponent;
    public UILabel lIdOpponent;
    public UILabel lScoreOpponent;

    private const string MEDAL_ENABLE_SPRITE = "PVP_rank_medal_1";
    private const string MEDAL_DISABLE_SPRITE = "PVP_rank_medal_0";

    private void Awake()
    {
        new CSSceneLoaded(CSSceneLoaded.SCENE_PVP_END).Send();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
    }

    Vector3 tempVector3;
    string facebookIdOpponent;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        SoundManager.StopMainBG();
        SoundManager.StopMain2BG();
        SoundManager.PlayHomeBG();
        readyClickBack = true;
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayPvpEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayPvpEasy.ToString()) + 1);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayPvpHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.PlayPvpHard.ToString()) + 1);
        lTimeNumber.text = (CachePvp.PlayTime / 60).ToString("00") + ":" + (CachePvp.PlayTime % 60).ToString("00");

        if (CachePvp.MyResult != null)
        {
            scoreText.text = GameContext.FormatNumber(CachePvp.MyResult.score);
            goldText.text = GameContext.FormatNumber(CachePvp.MyResult.goldBet);

            if (CachePvp.MyResult.result == PlayerResult.LOSE)
            {
                sTitle.spriteName = "ingame_title_3";
                lTitle.text = I2.Loc.ScriptLocalization.lose;
                goldText.text = GameContext.FormatNumber(CachePvp.MyResult.goldBet);
                lElo.text = CachePvp.MyResult.exp.ToString();
            }
            else if (CachePvp.MyResult.result == PlayerResult.WIN)
            {
                sTitle.spriteName = "ingame_title_1";
                lTitle.text = I2.Loc.ScriptLocalization.win;
                goldText.text = "+" + GameContext.FormatNumber(CachePvp.MyResult.goldBet);
                lElo.text = "+" + CachePvp.MyResult.exp.ToString();
                CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.WinPvpEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.WinPvpEasy.ToString()) + 1);
                CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.WinPvpHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.WinPvpHard.ToString()) + 1);

            }
            else if (CachePvp.MyResult.result == PlayerResult.DRAWN)
            {
                sTitle.spriteName = "ingame_title_1";
                lTitle.text = I2.Loc.ScriptLocalization.draw;
                goldText.text = "+0";
                lElo.text = CachePvp.MyResult.exp.ToString();
            }
            if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
            {
                ShowEffect(CachePvp.PlayerLevel.code, CachePvp.ConvertChina(CachePvp.PlayerLevel.name), CachePvp.MyResult.playerLevel.code, CachePvp.ConvertChina(CachePvp.MyResult.playerLevel.name), CachePvp.PlayerLevel.elo, CachePvp.MyResult.playerLevel.elo);
            }
            else
            {
                lWinNeedValue.text = CachePvp.MyResult.tournamentInfo.nextRewardIn.ToString();
            }
        }

        if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
        {
            gResult.SetActive(true);
            gUserRank.SetActive(true);
            gTournament.SetActive(false);
            if (MinhCacheGame.GetBettingAmountPvP() == 0)
            {
                buttonReplay.SetActive(true);
            }
            else
            {
                buttonReplay.SetActive(false);
            }
            buttonRank.SetActive(true);
        }
        else
        {
            gResult.SetActive(false);
            gUserRank.SetActive(false);
            gTournament.SetActive(true);
            buttonReplay.SetActive(false);
            buttonRank.SetActive(false);
        }

        if (CachePvp.Opponent != null)
        {
            lNameOpponent.text = CachePvp.Opponent.name;
            tempVector3.x = lNameClanOpponent.transform.localPosition.x;
            tempVector3.z = lNameClanOpponent.transform.localPosition.z;
            if (CachePvp.Opponent.playerClan != null)
            {
                lNameClanOpponent.text = CachePvp.Opponent.playerClan.name;
                tempVector3.y = 24.5f;
            }
            else
            {
                lNameClanOpponent.text = "";
                tempVector3.y = 40.7f;
            }
            lNameClanOpponent.transform.localPosition = tempVector3;
            if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
            {
                lEloOpponent.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.Opponent.playerLevel.elo);
            }
            else
            {
                lEloOpponent.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.Opponent.megaPlayerLevel.elo);
            }
            if (CachePvp.OpponentResult != null)
            {
                if (CachePvp.OpponentResult.result == PlayerResult.WIN)
                {
                    CachePvp.Opponent.win++;
                }
                else if (CachePvp.OpponentResult.result == PlayerResult.LOSE)
                {
                    CachePvp.Opponent.lose++;
                }
                lScoreOpponent.text = I2.Loc.ScriptLocalization.score + ": " + GameContext.FormatNumber(CachePvp.OpponentResult.score);
            }
            lIdOpponent.text = I2.Loc.ScriptLocalization.id_key + ": " + CachePvp.Opponent.code;
            int rank = CachePvp.dictionaryRank[CachePvp.Opponent.playerLevel.code];
            MedalRankTitle rankTitle = (MedalRankTitle)(rank);
            lRankOpponent.text = rankTitle + " " + CachePvp.ConvertChina(CachePvp.Opponent.playerLevel.name);
            sRankOpponent.spriteName = "PVP_rank_" + rank;

            sAvatarOpponent.sprite2D = CacheAvatarManager.Instance.defaultAvatar;
            bool showImageFB = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (!string.IsNullOrEmpty(CachePvp.Opponent.appCenterId))
                {
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.Opponent.appCenterId))
                    {
                        showImageFB = false;
                        facebookIdOpponent = CachePvp.Opponent.appCenterId;
                        sAvatarOpponent.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.Opponent.appCenterId];
                    }
                    else
                    {
                        //Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.Opponent.appCenterId);
                        //if (t != null)
                        //{
                        //    showImageFB = false;
                        //    facebookIdOpponent = CachePvp.Opponent.appCenterId;
                        //    CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.Opponent.appCenterId, t);
                        //}
                    }
                }
            }
            if (showImageFB)
            {
                if (!string.IsNullOrEmpty(CachePvp.Opponent.facebookId))
                {
                    facebookIdOpponent = CachePvp.Opponent.facebookId;
                    if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(CachePvp.Opponent.facebookId))
                    {
                        sAvatarOpponent.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[CachePvp.Opponent.facebookId];
                    }
                    else
                    {
                        string query = "https://graph.facebook.com/" + CachePvp.Opponent.facebookId + "/picture?type=small&width=135&height=135";
                        CacheAvatarManager.Instance.DownloadImageFormUrl(query, CachePvp.Opponent.facebookId);
                    }
                }
            }
            int vip = CachePvp.GetVipFromVipPoint(CachePvp.Opponent.vip);
            Debug.Log("Opponent.vip : " + vip);
            if (vip == 0)
            {
                sVipIconOpponent.gameObject.SetActive(false);
            }
            else
            {
                sVipIconOpponent.gameObject.SetActive(true);
                sVipIconOpponent.spriteName = "Icon__VIP_" + (vip - 1);
            }
        }
    }

    void ShowEffect(string currentRank, string currentSymbol, string targetRank, string targetSymbol, int startScore, int endScore)
    {
        int tempStartRank = CachePvp.dictionaryRank[currentRank];
        MedalRankTitle rankTitle = (MedalRankTitle)(tempStartRank);
        sRank.spriteName = "PVP_rank_" + tempStartRank;
        lRank.text = rankTitle + " " + currentSymbol;
        int number = 3;
        int currentStar = CachePvp.PlayerLevel.medal;
        int temp = startScore - (startScore % 10);
        if (currentRank == "BRONZE")
        {
            number = 3;
        }
        else if (currentRank == "SILVER")
        {
            number = 4;
        }
        else
        {
            number = 5;
        }

        for (int i = 0; i < listMedalActive.Count; i++)
        {
            listMedalEffectActive[i].gameObject.SetActive(false);
            listMedalEffectInactive[i].gameObject.SetActive(false);
            if (i < number)
            {
                listMedalInactive[i].SetActive(true);
                if (i < currentStar)
                {
                    listMedalActive[i].gameObject.SetActive(true);
                }
                else
                {
                    listMedalActive[i].gameObject.SetActive(false);
                }
            }
            else
            {
                listMedalActive[i].gameObject.SetActive(false);
                listMedalInactive[i].SetActive(false);
            }
        }

        sBonusMedal.fillAmount = (startScore % 10) / 10f;
        lBonusMedal.text = I2.Loc.ScriptLocalization.elo_key + ": " + GameContext.FormatNumber(CachePvp.MyResult.playerLevel.elo);
        if (endScore > startScore)
        {
            LeanTween.value(gameObject, startScore, endScore, 2).setOnComplete(() =>
            {
                Debug.Log("done");
            }).setOnUpdate((float obj) =>
            {
                if (obj >= temp + 10)
                {
                    if (currentStar >= number)
                    {
                        //int tempTargetRank = dictRank[targetRank];
                        int tempTargetRank = CachePvp.dictionaryRank[targetRank];
                        MedalRankTitle rankTitle1 = (MedalRankTitle)(tempTargetRank);
                        sRank.spriteName = "PVP_rank_" + tempTargetRank;
                        lRank.text = rankTitle1 + " " + targetSymbol;
                        Debug.Log("len cap");
                        PopupManager.Instance.ShowCongratulation();
                        temp += 10;
                        if (targetRank == "BRONZE")
                        {
                            number = 3;
                        }
                        else if (targetRank == "SILVER")
                        {
                            number = 4;
                        }
                        else
                        {
                            number = 5;
                        }
                        currentStar = 0;
                        for (int i = 0; i < listMedalActive.Count; i++)
                        {
                            if (i < number)
                            {
                                listMedalInactive[i].SetActive(true);
                                if (i < currentStar)
                                {
                                    listMedalActive[i].gameObject.SetActive(true);
                                }
                                else
                                {
                                    listMedalActive[i].gameObject.SetActive(false);
                                }
                            }
                            else
                            {
                                listMedalActive[i].gameObject.SetActive(false);
                                listMedalInactive[i].SetActive(false);
                            }
                        }
                    }
                    else
                    {
                        temp += 10;
                        listMedalActive[currentStar].gameObject.SetActive(true);
                        listMedalEffectActive[currentStar].gameObject.SetActive(true);
                        listMedalEffectActive[currentStar].Play();
                        currentStar++;
                    }
                }
                sBonusMedal.fillAmount = (obj % 10) / 10f;
            }).setIgnoreTimeScale(true).setDelay(2).setEase(LeanTweenType.easeOutQuad);
        }
        else if (endScore < startScore)
        {
            LeanTween.value(gameObject, startScore, endScore, 2).setOnComplete(() =>
            {
                Debug.Log("done");
            }).setOnUpdate((float obj) =>
            {
                if (obj < temp && temp > 1000)
                {
                    if (currentStar == 0)
                    {
                        int tempTargetRank = CachePvp.dictionaryRank[targetRank];
                        MedalRankTitle rankTitle1 = (MedalRankTitle)(tempTargetRank);
                        sRank.spriteName = "PVP_rank_" + tempTargetRank;
                        lRank.text = rankTitle1 + " " + targetSymbol;
                        Debug.Log("giam cap");
                        PopupManager.Instance.ShowCongratulation();
                        temp -= 10;
                        if (targetRank == "BRONZE")
                        {
                            number = 3;
                        }
                        else if (targetRank == "SILVER")
                        {
                            number = 4;
                        }
                        else
                        {
                            number = 5;
                        }
                        currentStar = number;
                        for (int i = 0; i < listMedalActive.Count; i++)
                        {
                            if (i < number)
                            {
                                listMedalInactive[i].SetActive(true);
                                listMedalActive[i].gameObject.SetActive(true);
                            }
                            else
                            {
                                listMedalActive[i].gameObject.SetActive(false);
                                listMedalInactive[i].SetActive(false);
                            }
                        }
                    }
                    else
                    {
                        temp -= 10;
                        currentStar--;
                        listMedalActive[currentStar].gameObject.SetActive(false);
                        listMedalEffectInactive[currentStar].gameObject.SetActive(true);
                        listMedalEffectInactive[currentStar].Play();
                    }
                }
                sBonusMedal.fillAmount = (obj % 10) / 10f;
            }).setIgnoreTimeScale(true).setDelay(2).setEase(LeanTweenType.easeInQuad);
        }
        if (CachePvp.MyResult.result == PlayerResult.WIN)
        {
            CachePvp.SetGold(CachePvp.GetGold() + CachePvp.MyResult.goldBet * 2, FirebaseLogSpaceWar.PvP_why, FirebaseLogSpaceWar.Unknown_why);
            //CachePvp.Gold += CachePvp.MyResult.goldBet * 2;
            FirebaseLogSpaceWar.LogGoldIn(MinhCacheGame.GetBettingAmountPvP() * 2, FirebaseLogSpaceWar.PvP_why, "");
        }
        else if (CachePvp.MyResult.result == PlayerResult.DRAWN)
        {
            //CachePvp.Gold += CachePvp.MyResult.goldBet;
            CachePvp.SetGold(CachePvp.GetGold() + CachePvp.MyResult.goldBet, FirebaseLogSpaceWar.PvP_why, FirebaseLogSpaceWar.Unknown_why);
            FirebaseLogSpaceWar.LogGoldIn(MinhCacheGame.GetBettingAmountPvP(), FirebaseLogSpaceWar.PvP_why, "");
        }
        else
        {
            PvpUtil.SendUpdatePlayer("WinLosePVP");
            //PlayerPrefs.Save();
        }
        if (PanelCoinGem.Instance != null)
        {
            PanelCoinGem.Instance.AddCoinTop();
        }
    }

    public void ShowShop()
    {
        SoundManager.PlayShowPopup();
        PopupManagerCuong.Instance.ShowShopPopup();
        if (PopupShop.Instance != null)
        {
            PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.Upgrade);
        }
        //FirebaseLogSpaceWar.LogClickButton("Upgrade");
    }

    public void ShowInfo()
    {
        if (CachePvp.Opponent != null)
        {
            PopupManager.Instance.ShowProfilePopup(CachePvp.Opponent);
        }
    }

    void OnRefreshAvatar(IMessage msg)
    {
        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(facebookIdOpponent))
        {
            sAvatarOpponent.sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[facebookIdOpponent];
        }
    }

    public void OnClickBtnBack()
    {
        LeanTween.cancel(gameObject);
        if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
        {
            SceneManager.LoadScene("Home");
        }
        else
        {
            SceneManager.LoadScene("PVPMain");
        }
    }

    private bool readyClickBack;

    IEnumerator ReadyClickBack()
    {
        readyClickBack = false;
        yield return new WaitForSecondsRealtime(0.38f);
        readyClickBack = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && readyClickBack)
        {
            StartCoroutine(ReadyClickBack());
            if (PopupManager.Instance.IsProfilePopupActive())
            {

            }
            else if (PopupManager.Instance.IsRankingPopupActive())
            {
                PopupManager.Instance.HideRankingPopup();
            }
            else if (PopupManagerCuong.Instance.IsActiveShopPopup())
            {
                PopupManagerCuong.Instance.HideShopPopup();
            }
            else if (PopupManager.Instance.IsVideoRewardPopupActive())
            {
                PopupManager.Instance.HideVideoRewardPopup();
            }
            else
            {
                OnClickBtnBack();
            }
        }
    }

    public void OnClickButtonLeaderboardPVP()
    {
        PopupManager.Instance.ShowRankingPVP();
    }

    public void OnClickButtonReplay()
    {
        new CSPvPInvite(CachePvp.Opponent.code, CacheGame.GetSpaceShipUserSelected(), PvPDataHelper.Instance.GetMyWaveCacheIndex(), PvPDataHelper.Instance.GetMyDataString()).Send();
    }
}
