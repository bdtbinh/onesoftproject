﻿using OSNet;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCClanKickMember : SCMessage
    {
        public const int CLAN_NOT_EXIST = 10;
        public const int UNAUTHORITY = 12;//Ko có thẩm quyền trong clan
        public const int NOT_CLAN_MEMBER = 13;//ko phải là clan member
        public string member;
        public override string GetEvent()
        {
            return Events.CLAN_KICK_MEMBER;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.KickClan.ToString(), member, 0);
        }
    }
}