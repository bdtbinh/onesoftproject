using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCFacebookConnect : SCMessage
    {
        public const int HAS_CONNECTED_BEFORE = 2;
        public Player player;
        public string token;
        public override string GetEvent()
        {
            return Events.FACEBOOK_CONNECT;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCFacebookConnect");
            CachePvp.Token = token;
            if (player.code.Length >= 3)
            {
                CachePvp.Code = player.code;
            }
            CachePvp.Name = player.name;
            CachePvp.FacebookId = player.facebookId;
            CachePvp.GamecenterId = player.appCenterId;
            CachePvp.PlayerLevel = player.playerLevel;
            CachePvp.MegaPlayerLevel = player.megaPlayerLevel;
            if (player.pvp2vs2PlayerLevel != null)
            {
                CachePvp.PlayerLevel2vs2 = player.pvp2vs2PlayerLevel;
            }
            CachePvp.FriendshipStats = player.friendshipStats;
            if (player.friendshipStats != null)
            {
                CachePvp.CountFriendsMinh = player.friendshipStats.numFriendFb;
            }
            CachePvp.PlayerClan = player.playerClan;
            if (player.pvP2vs2SeasonData != null)
            {
                CachePvp.PvP2vs2SeasonData = player.pvP2vs2SeasonData;
            }
            if (GameContext.IS_CHINA_VERSION)
            {
                Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(CachePvp.GamecenterId);
                if (t != null)
                {
                    CacheAvatarManager.Instance.DownloadImageFormUrl("", CachePvp.GamecenterId, t);
                }
            }
            else
            {
                CacheAvatarManager.Instance.DownloadImageFormUrl(CachePvp.facebookUser.picture.data.url, CachePvp.facebookUser.id);
            }
            MessageDispatcher.SendMessage(EventName.LoginFacebook.LoggedFacebook.ToString());
            SocialManager.Instance.SetFriendsFacebookToServer();
        }
    }
}