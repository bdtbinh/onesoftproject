﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OSNet;

namespace Mp.Pvp
{
    public class SCLogFirebase : SCMessage
    {

        public string keyLog;

        public override string GetEvent()
        {
            return Events.LOG_FIREBASE;
        }

        public override void OnData()
        {
            FirebaseLogSpaceWar.LogOtherEvent(keyLog);
        }
    }
}
