﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCCoopPvPSeasonRewardedClaim : SCMessage
    {
        public override string GetEvent()
        {
            return Events.COOP_PVP_SEASON_REWARD_CLAIM;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.PVP.CoopPvPSeasonRewardedClaim.ToString(), this, 0);
        }
    }
}