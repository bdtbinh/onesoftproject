﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCPlayerInfo : SCMessage
    {
        public Player player;
        public override string GetEvent()
        {
            return Events.PLAYER_INFO;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.PlayerInfo.ToString(), player, 0);
        }
    }
}