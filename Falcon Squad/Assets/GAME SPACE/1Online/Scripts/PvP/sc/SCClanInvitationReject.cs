﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCClanInvitationReject : SCMessage
    {

        public long clanId;
        public override string GetEvent()
        {
            return Events.CLAN_INVITATION_REJECT;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.RejectClanInvitation.ToString(), clanId, 0);
        }
    }
}