﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCDonateNotify : SCMessage
    {
        public const int ACTION_INSERT = 1;
        public const int ACTION_UPDATE = 2;
        public const int ACTION_DELETE = 3;

        public string id;
        public int action;
        public Chat data;

        public override string GetEvent()
        {
            return Events.CLAN_DONATE_NOTIFY;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.Clan.DonateNotify.ToString(), this, 0);
        }
    }
}