﻿using OSNet;
using System.Collections.Generic;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCClanMemberRequest : SCMessage
    {
        public const int CLAN_NOT_FOUND = 2;
        public const int UNAUTHORITY = 3;

        public List<Player> players;
        public override string GetEvent()
        {
            return Events.CLAN_MEMBER_REQUEST;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.LoadJoinRequest.ToString(), players, 0);
        }
    }
}