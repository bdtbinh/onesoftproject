﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCCoopPvPRematch : SCMessage
    {
        // người thực hiện yêu cầu
        public string code;

        public const int TYPE_NOT_REMATCH = 0;
        public const int TYPE_REMATCH = 1;

        // kiểu hành vi
        public int type;
        public override string GetEvent()
        {
            return Events.COOP_PVP_REMATCH;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.PVP.SCCoopPvPRematch.ToString(), this, 0);
        }
    }
}