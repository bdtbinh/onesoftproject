using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCEndGame : SCMessage
    {
        public string roomId;
        public List<PlayerResult> infos;

        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;
        public int type;

        public override string GetEvent()
        {
            return Events.END_GAME;
        }
        public override void OnData()
        {
            CachePvp.typePVP = type;
            PlayerResult me = new PlayerResult();
            PlayerResult opp = new PlayerResult();
            foreach (var playerResult in infos)
            {
                if (playerResult.code == CachePvp.Code)
                {
                    me = playerResult;
                    CachePvp.MyResult = playerResult;
                }
                else
                {
                    opp = playerResult;
                    CachePvp.OpponentResult = playerResult;
                }
            }
            UnityEngine.Debug.Log("SCEndGame: " + me.result);
            if (InGamePvP.Instance != null)
            {
                if (me.result == PlayerResult.WIN)
                {
                    InGamePvP.Instance.OnWin();
                }
                else if ((me.result == PlayerResult.LOSE))
                {
                    InGamePvP.Instance.OnLose();
                }
                else if ((me.result == PlayerResult.DRAWN))
                {
                    InGamePvP.Instance.OnDrawn();
                }
            }
            else if (PopupFindPvP.Instance != null)
            {
                PopupFindPvP.Instance.OnEndGame();
            }
        }
    }
}