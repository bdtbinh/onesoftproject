﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCClanInfo : SCMessage
    {
        public ClanInfo info;
        public override string GetEvent()
        {
            return Events.CLAN_INFO;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                MessageDispatcher.SendMessage(this, EventName.Clan.ClanInfo.ToString(), info, 0);
            }
        }
    }
}