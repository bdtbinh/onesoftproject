using System;
using System.Collections.Generic;
using com.ootii.Messages;
using OSNet;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCSessionInfo : SCMessage
    {
        public int state;
        public Player player;
        public RoomInfo roomInfo;
        public string roomId;
        public const int LOGGED_IN = 0;
        public const int NOT_LOGGED_IN = 1;
        public override string GetEvent()
        {
            return Events.SESSION_INFORMATION;
        }
        public override void OnData()
        {
            Debug.Log("SCSessionInfo");
            CachePvp.timeFromServer = localTimeString;
            if (state == NOT_LOGGED_IN)
            {
                if (!string.IsNullOrEmpty(CachePvp.Token) && !string.IsNullOrEmpty(CachePvp.Code))
                {
                    new CSValidateToken(CachePvp.Token, CachePvp.Code, CachePvp.deviceId, CachePvp.FacebookId).Send();
                    MessageDispatcher.SendMessage(EventName.SyncData.NoSync.ToString());
                }
                else
                {
                    if (string.IsNullOrEmpty(CachePvp.deviceId))
                    {
#if UNITY_EDITOR
                        if (SystemInfo.deviceModel.Equals("MS-7971 (MSI)"))
                        {
                            CachePvp.deviceId = "editornamnx";
                            new CSValidateDeviceId(CachePvp.deviceId).Send();
                        }
                        else if (SystemInfo.deviceModel.Equals("MS-7A70 (MSI)"))
                        {
                            CachePvp.deviceId = "editorminhddv2";
                            new CSValidateDeviceId(CachePvp.deviceId).Send();
                        }
                        else
                        {
                            new CSGetToken(CachePvp.deviceId, CachePvp.FacebookId).Send();
                            MessageDispatcher.SendMessage(EventName.SyncData.NoSync.ToString());
                        }
#else
                        bool check = Application.RequestAdvertisingIdentifierAsync((string advertisingId, bool trackingEnabled, string error) =>
                        {
                            CachePvp.deviceId = advertisingId;
                            new CSValidateDeviceId(advertisingId).Send();
                        });
                        Debug.Log("is support RequestAdvertisingIdentifierAsync : " + check);
                        if (!check)
                        {
                            new CSGetToken(CachePvp.deviceId, CachePvp.FacebookId).Send();
                            MessageDispatcher.SendMessage(EventName.SyncData.NoSync.ToString());
                        }
#endif
                    }
                    else
                    {
                        new CSValidateDeviceId(CachePvp.deviceId).Send();
                    }
                }
            }
            else
            {
                CachePvp.isConnectServer = true;
                ConnectManager.Instance.HandShake();
                CachePvp.MyCode = int.Parse(CachePvp.Code);
            }
        }
    }
}