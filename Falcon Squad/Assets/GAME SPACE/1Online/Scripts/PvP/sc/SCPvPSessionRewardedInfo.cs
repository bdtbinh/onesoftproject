﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCPvPSessionRewardedInfo : SCMessage
    {
        public int gold;
        public int gem;
        public int card;
        public override string GetEvent()
        {
            return Events.PVP_SESSION_REWARDED_INFO;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.PVP.SessionRewardedInfo.ToString(), this, 0);
        }
    }
}