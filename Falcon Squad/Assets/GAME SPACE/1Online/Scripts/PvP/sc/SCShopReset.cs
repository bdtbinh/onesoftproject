﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCShopReset : SCMessage
    {
        public const int NOT_ENOUGH_MEDAL = 2;
        public const int NOT_ENOUGH_RESET_QUOTA = 3;

        public Shop shop;
        public override string GetEvent()
        {
            return Events.SHOP_RESET;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                CachePvp.listShopItem = shop.shopItems;
            }
            MessageDispatcher.SendMessage(this, EventName.Shop.ShopReset.ToString(), this, 0);
        }
    }
}