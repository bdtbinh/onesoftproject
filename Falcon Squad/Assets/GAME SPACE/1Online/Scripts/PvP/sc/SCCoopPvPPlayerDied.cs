﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCCoopPvPPlayerDied : SCMessage
    {
        public string code;
        public override string GetEvent()
        {
            return Events.COOP_PVP_DIED;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.PVP.SCCoopPvPDied.ToString(), this, 0);
        }
    }
}