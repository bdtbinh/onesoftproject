﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCSelectLvEvent : SCMessage
    {
        public EventInfo info;
        public override string GetEvent()
        {
            return Events.SELECT_LV_EVENT;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.SelectLevelEvent.SelectLvEvent.ToString(), this, 0);
        }
    }
}

public class EventInfo
{
    public long id;
    // account code
    public string code;
    // số lượng tiket còn lại
    public int ticket;
    //Số phần thưởng đã nhận (ko trả về trong SelectLv event)
    public int countRewarded;
    //Tổng thành tích nhận thưởng đạt được (ko trả về trong SelectLv event)
    public int totalReward;

    public long startTimeRemain;
    public long endTimeRemain;
}

public class SelectLvEventInfo : EventInfo
{
    public int maxTicket;
    // Chu kì hồi phục 1 ticket (phút)
    public long gainDuration;
    // Thời điểm hồi phục ticket tiếp theo
    // giá trị này = 0 nếu số lượng ticket đạt max
    public long refillRemain;
}