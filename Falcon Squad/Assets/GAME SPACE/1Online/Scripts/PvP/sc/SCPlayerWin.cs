using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCPlayerWin : SCMessage
    {
        public override string GetEvent()
        {
            return Events.PLAYER_WIN;
        }
		public override void OnData()
		{
			UnityEngine.Debug.Log("SCPlayerWin");
		}
    }
}