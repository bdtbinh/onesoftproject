﻿using OSNet;
using System.Collections.Generic;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCLiveScore : SCMessage
    {
        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;
        public const int TYPE_PVP_2VS2 = 2;

        public int type;
        public RoomInfo room;
        public CoopPvPRoomData roomData;
        public override string GetEvent()
        {
            return Events.LIVE_SCORE;
        }
        public override void OnData()
        {
            if (PopupManager.Instance.IsLiveScorePopupActive())
            {
                MessageDispatcher.SendMessage(this, EventName.LiveScore.LoadedRoomInfo.ToString(), this, 0);
            }
        }
    }
}