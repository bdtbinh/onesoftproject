﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCCoopPvPStopMatching : SCMessage
    {
        public override string GetEvent()
        {
            return Events.COOP_PVP_STOP_MATCHING;
        }
        public override void OnData()
        {
            CachePvp.coopPvPRoomInfo = null;
        }
    }
}