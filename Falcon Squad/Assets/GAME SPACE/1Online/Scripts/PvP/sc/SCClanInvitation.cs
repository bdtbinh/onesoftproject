﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCClanInvitation : SCMessage
    {
        public const int NOT_IN_CLAN = 2;
        public const int IS_MEMBER = 4;//đã là member rồi
        public const int MEMBER_HAS_CLAN = 5;//Đã join vào một clan
        public const int MEMBER_NOT_EXIST = 7;

        public const int CLAN_NOT_EXIST = 10;
        public const int FULL_MEMBER = 11;
        public const int UNAUTHORITY = 12;
        public string member;
        public override string GetEvent()
        {
            return Events.CLAN_INVITATION;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.ClanInvitation.ToString(), message, 0);
        }
    }
}