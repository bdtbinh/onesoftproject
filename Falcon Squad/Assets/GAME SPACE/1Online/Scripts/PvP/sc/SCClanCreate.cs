﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
using Mp.Pvp;

namespace Mp.Pvp
{
    public class SCClanCreate : SCMessage
    {
        //public const int NAME_IS_EMPTY = 20;
        //public const int ERROR_JOIN_REQUESTING = 21;
        //public const int ERROR_JOINED_CLAN = 22;
        //public const int NOT_ENOUGHT_LEVEL = 23;
        //public const int NOT_ENOUGHT_GEM = 24;
        //public const int NAME_EXISTED = 25;

        public const int JOINED_CLAN = 2;
        public const int NOT_ENOUGHT_GEM = 3;
        public const int NOT_ENOUGHT_LEVEL = 4;
        public const int NAME_IS_EMPTY = 5;
        public const int LENGTH_NAME_INVALID = 6;
        public ClanInfo info;
        public override string GetEvent()
        {
            return Events.CLAN_CREATE;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.CreatedClan.ToString(), info, 0);
        }
    }
}
public class ClanInfo
{
    public long id;
    public string name;
    public long score;
    public long scoreTournament;
    public int avatar;
    public int size;
    public int totalMember;
    public int totalJoinRequest;
    public int rank;
    public int type;//0: open, 1: close
    public int requiredLevel;
    public Player master;
    public List<Player> viceMaster;
    public List<Player> members;
    public long createDate;
    public long lastUpdate;
}