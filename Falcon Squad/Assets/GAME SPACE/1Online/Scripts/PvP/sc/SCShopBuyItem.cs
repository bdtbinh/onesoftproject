﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCShopBuyItem : SCMessage
    {
        public const int NOT_ENOUGH_MEDAL = 2;
        public const int ITEM_NOT_IN_SHOP = 3;
        public const int ITEM_SOLD_OUT = 4;

        public int itemId;
        public Shop shop;

        public override string GetEvent()
        {
            return Events.SHOP_BUY_ITEM;
        }

        public override void OnData()
        {
            if (status == SUCCESS)
            {
                CachePvp.listShopItem = shop.shopItems;
                for (int i = 0; i < CachePvp.listShopItem.Count; i++)
                {
                    if (CachePvp.listShopItem[i].id == itemId)
                    {
                        if (itemId <= 5)
                        {
                            //energy
                            //PanelCoinGem.Instance.AddEnergyTop(CachePvp.listShopItem[i].quantity, "clan_shop_buy_item");
                            List<ItemVolumeFalconMail> list = new List<ItemVolumeFalconMail>();
                            ItemVolumeFalconMail item = new ItemVolumeFalconMail();
                            item.key = FalconMail.ITEM_ENERGY;
                            item.value = CachePvp.listShopItem[i].quantity;
                            list.Add(item);
                            PopupManager.Instance.ShowClaimPopup(list, EventName.CLAN_SHOP_BUY_ITEM);
                        }
                        else if (itemId <= 10)
                        {
                            //wing 1 card
                            //MinhCacheGame.SetWingCards(WingTypeEnum.WingOfJustice, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfJustice) + CachePvp.listShopItem[i].quantity, "clan_shop_buy_item", "", WingTypeEnum.WingOfJustice.ToString());
                            List<ItemVolumeFalconMail> list = new List<ItemVolumeFalconMail>();
                            ItemVolumeFalconMail item = new ItemVolumeFalconMail();
                            item.key = FalconMail.ITEM_CARD_WING_1;
                            item.value = CachePvp.listShopItem[i].quantity;
                            list.Add(item);
                            PopupManager.Instance.ShowClaimPopup(list, EventName.CLAN_SHOP_BUY_ITEM);
                        }
                        else if (itemId <= 15)
                        {
                            //wing 2 card
                            //MinhCacheGame.SetWingCards(WingTypeEnum.WingOfRedemption, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfRedemption) + CachePvp.listShopItem[i].quantity, "clan_shop_buy_item", "", WingTypeEnum.WingOfRedemption.ToString());
                            List<ItemVolumeFalconMail> list = new List<ItemVolumeFalconMail>();
                            ItemVolumeFalconMail item = new ItemVolumeFalconMail();
                            item.key = FalconMail.ITEM_CARD_WING_2;
                            item.value = CachePvp.listShopItem[i].quantity;
                            list.Add(item);
                            PopupManager.Instance.ShowClaimPopup(list, EventName.CLAN_SHOP_BUY_ITEM);
                        }
                        break;
                    }
                }
            }
            MessageDispatcher.SendMessage(this, EventName.Shop.ShopBuyItem.ToString(), this, 0);
        }
    }
}