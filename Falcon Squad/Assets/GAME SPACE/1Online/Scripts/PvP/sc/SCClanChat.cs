﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCClanChat : SCMessage
    {
        public Chat chat;

        public override string GetEvent()
        {
            return Events.CLAN_CHAT;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.Clan.InputChatSubmitClan.ToString(), this, 0);
        }
    }
}