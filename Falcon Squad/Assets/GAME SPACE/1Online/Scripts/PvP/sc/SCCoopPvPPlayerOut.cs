﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCCoopPvPPlayerOut : SCMessage
    {
        public const int NOT_LOGGED_IN = 2;
        public const int NOT_IN_ROOM = 3;
        public const int OUT_WAIT_FOR_READY_SUCCESS = 4; // thoát khi trạng thái của phòng đang ở WAIT_FOR_ READY
        public const int OUT_START_SUCCESS = 5; // thoát khi trạng thái của phòng đang ở START
        public const int INVALID_GAME_STATE = 6;

        public Player player;
        public override string GetEvent()
        {
            return Events.COOP_PVP_OUT;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.PVP.SCCoopPvPOut.ToString(), this, 0);
        }
    }
}