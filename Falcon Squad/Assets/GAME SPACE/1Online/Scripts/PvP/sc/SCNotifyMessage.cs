using System;
using System.Collections.Generic;
using OSNet;
using UnityEngine.SceneManagement;

namespace Mp.Pvp
{
    public class SCNotifyMessage : SCMessage
    {
        public long id;
        public string url;
        public string urlImage;
        public int type;
        public List<PromotionItem> promotionItems;
        //public Dictionary<int, int> promotionItems;
        public override string GetEvent()
        {
            return Events.NOTIFY_MESSAGE;
        }
        public override void OnData()
        {
            //if not in game then show
            if (SceneManager.GetActiveScene().name == "Home" || SceneManager.GetActiveScene().name == "SelectLevel" || SceneManager.GetActiveScene().name == "PVPMain")
            {
                PopupManager.Instance.ShowNotifyPopup(id, url, message, urlImage, type, promotionItems);
            }
        }
    }
}

public class PromotionItem
{
    public int itemIndex;
    public int volume;
}