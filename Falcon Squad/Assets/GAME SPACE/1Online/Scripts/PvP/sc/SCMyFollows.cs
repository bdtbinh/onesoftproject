﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCMyFollow : SCMessage
    {
        public int total;
        public List<Player> players;
        public override string GetEvent()
        {
            return Events.MY_FOLLOW;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.LoadProfile.LoadFollow.ToString(), this, 0);
        }
    }
}