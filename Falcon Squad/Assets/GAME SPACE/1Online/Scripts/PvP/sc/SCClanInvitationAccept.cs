﻿using OSNet;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCClanInvitationAccept : SCMessage
    {
        public const int INVITATION_NOT_EXIST = 2;
        public const int CLAN_NOT_FOUND = 3;
        public const int FULL_MEMBER = 4;
        public const int JOINED_CLAN = 5;
        public override string GetEvent()
        {
            return Events.CLAN_INVITATION_ACCEPT;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.Clan.AcceptClanInvitation.ToString(), status, 0);
        }
    }
}