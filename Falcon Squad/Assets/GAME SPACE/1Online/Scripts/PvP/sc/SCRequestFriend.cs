﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCRequestFriend : SCMessage
    {
        public const int ERROR_CODE_WAS_FRIEND = 21;
        public const int ERROR_CODE_DUPLICATE_REQUEST = 22;
        public const int BE_FRIEND = 23;
        public int type;
        public string code;
        public override string GetEvent()
        {
            return Events.REQUEST_FRIEND;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.LoadProfile.RequestFriend.ToString(), this, 0);
        }
    }
}