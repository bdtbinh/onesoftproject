﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCMegaTournamentClaim : SCMessage
    {
        public int key;
        public bool bonus;
        public int NOT_LOGGED_IN = 2;
        public int TOURNAMENT_REWARD_NOT_EXIST = 32;
        public int TOURNAMENT_REWARD_RECEIVED = 33;

        public override string GetEvent()
        {
            return Events.MEGA_TOURNAMENT_CLAIM;
        }

        public override void OnData()
        {
            if (status == SUCCESS)
            {
                List<ItemVolumeFalconMail> list = new List<ItemVolumeFalconMail>();
                ItemVolumeFalconMail item = new ItemVolumeFalconMail();
                int keyItem = 0;
                int valueItem = 0;
                for (int i = 0; i < CachePvp.megaTournamentInfo.configRewards.Count; i++)
                {
                    if (CachePvp.megaTournamentInfo.configRewards[i].win == key)
                    {
                        if (bonus)
                        {
                            keyItem = CachePvp.megaTournamentInfo.configRewards[i].bonus.key;
                            valueItem = CachePvp.megaTournamentInfo.configRewards[i].bonus.value;
                        }
                        else
                        {
                            keyItem = CachePvp.megaTournamentInfo.configRewards[i].free.key;
                            valueItem = CachePvp.megaTournamentInfo.configRewards[i].free.value;
                        }
                    }
                }
                if (keyItem != FalconMail.ITEM_BOX_CARD1 && keyItem != FalconMail.ITEM_BOX_CARD2 && keyItem != FalconMail.ITEM_BOX_CARD3 && keyItem != FalconMail.ITEM_BOX_CARD4)
                {
                    item.key = keyItem;
                    item.value = valueItem;
                }
                else
                {
                    if (keyItem == FalconMail.ITEM_BOX_CARD1)
                    {
                        GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.Tournament_why, "BoxCard1", 1, false);
                    }
                    else if (keyItem == FalconMail.ITEM_BOX_CARD2)
                    {
                        GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.Tournament_why, "BoxCard2", 1, false);
                    }
                    else if (keyItem == FalconMail.ITEM_BOX_CARD3)
                    {
                        GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.Tournament_why, "BoxCard3", 1, false);
                    }
                    else if (keyItem == FalconMail.ITEM_BOX_CARD4)
                    {
                        GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.Tournament_why, "BoxCard4", 1, false);
                    }
                    item.key = ChangeTypeToFalconMail(GetCardInBox.typeItemPopupItemReward);
                    item.value = GetCardInBox.numItemPopupItemReward;
                }
                list.Add(item);
                PopupManager.Instance.ShowClaimPopup(list, FirebaseLogSpaceWar.Tournament_why);
                AddReward();
            }
            else if (status == TOURNAMENT_REWARD_NOT_EXIST)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast("reward not exists", false, 1.5f);
            }
            else if (status == TOURNAMENT_REWARD_RECEIVED)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_claimed, false, 1.5f);
            }
            else
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(message, false, 1.5f);
            }
            MessageDispatcher.SendMessage(this, EventName.Tournament.MegaTournamentClaim.ToString(), this, 0);
        }

        void AddReward()
        {
            bool exists = false;
            for (int i = 0; i < CachePvp.megaTournamentInfo.info.rewards.Count; i++)
            {
                if (bonus)
                {
                    if (CachePvp.megaTournamentInfo.info.rewards[i].win == key)
                    {
                        exists = true;
                        CachePvp.megaTournamentInfo.info.rewards[i].bonusRewarded = true;
                        break;
                    }
                }
                else
                {
                    if (CachePvp.megaTournamentInfo.info.rewards[i].win == key)
                    {
                        exists = true;
                        CachePvp.megaTournamentInfo.info.rewards[i].rewarded = true;
                        break;
                    }
                }
            }
            //chua co key
            if (!exists)
            {
                MegaTournamentReward r = new MegaTournamentReward();
                Item i = new Item();
                i.key = key;
                Item i2 = new Item();
                i2.key = key;
                r.bonus = i;
                r.free = i2;
                if (bonus)
                {
                    r.bonusRewarded = true;
                }
                else
                {
                    r.rewarded = true;
                }
                CachePvp.megaTournamentInfo.info.rewards.Add(r);
            }
        }

        public int ChangeTypeToFalconMail(GameContext.TypeItemInPopupItemReward item)
        {
            switch (item)
            {
                case GameContext.TypeItemInPopupItemReward.Gold:
                    return FalconMail.ITEM_GOLD;
                case GameContext.TypeItemInPopupItemReward.Gem:
                    return FalconMail.ITEM_GEM;
                case GameContext.TypeItemInPopupItemReward.PowerUp:
                    return FalconMail.ITEM_POWER_UP;
                case GameContext.TypeItemInPopupItemReward.EMP:
                    return FalconMail.ITEM_ENERGY;
                case GameContext.TypeItemInPopupItemReward.Life:
                    return FalconMail.ITEM_LIFE;
                case GameContext.TypeItemInPopupItemReward.AircraftGeneralCard:
                    return FalconMail.ITEM_CARD_PLANE_GENERAL;
                case GameContext.TypeItemInPopupItemReward.Aircraft1Card:
                    return FalconMail.ITEM_CARD_PLANE_1;
                case GameContext.TypeItemInPopupItemReward.Aircraft2Card:
                    return FalconMail.ITEM_CARD_PLANE_2;
                case GameContext.TypeItemInPopupItemReward.Aircraft3Card:
                    return FalconMail.ITEM_CARD_PLANE_3;
                case GameContext.TypeItemInPopupItemReward.Aircraft6Card:
                    return FalconMail.ITEM_CARD_PLANE_4;
                case GameContext.TypeItemInPopupItemReward.Aircraft7Card:
                    return FalconMail.ITEM_CARD_PLANE_5;
                case GameContext.TypeItemInPopupItemReward.Aircraft8Card:
                    return FalconMail.ITEM_CARD_PLANE_6;
                case GameContext.TypeItemInPopupItemReward.Aircraft9Card:
                    return FalconMail.ITEM_CARD_PLANE_7;
                case GameContext.TypeItemInPopupItemReward.Aircraft10Card:
                    return FalconMail.ITEM_CARD_PLANE_8;
                case GameContext.TypeItemInPopupItemReward.DroneGeneralCard:
                    return FalconMail.ITEM_CARD_WINGMAN_GENERAL;
                case GameContext.TypeItemInPopupItemReward.Drone1Card:
                    return FalconMail.ITEM_CARD_WINGMAN_1;
                case GameContext.TypeItemInPopupItemReward.Drone2Card:
                    return FalconMail.ITEM_CARD_WINGMAN_2;
                case GameContext.TypeItemInPopupItemReward.Drone3Card:
                    return FalconMail.ITEM_CARD_WINGMAN_3;
                case GameContext.TypeItemInPopupItemReward.Drone4Card:
                    return FalconMail.ITEM_CARD_WINGMAN_4;
                case GameContext.TypeItemInPopupItemReward.Drone5Card:
                    return FalconMail.ITEM_CARD_WINGMAN_5;
                case GameContext.TypeItemInPopupItemReward.Drone6Card:
                    return FalconMail.ITEM_CARD_WINGMAN_6;
                case GameContext.TypeItemInPopupItemReward.WingGeneralCard:
                    return FalconMail.ITEM_CARD_WING_GENERAL;
                case GameContext.TypeItemInPopupItemReward.Wing1Card:
                    return FalconMail.ITEM_CARD_WING_1;
                case GameContext.TypeItemInPopupItemReward.Wing2Card:
                    return FalconMail.ITEM_CARD_WING_2;
                case GameContext.TypeItemInPopupItemReward.Wing3Card:
                    return FalconMail.ITEM_CARD_WING_3;
                case GameContext.TypeItemInPopupItemReward.Energy:
                    return FalconMail.ITEM_ENERGY;
                default:
                    return 0;
            }
        }
    }
}