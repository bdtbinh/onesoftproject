using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp {
	public class SCLocalLevelValue : SCMessage {
		public long levelValue;

		public override string GetEvent () {
			return Events.LOCAL_LEVEL_VALUE;
		}

		public override void OnData () {
			UnityEngine.Debug.Log ("SCLocalLevelValue");
		}
	}
}