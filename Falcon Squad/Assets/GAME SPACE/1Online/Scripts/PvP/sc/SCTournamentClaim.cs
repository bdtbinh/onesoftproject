﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCTournamentClaim : SCMessage
    {
        public Chest chest;
        public bool reward;

        public override string GetEvent()
        {
            return Events.CLAN_TOURNAMENT_CLAIM;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.GloryChest.TournamentClaim.ToString(), this, 0);
        }
    }
}