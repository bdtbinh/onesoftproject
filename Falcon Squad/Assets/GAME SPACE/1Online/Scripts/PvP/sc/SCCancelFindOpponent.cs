using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCCancelFindOpponent : SCMessage
    {
        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;
        public int type;
        public override string GetEvent()
        {
            return Events.CANCEL_FIND_OPPONENT;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCCancelFindOpponent");
        }
    }
}