﻿using OSNet;
namespace Mp.Pvp
{
    public class SCForceUpdate : SCMessage
    {
        public int vipPoint;
        public int gold;
        public int gem;
        public string data;

        public override string GetEvent()
        {
            return Events.FORCE_UPDATE;
        }
        public override void OnData()
        {
            CachePvp.isForceUpdate = true;

            if (vipPoint != -1)
            {
                CachePvp.myVipPoint = vipPoint;
            }
            if (gold != -1)
            {
                CachePvp.SetGold(gold, FirebaseLogSpaceWar.ServerPush_why, FirebaseLogSpaceWar.Unknown_why);
            }
            if (gem != -1)
            {
                CachePvp.SetGem(gem, FirebaseLogSpaceWar.ServerPush_why, FirebaseLogSpaceWar.Unknown_why);
            }
            if (!string.IsNullOrEmpty(data) && data.Length > 10)
            {
                CachePvp.ForceSetPlayerData(PlayerDataUtil.StringToObject(data));
            }
            CachePvp.isForceUpdate = false;
            PvpUtil.SendUpdatePlayer();
        }
    }
}