﻿using System;
using System.Collections.Generic;
using OSNet;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCClientConfig : SCMessage
    {
        public int ironsrcOfferWall;
        public int crossPromotionOfferWall;
        public long pvpSessionEndTime;
        public string assetBundleUrl = "";
        public const string ASSET_BUNDLE_URL_DEFAULT = "https://storage.googleapis.com/falcon_squad/game_config/config.txt";
        public int isDebug;
        public int intertitialCountDown = 80;
        public int intertitiRatio = 100;  // phan tram 1-100
        public int videoTopCountDown = 3; // phut
        public int videoTopMaxShownADay = 6;
        public List<int> listGoldBet = new List<int>();
        public List<int> listGoldBet2vs2 = new List<int>();
        public int turnOnTeam = 1;
        public int gemToCreateTeam = 100;
        public string forumAppId = "343950402792100";
        public int indexAircraftSellCard = 0; // mặc định =0 là sẽ không bán card của máy bay nào 
        public int turnOnVideoEndGame = 1; // mặc định =1 là sẽ có bật video
        public int turnOnPurchaseOneTime = 0; // mặc định =0 là sẽ  không sử dụng Purchase One Time



        public override string GetEvent()
        {
            return Events.CLIENT_CONFIG;
        }
        public override void OnData()
        {
            //Debug.LogError("OnData SCClientConfig");
            if (isDebug == 1)
            {
                Debug.unityLogger.logEnabled = true;
                SRDebug.Init();
            }
            CachePvp.ironsrcOfferWall = ironsrcOfferWall;
            CachePvp.crossPromotionOfferWall = crossPromotionOfferWall;
            CachePvp.timeEndSession = pvpSessionEndTime;
            if (assetBundleUrl == null || assetBundleUrl.Length <= 1)
                assetBundleUrl = ASSET_BUNDLE_URL_DEFAULT;
            CachePvp.assetBundleUrl = assetBundleUrl;
            CachePvp.listGoldBet = listGoldBet;
            CachePvp.listGoldBet2vs2 = listGoldBet2vs2;
            CachePvp.sCClientConfig = this;
        }
    }
}