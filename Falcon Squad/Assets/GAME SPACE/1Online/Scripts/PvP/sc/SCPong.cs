using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCPong : SCMessage
    {        
        public override string GetEvent()
        {
            return Events.PONG;
        }
		public override void OnData()
		{
			UnityEngine.Debug.Log("SCPong: " + message);
		}
    }
}