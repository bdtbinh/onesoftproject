﻿using OSNet;

namespace Mp.Pvp
{
    public class SCSpeaker : SCMessage
    {
        public override string GetEvent()
        {
            return Events.SPEAKER;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                PopupManager.Instance.ShowTextRunningPopup(message);
            }
        }
    }
}