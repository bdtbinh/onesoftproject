﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCTournamentInfo : SCMessage
    {
        public const int CLAN_NOT_EXIST = 10;
        //Thời gian kết thúc tournament (tính bằng giây)
        public long remainTime;

        //true: mùa giải đã kết thúc
        public bool endTournament;

        //Xác nhận đã nhận thưởng hay chưa
        public bool rewarded;

        //Có đủ đk được nhận thưởng hay không
        public bool canReward;

        //thành tích đạt được của user
        //public TournamentAchievementPvPWin achievement;
        public ClanAchievement achievement;

        //Danh sách chest phần thưởng
        public List<Chest> chests;

        //thông tin clan
        public ClanInfo clanInfo;

        //chest thưởng của player (= null khi không đủ các điều kiện nhận thưởng hoặc chest chưa được tạo)
        public Chest chest;

        public override string GetEvent()
        {
            return Events.CLAN_TOURNAMENT_INFO;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.GloryChest.TournamentInfo.ToString(), this, 0);
        }
    }
}

public class Chest
{
    public string name;
    public int glory;
    public int gold;
    public int gem;
    public int universalCard;
}

public class ClanAchievement
{
    public List<ClanAchievementItem> items;
}

public class ClanAchievementItem
{
    public int id;        // id nhiệm vụ
    public int max;            // số lượt cần thực hiện để hoàn thành nhiệm vụ
    public int current;        // số lượt hiện tại
    public int gloryPoint;     // số glory point được thưởng
}

public class TournamentAchievementPvPWin
{

    //Số trận thắng liên tiếp
    public int win;

    //Số trận thắng liên tiếp cao nhất
    public int maxWin;

    //nhiệm vụ đang làm
    public int current;

    //Nhiệm vụ đã hoàn thành
    public List<int> completed;

    //Nhiệm vụ đã hoàn thành và nhận thưởng (tính điểm) (1,2,3,5)
    public List<int> rewards;
}