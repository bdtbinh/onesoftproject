﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
using UnityEngine.SceneManagement;

namespace Mp.Pvp
{
    public class SCCoopPvPMatching : SCMessage
    {
        public const int STATUS_DUPLICATE_REQUEST = 2;
        public override string GetEvent()
        {
            return Events.COOP_PVP_MATCHING;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                if (SceneManager.GetActiveScene().name == "Home" || SceneManager.GetActiveScene().name == "SelectLevel" || SceneManager.GetActiveScene().name == "PVPMain" || SceneManager.GetActiveScene().name == "PVPEndGame")
                {
                    CachePvp.typePVP = CSFindOpponent.TYPE_2V2;
                    PopupManager.Instance.goTo2v2 = true;
                    SceneManager.LoadScene("PVPMain");
                }
            }
            else
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(message, false, 1.5f);
            }
        }
    }
}