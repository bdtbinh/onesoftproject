using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCRankMe : SCMessage
    {
        public int type;
        public int ranking;
        public override string GetEvent()
        {
            return Events.MY_RANK;
        }
		public override void OnData()
		{
			UnityEngine.Debug.Log("SCRankMe");
		}
    }
}