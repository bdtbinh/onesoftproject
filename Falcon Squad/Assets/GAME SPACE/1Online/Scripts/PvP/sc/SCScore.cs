using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCScore : SCMessage
    {
        public const int INVALID_REQUEST = 2;
        public string code;
        public int score;
        public int wave;
        public string datas;
        public override string GetEvent()
        {
            return Events.SCORE;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCScore: " + code + " " + score + " " + wave + " " + datas);
            if (InGamePvP.Instance != null)
            {
                int a;
                if (int.TryParse(datas, out a))
                {
                    InGamePvP.Instance.ChangeOpponentScore(score, a, code);
                }
            }
        }
    }
}