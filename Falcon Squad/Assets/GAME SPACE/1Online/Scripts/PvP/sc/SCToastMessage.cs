using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCToastMessage : SCMessage
    {
        public override string GetEvent()
        {
            return Events.TOAST_MESSAGE;
        }
        public override void OnData()
        {
            PopupManager.Instance.ShowToast(message);
        }
    }
}