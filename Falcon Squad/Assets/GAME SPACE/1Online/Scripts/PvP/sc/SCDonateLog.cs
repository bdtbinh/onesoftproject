﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCDonateLog : SCMessage
    {
        public List<ClanDonateData> logs;

        public override string GetEvent()
        {
            return Events.CLAN_DONATE_LOG;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.Clan.DonateLog.ToString(), this, 0);
        }
    }
}