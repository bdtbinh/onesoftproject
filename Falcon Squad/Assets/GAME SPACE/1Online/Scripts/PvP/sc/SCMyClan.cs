﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCMyClan : SCMessage
    {
        public ClanInfo info;
        public override string GetEvent()
        {
            return Events.MY_CLAN;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                MessageDispatcher.SendMessage(this, EventName.Clan.LoadMyClan.ToString(), info, 0);
            }
        }
    }
}