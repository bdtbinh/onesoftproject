﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCLiveScoreLeave : SCMessage
    {
        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;

        public int type;
        public override string GetEvent()
        {
            return Events.LIVE_SCORE_LEAVE;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
            }
        }
    }
}