using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCFacebookFriends : SCMessage
    {
        public override string GetEvent()
        {
            return Events.FACEBOOK_FRIENDS;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(EventName.LoginFacebook.CheckFriends.ToString());
        }
    }
}