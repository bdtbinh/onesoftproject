﻿using OSNet;
using System.Collections.Generic;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCMyClanInvitation : SCMessage
    {
        public List<ClanInfo> clans;
        public override string GetEvent()
        {
            return Events.MY_CLAN_INVITATION;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.LoadClanInvite.ToString(), clans, 0);
        }
    }
}