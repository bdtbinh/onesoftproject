﻿using OSNet;
using System.Collections.Generic;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCClanSuggestion : SCMessage
    {
        public const int JOINED_CLAN = 2;
        public List<ClanInfo> clans;
        public override string GetEvent()
        {
            return Events.CLAN_LIST_SUGGEST;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                MessageDispatcher.SendMessage(this, EventName.Clan.LoadedSuggestClan.ToString(), clans, 0);
            }
        }
    }
}