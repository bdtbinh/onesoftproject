﻿using OSNet;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCClanJoinRequest : SCMessage
    {
        public long id;
        public const int JOINED_CLAN = 3;
        public const int JOIN_REQUEST_EXISTED = 4;
        public const int CLAN_NOT_EXIST = 10;
        public const int CLAN_FULL_MEMBER = 11;
        public const int NOT_ENOUGH_LEVEL = 20;
        public override string GetEvent()
        {
            return Events.CLAN_JOIN_REQUEST;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.RequestedJoinClan.ToString(), id, 0);
        }
    }
}