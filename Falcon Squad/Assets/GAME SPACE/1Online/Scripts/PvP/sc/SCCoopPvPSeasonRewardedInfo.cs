﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCCoopPvPSeasonRewardedInfo : SCMessage
    {
        public List<PromotionItem> items;
        public override string GetEvent()
        {
            return Events.COOP_PVP_SEASON_REWARD_INFO;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.PVP.CoopPvPSeasonRewardedInfo.ToString(), this, 0);
        }
    }
}