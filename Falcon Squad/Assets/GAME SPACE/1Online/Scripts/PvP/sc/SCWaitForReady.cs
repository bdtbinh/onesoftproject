using System;
using System.Collections.Generic;
using OSNet;
using UnityEngine.SceneManagement;

namespace Mp.Pvp
{
    public class SCWaitForReady : SCMessage
    {
        public string roomId;
        public int delayTime;
        public int map;
        public List<Player> players;
        public int playTime;

        public int waveCacheIndex;
        public string waveCacheData;

        public override string GetEvent()
        {
            return Events.WAIT_FOR_READY;
        }
        public override void OnData()
        {
            CachePvp.PlayTime = playTime;
            UnityEngine.Debug.Log("SCWaitForReady");
            if (waveCacheIndex != -1 && waveCacheData != null && waveCacheData.Length > 0)
            {
                PvPDataHelper.Instance.SetWaveCacheIndexForPvP(waveCacheIndex);
                PvPDataHelper.Instance.SetDataForPvP(waveCacheData);
            }
            else
            {
                PvPDataHelper.Instance.SetWaveCacheIndexForPvP(PvPDataHelper.Instance.GetMyWaveCacheIndex());
                PvPDataHelper.Instance.SetDataForPvP(PvPDataHelper.Instance.GetMyDataString());
            }

            if (PvPUIManager.Instance != null)
            {
                if (PvPUIManager.Instance.popupFindPvP.gameObject.activeInHierarchy)
                {
                    PvPUIManager.Instance.popupFindPvP.StartLoadingGameScene(map, delayTime, players, roomId, CachePvp.typePVP);
                    CachePvp.roomId = "";
                    CachePvp.delayTime = 0;
                    CachePvp.map = 0;
                    CachePvp.players = null;
                }
                else
                {
                    CachePvp.roomId = roomId;
                    CachePvp.delayTime = delayTime;
                    CachePvp.map = map;
                    CachePvp.players = players;
                    PopupManager.Instance.goToPVP = true;
                    //SceneManager.LoadScene("PVPMain");
                    PvPUIManager.Instance.popupMainPvP.gameObject.SetActive(false);
                    PvPUIManager.Instance.popupFindPvP.gameObject.SetActive(true);
                }
            }
            else
            {
                CachePvp.roomId = roomId;
                CachePvp.delayTime = delayTime;
                CachePvp.map = map;
                CachePvp.players = players;
                PopupManager.Instance.goToPVP = true;
                SceneManager.LoadScene("PVPMain");
            }
        }
    }
}