﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCPvPSessionRewardedClaim : SCMessage
    {
        public override string GetEvent()
        {
            return Events.PVP_SESSION_REWARDED_CLAIM;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.PVP.SessionRewardedClaim.ToString(), this, 0);
        }
    }
}