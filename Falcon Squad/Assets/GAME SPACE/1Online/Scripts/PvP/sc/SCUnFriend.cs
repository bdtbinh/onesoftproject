﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCUnFriend : SCMessage
    {
        public string code;//ID của bạn bè muốn hủy
        public override string GetEvent()
        {
            return Events.UNFRIEND;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.LoadProfile.Unfriend.ToString(), this, 0);
        }
    }
}