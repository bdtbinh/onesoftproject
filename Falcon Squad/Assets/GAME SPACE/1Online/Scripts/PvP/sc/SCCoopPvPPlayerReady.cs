﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCCoopPvPPlayerReady : SCMessage
    {
        public override string GetEvent()
        {
            return Events.COOP_PVP_READY;
        }
        public override void OnData()
        {
        }
    }
}