using System;
using System.Collections.Generic;
using Facebook.Unity;
using OSNet;
using com.ootii.Messages;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCValidateDeviceId : SCMessage
    {
        public string token;
        public Player player;
        public override string GetEvent()
        {
            return Events.VALIDATE_DEVICE_ID;
        }
        public override void OnData()
        {
            if (player == null)
            {
                new CSGetToken(CachePvp.deviceId, CachePvp.FacebookId).Send();
                MessageDispatcher.SendMessage(EventName.SyncData.NoSync.ToString());
            }
            else
            {
                //hien popup dong bo
                if (player.debug)
                {
                    Debug.unityLogger.logEnabled = true;
                    SRDebug.Init();
                }
                //if (SceneManager.GetActiveScene().name == "InitScene")
                //{
                UnityEngine.Debug.Log("CachePvp.Code : " + CachePvp.Code + " - player.code : " + player.code);
                if (string.IsNullOrEmpty(CachePvp.Code) || !CachePvp.Code.Equals(player.code))
                {
                    if (PopupManager.Instance.isTest || CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL) > 1)
                    {
                        CachePvp.timeSinceStartUp = (long)(Time.realtimeSinceStartup * 1000);
                        if (SceneManager.GetActiveScene().name == "InitScene" || SceneManager.GetActiveScene().name == "Home" || SceneManager.GetActiveScene().name == "SelectLevel")
                        {
                            PopupManager.Instance.ShowSyncPopup(player, token);
                            CachePvp.needSync = false;
                            Debug.Log("CachePvp.needSync : " + CachePvp.needSync);
                        }
                        else
                        {
                            CachePvp.needSync = true;
                            Debug.Log("CachePvp.needSync : " + CachePvp.needSync);
                        }
                        MessageDispatcher.SendMessage(EventName.SyncData.StopCoroutineWait.ToString());
                    }
                    else
                    {
                        if (SceneManager.GetActiveScene().name == "InitScene" || SceneManager.GetActiveScene().name == "Home" || SceneManager.GetActiveScene().name == "SelectLevel")
                        {
                            DataSync data = new DataSync();
                            data.player = player;
                            data.token = token;
                            MessageDispatcher.SendMessage(this, EventName.SyncData.SyncFromServerToClient.ToString(), data, 0);
                        }
                        else
                        {
                            CachePvp.SetPlayerData(player, token);
                            if (CachePvp.facebookUser != null)
                            {
                                new CSFacebookConnect(SocialManager.Instance.FBGetUserToken(), CachePvp.facebookUser.id, CachePvp.facebookUser.name, 2).Send();
                            }
                        }
                    }
                }
                //}
            }
        }
    }
}