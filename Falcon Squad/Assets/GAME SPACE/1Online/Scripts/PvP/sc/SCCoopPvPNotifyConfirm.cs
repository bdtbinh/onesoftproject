﻿using OSNet;
using UnityEngine.SceneManagement;

namespace Mp.Pvp
{
    public class SCCoopPvPNotifyConfirm : SCMessage
    {
        public const int ROOM_NOT_EXIST = 2;     // phòng không tồn tại
        public const int DENIED_SUCCESS = 3;

        public int confirm; //0: accept, 1: denied
        public override string GetEvent()
        {
            return Events.COOP_PVP_NOTIFY_CONFIRM;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                if (confirm == 0)
                {
                    CachePvp.typePVP = CSFindOpponent.TYPE_2V2;
                    PopupManager.Instance.goTo2v2 = true;
                    SceneManager.LoadScene("PVPMain");
                }
            }
            else if (status == ROOM_NOT_EXIST)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_pvp_room_destroyed, false);
            }
        }
    }
}