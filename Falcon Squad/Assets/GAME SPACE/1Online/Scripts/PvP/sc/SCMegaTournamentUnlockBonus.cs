﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCMegaTournamentUnlockBonus : SCMessage
    {
        public int TOURNAMENT_WAS_END = 31;

        public override string GetEvent()
        {
            return Events.MEGA_TOURNAMENT_UNLOCK_BONUS;
        }

        public override void OnData()
        {
            if (status == SUCCESS)
            {
                PanelCoinGem.Instance.AddGemTop(-CachePvp.megaTournamentInfo.info.unlockBonusGem, "Tournament_Unlock_Bonus");
                CachePvp.megaTournamentInfo.info.unlocked = true;
                MessageDispatcher.SendMessage(this, EventName.Tournament.MegaTournamentUnlockBonus.ToString(), this, 0);
            }
            else if (status == TOURNAMENT_WAS_END)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_tourmanet_not_start_yet, false, 1.5f);
            }
            else
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(message, false, 1.5f);
            }
        }
    }
}