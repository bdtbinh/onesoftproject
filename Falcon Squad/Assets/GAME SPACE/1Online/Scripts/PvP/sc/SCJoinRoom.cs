using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCJoinRoom : SCMessage
    {
        public const int JOINED_ROOM = 2;
        public const int NOT_EXIST_ROOM = 3;
        public override string GetEvent()
        {
            return Events.JOIN_ROOM;
        }
		public override void OnData()
		{
			UnityEngine.Debug.Log("SCJoinRoom");
		}
    }
}