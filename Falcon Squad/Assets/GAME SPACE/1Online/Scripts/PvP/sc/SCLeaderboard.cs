using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCLeaderboard : SCMessage
    {
        public Leaderboard leaderboard;

        public override string GetEvent()
        {
            return Events.LEADERBOARD;
        }

        public override void OnData()
        {
            UnityEngine.Debug.Log("SCLeaderboard");
            switch (leaderboard.type)
            {
                case 0:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedWorldRank.ToString(), leaderboard, 0);
                    break;
                case 1:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedLocalRank.ToString(), leaderboard, 0);
                    break;
                case 2:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedFacebookRank.ToString(), leaderboard, 0);
                    break;
                case 3:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedWorldRank.ToString(), leaderboard, 0);
                    break;
                case 4:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedLocalRank.ToString(), leaderboard, 0);
                    break;
                case 5:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedFacebookRank.ToString(), leaderboard, 0);
                    break;
                case 6:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedWorldMegaRank.ToString(), leaderboard, 0);
                    break;
                case 7:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedLocalMegaRank.ToString(), leaderboard, 0);
                    break;
                case 8:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedFacebookMegaRank.ToString(), leaderboard, 0);
                    break;
                case 9:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedWorldRank.ToString(), leaderboard, 0);
                    break;
                case 10:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedLocalRank.ToString(), leaderboard, 0);
                    break;
                case 11:
                    MessageDispatcher.SendMessage(this, EventName.LoadLeaderboard.LoadedFacebookRank.ToString(), leaderboard, 0);
                    break;
                default:
                    break;
            }
        }
    }
}