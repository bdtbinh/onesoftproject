﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCMyShop : SCMessage
    {
        public Shop shop;
        public override string GetEvent()
        {
            return Events.MY_SHOP;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                CachePvp.listShopItem = shop.shopItems;
            }
            MessageDispatcher.SendMessage(this, EventName.Shop.MyShop.ToString(), this, 0);
        }
    }
}

public class Shop
{
    public List<ShopItem> shopItems;

    public int medal;
    public int resetPrice;
    public long timeToReset;
    public int resetRemain;
}

public class ShopItem
{
    public int id;
    public string name;
    public int quantity;
    public int price;
    public bool soldOut;
}