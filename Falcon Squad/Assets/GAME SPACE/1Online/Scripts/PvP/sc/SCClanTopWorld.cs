﻿using OSNet;
using com.ootii.Messages;
using System.Collections.Generic;

namespace Mp.Pvp
{
    public class SCClanListTopWorld : SCMessage
    {
        public List<ClanInfo> clans;

        public override string GetEvent()
        {
            return Events.CLAN_LIST_TOP_WORLD;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.LoadListClanTopWorld.ToString(), clans, 0);
        }
    }
}