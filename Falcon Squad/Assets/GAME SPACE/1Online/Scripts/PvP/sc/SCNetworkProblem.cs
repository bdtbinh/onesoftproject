using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCNetworkProblem : SCMessage
    {
        public string token;
        public RoomInfo roomInfo;
        public override string GetEvent()
        {
            return Events.NETWORK_PROBLEM;
        }
		public override void OnData()
		{
			UnityEngine.Debug.Log("SCNetworkProblem");
		}
    }
}