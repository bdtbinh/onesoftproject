using System;
using System.Collections.Generic;
using Facebook.Unity;
using OSNet;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCUpdateDeviceId : SCMessage
    {
        public override string GetEvent()
        {
            return Events.UPDATE_DEVICE_ID;
        }
        public override void OnData()
        {
            if (status == SCMessage.SUCCESS)
            {
                CachePvp.IsSentValidateId = 1;
            }
        }
    }
}