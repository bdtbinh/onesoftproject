﻿using System;
using System.Collections.Generic;
using OSNet;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCCoopPvPNotify : SCMessage
    {
        // id của phòng gửi thông báo
        public string roomId;

        // cờ trạng thái
        public const int FLAG_ON = 0;
        public const int FLAG_OFF = 1;
        public int flag;

        // số gold đặt cược
        public int gold;

        // danh sách player đang trong phòng
        public List<CoopPvPPlayerData> players;

        public override string GetEvent()
        {
            return Events.COOP_PVP_NOTIFY;
        }
        public override void OnData()
        {
            Debug.Log("SCCoopPvPNotify");
            if (status == SUCCESS)
            {
                if ((flag == FLAG_ON && CachePvp.GetGold() >= gold) || flag == FLAG_OFF)
                {
                    CachePvp.goldBetFromInvitation2v2 = gold;
                    PopupManager.Instance.ShowInviteButton2v2(roomId, players, flag, gold);
                }
            }
        }
    }
}