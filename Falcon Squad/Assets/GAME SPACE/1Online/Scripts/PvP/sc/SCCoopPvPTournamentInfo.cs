﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCCoopPvPTournamentInfo : SCMessage
    {
        public PvP2vs2TournamentInfo info;
        public override string GetEvent()
        {
            return Events.COOP_PVP_TOURNAMENT_INFO;
        }
        public override void OnData()
        {
            CachePvp.coopPvPTournamentInfo = this;
            MessageDispatcher.SendMessage(this, EventName.PVP.CoopPvPTournamentInfo.ToString(), this, 0);
        }
    }
}

public class PvP2vs2TournamentInfo
{
    // id của mùa giải
    public long id;

    // trạng thái mùa giải
    public const int START = 1;      // đang diễn ra
    public const int END = 0;         // đang kết thúc
    public int state;

    // thời gian 
    public long startTimeRemain;           // thời gian sắp diễn ra (đơn vị giây)
    public long endTimeRemain;            // thời gian sắp kết thúc ( đơn vị giây) 

    // phần thưởng của mùa giải đang diễn ra
    public string chestName;               // BRONZE | SILVER | GOLD | PLATINUM | DIAMOND
    public int gold;
    public int gem;
    public int wingGeneral;

    // cấu hình phần thưởng mùa giải
    public List<PvP2vs2SeasonRewardData> rewards;
}

public class PvP2vs2SeasonRewardData
{
    public string name;            // tên phần thưởng     BRONZE | SILVER | GOLD | PLATINUM | DIAMOND
    public int scoreWin;           // điểm đạt được nếu thắng 1 trận PvP 2vs2
    public int scoreLose;          // điểm bị trừ nếu thua 1 trận PvP 2vs2

    public int scoreReward;        // điểm tối thiểu cần đạt được để có thể nhận phần thưởng
    public int gold;               // phần thưởng vàng
    public int gem;                // phần thưởng gem
    public int wing;               // phần thưởng wing
}