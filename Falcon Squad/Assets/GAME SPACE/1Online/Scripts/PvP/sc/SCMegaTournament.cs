﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCMegaTournament : SCMessage
    {
        public int NOT_LOGGED_IN = 2;
        public EventInfo tournament;

        public override string GetEvent()
        {
            return Events.MEGA_TOURNAMENT;
        }

        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.Tournament.MegaTournament.ToString(), this, 0);
        }
    }
}