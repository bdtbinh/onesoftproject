﻿using OSNet;
using com.ootii.Messages;
using System.Collections.Generic;

namespace Mp.Pvp
{
    public class SCClanMember : SCMessage
    {
        public long clanId;
        public List<Player> members;
        public override string GetEvent()
        {
            return Events.CLAN_MEMBER;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                MessageDispatcher.SendMessage(this, clanId, EventName.Clan.ClanMember.ToString(), members, 0);
            }
        }
    }
}