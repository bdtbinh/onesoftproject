using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCPVPTimeRemaining : SCMessage
    {
        public int amount;
        public override string GetEvent()
        {
            return Events.PVP_TIME_REMAINING;
        }
        public override void OnData()
        {
            if (InGamePvP.Instance)
            {
                //PopupManager.Instance.ShowToast(message);
                InGamePvP.Instance.SetTimeRemaining(amount);
            }
        }
    }
}