﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCRandomShopReset : SCMessage
    {
        public const int NOT_ENOUGH_RESET_QUOTA = 3;

        public RandomShop shop;
        public override string GetEvent()
        {
            return Events.RANDOM_SHOP_RESET;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.RandomShop.RandomShopReset.ToString(), this, 0);
        }
    }
}
