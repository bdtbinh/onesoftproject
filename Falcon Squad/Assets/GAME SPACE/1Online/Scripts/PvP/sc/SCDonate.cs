﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCDonate : SCMessage
    {
        public const int NOT_EXIST = 2;
        public const int EXPIRED = 3;
        public const int FULLY = 4;
        public const int NOT_ENOUNGH_QUOTA = 5;
        public const int DUPLICATE = 6;

        public string chatId;
        public int planeId;
        public int medal;
        public int incrementMedal;

        public override string GetEvent()
        {
            return Events.CLAN_DONATE;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                int cardAmount = -1;
                switch (planeId)
                {
                    case FalconMail.ITEM_CARD_PLANE_1:
                        MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.BataFD, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD) + cardAmount, EventName.CLAN_DONATE, "", AircraftTypeEnum.BataFD.ToString());
                        break;
                    case FalconMail.ITEM_CARD_PLANE_2:
                        MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.SkyWraith, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith) + cardAmount, EventName.CLAN_DONATE, "", AircraftTypeEnum.SkyWraith.ToString());
                        break;
                    case FalconMail.ITEM_CARD_PLANE_3:
                        MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.FuryOfAres, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres) + cardAmount, EventName.CLAN_DONATE, "", AircraftTypeEnum.FuryOfAres.ToString());
                        break;
                    case FalconMail.ITEM_CARD_PLANE_4:
                        MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.MacBird, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird) + cardAmount, EventName.CLAN_DONATE, "", AircraftTypeEnum.MacBird.ToString());
                        break;
                    case FalconMail.ITEM_CARD_PLANE_5:
                        MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.TwilightX, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX) + cardAmount, EventName.CLAN_DONATE, "", AircraftTypeEnum.TwilightX.ToString());
                        break;
                    case FalconMail.ITEM_CARD_PLANE_6:
                        MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.StarBomb, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.StarBomb) + cardAmount, EventName.CLAN_DONATE, "", AircraftTypeEnum.StarBomb.ToString());
                        break;
                    case FalconMail.ITEM_CARD_PLANE_7:
                        MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.IceShard, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.IceShard) + cardAmount, EventName.CLAN_DONATE, "", AircraftTypeEnum.IceShard.ToString());
                        break;
                    case FalconMail.ITEM_CARD_PLANE_8:
                        MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.ThunderBolt, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.ThunderBolt) + cardAmount, EventName.CLAN_DONATE, "", AircraftTypeEnum.ThunderBolt.ToString());
                        break;
                    case FalconMail.ITEM_CARD_WINGMAN_1:
                        MinhCacheGame.SetWingmanCards(WingmanTypeEnum.GatlingGun, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.GatlingGun) + cardAmount, EventName.CLAN_DONATE, "", WingmanTypeEnum.GatlingGun.ToString());
                        break;
                    case FalconMail.ITEM_CARD_WINGMAN_2:
                        MinhCacheGame.SetWingmanCards(WingmanTypeEnum.AutoGatlingGun, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.AutoGatlingGun) + cardAmount, EventName.CLAN_DONATE, "", WingmanTypeEnum.AutoGatlingGun.ToString());
                        break;
                    case FalconMail.ITEM_CARD_WINGMAN_3:
                        MinhCacheGame.SetWingmanCards(WingmanTypeEnum.Lazer, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Lazer) + cardAmount, EventName.CLAN_DONATE, "", WingmanTypeEnum.Lazer.ToString());
                        break;
                    case FalconMail.ITEM_CARD_WINGMAN_4:
                        MinhCacheGame.SetWingmanCards(WingmanTypeEnum.DoubleGalting, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.DoubleGalting) + cardAmount, EventName.CLAN_DONATE, "", WingmanTypeEnum.DoubleGalting.ToString());
                        break;
                    case FalconMail.ITEM_CARD_WINGMAN_5:
                        MinhCacheGame.SetWingmanCards(WingmanTypeEnum.HomingMissile, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.HomingMissile) + cardAmount, EventName.CLAN_DONATE, "", WingmanTypeEnum.HomingMissile.ToString());
                        break;
                    case FalconMail.ITEM_CARD_WINGMAN_6:
                        MinhCacheGame.SetWingmanCards(WingmanTypeEnum.Splasher, MinhCacheGame.GetWingmanCards(WingmanTypeEnum.Splasher) + cardAmount, EventName.CLAN_DONATE, "", WingmanTypeEnum.Splasher.ToString());
                        break;
                    case FalconMail.ITEM_CARD_WING_1:
                        MinhCacheGame.SetWingCards(WingTypeEnum.WingOfJustice, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfJustice) + cardAmount, EventName.CLAN_DONATE, "", WingTypeEnum.WingOfJustice.ToString());
                        break;
                    case FalconMail.ITEM_CARD_WING_2:
                        MinhCacheGame.SetWingCards(WingTypeEnum.WingOfRedemption, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfRedemption) + cardAmount, EventName.CLAN_DONATE, "", WingTypeEnum.WingOfRedemption.ToString());
                        break;
                    case FalconMail.ITEM_CARD_WING_3:
                        MinhCacheGame.SetWingCards(WingTypeEnum.WingOfResolution, MinhCacheGame.GetWingCards(WingTypeEnum.WingOfResolution) + cardAmount, EventName.CLAN_DONATE, "", WingTypeEnum.WingOfResolution.ToString());
                        break;
                    default:
                        break;
                }
            }
            MessageDispatcher.SendMessage(this, EventName.Clan.SCDonate.ToString(), this, 0);
        }
    }
}