﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
using Mp.Pvp;

namespace Mp.Pvp
{
    public class SCMyClanRequest : SCMessage
    {
        public List<ClanInfo> clans;
        public override string GetEvent()
        {
            return Events.MY_CLAN_REQUEST;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.MyClanRequest.ToString(), clans, 0);
        }
    }
}