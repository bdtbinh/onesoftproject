﻿using Mp.Pvp;
using OSNet;
using System.Collections.Generic;
using com.ootii.Messages;
public class SCGiftcode : SCMessage
{
    public const int GIFTCODE_WAS_USED = 36;
    public const int GIFTCODE_WAS_NOT_FOUND = 37;
    public string action; //always = "check" so ignore this param
    public Giftcode data;

    public override string GetEvent()
    {
        return Events.GIFT_CODE;
    }

    public override void OnData()
    {
        MessageDispatcher.SendMessage(this, EventName.UIHome.GiftCode.ToString(), this, 0);
    }
}

public class Giftcode
{
    public string code;
    public string desc;
    public List<GiftcodeItem> items;
}

public class GiftcodeItem
{
    public int id;
    public string item;
    public int value;
}