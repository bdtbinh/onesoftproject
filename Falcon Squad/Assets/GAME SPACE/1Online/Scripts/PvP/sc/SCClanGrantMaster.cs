﻿using OSNet;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCClanGrantMaster : SCMessage
    {
        public string member;
        public override string GetEvent()
        {
            return Events.CLAN_GRANT_MASTER;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.GrantMaster.ToString(), member, 0);
        }
    }
}