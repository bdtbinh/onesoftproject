using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCPlayerOut : SCMessage
    {
        public int NOT_LOGGED_IN = 2;
        public int NOT_IN_ROOM = 3;
        public int OUT_WAIT_FOR_READY_SUCCESS = 4;
        public int OUT_START_SUCCESS = 5;
        public int INVALID_GAME_STATE = 6;
        //public string code;
        public Player player;

        public override string GetEvent()
        {
            return Events.PLAYER_OUT;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCPlayerOut");
            UnityEngine.Debug.Log("cache pvp code : " + CachePvp.Code + " - this.code" + player.code);
            if (!player.code.Equals(CachePvp.Code))
            {
                if (status == 4)
                {
                    if (PopupFindPvP.Instance != null)
                    {
                        PopupFindPvP.Instance.OpponentOut();
                    }
                }
                else if (status == 5)
                {
                    if (InGamePvP.Instance != null)
                    {
                        InGamePvP.Instance.OpponentOut();
                    }
                }
            }
        }
    }
}