﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
using UnityEngine.SceneManagement;

namespace Mp.Pvp
{
    public class SCCoopPvPInvite : SCMessage
    {
        // trạng thái request
        public const int INVITING = 2; // thông báo cho A là hệ thống đang gửi lời mời
        public const int INVITED = 3; //  thông báo cho B là A đang mời 
        public const int OFFLINE = 5; // B offline
        public const int BUSY = 6; // B bận
        public const int PVP_MAINTAIN = 7; // mode PvP đang bảo trì
        public const int DUPLICATE = 8; // đang mời rồi, không mời được nữa. đợi hết timeout rồi mời tiếp
        public const int TIMEOUT = 9; // quá timeout

        public int timeout;
        public Player player; // người thực hiện yêu cầu
        public int goldBet;
        public override string GetEvent()
        {
            return Events.COOP_PVP_INVITE;
        }
        public override void OnData()
        {
            if (status == INVITED)
            {
                if (SceneManager.GetActiveScene().name == "Home" || SceneManager.GetActiveScene().name == "SelectLevel" || SceneManager.GetActiveScene().name == "PVPMain" || SceneManager.GetActiveScene().name == "PVPEndGame")
                {
                    if (!PopupManager.Instance.IsInvitePopupActive() && CachePvp.GetGold() >= goldBet)
                    {
                        PopupManager.Instance.cachedPlayer = player;
                        CachePvp.typePVP = CSFindOpponent.TYPE_2V2;
                        PopupManager.Instance.ShowInvitePopup(CachePvp.TYPE_FRIEND_2v2, goldBet);
                    }
                }
            }
            else
            {
                if (status == OFFLINE)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_invite_user_offline, false, 1.5f);
                }
                else if (status == INVITING)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_you_have_invited_to_friend, true);
                }
                else if (status == BUSY)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_invite_user_busy, false, 1.5f);
                }
                else if (status == PVP_MAINTAIN)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_invite_server_maintain, false, 1.5f);
                }
                else if (status == DUPLICATE)
                {
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_invite_duplicate_invite, false, 1.5f);
                }
                else if (status == TIMEOUT)
                {
                    PopupManager.Instance.HideInvitePopup();
                    //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_invite_timeout, false, 1.5f);
                }
            }
        }
    }
}