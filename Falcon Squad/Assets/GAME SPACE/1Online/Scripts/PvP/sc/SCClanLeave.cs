﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCClanLeave : SCMessage
    {
        public const int CLAN_NOT_EXIST = 10;
        public const int NOT_A_MEMBER = 13;
        public const int GRANT_MASTER_BEFORE_LEAVE = 16;

        public override string GetEvent()
        {
            return Events.CLAN_LEAVE;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.Clan.LeaveClan.ToString(), status, 0);
        }
    }
}