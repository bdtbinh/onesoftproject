﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCClanMailNotify : SCMessage
    {
        public const int STATUS_UNAUTHORITY = 12;
        public override string GetEvent()
        {
            return Events.CLAN_MAIL;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                MessageDispatcher.SendMessage(this, EventName.TeamMail.SentMail.ToString(), this, 0);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.title_mail_sent, true);
            }
            else if (status == STATUS_UNAUTHORITY)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_permission_error, false);
            }
            else
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(message, false);
            }
        }
    }
}