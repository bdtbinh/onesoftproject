using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCConfirmOpponentFidingNotify : SCMessage
    {
        public const int RIVAL_BUSY = 2;
        public const int TIMEOUT = 3;
        public const int DENIED_SUCCESS = 4;
        public const int ACCEPT_SUCESS = 5;

        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;
        public int type;

        public override string GetEvent()
        {
            return Events.CONFIRM_OPPONENT_FINDING_NOTIFY;
        }

        public override void OnData()
        {
            UnityEngine.Debug.Log("SCConfirmOpponentFidingNotify");
            if (status == ACCEPT_SUCESS)
            {
            }
            else if (status == RIVAL_BUSY || status == TIMEOUT)
            {
                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.msg_invite_user_busy);
            }
        }
    }
}