﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCAppCenterConnect : SCMessage
    {
        public Player player;
        public string token;
        public override string GetEvent()
        {
            return Events.GAMECENTER_CONNECT;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCFacebookConnect");
            CachePvp.Token = token;
            if (player.code.Length >= 3)
            {
                CachePvp.Code = player.code;
            }
            CachePvp.Name = player.name;
            CachePvp.GamecenterId = player.appCenterId;
            CachePvp.PlayerLevel = player.playerLevel;
            CachePvp.MegaPlayerLevel = player.megaPlayerLevel;
            if (player.pvp2vs2PlayerLevel != null)
            {
                CachePvp.PlayerLevel2vs2 = player.pvp2vs2PlayerLevel;
            }
            CachePvp.FriendshipStats = player.friendshipStats;
            if (player.friendshipStats != null)
            {
                CachePvp.CountFriendsMinh = player.friendshipStats.numFriendFb;
            }
            CachePvp.PlayerClan = player.playerClan;
            if (player.pvP2vs2SeasonData != null)
            {
                CachePvp.PvP2vs2SeasonData = player.pvP2vs2SeasonData;
            }
            //CacheAvatarManager.Instance.DownloadImageFormUrl(CachePvp.facebookUser.picture.data.url, CachePvp.facebookUser.id);
            MessageDispatcher.SendMessage(EventName.LoginFacebook.LoggedFacebook.ToString());
            SocialManager.Instance.SetFriendsFacebookToServer();
        }
    }
}