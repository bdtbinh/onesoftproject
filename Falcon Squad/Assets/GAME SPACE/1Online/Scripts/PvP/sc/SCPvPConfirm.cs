﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCPvPConfirm : SCMessage
    {
        public Player player;
        public const int OFFLINE = 3; // B offline
        public const int BUSY = 4; // B bận
        public const int PVP_MAINTAIN = 5; // PvP đang bảo trì
        public const int TIMEOUT = 6; // hết timeout
        public const int DENIDED = 7; // A denied

        public override string GetEvent()
        {
            return Events.PVP_INVITE_CONFIRM;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                MinhCacheGame.SetBettingAmountPvP(0);
                PopupManager.Instance.GoToPVP();
            }
            else
            {
                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.msg_invite_user_busy);
                PopupManager.Instance.HideInvitePopup();
            }
            //MessageDispatcher.SendMessage(this, EventName.PVP.InvitePVPConfirm.ToString(), this, 0);
        }
    }
}