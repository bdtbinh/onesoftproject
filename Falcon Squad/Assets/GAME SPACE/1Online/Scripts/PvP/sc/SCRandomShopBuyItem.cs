﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCRandomShopBuyItem : SCMessage
    {
        public const int ITEM_NOT_IN_SHOP = 3;
        public const int ITEM_SOLD_OUT = 4;

        public int itemId;
        public RandomShop randomShop;

        public override string GetEvent()
        {
            return Events.RANDOM_SHOP_BUY_ITEM;
        }

        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.RandomShop.RandomShopBuyItem.ToString(), this, 0);
        }
    }
}