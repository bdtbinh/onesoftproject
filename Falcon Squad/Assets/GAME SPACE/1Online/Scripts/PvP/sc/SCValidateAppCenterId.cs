﻿using System;
using System.Collections.Generic;
using Facebook.Unity;
using OSNet;
using com.ootii.Messages;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCValidateAppCenterId : SCMessage
    {
        public Player player;
        public override string GetEvent()
        {
            return Events.VALIDATE_GAMECENTER_ID;
        }
        public override void OnData()
        {
            if (player == null)
            {
                new CSAppCenterConnect("", Social.localUser.id, Social.localUser.userName, 1).Send();
            }
            else
            {
                if (player.debug)
                {
                    Debug.unityLogger.logEnabled = true;
                    SRDebug.Init();
                }
                //hien popup dong bo
                if (SceneManager.GetActiveScene().name == "Home")
                {
                    if (string.IsNullOrEmpty(CachePvp.GamecenterId))
                    {
                        PopupManager.Instance.ShowSyncPopup(player);
                    }
                    else
                    {
                        new CSAppCenterConnect("", Social.localUser.id, Social.localUser.userName, 1).Send();
                    }
                }
            }
        }
    }
}