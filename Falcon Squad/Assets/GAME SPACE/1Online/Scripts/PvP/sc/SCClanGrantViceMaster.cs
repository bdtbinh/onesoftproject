﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCClanGrantViceMaster : SCMessage
    {
        public string member;
        public override string GetEvent()
        {
            return Events.CLAN_GRANT_VICE_MASTER;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.GrantViceMaster.ToString(), member, 0);
        }
    }
}