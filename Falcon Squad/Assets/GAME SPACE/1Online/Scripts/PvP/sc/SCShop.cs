using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCShop : SCMessage
    {
        public List<int> ordersGold; //5,6,4,3,2,1
        public List<int> promotionsGold; //5
        public int serverX2PurchaseGold; //0 :no, 1 : yes
        public List<int> ordersGem; //5,6,4,3,2,1
        public List<int> promotionsGem; //5
        public int serverX2PurchaseGem; //0 :no, 1 : yes
        public int firstTabIndex; //0 : show gold, 1 : show gem;

        public override string GetEvent()
        {
            return Events.SHOP;
        }
        public override void OnData()
        {
            CachePvp.listOrdersGold = ordersGold;
            CachePvp.listPromotionsGold = promotionsGold;
            CachePvp.listOrdersGem = ordersGem;
            CachePvp.listPromotionsGem = promotionsGem;
            CachePvp.serverX2PurchaseGold = serverX2PurchaseGold;
            CachePvp.serverX2PurchaseGem = serverX2PurchaseGem;
            CachePvp.FirstTabIndex = firstTabIndex;

            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.ShowGlowButtonIapGold();
                PanelCoinGem.Instance.ShowGlowButtonIapGem();
            }
        }
    }
}