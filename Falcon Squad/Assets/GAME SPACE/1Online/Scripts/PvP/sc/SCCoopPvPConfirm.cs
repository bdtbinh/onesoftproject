﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
using UnityEngine.SceneManagement;

namespace Mp.Pvp
{
    public class SCCoopPvPConfirm : SCMessage
    {
        public const int OFFLINE = 3;
        public const int BUSY = 4;
        public const int PVP_MAINTAIN = 5;
        public const int TIMEOUT = 6;
        public const int DENIDED = 7;

        public Player player;
        public override string GetEvent()
        {
            return Events.COOP_PVP_CONFIRM;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                //MinhCacheGame.SetBettingAmountPvP(0);
                CachePvp.typePVP = CSFindOpponent.TYPE_2V2;
                PopupManager.Instance.goTo2v2 = true;
                if (PvPUIManager.Instance == null)
                {
                    SceneManager.LoadScene("PVPMain");
                }
                else
                {
                    PvPUIManager.Instance.popupMainPvP.gameObject.SetActive(false);
                    PvPUIManager.Instance.popupFindPvP.gameObject.SetActive(true);
                }
            }
            else if (status == OFFLINE || status == BUSY || status == TIMEOUT)
            {
                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.msg_invite_user_busy);
                PopupManager.Instance.HideInvitePopup();
            }
        }
    }
}