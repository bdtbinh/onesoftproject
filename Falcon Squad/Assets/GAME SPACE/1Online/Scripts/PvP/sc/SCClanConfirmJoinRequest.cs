﻿using OSNet;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCClanConfirmJoinRequest : SCMessage
    {
        public const int ACCEPT = 0;
        public const int REJECT = 1;

        public int action;
        public string code;

        public override string GetEvent()
        {
            return Events.CLAN_CONFIRM_JOIN_REQUEST;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                MessageDispatcher.SendMessage(this, action, EventName.Clan.ConfirmJoinRequest.ToString(), code, 0);
            }
            else
            {
                if (string.IsNullOrEmpty(code))
                {
                    code = "";
                }
                MessageDispatcher.SendMessage(this, action, EventName.Clan.ConfirmJoinRequestError.ToString(), code, 0);
            }
        }
    }
}