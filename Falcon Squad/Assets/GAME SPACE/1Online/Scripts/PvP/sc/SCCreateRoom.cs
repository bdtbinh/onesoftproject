using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCCreateRoom : SCMessage
    {
        public const int JOINED_ROOM = 2;
        public const int EXISTED_ROOM = 3;
        public override string GetEvent()
        {
            return Events.CREATE_ROOM;
        }
		public override void OnData()
		{
			UnityEngine.Debug.Log("SCCreateRoom");
		}
    }
}