using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCPlayerReady : SCMessage
    {
        public override string GetEvent()
        {
            return Events.PLAYER_READY;
        }
		public override void OnData()
		{
			UnityEngine.Debug.Log("SCPlayerReady");
		}
    }
}