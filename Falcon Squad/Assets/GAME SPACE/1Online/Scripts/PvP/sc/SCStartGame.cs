using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCStartGame : SCMessage
    {
        public string roomId;
        public int playTime;
        public RoomInfo info;

        public override string GetEvent()
        {
            return Events.START_GAME;
        }

        public override void OnData()
        {
            UnityEngine.Debug.Log("SCStartGame");
            CachePvp.SetGold(CachePvp.GetGold() - MinhCacheGame.GetBettingAmountPvP(), FirebaseLogSpaceWar.PvP_why, FirebaseLogSpaceWar.Unknown_why);
            FirebaseLogSpaceWar.LogGoldOut(MinhCacheGame.GetBettingAmountPvP(), FirebaseLogSpaceWar.PvP_why, "");
            CachePvp.PlayTime = playTime;
            CachePvp.RoomInfo = info;
            MessageDispatcher.SendMessage(EventName.PVP.StartGamePVP.ToString());
        }
    }
}