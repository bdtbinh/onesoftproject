﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCAppCenterFriends : SCMessage
    {
        public override string GetEvent()
        {
            return Events.GAMECENTER_FRIENDS;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCAppCenterFriends");
        }
    }
}