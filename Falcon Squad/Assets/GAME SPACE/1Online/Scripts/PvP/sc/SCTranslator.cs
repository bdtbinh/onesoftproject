﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCTranslator : SCMessage
    {
        public string langFrom;
        public string langTo;
        public string text;
        public override string GetEvent()
        {
            return Events.TRANSLATE;
        }
        public override void OnData()
        {
            
        }
    }
}