﻿using com.ootii.Messages;
using OSNet;

namespace Mp.Pvp
{
    public class SCGetRoyaltyPackInfo : SCMessage
    {
        public bool auto;
        public RoyaltyPackData data;
        public int type;

        public override string GetEvent()
        {
            return Events.GET_ROYALTY_PACK_INFO;
        }

        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.RoyaltyPack.GetRoyaltyPackInfo.ToString(), this, 0);
        }
    }
}
