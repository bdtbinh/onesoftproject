using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCPlayerDied : SCMessage
    {
        public string code;
        public override string GetEvent()
        {
            return Events.PLAYER_DIED;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCPlayerDied");
            if (InGamePvP.Instance != null)
            {
                InGamePvP.Instance.PlayerDied(code);
            }
        }
    }
}