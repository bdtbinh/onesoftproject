﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mp.Pvp;
using OSNet;
using UnityEngine.SceneManagement;
using com.ootii.Messages;

public class SCMail : SCMessage
{
    public List<FalconMail> listMailbox;
    public int type; //0: client tu goi, 1: server forced

    public override string GetEvent()
    {
        return Events.FALCON_MAIL;
    }

    public override void OnData()
    {
        DataJsonMail djm;
        if (!string.IsNullOrEmpty(CachePvp.FalconMailJson))
        {
            djm = DataJsonMail.ToJsonObject(CachePvp.FalconMailJson);
        }
        else
        {
            djm = new DataJsonMail();
            djm.listFalconMail = new List<FalconMail>();
        }
        for (int i = listMailbox.Count - 1; i >= 0; i--)
        {
            if (listMailbox[i].player != null && listMailbox[i].player.playerClan != null)
            {
                listMailbox[i].clanId = listMailbox[i].player.playerClan.id;
            }
            djm.listFalconMail.Insert(0, listMailbox[i]);
        }
        CachePvp.FalconMailJson = djm.ToJson();
        if (type == 1)
        {
            CachePvp.NewMailNumberForce = listMailbox.Count;
        }
        else
        {
            CachePvp.NewMailNumber = 0;
        }
        MessageDispatcher.SendMessage(this, EventName.LoadMailBox.LoadedMailBox.ToString(), djm, 0);
    }
}

[System.Serializable]
public class FalconMail
{
    private const string COIN_GIFT_SPRITE_NAME = "icon_coin";
    private const string GEM_GIFT_SPRITE_NAME = "icon_daily_quest_gem";
    private const string ENERGY_GIFT_SPRITE_NAME = "icon_daily_quest_enegy";
    private const string POWER_UP_GIFT_SPRITE_NAME = "icon_powerup";
    private const string EMP_GIFT_SPRITE_NAME = "icon_dailyquest_enegy";
    private const string LIFE_GIFT_SPRITE_NAME = "icon_life";
    //list plane
    private const string PLANE1_GIFT_SPRITE_NAME = "hangar_main1";
    private const string PLANE2_GIFT_SPRITE_NAME = "hangar_main3";
    private const string PLANE3_GIFT_SPRITE_NAME = "hangar_main2";
    private const string PLANE4_GIFT_SPRITE_NAME = "hangar_main_xmas_2";
    private const string PLANE5_GIFT_SPRITE_NAME = "hangar_main_xmas_1";
    private const string PLANE6_GIFT_SPRITE_NAME = "hangar_main5";
    private const string PLANE7_GIFT_SPRITE_NAME = "hangar_main4";
    private const string PLANE8_GIFT_SPRITE_NAME = "hangar_main6";
    private const string PLANE9_GIFT_SPRITE_NAME = "hangar_main_xmas_1";
    private const string PLANE10_GIFT_SPRITE_NAME = "hangar_main7";
    //list drone
    private const string DRONE1_GIFT_SPRITE_NAME = "hangar_drone_1";
    private const string DRONE2_GIFT_SPRITE_NAME = "hangar_drone_2";
    private const string DRONE3_GIFT_SPRITE_NAME = "hangar_drone_3";
    private const string DRONE4_GIFT_SPRITE_NAME = "hangar_drone_4";
    private const string DRONE5_GIFT_SPRITE_NAME = "hangar_drone_5";
    private const string DRONE6_GIFT_SPRITE_NAME = "hangar_drone_6";
    //list wing
    private const string WING1_GIFT_SPRITE_NAME = "hangar_wing_1";
    private const string WING2_GIFT_SPRITE_NAME = "hangar_wing_2";
    //list super weapon
    private const string SUPER_WEAPON_1_GIFT_SPRITE_NAME = "hangar_2ndWp_B";
    private const string SUPER_WEAPON_2_GIFT_SPRITE_NAME = "hangar_2ndWp_S";
    private const string SUPER_WEAPON_3_GIFT_SPRITE_NAME = "hangar_2ndWp_R";
    private const string SUPER_WEAPON_4_GIFT_SPRITE_NAME = "hangar_2ndWp_E";
    //list card
    private const string CARD_A0_SPRITE_NAME = "card_A0";
    private const string CARD_A1_SPRITE_NAME = "card_A1";
    private const string CARD_A2_SPRITE_NAME = "card_A2";
    private const string CARD_A3_SPRITE_NAME = "card_A3";
    private const string CARD_A4_SPRITE_NAME = "card_A4";
    private const string CARD_A5_SPRITE_NAME = "card_A5";
    private const string CARD_A6_SPRITE_NAME = "card_A6";
    private const string CARD_A7_SPRITE_NAME = "card_A7";
    private const string CARD_A8_SPRITE_NAME = "card_A8";

    private const string CARD_D0_SPRITE_NAME = "card_D0";
    private const string CARD_D1_SPRITE_NAME = "card_D1";
    private const string CARD_D2_SPRITE_NAME = "card_D2";
    private const string CARD_D3_SPRITE_NAME = "card_D3";
    private const string CARD_D4_SPRITE_NAME = "card_D4";
    private const string CARD_D5_SPRITE_NAME = "card_D5";
    private const string CARD_D6_SPRITE_NAME = "card_D6";

    private const string CARD_W0_SPRITE_NAME = "card_W0";
    private const string CARD_W1_SPRITE_NAME = "card_W1";
    private const string CARD_W2_SPRITE_NAME = "card_W2";
    private const string CARD_W3_SPRITE_NAME = "card_W3";

    private const string BOX_CARD_SPRITE_NAME = "card_R";

    private const string ITEM_VIP_POINT_SPRITE_NAME = "item_VIPpoint";
    private const string ITEM_TICKET_SPRITE_NAME = "icon_ticket";
    private const string ITEM_PREMIUM_PACK_SPRITE_NAME = "item_premium_pack";
    private const string ITEM_INFINITY_PACK_SPRITE_NAME = "item_infinity_pack";

    public string id;
    public long eventTime;
    public Player player; // == null if System
    public long clanId;
    public string message;
    public string url;
    public int action;
    public int hasClaimed;
    public int hasConfirm;
    [SerializeField]
    public List<ItemVolumeFalconMail> items;

    public const int ACTION_NOTHING = -1;
    public const int ACTION_GO_URL = 1;
    public const int ACTION_GO_SHOP = 2;
    public const int ACTION_CLAIM = 3;
    public const int ACTION_CLAIMED = 4;
    public const int ACTION_CLAN_INFO = 5;
    public const int ACTION_CLAN_HOME = 6;

    public const int ITEM_GOLD = 1;
    public const int ITEM_GEM = 2;
    public const int ITEM_ENERGY = 31;

    public const int ITEM_POWER_UP = 3;
    public const int ITEM_ACTIVESKILL = 4; // ITEM SỬ DỤNG ACTIVE SKILL
    public const int ITEM_LIFE = 5;
    public const int ITEM_PLANE = 6; // list may bay
    public const int ITEM_WINGMAN = 7; // list wing man
    public const int ITEM_SECONDARY_WEAPON = 8; // list secondary weapon
    public const int ITEM_CARD_PLANE_GENERAL = 9;
    public const int ITEM_CARD_PLANE_1 = 10;
    public const int ITEM_CARD_PLANE_2 = 11;
    public const int ITEM_CARD_PLANE_3 = 12;
    public const int ITEM_CARD_PLANE_4 = 13;
    public const int ITEM_CARD_PLANE_5 = 14;
    public const int ITEM_CARD_PLANE_6 = 24;
    public const int ITEM_CARD_PLANE_7 = 25;
    public const int ITEM_CARD_PLANE_8 = 38;
    public const int ITEM_CARD_WINGMAN_GENERAL = 15;
    public const int ITEM_CARD_WINGMAN_1 = 16;
    public const int ITEM_CARD_WINGMAN_2 = 17;
    public const int ITEM_CARD_WINGMAN_3 = 18;
    public const int ITEM_CARD_WINGMAN_4 = 19;
    public const int ITEM_CARD_WINGMAN_5 = 20;
    public const int ITEM_CARD_WINGMAN_6 = 32;

    public const int ITEM_BOX_CARD1 = 33;
    public const int ITEM_BOX_CARD2 = 34;
    public const int ITEM_BOX_CARD3 = 35;
    public const int ITEM_BOX_CARD4 = 36;

    public const int ITEM_WING = 27; //list wing
    public const int ITEM_CARD_WING_GENERAL = 28;
    public const int ITEM_CARD_WING_1 = 29;
    public const int ITEM_CARD_WING_2 = 30;
    public const int ITEM_CARD_WING_3 = 37;

    public const int ITEM_VIP_POINT = 21;
    public const int ITEM_LUCKY_WHEEL_TICKET = 22;
    public const int ITEM_PREMIUM_PACK = 23;
    public const int ITEM_INFINITY_PACK = 26;
    //thêm item thì check thêm trong: (prefab + script)
    //-PopupShowProfile
    //-PopupShowProfileOnTop
    //-PopupSelectCardType
    //-PopupSelectAirCraftTournament
    //-PopupSelectWingmanTournament
    //-PopupSelectWingTournament
    public static string GetSpriteName(int key, int value = 0)
    {
        string sprite = "";
        switch (key)
        {
            case ITEM_GOLD:
                sprite = COIN_GIFT_SPRITE_NAME;
                break;
            case ITEM_GEM:
                sprite = GEM_GIFT_SPRITE_NAME;
                break;
            case ITEM_ENERGY:
                sprite = ENERGY_GIFT_SPRITE_NAME;
                break;
            case ITEM_POWER_UP:
                sprite = POWER_UP_GIFT_SPRITE_NAME;
                break;
            case ITEM_ACTIVESKILL:
                sprite = EMP_GIFT_SPRITE_NAME;
                break;
            case ITEM_LIFE:
                sprite = LIFE_GIFT_SPRITE_NAME;
                break;
            case ITEM_PLANE:
                switch (value)
                {
                    case 0:
                        sprite = PLANE1_GIFT_SPRITE_NAME;
                        break;
                    case 1:
                        sprite = PLANE2_GIFT_SPRITE_NAME;
                        break;
                    case 2:
                        sprite = PLANE3_GIFT_SPRITE_NAME;
                        break;
                    case 3:
                        sprite = PLANE4_GIFT_SPRITE_NAME;
                        break;
                    case 4:
                        sprite = PLANE5_GIFT_SPRITE_NAME;
                        break;
                    case 5:
                        sprite = PLANE6_GIFT_SPRITE_NAME;
                        break;
                    case 6:
                        sprite = PLANE7_GIFT_SPRITE_NAME;
                        break;
                    case 7:
                        sprite = PLANE8_GIFT_SPRITE_NAME;
                        break;
                    case 8:
                        sprite = PLANE9_GIFT_SPRITE_NAME;
                        break;
                    case 9:
                        sprite = PLANE10_GIFT_SPRITE_NAME;
                        break;
                    default:
                        break;
                }
                break;
            case ITEM_WINGMAN:
                switch (value)
                {
                    case 0:
                        sprite = DRONE1_GIFT_SPRITE_NAME;
                        break;
                    case 1:
                        sprite = DRONE2_GIFT_SPRITE_NAME;
                        break;
                    case 2:
                        sprite = DRONE3_GIFT_SPRITE_NAME;
                        break;
                    case 3:
                        sprite = DRONE4_GIFT_SPRITE_NAME;
                        break;
                    case 4:
                        sprite = DRONE5_GIFT_SPRITE_NAME;
                        break;
                    case 5:
                        sprite = DRONE6_GIFT_SPRITE_NAME;
                        break;
                    default:
                        break;
                }
                break;
            case ITEM_WING:
                switch (value)
                {
                    case 0:
                        sprite = WING1_GIFT_SPRITE_NAME;
                        break;
                    case 1:
                        sprite = WING2_GIFT_SPRITE_NAME;
                        break;
                    default:
                        break;
                }
                break;
            case ITEM_SECONDARY_WEAPON:
                switch (value)
                {
                    case 0:
                        sprite = SUPER_WEAPON_1_GIFT_SPRITE_NAME;
                        break;
                    case 1:
                        sprite = SUPER_WEAPON_2_GIFT_SPRITE_NAME;
                        break;
                    case 2:
                        sprite = SUPER_WEAPON_3_GIFT_SPRITE_NAME;
                        break;
                    case 3:
                        sprite = SUPER_WEAPON_4_GIFT_SPRITE_NAME;
                        break;
                    default:
                        break;
                }
                break;
            case ITEM_CARD_PLANE_GENERAL:
                sprite = CARD_A0_SPRITE_NAME;
                break;
            case ITEM_CARD_PLANE_1:
                sprite = CARD_A1_SPRITE_NAME;
                break;
            case ITEM_CARD_PLANE_2:
                sprite = CARD_A2_SPRITE_NAME;
                break;
            case ITEM_CARD_PLANE_3:
                sprite = CARD_A3_SPRITE_NAME;
                break;
            case ITEM_CARD_PLANE_4:
                sprite = CARD_A4_SPRITE_NAME;
                break;
            case ITEM_CARD_PLANE_5:
                sprite = CARD_A5_SPRITE_NAME;
                break;
            case ITEM_CARD_PLANE_6:
                sprite = CARD_A6_SPRITE_NAME;
                break;
            case ITEM_CARD_PLANE_7:
                sprite = CARD_A7_SPRITE_NAME;
                break;
            case ITEM_CARD_PLANE_8:
                sprite = CARD_A8_SPRITE_NAME;
                break;
            case ITEM_CARD_WINGMAN_GENERAL:
                sprite = CARD_D0_SPRITE_NAME;
                break;
            case ITEM_CARD_WINGMAN_1:
                sprite = CARD_D1_SPRITE_NAME;
                break;
            case ITEM_CARD_WINGMAN_2:
                sprite = CARD_D2_SPRITE_NAME;
                break;
            case ITEM_CARD_WINGMAN_3:
                sprite = CARD_D3_SPRITE_NAME;
                break;
            case ITEM_CARD_WINGMAN_4:
                sprite = CARD_D4_SPRITE_NAME;
                break;
            case ITEM_CARD_WINGMAN_5:
                sprite = CARD_D5_SPRITE_NAME;
                break;
            case ITEM_CARD_WINGMAN_6:
                sprite = CARD_D6_SPRITE_NAME;
                break;
            case ITEM_CARD_WING_GENERAL:
                sprite = CARD_W0_SPRITE_NAME;
                break;
            case ITEM_CARD_WING_1:
                sprite = CARD_W1_SPRITE_NAME;
                break;
            case ITEM_CARD_WING_2:
                sprite = CARD_W2_SPRITE_NAME;
                break;
            case ITEM_CARD_WING_3:
                sprite = CARD_W3_SPRITE_NAME;
                break;
            case ITEM_BOX_CARD1:
            case ITEM_BOX_CARD2:
            case ITEM_BOX_CARD3:
            case ITEM_BOX_CARD4:
                sprite = BOX_CARD_SPRITE_NAME;
                break;
            case ITEM_VIP_POINT:
                sprite = ITEM_VIP_POINT_SPRITE_NAME;
                break;
            case ITEM_LUCKY_WHEEL_TICKET:
                sprite = ITEM_TICKET_SPRITE_NAME;
                break;
            case ITEM_PREMIUM_PACK:
                sprite = ITEM_PREMIUM_PACK_SPRITE_NAME;
                break;
            case ITEM_INFINITY_PACK:
                sprite = ITEM_INFINITY_PACK_SPRITE_NAME;
                break;
            default:
                sprite = "";
                break;
        }
        return sprite;
    }
}