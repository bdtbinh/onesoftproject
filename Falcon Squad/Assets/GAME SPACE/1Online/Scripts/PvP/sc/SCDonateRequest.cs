﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCDonateRequest : SCMessage
    {
        public static int NOT_JOINED_CLAN = 2;
        public static int DUPLICATED_REQUEST = 3;
        public int planeId;

        public override string GetEvent()
        {
            return Events.CLAN_DONATE_REQUEST;
        }
        public override void OnData()
        {
            if (status != SUCCESS)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(message, false, 1.5f);
            }
        }
    }
}