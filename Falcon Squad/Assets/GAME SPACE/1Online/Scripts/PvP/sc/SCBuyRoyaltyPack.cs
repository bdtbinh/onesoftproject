﻿using com.ootii.Messages;
using OSNet;

namespace Mp.Pvp
{
    public class SCBuyRoyaltyPack : SCMessage
    {
        public RoyaltyPackData data;
        public override string GetEvent()
        {
            return Events.BUY_ROYALTY_PACK;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.RoyaltyPack.BuyRoyaltyPack.ToString(), this, 0);
        }
    }
}

public class RoyaltyPackData
{
    // trạng thái đã nhận 70 gem trong ngày chưa
    public bool gemDaily;

    // trạng thái đã mở Chest trong ngày chưa
    public bool goldenChest;

    // thời gian còn lại của gói (đơn vị giây)
    public long timeRemain;

    // trạng thái của gói
    public const int ACTIVE = 0;
    public const int EXPIRE = 1;
    public int status;
}
