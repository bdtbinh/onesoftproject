﻿using System;
using System.Collections.Generic;
using OSNet;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCVersionConfig : SCMessage
    {
        public List<VersionConfig> configs = new List<VersionConfig>();
        private float tournament = 0;
        private float mail = 0;
        private float pvp = 0;
        private float extra = 0;
        private float start = 0;
        private float rank = 0;
        private float clan = 0;
        private float randomCardShop = 0;
        private float pvp2v2 = 0;

        public bool canTournament = true;
        public bool isMaintainTournament = false;
        public bool canMail = true;
        public bool isMaintainMail = false;
        public bool canPVP = true;
        public bool isMaintainPVP = false;
        public bool canPVP2v2 = true;
        public bool isMaintainPVP2v2 = false;
        public bool canExtra = true;
        public bool isMaintainExtra = false;
        public bool canStart = true;
        public bool isMaintainStart = false;
        public bool canRank = true;
        public bool isMaintainRank = false;
        public bool canClan = true;
        public bool isMaintainClan = false;
        public bool canRandomCardShop = true;
        public bool isMaintainRandomCardShop = false;

        public override string GetEvent()
        {
            return Events.VERSION_CONFIG;
        }
        public override void OnData()
        {
            VersionConfig vc;
            string os = "";
#if UNITY_ANDROID
            os = "android";
#elif UNITY_IPHONE
            os = "ios";
#endif
            string bundle = Application.identifier;
            for (int i = 0; i < configs.Count; i++)
            {
                vc = configs[i];
                if (vc.os.Equals(os) && vc.bundle.Equals(bundle))
                {
                    switch (vc.at)
                    {
                        case "tournament":
                            float.TryParse(vc.version, out tournament);
                            if (tournament > float.Parse(Application.version))
                            {
                                canTournament = false;
                            }
                            else
                            {
                                canTournament = true;
                            }
                            if (vc.type == 0)
                            {
                                isMaintainTournament = false;
                            }
                            else
                            {
                                isMaintainTournament = true;
                            }
                            break;
                        case "mail":
                            float.TryParse(vc.version, out mail);
                            if (mail > float.Parse(Application.version))
                            {
                                canMail = false;
                            }
                            else
                            {
                                canMail = true;
                            }
                            if (vc.type == 0)
                            {
                                isMaintainMail = false;
                            }
                            else
                            {
                                isMaintainMail = true;
                            }
                            break;
                        case "pvp":
                            float.TryParse(vc.version, out pvp);
                            if (pvp > float.Parse(Application.version))
                            {
                                canPVP = false;
                            }
                            else
                            {
                                canPVP = true;
                            }
                            if (vc.type == 0)
                            {
                                isMaintainPVP = false;
                            }
                            else
                            {
                                isMaintainPVP = true;
                            }
                            break;
                        case "pvp2v2":
                            float.TryParse(vc.version, out pvp2v2);
                            if (pvp2v2 > float.Parse(Application.version))
                            {
                                canPVP2v2 = false;
                            }
                            else
                            {
                                canPVP2v2 = true;
                            }
                            if (vc.type == 0)
                            {
                                isMaintainPVP2v2 = false;
                            }
                            else
                            {
                                isMaintainPVP2v2 = true;
                            }
                            break;
                        case "extra":
                            float.TryParse(vc.version, out extra);
                            if (extra > float.Parse(Application.version))
                            {
                                canExtra = false;
                            }
                            else
                            {
                                canExtra = true;
                            }
                            if (vc.type == 0)
                            {
                                isMaintainExtra = false;
                            }
                            else
                            {
                                isMaintainExtra = true;
                            }
                            break;
                        case "start":
                            float.TryParse(vc.version, out start);
                            if (start > float.Parse(Application.version))
                            {
                                canStart = false;
                            }
                            else
                            {
                                canStart = true;
                            }
                            if (vc.type == 0)
                            {
                                isMaintainStart = false;
                            }
                            else
                            {
                                isMaintainStart = true;
                            }
                            break;
                        case "rank":
                            float.TryParse(vc.version, out rank);
                            if (rank > float.Parse(Application.version))
                            {
                                canRank = false;
                            }
                            else
                            {
                                canRank = true;
                            }
                            if (vc.type == 0)
                            {
                                isMaintainRank = false;
                            }
                            else
                            {
                                isMaintainRank = true;
                            }
                            break;
                        case "clan":
                            float.TryParse(vc.version, out clan);
                            if (clan > float.Parse(Application.version))
                            {
                                canClan = false;
                            }
                            else
                            {
                                canClan = true;
                            }
                            if (vc.type == 0)
                            {
                                isMaintainClan = false;
                            }
                            else
                            {
                                isMaintainClan = true;
                            }
                            break;
                        case "randomCardShop":
                            float.TryParse(vc.version, out randomCardShop);
                            if (randomCardShop > float.Parse(Application.version))
                            {
                                canRandomCardShop = false;
                            }
                            else
                            {
                                canRandomCardShop = true;
                            }
                            if (vc.type == 0)
                            {
                                isMaintainRandomCardShop = false;
                            }
                            else
                            {
                                isMaintainRandomCardShop = true;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            CachePvp.sCVersionConfig = this;
        }
    }
}

public class VersionConfig
{
    public string os;
    public string bundle;
    public string at;
    public string version;
    public int type; //0:update, 1: maintain
}