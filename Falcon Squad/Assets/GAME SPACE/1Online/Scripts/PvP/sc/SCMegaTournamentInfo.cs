﻿using System;
using System.Collections.Generic;
using Facebook.Unity;
using OSNet;
using com.ootii.Messages;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCMegaTournamentInfo : SCMessage
    {
        public int NOT_LOGGED_IN = 2;
        public MegaTournamentInfo info;
        public Ticket ticketBought;
        public List<Ticket> ticketSales;

        public List<MegaTournamentReward> configRewards;
        //Phần thưởng cho top 10
        public Item top10Reward;
        public override string GetEvent()
        {
            return Events.MEGA_TOURNAMENT_INFO;
        }

        public override void OnData()
        {
            CachePvp.megaTournamentInfo = this;
            if (ticketBought != null)
            {
                PanelCoinGem.Instance.AddGemTop(-ticketBought.gem, "buy_ticket", "buy_ticket");
            }
            MessageDispatcher.SendMessage(this, EventName.Tournament.MegaTournamentInfo.ToString(), this, 0);
        }
    }
}

public class MegaTournamentInfo
{

    public long id;

    //ID của người chơi
    public string code;

    // số lượng tiket còn lại
    public int ticket;

    //Số ticket tối đa
    public int maxTicket;

    // tổng số trận thắng đạt được trong tour
    public int win;

    // số trận thua liên tiếp
    public int lose;
    //Đã unlock phần thưởng bonus hay chưa
    public bool unlocked;
    //Số Gem dùng để unlock bonus
    public int unlockBonusGem;

    // win need for next reward trong tournament
    public int nextRewardIn;

    // Chu kì hồi phục 1 ticket (phút)
    public long gainDuration;

    // Thời điểm còn lại (giấy) để hồi phục ticket tiếp theo
    // giá trị này = 0 nếu số lượng ticket đã đạt max
    public long refillRemain;

    //Thời gian còn lại (giây) để bắt đâu/kết thúc tournament
    public long startTimeRemain;
    public long endTimeRemain;

    //Thành tích và phần thưởng đạt được
    public List<MegaTournamentReward> rewards;
}

public class Item
{
    public int key;
    public string item;
    public int value;
}

public class MegaTournamentReward
{
    public int win;//số trận thắng
    public Item free;
    public Item bonus;

    public bool rewarded;
    //Đã nhận thưởng của bonus hay chưa
    public bool bonusRewarded;
}

public class Ticket
{
    public int ticket;
    public int gem;
}