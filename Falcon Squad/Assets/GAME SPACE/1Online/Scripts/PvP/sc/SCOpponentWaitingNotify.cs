using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCOpponentWaitingNotify : SCMessage
    {
        public const int FLAG_ON = 0;
        public const int FLAG_OFF = 1;
        public string token;
        public Player player;
        public int flag;
        public int gold;

        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;
        public int type;

        public override string GetEvent()
        {
            return Events.OPPONENT_FINDING_NOTIFY;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCOpponentWaitingNotify");
            if (status == SUCCESS)
            {
                if (type == TYPE_PVP)
                {
                    if ((flag == FLAG_ON && CachePvp.GetGold() >= gold) || flag == FLAG_OFF)
                    {
                        CachePvp.goldBetFromInvitation = gold;
                        PopupManager.Instance.ShowInviteButton(token, player, flag, gold);
                    }
                }
                else if (type == TYPE_MEGA)
                {
                    PopupManager.Instance.ShowInviteButtonTournament(token, player, flag, gold);
                }
            }
        }
    }
}