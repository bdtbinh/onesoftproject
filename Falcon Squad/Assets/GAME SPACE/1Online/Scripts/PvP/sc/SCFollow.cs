﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCFollow : SCMessage
    {
        public const int ERROR_FOLLOW_MY_SEFL = 20;
        public int type;
        public string code;
        public override string GetEvent()
        {
            return Events.FOLLOW;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.LoadProfile.FollowFriend.ToString(), this, 0);
        }
    }
}