using System;
using System.Collections.Generic;
using Facebook.Unity;
using OSNet;
using com.ootii.Messages;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCValidateFacebookId : SCMessage
    {
        public Player player;
        public override string GetEvent()
        {
            return Events.VALIDATE_FACEBOOK_ID;
        }
        public override void OnData()
        {
            if (player == null)
            {
                new CSFacebookConnect(SocialManager.Instance.FBGetUserToken(), CachePvp.facebookUser.id, CachePvp.facebookUser.name, 1).Send();
            }
            else
            {
                if (player.debug)
                {
                    Debug.unityLogger.logEnabled = true;
                    SRDebug.Init();
                }
                //hien popup dong bo
                if (SceneManager.GetActiveScene().name == "Home")
                {

                    if (string.IsNullOrEmpty(CachePvp.FacebookId))
                    {
                        PopupManager.Instance.ShowSyncPopup(player);
                    }
                    else
                    {
                        new CSFacebookConnect(SocialManager.Instance.FBGetUserToken(), CachePvp.facebookUser.id, CachePvp.facebookUser.name, 1).Send();
                    }
                }
            }
        }
    }
}