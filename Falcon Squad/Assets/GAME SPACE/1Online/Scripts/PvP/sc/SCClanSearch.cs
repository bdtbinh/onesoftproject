﻿using com.ootii.Messages;
using OSNet;
using System.Collections.Generic;

namespace Mp.Pvp
{
    public class SCClanSearch : SCMessage
    {
        public List<ClanInfo> clans;
        public override string GetEvent()
        {
            return Events.CLAN_SEARCH;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                MessageDispatcher.SendMessage(this, EventName.Clan.LoadedSearchClan.ToString(), clans, 0);
            }
        }
    }
}