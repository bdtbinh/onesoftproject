using System;
using System.Collections.Generic;
using OSNet;
using UnityEngine.SceneManagement;

namespace Mp.Pvp
{
    public class SCFindOpponent : SCMessage
    {
        public const int WAITING = 2;
        public const int TIMEOUT = 3;
        public const int NOT_ENOUGH_MONEY = 4;
        public List<Player> players;
        public int timeout;
        public int winEloPoint;
        public int drawEloPoint;
        public int loseEloPoint;
        public int bet;

        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;
        public int type;

        public override string GetEvent()
        {
            return Events.FIND_OPPONENT;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCFindOpponent: " + status + " " + message);
            if (status == SUCCESS)
            {
                CachePvp.eloWin = winEloPoint;
                CachePvp.eloDraw = drawEloPoint;
                CachePvp.eloLose = loseEloPoint;
                CachePvp.typePVP = type;
            }
            if (PvPUIManager.Instance != null)
            {
                if (PvPUIManager.Instance.popupFindPvP.gameObject.activeInHierarchy)
                {
                    PvPUIManager.Instance.popupFindPvP.SCFindOpponentCallback((FindState)status, timeout, message);
                }
                else
                {
                    UnityEngine.Debug.Log("SCFindOpponent: " + status + " " + message);
                    if (status == SUCCESS)
                    {
                        for (int i = 0; i < players.Count; i++)
                        {
                            if (!players[i].code.Equals(CachePvp.Code))
                            {
                                PopupManager.Instance.cachedPlayer = players[i];
                                break;
                            }
                        }
                        PvPUIManager.Instance.popupMainPvP.SCFindOpponentCallback((FindState)status, timeout);
                    }
                }
            }
            else
            {
                if (status == SUCCESS)
                {
                    for (int i = 0; i < players.Count; i++)
                    {
                        if (!players[i].code.Equals(CachePvp.Code))
                        {
                            PopupManager.Instance.cachedPlayer = players[i];
                            break;
                        }
                    }
                    //PopupManager.Instance.goToPVP = true;
                    //SceneManager.LoadScene("PVPMain");
                }
            }
            if (bet == 0)
            {
                MinhCacheGame.SetBettingAmountPvP(0);
            }
        }
    }
}