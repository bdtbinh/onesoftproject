﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OSNet;
using Mp.Pvp;

public class SCLevelModify : SCMessage
{
    public double coeff;
    public int level;

    public override string GetEvent()
    {
        return Events.LEVEL_MODIFY;
    }

    public override void OnData()
    {

        //Debug.LogError("OnDataSCLevelModify--level:" + level + "---coeff:" + (float)coeff);
        CacheGame.SetValueLevelModifiServer(level, (float)coeff);

        //if (!PvpUtil.dicLevelToCoeff.ContainsKey(level))
        //{
        //    PvpUtil.dicLevelToCoeff.Add(level, coeff);
        //    CacheGame.SetValueLevelModifiServer(level,(float)coeff);
        //}
    }
}
