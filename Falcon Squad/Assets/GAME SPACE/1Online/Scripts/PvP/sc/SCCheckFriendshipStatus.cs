﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCFriendshipStatus : SCMessage
    {
        public FriendshipStatus friendshipStatus;
        public override string GetEvent()
        {
            return Events.FRIENDSHIP_STATUS;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.LoadProfile.LoadFriendshipStatus.ToString(), this, 0);
        }
    }
}

public class FriendshipStatus
{

    public string code;
    public bool isFriend;

    //đang gửi request kết bạn
    public bool isRequesting;

    //đang follow
    public bool isFollowing;
}