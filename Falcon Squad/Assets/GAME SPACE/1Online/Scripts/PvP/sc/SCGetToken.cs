using System;
using System.Collections.Generic;
using com.ootii.Messages;
using Facebook.Unity;
using OSNet;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mp.Pvp
{
    public class SCGetToken : SCMessage
    {
        public string token;
        public string code;
        public override string GetEvent()
        {
            return Events.GET_TOKEN;
        }
        public override void OnData()
        {
            CachePvp.Token = token;
            CachePvp.Code = code;
            if (status == SCMessage.SUCCESS)
            {
                CachePvp.FacebookId = "";
                CachePvp.facebookUser = null;
                CachePvp.Name = "";
                if (FB.IsLoggedIn)
                {
                    FB.LogOut();
                }
                CachePvp.isConnectServer = true;
                ConnectManager.Instance.HandShake();
                CachePvp.MyCode = int.Parse(CachePvp.Code);
                PvpUtil.SendUpdatePlayer("SCValidateToken");
                new CSClientInfo().Send();
                if (SceneManager.GetActiveScene().name.Equals("Home"))
                {
                    if (!CachePvp.isHomeOK)
                    {
                        CachePvp.isHomeOK = true;
                        new CSHomeOK(PvpUtil.PVP_VERSION).Send();
                    }
                    if (!CachePvp.isSendLoadingTime)
                    {
                        CachePvp.isSendLoadingTime = true;
                        long time = CachePvp.timeSinceStartUp;
                        string deviceId = CachePvp.deviceId;
                        string deviceName = SystemInfo.deviceModel;
                        string code = CachePvp.Code;
                        string platform = "";
#if UNITY_IPHONE
                platform = "ios";
#elif UNITY_ANDROID
                        platform = "android";
#endif
                        Debug.Log("send CSLoadingTime");
                        new CSLoadingTime(time, deviceId, deviceName, platform, code, Application.identifier).Send();
                        Debug.Log("after send CSLoadingTime");
                    }
                }
            }
            MessageDispatcher.SendMessage(this, EventName.LoadProfile.LoadedProfile.ToString(), 0, 0);
            if (string.IsNullOrEmpty(CachePvp.deviceId))
            {
#if UNITY_EDITOR
                if (SystemInfo.deviceModel.Equals("MS-7971 (MSI)"))
                {
                    CachePvp.deviceId = "editornamnx";
                    new CSUpdateDeviceId(CachePvp.deviceId).Send();
                }
                else if (SystemInfo.deviceModel.Equals("MS-7A70 (MSI)"))
                {
                    CachePvp.deviceId = "editorminhddv2";
                    new CSUpdateDeviceId(CachePvp.deviceId).Send();
                }
#else
                bool check = Application.RequestAdvertisingIdentifierAsync((string advertisingId, bool trackingEnabled, string error) =>
                {
                    CachePvp.deviceId = advertisingId;
                    new CSUpdateDeviceId(advertisingId).Send();
                });
#endif
            }
            else
            {
                new CSUpdateDeviceId(CachePvp.deviceId).Send();
            }
        }
    }
}