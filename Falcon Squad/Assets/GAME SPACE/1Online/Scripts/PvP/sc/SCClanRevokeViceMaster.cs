﻿using OSNet;
using com.ootii.Messages;
namespace Mp.Pvp
{
    public class SCClanRevokeViceMaster : SCMessage
    {
        public string member;
        public override string GetEvent()
        {
            return Events.CLAN_REVOKE_VICE_MASTER;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.RevokeViceMaster.ToString(), member, 0);
        }
    }
}