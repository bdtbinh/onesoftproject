using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCUpdatePlayer : SCMessage
    {
        public Player player;

        public override string GetEvent()
        {
            return Events.UPDATE_PLAYER;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCUpdatePlayer: " + message);
            CachePvp.Medal = player.medal;
            CachePvp.Win = player.win;
            CachePvp.Lose = player.lose;
            CachePvp.Country = player.country;
            if (player.code.Length >= 3)
            {
                CachePvp.Code = player.code;
            }
            CachePvp.PlayerLevel = player.playerLevel;
            if (player.pvp2vs2PlayerLevel != null)
            {
                CachePvp.PlayerLevel2vs2 = player.pvp2vs2PlayerLevel;
            }
            CachePvp.FriendshipStats = player.friendshipStats;
            if (player.friendshipStats != null)
            {
                CachePvp.CountFriendsMinh = player.friendshipStats.numFriendFb;
            }
            CachePvp.PlayerClan = player.playerClan;
            if (player.pvP2vs2SeasonData != null)
            {
                CachePvp.PvP2vs2SeasonData = player.pvP2vs2SeasonData;
            }
            CachePvp.MegaPlayerLevel = player.megaPlayerLevel;
            if (player.playerClan == null)
            {
                CachePvp.TypeMemberClan = CachePvp.HAS_NO_CLAN;
            }
            else
            {
                CachePvp.TypeMemberClan = player.playerClan.type;
            }
            //test
            //CachePvp.PlayerLevel.code = "SILVER";
            //CachePvp.PlayerLevel.name = "IV";
            //CachePvp.PlayerLevel.R = 1225;
            //CachePvp.PlayerLevel.star = 2;
            //end test
            //CachePvp.Medal = player.medal;
            UnityEngine.Debug.Log("COUNTRY: " + player.country);
            MessageDispatcher.SendMessage(EventName.ChangeProperties.UpdatePlayerInfo.ToString());
        }
    }
}