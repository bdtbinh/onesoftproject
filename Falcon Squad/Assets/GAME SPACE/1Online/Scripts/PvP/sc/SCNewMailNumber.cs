using OSNet;
using UnityEngine;

namespace Mp.Pvp
{
    public class SCNewMailNumber : SCMessage
    {
        public int numMail;

        public override string GetEvent()
        {
            return Events.NUM_MAIL;
        }
        public override void OnData()
        {
            CachePvp.NewMailNumber = numMail;
        }
    }
}