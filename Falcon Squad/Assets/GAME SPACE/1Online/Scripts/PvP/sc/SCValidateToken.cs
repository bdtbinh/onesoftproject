using System;
using System.Collections.Generic;
using com.ootii.Messages;
using Facebook.Unity;
using OSNet;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace Mp.Pvp
{
    public class SCValidateToken : SCMessage
    {
        public const int TOKEN_INVALID = 2;
        public bool pushFriends;
        public bool facebookConnected;
        public bool appCenterConnected;

        public override string GetEvent()
        {
            return Events.VALIDATE_TOKEN;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCValidateToken: status = " + status);
            if (status == SCMessage.SUCCESS)
            {
                CachePvp.isConnectServer = true;
                ConnectManager.Instance.HandShake();
                CachePvp.MyCode = int.Parse(CachePvp.Code);
                if (SceneManager.GetActiveScene().name.Equals("Home"))
                {
                    if (!CachePvp.isHomeOK)
                    {
                        CachePvp.isHomeOK = true;
                        new CSHomeOK(PvpUtil.PVP_VERSION).Send();
                    }
                    if (!CachePvp.isSendLoadingTime)
                    {
                        CachePvp.isSendLoadingTime = true;
                        long time = CachePvp.timeSinceStartUp;
                        string deviceId = CachePvp.deviceId;
                        string deviceName = SystemInfo.deviceModel;
                        string code = CachePvp.Code;
                        string platform = "";
#if UNITY_IPHONE
                platform = "ios";
#elif UNITY_ANDROID
                        platform = "android";
#endif
                        Debug.Log("send CSLoadingTime");
                        new CSLoadingTime(time, deviceId, deviceName, platform, code, Application.identifier).Send();
                        Debug.Log("after send CSLoadingTime");
                    }
                }
                if (GameContext.IS_CHINA_VERSION)
                {
                    if (!appCenterConnected)
                    {
                        if (!string.IsNullOrEmpty(CachePvp.GamecenterId))
                        {
                            CachePvp.GamecenterId = "";
                            CachePvp.Name = "";
                        }
                    }
                }
                else
                {
                    if (!facebookConnected)
                    {
                        if (!string.IsNullOrEmpty(CachePvp.FacebookId))
                        {
                            CachePvp.FacebookId = "";
                            CachePvp.facebookUser = null;
                            CachePvp.Name = "";
                            if (FB.IsLoggedIn)
                            {
                                FB.LogOut();
                            }
                        }
                    }
                }
                NetManager.Instance.PushQueue();
                PvpUtil.SendUpdatePlayer("SCValidateToken");
                new CSClientInfo().Send();
                UnityEngine.Debug.Log("pushFriends : " + pushFriends);
                if (GameContext.IS_CHINA_VERSION)
                {
                    if (appCenterConnected)
                    {
                        if (pushFriends)
                        {
                            if (Social.localUser.authenticated)
                            {
                                SocialManager.Instance.SetFriendsFacebookToServer();
                            }
                            else
                            {
                                SocialManager.Instance.LoginGameCenter();
                            }
                        }
                    }
                }
                else
                {
                    if (facebookConnected)
                    {
                        if (pushFriends)
                        {
                            if (FB.IsLoggedIn)
                            {
                                SocialManager.Instance.SetFriendsFacebookToServer();
                            }
                            else
                            {
                                SocialManager.Instance.FBLogIn();
                            }
                        }
                    }
                }
                MessageDispatcher.SendMessage(this, EventName.LoadProfile.LoadedProfile.ToString(), 0, 0);
                if (string.IsNullOrEmpty(CachePvp.deviceId))
                {
#if UNITY_EDITOR
                    if (SystemInfo.deviceModel.Equals("MS-7971 (MSI)"))
                    {
                        CachePvp.deviceId = "editornamnx";
                        new CSUpdateDeviceId(CachePvp.deviceId).Send();
                    }
                    else if (SystemInfo.deviceModel.Equals("MS-7A70 (MSI)"))
                    {
                        CachePvp.deviceId = "editorminhddv2";
                        new CSUpdateDeviceId(CachePvp.deviceId).Send();
                    }
#else
                    bool check = Application.RequestAdvertisingIdentifierAsync((string advertisingId, bool trackingEnabled, string error) =>
                    {
                        CachePvp.deviceId = advertisingId;
                        new CSUpdateDeviceId(advertisingId).Send();
                    });
#endif
                }
                else
                {
                    new CSUpdateDeviceId(CachePvp.deviceId).Send();
                }
            }
            else if (status == TOKEN_INVALID)
            {
                new CSGetToken(CachePvp.deviceId, CachePvp.FacebookId).Send();
                MessageDispatcher.SendMessage(EventName.SyncData.NoSync.ToString());
            }
        }
    }
}