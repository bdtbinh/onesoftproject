﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCClanEdit : SCMessage
    {
        public const int CLAN_NOT_EXIST = 10;
        public const int NOT_A_MASTER_CLAN = 14;

        public ClanInfo info;

        public override string GetEvent()
        {
            return Events.CLAN_EDIT;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, status, EventName.Clan.EditedClan.ToString(), info, 0);
        }
    }
}