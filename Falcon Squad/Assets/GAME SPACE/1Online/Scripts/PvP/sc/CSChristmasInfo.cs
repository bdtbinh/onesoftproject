﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCSelectLvEventInfo : SCMessage
    {
        int EVENT_IS_NOT_GOING_ON = 34;
        public SelectLvEventInfo info;
        public override string GetEvent()
        {
            return Events.SELECT_LV_EVENT_INFO;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.SelectLevelEvent.SelectLvEventInfo.ToString(), this, 0);
        }
    }
}