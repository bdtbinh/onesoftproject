﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCFriendshipStats : SCMessage
    {
        public string code;
        public FriendshipStats friendshipStats;
        public override string GetEvent()
        {
            return Events.FRIENDSHIP_STATS;
        }
        public override void OnData()
        {
            if (code.Equals(CachePvp.Code))
            {
                CachePvp.FriendshipStats = friendshipStats;
                if (friendshipStats != null)
                {
                    CachePvp.CountFriendsMinh = friendshipStats.numFriendFb;
                }
                MessageDispatcher.SendMessage(EventName.LoadProfile.LoadFriendshipStats.ToString());
            }
        }
    }
}