﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCClanJoinChat : SCMessage
    {
        public int type;
        public List<Chat> histories;
        public override string GetEvent()
        {
            return Events.CLAN_JOIN_CHAT;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                MessageDispatcher.SendMessage(this, type, EventName.Clan.LoadChat.ToString(), histories, 0);
            }
            else
            {
                MessageDispatcher.SendMessage(this, type, EventName.Clan.LoadedChatError.ToString(), histories, 0);
            }
        }
    }
}

public class Chat
{
    public const int CONTENT_TYPE_CHAT = 0;
    public const int CONTENT_TYPE_DONATE = 1;
    public const int CONTENT_TYPE_SYSTEM = 2;

    public string id;
    public int type;
    public string code;
    public string name;
    public string facebookId;
    public string appCenterId;
    public int contentType = CONTENT_TYPE_CHAT;//0: chat thường, 1: xin card
    public string content;
    public long time;

    public bool isDeleted;
}

public class ClanDonateData
{
    public int cardType;

    public int maxCanReceive;

    public long remainTime;
    public int received;

    public List<string> donaters;

    public long startTime;
}