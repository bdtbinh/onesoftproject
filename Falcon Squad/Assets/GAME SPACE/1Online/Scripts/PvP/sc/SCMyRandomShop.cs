﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCMyRandomShop : SCMessage
    {
        public RandomShop shop;
        public override string GetEvent()
        {
            return Events.MY_RANDOM_SHOP;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.RandomShop.MyRandomShop.ToString(), this, 0);
        }
    }
}

public class RandomShop
{

    public List<RandomShopItem> shopItems;

    public int resetPrice;
    public long timeToReset;
    public int resetRemain;
}

public class RandomShopItem
{
    public int id;
    public string name;
    public int quantity;
    public int price;
    public int extraAmount;
    public bool sale;
    public bool soldOut;
}
