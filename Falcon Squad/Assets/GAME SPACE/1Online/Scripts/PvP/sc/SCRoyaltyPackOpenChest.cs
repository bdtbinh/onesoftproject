﻿using com.ootii.Messages;
using OSNet;

namespace Mp.Pvp
{
    public class SCRoyaltyPackOpenChest : SCMessage
    {
        public override string GetEvent()
        {
            return Events.ROYALTY_PACK_OPEN_CHEST;
        }

        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.RoyaltyPack.RoyaltyPackOpenChest.ToString(), this, 0);
        }
    }
}
