﻿using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCSelectLvEventTicket : SCMessage
    {
        public int TICKET_IS_EMPTY = 30;
        public int EVENT_IS_NOT_GOING_ON = 34;
        public int ticket;
        public override string GetEvent()
        {
            return Events.SELECT_LV_EVENT_TICKET;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.SelectLevelEvent.SelectLvEventTicket.ToString(), this, 0);
        }
    }
}