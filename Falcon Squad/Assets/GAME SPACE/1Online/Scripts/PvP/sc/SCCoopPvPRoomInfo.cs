﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;
using Mp.Pvp;

namespace Mp.Pvp
{
    public class SCCoopPvPRoomInfo : SCMessage
    {
        public CoopPvPRoomData data;

        public override string GetEvent()
        {
            return Events.COOP_PVP_ROOM_INFO;
        }
        public override void OnData()
        {
            CachePvp.coopPvPRoomInfo = this;
            MessageDispatcher.SendMessage(this, EventName.PVP.CoopRoomInfo.ToString(), this, 0);
        }
    }
}

public class CoopPvPRoomData
{
    public const int MAX_MEMBER = 4;
    // room id
    public string id;
    // trạng thái phòng
    public const int WAIT_FOR_MATCHING = 0;
    public const int WAIT_FOR_READY = 1;
    public const int START = 2;
    public const int END = 3;
    public const int DESTROY = 4;

    public int state = WAIT_FOR_MATCHING;

    public int playTime;

    public long remainTime;

    public List<CoopPvPPlayerData> players;
    // map index (1 | 2) to team
    public List<CoopPvPTeamData> teams;

    public int waveCacheIndex;
    public string waveCacheData;
}

public class CoopPvPTeamData
{
    // index of team (1 | 2)
    public int index;
    public List<CoopPvPPlayerData> members;

    // điểm đạt được trong ván PvP
    public int score;

    // kết quả
    public const int WIN = 1;
    public const int LOSE = -1;
    public const int DRAWN = 0;

    // kết quả (WIN/LOSE/DRAWN/FORCE_WIN/FORCE_LOSE)
    public int result;
}

public class CoopPvPPlayerData
{
    // ingame state
    public const int STATE_WAITING = 1;
    public const int STATE_LOADING = 2;
    public const int STATE_READY = 3;
    public const int STATE_OUT = 4;
    public const int STATE_END = 5;

    // trạng thái game (DIED/OUT)
    public int state;

    // số điểm đạt được trong ván chơi
    public int score;

    // số điểm mùa giải đạt được sau khi kết thúc game
    public int seasonScoreRewards;

    // số vàng đặt cược
    public int goldBet;

    // map data
    public int spaceship;
    public int waveCacheIndex = 1;
    public string waveCacheData = "";
    public Player player;
}