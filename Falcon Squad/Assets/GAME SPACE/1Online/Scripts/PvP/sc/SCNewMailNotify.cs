using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCNewMailNotify : SCMessage
    {
        public override string GetEvent()
        {
            return Events.NEW_MAIL_NOTIFY;
        }
        public override void OnData()
        {
            CachePvp.NewMailNumber++;
        }
    }
}