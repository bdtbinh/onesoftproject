using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCCanPlayPVP : SCMessage
    {
        public long timeEndSession;
        public override string GetEvent()
        {
            return Events.CAN_PLAY_PVP;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                CachePvp.timeEndSession = timeEndSession;
                MessageDispatcher.SendMessage(EventName.PopupExtra.GoToPVP.ToString());
            }
        }
    }
}