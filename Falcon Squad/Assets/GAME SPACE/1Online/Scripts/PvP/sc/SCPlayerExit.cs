using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCPlayerExit : SCMessage
    {
        public string code;
        public override string GetEvent()
        {
            return Events.PLAYER_EXIT;
        }
		public override void OnData()
		{
			UnityEngine.Debug.Log("SCPlayerExit");
		}
    }
}