using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCRankings : SCMessage
    {
        public int type;
        public List<Player> players;
        public override string GetEvent()
        {
            return Events.RANKING;
        }
		public override void OnData()
		{
			UnityEngine.Debug.Log("SCRankings");
		}
    }
}