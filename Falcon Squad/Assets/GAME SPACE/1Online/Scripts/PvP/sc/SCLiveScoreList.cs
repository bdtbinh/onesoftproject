﻿using OSNet;
using System.Collections.Generic;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCLiveScoreList : SCMessage
    {
        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;

        public int type;
        public List<RoomInfo> rooms;
        public override string GetEvent()
        {
            return Events.LIVE_SCORE_LIST;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, "list", EventName.LiveScore.LoadListRoom.ToString(), this, 0);
        }
    }
}