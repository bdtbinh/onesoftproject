﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCMyRequests : SCMessage
    {
        public int total;
        public List<Player> players;
        public override string GetEvent()
        {
            return Events.MY_REQUEST;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.LoadProfile.LoadFriendRequest.ToString(), this, 0);
        }
    }
}