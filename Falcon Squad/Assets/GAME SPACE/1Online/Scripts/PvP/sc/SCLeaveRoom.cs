using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCLeaveRoom : SCMessage
    {
        public const int NOT_IN_ROOM = 2;
        public override string GetEvent()
        {
            return Events.LEAVE_ROOM;
        }
		public override void OnData()
		{
			UnityEngine.Debug.Log("SCLeaveRoom");
		}
    }
}