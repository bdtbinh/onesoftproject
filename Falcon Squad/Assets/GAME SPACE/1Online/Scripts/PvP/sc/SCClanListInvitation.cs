﻿using OSNet;
using System.Collections.Generic;

namespace Mp.Pvp
{
    public class SCClanListInvitation : SCMessage
    {
        public const int HAS_UNAUTHORITY = 2;
        public List<Player> players;
        public override string GetEvent()
        {
            return Events.CLAN_LIST_INVITATION;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {

            }
        }
    }
}