﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCCoopPvPEnemyDie : SCMessage
    {
        public int id;
        public override string GetEvent()
        {
            return Events.COOP_PVP_ENEMY_DIE;
        }
        public override void OnData()
        {
            if (status == SUCCESS)
            {
                MessageDispatcher.SendMessage(this, EventName.PVP.CoopPvPEnemyDie.ToString(), id, 0);
            }
        }
    }
}