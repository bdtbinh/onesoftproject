using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class SCRoomDestroy : SCMessage
    {
        public string roomId;
        public override string GetEvent()
        {
            return Events.ROOM_DESTROY;
        }
        public override void OnData()
        {
            UnityEngine.Debug.Log("SCRoomDestroy");
            if (PvPUIManager.Instance != null)
            {
                PvPUIManager.Instance.popupFindPvP.StopLoadingGameScene(roomId);
            }
            else if (InGamePvP.Instance != null)
            {
                InGamePvP.Instance.DestroyRoom();
            }
            else
            {
                CachePvp.isRoomDestroyed = true;
            }
        }
    }
}