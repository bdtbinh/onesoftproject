﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCLiveScoreJoin : SCMessage
    {
        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;
        public const int TYPE_PVP_2VS2 = 2;

        public int type;
        public List<RoomInfo> rooms;
        public List<CoopPvPRoomData> roomDatas;
        public override string GetEvent()
        {
            return Events.LIVE_SCORE_JOIN;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.LiveScore.LoadListRoom.ToString(), this, 0);
        }
    }
}