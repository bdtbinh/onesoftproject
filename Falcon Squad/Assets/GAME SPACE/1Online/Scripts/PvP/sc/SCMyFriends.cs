﻿using System;
using System.Collections.Generic;
using OSNet;
using com.ootii.Messages;

namespace Mp.Pvp
{
    public class SCMyFriends : SCMessage
    {
        public int total;
        public List<Player> players;

        public override string GetEvent()
        {
            return Events.MY_FRIENDS;
        }
        public override void OnData()
        {
            MessageDispatcher.SendMessage(this, EventName.LoadProfile.LoadedListFriends.ToString(), this, 0);
        }
    }
}