﻿using UnityEngine;
using BestHTTP.SocketIO;
using OSNet;
using Mp.Pvp;
using com.ootii.Messages;
using System.Collections;

public class OSNetMain : MonoBehaviour
{

    string uri = "http://falconsquad.net:9988/falcon-squad/";

    private SocketManager manager;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        //RemoteSettings.Updated += new RemoteSettings.UpdatedEventHandler(HandleRemoteUpdate);
    }

    private void Start()
    {
        StartCoroutine(StartURL());
    }

    IEnumerator StartURL()
    {
        //yield return new WaitUntil(() => CachePvp.isFetchFirebase == true);
        //string listServer = CacheFireBase.GetListSeverPvp; //http://api.falconsquad.net:9988/falcon-squad/;http://35.188.16.223:9988/falcon-squad/;
        //string txtDdos = CacheFireBase.GetUrlTxtDdos; // http://abc.xyz;
        //Debug.Log("listServer : " + listServer);
        //Debug.Log("txtDdos : " + txtDdos);
        //WWW www = new WWW(txtDdos);
        //yield return www;
        //string[] itemsListServer = listServer.Split(';');
        //if (string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.text))
        //{
        //    string[] itemsTxtDdos = www.text.Split(';');
        //    for (int i = 0; i < itemsListServer.Length; i++)
        //    {
        //        if (IsExists(i, itemsTxtDdos))
        //        {
        //            continue;
        //        }
        //        else
        //        {
        //            uri = itemsListServer[i];
        //            break;
        //        }
        //    }
        //}
        //if (string.IsNullOrEmpty(uri))
        //{
        //    uri = itemsListServer[0];
        //}

        yield return null;
        uri = CacheFireBase.GetSeverPvp;


        //uri = "http://35.188.16.223:9966/falcon-squad/";
        //uri = "http://falconsquad.net:9988/falcon-squad/";
        Debug.LogError("uri: " + uri);
        NetManager.Instance.OnStartListening = manager =>
        {
            this.manager = manager;
            manager.Socket.On(SocketIOEventTypes.Connect, OnServerConnect);
            manager.Socket.On(SocketIOEventTypes.Disconnect, OnServerDisconnect);
            manager.Socket.On(SocketIOEventTypes.Error, OnError);
            manager.Socket.On("reconnect", OnReconnect);
            manager.Socket.On("reconnecting", OnReconnecting);
            manager.Socket.On("reconnect_attempt", OnReconnectAttempt);
            manager.Socket.On("reconnect_failed", OnReconnectFailed);
        };
        NetManager.Instance.Start(uri);
        //Debug.LogError("StartURL: " + uri);
        Debug.Log("<color=#00ff00>" + "StartURL:" + uri + "</color>");
    }

    private bool IsExists(int i, string[] list)
    {
        for (int j = 0; j < list.Length; j++)
        {
            int b;
            if (int.TryParse(list[j], out b) && b == i)
            {
                Debug.Log(b);
                return true;
            }
            Debug.Log("out : " + b);
        }
        return false;
    }

    void OnServerConnect(Socket socket, Packet packet, params object[] args)
    {
        Debug.Log("Connected");
        var session = socket.Id;
        manager.Options.AdditionalQueryParams.Remove("session");
        //manager.Options.AdditionalQueryParams.Remove("version");
        //manager.Options.AdditionalQueryParams.Remove("platform");
        manager.Options.AdditionalQueryParams.Add("session", session);
        CachePvp.isShowPopupVideo = false;
        //        manager.Options.AdditionalQueryParams.Add("version", Application.version);
        //#if UNITY_ANDROID
        //        manager.Options.AdditionalQueryParams.Add("platform", "android");
        //#elif UNITY_IOS
        //        manager.Options.AdditionalQueryParams.Add("platform", "ios");
        //#endif
    }

    void OnServerDisconnect(Socket socket, Packet packet, params object[] args)
    {
        CachePvp.isConnectServer = false;
        Debug.Log("Disconnected");
        MessageDispatcher.SendMessage(EventName.ChangeProperties.DisconnectFromServer.ToString());
    }

    void OnError(Socket socket, Packet packet, params object[] args)
    {
        Debug.Log("OnError");
    }

    void OnReconnect(Socket socket, Packet packet, params object[] args)
    {
        Debug.Log("Reconnected");
        MessageDispatcher.SendMessage(EventName.ChangeProperties.DisconnectFromServer.ToString());
    }

    void OnReconnecting(Socket socket, Packet packet, params object[] args)
    {
        Debug.Log("Reconnecting");
    }

    void OnReconnectAttempt(Socket socket, Packet packet, params object[] args)
    {
        Debug.Log("ReconnectAttempt");
    }

    void OnReconnectFailed(Socket socket, Packet packet, params object[] args)
    {
        Debug.Log("ReconnectFailed");
    }

}
