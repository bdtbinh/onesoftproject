﻿namespace Mp.Pvp {
    public class Const {
        public const string defaultURI = "http://123.30.186.226:7788/falcon-squad/";

        public const string TOKEN_PLAYERPREFS_KEY = "MultiplayerToken";
        public const string NAME_PLAYERPREFS_KEY = "MultiplayerName";
        public const string USE_FB_PLAYERPREFS_KEY = "MultiplayerUseFB";
        public const string DEFAULT_NAME = "User";
    }

    public enum EndGamePlayerState {
        DIED = 1,
        OUT = 2,
        READY = 3,
        UNREADY = 4
    }

    public enum EndGameState {
        WIN = 1,
        LOSE = -1,
        DRAWN = 0
    }

    public enum FindState {
        SUCCESS = 0,
        ERROR = 1,
        WAITING = 2,
        TIMEOUT = 3,
        NOT_ENOUGH_MONEY = 4
    }

    public enum FacebookConnectState {
        SUCCESS = 0,
        ERROR = 1,
        SYSTEM_TOKEN_INVALID = 2,
        FACEBOOK_USED_ON_ANOTHER_DEVICE = 3,
        FACEBOOK_TOKEN_INVALID = 4
    }

    public enum UseFBState {
        NotChoose,
        Yes,
        No
    }

    public enum CreateRoomState {
        SUCCESS = 0,
        ERROR = 1,
        /// <summary>
        /// User đang trong room nên không thể tạo room
        /// </summary>
        JOINED_ROOM = 2,
        /// <summary>
        /// Tên room đã tồng tại
        /// </summary>
        EXISTED_ROOM = 3
    }

    public enum JoinRoomState {
        SUCCESS = 0,
        ERROR = 1,
        /// <summary>
        /// User đã join room khác trước đó
        /// </summary>
        JOINED_ROOM = 2,
        /// <summary>
        /// Room không tồn tại
        /// </summary>
        NOT_EXIST_ROOM = 3
    }

    public enum RankType {
        world,
        country,
        friend
    }
}
