﻿using UnityEngine;

public class OSNetMethod : MonoBehaviour {
    public static void SetNumberByIcon(GameObject[] icons, int number) {
        for (int i = 0; i < icons.Length; i++) {
            icons[i].SetActive(number > i);
        }
    }
}
