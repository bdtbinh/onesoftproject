﻿using System.Collections;
using System.Collections.Generic;
using OneSoftGame.Tools;
using UnityEngine;
using com.ootii.Messages;
using Mp.Pvp;
using System;
using UnityEngine.SceneManagement;

public class PopupManager : PersistentSingleton<PopupManager>
{
    public bool isTest;

    public GameObject registerPopup;
    public GameObject mailPopup;
    public GameObject toastText;
    public GameObject rankingPopup;
    public GameObject profilePopup;
    public GameObject profilePopupOnTop;
    public GameObject inviteButton;
    public GameObject inviteButton2v2;
    public GameObject inviteButtonTournament;
    public GameObject invitePopup;
    public GameObject invitePopupTournament;
    public GameObject popupCongratulationPVP;
    public GameObject popupCongratulationVip;
    public GameObject popupFriendsRequest;
    public GameObject popupFollow;
    public GameObject popupUIPVP;
    public GameObject popupNotify;
    public GameObject popupForceUpdate;
    public GameObject popupSync;
    public GameObject popupVip;
    public GameObject popupVipInfo;
    public GameObject popupPromotion;
    public GameObject popupRankReward;
    public GameObject popupRankReward2v2;
    public GameObject popupClan;
    public GameObject popupClanCongratulation;
    public GameObject popupClaim;
    public GameObject popupVideoReward;
    public GameObject popupButtonTextRunning;
    public GameObject popupLiveScore;
    public GameObject popupGloryChest;
    public GameObject popupInviteFriends;
    public GameObject popupInviteFriends2v2;
    public GameObject popupTournament;
    public GameObject popupSelectAircraft;
    public GameObject popupSelectCardType;
    public GameObject popupSelectWingman;
    public GameObject popupSelectWing;
    public GameObject popupReceiveRequest;
    public GameObject popupShopClan;
    public GameObject popupResetShopClan;
    public GameObject popupBuyTicket;
    public GameObject popupBonusReward;
    public GameObject popupDonateInfo;
    public GameObject popupPVP2V2;
    public GameObject popupChatWorld;
    public GameObject popupTeamMail;

    DateTime dateTimeClickWorld;
    DateTime dateTimeClickLocal;
    DateTime dateTimeClickFacebook;

    DateTime dateTimeClickWorld2v2;
    DateTime dateTimeClickLocal2v2;
    DateTime dateTimeClickFacebook2v2;

    DateTime dateTimeClickWorldPVP;
    DateTime dateTimeClickLocalPVP;
    DateTime dateTimeClickFacebookPVP;

    bool _canLoadWorld = true;

    public bool canLoadWorld
    {
        get
        {
            return _canLoadWorld;
        }
        set
        {
            _canLoadWorld = value;
        }
    }

    bool _canLoadLocal = true;

    public bool canLoadLocal
    {
        get
        {
            return _canLoadLocal;
        }
        set
        {
            _canLoadLocal = value;
        }
    }

    bool _canLoadFacebook = true;

    public bool canLoadFacebook
    {
        get
        {
            return _canLoadFacebook;
        }
        set
        {
            _canLoadFacebook = value;
        }
    }

    bool _canLoadWorld2v2 = true;

    public bool canLoadWorld2v2
    {
        get
        {
            return _canLoadWorld2v2;
        }
        set
        {
            _canLoadWorld2v2 = value;
        }
    }

    bool _canLoadLocal2v2 = true;

    public bool canLoadLocal2v2
    {
        get
        {
            return _canLoadLocal2v2;
        }
        set
        {
            _canLoadLocal2v2 = value;
        }
    }

    bool _canLoadFacebook2v2 = true;

    public bool canLoadFacebook2v2
    {
        get
        {
            return _canLoadFacebook2v2;
        }
        set
        {
            _canLoadFacebook2v2 = value;
        }
    }

    bool _canLoadWorldPVP = true;

    public bool canLoadWorldPVP
    {
        get
        {
            return _canLoadWorldPVP;
        }
        set
        {
            _canLoadWorldPVP = value;
        }
    }

    bool _canLoadLocalPVP = true;

    public bool canLoadLocalPVP
    {
        get
        {
            return _canLoadLocalPVP;
        }
        set
        {
            _canLoadLocalPVP = value;
        }
    }

    bool _canLoadFacebookPVP = true;

    public bool canLoadFacebookPVP
    {
        get
        {
            return _canLoadFacebookPVP;
        }
        set
        {
            _canLoadFacebookPVP = value;
        }
    }

    int _typeRank = 0; //0: normal, 1: pvp, 2: 2v2

    public int typeRank
    {
        get
        {
            return _typeRank;
        }
        set
        {
            _typeRank = value;
        }
    }

    public int typeInvite;

    private Dictionary<string, GameObject> cachedPopups = new Dictionary<string, GameObject>();

    public const string POPUP_REGISTER = "register";
    public const string POPUP_RANKING = "ranking";
    public const string POPUP_PROFILE = "profile";
    public const string POPUP_PROFILE_ON_TOP = "profile_on_top";
    public const string POPUP_INVITE = "invite";
    public const string POPUP_INVITE_TOURNAMENT = "invite_tournament";
    public const string POPUP_CONGRATULATION = "congratulation";
    public const string POPUP_INFO_VIP = "vip_info";
    public const string POPUP_CONGRATULATION_VIP = "congratulation_vip";
    public const string POPUP_FRIENDS_REQUEST = "friend_request";
    public const string POPUP_FOLLOW = "follow";
    public const string POPUP_UI_PVP = "ui_pvp";
    public const string POPUP_NOTIFY = "notify";
    public const string POPUP_FORCE_UPDATE = "update";
    public const string POPUP_VIP = "vip";
    public const string POPUP_SYNC = "sync";
    public const string POPUP_MAIL = "mail";
    public const string POPUP_PROMOTION = "promotion";
    public const string POPUP_RANK_REWARD = "rank_reward";
    public const string POPUP_RANK_REWARD_2V2 = "rank_reward_2v2";
    public const string POPUP_CLAN = "clan";
    public const string POPUP_CLAN_CONGRATULATION = "clan_congratulation";
    public const string POPUP_CLAIM = "claim";
    public const string POPUP_VIDEO_REWARD = "video_reward";
    public const string POPUP_TEXT_RUNNING = "text_running";
    public const string POPUP_LIVE_SCORE = "live_score";
    public const string POPUP_GLORY_CHEST = "glory_chest";
    public const string POPUP_INVITE_FRIENDS = "invite_friends";
    public const string POPUP_INVITE_FRIENDS_2V2 = "invite_friends_2v2";
    public const string POPUP_TOURNAMENT = "tournament";
    public const string POPUP_SELECT_AIRCRAFT = "select_aircraft";
    public const string POPUP_SELECT_CARDTYPE = "select_cardType";
    public const string POPUP_SELECT_WINGMAN = "select_wingman";
    public const string POPUP_SELECT_WING = "select_wing";
    public const string POPUP_RECEIVE_REQUEST = "receive_request";
    public const string POPUP_SHOP_CLAN = "shop_clan";
    public const string POPUP_RESET_SHOP_CLAN = "reset_shop_clan";
    public const string POPUP_BUY_TICKET = "buy_ticket";
    public const string POPUP_BONUS_REWARD = "bonus_reward";
    public const string POPUP_DONATE_INFO = "donate_info";
    public const string POPUP_PVP_2V2 = "pvp_2v2";
    public const string POPUP_CHAT_WORLD = "chat_world";
    public const string POPUP_TEAM_MAIL = "team_mail";

    void Awake()
    {
        base.Awake();
        SceneManager.sceneLoaded += OnSceneFinishedLoading;
        MessageDispatcher.AddListener(EventName.ChangeProperties.UpdatePlayerInfo.ToString(), OnUpdatePlayerInfo, true);
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneFinishedLoading;
        MessageDispatcher.RemoveListener(EventName.ChangeProperties.UpdatePlayerInfo.ToString(), OnUpdatePlayerInfo, true);
    }

    private void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.name.Equals("Home") || scene.name.Equals("SelectLevel") || scene.name.Equals("PVPMain"))
        {
            if (needRunAgain)
            {
                ShowTextRunningPopup(text, startPosition, timeRemaining);
            }
        }
    }

    public bool GetAvailableClickWorld()
    {
        return canLoadWorld;
    }

    public bool GetAvailableClickLocal()
    {
        return canLoadLocal;
    }

    public bool GetAvailableClickFacebook()
    {
        return canLoadFacebook;
    }

    public bool GetAvailableClickWorld2v2()
    {
        return canLoadWorld2v2;
    }

    public bool GetAvailableClickLocal2v2()
    {
        return canLoadLocal2v2;
    }

    public bool GetAvailableClickFacebook2v2()
    {
        return canLoadFacebook2v2;
    }

    public bool GetAvailableClickWorldPVP()
    {
        return canLoadWorldPVP;
    }

    public bool GetAvailableClickLocalPVP()
    {
        return canLoadLocalPVP;
    }

    public bool GetAvailableClickFacebookPVP()
    {
        return canLoadFacebookPVP;
    }

    public void ResetClickWorld()
    {
        dateTimeClickWorld = DateTime.Now;
        canLoadWorld = false;
    }

    public void ResetClickLocal()
    {
        dateTimeClickLocal = DateTime.Now;
        canLoadLocal = false;
    }

    public void ResetClickFacebook()
    {
        dateTimeClickFacebook = DateTime.Now;
        canLoadFacebook = false;
    }

    public void ResetClickWorld2v2()
    {
        dateTimeClickWorld2v2 = DateTime.Now;
        canLoadWorld2v2 = false;
    }

    public void ResetClickLocal2v2()
    {
        dateTimeClickLocal2v2 = DateTime.Now;
        canLoadLocal2v2 = false;
    }

    public void ResetClickFacebook2v2()
    {
        dateTimeClickFacebook2v2 = DateTime.Now;
        canLoadFacebook2v2 = false;
    }

    public void ResetClickWorldPVP()
    {
        dateTimeClickWorldPVP = DateTime.Now;
        canLoadWorldPVP = false;
    }

    public void ResetClickLocalPVP()
    {
        dateTimeClickLocalPVP = DateTime.Now;
        canLoadLocalPVP = false;
    }

    public void ResetClickFacebookPVP()
    {
        dateTimeClickFacebookPVP = DateTime.Now;
        canLoadFacebookPVP = false;
    }

    public double GetSecondsClickWorldPassed()
    {
        return DateTime.Now.Subtract(dateTimeClickWorld).TotalSeconds;
    }

    public double GetSecondsClickLocalPassed()
    {
        return DateTime.Now.Subtract(dateTimeClickLocal).TotalSeconds;
    }

    public double GetSecondsClickFacebookPassed()
    {
        return DateTime.Now.Subtract(dateTimeClickFacebook).TotalSeconds;
    }

    public double GetSecondsClickWorldPassed2v2()
    {
        return DateTime.Now.Subtract(dateTimeClickWorld2v2).TotalSeconds;
    }

    public double GetSecondsClickLocalPassed2v2()
    {
        return DateTime.Now.Subtract(dateTimeClickLocal2v2).TotalSeconds;
    }

    public double GetSecondsClickFacebookPassed2v2()
    {
        return DateTime.Now.Subtract(dateTimeClickFacebook2v2).TotalSeconds;
    }

    public double GetSecondsClickWorldPassedPVP()
    {
        return DateTime.Now.Subtract(dateTimeClickWorldPVP).TotalSeconds;
    }

    public double GetSecondsClickLocalPassedPVP()
    {
        return DateTime.Now.Subtract(dateTimeClickLocalPVP).TotalSeconds;
    }

    public double GetSecondsClickFacebookPassedPVP()
    {
        return DateTime.Now.Subtract(dateTimeClickFacebookPVP).TotalSeconds;
    }

    void OnUpdatePlayerInfo(IMessage msg)
    {
        canLoadWorld = true;
        canLoadLocal = true;
        canLoadFacebook = true;
        canLoadWorldPVP = true;
        canLoadLocalPVP = true;
        canLoadFacebookPVP = true;
    }

    public void ShowRegisterPopup(string target = "")
    {
        GameObject objRegister;
        if (!cachedPopups.TryGetValue(POPUP_REGISTER, out objRegister) || objRegister == null)
        {
            objRegister = Instantiate(registerPopup, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_REGISTER);
            cachedPopups.Add(POPUP_REGISTER, objRegister);
        }
        objRegister.transform.parent = GameObject.Find("UI Root").transform;
        objRegister.transform.localScale = Vector3.one;
        objRegister.SetActive(true);
        objRegister.GetComponent<PopupRegisterPvP>().SetTargetScreen(target);
    }

    public void HideRegisterPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_REGISTER) && cachedPopups[POPUP_REGISTER] != null)
        {
            cachedPopups[POPUP_REGISTER].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_REGISTER]);
            cachedPopups[POPUP_REGISTER].SetActive(false);
        }
    }

    public void ShowTextRunningPopup(string text, float startPosition = 360, float timeRemaining = 0, string url = "")
    {
        GameObject objTextRuning;
        bool isRunning = true;
        if (!cachedPopups.TryGetValue(POPUP_TEXT_RUNNING, out objTextRuning) || objTextRuning == null)
        {
            objTextRuning = Instantiate(popupButtonTextRunning, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_TEXT_RUNNING);
            cachedPopups.Add(POPUP_TEXT_RUNNING, objTextRuning);
            isRunning = false;
        }
        objTextRuning.transform.parent = GameObject.Find("UI Root").transform;
        objTextRuning.transform.localScale = Vector3.one;
        objTextRuning.transform.localPosition = new Vector3(0, 514, 0);
        objTextRuning.SetActive(true);
        objTextRuning.GetComponent<ButtonTextRunning>().SetData(text, isRunning, startPosition, timeRemaining, url);
    }

    public void HideTextRunningPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_TEXT_RUNNING) && cachedPopups[POPUP_TEXT_RUNNING] != null)
        {
            cachedPopups[POPUP_TEXT_RUNNING].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_TEXT_RUNNING]);
            cachedPopups[POPUP_TEXT_RUNNING].SetActive(false);
            needRunAgain = false;
        }
    }

    bool needRunAgain;
    string text = "";
    float startPosition;
    float timeRemaining;

    public void RunningTextAgain(string text, float startPosition, float timeRemaining)
    {
        needRunAgain = true;
        this.text = text;
        this.startPosition = startPosition;
        this.timeRemaining = timeRemaining;
    }

    public void ChangeDepthRegisterPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_REGISTER) && cachedPopups[POPUP_REGISTER] != null)
        {
            cachedPopups[POPUP_REGISTER].GetComponent<ChangeDepthLayer>().enabled = true;
        }
    }

    public void ShowSelectAircraftPopup()
    {
        GameObject objSelectAircraft;
        if (!cachedPopups.TryGetValue(POPUP_SELECT_AIRCRAFT, out objSelectAircraft) || objSelectAircraft == null)
        {
            objSelectAircraft = Instantiate(popupSelectAircraft, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_SELECT_AIRCRAFT);
            cachedPopups.Add(POPUP_SELECT_AIRCRAFT, objSelectAircraft);
        }
        objSelectAircraft.transform.parent = GameObject.Find("UI Root").transform;
        objSelectAircraft.transform.localScale = Vector3.one;
        objSelectAircraft.SetActive(true);
    }

    public void HideSelectAircraftPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_SELECT_AIRCRAFT) && cachedPopups[POPUP_SELECT_AIRCRAFT] != null)
        {
            cachedPopups[POPUP_SELECT_AIRCRAFT].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_SELECT_AIRCRAFT]);
            cachedPopups[POPUP_SELECT_AIRCRAFT].SetActive(false);
        }
    }

    public bool IsSelectAircraftPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_SELECT_AIRCRAFT) && cachedPopups[POPUP_SELECT_AIRCRAFT] != null)
        {
            return cachedPopups[POPUP_SELECT_AIRCRAFT].activeInHierarchy;
        }
        return false;
    }

    public void ShowBuyTicketPopup()
    {
        GameObject objBuyTicket;
        if (!cachedPopups.TryGetValue(POPUP_BUY_TICKET, out objBuyTicket) || objBuyTicket == null)
        {
            objBuyTicket = Instantiate(popupBuyTicket, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_BUY_TICKET);
            cachedPopups.Add(POPUP_BUY_TICKET, objBuyTicket);
        }
        objBuyTicket.transform.parent = GameObject.Find("UI Root").transform;
        objBuyTicket.transform.localScale = Vector3.one;
        objBuyTicket.SetActive(true);
    }

    public void HideBuyTicketPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_BUY_TICKET) && cachedPopups[POPUP_BUY_TICKET] != null)
        {
            cachedPopups[POPUP_BUY_TICKET].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_BUY_TICKET]);
            cachedPopups[POPUP_BUY_TICKET].SetActive(false);
        }
    }

    public bool IsBuyTicketPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_BUY_TICKET) && cachedPopups[POPUP_BUY_TICKET] != null)
        {
            return cachedPopups[POPUP_BUY_TICKET].activeInHierarchy;
        }
        return false;
    }

    public void ShowBonusRewardPopup(bool bonus = true)
    {
        GameObject objBonusReward;
        if (!cachedPopups.TryGetValue(POPUP_BONUS_REWARD, out objBonusReward) || objBonusReward == null)
        {
            objBonusReward = Instantiate(popupBonusReward, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_BONUS_REWARD);
            cachedPopups.Add(POPUP_BONUS_REWARD, objBonusReward);
        }
        objBonusReward.transform.parent = GameObject.Find("UI Root").transform;
        objBonusReward.transform.localScale = Vector3.one;
        objBonusReward.SetActive(true);
        objBonusReward.GetComponent<PopupBonusReward>().SetData(bonus);
    }

    public void HideBonusRewardPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_BONUS_REWARD) && cachedPopups[POPUP_BONUS_REWARD] != null)
        {
            cachedPopups[POPUP_BONUS_REWARD].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_BONUS_REWARD]);
            cachedPopups[POPUP_BONUS_REWARD].SetActive(false);
        }
    }

    public bool IsBonusRewardPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_BONUS_REWARD) && cachedPopups[POPUP_BONUS_REWARD] != null)
        {
            return cachedPopups[POPUP_BONUS_REWARD].activeInHierarchy;
        }
        return false;
    }

    public void ShowShopClanPopup()
    {
        GameObject objSelectShopClan;
        if (!cachedPopups.TryGetValue(POPUP_SHOP_CLAN, out objSelectShopClan) || objSelectShopClan == null)
        {
            objSelectShopClan = Instantiate(popupShopClan, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_SHOP_CLAN);
            cachedPopups.Add(POPUP_SHOP_CLAN, objSelectShopClan);
        }
        objSelectShopClan.transform.parent = GameObject.Find("UI Root").transform;
        objSelectShopClan.transform.localScale = Vector3.one;
        objSelectShopClan.SetActive(true);
    }

    public void HideShopClanPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_SHOP_CLAN) && cachedPopups[POPUP_SHOP_CLAN] != null)
        {
            cachedPopups[POPUP_SHOP_CLAN].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_SHOP_CLAN]);
            cachedPopups[POPUP_SHOP_CLAN].SetActive(false);
        }
    }

    public bool IsShopClanPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_SHOP_CLAN) && cachedPopups[POPUP_SHOP_CLAN] != null)
        {
            return cachedPopups[POPUP_SHOP_CLAN].activeInHierarchy;
        }
        return false;
    }

    public void ShowResetShopClanPopup(int price)
    {
        GameObject objResetSelectShopClan;
        if (!cachedPopups.TryGetValue(POPUP_RESET_SHOP_CLAN, out objResetSelectShopClan) || objResetSelectShopClan == null)
        {
            objResetSelectShopClan = Instantiate(popupResetShopClan, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_RESET_SHOP_CLAN);
            cachedPopups.Add(POPUP_RESET_SHOP_CLAN, objResetSelectShopClan);
        }
        objResetSelectShopClan.transform.parent = GameObject.Find("UI Root").transform;
        objResetSelectShopClan.transform.localScale = Vector3.one;
        objResetSelectShopClan.SetActive(true);
        objResetSelectShopClan.GetComponent<PopupResetClanShop>().SetData(price);
    }

    public void HideResetShopClanPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_RESET_SHOP_CLAN) && cachedPopups[POPUP_RESET_SHOP_CLAN] != null)
        {
            cachedPopups[POPUP_RESET_SHOP_CLAN].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_RESET_SHOP_CLAN]);
            cachedPopups[POPUP_RESET_SHOP_CLAN].SetActive(false);
        }
    }

    public bool IsResetShopClanPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_RESET_SHOP_CLAN) && cachedPopups[POPUP_RESET_SHOP_CLAN] != null)
        {
            return cachedPopups[POPUP_RESET_SHOP_CLAN].activeInHierarchy;
        }
        return false;
    }

    public void ShowDonateInfoPopup()
    {
        GameObject objDonateInfo;
        if (!cachedPopups.TryGetValue(POPUP_DONATE_INFO, out objDonateInfo) || objDonateInfo == null)
        {
            objDonateInfo = Instantiate(popupDonateInfo, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_DONATE_INFO);
            cachedPopups.Add(POPUP_DONATE_INFO, objDonateInfo);
        }
        objDonateInfo.transform.parent = GameObject.Find("UI Root").transform;
        objDonateInfo.transform.localScale = Vector3.one;
        objDonateInfo.SetActive(true);
    }

    public void HideDonateInfoPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_DONATE_INFO) && cachedPopups[POPUP_DONATE_INFO] != null)
        {
            cachedPopups[POPUP_DONATE_INFO].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_DONATE_INFO]);
            cachedPopups[POPUP_DONATE_INFO].SetActive(false);
        }
    }

    public bool IsDonateInfoPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_DONATE_INFO) && cachedPopups[POPUP_DONATE_INFO] != null)
        {
            return cachedPopups[POPUP_DONATE_INFO].activeInHierarchy;
        }
        return false;
    }

    public void ShowTeamMailPopup()
    {
        GameObject objTeamMail;
        if (!cachedPopups.TryGetValue(POPUP_TEAM_MAIL, out objTeamMail) || objTeamMail == null)
        {
            objTeamMail = Instantiate(popupTeamMail, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_TEAM_MAIL);
            cachedPopups.Add(POPUP_TEAM_MAIL, objTeamMail);
        }
        objTeamMail.transform.parent = GameObject.Find("UI Root").transform;
        objTeamMail.transform.localScale = Vector3.one;
        objTeamMail.SetActive(true);
    }

    public void HideTeamMailPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_TEAM_MAIL) && cachedPopups[POPUP_TEAM_MAIL] != null)
        {
            cachedPopups[POPUP_TEAM_MAIL].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_TEAM_MAIL]);
            cachedPopups[POPUP_TEAM_MAIL].SetActive(false);
        }
    }

    public bool IsTeamMailPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_TEAM_MAIL) && cachedPopups[POPUP_TEAM_MAIL] != null)
        {
            return cachedPopups[POPUP_TEAM_MAIL].activeInHierarchy;
        }
        return false;
    }

    public void ShowSelectCardTypePopup()
    {
        GameObject objSelectAircraft;
        if (!cachedPopups.TryGetValue(POPUP_SELECT_CARDTYPE, out objSelectAircraft) || objSelectAircraft == null)
        {
            objSelectAircraft = Instantiate(popupSelectCardType, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_SELECT_CARDTYPE);
            cachedPopups.Add(POPUP_SELECT_CARDTYPE, objSelectAircraft);
        }
        objSelectAircraft.transform.parent = GameObject.Find("UI Root").transform;
        objSelectAircraft.transform.localScale = Vector3.one;
        objSelectAircraft.SetActive(true);
    }

    public void HideSelectCardTypePopup()
    {
        if (cachedPopups.ContainsKey(POPUP_SELECT_CARDTYPE) && cachedPopups[POPUP_SELECT_CARDTYPE] != null)
        {
            cachedPopups[POPUP_SELECT_CARDTYPE].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_SELECT_CARDTYPE]);
            cachedPopups[POPUP_SELECT_CARDTYPE].SetActive(false);
        }
    }

    public bool IsSelectCardTypePopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_SELECT_CARDTYPE) && cachedPopups[POPUP_SELECT_CARDTYPE] != null)
        {
            return cachedPopups[POPUP_SELECT_CARDTYPE].activeInHierarchy;
        }
        return false;
    }

    public void ShowSelectWingmanPopup(bool isRight)
    {
        GameObject objSelectWingman;
        if (!cachedPopups.TryGetValue(POPUP_SELECT_WINGMAN, out objSelectWingman) || objSelectWingman == null)
        {
            objSelectWingman = Instantiate(popupSelectWingman, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_SELECT_WINGMAN);
            cachedPopups.Add(POPUP_SELECT_WINGMAN, objSelectWingman);
        }
        objSelectWingman.transform.parent = GameObject.Find("UI Root").transform;
        objSelectWingman.transform.localScale = Vector3.one;
        objSelectWingman.SetActive(true);
        objSelectWingman.GetComponent<PopupSelectWingmanTournament>().SetData(isRight);
    }

    public void HideSelectWingmanPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_SELECT_WINGMAN) && cachedPopups[POPUP_SELECT_WINGMAN] != null)
        {
            cachedPopups[POPUP_SELECT_WINGMAN].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_SELECT_WINGMAN]);
            cachedPopups[POPUP_SELECT_WINGMAN].SetActive(false);
        }
    }

    public bool IsSelectWingmanPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_SELECT_WINGMAN) && cachedPopups[POPUP_SELECT_WINGMAN] != null)
        {
            return cachedPopups[POPUP_SELECT_WINGMAN].activeInHierarchy;
        }
        return false;
    }

    public void ShowSelectWingPopup()
    {
        GameObject objSelectWing;
        if (!cachedPopups.TryGetValue(POPUP_SELECT_WING, out objSelectWing) || objSelectWing == null)
        {
            objSelectWing = Instantiate(popupSelectWing, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_SELECT_WING);
            cachedPopups.Add(POPUP_SELECT_WING, objSelectWing);
        }
        objSelectWing.transform.parent = GameObject.Find("UI Root").transform;
        objSelectWing.transform.localScale = Vector3.one;
        objSelectWing.SetActive(true);
    }

    public void HideSelectWingPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_SELECT_WING) && cachedPopups[POPUP_SELECT_WING] != null)
        {
            cachedPopups[POPUP_SELECT_WING].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_SELECT_WING]);
            cachedPopups[POPUP_SELECT_WING].SetActive(false);
        }
    }

    public bool IsSelectWingPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_SELECT_WING) && cachedPopups[POPUP_SELECT_WING] != null)
        {
            return cachedPopups[POPUP_SELECT_WING].activeInHierarchy;
        }
        return false;
    }

    public void ShowGloryChestPopup()
    {
        GameObject objGloryChest;
        if (!cachedPopups.TryGetValue(POPUP_GLORY_CHEST, out objGloryChest) || objGloryChest == null)
        {
            objGloryChest = Instantiate(popupGloryChest, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_GLORY_CHEST);
            cachedPopups.Add(POPUP_GLORY_CHEST, objGloryChest);
        }
        objGloryChest.transform.parent = GameObject.Find("UI Root").transform;
        objGloryChest.transform.localScale = Vector3.one;
        objGloryChest.SetActive(true);
        //objMail.GetComponent<PopupMailBox>().SetTargetScreen(target);
    }

    public void HideGloryChestPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_GLORY_CHEST) && cachedPopups[POPUP_GLORY_CHEST] != null)
        {
            cachedPopups[POPUP_GLORY_CHEST].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_GLORY_CHEST]);
            cachedPopups[POPUP_GLORY_CHEST].SetActive(false);
        }
    }

    public bool IsGloryChestPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_GLORY_CHEST) && cachedPopups[POPUP_GLORY_CHEST] != null)
        {
            return cachedPopups[POPUP_GLORY_CHEST].activeInHierarchy;
        }
        return false;
    }

    public void ShowReceiveRequestPopup(int cardType, int cardAmount)
    {
        GameObject objReceiveRequest;
        if (!cachedPopups.TryGetValue(POPUP_RECEIVE_REQUEST, out objReceiveRequest) || objReceiveRequest == null)
        {
            objReceiveRequest = Instantiate(popupReceiveRequest, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_RECEIVE_REQUEST);
            cachedPopups.Add(POPUP_RECEIVE_REQUEST, objReceiveRequest);
        }
        objReceiveRequest.transform.parent = GameObject.Find("UI Root").transform;
        objReceiveRequest.transform.localScale = Vector3.one;
        objReceiveRequest.SetActive(true);
        objReceiveRequest.GetComponent<PopupReceiveRequest>().SetData(cardType, cardAmount);
    }

    public void HideReceiveRequestPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_RECEIVE_REQUEST) && cachedPopups[POPUP_RECEIVE_REQUEST] != null)
        {
            cachedPopups[POPUP_RECEIVE_REQUEST].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_RECEIVE_REQUEST]);
            cachedPopups[POPUP_RECEIVE_REQUEST].SetActive(false);
        }
    }

    public bool IsReceiveRequestPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_RECEIVE_REQUEST) && cachedPopups[POPUP_RECEIVE_REQUEST] != null)
        {
            return cachedPopups[POPUP_RECEIVE_REQUEST].activeInHierarchy;
        }
        return false;
    }

    public void ShowTournamentPopup()
    {
        GameObject objTournament;
        if (!cachedPopups.TryGetValue(POPUP_TOURNAMENT, out objTournament) || objTournament == null)
        {
            objTournament = Instantiate(popupTournament, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_TOURNAMENT);
            cachedPopups.Add(POPUP_TOURNAMENT, objTournament);
        }
        objTournament.transform.parent = GameObject.Find("UI Root").transform;
        objTournament.transform.localScale = Vector3.one;
        objTournament.SetActive(true);
        HideChatWorldPopup();
    }

    public void HideTournamentPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_TOURNAMENT) && cachedPopups[POPUP_TOURNAMENT] != null)
        {
            cachedPopups[POPUP_TOURNAMENT].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_TOURNAMENT]);
            cachedPopups[POPUP_TOURNAMENT].SetActive(false);
        }
    }

    public bool IsTournamentPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_TOURNAMENT) && cachedPopups[POPUP_TOURNAMENT] != null)
        {
            return cachedPopups[POPUP_TOURNAMENT].activeInHierarchy;
        }
        return false;
    }

    public void ShowPVP2V2Popup()
    {
        GameObject obj2v2;
        if (!cachedPopups.TryGetValue(POPUP_PVP_2V2, out obj2v2) || obj2v2 == null)
        {
            obj2v2 = Instantiate(popupPVP2V2, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_PVP_2V2);
            cachedPopups.Add(POPUP_PVP_2V2, obj2v2);
        }
        obj2v2.transform.parent = GameObject.Find("UI Root").transform;
        obj2v2.transform.localScale = Vector3.one;
        obj2v2.SetActive(true);
        HideChatWorldPopup();
    }

    public void HidePVP2V2Popup()
    {
        if (cachedPopups.ContainsKey(POPUP_PVP_2V2) && cachedPopups[POPUP_PVP_2V2] != null)
        {
            cachedPopups[POPUP_PVP_2V2].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_PVP_2V2]);
            cachedPopups[POPUP_PVP_2V2].SetActive(false);
        }
    }

    public bool IsPVP2V2PopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_PVP_2V2) && cachedPopups[POPUP_PVP_2V2] != null)
        {
            return cachedPopups[POPUP_PVP_2V2].activeInHierarchy;
        }
        return false;
    }

    PopupChatWorld chatWorld;

    public void ShowChatWorldPopup()
    {
        if (SceneManager.GetActiveScene().name.Equals("Home") && !PopupManagerCuong.Instance.IsActiveSelectLevelPopup())
        {
            GameObject obj2v2;
            if (!cachedPopups.TryGetValue(POPUP_CHAT_WORLD, out obj2v2) || obj2v2 == null)
            {
                obj2v2 = Instantiate(popupChatWorld, Vector3.zero, Quaternion.identity);
                cachedPopups.Remove(POPUP_CHAT_WORLD);
                cachedPopups.Add(POPUP_CHAT_WORLD, obj2v2);
            }
            obj2v2.transform.parent = GameObject.Find("UI Root").transform;
            obj2v2.transform.localScale = Vector3.one;
            obj2v2.SetActive(true);
            if (chatWorld == null)
            {
                chatWorld = obj2v2.GetComponent<PopupChatWorld>();
            }
            chatWorld.ShowButton();
        }
    }

    public void HideChatWorldPopup()
    {
        Debug.Log("HideChatWorldPopup");
        if (cachedPopups.ContainsKey(POPUP_CHAT_WORLD) && cachedPopups[POPUP_CHAT_WORLD] != null)
        {
            cachedPopups[POPUP_CHAT_WORLD].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_CHAT_WORLD]);
            cachedPopups[POPUP_CHAT_WORLD].SetActive(false);
        }
    }

    public bool IsChatWorldPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_CHAT_WORLD) && cachedPopups[POPUP_CHAT_WORLD] != null)
        {
            if (cachedPopups[POPUP_CHAT_WORLD].activeInHierarchy)
            {
                return chatWorld.IsShow();
            }
        }
        return false;
    }

    public void ShowInviteFriendsPopup()
    {
        GameObject objInviteFriends;
        if (!cachedPopups.TryGetValue(POPUP_INVITE_FRIENDS, out objInviteFriends) || objInviteFriends == null)
        {
            objInviteFriends = Instantiate(popupInviteFriends, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_INVITE_FRIENDS);
            cachedPopups.Add(POPUP_INVITE_FRIENDS, objInviteFriends);
        }
        objInviteFriends.transform.parent = GameObject.Find("UI Root").transform;
        objInviteFriends.transform.localScale = Vector3.one;
        objInviteFriends.SetActive(true);
        //objMail.GetComponent<PopupMailBox>().SetTargetScreen(target);
    }

    public void HideInviteFriendsPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_INVITE_FRIENDS) && cachedPopups[POPUP_INVITE_FRIENDS] != null)
        {
            cachedPopups[POPUP_INVITE_FRIENDS].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_INVITE_FRIENDS]);
            cachedPopups[POPUP_INVITE_FRIENDS].SetActive(false);
        }
    }

    public bool IsInviteFriendsPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_INVITE_FRIENDS) && cachedPopups[POPUP_INVITE_FRIENDS] != null)
        {
            return cachedPopups[POPUP_INVITE_FRIENDS].activeInHierarchy;
        }
        return false;
    }

    public void ShowInviteFriends2v2Popup()
    {
        GameObject objInviteFriends2v2;
        if (!cachedPopups.TryGetValue(POPUP_INVITE_FRIENDS_2V2, out objInviteFriends2v2) || objInviteFriends2v2 == null)
        {
            objInviteFriends2v2 = Instantiate(popupInviteFriends2v2, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_INVITE_FRIENDS_2V2);
            cachedPopups.Add(POPUP_INVITE_FRIENDS_2V2, objInviteFriends2v2);
        }
        objInviteFriends2v2.transform.parent = GameObject.Find("UI Root").transform;
        objInviteFriends2v2.transform.localScale = Vector3.one;
        objInviteFriends2v2.SetActive(true);
        //objMail.GetComponent<PopupMailBox>().SetTargetScreen(target);
    }

    public void HideInviteFriends2v2Popup()
    {
        if (cachedPopups.ContainsKey(POPUP_INVITE_FRIENDS_2V2) && cachedPopups[POPUP_INVITE_FRIENDS_2V2] != null)
        {
            cachedPopups[POPUP_INVITE_FRIENDS_2V2].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_INVITE_FRIENDS_2V2]);
            cachedPopups[POPUP_INVITE_FRIENDS_2V2].SetActive(false);
        }
    }

    public bool IsInviteFriends2v2PopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_INVITE_FRIENDS_2V2) && cachedPopups[POPUP_INVITE_FRIENDS_2V2] != null)
        {
            return cachedPopups[POPUP_INVITE_FRIENDS_2V2].activeInHierarchy;
        }
        return false;
    }

    public void ShowMailPopup()
    {
        GameObject objMail;
        if (!cachedPopups.TryGetValue(POPUP_MAIL, out objMail) || objMail == null)
        {
            objMail = Instantiate(mailPopup, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_MAIL);
            cachedPopups.Add(POPUP_MAIL, objMail);
        }
        objMail.transform.parent = GameObject.Find("UI Root").transform;
        objMail.transform.localScale = Vector3.one;
        objMail.SetActive(true);

        //objMail.GetComponent<PopupMailBox>().SetTargetScreen(target);
        if (SceneManager.GetActiveScene().name.Equals("Home"))
        {
            HideChatWorldPopup();
        }
    }

    public void HideMailPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_MAIL) && cachedPopups[POPUP_MAIL] != null)
        {
            cachedPopups[POPUP_MAIL].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_MAIL]);
            cachedPopups[POPUP_MAIL].SetActive(false);
        }
        if (SceneManager.GetActiveScene().name.Equals("Home"))
        {
            ShowChatWorldPopup();
        }
    }

    public bool IsMailPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_MAIL) && cachedPopups[POPUP_MAIL] != null)
        {
            return cachedPopups[POPUP_MAIL].activeInHierarchy;
        }
        return false;
    }

    public void ShowLiveScorePopup()
    {
        GameObject objLiveScore;
        if (!cachedPopups.TryGetValue(POPUP_LIVE_SCORE, out objLiveScore) || objLiveScore == null)
        {
            objLiveScore = Instantiate(popupLiveScore, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_LIVE_SCORE);
            cachedPopups.Add(POPUP_LIVE_SCORE, objLiveScore);
        }
        objLiveScore.transform.parent = GameObject.Find("UI Root").transform;
        objLiveScore.transform.localScale = Vector3.one;
        objLiveScore.SetActive(true);
    }

    public void HideLiveScorePopup()
    {
        if (cachedPopups.ContainsKey(POPUP_LIVE_SCORE) && cachedPopups[POPUP_LIVE_SCORE] != null)
        {
            cachedPopups[POPUP_LIVE_SCORE].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_LIVE_SCORE]);
            cachedPopups[POPUP_LIVE_SCORE].SetActive(false);
            CachePvp.LoadLiveScore = 0;
        }
    }

    public bool IsLiveScorePopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_LIVE_SCORE) && cachedPopups[POPUP_LIVE_SCORE] != null)
        {
            return cachedPopups[POPUP_LIVE_SCORE].activeInHierarchy;
        }
        return false;
    }

    public void ShowClanPopup()
    {
        GameObject objClan;
        if (!cachedPopups.TryGetValue(POPUP_CLAN, out objClan) || objClan == null)
        {
            objClan = Instantiate(popupClan, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_CLAN);
            cachedPopups.Add(POPUP_CLAN, objClan);
        }
        objClan.transform.parent = GameObject.Find("UI Root").transform;


        objClan.transform.localScale = Vector3.one;
        objClan.SetActive(true);
    }

    public void HideClanPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_CLAN) && cachedPopups[POPUP_CLAN] != null)
        {
            cachedPopups[POPUP_CLAN].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_CLAN]);
            cachedPopups[POPUP_CLAN].SetActive(false);
        }
    }

    public bool IsClanPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_CLAN) && cachedPopups[POPUP_CLAN] != null)
        {
            return cachedPopups[POPUP_CLAN].activeInHierarchy;
        }
        return false;
    }

    public void ShowClanCongratulationPopup(int index, string name)
    {
        GameObject objClanCongratulation;
        if (!cachedPopups.TryGetValue(POPUP_CLAN_CONGRATULATION, out objClanCongratulation) || objClanCongratulation == null)
        {
            objClanCongratulation = Instantiate(popupClanCongratulation, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_CLAN_CONGRATULATION);
            cachedPopups.Add(POPUP_CLAN_CONGRATULATION, objClanCongratulation);
        }
        objClanCongratulation.transform.parent = GameObject.Find("UI Root").transform;


        objClanCongratulation.transform.localScale = Vector3.one;
        objClanCongratulation.SetActive(true);
        objClanCongratulation.GetComponent<PopupClanCongratulation>().Refresh(index, name);
    }

    public void HideClanCongratulationPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_CLAN_CONGRATULATION) && cachedPopups[POPUP_CLAN_CONGRATULATION] != null)
        {
            cachedPopups[POPUP_CLAN_CONGRATULATION].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_CLAN_CONGRATULATION]);
            cachedPopups[POPUP_CLAN_CONGRATULATION].SetActive(false);
        }
    }

    public bool IsClanCongratulationPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_CLAN_CONGRATULATION) && cachedPopups[POPUP_CLAN_CONGRATULATION] != null)
        {
            return cachedPopups[POPUP_CLAN_CONGRATULATION].activeInHierarchy;
        }
        return false;
    }

    public void ShowClaimPopup(List<ItemVolumeFalconMail> items, string why, bool rewardFromPVP = false, bool rewardFromPVP2v2 = false)
    {
        Debug.Log("day");
        GameObject objClaim;
        if (!cachedPopups.TryGetValue(POPUP_CLAIM, out objClaim) || objClaim == null)
        {
            objClaim = Instantiate(popupClaim, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_CLAIM);
            cachedPopups.Add(POPUP_CLAIM, objClaim);
        }
        objClaim.transform.parent = GameObject.Find("UI Root").transform;

        objClaim.transform.localScale = Vector3.one;
        objClaim.SetActive(true);
        objClaim.GetComponent<PopupClaim>().GetReward(items, why, rewardFromPVP, rewardFromPVP2v2);
    }

    public void HideClaimPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_CLAIM) && cachedPopups[POPUP_CLAIM] != null)
        {
            cachedPopups[POPUP_CLAIM].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_CLAIM]);
            cachedPopups[POPUP_CLAIM].SetActive(false);
        }
    }

    public bool IsClaimPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_CLAIM) && cachedPopups[POPUP_CLAIM] != null)
        {
            return cachedPopups[POPUP_CLAIM].activeInHierarchy;
        }
        return false;
    }

    public void ShowVideoRewardPopup()
    {
        GameObject objVideo;
        if (!cachedPopups.TryGetValue(POPUP_VIDEO_REWARD, out objVideo) || objVideo == null)
        {
            objVideo = Instantiate(popupVideoReward, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_VIDEO_REWARD);
            cachedPopups.Add(POPUP_VIDEO_REWARD, objVideo);
        }
        objVideo.transform.parent = GameObject.Find("UI Root").transform;


        objVideo.transform.localScale = Vector3.one;
        objVideo.SetActive(true);
    }

    public void HideVideoRewardPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_VIDEO_REWARD) && cachedPopups[POPUP_VIDEO_REWARD] != null)
        {
            cachedPopups[POPUP_VIDEO_REWARD].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_VIDEO_REWARD]);
            cachedPopups[POPUP_VIDEO_REWARD].SetActive(false);
        }
    }

    public bool IsVideoRewardPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_VIDEO_REWARD) && cachedPopups[POPUP_VIDEO_REWARD] != null)
        {
            return cachedPopups[POPUP_VIDEO_REWARD].activeInHierarchy;
        }
        return false;
    }

    public void ShowVipPopup(bool openFromHome = true)
    {
        GameObject objVip;
        if (!cachedPopups.TryGetValue(POPUP_VIP, out objVip) || objVip == null)
        {
            objVip = Instantiate(popupVip, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_VIP);
            cachedPopups.Add(POPUP_VIP, objVip);
        }
        objVip.transform.parent = GameObject.Find("UI Root").transform;


        objVip.transform.localScale = Vector3.one;
        objVip.SetActive(true);
        PopupVip.OpenFromHome = openFromHome;
    }

    public void HideVipPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_VIP) && cachedPopups[POPUP_VIP] != null)
        {
            cachedPopups[POPUP_VIP].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_VIP]);
            cachedPopups[POPUP_VIP].SetActive(false);
        }
    }

    public bool IsVipPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_VIP) && cachedPopups[POPUP_VIP] != null)
        {
            return cachedPopups[POPUP_VIP].activeInHierarchy;
        }
        return false;
    }

    public void ChangePopupVipDepth(int depthAdd = 5000)
    {
        if (cachedPopups.ContainsKey(POPUP_VIP) && cachedPopups[POPUP_VIP] != null)
        {
            ChangeDepthLayer changeDepthLayer = cachedPopups[POPUP_VIP].GetComponent<ChangeDepthLayer>();
            changeDepthLayer.depthAdd = depthAdd;
            changeDepthLayer.enabled = true;

        }
    }


    public void ShowRankRewardPopup()
    {
        GameObject objReward;
        if (!cachedPopups.TryGetValue(POPUP_RANK_REWARD, out objReward) || objReward == null)
        {
            objReward = Instantiate(popupRankReward, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_RANK_REWARD);
            cachedPopups.Add(POPUP_RANK_REWARD, objReward);
        }
        objReward.transform.parent = GameObject.Find("UI Root").transform;


        objReward.transform.localScale = Vector3.one;
        objReward.SetActive(true);
        //objVip.GetComponent<PopupNotify>().SetUrl(id, url, message, urlImage, type, promotions);
    }

    public void HideRankRewardPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_RANK_REWARD) && cachedPopups[POPUP_RANK_REWARD] != null)
        {
            cachedPopups[POPUP_RANK_REWARD].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_RANK_REWARD]);
            cachedPopups[POPUP_RANK_REWARD].SetActive(false);
        }
    }

    public bool IsRankRewardPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_RANK_REWARD) && cachedPopups[POPUP_RANK_REWARD] != null)
        {
            return cachedPopups[POPUP_RANK_REWARD].activeInHierarchy;
        }
        return false;
    }

    public void ShowRankRewardPopup2v2()
    {
        GameObject objReward;
        if (!cachedPopups.TryGetValue(POPUP_RANK_REWARD_2V2, out objReward) || objReward == null)
        {
            objReward = Instantiate(popupRankReward2v2, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_RANK_REWARD_2V2);
            cachedPopups.Add(POPUP_RANK_REWARD_2V2, objReward);
        }
        objReward.transform.parent = GameObject.Find("UI Root").transform;

        objReward.transform.localScale = Vector3.one;
        objReward.SetActive(true);
    }

    public void HideRankRewardPopup2v2()
    {
        if (cachedPopups.ContainsKey(POPUP_RANK_REWARD_2V2) && cachedPopups[POPUP_RANK_REWARD_2V2] != null)
        {
            cachedPopups[POPUP_RANK_REWARD_2V2].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_RANK_REWARD_2V2]);
            cachedPopups[POPUP_RANK_REWARD_2V2].SetActive(false);
        }
    }

    public bool IsRankRewardPopupActive2v2()
    {
        if (cachedPopups.ContainsKey(POPUP_RANK_REWARD_2V2) && cachedPopups[POPUP_RANK_REWARD_2V2] != null)
        {
            return cachedPopups[POPUP_RANK_REWARD_2V2].activeInHierarchy;
        }
        return false;
    }

    public void ShowPromotionPopup()
    {
        GameObject objPromotion;
        if (!cachedPopups.TryGetValue(POPUP_PROMOTION, out objPromotion) || objPromotion == null)
        {
            objPromotion = Instantiate(popupPromotion, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_PROMOTION);
            cachedPopups.Add(POPUP_PROMOTION, objPromotion);
        }
        objPromotion.transform.parent = GameObject.Find("UI Root").transform;

        objPromotion.transform.localScale = Vector3.one;
        objPromotion.SetActive(true);
        //OfferWallController.Instance.ShowOfferWall();
    }

    public void HidePromotionPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_PROMOTION) && cachedPopups[POPUP_PROMOTION] != null)
        {
            cachedPopups[POPUP_PROMOTION].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_PROMOTION]);
            cachedPopups[POPUP_PROMOTION].SetActive(false);
        }
    }

    public bool IsPromotionPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_PROMOTION) && cachedPopups[POPUP_PROMOTION] != null)
        {
            return cachedPopups[POPUP_PROMOTION].activeInHierarchy;
        }
        return false;
    }

    public void ShowNotifyPopup(long id, string url, string message, string urlImage, int type, List<PromotionItem> promotions)
    {
        GameObject objNotify;
        if (!cachedPopups.TryGetValue(POPUP_NOTIFY, out objNotify) || objNotify == null)
        {
            objNotify = Instantiate(popupNotify, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_NOTIFY);
            cachedPopups.Add(POPUP_NOTIFY, objNotify);
        }
        objNotify.transform.parent = GameObject.Find("UI Root").transform;


        objNotify.transform.localScale = Vector3.one;
        objNotify.SetActive(true);
        objNotify.GetComponent<PopupNotify>().SetUrl(id, url, message, urlImage, type, promotions);
    }

    public void HideNotifyPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_NOTIFY) && cachedPopups[POPUP_NOTIFY] != null)
        {
            cachedPopups[POPUP_NOTIFY].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_NOTIFY]);
            cachedPopups[POPUP_NOTIFY].SetActive(false);
        }
    }

    public bool IsNotifyPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_NOTIFY) && cachedPopups[POPUP_NOTIFY] != null)
        {
            return cachedPopups[POPUP_NOTIFY].activeInHierarchy;
        }
        return false;
    }

    public void ShowForceUpdatePopup(bool isMaintain)
    {
        GameObject objForceUpdate;
        if (!cachedPopups.TryGetValue(POPUP_FORCE_UPDATE, out objForceUpdate) || objForceUpdate == null)
        {
            objForceUpdate = Instantiate(popupForceUpdate, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_FORCE_UPDATE);
            cachedPopups.Add(POPUP_FORCE_UPDATE, objForceUpdate);
        }
        objForceUpdate.transform.parent = GameObject.Find("UI Root").transform;

        objForceUpdate.transform.localScale = Vector3.one;
        objForceUpdate.SetActive(true);
        objForceUpdate.GetComponent<PopupForceUpdateVersion>().SetData(isMaintain);
    }

    public void HideForceUpdatePopup()
    {
        if (cachedPopups.ContainsKey(POPUP_FORCE_UPDATE) && cachedPopups[POPUP_FORCE_UPDATE] != null)
        {
            cachedPopups[POPUP_FORCE_UPDATE].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_FORCE_UPDATE]);
            cachedPopups[POPUP_FORCE_UPDATE].SetActive(false);
        }
    }

    public bool IsForceUpdatePopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_FORCE_UPDATE) && cachedPopups[POPUP_FORCE_UPDATE] != null)
        {
            return cachedPopups[POPUP_FORCE_UPDATE].activeInHierarchy;
        }
        return false;
    }

    public void ShowSyncPopup(Player player, string token = "")
    {
        GameObject objSync;
        if (!cachedPopups.TryGetValue(POPUP_SYNC, out objSync) || objSync == null)
        {
            objSync = Instantiate(popupSync, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_SYNC);
            cachedPopups.Add(POPUP_SYNC, objSync);
        }
        objSync.transform.parent = GameObject.Find("UI Root").transform;
        objSync.transform.localScale = Vector3.one;
        objSync.SetActive(true);
        objSync.GetComponent<PopupSync>().SetData(player, token);
    }

    public void HideSyncPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_SYNC) && cachedPopups[POPUP_SYNC] != null)
        {
            cachedPopups[POPUP_SYNC].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_SYNC]);
            cachedPopups[POPUP_SYNC].SetActive(false);
        }
    }

    public bool IsSyncPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_SYNC) && cachedPopups[POPUP_SYNC] != null)
        {
            return cachedPopups[POPUP_SYNC].activeInHierarchy;
        }
        return false;
    }

    public void ShowUIPVPPopup()
    {
        GameObject objUIPvp;
        if (!cachedPopups.TryGetValue(POPUP_UI_PVP, out objUIPvp) || objUIPvp == null)
        {
            objUIPvp = Instantiate(popupUIPVP, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_UI_PVP);
            cachedPopups.Add(POPUP_UI_PVP, objUIPvp);
        }
        objUIPvp.transform.parent = GameObject.Find("UI Root").transform;
        objUIPvp.transform.localScale = Vector3.one;
        objUIPvp.SetActive(true);
    }

    public void HideUIPVPPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_UI_PVP) && cachedPopups[POPUP_UI_PVP] != null)
        {
            cachedPopups[POPUP_UI_PVP].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_UI_PVP]);
            cachedPopups[POPUP_UI_PVP].SetActive(false);
        }
    }

    public bool IsRegisterPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_REGISTER) && cachedPopups[POPUP_REGISTER] != null)
        {
            return cachedPopups[POPUP_REGISTER].activeInHierarchy;
        }
        return false;
    }

    bool isShowing = false;
    public void ShowToast(string text)
    {
        if (!isShowing)
        {
            isShowing = true;
            GameObject objNotify = Instantiate(toastText, Vector3.zero, Quaternion.identity);
            objNotify.transform.parent = GameObject.Find("UI Root").transform;
            objNotify.transform.localScale = Vector3.one;
            objNotify.GetComponent<NotificationText>().ShowTextNotifiClickButton(text, false);

            LeanTween.delayedCall(0.36f, () =>
            {
                isShowing = false;
            }).setIgnoreTimeScale(true);
        }
    }

    public void ShowRanking(bool isMega = false, bool is2v2 = false)
    {
        if (!is2v2)
        {
            typeRank = 0;
        }
        else
        {
            typeRank = 2;
        }
        GameObject objRanking;
        if (!cachedPopups.TryGetValue(POPUP_RANKING, out objRanking) || objRanking == null)
        {
            objRanking = Instantiate(rankingPopup, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_RANKING);
            cachedPopups.Add(POPUP_RANKING, objRanking);
        }
        objRanking.transform.parent = GameObject.Find("UI Root").transform;
        objRanking.transform.localScale = Vector3.one;
        objRanking.SetActive(true);
        CachePvp.LoadMegaRank = isMega;
        if (!isMega)
        {
            MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadLocalRank.ToString(), 1, 0);
        }
        else
        {
            MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadLocalMegaRank.ToString(), 1, 0);
        }
    }

    public void ShowRankingPVP(bool is2v2 = false)
    {
        typeRank = 1;
        GameObject objRanking;
        if (!cachedPopups.TryGetValue(POPUP_RANKING, out objRanking) || objRanking == null)
        {
            objRanking = Instantiate(rankingPopup, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_RANKING);
            cachedPopups.Add(POPUP_RANKING, objRanking);
        }

        objRanking.transform.parent = GameObject.Find("UI Root").transform;
        objRanking.transform.localScale = Vector3.one;
        objRanking.SetActive(true);
        MessageDispatcher.SendMessage(gameObject, EventName.LoadLeaderboard.LoadLocalRank.ToString(), 1, 0);
    }

    public void HideRankingPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_RANKING) && cachedPopups[POPUP_RANKING] != null)
        {
            cachedPopups[POPUP_RANKING].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_RANKING]);
            cachedPopups[POPUP_RANKING].SetActive(false);
            CachePvp.LoadMegaRank = false;
        }
    }

    public bool IsRankingPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_RANKING) && cachedPopups[POPUP_RANKING] != null)
        {
            return cachedPopups[POPUP_RANKING].activeInHierarchy;
        }
        return false;
    }

    public void ShowCongratulationVip()
    {
        GameObject objCongratulation = null;
        if (!cachedPopups.TryGetValue(POPUP_CONGRATULATION_VIP, out objCongratulation) || objCongratulation == null)
        {
            objCongratulation = Instantiate(popupCongratulationVip, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_CONGRATULATION_VIP);
            cachedPopups.Add(POPUP_CONGRATULATION_VIP, objCongratulation);
        }
        objCongratulation.transform.parent = GameObject.Find("[PopupManagerCuong]").transform;
        objCongratulation.transform.localScale = Vector3.one;
        objCongratulation.SetActive(true);
        objCongratulation.GetComponent<PopupVipCongratulation>().Refresh();
    }

    public void HideCongratulationVip()
    {
        if (cachedPopups.ContainsKey(POPUP_CONGRATULATION_VIP) && cachedPopups[POPUP_CONGRATULATION_VIP] != null)
        {
            cachedPopups[POPUP_CONGRATULATION_VIP].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_CONGRATULATION_VIP]);
            cachedPopups[POPUP_CONGRATULATION_VIP].SetActive(false);
        }
    }

    public bool IsCongratulationVipActive()
    {
        if (cachedPopups.ContainsKey(POPUP_CONGRATULATION_VIP) && cachedPopups[POPUP_CONGRATULATION_VIP] != null)
        {
            return cachedPopups[POPUP_CONGRATULATION_VIP].activeInHierarchy;
        }
        return false;
    }

    public void ShowVipInfo()
    {
        GameObject objVipInfo = null;
        if (!cachedPopups.TryGetValue(POPUP_INFO_VIP, out objVipInfo) || objVipInfo == null)
        {
            objVipInfo = Instantiate(popupVipInfo, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_INFO_VIP);
            cachedPopups.Add(POPUP_INFO_VIP, objVipInfo);
        }
        objVipInfo.transform.parent = GameObject.Find("UI Root").transform;
        objVipInfo.transform.localScale = Vector3.one;
        objVipInfo.SetActive(true);
    }

    public void HideVipInfo()
    {
        if (cachedPopups.ContainsKey(POPUP_INFO_VIP) && cachedPopups[POPUP_INFO_VIP] != null)
        {
            cachedPopups[POPUP_INFO_VIP].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_INFO_VIP]);
            cachedPopups[POPUP_INFO_VIP].SetActive(false);
        }
    }

    public bool IsVipInfoActive()
    {
        if (cachedPopups.ContainsKey(POPUP_INFO_VIP) && cachedPopups[POPUP_INFO_VIP] != null)
        {
            return cachedPopups[POPUP_INFO_VIP].activeInHierarchy;
        }
        return false;
    }

    public void ChangeVipInfoDepth(int depthAdd)
    {
        if (cachedPopups.ContainsKey(POPUP_INFO_VIP) && cachedPopups[POPUP_INFO_VIP] != null)
        {
            ChangeDepthLayer changeDepthLayer = cachedPopups[POPUP_INFO_VIP].GetComponent<ChangeDepthLayer>();

            changeDepthLayer.depthAdd = depthAdd;
            changeDepthLayer.enabled = true;
        }
    }

    public void ShowCongratulation()
    {
        GameObject objCongratulation = null;
        if (!cachedPopups.TryGetValue(POPUP_CONGRATULATION, out objCongratulation) || objCongratulation == null)
        {
            objCongratulation = Instantiate(popupCongratulationPVP, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_CONGRATULATION);
            cachedPopups.Add(POPUP_CONGRATULATION, objCongratulation);
        }
        objCongratulation.transform.parent = GameObject.Find("UI Root").transform;
        objCongratulation.transform.localScale = Vector3.one;
        objCongratulation.SetActive(true);
        objCongratulation.GetComponent<PopupCongratulationPVP>().ShowCongratulation();
    }

    public void HideCongratulation()
    {
        if (cachedPopups.ContainsKey(POPUP_CONGRATULATION) && cachedPopups[POPUP_CONGRATULATION] != null)
        {
            cachedPopups[POPUP_CONGRATULATION].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_CONGRATULATION]);
            cachedPopups[POPUP_CONGRATULATION].SetActive(false);
        }
    }

    public void ShowFollowPopup()
    {
        GameObject objFollow;
        if (!cachedPopups.TryGetValue(POPUP_FOLLOW, out objFollow) || objFollow == null)
        {
            Debug.Log("create");
            objFollow = Instantiate(popupFollow, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_FOLLOW);
            cachedPopups.Add(POPUP_FOLLOW, objFollow);
        }
        objFollow.transform.parent = GameObject.Find("UI Root").transform;
        objFollow.transform.localScale = Vector3.one;
        objFollow.SetActive(true);
        objFollow.GetComponent<PopupFollow>().ShowFollow();
    }

    public void HideFollowPopup()
    {
        if (cachedPopups.ContainsKey(POPUP_FOLLOW))
        {
            cachedPopups[POPUP_FOLLOW].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_FOLLOW]);
            cachedPopups[POPUP_FOLLOW].SetActive(false);
        }
    }

    public bool IsFollowPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_FOLLOW) && cachedPopups[POPUP_FOLLOW] != null)
        {
            return cachedPopups[POPUP_FOLLOW].activeInHierarchy;
        }
        return false;
    }

    public void ShowFriendsRequest()
    {
        GameObject objFriendsRequest;
        if (!cachedPopups.TryGetValue(POPUP_FRIENDS_REQUEST, out objFriendsRequest) || objFriendsRequest == null)
        {
            objFriendsRequest = Instantiate(popupFriendsRequest, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_FRIENDS_REQUEST);
            cachedPopups.Add(POPUP_FRIENDS_REQUEST, objFriendsRequest);
        }
        objFriendsRequest.transform.parent = GameObject.Find("UI Root").transform;
        objFriendsRequest.transform.localScale = Vector3.one;
        objFriendsRequest.SetActive(true);
    }

    public void HideFriendsRequest()
    {
        if (cachedPopups.ContainsKey(POPUP_FRIENDS_REQUEST))
        {
            cachedPopups[POPUP_FRIENDS_REQUEST].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_FRIENDS_REQUEST]);
            cachedPopups[POPUP_FRIENDS_REQUEST].SetActive(false);
        }
    }

    public bool IsFriendsRequestPopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_FRIENDS_REQUEST) && cachedPopups[POPUP_FRIENDS_REQUEST] != null)
        {
            return cachedPopups[POPUP_FRIENDS_REQUEST].activeInHierarchy;
        }
        return false;
    }


    public void ShowProfilePopup(Player player = null)
    {
        GameObject objProfile = null;
        if (!cachedPopups.TryGetValue(POPUP_PROFILE, out objProfile) || objProfile == null)
        {
            objProfile = Instantiate(profilePopup, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_PROFILE);
            cachedPopups.Add(POPUP_PROFILE, objProfile);
        }

        objProfile.transform.parent = GameObject.Find("UI Root").transform;
        objProfile.transform.localScale = Vector3.one;
        objProfile.SetActive(true);
        if (player != null)
        {
            MessageDispatcher.SendMessage(gameObject, EventName.LoadProfile.LoadProfile.ToString(), player, 0);
        }
        else
        {
            MessageDispatcher.SendMessage(EventName.LoadProfile.LoadProfileLocal.ToString());
        }
    }

    public void HideProfilePopup()
    {
        if (cachedPopups.ContainsKey(POPUP_PROFILE) && cachedPopups[POPUP_PROFILE] != null)
        {
            cachedPopups[POPUP_PROFILE].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_PROFILE]);
            cachedPopups[POPUP_PROFILE].SetActive(false);
        }
    }

    public bool IsProfilePopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_PROFILE) && cachedPopups[POPUP_PROFILE] != null)
        {
            return cachedPopups[POPUP_PROFILE].activeInHierarchy;
        }
        return false;
    }

    public void ShowProfilePopupOnTop(Player player = null)
    {
        Debug.Log("ShowProfilePopupOnTop");
        GameObject objProfile = null;
        if (!cachedPopups.TryGetValue(POPUP_PROFILE_ON_TOP, out objProfile) || objProfile == null)
        {
            objProfile = Instantiate(profilePopupOnTop, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_PROFILE_ON_TOP);
            cachedPopups.Add(POPUP_PROFILE_ON_TOP, objProfile);
        }

        objProfile.transform.parent = GameObject.Find("UI Root").transform;
        objProfile.transform.localScale = Vector3.one;
        objProfile.SetActive(true);
        if (player != null)
        {
            MessageDispatcher.SendMessage(gameObject, EventName.LoadProfile.LoadProfile.ToString(), player, 0);
        }
        else
        {
            MessageDispatcher.SendMessage(EventName.LoadProfile.LoadProfileLocal.ToString());
        }
    }

    public void HideProfilePopupOnTop()
    {
        if (cachedPopups.ContainsKey(POPUP_PROFILE_ON_TOP) && cachedPopups[POPUP_PROFILE_ON_TOP] != null)
        {
            cachedPopups[POPUP_PROFILE_ON_TOP].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_PROFILE_ON_TOP]);
            cachedPopups[POPUP_PROFILE_ON_TOP].SetActive(false);
        }
    }

    public bool IsProfilePopupOnTopActive()
    {
        if (cachedPopups.ContainsKey(POPUP_PROFILE_ON_TOP) && cachedPopups[POPUP_PROFILE_ON_TOP] != null)
        {
            return cachedPopups[POPUP_PROFILE_ON_TOP].activeInHierarchy;
        }
        return false;
    }

    string tempToken;
    int tempFlag;
    int tempGold;

    GameObject objInviteButton;

    IEnumerator coroutine30s;

    IEnumerator Wait30s()
    {
        yield return new WaitForSecondsRealtime(30);
        HideInvitePopup();
        HideInviteButton();
    }

    public void ShowInviteButton(string token, Player player, int flag, int gold, int type = CSFindOpponent.TYPE_PVP)
    {
        Debug.Log("ShowInviteButton");
        Debug.Log("flag : " + flag);
        if (flag == 0 && SceneManager.GetActiveScene().name != "Home" && SceneManager.GetActiveScene().name != "SelectLevel")
        {
            return;
        }
        if (flag == 0)
        {
            //create new
            objInviteButton = Instantiate(inviteButton, Vector3.zero, Quaternion.identity);
            objInviteButton.transform.parent = GameObject.Find("UI Root").transform;
            objInviteButton.transform.localScale = Vector3.one;
            objInviteButton.transform.localPosition = Vector3.zero;

            tempToken = token;
            cachedPlayer = player;
            tempFlag = flag;
            tempGold = gold;
            CachePvp.typePVP = type;
            if (coroutine30s != null)
            {
                StopCoroutine(coroutine30s);
            }
            coroutine30s = Wait30s();
            StartCoroutine(coroutine30s);
        }
        else if (flag == 1)
        {
            HideInviteButton();
            if (IsInvitePopupActive())
            {
                HideInvitePopup();
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.have_been_outrun, false);
            }
        }
    }

    public void DestroyInviteButton()
    {
        if (objInviteButton != null)
        {
            Destroy(objInviteButton);
            objInviteButton = null;
            if (coroutine30s != null)
            {
                StopCoroutine(coroutine30s);
            }
        }
    }

    public void HideInviteButton()
    {
        if (objInviteButton != null)
        {
            Destroy(objInviteButton);
            objInviteButton = null;
        }
        if (coroutine30s != null)
        {
            StopCoroutine(coroutine30s);
        }
    }

    string tempTokenTournament;
    int tempFlagTournament;
    int tempGoldTournament;

    GameObject objInviteButtonTournament;

    IEnumerator coroutine30sTournament;

    IEnumerator Wait30sTournament()
    {
        yield return new WaitForSecondsRealtime(30);
        HideInvitePopupTournament();
        HideInviteButtonTournament();
    }

    public void ShowInviteButtonTournament(string token, Player player, int flag, int gold)
    {
        Debug.Log("ShowInviteButtonTournament");
        Debug.Log("flag : " + flag);
        if (flag == 0 && SceneManager.GetActiveScene().name != "Home" && SceneManager.GetActiveScene().name != "SelectLevel")
        {
            return;
        }
        if (flag == 0)
        {
            //create new
            objInviteButtonTournament = Instantiate(inviteButtonTournament, Vector3.zero, Quaternion.identity);
            objInviteButtonTournament.transform.parent = GameObject.Find("UI Root").transform;
            objInviteButtonTournament.transform.localScale = Vector3.one;
            objInviteButtonTournament.transform.localPosition = Vector3.zero;

            tempTokenTournament = token;
            cachedPlayer = player;
            tempFlagTournament = flag;
            tempGoldTournament = gold;
            CachePvp.typePVP = CSFindOpponent.TYPE_MEGA;
            if (coroutine30sTournament != null)
            {
                StopCoroutine(coroutine30sTournament);
            }
            coroutine30sTournament = Wait30sTournament();
            StartCoroutine(coroutine30sTournament);
        }
        else if (flag == 1)
        {
            HideInviteButtonTournament();
            if (IsInvitePopupTournamentActive())
            {
                HideInvitePopupTournament();
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.have_been_outrun, false);

            }
        }
    }

    public void DestroyInviteButtonTournament()
    {
        if (objInviteButtonTournament != null)
        {
            Destroy(objInviteButtonTournament);
            objInviteButtonTournament = null;
            if (coroutine30sTournament != null)
            {
                StopCoroutine(coroutine30sTournament);
            }
        }
    }

    public void HideInviteButtonTournament()
    {
        if (objInviteButtonTournament != null)
        {
            Destroy(objInviteButtonTournament);
            objInviteButtonTournament = null;
        }
        if (coroutine30sTournament != null)
        {
            StopCoroutine(coroutine30sTournament);
        }
    }

    public void ShowInvitePopup(int typeInvite = CachePvp.TYPE_PVP, int gold = 0) //0: pvp, 1: friend, 2: mega tournament, 3: coop
    {
        GameObject objInvite = null;
        if (!cachedPopups.TryGetValue(POPUP_INVITE, out objInvite) || objInvite == null)
        {
            objInvite = Instantiate(invitePopup, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_INVITE);
            cachedPopups.Add(POPUP_INVITE, objInvite);
        }

        objInvite.transform.parent = GameObject.Find("UI Root").transform;
        objInvite.transform.localScale = Vector3.one;
        objInvite.SetActive(true);
        if (typeInvite == CachePvp.TYPE_PVP_FRIENDS)
        {
            tempToken = "";
            tempGold = 0;
        }
        if (typeInvite == CachePvp.TYPE_2v2)
        {
            objInvite.GetComponent<PopupPVPInvite>().SetInformation(roomId2v2, cachedPlayer, tempGold2v2, typeInvite, CachePvp.typePVP);
        }
        else
        {
            if (typeInvite == CachePvp.TYPE_FRIEND_2v2)
            {
                tempGold = gold;
            }
            objInvite.GetComponent<PopupPVPInvite>().SetInformation(tempToken, cachedPlayer, tempGold, typeInvite, CachePvp.typePVP);
        }
    }

    public void HideInvitePopup(bool fromPopup = false)
    {
        if (cachedPopups.ContainsKey(POPUP_INVITE) && cachedPopups[POPUP_INVITE] != null)
        {
            cachedPopups[POPUP_INVITE].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_INVITE]);
            cachedPopups[POPUP_INVITE].SetActive(false);
        }
        if (coroutine30s != null)
        {
            StopCoroutine(coroutine30s);
        }
        if (fromPopup)
        {
            if (typeInvite != CachePvp.TYPE_PVP_FRIENDS)
            {
                if (CachePvp.typePVP == CSFindOpponent.TYPE_PVP)
                {
                    new CSConfirmOpponentFindingNotify(1, CachePvp.numberCoin, tempToken, CacheGame.GetSpaceShipUserSelected(), CachePvp.typePVP).Send();
                }
                else if (CachePvp.typePVP == CSFindOpponent.TYPE_MEGA)
                {
                    new CSConfirmOpponentFindingNotify(1, CachePvp.numberCoin, tempToken, CacheGame.GetSpaceShipUserSelected(), CachePvp.typePVP).Send();
                }
                else if (CachePvp.typePVP == CSFindOpponent.TYPE_2V2)
                {
                    if (typeInvite == CachePvp.TYPE_FRIEND_2v2)
                    {
                        new CSCoopPvPConfirm(CSCoopPvPConfirm.DENIED, CachePvp.Code, CachePvp.numberCoin).Send();
                    }
                    else
                    {
                        new CSCoopPvPNotifyConfirm(roomId2v2, CSCoopPvPConfirm.DENIED, CachePvp.numberCoin, CacheGame.GetSpaceShipUserSelected(), PvPDataHelper.Instance.GetMyWaveCacheIndex(CachePvp.typePVP), PvPDataHelper.Instance.GetMyDataString(CachePvp.typePVP)).Send();
                    }
                }
            }
            else
            {
                new CSPvPConfirm(cachedPlayer.code, CSPvPConfirm.DENIED, CacheGame.GetSpaceShipUserSelected(), PvPDataHelper.Instance.GetMyWaveCacheIndex(), PvPDataHelper.Instance.GetMyDataString()).Send();
            }
        }
    }

    public bool IsInvitePopupActive()
    {
        if (cachedPopups.ContainsKey(POPUP_INVITE) && cachedPopups[POPUP_INVITE] != null)
        {
            return cachedPopups[POPUP_INVITE].activeInHierarchy;
        }
        return false;
    }

    public void ShowInvitePopupTournament()
    {
        GameObject objInvite = null;
        if (!cachedPopups.TryGetValue(POPUP_INVITE_TOURNAMENT, out objInvite) || objInvite == null)
        {
            objInvite = Instantiate(invitePopupTournament, Vector3.zero, Quaternion.identity);
            cachedPopups.Remove(POPUP_INVITE_TOURNAMENT);
            cachedPopups.Add(POPUP_INVITE_TOURNAMENT, objInvite);
        }
        objInvite.transform.parent = GameObject.Find("UI Root").transform;
        objInvite.transform.localScale = Vector3.one;
        objInvite.SetActive(true);
        objInvite.GetComponent<PopupPVPInvite>().SetInformation(tempTokenTournament, cachedPlayer, tempGoldTournament, CachePvp.TYPE_MEGA_TOURNAMENT, CSFindOpponent.TYPE_MEGA);
    }

    public void HideInvitePopupTournament()
    {
        if (cachedPopups.ContainsKey(POPUP_INVITE_TOURNAMENT) && cachedPopups[POPUP_INVITE_TOURNAMENT] != null)
        {
            cachedPopups[POPUP_INVITE_TOURNAMENT].transform.parent = null;
            DontDestroyOnLoad(cachedPopups[POPUP_INVITE_TOURNAMENT]);
            cachedPopups[POPUP_INVITE_TOURNAMENT].SetActive(false);
        }
        if (coroutine30sTournament != null)
        {
            StopCoroutine(coroutine30sTournament);
        }
    }

    public bool IsInvitePopupTournamentActive()
    {
        if (cachedPopups.ContainsKey(POPUP_INVITE_TOURNAMENT) && cachedPopups[POPUP_INVITE_TOURNAMENT] != null)
        {
            return cachedPopups[POPUP_INVITE_TOURNAMENT].activeInHierarchy;
        }
        return false;
    }

    string roomId2v2;
    int tempFlag2v2;
    int tempGold2v2;

    GameObject objInviteButton2v2;

    IEnumerator coroutine30s2v2;

    IEnumerator Wait30s2v2()
    {
        yield return new WaitForSecondsRealtime(30);
        HideInvitePopup();
        HideInviteButton2v2();
    }

    public void ShowInviteButton2v2(string roomId, List<CoopPvPPlayerData> players, int flag, int gold)
    {
        Debug.Log("ShowInviteButton2v2");
        Debug.Log("flag : " + flag);
        if (flag == 0 && SceneManager.GetActiveScene().name != "Home" && SceneManager.GetActiveScene().name != "SelectLevel")
        {
            return;
        }
        if (flag == 0)
        {
            //create new
            objInviteButton2v2 = Instantiate(inviteButton2v2, Vector3.zero, Quaternion.identity);
            objInviteButton2v2.transform.parent = GameObject.Find("UI Root").transform;
            objInviteButton2v2.transform.localScale = Vector3.one;
            objInviteButton2v2.transform.localPosition = Vector3.zero;

            roomId2v2 = roomId;
            listPlayersInvite = players;
            tempFlag2v2 = flag;
            tempGold2v2 = gold;
            CachePvp.typePVP = CSFindOpponent.TYPE_2V2;
            if (coroutine30s2v2 != null)
            {
                StopCoroutine(coroutine30s2v2);
            }
            coroutine30s2v2 = Wait30s2v2();
            StartCoroutine(coroutine30s2v2);
        }
        else if (flag == 1)
        {
            HideInvitePopup();
            HideInviteButton2v2();
        }
    }

    public void DestroyInviteButton2v2()
    {
        if (objInviteButton2v2 != null)
        {
            Destroy(objInviteButton2v2);
            objInviteButton2v2 = null;
            if (coroutine30s2v2 != null)
            {
                StopCoroutine(coroutine30s2v2);
            }
        }
    }

    public void HideInviteButton2v2()
    {
        if (objInviteButton2v2 != null)
        {
            Destroy(objInviteButton2v2);
            objInviteButton2v2 = null;
        }
        if (coroutine30s2v2 != null)
        {
            StopCoroutine(coroutine30s2v2);
        }
    }

    bool _goToPVP = false;

    public bool goToPVP
    {
        get
        {
            return _goToPVP;
        }
        set
        {
            _goToPVP = value;
        }
    }

    bool _goToTournament = false;

    public bool goToTournament
    {
        get
        {
            return _goToTournament;
        }
        set
        {
            _goToTournament = value;
        }
    }

    bool _goTo2v2 = false;

    public bool goTo2v2
    {
        get
        {
            return _goTo2v2;
        }
        set
        {
            _goTo2v2 = value;
        }
    }

    public Player cachedPlayer { get; set; }

    public List<CoopPvPPlayerData> listPlayersInvite { get; set; }

    public void GoToPVP()
    {
        goToPVP = true;
        if (PvPUIManager.Instance == null)
        {
            SceneManager.LoadScene("PVPMain");
        }
        else
        {
            PvPUIManager.Instance.popupMainPvP.gameObject.SetActive(false);
            PvPUIManager.Instance.popupFindPvP.gameObject.SetActive(true);
        }
    }
}
