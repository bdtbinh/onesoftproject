﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class PlayerDataUtil
{
    public static PlayerProfileData GetPlayerProfileData()
    {
        PlayerProfileData player = new PlayerProfileData();

        player.eventResourceSummer_Holiday_2019 = MinhCacheGame.GetEventResources(EventItemType.IceCream_4_2019.ToString());

        player.eventMissionProgressSummer_Holiday_2019 = new int[4];

        for (int i = 0; i < player.eventMissionProgressSummer_Holiday_2019.Length; i++)
        {
            player.eventMissionProgressSummer_Holiday_2019[i] = MinhCacheGame.GetEventMissionProgress(i + 1);
        }

        player.listPromoteEventClaimed = new int[4];
        for (int i = 0; i < 4; i++)
        {
            player.listPromoteEventClaimed[i] = MinhCacheGame.IsPromoteEventClaimed(i + 1, EventName.PROMOTE_EVENT_NAME) ? 1 : 0;
        }

        player.numberItemLife = CacheGame.GetTotalItemLife();
        player.numberItemEmp = CacheGame.GetTotalItemActiveSkill();
        player.numberItemPowerUp = CacheGame.GetTotalItemPowerUp();
        player.buyInfinityPack = CacheGame.GetBuyInfinityPack();

        Array airCrafts = Enum.GetValues(typeof(AircraftTypeEnum));
        player.listAirCraft = new int[airCrafts.Length];
        player.listLevelPlane = new int[airCrafts.Length];
        player.listRankPlane = new int[airCrafts.Length];
        player.listSpaceShipCard = new int[airCrafts.Length];
        player.listAircraftUsedTimePerDay = new int[airCrafts.Length];
        for (int i = 0; i < airCrafts.Length; i++)
        {
            int index = (int)airCrafts.GetValue(i);
            player.listAirCraft[i] = CacheGame.GetSpaceShipIsUnlocked(index);
            player.listLevelPlane[i] = CacheGame.GetSpaceShipLevel((AircraftTypeEnum)index);
            player.listRankPlane[i] = (int)CacheGame.GetSpaceShipRank((AircraftTypeEnum)index);
            player.listSpaceShipCard[i] = MinhCacheGame.GetSpaceShipCards((AircraftTypeEnum)index);
            player.listAircraftUsedTimePerDay[i] = MinhCacheGame.GetAircraftUsedTimePerDay((AircraftTypeEnum)index);
        }
        player.spaceShipGeneralCard = MinhCacheGame.GetSpaceShipGeneralCards();

        Array wingmans = Enum.GetValues(typeof(WingmanTypeEnum));
        player.listWingman = new int[wingmans.Length - 1];
        player.listLevelWingmanNew = new int[wingmans.Length - 1];
        player.listRankWingman = new int[wingmans.Length - 1];
        player.listWingmanCard = new int[wingmans.Length - 1];
        for (int i = 1; i < wingmans.Length; i++)
        {
            int index = (int)wingmans.GetValue(i);
            player.listWingman[i - 1] = CacheGame.GetWingManIsUnlocked(index);
            player.listLevelWingmanNew[i - 1] = CacheGame.GetWingmanLevel((WingmanTypeEnum)index);
            player.listRankWingman[i - 1] = (int)CacheGame.GetWingmanRank((WingmanTypeEnum)index);
            player.listWingmanCard[i - 1] = MinhCacheGame.GetWingmanCards((WingmanTypeEnum)index);
        }
        player.wingmanGeneralCard = MinhCacheGame.GetWingmanGeneralCards();

        Array wings = Enum.GetValues(typeof(WingTypeEnum));
        player.listWing = new int[wings.Length - 1];
        player.listLevelWing = new int[wings.Length - 1];
        player.listRankWing = new int[wings.Length - 1];
        player.listWingCard = new int[wings.Length - 1];
        for (int i = 1; i < wings.Length; i++)
        {
            int index = (int)wings.GetValue(i);
            player.listWing[i - 1] = CacheGame.GetWingIsUnlocked(index);
            player.listLevelWing[i - 1] = CacheGame.GetWingLevel((WingTypeEnum)index);
            player.listRankWing[i - 1] = (int)CacheGame.GetWingRank((WingTypeEnum)index);
            player.listWingCard[i - 1] = MinhCacheGame.GetWingCards((WingTypeEnum)index);
        }
        player.wingGeneralCard = MinhCacheGame.GetWingGeneralCards();

        long unixTimestamp = (long)(MinhCacheGame.GetCountdownVideoTime().Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        player.countdownVideoTime = unixTimestamp;
        unixTimestamp = (long)(MinhCacheGame.GetCountdownFreeTime().Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        player.countdownFreeTime = unixTimestamp;
        player.spinTickets = MinhCacheGame.GetSpinTickets();
        player.lastDayUseVideoSpin = CacheGame.GetDayUseVideoLuckyWheel();
        player.lastDayUseFreeSpin = CacheGame.GetDayUseFreeLuckyWheel();


        //Array secondaryWeapons = Enum.GetValues(typeof(SecondaryWeaponTypeEnum));
        //player.listSecondaryWeapon = new int[secondaryWeapons.Length];

        //for (int i = 0; i < secondaryWeapons.Length; i++)
        //{
        //    int index = (int)secondaryWeapons.GetValue(i);
        //    player.listSecondaryWeapon[i] = CacheGame.GetSecondaryWeaponIsUnlocked(index);
        //}
        player.removeAds = CacheGame.GetUsedInapp();
        player.currentLevel3Mode = new int[3];
        player.currentLevel3Mode[0] = CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL);
        player.currentLevel3Mode[1] = CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD);
        player.currentLevel3Mode[2] = CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL);

        //like, join, insta follow, twitter follow
        player.listSocial = new int[9];
        player.listSocial[0] = SocialManager.Instance.FBIsAlreadyLike() ? 1 : 0;
        player.listSocial[1] = SocialManager.Instance.FBIsAlreadyJoinGroup() ? 1 : 0;
        player.listSocial[2] = SocialManager.Instance.InstagramIsAlreadyFollow() ? 1 : 0;
        player.listSocial[3] = SocialManager.Instance.TwitterIsAlreadyFollow() ? 1 : 0;

        //1, 5, 10, 20 friends
        player.listSocial[4] = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(1) ? 1 : 0;
        player.listSocial[5] = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(5) ? 1 : 0;
        player.listSocial[6] = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(10) ? 1 : 0;
        player.listSocial[7] = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(20) ? 1 : 0;
        player.listSocial[8] = SocialManager.Instance.FBGetNumberOfFriends();

        player.dateShareSocial = MinhCacheGame.GetFacebookShareDate().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

        player.vipPack = CacheGame.GetPurchasedVIPPack();
        player.starterPack = CacheGame.GetPurchasedStarterPack();
        player.lastDayOpenGame = CacheGame.GetLastDayOpenGame();

        player.versionMinhDDV = MinhCacheGame.GetVersion();
        player.hasConvertedNewDataMinhDDV = MinhCacheGame.IsAlreadyDataConverted1() ? 1 : 0;

        player.selectedShip = CacheGame.GetSpaceShipUserSelected();
        player.selectedLeftWingman = CacheGame.GetWingManLeftUserSelected();
        player.selectedRightWingman = CacheGame.GetWingManRightUserSelected();
        player.selectedWing = CacheGame.GetWingUserSelected();
        player.selectedShipTournament = CacheGame.GetSpaceShipTournament();

        player.alreadyPurchasePremiumPack = MinhCacheGame.IsAlreadyPurchasePremiumPack() ? 1 : 0;
        unixTimestamp = (long)(MinhCacheGame.GetPremiumPackTimeExpiredTime().Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        player.premiumPackExpiredTime = unixTimestamp;

        player.listDailyQuest = new int[GameContext.TOTAL_TYPE_DAILYQUEST * 3];
        string nameItem = "";
        for (int i = 1; i <= GameContext.TOTAL_TYPE_DAILYQUEST; i++)
        {
            nameItem = ((GameContext.TypeItemDailyQuest)i).ToString();
            player.listDailyQuest[3 * (i - 1)] = CacheGame.GetItemDailyQuestChooseShow(nameItem);
            player.listDailyQuest[3 * (i - 1) + 1] = CacheGame.GetCompleteItemDailyQuest(nameItem);
            player.listDailyQuest[3 * (i - 1) + 2] = CacheGame.GetNumberDailyQuestAchieved(nameItem);
        }

        player.listAchievements = new int[GameContext.TOTAL_TYPE_ACHIEVEMENTS * 3];
        nameItem = "";
        for (int i = 1; i <= GameContext.TOTAL_TYPE_ACHIEVEMENTS; i++)
        {
            nameItem = ((GameContext.TypeItemAchievements)i).ToString();
            player.listAchievements[3 * (i - 1)] = CacheGame.GetCompleteAllLevelAchievement(nameItem);
            player.listAchievements[3 * (i - 1) + 1] = CacheGame.GetCurrentLevelAchievement(nameItem);
            player.listAchievements[3 * (i - 1) + 2] = CacheGame.GetNumberAchievementAchieved(nameItem);
        }

        player.listTypeShowGiftBoxDailyQuest = new int[5];
        for (int i = 0; i < 5; i++)
        {
            player.listTypeShowGiftBoxDailyQuest[i] = CacheGame.GetTypeShowGiftBoxDailyQuest(i + 1);
        }
        player.dateOpenDailyQuest = CacheGame.GetDateOpenDailyQuest();
        player.numUseHourlyRewardOneDay = CacheGame.GetNumUseHourlyRewardOneDay();
        player.numCardGetToday = CacheGame.GetNumCardGetToday();
        player.numGemGetToday = CacheGame.GetNumGemGetToday();
        player.numEnergyGetToday = CacheGame.GetNumEnergyGetToday();
        player.numUseVideoEndGameOneDay = CacheGame.GetNumUseVideoEndGameOneDay();

        player.lastDayOf30Days = MinhCacheGame.GetLastDayOf30Days();
        player.rewardIndexDayOf30Days = MinhCacheGame.GetRewardIndexOf30Days();

        player.starsChestNormal = new int[GameContext.TOTAL_LEVEL * 3 / 7];
        for (int i = 0; i < player.starsChestNormal.Length; i++)
        {
            player.starsChestNormal[i] = MinhCacheGame.IsStarChestClaimed(Mathf.CeilToInt((i + 1) / 3f), GameContext.DIFFICULT_NOMAL, i % 3 + 1) ? 1 : 0;
        }

        player.starsChestHard = new int[GameContext.TOTAL_LEVEL * 3 / 7];
        for (int i = 0; i < player.starsChestHard.Length; i++)
        {
            player.starsChestHard[i] = MinhCacheGame.IsStarChestClaimed(Mathf.CeilToInt((i + 1) / 3f), GameContext.DIFFICULT_HARD, i % 3 + 1) ? 1 : 0;
        }

        player.starsChestHell = new int[GameContext.TOTAL_LEVEL * 3 / 7];
        for (int i = 0; i < player.starsChestHell.Length; i++)
        {
            player.starsChestHell[i] = MinhCacheGame.IsStarChestClaimed(Mathf.CeilToInt((i + 1) / 3f), GameContext.DIFFICULT_HELL, i % 3 + 1) ? 1 : 0;
        }

        player.starsNormal = new int[CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL) * 3];
        player.starsHard = new int[CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD) * 3];
        player.starsHell = new int[CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL) * 3];

        for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL); i++)
        {
            for (int j = 1; j <= 3; j++)
            {
                player.starsNormal[(i - 1) * 3 + (j - 1)] = CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal);
            }
        }

        for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD); i++)
        {
            for (int j = 1; j <= 3; j++)
            {
                player.starsHard[(i - 1) * 3 + (j - 1)] = CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HARD, GameContext.LevelCampaignMode.Normal);
            }
        }

        for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL); i++)
        {
            for (int j = 1; j <= 3; j++)
            {
                player.starsHell[(i - 1) * 3 + (j - 1)] = CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HELL, GameContext.LevelCampaignMode.Normal);
            }
        }

        player.starsNormalExtra = new int[GameContext.TOTAL_LEVEL / 7 * 2 * 3];
        player.starsHardExtra = new int[GameContext.TOTAL_LEVEL / 7 * 2 * 3];
        player.starsHellExtra = new int[GameContext.TOTAL_LEVEL / 7 * 2 * 3];

        for (int i = 0, k = 0; k < GameContext.TOTAL_LEVEL; i++)
        {
            if (i % 2 == 0)
            {
                k += 3;
            }
            else
            {
                k += 4;
            }
            for (int j = 1; j <= 3; j++)
            {
                player.starsNormalExtra[i * 3 + (j - 1)] = CacheGame.GetNumStarLevel(k, j, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Extra);
            }
        }

        for (int i = 0, k = 0; k < GameContext.TOTAL_LEVEL; i++)
        {
            if (i % 2 == 0)
            {
                k += 3;
            }
            else
            {
                k += 4;
            }
            for (int j = 1; j <= 3; j++)
            {
                player.starsHardExtra[i * 3 + (j - 1)] = CacheGame.GetNumStarLevel(k, j, GameContext.DIFFICULT_HARD, GameContext.LevelCampaignMode.Extra);
            }
        }

        for (int i = 0, k = 0; k < GameContext.TOTAL_LEVEL; i++)
        {
            if (i % 2 == 0)
            {
                k += 3;
            }
            else
            {
                k += 4;
            }
            for (int j = 1; j <= 3; j++)
            {
                player.starsHellExtra[i * 3 + (j - 1)] = CacheGame.GetNumStarLevel(k, j, GameContext.DIFFICULT_HELL, GameContext.LevelCampaignMode.Extra);
            }
        }

        player.passedLevelExtraNormal = new int[GameContext.TOTAL_LEVEL / 7 * 2];
        player.passedLevelExtraHard = new int[GameContext.TOTAL_LEVEL / 7 * 2];
        player.passedLevelExtraHell = new int[GameContext.TOTAL_LEVEL / 7 * 2];

        for (int i = 0, k = 0; k < GameContext.TOTAL_LEVEL; i++)
        {
            if (i % 2 == 0)
            {
                k += 3;
            }
            else
            {
                k += 4;
            }
            player.passedLevelExtraNormal[i] = CacheGame.GetPassedLevelExtra(k, GameContext.DIFFICULT_NOMAL);
        }

        for (int i = 0, k = 0; k < GameContext.TOTAL_LEVEL; i++)
        {
            if (i % 2 == 0)
            {
                k += 3;
            }
            else
            {
                k += 4;
            }
            player.passedLevelExtraHard[i] = CacheGame.GetPassedLevelExtra(k, GameContext.DIFFICULT_HARD);
        }

        for (int i = 0, k = 0; k < GameContext.TOTAL_LEVEL; i++)
        {
            if (i % 2 == 0)
            {
                k += 3;
            }
            else
            {
                k += 4;
            }
            player.passedLevelExtraHell[i] = CacheGame.GetPassedLevelExtra(k, GameContext.DIFFICULT_HELL);
        }

        player.starsChestExtraNormal = new int[GameContext.TOTAL_LEVEL / 7];
        for (int i = 0; i < player.starsChestExtraNormal.Length; i++)
        {
            player.starsChestExtraNormal[i] = MinhCacheGame.IsStarChestExtraClaimed(Mathf.CeilToInt(i + 1), GameContext.DIFFICULT_NOMAL) ? 1 : 0;
        }

        player.starsChestExtraHard = new int[GameContext.TOTAL_LEVEL / 7];
        for (int i = 0; i < player.starsChestExtraHard.Length; i++)
        {
            player.starsChestExtraHard[i] = MinhCacheGame.IsStarChestExtraClaimed(Mathf.CeilToInt(i + 1), GameContext.DIFFICULT_HARD) ? 1 : 0;
        }

        player.starsChestExtraHell = new int[GameContext.TOTAL_LEVEL / 7];
        for (int i = 0; i < player.starsChestExtraHell.Length; i++)
        {
            player.starsChestExtraHell[i] = MinhCacheGame.IsStarChestExtraClaimed(Mathf.CeilToInt(i + 1), GameContext.DIFFICULT_HELL) ? 1 : 0;
        }

        return player;
    }

    public static PlayerProfileData StringToObject(string data)
    {
        return JsonFx.Json.JsonReader.Deserialize<PlayerProfileData>(data);
    }

    public class PlayerProfileData
    {
        public int[] listPromoteEventClaimed;
        public long countdownVideoTime;
        public long countdownFreeTime;
        public int spinTickets;
        public int lastDayUseVideoSpin;
        public int lastDayUseFreeSpin;

        public int numberItemLife;
        public int numberItemPowerUp;
        public int numberItemEmp;
        public int buyInfinityPack;

        public int lastDayOpenGame;

        public int vipPack;
        public int starterPack;

        public int removeAds;
        public int[] currentLevel3Mode;

        public int[] listAirCraft;
        public int[] listWing;
        public int[] listWingman;

        public int[] listSocial;
        public string dateShareSocial;

        public int[] listLevelPlane;
        public int[] listLevelWing;
        public int[] listLevelWingmanNew;
        public int[] listRankPlane;
        public int[] listRankWing;
        public int[] listRankWingman;
        public int[] listSpaceShipCard;
        public int[] listWingCard;
        public int spaceShipGeneralCard;
        public int wingGeneralCard;
        public int[] listWingmanCard;
        public int wingmanGeneralCard;

        public int versionMinhDDV;
        public int hasConvertedNewDataMinhDDV;

        public int selectedShip;
        public int selectedLeftWingman;
        public int selectedRightWingman;
        public int selectedWing;
        public int selectedShipTournament;

        public int alreadyPurchasePremiumPack;
        public long premiumPackExpiredTime;

        public int[] listDailyQuest;

        public int[] listTypeShowGiftBoxDailyQuest;
        public int dateOpenDailyQuest;

        public int[] listAchievements;

        public int numUseHourlyRewardOneDay;
        public int numCardGetToday;
        public int numGemGetToday;
        public int numEnergyGetToday;
        public int numUseVideoEndGameOneDay;

        public int lastDayOf30Days;
        public int rewardIndexDayOf30Days;

        public int eventResourceSummer_Holiday_2019; //summer
        public int[] eventMissionProgressSummer_Holiday_2019; //summer

        public int[] listAircraftUsedTimePerDay;

        public int[] starsChestNormal;
        public int[] starsChestHard;
        public int[] starsChestHell;

        public int[] starsNormal;
        public int[] starsHard;
        public int[] starsHell;

        public int[] starsNormalExtra;
        public int[] starsHardExtra;
        public int[] starsHellExtra;

        public int[] passedLevelExtraNormal;
        public int[] passedLevelExtraHard;
        public int[] passedLevelExtraHell;

        public int[] starsChestExtraNormal;
        public int[] starsChestExtraHard;
        public int[] starsChestExtraHell;

        public override string ToString()
        {
            return JsonFx.Json.JsonWriter.Serialize(this);
        }
    }
}