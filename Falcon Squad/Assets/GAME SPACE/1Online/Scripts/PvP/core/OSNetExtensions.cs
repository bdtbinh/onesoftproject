﻿using Mp.Pvp;

public static class OSNetExtensions
{
    public static float GetWinrate(this Player player)
    {
        if (player.win == 0) return 0;
        else return (float)player.win * 100 / (player.win + player.lose);
    }
    public static string GetWinrateText(this Player player)
    {
        return player.GetWinrate().ToString("0.00") + "%";
    }
    public static float GetWinrate2v2(this Player player)
    {
        if (player.pvP2vs2SeasonData == null || player.pvP2vs2SeasonData.win == 0) return 0;
        else return (float)player.pvP2vs2SeasonData.win * 100 / (player.pvP2vs2SeasonData.win + player.pvP2vs2SeasonData.lose);
    }
    public static string GetWinrateText2v2(this Player player)
    {
        return player.GetWinrate2v2().ToString("0.00") + "%";
    }
    public static int GetBonus(this Player player)
    {
        return player.win % 10;
    }
    public static string FixedCountry(this Player player)
    {
        return player.country.ToLower().Substring(0, 2);
    }
}
