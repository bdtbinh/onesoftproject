﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Mp.Pvp;
using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;

public class PvpUtil
{
    public const int TOTAL_LEVEL = GameContext.TOTAL_LEVEL;
    public const int REQUIRE_LEVEL_PLAY_PVP = 6;
    public const int REQUIRE_LEVEL_PLAY_2V2 = 21;
    public const int REQUIRE_LEVEL_UNLOCK_PLAYERINFO = 5;
    public const int CAN_PLAY_PVP_VALUE = 1;
    public const int MAX_MEDAL = 149;
    public const string SERVER_URL_PVP = "http://35.188.16.223:9988/falcon-squad/";
    public const string LIST_SERVERS_URL_PVP_DDOS = "http://falconsquad.net:9988/falcon-squad/";
    public const string URL_TXT_INDEX_DDOS = "https://storage.googleapis.com/data.falconsquad.net/server.txt";

    public const int PVP_VERSION = 15;
    public const int PVP_2V2_VERSION = 2;

    public const string WHY_GOLD = "gold";
    public const string WHY_GEM = "gem";
    public const string WHY_CARD = "card";
    public const string WHY_LEVEL = "level";
    public const string WHY_OTHER = "other";
    public static long LevelValue
    {
        get
        {
            int sumStarNormal = 0;
            int maxLevelNormal = 0;
            int sumStarHard = 0;
            int maxLevelHard = 0;
            int sumStarHell = 0;
            int maxLevelHell = 0;

            for (int i = 1; i <= TOTAL_LEVEL; i++)
                for (int j = 1; j <= 3; j++)
                {
                    int ijStarNormal = CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal);
                    int ijStarHard = CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HARD, GameContext.LevelCampaignMode.Normal);
                    int ijStarHell = CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HELL, GameContext.LevelCampaignMode.Normal);

                    int ijStarNormalExtra = CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Extra);
                    int ijStarHardExtra = CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HARD, GameContext.LevelCampaignMode.Extra);
                    int ijStarHellExtra = CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HELL, GameContext.LevelCampaignMode.Extra);

                    sumStarNormal += ijStarNormal + ijStarNormalExtra;
                    sumStarHard += ijStarHard + ijStarHardExtra;
                    sumStarHell += ijStarHell + ijStarHellExtra;

                    if (ijStarNormal > 0)
                        maxLevelNormal = i;
                    if (ijStarHard > 0)
                        maxLevelHard = i;
                    if (ijStarHell > 0)
                        maxLevelHell = i;
                }

            maxLevelNormal++;
            maxLevelHard++;
            maxLevelHell++;

            return long.Parse(maxLevelNormal.ToString().PadLeft(3, '0') + sumStarNormal.ToString().PadLeft(3, '0')
            + maxLevelHard.ToString().PadLeft(3, '0') + sumStarHard.ToString().PadLeft(3, '0')
            + maxLevelHell.ToString().PadLeft(3, '0') + sumStarHell.ToString().PadLeft(3, '0')
            );
        }
    }

    public static int[] StarExtra(PlayerDataUtil.PlayerProfileData profile)
    {
        int[] result = new int[3];
        int cntNormal = 0;
        int cntHard = 0;
        int cntHell = 0;
        //if (profile.starsNormalExtra != null && profile.starsHardExtra != null && profile.starsHellExtra != null)
        //{
        //    for (int i = 0, k = 0; k < TOTAL_LEVEL; i++)
        //    {
        //        if (i % 2 == 0)
        //        {
        //            k += 3;
        //        }
        //        else
        //        {
        //            k += 4;
        //        }
        //        for (int j = 1; j <= 3; j++)
        //        {
        //            if (profile.starsNormalExtra[i * 3 + (j - 1)] == 1)
        //            {
        //                cntNormal++;
        //            }
        //            if (profile.starsHardExtra[i * 3 + (j - 1)] == 1)
        //            {
        //                cntHard++;
        //            }
        //            if (profile.starsHellExtra[i * 3 + (j - 1)] == 1)
        //            {
        //                cntHell++;
        //            }
        //        }
        //    }
        //}
        result[0] = cntNormal;
        result[1] = cntHard;
        result[2] = cntHell;
        return result;
    }

    public static int MaxStar(int level)
    {
        int max = 0;
        int[] temp = new int[TOTAL_LEVEL / 7 * 2];
        for (int i = 0, k = 0; k < TOTAL_LEVEL; i++)
        {
            if (i % 2 == 0)
            {
                k += 3;
            }
            else
            {
                k += 4;
            }
            temp[i] = k;
        }
        for (int i = 1; i <= TOTAL_LEVEL; i++)
        {
            max += 3;
            for (int j = 0; j < temp.Length; j++)
            {
                if (i == temp[j])
                {
                    max += 3;
                }
            }
        }
        return max;
    }

    public static int[] LevelValueDecode(long levelValue)
    {
        int[] result = new int[6];
        string levelValueStr = levelValue.ToString().PadLeft(18, '0');
        for (int i = 0; i < 6; i++)
            result[i] = int.Parse(levelValueStr.Substring(3 * i, 3));
        return result;
    }

    public static void ShowRegisterPopup()
    {

    }

    //why = vị trí gọi hàm send
    //where = lý do thay đổi 
    public static void SendUpdatePlayer(string why = "", string where = "")
    {
        //Debug.LogError("GameContext.isInGameCampaign:==" + GameContext.isInGameCampaign);
        if (CachePvp.isForceUpdate)
        {
            return;
        }
        if (!GameContext.isInGameCampaign)
        {
            PlayerPrefs.Save();
            //if (CachePvp.Code.Equals("15009640"))
            //{
            //    CachePvp.Name = "NamNX";
            //}
            if (!string.IsNullOrEmpty(CachePvp.Code) && CachePvp.isConnectServer)
            {
                new CSUpdatePlayer(CachePvp.MyInfo, CachePvp.deviceId, why, where).Send();
            }
        }
    }

    public static void OnSetTotalCoin(int curCoin, int coin, string whyLogFirebase, string whereLogFirebase)
    {

        if (curCoin / Constant.BETTING_AMOUNT_MIN != coin / Constant.BETTING_AMOUNT_MIN)
        {
            SendUpdatePlayer("OnSetTotalCoin", whyLogFirebase + "--" + whereLogFirebase);

            if (CachePvp.GetGold() < CachePvp.goldBetFromInvitation)
            {
                PopupManager.Instance.HideInvitePopup();
                PopupManager.Instance.HideInviteButton();
            }
        }
    }

    public static Dictionary<int, double> dicLevelToCoeff = new Dictionary<int, double>();


    const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";

    public static string getRandomString()
    {
        string myString = "";
        for (int i = 0; i < 8; i++)
        {
            myString += glyphs[Random.Range(0, glyphs.Length)];
        }
        return myString;
    }
}