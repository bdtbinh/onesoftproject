namespace Mp.Pvp
{
    public class Events
    {
        public const string SESSION_INFORMATION = "session_information";
        public const string RANKING = "ranking";
        public const string MY_RANK = "my ranking";
        public const string LEADERBOARD = "leaderboard";
        public const string LIST_ACHIEVEMENT = "list achievement";
        public const string ACHIEVEMENT = "achievement";
        public const string ACHIEVEMENT_INFO = "achievement info";
        public const string ACHIEVEMENT_COMPLETE = "achievement complete";
        public const string LIST_FRIEND_REQUEST = "list friend request";
        public const string FRIEND = "friend";
        public const string FIND_OPPONENT = "find_opponent";
        public const string OPPONENT_FINDING_NOTIFY = "opponent_finding_notify";
        public const string CONFIRM_OPPONENT_FINDING_NOTIFY = "confirm_opponent_finding_notify";
        public const string START_GAME = "start_game";
        public const string END_GAME = "end_game";
        public const string WAIT_FOR_READY = "wait_for_ready";
        public const string PLAYER_DIED = "player_died";
        public const string PLAYER_WIN = "player_win";
        public const string PLAYER_READY = "player_ready";
        public const string PLAYER_OUT = "player_out";
        public const string PLAYER_EXIT = "player_exit";
        public const string SCORE = "score";
        public const string ROOM_INFO = "room_info";
        public const string CLIENT_CONFIG = "client_config";
        public const string VERSION_CONFIG = "version_config";
        public const string CANCEL_FIND_OPPONENT = "cancel_find_opponent";
        public const string NETWORK_PROBLEM = "network problem";
        public const string GET_TOKEN = "get_token";
        public const string VALIDATE_TOKEN = "validate_token";
        public const string FACEBOOK_CONNECT = "facebook_connect";
        public const string GAMECENTER_CONNECT = "app_center_connect";
        public const string FACEBOOK_FRIENDS = "facebook_friends";
        public const string GAMECENTER_FRIENDS = "app_center_friends";
        public const string FACEBOOK_DATA_SYNC = "facebook_data_sync";
        public const string CREATE_ROOM = "create_room";
        public const string JOIN_ROOM = "join_room";
        public const string LEAVE_ROOM = "leave_room";
        public const string DISPLAY_NAME = "display_name";
        public const string ROOM_DESTROY = "room_destroy";
        public const string REPLAY = "replay";
        public const string CHANGE_PROFILE = "change_profile";
        public const string UPDATE_RESOURCES = "update_resources";
        public const string LOCAL_LEVEL_VALUE = "local_level_value";
        public const string UPDATE_PLAYER = "update_player";
        public const string TOAST_MESSAGE = "toast_message";
        public const string NOTIFY_MESSAGE = "notify_message";
        public const string CAN_PLAY_PVP = "can_play_pvp";
        public const string VALIDATE_DEVICE_ID = "validate_device_id";
        public const string UPDATE_DEVICE_ID = "update_device_id";
        public const string VALIDATE_FACEBOOK_ID = "validate_facebook_id";
        public const string VALIDATE_GAMECENTER_ID = "validate_app_center_id";
        public const string PVP_TIME_REMAINING = "pvp_time_remaining";
        public const string SHOP = "shop";
        public const string HOME_OK = "home_ok";
        public const string FALCON_MAIL = "falcon_mail";
        public const string CLAN_MAIL = "clan_mail_notify";
        public const string CS_CHANGE_MONEY = "cs_change_money";
        public const string LOADING_TIME = "loading_time";
        public const string CHANGE_LANGUAGE = "change_language";

        public const string BUY_IAP = "buy_iap";
        public const string LOG_ADS = "log_ads";
        public const string LOG_CLICK_OK = "log_click_ok";
        public const string STAR_RATE = "star_rate";
        public const string END_LEVEL = "end_level";
        public const string NOTIFY_CLICK = "notify_click";
        public const string LEVEL_MODIFY = "level_modify";
        public const string VIP = "vip";
        public const string CS_CLIENT_INFO = "cs_client_info";
        public const string FCM_TOPIC = "fcm_topic";
        public const string FORCE_UPDATE = "force_update";
        public const string HACK_DETECTOR = "hack_detector";

        public const string NUM_MAIL = "new_mail_num";
        public const string NEW_MAIL_NOTIFY = "new_mail_notify";
        public const string LOG_FIREBASE = "log_firebase";
        public const string SCENE_LOADED = "scene_loaded";


        public const string CLAN_CREATE = "clan_create";
        public const string CLAN_LEAVE = "clan_leave";
        public const string CLAN_JOIN_REQUEST = "clan_join_request";
        public const string CLAN_DESTROY_JOIN_REQUEST = "clan_destroy_join_request";
        public const string CLAN_CONFIRM_JOIN_REQUEST = "clan_confirm_join_request";
        public const string CLAN_KICK_MEMBER = "clan_kick_member";
        public const string CLAN_GRANT_MASTER = "clan_grant_master";
        public const string CLAN_GRANT_VICE_MASTER = "clan_grant_vice_master";
        public const string CLAN_REVOKE_VICE_MASTER = "clan_revoke_vice_master";
        public const string CLAN_LIST_SUGGEST = "clan_list_suggest";
        public const string MY_CLAN = "my_clan";
        public const string CLAN_LIST_TOP_WORLD = "clan_list_top_world";
        public const string CLAN_SEARCH = "clan_search";
        public const string CLAN_INVITATION = "clan_invitation";
        public const string MY_CLAN_INVITATION = "my_clan_invitation";
        public const string CLAN_LIST_INVITATION = "clan_list_invitation";
        public const string CLAN_INVITATION_ACCEPT = "clan_invitation_accept";
        public const string CLAN_INVITATION_REJECT = "clan_invitation_reject";
        public const string CLAN_MEMBER_REQUEST = "clan_member_request";
        public const string CLAN_MEMBER = "clan_member";
        public const string MY_CLAN_REQUEST = "my_clan_request";
        public const string PLAYER_INFO = "player_info";
        public const string CLAN_JOIN_CHAT = "clan_join_chat";
        public const string CLAN_LEAVE_CHAT = "clan_leave_chat";
        public const string CLAN_CHAT = "clan_chat";
        public const string CLAN_DONATE = "donate";
        public const string CLAN_DONATE_LOG = "donate_log";
        public const string CLAN_DONATE_CARD_QUEUE = "donate_card_queue";
        public const string CLAN_DONATE_NOTIFY = "donate_notify";
        public const string CLAN_DONATE_REQUEST = "donate_request";
        public const string SPEAKER = "speaker";

        public const string SHARE_FACEBOOK = "share_facebook";
        public const string DAILY_SPIN = "daily_spin";
        public const string ACHIEVEMENT_COMPLETED = "achievement_completed";
        public const string TRANSLATE = "translate";

        public const string LIVE_SCORE_JOIN = "live_score_join";
        public const string LIVE_SCORE_LEAVE = "live_score_leave";
        public const string LIVE_SCORE_LIST = "live_score_list";
        public const string LIVE_SCORE = "live_score";
        public const string MAIL_TO_SERVER = "feedback_to_system";
        public const string MY_FRIENDS = "my_friends";
        public const string CLAN_EDIT = "clan_edit";
        public const string CLAN_TOURNAMENT_INFO = "clan_tournament_info";
        public const string CLAN_TOURNAMENT_CLAIM = "clan_tournament_claim";
        public const string PVP_INVITE = "pvp_invite";
        public const string PVP_INVITE_CONFIRM = "pvp_invite_confirm";
        public const string CLAN_INFO = "clan_info";
        public const string GIFT_CODE = "giftcode";

        public const string REQUEST_FRIEND = "request_friend";
        public const string UNFRIEND = "unfriend";
        public const string MY_REQUEST = "my_requests";
        public const string FOLLOW = "follow";
        public const string MY_FOLLOW = "my_follows";
        public const string FRIENDSHIP_STATUS = "friendship_status";
        public const string FRIENDSHIP_STATS = "stats_friendship";

        public const string MEGA_TOURNAMENT = "mega_tournament";
        public const string MEGA_TOURNAMENT_INFO = "mega_tournament_info";
        public const string MEGA_TOURNAMENT_CLAIM = "mega_tournament_claim";
        public const string MEGA_TOURNAMENT_UNLOCK_BONUS = "mega_tournament_unlock_bonus";

        public const string EXPANDSION = "expandsion";

        public const string PVP_SESSION_REWARDED_INFO = "pvp_session_rewarded_info";
        public const string PVP_SESSION_REWARDED_CLAIM = "pvp_session_rewarded_claim";

        public const string SELECT_LV_EVENT = "summer_holiday_event";
        public const string SELECT_LV_EVENT_INFO = "summer_holiday_event_detail";
        public const string SELECT_LV_EVENT_TICKET = "summer_holiday_event_ticket";

        public const string PING = "ping_";
        public const string PONG = "pong_";

        public const string MY_SHOP = "my_shop";
        public const string MY_RANDOM_SHOP = "my_random_shop";
        public const string SHOP_RESET = "shop_reset";
        public const string RANDOM_SHOP_RESET = "random_shop_reset";
        public const string SHOP_BUY_ITEM = "shop_buy_item";
        public const string RANDOM_SHOP_BUY_ITEM = "random_shop_buy_item";
        public const string MEGA_TOURNAMENT_BUY_TICKET = "mega_tournament_buy_ticket";

        public const string COOP_PVP_MATCHING = "coop_pvp_matching";
        public const string COOP_PVP_STOP_MATCHING = "coop_pvp_stop_matching";
        public const string COOP_PVP_ROOM_INFO = "coop_pvp_room_info";
        public const string COOP_PVP_INVITE = "coop_pvp_invite";
        public const string COOP_PVP_CONFIRM = "coop_pvp_confirm";
        public const string COOP_PVP_READY = "coop_pvp_player_ready";
        public const string COOP_PVP_OUT = "coop_pvp_player_out";
        public const string COOP_PVP_DIED = "coop_pvp_player_died";
        public const string COOP_PVP_REMATCH = "coop_pvp_rematch";
        public const string COOP_PVP_TOURNAMENT_INFO = "coop_pvp_tournament_info";
        public const string COOP_PVP_SEASON_REWARD_INFO = "coop_pvp_season_rewarded_info";
        public const string COOP_PVP_SEASON_REWARD_CLAIM = "coop_pvp_season_rewarded_claim";
        public const string COOP_PVP_ENEMY_DIE = "coop_pvp_enemy_die";

        public const string COOP_PVP_NOTIFY = "coop_pvp_notify";
        public const string COOP_PVP_NOTIFY_CONFIRM = "coop_pvp_notify_confirm";
        public const string BUY_ROYALTY_PACK = "buy_royalty_pack";
        public const string GET_ROYALTY_PACK_INFO = "get_royalty_pack_info";
        public const string ROYALTY_PACK_OPEN_CHEST = "royalty_pack_open_chest";
        public const string OPEN_LUCKY_BOX_GOLD_X10 = "open_lucky_box_gold_x10";
    }
}