﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class PvPCustomEditor : MonoBehaviour {
    [MenuItem("Tools/Clear PvP data")]
    public static void LogSelectedTransformName() {
        CachePvp.ClearMultiplayerData();
    }
}
#endif
