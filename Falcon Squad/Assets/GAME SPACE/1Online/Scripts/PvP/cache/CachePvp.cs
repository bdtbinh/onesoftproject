﻿using Mp.Pvp;
using UnityEngine;
using OSNet;
using System.Collections.Generic;
using System;
using System.Globalization;
using com.ootii.Messages;
using UnityEngine.SceneManagement;

public class CachePvp
{
    public const int HANDSHAKE = 0;
    public const int PLAYER_MOVE = 1;
    public const int ENEMY_DIE = 2;
    public const int ENEMY_HP = 3;
    public const int ACTIVE_SKILL = 4;
    public const int SCORE = 5;
    public const int PLAYER_HP = 6;
    public const int PLAYER_BULLET = 7;

    public const int TYPE_ACTIVE_SKILL_PLAYER = 0;
    public const int TYPE_ACTIVE_SKILL_WING = 1;


    public const int TYPE_PVP = 0;
    public const int TYPE_PVP_FRIENDS = 1;
    public const int TYPE_MEGA_TOURNAMENT = 2;
    public const int TYPE_2v2 = 3;
    public const int TYPE_FRIEND_2v2 = 4;

    public static bool isInvite;

    private static CachePvp _instance = null;

    public static CachePvp Instance
    {
        get
        {
            if (_instance == null)
                _instance = new CachePvp();
            return _instance;
        }
    }

    public static void Destroy()
    {
        if (_instance != null)
        {
            Opponent = null;
            _instance = null;
        }
    }

    public static string ConvertChina(string name)
    {
        if (GameContext.IS_CHINA_VERSION)
        {
            switch (name)
            {
                case "V":
                    return "5";
                case "IV":
                    return "4";
                case "III":
                    return "3";
                case "II":
                    return "2";
                case "I":
                    return "1";
            }
        }
        return name;
    }

    public static int numberCoin = 100;

    public static bool isShowPopupVideo = false;

    public static bool canPause = false;

    public static bool LoadMegaRank;
    public static int LoadLiveScore; //0: pvp, 1: mega, 2: 2v2

    public const string TOKEN_PLAYERPREFS_KEY = "pvp_token";
    public const string NAME_PLAYERPREFS_KEY = "pvp_name";
    public const string CODE_PLAYERPREFS_KEY = "pvp_id";
    public const string FBID_PLAYERPREFS_KEY = "pvp_fbid";
    public const string GCID_PLAYERPREFS_KEY = "pvp_gcid";
    public const string DEVICEID_PLAYERPREFS_KEY = "pvp_deviceId";
    public const string SERVER_URL_PLAYERPREFS_KEY = "pvp_server_url";
    public const string CAN_PLAY_PVP = "Can_Play_PVP";
    public const string REQUIRE_LEVEL_PLAYERPREFS_KEY = "pvp_require_level";
    public const string REQUIRE_LEVEL_2V2_PLAYERPREFS_KEY = "2v2_require_level";
    public const string REQUIRE_LEVEL_PLAYERINFO_PLAYERPREFS_KEY = "pvp_require_playerinfo_level";
    public const string AVATAR_PLAYERPREFS_KEY = "pvp_avatar";
    public const string WIN_PLAYERPREFS_KEY = "pvp_win";
    public const string LOSE_PLAYERPREFS_KEY = "pvp_lose";
    public const string EXP_PLAYERPREFS_KEY = "pvp_exp";
    public const string MEDAL_PLAYERPREFS_KEY = "medal_shop_clan";
    public const string RANKING_PLAYERPREFS_KEY = "pvp_ranking";
    public const string COUNTRY_PLAYERPREFS_KEY = "pvp_country";
    public const string ENDLESS_VALUE_PLAYERPREFS_KEY = "endless_value";
    public const string DATE_TIME_SERVER = "date_time_server_namnx";
    public const string IS_SENT_VALIDATE_ID = "is_sent_validate_id_namnx";
    public const string VIP_POINT = "pvp_po_namnx";
    public const string VIP_POINT_NEW = "pvp_po_new_namnx";
    public const string FALCON_MAIL = "falcon_mail_namnx";
    public const string RESTORE_VIP = "restore_vip_namnx";
    public const string CAN_RECEIVE_NOTIFY = "can_receive_notify_namnx";
    public const string FIRST_TAB_INDEX = "first_tab_index_namnx";
    public const string COUNT_FRIENDS = "count_friends_namnx";
    public const string COUNT_FRIENDS_MINH = "count_friends_minh";

    public const string ORDER_SHOP_GOLD_PLAYERPREFS_KEY = "order_shop_gold";
    public const string ORDER_SHOP_GEM_PLAYERPREFS_KEY = "order_shop_gem";
    public const string PROMOTION_SHOP_GOLD_PLAYERPREFS_KEY = "promotion_shop_gold";
    public const string PROMOTION_SHOP_GEM_PLAYERPREFS_KEY = "promotion_shop_gem";
    public const string PVP_ASSET_BUNDLE_VERSION_KEY = "pvp_asset_bundle_version_key";
    public const string FCM_TOKEN = "fcm_token";
    public const string NEW_MAIL_NUMBER = "new_mail_number_namnx";
    public const string NEW_MAIL_NUMBER_FORCE = "new_mail_number_force_namnx";
    public const string ASSET_BUNDLE_URL = "asset_bundle_url";
    public const string ASSET_BUNDLE_CURRENT_VERSION = "asset_bundle_current_version";
    public const string ASSET_BUNDLE_ENABLE = "asset_bundle_enable";
    public const string SERIALIZABLE_SET_KEY = "sirializable_set_key";
    public const string SERIALIZABLE_SCCONFIG_KEY = "sirializable_scconfig_key";
    public const string SERIALIZABLE_SC_VERSION_CONFIG_KEY = "sirializable_sc_version_config_key";
    public const string GOLD_BET_JSON = "gold_bet_json_namnx";
    public const string GOLD_BET_2VS2_JSON = "gold_bet_2vs2_json_namnx";

    public const string RANK_WORLD = "world";
    public const string RANK_LOCAL = "local";
    public const string RANK_FACEBOOK = "facebook";
    public const string RANK_WORLD_PVP = "world_pvp";
    public const string RANK_LOCAL_PVP = "local_pvp";
    public const string RANK_FACEBOOK_PVP = "facebook_pvp";
    public const string RANK_WORLD_2V2 = "world_pvp_2v2";
    public const string RANK_LOCAL_2V2 = "local_pvp_2v2";
    public const string RANK_FACEBOOK_2V2 = "facebook_pvp_2v2";


    public const string NOTIFY_TOURNAMENT = "notify_tournament";

    public static bool isConnectServer;

    private static Player _myInfo;
    public static Player MyInfo
    {
        get
        {
            if (_myInfo == null)
            {
                _myInfo = new Player();
            }

            _myInfo.code = Code;
            _myInfo.name = Name;
            _myInfo.country = Country;
            _myInfo.facebookId = FacebookId;
            _myInfo.appCenterId = GamecenterId;
            _myInfo.avatar = Avatar;
            _myInfo.win = Win;
            _myInfo.lose = Lose;
            _myInfo.playerLevel = PlayerLevel;
            _myInfo.pvp2vs2PlayerLevel = PlayerLevel2vs2;
            _myInfo.megaPlayerLevel = MegaPlayerLevel;
            _myInfo.pvP2vs2SeasonData = PvP2vs2SeasonData;
            //_myInfo.exp = Exp;
            _myInfo.medal = Medal;
            _myInfo.gold = GetGold();
            _myInfo.diamond = GetGem();
            _myInfo.energy = GetEnergy();
            _myInfo.levelValue = PvpUtil.LevelValue;
            _myInfo.ranking = Ranking;
            _myInfo.offline = false;
            _myInfo.data = PlayerDataUtil.GetPlayerProfileData().ToString();
            _myInfo.endlessValue = EndlessValue;
            _myInfo.canReceiveNotify = CanReceiveNotify;
            _myInfo.vip = myVipPoint;
            return _myInfo;
        }
    }

    public static int MyCode;
    public static int MyTeammateCode;
    public static Player MyTeamate;

    public static SCCoopPvPTournamentInfo coopPvPTournamentInfo;

    public static SCMegaTournamentInfo megaTournamentInfo;
    public static ClanInfo clanInfo;
    public static SCCoopPvPRoomInfo coopPvPRoomInfo;
    public static List<Player> listPlayers2v2 = new List<Player>();
    public static Dictionary<long, Player> dictionaryPlayers2v2 = new Dictionary<long, Player>();
    public static int myTeamIndex = -1;
    public static int myIndex = -1;
    public static int myTeammateIndex = -1;

    private static List<int> _listGoldBet = new List<int>();
    private static List<int> _listGoldBet2vs2 = new List<int>();

    public static long clanId;

    public static List<int> listGoldBet
    {
        get
        {
            if (_listGoldBet.Count == 0)
            {
                List<JSONObject> list = goldBetting.ToJsonObject().ToList();
                for (int i = 0; i < list.Count; i++)
                {
                    _listGoldBet.Add(list[i].ToInt());
                }
            }
            return _listGoldBet;
        }
        set
        {
            _listGoldBet = value;
            JSONObject jo = "[]".ToJsonObject();
            for (int i = 0; i < _listGoldBet.Count; i++)
            {
                jo.AddItem(_listGoldBet[i]);
            }
            goldBetting = jo.ToJson();
        }
    }

    public static int HighestShip(PlayerDataUtil.PlayerProfileData profile)
    {
        Array airCrafts = Enum.GetValues(typeof(AircraftTypeEnum));
        if (profile.selectedShip > airCrafts.Length)
        {
            for (int i = airCrafts.Length - 1; i >= 0; i++)
            {
                if (profile.listAirCraft[i] == 1)
                {
                    return i + 1;
                }
            }
        }
        return profile.selectedShip;
    }

    public static int selectedWingmanLeft;

    public static int HighestWingmanLeft(PlayerDataUtil.PlayerProfileData profile)
    {
        Array wingmans = Enum.GetValues(typeof(WingmanTypeEnum));
        if (profile.selectedLeftWingman >= wingmans.Length)
        {
            for (int i = wingmans.Length - 1; i >= 0; i++)
            {
                if (profile.listWingman[i] == 1)
                {
                    if (profile.selectedRightWingman != i || profile.selectedRightWingman == 0)
                    {
                        selectedWingmanLeft = i;
                        return i;
                    }
                }
            }
        }
        selectedWingmanLeft = profile.selectedLeftWingman;
        return profile.selectedLeftWingman;
    }

    public static int HighestWingmanRight(PlayerDataUtil.PlayerProfileData profile)
    {
        Array wingmans = Enum.GetValues(typeof(WingmanTypeEnum));
        if (profile.selectedRightWingman >= wingmans.Length)
        {
            for (int i = wingmans.Length - 1; i >= 0; i++)
            {
                if (profile.listWingman[i] == 1)
                {
                    if (selectedWingmanLeft != i || selectedWingmanLeft == 0)
                    {
                        return i;
                    }
                }
            }
        }
        return profile.selectedRightWingman;
    }

    public static int HighestWing(PlayerDataUtil.PlayerProfileData profile)
    {
        Array wings = Enum.GetValues(typeof(WingTypeEnum));
        if (profile.selectedWing >= wings.Length)
        {
            for (int i = wings.Length - 1; i >= 0; i++)
            {
                if (profile.listWing[i] == 1)
                {
                    return i;
                }
            }
        }
        return profile.selectedWing;
    }

    public static int HighestShipTournament(PlayerDataUtil.PlayerProfileData profile)
    {
        Array airCrafts = Enum.GetValues(typeof(AircraftTypeEnum));
        if (profile.selectedShipTournament > airCrafts.Length)
        {
            for (int i = airCrafts.Length - 1; i >= 0; i++)
            {
                if (profile.listAirCraft[i] == 1)
                {
                    return i + 1;
                }
            }
        }
        return profile.selectedShipTournament;
    }

    public static List<int> listGoldBet2vs2
    {
        get
        {
            if (_listGoldBet2vs2.Count == 0)
            {
                List<JSONObject> list = goldBetting2vs2.ToJsonObject().ToList();
                for (int i = 0; i < list.Count; i++)
                {
                    _listGoldBet2vs2.Add(list[i].ToInt());
                }
            }
            return _listGoldBet2vs2;
        }
        set
        {
            _listGoldBet2vs2 = value;
            JSONObject jo = "[]".ToJsonObject();
            for (int i = 0; i < _listGoldBet2vs2.Count; i++)
            {
                jo.AddItem(_listGoldBet2vs2[i]);
            }
            goldBetting2vs2 = jo.ToJson();
        }
    }

    public static string falconMailId = "";

    public static bool isForceUpdate;

    public static Dictionary<string, int> dictionaryRank = new Dictionary<string, int>()
    {
        {"BRONZE", 1 },
        {"SILVER", 2 },
        {"GOLD", 3 },
        {"PLATINUM", 4 },
        {"DIAMOND", 5 },
        {"MASTER", 6 },
        {"CHALLENGER", 7 },
        {"LEGEND", 8 }
    };

    public static string MedalRank2v2Title(string chestName)
    {
        string name = "";
        switch (chestName)
        {
            case "BRONZE":
                name = I2.Loc.ScriptLocalization.rank_pvp_bronze;
                break;

            case "SILVER":
                name = I2.Loc.ScriptLocalization.rank_pvp_silver;
                break;

            case "GOLD":
                name = I2.Loc.ScriptLocalization.rank_pvp_gold;
                break;

            case "PLATINUM":
                name = I2.Loc.ScriptLocalization.rank_pvp_platinum;
                break;

            case "DIAMOND":
                name = I2.Loc.ScriptLocalization.rank_pvp_diamond;
                break;

            default:
                name = "";
                break;
        }
        return name;
    }

    public static List<Player> listPlayerLeaderboard = new List<Player>();

    public static int ironsrcOfferWall;
    public static int crossPromotionOfferWall;

    public static long timeEndSession { get; set; }
    public static long timeSinceStartUp { get; set; }

    public static List<ShopItem> listShopItem;

    public static bool needSync;
    public static SCClientConfig sCClientConfig
    {
        get
        {
            string data = PlayerPrefs.GetString(SERIALIZABLE_SCCONFIG_KEY);
            SCClientConfig s;
            if (string.IsNullOrEmpty(data))
            {
                s = new SCClientConfig();
                Debug.Log("new SCClientConfig");
            }
            else
            {
                //s = data.
                s = JsonFx.Json.JsonReader.Deserialize<SCClientConfig>(data);
                Debug.Log("cache SCClientConfig");
            }
            return s;
        }
        set
        {
            PlayerPrefs.SetString(SERIALIZABLE_SCCONFIG_KEY, JsonFx.Json.JsonWriter.Serialize(value));
            MessageDispatcher.SendMessage(EventName.UIHome.SetTurnOnTeamSCClientConfig.ToString());
        }
    }

    public static SCVersionConfig sCVersionConfig
    {
        get
        {
            string data = PlayerPrefs.GetString(SERIALIZABLE_SC_VERSION_CONFIG_KEY);
            SCVersionConfig s;
            if (string.IsNullOrEmpty(data))
            {
                s = new SCVersionConfig();
                Debug.Log("new SCVersionConfig");
            }
            else
            {
                //s = data.
                s = JsonFx.Json.JsonReader.Deserialize<SCVersionConfig>(data);
                Debug.Log("cache SCVersionConfig");
            }
            return s;
        }
        set
        {
            PlayerPrefs.SetString(SERIALIZABLE_SC_VERSION_CONFIG_KEY, JsonFx.Json.JsonWriter.Serialize(value));
            //MessageDispatcher.SendMessage(EventName.UIHome.SetTurnOnTeamSCClientConfig.ToString());
        }
    }

    public static string goldBetting
    {
        get
        {
            return PlayerPrefs.GetString(GOLD_BET_JSON, "[]");
        }
        set
        {
            PlayerPrefs.SetString(GOLD_BET_JSON, value);
        }
    }

    public static string goldBetting2vs2
    {
        get
        {
            return PlayerPrefs.GetString(GOLD_BET_JSON, "[]");
        }
        set
        {
            PlayerPrefs.SetString(GOLD_BET_JSON, value);
        }
    }

    public static string assetBundleUrl
    {
        get
        {
            return PlayerPrefs.GetString(ASSET_BUNDLE_URL, SCClientConfig.ASSET_BUNDLE_URL_DEFAULT);
        }
        set
        {
            PlayerPrefs.SetString(ASSET_BUNDLE_URL, value);
        }
    }

    public static int assetBundleCurrentVersion
    {
        get
        {
            return PlayerPrefs.GetInt(ASSET_BUNDLE_CURRENT_VERSION, AutoLoadConfigFile.ASSET_BUNDLE_VERSION);
        }
        set
        {
            PlayerPrefs.SetInt(ASSET_BUNDLE_CURRENT_VERSION, value);
        }
    }

    public static int assetBundleEnable
    {
        get
        {
            return PlayerPrefs.GetInt(ASSET_BUNDLE_ENABLE, -1);
        }
        set
        {
            PlayerPrefs.SetInt(ASSET_BUNDLE_ENABLE, value);
        }
    }

    public static SerializableSet serializableSet
    {
        get
        {
            string jsonData = PlayerPrefs.GetString(SERIALIZABLE_SET_KEY, "");
            if (jsonData.Length > 0)
                return JsonFx.Json.JsonReader.Deserialize<SerializableSet>(jsonData);
            else
                return null;
        }
        set
        {
            PlayerPrefs.SetString(SERIALIZABLE_SET_KEY, JsonFx.Json.JsonWriter.Serialize(value));
        }
    }

    public static int GetVipFromVipPoint(int vipPoint)
    {
        int v = 0;
        for (int i = 0; i < vipArray.Length; i++)
        {
            if (vipPoint < vipArray[i])
            {
                v = i;
                break;
            }
        }
        if (vipPoint >= vipArray[vipArray.Length - 1])
        {
            v = vipArray.Length;
        }
        return v;
    }

    private const string CHEST_1 = "2vs2Chest_1";
    private const string CHEST_2 = "2vs2Chest_2";
    private const string CHEST_3 = "2vs2Chest_3";
    private const string CHEST_4 = "2vs2Chest_4";
    private const string CHEST_5 = "2vs2Chest_5";

    public static string GetSpriteNameChest(string chest)
    {
        // BRONZE | SILVER | GOLD | PLATINUM | DIAMOND
        switch (chest)
        {
            case "BRONZE":
                return CHEST_1;
            case "SILVER":
                return CHEST_2;
            case "GOLD":
                return CHEST_3;
            case "PLATINUM":
                return CHEST_4;
            case "DIAMOND":
                return CHEST_5;
            default:
                return "";
        }
    }

    private static PlayerLevel _playerLevel;

    public static PlayerLevel PlayerLevel
    {
        get
        {
            if (_playerLevel == null)
            {
                _playerLevel = new PlayerLevel();
            }
            return _playerLevel;
        }
        set
        {
            _playerLevel = value;
        }
    }

    private static PlayerLevel _playerLevel2vs2;

    public static PlayerLevel PlayerLevel2vs2
    {
        get
        {
            if (_playerLevel2vs2 == null)
            {
                _playerLevel2vs2 = new PlayerLevel();
            }
            return _playerLevel2vs2;
        }
        set
        {
            _playerLevel2vs2 = value;
        }
    }

    private static PvP2vs2SeasonData _pvP2vs2SeasonData;

    public static PvP2vs2SeasonData PvP2vs2SeasonData
    {
        get
        {
            if (_pvP2vs2SeasonData == null)
            {
                _pvP2vs2SeasonData = new PvP2vs2SeasonData();
            }
            return _pvP2vs2SeasonData;
        }
        set
        {
            _pvP2vs2SeasonData = value;
        }
    }

    private static PlayerLevel _megaPlayerLevel;

    public static PlayerLevel MegaPlayerLevel
    {
        get
        {
            if (_megaPlayerLevel == null)
            {
                _megaPlayerLevel = new PlayerLevel();
            }
            return _megaPlayerLevel;
        }
        set
        {
            _megaPlayerLevel = value;
        }
    }

    private static FriendshipStats _friendshipStats;

    public static FriendshipStats FriendshipStats
    {
        get
        {
            if (_friendshipStats == null)
            {
                _friendshipStats = new FriendshipStats();
            }
            return _friendshipStats;
        }
        set
        {
            _friendshipStats = value;
        }
    }

    private static PlayerClan _playerClan;

    public static PlayerClan PlayerClan
    {
        get
        {
            if (_playerClan == null)
            {
                _playerClan = new PlayerClan();
            }
            return _playerClan;
        }
        set
        {
            _playerClan = value;
        }
    }

    public static void SetGem(int gem, string why, string where)
    {
        CacheGame.SetTotalGem(gem, why, where);
    }

    public static int GetGem()
    {
        return CacheGame.GetTotalGem();
    }

    public static void SetEnergy(int energy, string why, string where)
    {
        CacheGame.SetTotalEnergy(energy, why, where);
    }

    public static int GetEnergy()
    {
        return CacheGame.GetTotalEnergy();
    }

    private static int _topChat;

    public static int topChat
    {
        get
        {
            return _topChat;
        }
        set
        {
            _topChat = value;
            //MessageDispatcher.SendMessage(EventName.Clan.ChangeTopChat.ToString());
        }
    }

    private static int _bottomChat;

    public static int bottomChat
    {
        get
        {
            return _bottomChat;
        }
        set
        {
            _bottomChat = value;
            //MessageDispatcher.SendMessage(EventName.Clan.ChangeBottomChat.ToString());
        }
    }

    public static float GetWinrate()
    {
        if (Win == 0) return 0;
        else return (float)Win * 100 / (Win + Lose);
    }
    public static string GetWinrateText()
    {
        return GetWinrate().ToString("0.00") + "%";
    }

    public static bool isFetchFirebase = false;

    public void LogIAPToServer(string name, string price, string currencyCode, string where, int vipPoint, string productID, string transactionID, string packageName, string purchaseToken, string purchaseDate)
    {
        new CSInApp(name, price, currencyCode, where, vipPoint, productID, transactionID, packageName, purchaseToken, purchaseDate).Send(true);
    }

    public void LogStarRateToServer(int star)
    {
        new CSRating(star).Send();
    }

    public void LogAds(string ads_id, string ads_type, string where, int ads_status)
    {
        new CSLogAds(ads_id, ads_type, where, ads_status).Send();
    }

    public void LogEndLevel(int level, int status, int time, int typeLevel, string stars = "")
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            new CSEndLevel(level, status, time, typeLevel, stars).Send();
        }
    }

    public static int typePVP;

    public static Player Opponent { get; set; }
    public static PlayerResult MyResult { get; set; }
    public static PlayerResult OpponentResult { get; set; }
    public static RoomInfo RoomInfo { get; set; }

    public static FBUser facebookUser { get; set; }
    public static List<long> listClanIdRequest = new List<long>();

    public static string roomId { get; set; }
    public static int delayTime { get; set; }
    public static int map { get; set; }
    public static int eloWin { get; set; }
    public static int eloDraw { get; set; }
    public static int eloLose { get; set; }
    public static List<Player> players { get; set; }

    public static int goldBetFromInvitation { get; set; }
    public static int goldBetFromInvitation2v2 { get; set; }
    public static bool isRoomDestroyed { get; set; }

    public static Player memberPlayer { get; set; }
    public static int typeActionMemberClan { get; set; }
    //set to vice : 0
    //set to member : 1
    //set to master : 2
    //kick : 3
    //quit : 4
    //private static List<int> listRandom = new List<int>() { 50, 15, 10, 5, 5, 5, 10 };
    //private static List<int> listRandom = new List<int>() { 60, 15, 10, 5, 10 };
    private static List<int> listRandom = new List<int>() { 50, 10, 10, 5, 5, 3, 5, 2, 10 };

    private static readonly List<int> listIdentityGold = new List<int>() { 100, 200, 300, 400, 420, 450, 480, 500, 600, 700, 800, 900, 2000, 5000, 10000, 20000, 50000, 100000 };
    private static readonly int identityGold = 300;

    private static readonly List<int> listIdentityGem = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 150, 300, 500, 900, 1000 };
    private static readonly int identityGem = 5;

    private static readonly List<int> listIdentityCard = new List<int>() { 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 5, 10, 50, 100 };
    private static readonly int identityCard = 2;

    private static readonly List<int> listIdentityItem = new List<int>() { 1, 4, 5, 6, 7, 8, 9, 10, 12, 14, 17, 20 };
    private static readonly int identityItem = 5;

    public static ItemVolumeFalconMail GetRewardFromVideo()
    {
        ItemVolumeFalconMail item = new ItemVolumeFalconMail();
        int r = UnityEngine.Random.Range(0, 101);
        int cnt = 0;
        for (int i = 0; i < listRandom.Count; i++)
        {
            cnt += listRandom[i];
            if (r <= cnt)
            {
                if (i == 0)
                {
                    //gold
                    item.key = FalconMail.ITEM_GOLD;
                    item.value = GetValueFromList(listIdentityGold, identityGold);
                }
                else if (i == 1)
                {
                    //gem
                    item.key = FalconMail.ITEM_GEM;
                    item.value = GetValueFromList(listIdentityGem, identityGem);
                }
                else if (i == 2)
                {
                    //card rieng plane
                    int r1 = UnityEngine.Random.Range(0, 7);
                    if (r1 < 5)
                    {
                        item.key = FalconMail.ITEM_CARD_PLANE_1 + r1;
                    }
                    else
                    {
                        item.key = FalconMail.ITEM_CARD_PLANE_6 + r1 - 5;
                    }
                    item.value = GetValueFromList(listIdentityCard, identityCard);
                }
                else if (i == 3)
                {
                    //card chung
                    item.key = FalconMail.ITEM_CARD_PLANE_GENERAL;
                    item.value = GetValueFromList(listIdentityCard, identityCard);
                }
                else if (i == 4)
                {
                    //card rieng drone
                    int r1 = UnityEngine.Random.Range(0, 6);
                    if (r1 <= 4)
                    {
                        item.key = FalconMail.ITEM_CARD_WINGMAN_1 + r1;
                    }
                    else
                    {
                        item.key = FalconMail.ITEM_CARD_WINGMAN_6;
                    }
                    item.value = GetValueFromList(listIdentityCard, identityCard);
                }
                else if (i == 5)
                {
                    //card chung drone
                    item.key = FalconMail.ITEM_CARD_WINGMAN_GENERAL;
                    item.value = GetValueFromList(listIdentityCard, identityCard);
                }
                else if (i == 6)
                {
                    //card rieng wing
                    int r1 = UnityEngine.Random.Range(0, 2);
                    item.key = FalconMail.ITEM_CARD_WING_1 + r1;
                    item.value = GetValueFromList(listIdentityCard, identityCard);
                }
                else if (i == 7)
                {
                    //card chung wing
                    item.key = FalconMail.ITEM_CARD_WINGMAN_GENERAL;
                    item.value = GetValueFromList(listIdentityCard, identityCard);
                }
                else if (i == 8)
                {
                    //powerup, active skill, life
                    int r1 = UnityEngine.Random.Range(0, 3);
                    item.key = FalconMail.ITEM_POWER_UP + r1;
                    item.value = GetValueFromList(listIdentityItem, identityItem);
                }
                return item;
            }
        }

        return item;
    }

    private static List<float> tempList = new List<float>();

    private static int GetValueFromList(List<int> list, int value)
    {
        tempList.Clear();
        float total = 0;
        for (int i = 0; i < list.Count; i++)
        {
            float f = value * 1.0f / list[i];
            tempList.Add(f);
            total += f;
        }

        float r = UnityEngine.Random.Range(0, total);
        float f1 = 0;
        for (int i = 0; i < tempList.Count; i++)
        {
            f1 += tempList[i];
            if (r <= f1)
            {
                return list[i];
            }
        }
        return 0;
    }

    public static int serverX2PurchaseGold; //0: no, 1: yes
    public static int serverX2PurchaseGem; //0: no, 1: yes

    public static void ClearPlayerPref()
    {
        PlayerPrefs.DeleteAll();
    }

    static string timeTemp;
    static string deviceIdTemp;

    public static void SetPlayerData(Player player, string token)
    {
        deviceIdTemp = deviceId;
        timeTemp = timeFromServer;
        ClearPlayerPref();
        timeFromServer = timeTemp;
        deviceId = deviceIdTemp;
        isForceUpdate = true;
        Debug.Log("=================SetPlayerData===============");
        Code = player.code;
        Debug.Log("Code : " + Code);
        Token = token;
        Debug.Log("Token : " + token);
        Name = player.name;
        Debug.Log("Name : " + Name);
        Country = player.country;
        Debug.Log("Country : " + Country);
        FacebookId = player.facebookId;
        Debug.Log("FacebookId : " + FacebookId);
        GamecenterId = player.appCenterId;
        Debug.Log("GamecenterId : " + GamecenterId);
        Win = player.win;
        Lose = player.lose;
        //Exp = player.exp;
        //Medal = player.medal;
        PlayerLevel = player.playerLevel;
        MegaPlayerLevel = player.megaPlayerLevel;
        if (player.pvp2vs2PlayerLevel != null)
        {
            PlayerLevel2vs2 = player.pvp2vs2PlayerLevel;
        }
        FriendshipStats = player.friendshipStats;
        if (player.friendshipStats != null)
        {
            CountFriendsMinh = player.friendshipStats.numFriendFb;
        }
        PlayerClan = player.playerClan;
        if (player.pvP2vs2SeasonData != null)
        {
            PvP2vs2SeasonData = player.pvP2vs2SeasonData;
        }
        RestoreVip = 1;
        myVipPoint = player.vip;
        RestoreVip = 0;
        CacheGame.SetTotalCoinNamNX(player.gold);
        CacheGame.SetTotalGemNamNX(player.diamond);
        CacheGame.SetTotalEnergyNamNX(player.energy);
        EndlessValue = player.endlessValue;
        Ranking = player.ranking;
        Debug.Log("=================profile===============");
        PlayerDataUtil.PlayerProfileData profile = PlayerDataUtil.StringToObject(player.data);
        ForceSetPlayerData(profile);
        isForceUpdate = false;
    }

    public static void ForceSetPlayerData(PlayerDataUtil.PlayerProfileData profile)
    {
        try
        {
            CacheGame.SetTotalItemLife(profile.numberItemLife);
            CacheGame.SetTotalItemActiveSkill(profile.numberItemEmp);
            CacheGame.SetTotalItemPowerUp(profile.numberItemPowerUp);
            CacheGame.SetBuyInfinityPack(profile.buyInfinityPack);
        }
        catch (Exception)
        {
            Debug.Log("Error group 1");
            throw;
        }
        try
        {
            Debug.Log("=================airCrafts===============");
            Array airCrafts = Enum.GetValues(typeof(AircraftTypeEnum));
            Debug.Log("airCrafts.Length : " + airCrafts.Length);
            for (int i = 0; i < airCrafts.Length; i++)
            {

                Debug.Log("airCrafts.GetValue(" + i + ") : " + airCrafts.GetValue(i));
                int index = (int)airCrafts.GetValue(i);
                if (profile.listAirCraft != null && profile.listAirCraft.Length > i)
                {
                    Debug.Log("profile.listAirCraft[" + i + "] : " + profile.listAirCraft[i]);
                    CacheGame.SetSpaceShipIsUnlockedNamNX(index, profile.listAirCraft[i]);
                }
                if (profile.listLevelPlane != null && profile.listLevelPlane.Length > i)
                {
                    CacheGame.SetSpaceShipLevel((AircraftTypeEnum)index, profile.listLevelPlane[i]);
                    CacheGame.SetSpaceShipRank((AircraftTypeEnum)index, (Rank)profile.listRankPlane[i]);
                    MinhCacheGame.SetSpaceShipCardsNamNX((AircraftTypeEnum)index, profile.listSpaceShipCard[i]);
                }
                if (profile.listAircraftUsedTimePerDay != null && profile.listAircraftUsedTimePerDay.Length > i)
                {
                    MinhCacheGame.SetAircraftUsedTimePerDay((AircraftTypeEnum)index, profile.listAircraftUsedTimePerDay[i]);
                }
                else
                {
                    MinhCacheGame.SetAircraftUsedTimePerDay((AircraftTypeEnum)index, MinhCacheGame.GetAircraftUsedTimePerDay((AircraftTypeEnum)index));
                }
            }
            MinhCacheGame.SetSpaceShipGeneralCards(profile.spaceShipGeneralCard, FirebaseLogSpaceWar.ServerPush_why, "");
        }
        catch (Exception)
        {
            Debug.Log("Error group 2");
            throw;
        }
        try
        {
            Array wingmans = Enum.GetValues(typeof(WingmanTypeEnum));

            for (int i = 1; i < wingmans.Length; i++)
            {
                Debug.Log("wingmans.GetValue(" + i + ") : " + wingmans.GetValue(i));
                int index = (int)wingmans.GetValue(i);
                if (profile.listWingman != null && profile.listWingman.Length > i - 1)
                {
                    Debug.Log("profile.listWingman[" + (i - 1) + "] : " + profile.listWingman[i - 1]);
                    CacheGame.SetWingManIsUnlockedNamNX(index, profile.listWingman[i - 1]);
                }
                if (profile.listLevelWingmanNew != null && profile.listLevelWingmanNew.Length > i - 1)
                {
                    CacheGame.SetWingmanLevel((WingmanTypeEnum)index, profile.listLevelWingmanNew[i - 1]);
                    CacheGame.SetWingmanRank((WingmanTypeEnum)index, (Rank)profile.listRankWingman[i - 1]);
                    MinhCacheGame.SetWingmanCardsNamNX((WingmanTypeEnum)index, profile.listWingmanCard[i - 1]);
                }
            }
            MinhCacheGame.SetWingmanGeneralCardsNamNX(profile.wingmanGeneralCard);
        }
        catch (Exception)
        {
            Debug.Log("Error group 3");
            throw;
        }

        try
        {
            Debug.Log("=================Wing===============");
            Array wings = Enum.GetValues(typeof(WingTypeEnum));
            Debug.Log("wings.Length : " + wings.Length);
            for (int i = 1; i < wings.Length; i++)
            {
                Debug.Log("wings.GetValue(" + i + ") : " + wings.GetValue(i));
                int index = (int)wings.GetValue(i);
                if (profile.listWing != null && profile.listWing.Length > i - 1)
                {
                    Debug.Log("profile.listWing[" + (i - 1) + "] : " + profile.listWing[i - 1]);
                    CacheGame.SetWingIsUnlocked(index, profile.listWing[i - 1]);
                }
                if (profile.listLevelWing != null && profile.listLevelWing.Length > i - 1)
                {
                    CacheGame.SetWingLevel((WingTypeEnum)index, profile.listLevelWing[i - 1]);
                    CacheGame.SetWingRank((WingTypeEnum)index, (Rank)profile.listRankWing[i - 1]);
                    MinhCacheGame.SetWingCardsNamNX((WingTypeEnum)index, profile.listWingCard[i - 1]);
                }
            }
            MinhCacheGame.SetWingGeneralCards(profile.wingGeneralCard, FirebaseLogSpaceWar.ServerPush_why, "");
        }
        catch (Exception)
        {
            Debug.Log("Error group Wing");
            throw;
        }

        try
        {
            Debug.Log("SetUsedInapp : " + profile.removeAds);
            CacheGame.SetUsedInapp(profile.removeAds);
            if (profile.currentLevel3Mode != null)
            {
                Debug.Log("SetMaxLevel DIFFICULT_NOMAL: " + profile.currentLevel3Mode[0]);
                CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, profile.currentLevel3Mode[0]);
                Debug.Log("SetMaxLevel DIFFICULT_HARD: " + profile.currentLevel3Mode[1]);
                CacheGame.SetMaxLevel(GameContext.DIFFICULT_HARD, profile.currentLevel3Mode[1]);
                Debug.Log("SetMaxLevel DIFFICULT_HELL: " + profile.currentLevel3Mode[2]);
                CacheGame.SetMaxLevel(GameContext.DIFFICULT_HELL, profile.currentLevel3Mode[2]);
            }
            DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
            double dou = profile.countdownVideoTime;
            DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
            MinhCacheGame.SetCoundownVideoTime(m_dateTime);

            unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
            dou = profile.countdownFreeTime;
            m_dateTime = unixEpoch.AddMilliseconds(dou);
            MinhCacheGame.SetCoundownFreeTime(m_dateTime);

            MinhCacheGame.SetSpinTickets(profile.spinTickets);
            CacheGame.SetDayUseVideoLuckyWheel(profile.lastDayUseVideoSpin);
            CacheGame.SetDayUseFreeLuckyWheel(profile.lastDayUseFreeSpin);
        }
        catch (Exception)
        {
            Debug.Log("error group 4");
            throw;
        }

        try
        {
            if (profile.starsNormal != null && profile.starsNormal.Length == CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL) * 3)
            {
                for (int i = 1; i <= GameContext.TOTAL_LEVEL; i++)
                {
                    for (int j = 1; j <= 3; j++)
                    {
                        CacheGame.SetNumStarLevelNamNX(i, j, 0, GameContext.DIFFICULT_NOMAL);
                    }
                }
                for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL); i++)
                {
                    for (int j = 1; j <= 3; j++)
                    {
                        Debug.Log("SetNumStarLevelNamNX : " + profile.starsNormal[(i - 1) * 3 + (j - 1)]);
                        CacheGame.SetNumStarLevelNamNX(i, j, profile.starsNormal[(i - 1) * 3 + (j - 1)], GameContext.DIFFICULT_NOMAL);
                    }
                }
            }
            if (profile.starsHard != null && profile.starsHard.Length == CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD) * 3)
            {
                for (int i = 1; i <= GameContext.TOTAL_LEVEL; i++)
                {
                    for (int j = 1; j <= 3; j++)
                    {
                        CacheGame.SetNumStarLevelNamNX(i, j, 0, GameContext.DIFFICULT_HARD);
                    }
                }
                for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD); i++)
                {
                    for (int j = 1; j <= 3; j++)
                    {
                        Debug.Log("SetNumStarLevelNamNX : " + profile.starsHard[(i - 1) * 3 + (j - 1)]);
                        CacheGame.SetNumStarLevelNamNX(i, j, profile.starsHard[(i - 1) * 3 + (j - 1)], GameContext.DIFFICULT_HARD);
                    }
                }
            }
            if (profile.starsHell != null && profile.starsHell.Length == CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL) * 3)
            {
                for (int i = 1; i <= GameContext.TOTAL_LEVEL; i++)
                {
                    for (int j = 1; j <= 3; j++)
                    {
                        CacheGame.SetNumStarLevelNamNX(i, j, 0, GameContext.DIFFICULT_HELL);
                    }
                }
                for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL); i++)
                {
                    for (int j = 1; j <= 3; j++)
                    {
                        Debug.Log("SetNumStarLevelNamNX : " + profile.starsHell[(i - 1) * 3 + (j - 1)]);
                        CacheGame.SetNumStarLevelNamNX(i, j, profile.starsHell[(i - 1) * 3 + (j - 1)], GameContext.DIFFICULT_HELL);
                    }
                }
            }
            if (profile.listSocial != null && profile.listSocial.Length > 8)
            {
                //like, join, insta follow, twitter follow
                Debug.Log("profile.listSocial[0] : " + profile.listSocial[0]);
                if (profile.listSocial[0] == 1)
                {
                    SocialManager.Instance.FBSetAlreadyLike();
                }
                Debug.Log("profile.listSocial[1] : " + profile.listSocial[1]);
                if (profile.listSocial[1] == 1)
                {
                    SocialManager.Instance.FBSetAlreadyJoinGroup();
                }
                Debug.Log("profile.listSocial[2] : " + profile.listSocial[2]);
                if (profile.listSocial[2] == 1)
                {
                    SocialManager.Instance.InstagramSetAlreadyFollow();
                }
                Debug.Log("profile.listSocial[3] : " + profile.listSocial[3]);
                if (profile.listSocial[3] == 1)
                {
                    SocialManager.Instance.TwitterSetAlreadyFollow();
                }

                //1, 5, 10, 20 friends
                Debug.Log("profile.listSocial[4] : " + profile.listSocial[4]);
                if (profile.listSocial[4] == 1)
                {
                    SocialManager.Instance.FBSetAlreadyClaimFriendsReward(1);
                }
                Debug.Log("profile.listSocial[5] : " + profile.listSocial[5]);
                if (profile.listSocial[5] == 1)
                {
                    SocialManager.Instance.FBSetAlreadyClaimFriendsReward(5);
                }
                Debug.Log("profile.listSocial[6] : " + profile.listSocial[6]);
                if (profile.listSocial[6] == 1)
                {
                    SocialManager.Instance.FBSetAlreadyClaimFriendsReward(10);
                }
                Debug.Log("profile.listSocial[7] : " + profile.listSocial[7]);
                if (profile.listSocial[7] == 1)
                {
                    SocialManager.Instance.FBSetAlreadyClaimFriendsReward(20);
                }
                SocialManager.Instance.FBSetNumberOfFriends(profile.listSocial[8]);
                Debug.Log("profile.dateShareSocial : " + profile.dateShareSocial);
                if (!string.IsNullOrEmpty(profile.dateShareSocial))
                {
                    DateTime dt = DateTime.ParseExact(profile.dateShareSocial, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    MinhCacheGame.SetFacebookShareDate(dt);
                }
            }
        }
        catch (Exception)
        {
            Debug.Log("error group 5");
            throw;
        }

        try
        {
            Debug.Log("Mathf.Max(CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL), CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD), CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL)) : " + Mathf.Max(CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL), CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD), CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL)));
            CacheGame.SetMaxLevel3Difficult(Mathf.Max(CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL), CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD), CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL)));
            Debug.Log("profile.vipPack : " + profile.vipPack);
            CacheGame.SetPurchasedVIPPack(profile.vipPack);
            Debug.Log("profile.starterPack : " + profile.starterPack);
            CacheGame.SetPurchasedStarterPack(profile.starterPack);

            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.AddCoinTop();
                PanelCoinGem.Instance.AddGemTop();
            }
            CacheGame.SetClaimedAllGiftDailyLogin(1);

            Debug.Log("profile.lastDayOpenGame : " + profile.lastDayOpenGame);
            CacheGame.SetLastDayOpenGame(profile.lastDayOpenGame);

            CacheGame.SetFirtSessionGame(1);
            CacheGame.SetTutorialGame(1);
            if (HomeScene.Instance != null)
            {
                HomeScene.Instance.SetActiveBtnLuckyWheel();
            }
            Debug.Log("profile.versionMinhDDV : " + profile.versionMinhDDV);
            MinhCacheGame.SetVersion(profile.versionMinhDDV);
            if (profile.hasConvertedNewDataMinhDDV == 1)
            {
                MinhCacheGame.SetAlreadyDataConverted1();
            }

            DateTime unixEpoch = DateTime.ParseExact("1970-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
            double dou = profile.premiumPackExpiredTime;
            DateTime m_dateTime = unixEpoch.AddMilliseconds(dou);
            Debug.Log("m_dateTime : " + m_dateTime);
            MinhCacheGame.SetPremiumPackExpiredTime(m_dateTime);
            Debug.Log("profile.alreadyPurchasePremiumPack : " + profile.alreadyPurchasePremiumPack);
            MinhCacheGame.SetAlreadyPurchasePremiumPack(profile.alreadyPurchasePremiumPack == 1 ? true : false);
            if (profile.listPromoteEventClaimed != null)
            {
                for (int i = 0; i < profile.listPromoteEventClaimed.Length; i++)
                {
                    if (profile.listPromoteEventClaimed[i] == 1)
                    {
                        MinhCacheGame.SetPromoteEventClaimed(i + 1, EventName.PROMOTE_EVENT_NAME);
                    }
                }
            }
        }
        catch (Exception)
        {
            Debug.Log("error group 6");
            throw;
        }

        try
        {
            if (profile.listDailyQuest != null)
            {
                string nameItem = "";
                for (int i = 1; i <= GameContext.TOTAL_TYPE_DAILYQUEST; i++)
                {
                    if (profile.listDailyQuest.Length > 3 * (i - 1) + 2)
                    {
                        nameItem = ((GameContext.TypeItemDailyQuest)i).ToString();
                        Debug.Log("profile.listDailyQuest[3 * (i - 1)] : " + profile.listDailyQuest[3 * (i - 1)]);
                        CacheGame.SetItemDailyQuestChooseShow(nameItem, profile.listDailyQuest[3 * (i - 1)]);
                        Debug.Log("profile.listDailyQuest[3 * (i - 1)] + 1 : " + profile.listDailyQuest[3 * (i - 1) + 1]);
                        CacheGame.SetCompleteItemDailyQuest(nameItem, profile.listDailyQuest[3 * (i - 1) + 1]);
                        Debug.Log("profile.listDailyQuest[3 * (i - 1)] + 2 : " + profile.listDailyQuest[3 * (i - 1) + 2]);
                        CacheGame.SetNumberDailyQuestAchieved(nameItem, profile.listDailyQuest[3 * (i - 1) + 2]);
                    }
                }

                for (int i = 1; i <= GameContext.TOTAL_TYPE_ACHIEVEMENTS; i++)
                {
                    nameItem = ((GameContext.TypeItemAchievements)i).ToString();
                    Debug.Log("profile.listAchievements[3 * (i - 1)] : " + profile.listAchievements[3 * (i - 1)]);
                    CacheGame.SetCompleteAllLevelAchievement(nameItem, profile.listAchievements[3 * (i - 1)]);
                    Debug.Log("profile.listAchievements[3 * (i - 1)] + 1 : " + profile.listAchievements[3 * (i - 1) + 1]);
                    CacheGame.SetCurrentLevelAchievement(nameItem, profile.listAchievements[3 * (i - 1) + 1]);
                    Debug.Log("profile.listAchievements[3 * (i - 1)] + 2 : " + profile.listAchievements[3 * (i - 1) + 2]);
                    CacheGame.SetNumberAchievementAchieved(nameItem, profile.listAchievements[3 * (i - 1) + 2]);
                }

                for (int i = 0; i < 5; i++)
                {
                    Debug.Log("profile.listTypeShowGiftBoxDailyQuest[i)] : " + profile.listTypeShowGiftBoxDailyQuest[i]);
                    CacheGame.SetTypeShowGiftBoxDailyQuest(i + 1, profile.listTypeShowGiftBoxDailyQuest[i]);
                }
                Debug.Log("profile.dateOpenDailyQuest : " + profile.dateOpenDailyQuest);
                CacheGame.SetDateOpenDailyQuest(profile.dateOpenDailyQuest);
            }
            Debug.Log("profile.numUseHourlyRewardOneDay : " + profile.numUseHourlyRewardOneDay);
            CacheGame.SetNumUseHourlyRewardOneDay(profile.numUseHourlyRewardOneDay);
            Debug.Log("profile.numCardGetToday : " + profile.numCardGetToday);
            CacheGame.SetNumCardGetToday(profile.numCardGetToday);
            Debug.Log("profile.numGemGetToday : " + profile.numGemGetToday);
            CacheGame.SetNumGemGetToday(profile.numGemGetToday);
            Debug.Log("profile.numEnergyGetToday : " + profile.numEnergyGetToday);
            CacheGame.SetNumEnergyGetToday(profile.numEnergyGetToday);
            Debug.Log("profile.numUseVideoEndGameOneDay : " + profile.numUseVideoEndGameOneDay);
            CacheGame.SetNumUseVideoEndGameOneDay(profile.numUseVideoEndGameOneDay);
            Debug.Log("profile.selectedShipTournament : " + profile.selectedShipTournament);
            if (profile.selectedShipTournament == 0)
            {
                CacheGame.SetSpaceShipTournament(CacheGame.GetSpaceShipTournament());
            }
            else
            {
                CacheGame.SetSpaceShipTournament(profile.selectedShipTournament);
            }
            Debug.Log("profile.selectedLeftWingman : " + profile.selectedLeftWingman);
            if (profile.selectedLeftWingman == 0)
            {
                CacheGame.SetWingManLeftUserSelected(CacheGame.GetWingManLeftUserSelected());
            }
            else
            {
                CacheGame.SetWingManLeftUserSelected(profile.selectedLeftWingman);
            }
            Debug.Log("profile.selectedRightWingman : " + profile.selectedRightWingman);
            if (profile.selectedRightWingman == 0)
            {
                CacheGame.SetWingManRightUserSelected(CacheGame.GetWingManRightUserSelected());
            }
            else
            {
                CacheGame.SetWingManRightUserSelected(profile.selectedRightWingman);
            }
            Debug.Log("profile.lastDayOf30Days : " + profile.lastDayOf30Days);
            if (profile.lastDayOf30Days == 0)
            {
                MinhCacheGame.SetLastDayOf30DaysForServer(MinhCacheGame.GetLastDayOf30Days());
            }
            else
            {
                MinhCacheGame.SetLastDayOf30DaysForServer(profile.lastDayOf30Days);
            }
            Debug.Log("profile.rewardIndexDayOf30Days : " + profile.rewardIndexDayOf30Days);
            if (profile.rewardIndexDayOf30Days == 0)
            {
                MinhCacheGame.SetRewardIndexOf30Days(MinhCacheGame.GetRewardIndexOf30Days());
            }
            else
            {
                MinhCacheGame.SetRewardIndexOf30Days(profile.rewardIndexDayOf30Days);
            }
            if (profile.starsChestNormal != null)
            {
                for (int i = 0; i < profile.starsChestNormal.Length; i++)
                {
                    if (profile.starsChestNormal[i] == 1)
                    {
                        MinhCacheGame.SetStarChestClaimed(Mathf.CeilToInt((i + 1) / 3f), GameContext.DIFFICULT_NOMAL, i % 3 + 1);
                        Debug.LogError("i: " + i + ", world: " + Mathf.CeilToInt((i + 1) / 3f) + ", index: " + (i % 3 + 1));
                    }
                }
            }

            if (profile.starsChestHard != null)
            {
                for (int i = 0; i < profile.starsChestHard.Length; i++)
                {
                    if (profile.starsChestHard[i] == 1)
                    {
                        MinhCacheGame.SetStarChestClaimed(Mathf.CeilToInt((i + 1) / 3f), GameContext.DIFFICULT_HARD, i % 3 + 1);
                    }
                }
            }

            if (profile.starsChestHell != null)
            {
                for (int i = 0; i < profile.starsChestHell.Length; i++)
                {
                    if (profile.starsChestHell[i] == 1)
                    {
                        MinhCacheGame.SetStarChestClaimed(Mathf.CeilToInt((i + 1) / 3f), GameContext.DIFFICULT_HELL, i % 3 + 1);
                    }
                }
            }

            if (profile.starsNormalExtra != null)
            {
                for (int i = 0, k = 0; i < profile.starsNormalExtra.Length / 3; i++)
                {
                    if (i % 2 == 0)
                    {
                        k += 3;
                    }
                    else
                    {
                        k += 4;
                    }
                    for (int j = 1; j <= 3; j++)
                    {
                        CacheGame.SetNumStarLevel(k, j, profile.starsNormalExtra[i * 3 + (j - 1)], GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Extra);
                    }
                }
            }

            if (profile.starsHardExtra != null)
            {
                for (int i = 0, k = 0; i < profile.starsHardExtra.Length / 3; i++)
                {
                    if (i % 2 == 0)
                    {
                        k += 3;
                    }
                    else
                    {
                        k += 4;
                    }
                    for (int j = 1; j <= 3; j++)
                    {
                        CacheGame.SetNumStarLevel(k, j, profile.starsHardExtra[i * 3 + (j - 1)], GameContext.DIFFICULT_HARD, GameContext.LevelCampaignMode.Extra);
                    }
                }
            }

            if (profile.starsHellExtra != null)
            {
                for (int i = 0, k = 0; i < profile.starsHellExtra.Length / 3; i++)
                {
                    if (i % 2 == 0)
                    {
                        k += 3;
                    }
                    else
                    {
                        k += 4;
                    }
                    for (int j = 1; j <= 3; j++)
                    {
                        CacheGame.SetNumStarLevel(k, j, profile.starsHellExtra[i * 3 + (j - 1)], GameContext.DIFFICULT_HELL, GameContext.LevelCampaignMode.Extra);
                    }
                }
            }

            if (profile.passedLevelExtraNormal != null)
            {
                for (int i = 0, k = 0; i < profile.passedLevelExtraNormal.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        k += 3;
                    }
                    else
                    {
                        k += 4;
                    }
                    CacheGame.SetPassedLevelExtra(k, GameContext.DIFFICULT_NOMAL, profile.passedLevelExtraNormal[i]);
                }
            }
            if (profile.passedLevelExtraHard != null)
            {
                for (int i = 0, k = 0; i < profile.passedLevelExtraHard.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        k += 3;
                    }
                    else
                    {
                        k += 4;
                    }
                    CacheGame.SetPassedLevelExtra(k, GameContext.DIFFICULT_HARD, profile.passedLevelExtraHard[i]);
                }
            }
            if (profile.passedLevelExtraHell != null)
            {
                for (int i = 0, k = 0; i < profile.passedLevelExtraHell.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        k += 3;
                    }
                    else
                    {
                        k += 4;
                    }
                    CacheGame.SetPassedLevelExtra(k, GameContext.DIFFICULT_HELL, profile.passedLevelExtraHell[i]);
                }
            }
            if (profile.starsChestExtraNormal != null)
            {
                for (int i = 0; i < profile.starsChestExtraNormal.Length; i++)
                {
                    if (profile.starsChestExtraNormal[i] == 1)
                    {
                        MinhCacheGame.SetStarChestExtraClaimed(Mathf.CeilToInt(i + 1), GameContext.DIFFICULT_NOMAL);
                    }
                }
            }
            if (profile.starsChestExtraHard != null)
            {
                for (int i = 0; i < profile.starsChestExtraHard.Length; i++)
                {
                    if (profile.starsChestExtraHard[i] == 1)
                    {
                        MinhCacheGame.SetStarChestExtraClaimed(Mathf.CeilToInt(i + 1), GameContext.DIFFICULT_HARD);
                    }
                }
            }
            if (profile.starsChestExtraHell != null)
            {
                for (int i = 0; i < profile.starsChestExtraHell.Length; i++)
                {
                    if (profile.starsChestExtraHell[i] == 1)
                    {
                        MinhCacheGame.SetStarChestExtraClaimed(Mathf.CeilToInt(i + 1), GameContext.DIFFICULT_HELL);
                    }
                }
            }
            MinhCacheGame.SetEventResources(EventItemType.IceCream_4_2019.ToString(), profile.eventResourceSummer_Holiday_2019);
            if (profile.eventMissionProgressSummer_Holiday_2019 != null)
            {
                for (int i = 0; i < profile.eventMissionProgressSummer_Holiday_2019.Length; i++)
                {
                    MinhCacheGame.SetEventMissionProgress(i + 1, profile.eventMissionProgressSummer_Holiday_2019[i]);
                }
            }
        }
        catch (Exception)
        {
            Debug.Log("error group 7");
            throw;
        }

        MessageDispatcher.SendMessage(PopupManager.Instance, EventName.LoadProfile.LoadedProfile.ToString(), 0, 0);
    }

    static int[] vipArray = new int[] { 1, 5, 10, 20, 50, 100, 200, 300, 500, 1000 };

    public static int myVipPoint
    {
        get
        {
            return AntiCheat.GetIntConverted(VIP_POINT, VIP_POINT_NEW, 0);
            //return PlayerPrefs.GetInt(VIP_POINT, 0);
        }
        set
        {
            //PlayerPrefs.SetInt(VIP_POINT, value);
            AntiCheat.SetIntConverted(VIP_POINT_NEW, value);
            VipBonusValue.GetVipBonusValue();
            //1
            MessageDispatcher.SendMessage(EventName.ChangeProperties.ChangeVip.ToString());
        }
    }
    static int _myVip = -1;
    public static int myVip
    {
        get
        {
            int oldMyVip = _myVip;
            _myVip = GetVipFromVipPoint(myVipPoint);
            Debug.Log("myvip : " + _myVip);
            Debug.Log("oldMyVip : " + oldMyVip);
            Debug.Log("CachePVP.RestoreVip : " + RestoreVip);

            if (oldMyVip != -1 && _myVip != oldMyVip && RestoreVip == 0)
            {
                LeanTween.delayedCall(2, () =>
                {
                    if (SceneManager.GetActiveScene().name == "InitScene" || SceneManager.GetActiveScene().name == "Home" || SceneManager.GetActiveScene().name == "SelectLevel")
                    {
                        PopupManager.Instance.ShowCongratulationVip();
                        Debug.Log("PopupManager.Instance.ShowCongratulationVip() after change Vip");
                    }
                }).setIgnoreTimeScale(true);
            }
            return _myVip;
        }
        set
        {
            _myVip = value;
        }
    }

    public static int CanReceiveNotify
    {
        get
        {
            return PlayerPrefs.GetInt(CAN_RECEIVE_NOTIFY, 1);
        }
        set
        {
            PlayerPrefs.SetInt(CAN_RECEIVE_NOTIFY, value);
            PvpUtil.SendUpdatePlayer("CanReceiveNotify");
        }
    }

    public static int IsSentValidateId
    {
        get
        {
            return PlayerPrefs.GetInt(IS_SENT_VALIDATE_ID, 0);
        }
        set
        {
            PlayerPrefs.SetInt(IS_SENT_VALIDATE_ID, value);
        }
    }

    public void ShareFacebookToServer()
    {
        new CSShareFacebook().Send();
    }

    public void SpinDaily()
    {
        new CSDailySpin().Send();
    }

    public void AchievementCompleted(string name)
    {
        new CSAchievementCompleted(name).Send();
    }

    public static bool isHomeOK = false;
    public static bool isSendLoadingTime = false;

    private static List<int> _listOrdersGold = new List<int>();

    public static List<int> listOrdersGold
    {
        get
        {
            JSONObject jo = OrderShopGold.ToJsonObject();
            _listOrdersGold.Clear();
            for (int i = 0; i < jo.Count; i++)
            {
                _listOrdersGold.Add(jo[i].ToInt());
            }
            return _listOrdersGold;
        }
        set
        {
            _listOrdersGold = value;
            JSONObject jo = "[]".ToJsonObject();
            for (int i = 0; i < _listOrdersGold.Count; i++)
            {
                jo.AddItem(_listOrdersGold[i]);
            }
            OrderShopGold = jo.ToJson();
        }
    }

    private static List<int> _listOrdersGem = new List<int>();

    public static List<int> listOrdersGem
    {
        get
        {
            JSONObject jo = OrderShopGem.ToJsonObject();
            _listOrdersGem.Clear();
            for (int i = 0; i < jo.Count; i++)
            {
                _listOrdersGem.Add(jo[i].ToInt());
            }
            return _listOrdersGem;
        }
        set
        {
            _listOrdersGem = value;
            JSONObject jo = "[]".ToJsonObject();
            for (int i = 0; i < _listOrdersGem.Count; i++)
            {
                jo.AddItem(_listOrdersGem[i]);
            }
            OrderShopGem = jo.ToJson();
        }
    }

    private static List<int> _listPromotionsGold = new List<int>();

    public static List<int> listPromotionsGold
    {
        get
        {
            JSONObject jo = PromotionShopGold.ToJsonObject();
            _listPromotionsGold.Clear();
            for (int i = 0; i < jo.Count; i++)
            {
                _listPromotionsGold.Add(jo[i].ToInt());
            }
            return _listPromotionsGold;
        }
        set
        {
            _listPromotionsGold = value;
            JSONObject jo = "[]".ToJsonObject();
            for (int i = 0; i < _listPromotionsGold.Count; i++)
            {
                jo.AddItem(_listPromotionsGold[i]);
            }
            PromotionShopGold = jo.ToJson();
        }
    }

    private static List<int> _listPromotionsGem = new List<int>();

    public static List<int> listPromotionsGem
    {
        get
        {
            JSONObject jo = PromotionShopGem.ToJsonObject();
            _listPromotionsGem.Clear();
            for (int i = 0; i < jo.Count; i++)
            {
                _listPromotionsGem.Add(jo[i].ToInt());
            }
            return _listPromotionsGem;
        }
        set
        {
            _listPromotionsGem = value;
            JSONObject jo = "[]".ToJsonObject();
            for (int i = 0; i < _listPromotionsGem.Count; i++)
            {
                jo.AddItem(_listPromotionsGem[i]);
            }
            PromotionShopGem = jo.ToJson();
        }
    }

    public static bool NeedReloadMailBox = true;

    public static string FalconMailJson
    {
        get
        {
            return PlayerPrefs.GetString(FALCON_MAIL, "");
        }
        set
        {
            PlayerPrefs.SetString(FALCON_MAIL, value);
        }
    }

    public static string OrderShopGold
    {
        get
        {
            return PlayerPrefs.GetString(ORDER_SHOP_GOLD_PLAYERPREFS_KEY, "[1,2,3,4,5,6]");
        }
        set
        {
            PlayerPrefs.SetString(ORDER_SHOP_GOLD_PLAYERPREFS_KEY, value);
        }
    }

    public static string OrderShopGem
    {
        get
        {
            return PlayerPrefs.GetString(ORDER_SHOP_GEM_PLAYERPREFS_KEY, "[1,2,3,4,5,6]");
        }
        set
        {
            PlayerPrefs.SetString(ORDER_SHOP_GEM_PLAYERPREFS_KEY, value);
        }
    }

    public const int CLAN_LOADING = -2;
    public const int HAS_NO_CLAN = -1;
    public const int CLAN_MEMBER = 0;
    public const int CLAN_VICE_MASTER = 1;
    public const int CLAN_MASTER = 2;
    public static int TypeMemberClan = -2;

    public static int RestoreVip
    {
        get
        {
            return PlayerPrefs.GetInt(RESTORE_VIP, 0);
        }
        set
        {
            PlayerPrefs.SetInt(RESTORE_VIP, value);
        }
    }

    public static string PromotionShopGold
    {
        get
        {
            return PlayerPrefs.GetString(PROMOTION_SHOP_GOLD_PLAYERPREFS_KEY, "[]");
        }
        set
        {
            PlayerPrefs.SetString(PROMOTION_SHOP_GOLD_PLAYERPREFS_KEY, value);
        }
    }

    public static string PromotionShopGem
    {
        get
        {
            return PlayerPrefs.GetString(PROMOTION_SHOP_GEM_PLAYERPREFS_KEY, "[]");
        }
        set
        {
            PlayerPrefs.SetString(PROMOTION_SHOP_GEM_PLAYERPREFS_KEY, value);
        }
    }

    public static string ServerUrl
    {
        get
        {
            return PlayerPrefs.GetString(SERVER_URL_PLAYERPREFS_KEY, PvpUtil.SERVER_URL_PVP);
        }
        set
        {
            PlayerPrefs.SetString(SERVER_URL_PLAYERPREFS_KEY, value);
        }
    }

    public static DateTime dateTime
    {
        get
        {
            DateTime d;
            Debug.Log("timeFromServer : " + timeFromServer);
            bool parse = DateTime.TryParseExact(timeFromServer, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out d);
            Debug.Log("parse : " + parse);
            if (parse)
            {
                return d;
            }
            else
            {
                return DateTime.Now;
            }
        }
    }

    public static string timeFromServer
    {
        get
        {
            return PlayerPrefs.GetString(DATE_TIME_SERVER, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
        }
        set
        {
            PlayerPrefs.SetString(DATE_TIME_SERVER, value);
            Debug.Log("timeFromServer : " + timeFromServer);
        }
    }

    public static int NewMailNumber
    {
        get
        {
            return PlayerPrefs.GetInt(NEW_MAIL_NUMBER, 0);
        }
        set
        {
            PlayerPrefs.SetInt(NEW_MAIL_NUMBER, value);
            MessageDispatcher.SendMessage(EventName.LoadMailBox.ChangeNewMailNumber.ToString());
        }
    }

    public static int NotifyTournament
    {
        get
        {
            return PlayerPrefs.GetInt(NOTIFY_TOURNAMENT, 0);
        }
        set
        {
            PlayerPrefs.SetInt(NOTIFY_TOURNAMENT, value);
        }
    }

    public static int NewMailNumberForce
    {
        get
        {
            return PlayerPrefs.GetInt(NEW_MAIL_NUMBER_FORCE, 0);
        }
        set
        {
            PlayerPrefs.SetInt(NEW_MAIL_NUMBER_FORCE, value);
            MessageDispatcher.SendMessage(EventName.LoadMailBox.ChangeNewMailNumber.ToString());
        }
    }

    public static int FirstTabIndex
    {
        get
        {
            return PlayerPrefs.GetInt(FIRST_TAB_INDEX, 0);
        }
        set
        {
            PlayerPrefs.SetInt(FIRST_TAB_INDEX, value);
        }
    }

    public static int CountFriends
    {
        get
        {
            return PlayerPrefs.GetInt(COUNT_FRIENDS, 0);
        }
        set
        {
            PlayerPrefs.SetInt(COUNT_FRIENDS, value);
        }
    }

    public static int CountFriendsMinh
    {
        get
        {
            return PlayerPrefs.GetInt(COUNT_FRIENDS_MINH, 0);
        }
        set
        {
            PlayerPrefs.SetInt(COUNT_FRIENDS_MINH, value);
        }
    }

    public static int RequireLevelPlayPvp
    {
        get
        {
            return PlayerPrefs.GetInt(REQUIRE_LEVEL_PLAYERPREFS_KEY, PvpUtil.REQUIRE_LEVEL_PLAY_PVP);
        }
        set
        {
            PlayerPrefs.SetInt(REQUIRE_LEVEL_PLAYERPREFS_KEY, value);
        }
    }

    public static int RequireLevelPlay2v2
    {
        get
        {
            return PlayerPrefs.GetInt(REQUIRE_LEVEL_2V2_PLAYERPREFS_KEY, PvpUtil.REQUIRE_LEVEL_PLAY_2V2);
        }
        set
        {
            PlayerPrefs.SetInt(REQUIRE_LEVEL_2V2_PLAYERPREFS_KEY, value);
        }
    }

    public static int RequireLevelPlayerInfo
    {
        get
        {
            return PlayerPrefs.GetInt(REQUIRE_LEVEL_PLAYERINFO_PLAYERPREFS_KEY, PvpUtil.REQUIRE_LEVEL_UNLOCK_PLAYERINFO);
        }
        set
        {
            PlayerPrefs.SetInt(REQUIRE_LEVEL_PLAYERINFO_PLAYERPREFS_KEY, value);
        }
    }

    public static int CanPlayPvp
    {
        get
        {
            return PlayerPrefs.GetInt(CAN_PLAY_PVP, PvpUtil.CAN_PLAY_PVP_VALUE);
        }
        set
        {
            PlayerPrefs.SetInt(CAN_PLAY_PVP, value);
        }
    }

    public static long EndlessValue
    {
        get
        {
            return long.Parse(PlayerPrefs.GetString(ENDLESS_VALUE_PLAYERPREFS_KEY, "0"));
        }
        set
        {
            PlayerPrefs.SetString(ENDLESS_VALUE_PLAYERPREFS_KEY, "" + value);
        }
    }

    public static string Token
    {
        get
        {
            return PlayerPrefs.GetString(TOKEN_PLAYERPREFS_KEY, "");
        }
        set
        {
            PlayerPrefs.SetString(TOKEN_PLAYERPREFS_KEY, value);
        }
    }

    public static string Name
    {
        get
        {
            return PlayerPrefs.GetString(NAME_PLAYERPREFS_KEY, "");
        }
        set
        {
            PlayerPrefs.SetString(NAME_PLAYERPREFS_KEY, value);
        }
    }

    public static string Code
    {
        get
        {
            return PlayerPrefs.GetString(CODE_PLAYERPREFS_KEY, "");
        }
        set
        {
            PlayerPrefs.SetString(CODE_PLAYERPREFS_KEY, value);
            FalconFirebaseLogger.UserID = value;
        }
    }

    public static string Country
    {
        get
        {
            return PlayerPrefs.GetString(COUNTRY_PLAYERPREFS_KEY, "");
        }
        set
        {
            PlayerPrefs.SetString(COUNTRY_PLAYERPREFS_KEY, value);
            MessageDispatcher.SendMessage(EventName.LoadProfile.LoadedProfile.ToString());
        }
    }

    public static string deviceId
    {
        get
        {
            return PlayerPrefs.GetString(DEVICEID_PLAYERPREFS_KEY, "");
        }
        set
        {
            PlayerPrefs.SetString(DEVICEID_PLAYERPREFS_KEY, value);
        }
    }

    public static string FacebookId
    {
        get
        {
            return PlayerPrefs.GetString(FBID_PLAYERPREFS_KEY, "");
        }
        set
        {
            PlayerPrefs.SetString(FBID_PLAYERPREFS_KEY, value);
        }
    }

    public static string GamecenterId
    {
        get
        {
            return PlayerPrefs.GetString(GCID_PLAYERPREFS_KEY, "");
        }
        set
        {
            PlayerPrefs.SetString(GCID_PLAYERPREFS_KEY, value);
        }
    }

    public static int PlayTime { get; set; }

    public static string Avatar
    {
        get
        {
            return PlayerPrefs.GetString(AVATAR_PLAYERPREFS_KEY, "");
        }
        set
        {
            PlayerPrefs.SetString(AVATAR_PLAYERPREFS_KEY, value);
        }
    }

    public static int Win
    {
        get
        {
            return PlayerPrefs.GetInt(WIN_PLAYERPREFS_KEY, 0);
        }
        set
        {
            PlayerPrefs.SetInt(WIN_PLAYERPREFS_KEY, value);
        }
    }


    public static int Lose
    {
        get
        {
            return PlayerPrefs.GetInt(LOSE_PLAYERPREFS_KEY, 0);
        }
        set
        {
            PlayerPrefs.SetInt(LOSE_PLAYERPREFS_KEY, value);
        }
    }

    public static int Exp
    {
        get
        {
            return PlayerPrefs.GetInt(EXP_PLAYERPREFS_KEY, 0);
        }
        set
        {
            PlayerPrefs.SetInt(EXP_PLAYERPREFS_KEY, value);
        }
    }

    public static int Medal
    {
        get
        {
            return PlayerPrefs.GetInt(MEDAL_PLAYERPREFS_KEY, 0);
        }
        set
        {
            PlayerPrefs.SetInt(MEDAL_PLAYERPREFS_KEY, value);
            MessageDispatcher.SendMessage(EventName.Clan.ChangeMedal.ToString());
        }
    }

    public static void SetGold(int gold, string why, string where)
    {
        CacheGame.SetTotalCoin(gold, why, where);
    }

    public static int GetGold()
    {
        return CacheGame.GetTotalCoin();
    }

    public static int Ranking
    {
        get
        {
            return PlayerPrefs.GetInt(RANKING_PLAYERPREFS_KEY, 0);
        }
        set
        {
            PlayerPrefs.SetInt(RANKING_PLAYERPREFS_KEY, value);
        }
    }


    public static int PvpVersion
    {
        get
        {
            return PlayerPrefs.GetInt(PVP_ASSET_BUNDLE_VERSION_KEY, PvpUtil.PVP_VERSION);
        }
        set
        {
            PlayerPrefs.SetInt(PVP_ASSET_BUNDLE_VERSION_KEY, value);
        }
    }

    public static string FcmToken
    {
        get
        {
            return PlayerPrefs.GetString(FCM_TOKEN, "");
        }
        set
        {
            PlayerPrefs.SetString(FCM_TOKEN, value);
        }
    }

    public static void ClearMultiplayerData()
    {
        PlayerPrefs.DeleteKey(TOKEN_PLAYERPREFS_KEY);
        PlayerPrefs.DeleteKey(NAME_PLAYERPREFS_KEY);
        PlayerPrefs.DeleteKey(CODE_PLAYERPREFS_KEY);
        PlayerPrefs.DeleteKey(FBID_PLAYERPREFS_KEY);
    }
}
