using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class Rank
    {
        public int myRank;
        public int type;
        public int from;
        public int to;
        public List<Player> players;
        public Rank()
        {
        }

        public Rank(int type, List<Player> players)
        {
            this.type = type;
            this.players = players;
        }
    }
}