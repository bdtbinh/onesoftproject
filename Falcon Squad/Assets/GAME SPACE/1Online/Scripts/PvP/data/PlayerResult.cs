using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class PlayerResult
    {
        public const int DIED = 1;
        public const int OUT = 2;
        public const int READY = 3;
        public const int UNREADY = 4;
        public const int EXIT = 5;
        public int state;
        public int exp;
        public int score;
        public int wave;
        public const int WIN = 1;
        public const int LOSE = -1;
        public const int DRAWN = 0;
        public int result;
        //public int medal;
        public int goldBet;
        public PlayerLevel playerLevel;
        public MegaTournamentInfo tournamentInfo;
        public string datas;
        public string code;
        public string name;
        public bool connected = true;
        public int spaceship;
        public string facebookId;
        public string appCenterId;
        public string country;
        public int vipPoint;
        public int selectedShip;
        public List<int> listRankPlane;
        public PlayerResult()
        {
        }

        //public PlayerResult(int state, int exp, int score, int wave, int result, int medal, int goldBet, string datas, string accountId, string name, bool connected, int spaceship)
        public PlayerResult(int state, int exp, int score, int wave, int result, PlayerLevel playerLevel, int goldBet, string datas, string accountId, string name, bool connected, int spaceship)
        {
            this.state = state;
            this.exp = exp;
            this.score = score;
            this.wave = wave;
            this.result = result;
            //this.medal = medal;
            this.goldBet = goldBet;
            this.datas = datas;
            this.code = accountId;
            this.name = name;
            this.connected = connected;
            this.spaceship = spaceship;
            this.playerLevel = playerLevel;
        }

        public virtual bool IsConnected()
        {
            return connected;
        }
    }
}