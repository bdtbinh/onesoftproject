using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [System.Serializable]
    public class Player
    {
        public int canReceiveNotify; //0: can't receive notify, 1 : can receive notify
        public string code;
        public string name;
        public string country;
        public string facebookId;
        public string appCenterId;
        public string avatar;
        public int win;
        public int lose;
        // level
        public PlayerLevel playerLevel;
        public PlayerLevel megaPlayerLevel;
        public PlayerLevel pvp2vs2PlayerLevel;
        public PlayerClan playerClan;
        public FriendshipStats friendshipStats;
        public PvP2vs2SeasonData pvP2vs2SeasonData;
        //public int exp;
        public int medal; //medal for shopclan
        public int gold;
        public int diamond;
        public int energy;
        public long endlessValue;
        public long levelValue;
        public int ranking = 1;
        public int status;
        public long createDate;
        public long lastUpdate;
        public bool offline;
        public string data;
        public bool debug;
        public int vip;
        public long lastDisconnectedDate;

        public virtual bool IsOffline()
        {
            return offline;
        }

        public override bool Equals(Object o)
        {
            if (o == null)
            {
                return false;
            }

            if (!(o is Player))
            {
                return false;
            }

            Player player = (Player)o;
            return (player.code.Equals(code));
        }

        public override int GetHashCode()
        {
            return code.GetHashCode();
        }

        public static string GetData()
        {
            return "";
        }
    }

    public class PlayerLevel
    {
        public string code = "BRONZE";
        public string name;
        public int medal = 0;
        public int exp = 0;
        public int elo = 1000;
    }
}

public class PlayerClan
{

    // state for status property
    public const int STATE_WAITING_CONFIRM = 0;
    public const int STATE_ACCEPTED = 1;
    public const int STATE_REJECTED = -1;
    public const int STATE_LEAVED = 2;
    public const int STATE_DESTROYED_JOIN_REQUEST = 3;
    public const int STATE_KICKED = 4;

    public long id;
    public string name;
    public string country;
    public int status;
    public int type;
    public long score; //glory
    public long scoreTournament; //score trong tournament hien tai
}

public class FriendshipStats
{
    public int numFriend;
    public int numRequested;
    public int numFollow;
    public int numFollower;
    public int numFriendFb;
}

public class PvP2vs2SeasonData
{
    public string chestName;
    public int score;
    public int win;
    public int lose;
}