using System;
using System.Collections.Generic;
using OSNet;
using UnityEngine;

namespace Mp.Pvp {
	public class PlayerRanking {
		public string id = "";
		public string code = "";
		public string name = "";
		public string country = "";
		public string facebookId = "";
		public string avatar = "";
		public int win;
		public int lose;
		public int exp;
		public int medal;
		public int gold;
		public long levelValue;
		public int ranking;
		public Sprite spriteAvatar;

		public override bool Equals (System.Object o) {
			if (o == null) {
				return false;
			}

			if (!(o is PlayerRanking)) {
				return false;
			}

			PlayerRanking player = (PlayerRanking)o;
			return (player.id.Equals (id));
		}

		public override int GetHashCode () {
			return id.GetHashCode ();
		}
	}
}