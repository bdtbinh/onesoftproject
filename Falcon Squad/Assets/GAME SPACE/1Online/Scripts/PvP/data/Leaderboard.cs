using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class Leaderboard
    {
        public int type;
        public Player player;
        public List<Player> topRanking;
        public Leaderboard() 
        {
        }
        public Leaderboard(int type, List<Player> topRanking): this (type, null, topRanking)
        {
        }

        public Leaderboard(int type, Player player, List<Player> topRanking)
        {
            this.type = type;
            this.player = player;
            this.topRanking = topRanking;
        }
    }
}