using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    public class RoomInfo
    {
        public const int END = 2;
        public const int START = 1;
        public const int WAIT_FOR_READY = 0;
        public string roomId;
        public int state;
        public List<PlayerResult> playerResults;
        public int map;
        public long timeRemaining;        
    }
}