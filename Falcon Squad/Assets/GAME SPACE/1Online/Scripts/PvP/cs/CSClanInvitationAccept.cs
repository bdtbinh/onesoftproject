﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanInvitationAccept : CSMessage
    {
        public long clanId;
        public CSClanInvitationAccept(long clanId)
        {
            this.clanId = clanId;
        }

        public override string GetEvent()
        {
            return Events.CLAN_INVITATION_ACCEPT;
        }
    }
}