﻿using OSNet;
using System;

namespace Mp.Pvp
{
    [Serializable]
    public class CSBuyRoyaltyPack : CSMessage
    {
        public override string GetEvent()
        {
            return Events.BUY_ROYALTY_PACK;
        }
    }
}
