using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSNotifyClick : CSMessage
    {
        public long id;
		public int ok; //1 : click ok, 0 : click cancel
        public CSNotifyClick(long id, int ok)
        {
            this.id = id;
			this.ok = ok;
        }

        public override string GetEvent()
        {
            return Events.NOTIFY_CLICK;
        }
    }
}