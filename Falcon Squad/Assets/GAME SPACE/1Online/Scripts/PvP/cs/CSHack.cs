﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSHack : CSMessage
    {
        public string key;
        public string value;
        public CSHack(string key, string value)
        {
            this.key = key;
            this.value = value;
        }

        public override string GetEvent()
        {
            return Events.HACK_DETECTOR;
        }
    }
}