﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSTournamentClaim : CSMessage
    {
        public bool reward; //false : get info
        public CSTournamentClaim(bool reward)
        {
            this.reward = reward;
        }

        public override string GetEvent()
        {
            return Events.CLAN_TOURNAMENT_CLAIM;
        }
    }
}