﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSShopBuyItem : CSMessage
    {
        public int itemId;
        public CSShopBuyItem(int itemId)
        {
            this.itemId = itemId;
        }

        public override string GetEvent()
        {
            return Events.SHOP_BUY_ITEM;
        }
    }
}