﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSRandomShopBuyItem : CSMessage
    {
        public int itemId;
        public CSRandomShopBuyItem(int itemId)
        {
            this.itemId = itemId;
        }

        public override string GetEvent()
        {
            return Events.RANDOM_SHOP_BUY_ITEM;
        }
    }
}

