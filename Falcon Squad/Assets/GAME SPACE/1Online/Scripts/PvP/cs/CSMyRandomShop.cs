﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMyRandomShop : CSMessage
    {
        public override string GetEvent()
        {
            return Events.MY_RANDOM_SHOP;
        }
    }
}