using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMyClanRequest : CSMessage
    {
        public override string GetEvent()
        {
            return Events.MY_CLAN_REQUEST;
        }
    }
}