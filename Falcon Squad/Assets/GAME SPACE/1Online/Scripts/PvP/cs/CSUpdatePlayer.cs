using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSUpdatePlayer : CSMessage
    {
        public Player player;
        public string deviceId;
        public string why;
        public string where;

        public CSUpdatePlayer(Player player, string deviceId, string why, string where)
        {
            this.player = player;
            this.deviceId = deviceId;
            this.why = why;
            this.where = where;
        }

        public override string GetEvent()
        {
            return Events.UPDATE_PLAYER;
        }
    }
}