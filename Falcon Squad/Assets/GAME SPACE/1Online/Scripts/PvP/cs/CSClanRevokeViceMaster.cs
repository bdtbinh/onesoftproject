﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanRevokeViceMaster : CSMessage
    {
        public string member;
        public CSClanRevokeViceMaster(string member)
        {
            this.member = member;
        }

        public override string GetEvent()
        {
            return Events.CLAN_REVOKE_VICE_MASTER;
        }
    }
}