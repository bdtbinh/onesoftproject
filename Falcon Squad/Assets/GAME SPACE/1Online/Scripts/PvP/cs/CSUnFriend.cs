﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSUnFriend : CSMessage
    {
        public string code;//ID của bạn bè muốn hủy
        public CSUnFriend(string code)
        {
            this.code = code;
        }

        public override string GetEvent()
        {
            return Events.UNFRIEND;
        }
    }
}