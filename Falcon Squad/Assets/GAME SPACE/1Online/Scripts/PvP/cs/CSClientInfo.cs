﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OSNet;
using Mp.Pvp;
using System;

[Serializable]
public class CSClientInfo : CSMessage
{
	public int pvpVersion = CachePvp.PvpVersion;
	public string fcmToken = CachePvp.FcmToken;

	public override string GetEvent()
	{
		return Events.CS_CLIENT_INFO;
	}
}
