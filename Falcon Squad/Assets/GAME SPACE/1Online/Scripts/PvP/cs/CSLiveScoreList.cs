﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSLiveScoreList : CSMessage
    {
        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;

        public int type;

        public CSLiveScoreList(int type)
        {
            this.type = type;
        }

        public override string GetEvent()
        {
            return Events.LIVE_SCORE_LIST;
        }
    }
}