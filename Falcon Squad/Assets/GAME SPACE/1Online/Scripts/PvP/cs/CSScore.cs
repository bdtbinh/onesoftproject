using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSScore : CSMessage
    {
        public int score;
        public int wave;
        public string datas;
        public CSScore(int score, int wave, string datas)
        {
            this.score = score;
            this.wave = wave;
            this.datas = datas;
        }

        public override string GetEvent()
        {
            return Events.SCORE;
        }
    }
}