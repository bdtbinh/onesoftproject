﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMyFriends : CSMessage
    {
        public override string GetEvent()
        {
            return Events.MY_FRIENDS;
        }
    }
}