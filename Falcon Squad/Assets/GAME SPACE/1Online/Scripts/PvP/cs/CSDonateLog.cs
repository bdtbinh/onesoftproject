﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSDonateLog : CSMessage
    {
        public long clanId;
        public CSDonateLog(long clanId)
        {
            this.clanId = clanId;
        }

        public override string GetEvent()
        {
            return Events.CLAN_DONATE_LOG;
        }
    }
}