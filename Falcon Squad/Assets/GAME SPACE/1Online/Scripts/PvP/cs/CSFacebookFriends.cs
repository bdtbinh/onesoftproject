using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSFacebookFriends : CSMessage
    {
        public string facebookIds;

        public CSFacebookFriends(string facebookIds)
        {
            this.facebookIds = facebookIds;
        }

        public override string GetEvent()
        {
			return Events.FACEBOOK_FRIENDS;
        }
    }
}