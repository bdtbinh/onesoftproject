﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMegaTournamentClaim : CSMessage
    {
        public int key;
        public bool bonus;
        public CSMegaTournamentClaim(int key, bool bonus)
        {
            this.key = key;
            this.bonus = bonus;
        }

        public override string GetEvent()
        {
            return Events.MEGA_TOURNAMENT_CLAIM;
        }
    }
}