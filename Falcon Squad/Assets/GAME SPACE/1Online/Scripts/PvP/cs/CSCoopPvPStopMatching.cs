﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCoopPvPStopMatching : CSMessage
    {
        public override string GetEvent()
        {
            return Events.COOP_PVP_STOP_MATCHING;
        }
    }
}