﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSPvPSessionRewardedClaim : CSMessage
    {
        public override string GetEvent()
        {
            return Events.PVP_SESSION_REWARDED_CLAIM;
        }
    }
}