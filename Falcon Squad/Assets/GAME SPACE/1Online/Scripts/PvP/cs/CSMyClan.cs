﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMyClan : CSMessage
    {
        public override string GetEvent()
        {
            return Events.MY_CLAN;
        }
    }
}