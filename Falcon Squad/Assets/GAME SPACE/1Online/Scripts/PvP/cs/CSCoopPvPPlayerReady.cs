﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCoopPvPPlayerReady : CSMessage
    {
        public override string GetEvent()
        {
            return Events.COOP_PVP_READY;
        }
    }
}