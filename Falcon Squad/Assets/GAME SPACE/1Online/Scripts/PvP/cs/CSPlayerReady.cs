using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSPlayerReady : CSMessage
    {
		public string levelZigzagFixedData;
		public int levelIndex;
        public override string GetEvent()
        {
            return Events.PLAYER_READY;
        }
    }
}