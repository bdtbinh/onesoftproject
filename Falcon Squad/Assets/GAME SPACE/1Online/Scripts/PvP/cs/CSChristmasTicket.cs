﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSSelectLvEventTicket : CSMessage
    {
        public override string GetEvent()
        {
            return Events.SELECT_LV_EVENT_TICKET;
        }
    }
}