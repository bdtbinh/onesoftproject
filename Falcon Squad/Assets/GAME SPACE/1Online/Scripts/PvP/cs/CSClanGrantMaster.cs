﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanGrantMaster : CSMessage
    {
        public string member;
        public CSClanGrantMaster(string member)
        {
            this.member = member;
        }

        public override string GetEvent()
        {
            return Events.CLAN_GRANT_MASTER;
        }
    }
}