﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSFriendshipStatus : CSMessage
    {
        public string code;
        public CSFriendshipStatus(string code)
        {
            this.code = code;
        }

        public override string GetEvent()
        {
            return Events.FRIENDSHIP_STATUS;
        }
    }
}