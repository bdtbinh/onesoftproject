﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSChangeLanguage : CSMessage
    {
        public string language;

        public CSChangeLanguage(string language)
        {
            this.language = language.ToUpper();
        }

        public override string GetEvent()
        {
            return Events.CHANGE_LANGUAGE;
        }
    }
}