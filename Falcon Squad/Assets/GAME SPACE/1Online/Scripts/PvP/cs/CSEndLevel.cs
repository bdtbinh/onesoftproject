using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSEndLevel : CSMessage
    {
        public int level;
        public int status;
        public int time;
        public int difficulty; //0: normal, 1: hard, 2: hell
        public string stars;

        public CSEndLevel(int level, int status, int time, int difficulty, string stars)
        {
            this.level = level;
            this.status = status;
            this.time = time;
            this.difficulty = difficulty;
            this.stars = stars;
        }

        public override string GetEvent()
        {
            return Events.END_LEVEL;
        }
    }
}