﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSTranslator : CSMessage
    {
        public string langFrom;
        public string langTo;
        public string text;
        public CSTranslator(string langFrom, string langTo, string text)
        {
            this.langFrom = langFrom;
            this.langTo = langTo;
            this.text = text;
        }

        public override string GetEvent()
        {
            return Events.TRANSLATE;
        }
    }
}