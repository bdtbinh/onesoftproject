using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSLeaveRoom : CSMessage
    {
        public override string GetEvent()
        {
            return Events.LEAVE_ROOM;
        }
    }
}