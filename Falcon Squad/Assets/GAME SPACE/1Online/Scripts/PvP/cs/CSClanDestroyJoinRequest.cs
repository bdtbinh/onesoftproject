﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanDestroyJoinRequest : CSMessage
    {
        public override string GetEvent()
        {
            return Events.CLAN_DESTROY_JOIN_REQUEST;
        }
    }
}