﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSAchievementCompleted : CSMessage
    {
        public string name;
        public CSAchievementCompleted(string name)
        {
            this.name = name;
        }

        public override string GetEvent()
        {
            return Events.ACHIEVEMENT_COMPLETED;
        }
    }
}