﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSSelectLvEvent : CSMessage
    {
        public override string GetEvent()
        {
            return Events.SELECT_LV_EVENT;
        }
    }
}