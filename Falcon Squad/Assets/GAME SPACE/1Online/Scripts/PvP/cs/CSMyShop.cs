﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMyShop : CSMessage
    {
        public override string GetEvent()
        {
            return Events.MY_SHOP;
        }
    }
}