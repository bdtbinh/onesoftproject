﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCoopPvPSeasonRewardedClaim : CSMessage
    {
        public override string GetEvent()
        {
            return Events.COOP_PVP_SEASON_REWARD_CLAIM;
        }
    }
}