﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanJoinRequest : CSMessage
    {
        public long id;
        public CSClanJoinRequest(long id)
        {
            this.id = id;
        }

        public override string GetEvent()
        {
            return Events.CLAN_JOIN_REQUEST;
        }
    }
}