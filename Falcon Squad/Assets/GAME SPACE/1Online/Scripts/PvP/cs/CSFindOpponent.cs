using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSFindOpponent : CSMessage
    {
        public int goldBet;
        public int spaceship;

        public int waveCacheIndex;
        public string waveCacheData;

        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;
        public const int TYPE_2V2 = 2;
        public int type;

        public CSFindOpponent(int goldBet, int spaceship, int type = TYPE_PVP)
        {
            new CSUpdatePlayer(CachePvp.MyInfo, CachePvp.deviceId, "", "").Send();
            this.goldBet = goldBet;
            this.spaceship = spaceship;
            this.type = type;
            waveCacheIndex = PvPDataHelper.Instance.GetMyWaveCacheIndex(type);
            waveCacheData = PvPDataHelper.Instance.GetMyDataString(type);
        }

        public override string GetEvent()
        {
            return Events.FIND_OPPONENT;
        }
    }
}