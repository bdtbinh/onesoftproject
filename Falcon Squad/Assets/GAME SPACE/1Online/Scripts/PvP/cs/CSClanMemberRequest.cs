﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanMemberRequest : CSMessage
    {
        public long clanId;
        public CSClanMemberRequest(long clanId)
        {
            this.clanId = clanId;
        }

        public override string GetEvent()
        {
            return Events.CLAN_MEMBER_REQUEST;
        }
    }
}