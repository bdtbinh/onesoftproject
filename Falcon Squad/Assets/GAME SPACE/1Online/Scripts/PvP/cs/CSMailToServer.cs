﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMailToServer : CSMessage
    {
        public string content;
        public CSMailToServer(string content)
        {
            this.content = content;
        }

        public override string GetEvent()
        {
            return Events.MAIL_TO_SERVER;
        }
    }
}