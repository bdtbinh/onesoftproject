﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSPvPInvite : CSMessage
    {
        // id của  B
        public string code;

        // dữ liệu A
        public int spaceship;
        public int waveCacheIndex = 1;
        public string waveCacheData = "";

        public CSPvPInvite(string code, int spaceship, int waveCacheIndex, string waveCacheData)
        {
            this.code = code;
            this.spaceship = spaceship;
            this.waveCacheIndex = waveCacheIndex;
            this.waveCacheData = waveCacheData;
        }

        public override string GetEvent()
        {
            return Events.PVP_INVITE;
        }
    }
}