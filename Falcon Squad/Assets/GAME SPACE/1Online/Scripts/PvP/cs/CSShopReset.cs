﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSShopReset : CSMessage
    {
        public override string GetEvent()
        {
            return Events.SHOP_RESET;
        }
    }
}