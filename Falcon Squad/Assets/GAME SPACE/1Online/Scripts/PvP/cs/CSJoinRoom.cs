using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSJoinRoom : CSMessage
    {
        public string name;
        public CSJoinRoom(string name)
        {
            this.name = name;
        }

        public override string GetEvent()
        {
            return Events.JOIN_ROOM;
        }
    }
}