﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mp.Pvp;
using OSNet;
using System;

[Serializable]
public class CSMail : CSMessage
{
    public override string GetEvent()
    {
        return Events.FALCON_MAIL;
    }
}
