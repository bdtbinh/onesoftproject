﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSSceneLoaded : CSMessage
    {
        public int sceneIndex;
        public const int SCENE_HOME = 0;
        public const int SCENE_LEVEL_SCROLL = 1;
        public const int SCENE_GAMEPLAY = 2;
        public const int SCENE_PVP_MAIN = 3;
        public const int SCENE_PVP_END = 4;
        public const int SCENE_COOP_PVP_MAIN_2v2 = 5;
        public const int SCENE_PVP_TOURNAMENT_MAIN = 6;
        public CSSceneLoaded(int sceneIndex)
        {
            this.sceneIndex = sceneIndex;
        }

        public override string GetEvent()
        {
            return Events.SCENE_LOADED;
        }
    }

}