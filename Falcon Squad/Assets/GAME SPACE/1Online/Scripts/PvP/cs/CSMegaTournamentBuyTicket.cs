﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMegaTournamentBuyTicket : CSMessage
    {
        public int ticket;
        public CSMegaTournamentBuyTicket(int ticket)
        {
            this.ticket = ticket;
        }

        public override string GetEvent()
        {
            return Events.MEGA_TOURNAMENT_BUY_TICKET;
        }
    }
}