﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSRequestFriend : CSMessage
    {
        public const int REQUEST = 0;
        public const int ACCEPT = 1;
        public const int REJECT = 2;

        public int type;
        public string code;
        public CSRequestFriend(int type, string code)
        {
            this.type = type;
            this.code = code;
        }

        public override string GetEvent()
        {
            return Events.REQUEST_FRIEND;
        }
    }
}