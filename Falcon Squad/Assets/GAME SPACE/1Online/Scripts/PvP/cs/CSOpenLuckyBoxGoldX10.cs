﻿using OSNet;
using System;

namespace Mp.Pvp
{
    [Serializable]
    public class CSOpenLuckyBoxGoldX10 : CSMessage
    {
        public override string GetEvent()
        {
            return Events.OPEN_LUCKY_BOX_GOLD_X10;
        }
    }
}

