﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSSelectLvEventInfo : CSMessage
    {
        public override string GetEvent()
        {
            return Events.SELECT_LV_EVENT_INFO;
        }
    }
}