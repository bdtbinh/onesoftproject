﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSPlayerInfo : CSMessage
    {
        public string code;
        public CSPlayerInfo(string code)
        {
            this.code = code;
        }

        public override string GetEvent()
        {
            return Events.PLAYER_INFO;
        }
    }
}