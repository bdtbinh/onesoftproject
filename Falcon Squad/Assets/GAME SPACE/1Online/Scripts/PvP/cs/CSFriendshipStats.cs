﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSFriendshipStats : CSMessage
    {
        public string code;
        public CSFriendshipStats(string code)
        {
            this.code = code;
        }

        public override string GetEvent()
        {
            return Events.FRIENDSHIP_STATS;
        }
    }
}