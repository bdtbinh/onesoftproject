using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSUpdateDeviceId : CSMessage
    {
        public string deviceId;

        public CSUpdateDeviceId(string deviceId)
        {
            this.deviceId = deviceId;
        }

        public override string GetEvent()
        {
            return Events.UPDATE_DEVICE_ID;
        }
    }
}