﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSAppCenterFriends : CSMessage
    {
        public string ids;

        public CSAppCenterFriends(string ids)
        {
            this.ids = ids;
        }

        public override string GetEvent()
        {
            return Events.GAMECENTER_FRIENDS;
        }
    }
}