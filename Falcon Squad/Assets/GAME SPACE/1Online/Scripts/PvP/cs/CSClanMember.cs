﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanMember : CSMessage
    {
        public long clanId;
        public CSClanMember(long clanId)
        {
            this.clanId = clanId;
        }

        public override string GetEvent()
        {
            return Events.CLAN_MEMBER;
        }
    }
}