﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCoopPvPTournamentInfo : CSMessage
    {
        public override string GetEvent()
        {
            return Events.COOP_PVP_TOURNAMENT_INFO;
        }
    }
}