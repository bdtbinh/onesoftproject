using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSRankMe : CSMessage
    {
        public int type;
        public override string GetEvent()
        {
            return Events.MY_RANK;
        }
    }
}