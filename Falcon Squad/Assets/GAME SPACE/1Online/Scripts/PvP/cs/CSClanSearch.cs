﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanSearch : CSMessage
    {
        public string name;
        public CSClanSearch(string name)
        {
            this.name = name;
        }

        public override string GetEvent()
        {
            return Events.CLAN_SEARCH;
        }
    }
}