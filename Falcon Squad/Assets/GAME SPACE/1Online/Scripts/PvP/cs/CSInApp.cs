using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSInApp : CSMessage
    {
        public string inapp_item;
        public string price;
        public string currencyCode;
        public string where;
        public int vipPoint;

        public string productID;
        public string transactionID;
        public string packageName;
        public string purchaseToken;
        public string purchaseDate;


        public CSInApp(string inapp_item, string price, string currencyCode, string where, int vipPoint, string productID, string transactionID, string packageName, string purchaseToken, string purchaseDate)
        {
            this.inapp_item = inapp_item;
            this.price = price;
            this.currencyCode = currencyCode;
            this.where = where;
            this.vipPoint = vipPoint;

            this.productID = productID;
            this.transactionID = transactionID;
            this.packageName = packageName;
            this.purchaseToken = purchaseToken;
            this.purchaseDate = purchaseDate;
        }

        public override string GetEvent()
        {
            //tag : public const string BUY_IAP = "buy_iap";
            return Events.BUY_IAP;
        }
    }
}