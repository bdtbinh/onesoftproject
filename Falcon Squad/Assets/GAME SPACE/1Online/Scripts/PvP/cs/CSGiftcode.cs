﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSGiftcode : CSMessage
    {
        public string giftcode;
        public string action;//check or claim

        public CSGiftcode(string giftcode)
        {
            this.giftcode = giftcode;
            action = "check";
        }

        public override string GetEvent()
        {
            return Events.GIFT_CODE;
        }
    }
}