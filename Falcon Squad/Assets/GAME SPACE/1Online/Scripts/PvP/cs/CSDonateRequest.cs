﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSDonateRequest : CSMessage
    {
        public int planeId;
        public CSDonateRequest(int planeId)
        {
            this.planeId = planeId;
        }

        public override string GetEvent()
        {
            return Events.CLAN_DONATE_REQUEST;
        }
    }
}