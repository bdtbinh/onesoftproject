using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSValidateDeviceId : CSMessage
    {
        public string deviceId;

        public CSValidateDeviceId(string deviceId)
        {
            this.deviceId = deviceId;
        }

        public override string GetEvent()
        {
            return Events.VALIDATE_DEVICE_ID;
        }
    }
}