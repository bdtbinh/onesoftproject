﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSLiveScoreLeave : CSMessage
    {
        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;
        public const int TYPE_PVP_2VS2 = 2;

        public int type;

        public CSLiveScoreLeave(int type)
        {
            this.type = type;
        }

        public override string GetEvent()
        {
            return Events.LIVE_SCORE_LEAVE;
        }
    }
}