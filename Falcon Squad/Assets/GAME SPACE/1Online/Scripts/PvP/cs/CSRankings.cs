using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSRankings : CSMessage
    {
        public int type;
        public int from;
        public int to;
        public override string GetEvent()
        {
            return Events.RANKING;
        }
    }
}