﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCoopPvPEnemyDie : CSMessage
    {
        public int id;
        public int score;
        public int hp;

        public CSCoopPvPEnemyDie(int id, int score, int hp)
        {
            this.id = id;
            this.score = score;
            this.hp = hp;
        }

        public override string GetEvent()
        {
            return Events.COOP_PVP_ENEMY_DIE;
        }
    }
}