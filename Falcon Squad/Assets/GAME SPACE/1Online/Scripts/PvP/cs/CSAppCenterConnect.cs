﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSAppCenterConnect : CSMessage
    {
        //public int SYNC_CLIENT_TO_SERVER = 1;
        //public int SYNC_SERVER_TO_CLIENT = 2;

        public int dataSyncType;

        public string appCenterToken;
        public string appCenterId;
        public string name;
        public CSAppCenterConnect(string appCenterToken, string appCenterId, string name, int dataSyncType)
        {
            this.appCenterToken = appCenterToken;
            this.appCenterId = appCenterId;
            this.name = name;
            this.dataSyncType = dataSyncType;
        }

        public override string GetEvent()
        {
            return Events.GAMECENTER_CONNECT;
        }
    }
}