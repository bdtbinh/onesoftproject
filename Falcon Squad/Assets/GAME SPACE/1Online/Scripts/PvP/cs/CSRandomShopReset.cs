﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSRandomShopReset : CSMessage
    {
        public override string GetEvent()
        {
            return Events.RANDOM_SHOP_RESET;
        }
    }
}
