using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSPlayerWin : CSMessage
    {
        public override string GetEvent()
        {
            return Events.PLAYER_WIN;
        }
    }
}