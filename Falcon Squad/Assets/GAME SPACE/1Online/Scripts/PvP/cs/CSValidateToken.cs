using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSValidateToken : CSMessage
    {
        public string token;
        public string code;
        public string deviceId;
        public string facebookId;

        public CSValidateToken(string token, string code, string deviceId, string facebookId)
        {
            this.token = token;
            this.code = code;
            this.deviceId = deviceId;
            this.facebookId = facebookId;
        }

        public override string GetEvent()
        {
            return Events.VALIDATE_TOKEN;
        }
    }
}