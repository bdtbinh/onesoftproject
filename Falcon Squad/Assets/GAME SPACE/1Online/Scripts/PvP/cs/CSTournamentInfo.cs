﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSTournamentInfo : CSMessage
    {
        public override string GetEvent()
        {
            return Events.CLAN_TOURNAMENT_INFO;
        }
    }
}