﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMegaTournamentInfo : CSMessage
    {
        public override string GetEvent()
        {
            return Events.MEGA_TOURNAMENT_INFO;
        }
    }
}