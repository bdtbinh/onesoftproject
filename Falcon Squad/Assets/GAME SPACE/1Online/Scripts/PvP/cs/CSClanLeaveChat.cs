﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanLeaveChat : CSMessage
    {
        public const int TYPE_JOIN_ROOM_CLAN = 0;
        public const int TYPE_JOIN_ROOM_WORLD = 1;
        public const int TYPE_JOIN_ROOM_FRIEND = 2;
        public int type;
        public CSClanLeaveChat(int type)
        {
            this.type = type;
        }

        public override string GetEvent()
        {
            return Events.CLAN_LEAVE_CHAT;
        }
    }
}