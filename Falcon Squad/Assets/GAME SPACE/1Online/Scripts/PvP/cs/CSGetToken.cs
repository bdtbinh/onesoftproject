using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSGetToken : CSMessage
    {
        public string deviceId;
        public string facebookId;
        public CSGetToken(string deviceId, string facebookId)
        {
            this.deviceId = deviceId;
            this.facebookId = facebookId;
        }

        public override string GetEvent()
        {
            return Events.GET_TOKEN;
        }
    }
}