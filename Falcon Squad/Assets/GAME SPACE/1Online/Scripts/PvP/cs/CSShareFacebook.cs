﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSShareFacebook : CSMessage
    {
        public override string GetEvent()
        {
            return Events.SHARE_FACEBOOK;
        }
    }
}