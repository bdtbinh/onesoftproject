﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSExpendsion : CSMessage
    {
        public override string GetEvent()
        {
            return Events.EXPANDSION;
        }
    }
}