using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCanPlayPVP : CSMessage
    {
		public int version = PvpUtil.PVP_VERSION;
        public override string GetEvent()
        {
            return Events.CAN_PLAY_PVP;
        }
    }
}