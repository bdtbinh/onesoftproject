﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanLeave : CSMessage
    {
        public override string GetEvent()
        {
            return Events.CLAN_LEAVE;
        }
    }
}