using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSConfirmOpponentFindingNotify : CSMessage
    {
        public const int ACCEPT = 0;
        public const int DENIED = 1;
        public int confirm;
        public int gold;
        public string token;
        public int spaceship;

        public int waveCacheIndex;
        public string waveCacheData;

        public const int TYPE_PVP = 0;
        public const int TYPE_MEGA = 1;
        public int type;

        public CSConfirmOpponentFindingNotify(int confirm, int gold, string token, int spaceship, int type = TYPE_PVP)
        {
            if (confirm == ACCEPT)
            {
                new CSUpdatePlayer(CachePvp.MyInfo, CachePvp.deviceId, "", "").Send();
            }
            UnityEngine.Debug.Log("CSConfirmOpponentFindingNotify : " + confirm);
            this.confirm = confirm;
            this.gold = gold;
            this.token = token;
            this.spaceship = spaceship;

            this.type = type;

            waveCacheIndex = PvPDataHelper.Instance.GetMyWaveCacheIndex();
            waveCacheData = PvPDataHelper.Instance.GetMyDataString();
        }

        public override string GetEvent()
        {
            return Events.CONFIRM_OPPONENT_FINDING_NOTIFY;
        }
    }
}