using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSLogClickOk : CSMessage
    {
        public string url;
        public int type;
        public CSLogClickOk(string url, int type)
        {
            this.url = url;
            this.type = type;
        }

        public override string GetEvent()
        {
            return Events.LOG_CLICK_OK;
        }
    }
}