﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCoopPvPNotifyConfirm : CSMessage
    {
        public string roomId;

        public const int ACCEPT = 0;
        public const int DENIED = 1;
        public int confirm;

        public int gold;
        public int spaceship;

        public int waveCacheIndex = -1;
        public string waveCacheData = "";
        public CSCoopPvPNotifyConfirm(string roomId, int confirm, int gold, int spaceship, int waveCacheIndex, string waveCacheData)
        {
            if (confirm == ACCEPT)
            {
                ConnectManager.Instance.HandShake();
                new CSUpdatePlayer(CachePvp.MyInfo, CachePvp.deviceId, "", "").Send();
            }
            this.roomId = roomId;
            this.confirm = confirm;
            this.gold = gold;
            this.spaceship = spaceship;
            this.waveCacheIndex = waveCacheIndex;
            this.waveCacheData = waveCacheData;
        }

        public override string GetEvent()
        {
            return Events.COOP_PVP_NOTIFY_CONFIRM;
        }
    }
}