using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSLeaderboard : CSMessage
    {
        public int type;
        public CSLeaderboard(int type)
        {
            this.type = type;
        }

        public override string GetEvent()
        {
            return Events.LEADERBOARD;
        }
    }
}