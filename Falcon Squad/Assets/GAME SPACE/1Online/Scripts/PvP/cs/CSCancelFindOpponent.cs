using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCancelFindOpponent : CSMessage
    {
        public int type;

        public CSCancelFindOpponent(int type = CSFindOpponent.TYPE_PVP)
        {
            this.type = type;
        }
        public override string GetEvent()
        {
            return Events.CANCEL_FIND_OPPONENT;
        }
    }
}