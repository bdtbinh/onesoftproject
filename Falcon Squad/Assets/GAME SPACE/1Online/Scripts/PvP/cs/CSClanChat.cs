﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanChat : CSMessage
    {
        public const int CHAT_CLAN = 0;
        public const int CHAT_WORLD = 1;
        public const int CHAT_FRIEND = 2;
        public const int CHAT_LOCAL = 3;
        public int type;
        public string content;
        public CSClanChat(int type, string content)
        {
            this.type = type;
            this.content = content;
        }

        public override string GetEvent()
        {
            return Events.CLAN_CHAT;
        }
    }
}