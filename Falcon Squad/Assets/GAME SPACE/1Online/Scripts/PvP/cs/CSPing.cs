using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSPing : CSMessage
    {
        public string message;
        public override string GetEvent()
        {
            return Events.PING;
        }
    }
}