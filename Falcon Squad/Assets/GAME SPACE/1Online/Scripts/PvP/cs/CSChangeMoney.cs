using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSChangeMoney : CSMessage
    {
        public int dGold;
        public int dGem;
        public string where;
        public CSChangeMoney(int dGold, int dGem, string where)
        {
            this.dGold = dGold;
            this.dGem = dGem;
            this.where = where;
        }

        public override string GetEvent()
        {
            return Events.CS_CHANGE_MONEY;
        }
    }
}