using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSUpdateResources : CSMessage
    {
        public int gold;
        public string data;
        public CSUpdateResources(int gold, string data)
        {
            this.gold = gold;
            this.data = data;
        }

        public override string GetEvent()
        {
            return Events.UPDATE_RESOURCES;
        }
    }
}