﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCoopPvPRematch : CSMessage
    {
        public const int TYPE_NOT_REMATCH = 0;
        public const int TYPE_REMATCH = 1;

        // kiểu hành vi
        public int type;
        public CSCoopPvPRematch(int type)
        {
            this.type = type;
        }

        public override string GetEvent()
        {
            return Events.COOP_PVP_REMATCH;
        }
    }
}