using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSChangeProfile : CSMessage
    {
        public string avatar;
        public string name;
        public CSChangeProfile(string avatar, string name)
        {
            this.avatar = avatar;
            this.name = name;
        }

        public override string GetEvent()
        {
            return Events.CHANGE_PROFILE;
        }
    }
}