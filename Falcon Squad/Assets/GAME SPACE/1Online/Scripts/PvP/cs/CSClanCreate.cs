﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanCreate : CSMessage
    {
        public int avatar;
        public string name;
        public int type; //0: open, 1: close
        public int requiredLevel;
        public CSClanCreate(string name, int avatar, int type, int requiredLevel)
        {
            this.avatar = avatar;
            this.name = name;
            this.type = type;
            this.requiredLevel = requiredLevel;
        }

        public override string GetEvent()
        {
            return Events.CLAN_CREATE;
        }
    }
}