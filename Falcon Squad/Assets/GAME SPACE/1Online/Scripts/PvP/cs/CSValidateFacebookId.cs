using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSValidateFacebookId : CSMessage
    {
        public string facebookId;

        public CSValidateFacebookId(string facebookId)
        {
            this.facebookId = facebookId;
        }

        public override string GetEvent()
        {
            return Events.VALIDATE_FACEBOOK_ID;
        }
    }
}