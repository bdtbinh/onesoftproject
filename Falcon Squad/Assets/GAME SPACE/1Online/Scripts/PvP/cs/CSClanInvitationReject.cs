﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanInvitationReject : CSMessage
    {
        public long clanId;
        public CSClanInvitationReject(long clanId)
        {
            this.clanId = clanId;
        }

        public override string GetEvent()
        {
            return Events.CLAN_INVITATION_REJECT;
        }
    }
}