﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCoopPvPMatching : CSMessage
    {
        public int goldBet;
        public int spaceship;

        public int waveCacheIndex = -1;
        public string waveCacheData = "";

        public CSCoopPvPMatching(int goldBet, int spaceship)
        {
            ConnectManager.Instance.HandShake();
            new CSUpdatePlayer(CachePvp.MyInfo, CachePvp.deviceId, "", "").Send();
            this.goldBet = goldBet;
            this.spaceship = spaceship;
            waveCacheIndex = PvPDataHelper.Instance.GetMyWaveCacheIndex(CSFindOpponent.TYPE_2V2);
            waveCacheData = PvPDataHelper.Instance.GetMyDataString(CSFindOpponent.TYPE_2V2);
        }

        public override string GetEvent()
        {
            return Events.COOP_PVP_MATCHING;
        }
    }
}