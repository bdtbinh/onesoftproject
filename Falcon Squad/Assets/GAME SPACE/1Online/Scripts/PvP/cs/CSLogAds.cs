using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSLogAds : CSMessage
    {
		public string ads_id;
        public string ads_type;
		public int ads_status;
        public string where;

		public const string TYPE_INTERTITIAL = "intertitial";
		public const string TYPE_VIDEO_ADS = "video_ads";
		public const int ADS_STATUS_SHOWED = 0;
		public const int ADS_STATUS_CLICKED = 3;
		public const int ADS_STATUS_VIDEO_REWARDED = 2;
		public const int ADS_STATUS_VIDEO_PLAYING = 1;
      
        public CSLogAds(string ads_id, string ads_type, string where, int ads_status)
        {
			this.ads_id = ads_id;
            this.ads_type = ads_type;
            this.where = where;
			this.ads_status = ads_status;
        }

        public override string GetEvent()
        {
            return Events.LOG_ADS;
        }
    }
}