﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMegaTournamentUnlockBonus : CSMessage
    {
        public override string GetEvent()
        {
            return Events.MEGA_TOURNAMENT_UNLOCK_BONUS;
        }
    }
}