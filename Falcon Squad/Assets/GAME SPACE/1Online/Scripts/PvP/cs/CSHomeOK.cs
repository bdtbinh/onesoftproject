using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSHomeOK : CSMessage
    {
        public int pvpVersion;

        public CSHomeOK(int pvpVersion)
        {
            this.pvpVersion = pvpVersion;
        }

        public override string GetEvent()
        {
            return Events.HOME_OK;
        }
    }
}