using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSLoadingTime : CSMessage
    {
        public long timeSinceStartUp; //milisecond
        public string deviceId;
		public string deviceName;
		public string platform;
		public string code;
        public string packageName;

        public CSLoadingTime(long timeSinceStartUp, string deviceId, string deviceName, string platform, string code, string packageName)
        {
            this.timeSinceStartUp = timeSinceStartUp;
            this.deviceId = deviceId;
            this.deviceName = deviceName;
            this.platform = platform;
			this.code = code;
            this.packageName = packageName;
        }

        public override string GetEvent()
        {
            return Events.LOADING_TIME;
        }
    }
}