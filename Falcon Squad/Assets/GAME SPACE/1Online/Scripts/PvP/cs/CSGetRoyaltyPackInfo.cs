﻿using OSNet;
using System;

namespace Mp.Pvp
{
    [Serializable]
    public class CSGetRoyaltyPackInfo : CSMessage
    {
        public const int HOME_SCREEN = 0;
        public const int POP_ROYALTY_PACK = 1;
        public const int POP_LUCKY_BOX = 2;

        public int type;
        public CSGetRoyaltyPackInfo(int type)
        {
            this.type = type;
        }

        public override string GetEvent()
        {
            return Events.GET_ROYALTY_PACK_INFO;
        }
    }
}
