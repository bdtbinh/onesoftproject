﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanEdit : CSMessage
    {
        public long id;
        public string name;
        public int avatar;
        public int type;//0: open, 1: close
        public int requiredLevel;
        public CSClanEdit(long id, string name, int avatar, int type, int requiredLevel)
        {
            this.id = id;
            this.name = name;
            this.avatar = avatar;
            this.type = type;
            this.requiredLevel = requiredLevel;
        }

        public override string GetEvent()
        {
            return Events.CLAN_EDIT;
        }
    }
}