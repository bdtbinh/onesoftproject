using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSReplay : CSMessage
    {
        public override string GetEvent()
        {
            return Events.REPLAY;
        }
    }
}