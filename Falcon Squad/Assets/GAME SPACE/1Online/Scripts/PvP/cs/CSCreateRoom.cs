using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCreateRoom : CSMessage
    {
        public string name;
        public CSCreateRoom(string name)
        {
            this.name = name;
        }

        public override string GetEvent()
        {
            return Events.CREATE_ROOM;
        }
    }
}