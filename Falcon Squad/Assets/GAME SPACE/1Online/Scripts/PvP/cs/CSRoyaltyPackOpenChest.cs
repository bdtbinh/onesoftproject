﻿using OSNet;
using System;

namespace Mp.Pvp
{
    [Serializable]
    public class CSRoyaltyPackOpenChest : CSMessage
    {
        public override string GetEvent()
        {
            return Events.ROYALTY_PACK_OPEN_CHEST;
        }
    }
}

