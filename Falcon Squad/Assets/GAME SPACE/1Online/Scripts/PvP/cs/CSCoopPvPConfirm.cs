﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCoopPvPConfirm : CSMessage
    {
        public const int ACCEPT = 0;
        public const int DENIED = 1;

        public string code; // id của B

        public int confirm;
        public int goldBet;

        public int spaceship;
        public int waveCacheIndex = -1;
        public string waveCacheData = "";

        public CSCoopPvPConfirm(int confirm, string code, int gold)
        {
            if (confirm == ACCEPT)
            {
                ConnectManager.Instance.HandShake();
                new CSUpdatePlayer(CachePvp.MyInfo, CachePvp.deviceId, "", "").Send();
            }
            this.code = code;
            this.confirm = confirm;
            goldBet = gold;
            spaceship = CacheGame.GetSpaceShipUserSelected();
            waveCacheIndex = PvPDataHelper.Instance.GetMyWaveCacheIndex(CSFindOpponent.TYPE_2V2);
            waveCacheData = PvPDataHelper.Instance.GetMyDataString(CSFindOpponent.TYPE_2V2);
        }

        public override string GetEvent()
        {
            return Events.COOP_PVP_CONFIRM;
        }
    }
}