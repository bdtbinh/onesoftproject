﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanKickMember : CSMessage
    {
        public string member;
        public CSClanKickMember(string member)
        {
            this.member = member;
        }

        public override string GetEvent()
        {
            return Events.CLAN_KICK_MEMBER;
        }
    }
}