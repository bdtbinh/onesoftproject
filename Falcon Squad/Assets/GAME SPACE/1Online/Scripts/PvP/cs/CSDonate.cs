﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSDonate : CSMessage
    {
        public string chatId;
        public CSDonate(string chatId)
        {
            this.chatId = chatId;
        }

        public override string GetEvent()
        {
            return Events.CLAN_DONATE;
        }
    }
}