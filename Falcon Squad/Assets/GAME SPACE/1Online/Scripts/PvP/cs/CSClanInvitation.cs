﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanInvitation : CSMessage
    {
        public string member;
        public CSClanInvitation(string member)
        {
            this.member = member;
        }

        public override string GetEvent()
        {
            return Events.CLAN_INVITATION;
        }
    }
}