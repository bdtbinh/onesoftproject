using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSLocalLevelValue : CSMessage
    {
        public long levelValue;
        public override string GetEvent()
        {
            return Events.LOCAL_LEVEL_VALUE;
        }
    }
}