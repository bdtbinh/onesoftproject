﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMyFollows : CSMessage
    {
        public override string GetEvent()
        {
            return Events.MY_FOLLOW;
        }
    }
}