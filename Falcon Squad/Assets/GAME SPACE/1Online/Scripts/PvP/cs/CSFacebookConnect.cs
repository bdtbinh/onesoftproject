using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSFacebookConnect : CSMessage
    {
        //public int SYNC_CLIENT_TO_SERVER = 1;
        //public int SYNC_SERVER_TO_CLIENT = 2;

        public int dataSyncType;

        public string facebookToken;
        public string facebookId;
        public string name;
        public CSFacebookConnect(string facebookToken, string facebookId, string name, int dataSyncType)
        {
            this.facebookToken = facebookToken;
            this.facebookId = facebookId;
            this.name = name;
            this.dataSyncType = dataSyncType;
        }

        public override string GetEvent()
        {
            return Events.FACEBOOK_CONNECT;
        }
    }
}