﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSCoopPvPInvite : CSMessage
    {
        public string code;

        public int spaceship;
        public int waveCacheIndex = -1;
        public string waveCacheData = "";
        public int goldBet;
        public CSCoopPvPInvite(string code, int spaceship, int gold)
        {
            this.code = code;
            this.spaceship = spaceship;
            goldBet = gold;
            waveCacheIndex = PvPDataHelper.Instance.GetMyWaveCacheIndex(CSFindOpponent.TYPE_2V2);
            waveCacheData = PvPDataHelper.Instance.GetMyDataString(CSFindOpponent.TYPE_2V2);
        }

        public override string GetEvent()
        {
            return Events.COOP_PVP_INVITE;
        }
    }
}