﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSPvPConfirm : CSMessage
    {
        public const int ACCEPT = 0;
        public const int DENIED = 1;

        public int confirm;

        // id của  B
        public string code;

        // dữ liệu A
        public int spaceship;
        public int waveCacheIndex = -1;
        public string waveCacheData = "";

        public CSPvPConfirm(string code, int confirm, int spaceship, int waveCacheIndex, string waveCacheData)
        {
            if (confirm == ACCEPT)
            {
                new CSUpdatePlayer(CachePvp.MyInfo, CachePvp.deviceId, "", "").Send();
            }
            this.code = code;
            this.confirm = confirm;
            this.spaceship = spaceship;
            this.waveCacheIndex = waveCacheIndex;
            this.waveCacheData = waveCacheData;
        }

        public override string GetEvent()
        {
            return Events.PVP_INVITE_CONFIRM;
        }
    }
}