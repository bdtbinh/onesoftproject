﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanGrantViceMaster : CSMessage
    {
        public string member;
        public CSClanGrantViceMaster(string member)
        {
            this.member = member;
        }

        public override string GetEvent()
        {
            return Events.CLAN_GRANT_VICE_MASTER;
        }
    }
}