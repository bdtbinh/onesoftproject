﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMyRequests : CSMessage
    {
        public override string GetEvent()
        {
            return Events.MY_REQUEST;
        }
    }
}