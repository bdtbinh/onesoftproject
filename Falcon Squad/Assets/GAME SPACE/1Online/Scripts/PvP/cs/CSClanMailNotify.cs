﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanMailNotify : CSMessage
    {
        public const int TYPE_ENCOURAGE = 1;
        public const int TYPE_WARNING = 2;
        public const int TYPE_THREAT = 3;
        public const int TYPE_TEAM_MISSION = 4;
        public const int TYPE_POST_WAR_BOOST = 5;
        public const int TYPE_WAR_NOTIFY = 6;

        public int type;
        public List<string> members;
        public CSClanMailNotify(int type, List<string> members)
        {
            this.type = type;
            this.members = members;
        }

        public override string GetEvent()
        {
            return Events.CLAN_MAIL;
        }
    }
}