using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSRating : CSMessage
    {
        public int star;
        public CSRating(int star)
        {
            this.star = star;
        }

        public override string GetEvent()
        {
            return Events.STAR_RATE;
        }
    }
}