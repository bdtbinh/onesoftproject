﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanInfo : CSMessage
    {
        public long id;
        public CSClanInfo(long id)
        {
            this.id = id;
        }

        public override string GetEvent()
        {
            return Events.CLAN_INFO;
        }
    }
}