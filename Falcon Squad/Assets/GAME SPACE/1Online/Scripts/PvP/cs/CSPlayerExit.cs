using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSPlayerExit : CSMessage
    {
        public override string GetEvent()
        {
            return Events.PLAYER_EXIT;
        }
    }
}