﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanConfirmJoinRequest : CSMessage
    {
        public const int ACCEPT = 0;
        public const int REJECT = 1;
        public int action;
        public string code;

        public CSClanConfirmJoinRequest(int action, string code)
        {
            this.action = action;
            this.code = code;
        }

        public override string GetEvent()
        {
            return Events.CLAN_CONFIRM_JOIN_REQUEST;
        }
    }
}