﻿using System;
using System.Collections.Generic;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSValidateAppCenterId : CSMessage
    {
        public string appCenterId;

        public CSValidateAppCenterId(string appCenterId)
        {
            this.appCenterId = appCenterId;
        }

        public override string GetEvent()
        {
            return Events.VALIDATE_GAMECENTER_ID;
        }
    }
}