﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSDailySpin : CSMessage
    {
        public override string GetEvent()
        {
            return Events.DAILY_SPIN;
        }
    }
}