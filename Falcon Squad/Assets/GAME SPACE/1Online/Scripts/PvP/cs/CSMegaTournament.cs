﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSMegaTournament : CSMessage
    {
        public override string GetEvent()
        {
            return Events.MEGA_TOURNAMENT;
        }
    }
}