﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSFollow : CSMessage
    {
        public const int FOLLOW = 0;
        public const int UNFOLLOW = 1;

        public int type;
        public string code;
        public CSFollow(int type, string code)
        {
            this.type = type;
            this.code = code;
        }

        public override string GetEvent()
        {
            return Events.FOLLOW;
        }
    }
}