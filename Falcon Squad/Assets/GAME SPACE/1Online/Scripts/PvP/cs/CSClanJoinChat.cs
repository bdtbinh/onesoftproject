﻿using System;
using OSNet;

namespace Mp.Pvp
{
    [Serializable]
    public class CSClanJoinChat : CSMessage
    {
        public const int TYPE_JOIN_ROOM_CLAN = 0;
        public const int TYPE_JOIN_ROOM_WORLD = 1;
        public const int TYPE_JOIN_ROOM_FRIEND = 2;
        public const int TYPE_JOIN_ROOM_LOCAL = 3;
        public int type;
        public CSClanJoinChat(int type)
        {
            this.type = type;
        }

        public override string GetEvent()
        {
            return Events.CLAN_JOIN_CHAT;
        }
    }
}