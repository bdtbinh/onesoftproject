﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using SkyGameKit;
using UnityEngine;
using System.Linq;
using PathologicalGames;

public class WaveCacheManager : SerializedMonoBehaviour
{
    void Tm_OnInitEnemy()
    {
    }

    public bool isEndless;

    [HideIf("isEndless")]
    public GameObject background;

    [ShowIf("isEndless")]
    public LevelManager levelManager;



    public ZigZag[] zigZags;
    public GOContainer[] wavePrefabos;

    Dictionary<int, GOContainer> difficultyToGOContainer = new Dictionary<int, GOContainer>();
    Dictionary<BaseEnemy, int> EnemyToDefaultHP = new Dictionary<BaseEnemy, int>();
    int curZigZagIndex; // bắt đầu từ 0
    int curZigZagOffset; // bắt đầu từ 0
                         // Use this for initialization

    DataForLevel dataForLevel;
    public int MAX_INIT_WAVE = 10;

    int sumWave = 0;
    int curWaveIndexForCreating = 0;
    int curWaveIndexForStarting = -1;

    [System.Serializable]
    public struct EnemyPoolInfo
    {
        public BaseEnemy enemy;
        public int maxQuantity;
    }

    public List<EnemyPoolInfo> limitOfEnemy = new List<EnemyPoolInfo>();
    Dictionary<BaseEnemy, int> numberOfEnemy = new Dictionary<BaseEnemy, int>();

    void Start()
    {
        if (difficultyToGOContainer.Count <= 0)
        {
            for (int i = 0; i < wavePrefabos.Length; i++)
            {
                difficultyToGOContainer.Add(wavePrefabos[i].hardLevel, wavePrefabos[i]);
            }
        }
        InitZigZag();
        sumWave = dataForLevel.miniDatas.Count;

        var enemyPool = PoolManager.Pools[Const.enemyPoolName];

        for (int i = 0; i < MAX_INIT_WAVE; i++)
            CreateWave();

        foreach (var item in limitOfEnemy)
        {
            if (numberOfEnemy.ContainsKey(item.enemy))
            {
                numberOfEnemy[item.enemy] = Mathf.Min(numberOfEnemy[item.enemy], item.maxQuantity);
            }
        }

        foreach (var item in numberOfEnemy)
        {

            PrefabPool prefabPool = new PrefabPool(item.Key.transform)
            {
                preloadAmount = item.Value
            };
            enemyPool.CreatePrefabPool(
                new PrefabPool(item.Key.transform)
                {
                    preloadAmount = item.Value
                });
        }

        levelManager.OnStartWave += OnStartWave;
    }

    public void InitZigZag()
    {
        if (isEndless)
            SetZigZag(GetRandomZigZag());
        else
            SetZigZag(PvPDataHelper.Instance.GetDataForPvP());
    }

    public void CreateWave()
    {
        MiniDataForLevel miniData = dataForLevel.miniDatas[curWaveIndexForCreating % dataForLevel.miniDatas.Count];
        int difficult = miniData.difficulty;
        //Debug.LogError("Difficulty: " + difficult);
        GOContainer goC;
        if (!difficultyToGOContainer.TryGetValue(difficult, out goC))
            goC = difficultyToGOContainer.First().Value;

        //int ind = Random.Range(0, goC.gos.Length);
        GameObject go = Instantiate(goC.gos[miniData.index], Vector3.zero, Quaternion.identity);
        Debug.Log("waveInit: " + go.name);
        WaveManager waveManager = go.GetComponent<WaveManager>();

        foreach (var item in RandomPoolUtils.CountEnemy(waveManager))
        {
            if (numberOfEnemy.ContainsKey(item.Key))
            {
                numberOfEnemy[item.Key] = Mathf.Max(numberOfEnemy[item.Key], item.Value);
            }
            else
            {
                numberOfEnemy[item.Key] = item.Value;
            }
        }

        levelManager.AddWave(waveManager, goC.waveTime);
        waveManager.transform.SetParent(levelManager.transform);
        foreach (TurnManager tm in waveManager.mTM)
        {
            tm.OnInitEnemy += OnInitEnemy;
        }

        curWaveIndexForCreating++;

        if (curWaveIndexForCreating == sumWave)
            curWaveIndexForCreating = 0;

    }

    public void OnStartWave(int waveIndex)
    {
        curWaveIndexForStarting++;
        if (curWaveIndexForStarting == sumWave + 1)
        {
            curWaveIndexForStarting = 0;
            InitZigZag();
        }

        CreateWave();

    }

    public void OnInitEnemy(BaseEnemy enemy)
    {
        int defaultHP;

        if (EnemyToDefaultHP.TryGetValue(enemy, out defaultHP))
        {
            enemy.maxHP = (int)((float)defaultHP * GetCurCoeff());
        }
        else
        {
            EnemyToDefaultHP.Add(enemy, enemy.maxHP);
            enemy.maxHP = (int)((float)enemy.maxHP * GetCurCoeff());

        }

        enemy.currentHP = enemy.maxHP;

    }


    private float GetCurCoeff()
    {
        int k = 0;
        int xsum = 0;
        while (xsum < curWaveIndexForStarting && k < zigZags.Length)
        {
            xsum += zigZags[k].zigzag.Split(',').Length;
            k++;
        }
        if (k >= zigZags.Length)
            k = zigZags.Length;

        if (k == 0) k = 1;
        return zigZags[k - 1].coeff;
    }


    public string GetRandomZigZag()
    {
        if (difficultyToGOContainer.Count <= 0)
        {
            for (int i = 0; i < wavePrefabos.Length; i++)
            {
                difficultyToGOContainer.Add(wavePrefabos[i].hardLevel, wavePrefabos[i]);
            }
        }

        DataForLevel dataForLevel = new DataForLevel();
        foreach (ZigZag zz in zigZags)
        {
            string[] zzDifficults = zz.zigzag.Split(',');
            foreach (string difficult in zzDifficults)
            {
                int difInt = int.Parse(difficult);
                GOContainer goC = difficultyToGOContainer[difInt];
                int ind = Random.Range(0, goC.gos.Length);
                dataForLevel.miniDatas.Add(new MiniDataForLevel(difInt, ind));
            }
        }

        return dataForLevel.ToString();
    }

    public void SetZigZag(string data)
    {
        dataForLevel = DataForLevel.CreateFromData(data);

    }
}

[System.Serializable]
public class GOContainer
{
    public float waveTime = -1;
    public int hardLevel;
    public GameObject[] gos;
}

[System.Serializable]
public class ZigZag
{
    public string zigzag;
    public float coeff;
}


public class DataForLevel
{
    public DataForLevel()
    {

    }
    public List<MiniDataForLevel> miniDatas = new List<MiniDataForLevel>();
    public string ToString()
    {
        return JsonFx.Json.JsonWriter.Serialize(this);
    }

    public static DataForLevel CreateFromData(string data)
    {
        Debug.Log("DATA: " + data);
        return JsonFx.Json.JsonReader.Deserialize<DataForLevel>(data);
    }
}

public class MiniDataForLevel
{
    public int difficulty;
    public int index;

    public MiniDataForLevel()
    {

    }

    public MiniDataForLevel(int difficulty, int index)
    {
        this.difficulty = difficulty;
        this.index = index;
    }
}




