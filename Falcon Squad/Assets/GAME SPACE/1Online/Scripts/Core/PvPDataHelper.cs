﻿using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PvPDataHelper
{

    public DataForLevel dataForLevel;
    public WaveCacheManager[] waveCaches;
    public WaveCacheManager[] waveCachesTournament;
    public WaveCacheManager[] waveCaches2vs2;

    private static PvPDataHelper _instance = null;

    private int waveCacheIndexForPvP;
    private string dataForPvP;

    public static PvPDataHelper Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PvPDataHelper();

            }

            return _instance;
        }
    }

    public int GetMyWaveCacheIndex(int type = CSFindOpponent.TYPE_PVP)
    {
        //int rankTitle = CachePvp.Medal / 30;
        /*
        { "BRONZE", 1 },
        { "SILVER", 2 },
        { "GOLD", 3 },
        { "PLATINUM", 4 },
        { "DIAMOND", 5 },
        { "MASTER", 6 },
        { "CHALLENGER", 7 },
        { "LEGEND", 8 }
        */
        int rankTitle = 0;
        if (type == CSFindOpponent.TYPE_PVP)
        {
            rankTitle = Mathf.Min(7, CachePvp.dictionaryRank[CachePvp.PlayerLevel.code] - 1);
        }
        else if (type == CSFindOpponent.TYPE_2V2)
        {
            rankTitle = Mathf.Min(7, CachePvp.dictionaryRank[CachePvp.PlayerLevel2vs2.code] - 1);
        }
        return rankTitle;
    }

    public string GetMyDataString(int type = CSFindOpponent.TYPE_PVP)
    {
        int index = GetMyWaveCacheIndex(type);
        WaveCacheManager waveCacheManager;
        if (type == CSFindOpponent.TYPE_PVP)
        {
            waveCacheManager = waveCaches[index];
        }
        else if (type == CSFindOpponent.TYPE_2V2)
        {
            waveCacheManager = waveCaches2vs2[index];
        }
        else
        {
            waveCacheManager = waveCachesTournament[index];
        }
        if (waveCacheManager == null)
        {
            if (type == CSFindOpponent.TYPE_PVP)
            {
                for (int i = 0; i < waveCaches.Length; i++)
                {
                    if (waveCaches[i] != null)
                    {
                        waveCacheManager = waveCaches[i];
                        break;
                    }
                }
            }
            else if (type == CSFindOpponent.TYPE_2V2)
            {
                for (int i = 0; i < waveCaches2vs2.Length; i++)
                {
                    if (waveCaches2vs2[i] != null)
                    {
                        waveCacheManager = waveCaches2vs2[i];
                        break;
                    }
                }
            }
        }
        return waveCacheManager.GetRandomZigZag();
    }


    public int GetWaveCacheIndexForPvP()
    {
        return waveCacheIndexForPvP;
    }

    public void SetWaveCacheIndexForPvP(int waveCacheIndex)
    {
        waveCacheIndexForPvP = waveCacheIndex;
    }

    public string GetDataForPvP()
    {
        return dataForPvP;
    }

    public void SetDataForPvP(string data)
    {
        Debug.Log("DATA " + data);
        dataForPvP = data;
    }
}


