﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitPvPLevel : MonoBehaviour
{

    public PvPLevelManager pvPLevelManager;
    public PVP2v2LevelManager pvP2v2LevelManager;
    public TournamentLevelManager tournamentLevelManager;

    // Use this for initialization
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        PvPDataHelper.Instance.waveCaches = pvPLevelManager.waveCaches;
        PvPDataHelper.Instance.waveCaches2vs2 = pvP2v2LevelManager.waveCaches;
        PvPDataHelper.Instance.waveCachesTournament = tournamentLevelManager.waveCaches;
    }
}
