﻿using UnityEngine;
using SkyGameKit;

public class TournamentLevelManager : MonoBehaviour
{
    public WaveCacheManager[] waveCaches;
    public LevelManager levelManager;

    private void Awake()
    {
        levelManager = Instantiate(levelManager, Vector3.zero, Quaternion.identity);
        levelManager.transform.parent = this.transform;
        foreach (WaveCacheManager waveCache in waveCaches)
        {
            waveCache.levelManager = levelManager;
        }
        WaveCacheManager wave = Instantiate(waveCaches[0], Vector3.zero, Quaternion.identity);
        GameObject bgIns = Instantiate(wave.background, Vector3.zero, Quaternion.identity);
        wave.transform.parent = this.transform;
    }
}
