﻿using UnityEngine;
using SkyGameKit;

public class PVP2v2LevelManager : MonoBehaviour
{
    public WaveCacheManager[] waveCaches;
    public LevelManager levelManager;

    private void Awake()
    {
        levelManager = Instantiate(levelManager, Vector3.zero, Quaternion.identity);
        levelManager.transform.parent = transform;
        foreach (WaveCacheManager waveCache in waveCaches)
        {
            waveCache.levelManager = levelManager;
        }
        WaveCacheManager wave = Instantiate(waveCaches[PvPDataHelper.Instance.GetWaveCacheIndexForPvP()], Vector3.zero, Quaternion.identity);
        GameObject bgIns = Instantiate(wave.background, Vector3.zero, Quaternion.identity);
        wave.transform.parent = transform;
    }
}
