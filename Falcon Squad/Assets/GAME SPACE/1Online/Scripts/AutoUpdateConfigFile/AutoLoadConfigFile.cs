﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Networking;

public class AutoLoadConfigFile : MonoBehaviour
{
	public const int ASSET_BUNDLE_VERSION = 1;
	private UnityWebRequest uwr;
	// Use this for initialization
	void Start()
	{
		StartCoroutine(StartLoadBundle());
	}

	// Update is called once per frame
	void Update()
	{

	}

	IEnumerator StartLoadBundle()
	{
		string r = "";
        r += UnityEngine.Random.Range(
                      1000000, 8000000).ToString();
        r += UnityEngine.Random.Range(
                      1000000, 8000000).ToString();
		WWW www = new WWW(CachePvp.assetBundleUrl + "?p=" + r);
		yield return www;
		if (string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.text))
		{
			string[] lines = www.text.Split('\n');
			foreach (string line in lines)
			{
				if (line.Length < 5) 
					continue;
				string[] parameters = line.Split(',');
				int minAppVersion = Int32.Parse(parameters[0]);
				int maxAppVersion = Int32.Parse(parameters[1]);
				int enabled = Int32.Parse(parameters[2]);
				int assetBundleVersion = Int32.Parse(parameters[3]);
				string assetBundleUrl = parameters[4];
				int appVersion = Int32.Parse(Application.version.Replace(".", "")) * 100;
				if (minAppVersion <= appVersion && appVersion <= maxAppVersion)
				{
					using (uwr = UnityWebRequestAssetBundle.GetAssetBundle(assetBundleUrl, (uint)assetBundleVersion, 0))
                    {
                        yield return uwr.SendWebRequest();
                  
                        if (uwr.isNetworkError || uwr.isHttpError)
                        {
                            //print error
                        }
                        else
                        {
                            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);
                            string[] allAssetNames = bundle.GetAllAssetNames();
                            Array.Sort(allAssetNames, StringComparer.Ordinal);
                            foreach (var assetName in allAssetNames)
                            {
								SerializableSet seriSet = bundle.LoadAsset<SerializableSet>(assetName);
								CachePvp.serializableSet = seriSet;
								CachePvp.assetBundleCurrentVersion = assetBundleVersion;
								break;
                            }

                            
                        }
                    }
					CachePvp.assetBundleEnable = enabled;
				}
			}
		}

	}

}
