﻿using BestHTTP.SocketIO;
using PlatformSupport.Collections.ObjectModel;
using System.Collections.Generic;
using System;
using UnityEngine;
using BestHTTP.SocketIO.JsonEncoders;
using Mp.Pvp;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace OSNet
{
    public class NetManager
    {
        private Queue<CSMessage> queueMessages = new Queue<CSMessage>();
        private List<CSMessage> listMessageImportant = new List<CSMessage>();
        private List<CSMessage> tempListMessageImportant = new List<CSMessage>();
        private static NetManager _instance = null;

        public static NetManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new NetManager();
                }
                return _instance;
            }
        }

        public static void Destroy()
        {
            if (_instance != null)
            {
                _instance.manager.Close();
                _instance = null;
            }
        }

        private SocketManager manager;
        public Action<SocketManager> OnStartListening;

        public void Start(string uri)
        {
            ObservableDictionary<string, string> dict = new ObservableDictionary<string, string>();
            dict.Add("version", Application.version);
#if UNITY_ANDROID
            dict.Add("platform", "android");
#elif UNITY_IOS
            dict.Add("platform", "ios");
#endif
            dict.Add("language_alpha2", SpriteNameConst.GetLanguageFlagCode(I2.Loc.LocalizationManager.CurrentLanguage).ToUpper());
            TimeZone tz = TimeZone.CurrentTimeZone;
            int addHours = tz.GetUtcOffset(DateTime.Now).Hours;
            dict.Add("timezone", addHours.ToString());
            dict.Add("language", Application.systemLanguage.ToString());
            dict.Add("coop_pvp_version", PvpUtil.PVP_2V2_VERSION.ToString());
            dict.Add("pvp_version", PvpUtil.PVP_VERSION.ToString());
            if (!string.IsNullOrEmpty(CachePvp.Code))
            {
                dict.Add("code", CachePvp.Code);
            }

            SocketOptions options = new SocketOptions
            {
                AutoConnect = false,
                ConnectWith = BestHTTP.SocketIO.Transports.TransportTypes.WebSocket,
                //AdditionalQueryParams = new ObservableDictionary<string, string>()
                AdditionalQueryParams = dict
            };
            manager = new SocketManager(new Uri(uri), options);
            manager.Encoder = new LitJsonEncoder();
            OnStartListening(manager);
            LoadEvents();
            manager.Open();
        }

        public void LoadEvents()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                var types = assembly.GetTypes();
                foreach (var type in types)
                {
                    if (type.IsSubclassOf(typeof(SCMessage)))
                    {
                        SCMessage message = (SCMessage)Activator.CreateInstance(type);
                        manager.Socket.On(message.GetEvent(), (socket, packet, args) =>
                        {
                            message = (SCMessage)Activator.CreateInstance(type);
                            Dictionary<string, object> dict = (Dictionary<string, object>)args[0];
                            DictionaryUtils.Decode(message, dict);
                            if (!(message is SCLiveScore))
                            {
                                Debug.Log("<color=#cc3333>" + System.DateTime.Now + "</color>");
                                Debug.Log("<color=#cc3333>" + message.GetType().ToString() + "</color>");
                                Debug.Log("<color=#cc3333>" + JsonFx.Json.JsonWriter.Serialize(message) + "</color>");
                            }
                            message.OnData();
                        });
                    }
                }
            }
        }




        public void Send(string eventConst, CSMessage message, bool isImportant = false)
        {
            Debug.Log("<color=#0092cc>" + System.DateTime.Now + "</color>");
            Debug.Log("<color=#0092cc>" + message.GetType().ToString() + "</color>");
            Debug.Log("<color=#0092cc>" + JsonFx.Json.JsonWriter.Serialize(message) + "</color>");

            //if (isImportant)
            //{
            //    listMessageImportant.Add(message);
            //    WriteToFile();
            //}

            try
            {
                if (manager != null && manager.GetSocket() != null && manager.GetSocket().IsOpen)
                    manager.Socket.Emit(eventConst, DictionaryUtils.encode(message));
                else
                {
                    if (message is CSLogAds)
                        queueMessages.Enqueue(message);
                    if (isImportant)
                    {
                        listMessageImportant.Add(message);
                        WriteToFile();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log("<color=#0092cc>" + "Send --- Exception: " + ex + "</color>");
            }

        }

        private void WriteToFile()
        {
            byte[] b = ObjectToByteArray(listMessageImportant);
#if !UNITY_EDITOR
            ByteArrayToFile(Application.dataPath + "/ImportantMessage", b);
#endif
        }

        byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        private System.Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            System.Object obj = (System.Object)binForm.Deserialize(memStream);

            return obj;
        }

        private byte[] FileToByteArray(string fileName)
        {
            return File.ReadAllBytes(fileName);
        }

        private bool ByteArrayToFile(string fileName, byte[] byteArray)
        {
            try
            {
                using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in process: {0}", ex);
                return false;
            }
        }

        public bool IsOnline()
        {

            //if (manager != null && manager.GetSocket() != null)
            //{
            //    return manager.GetSocket().IsOpen;
            //}
            try
            {
                return manager.GetSocket().IsOpen && CachePvp.isConnectServer;
            }
            catch (NullReferenceException e)
            {
                //Debug.LogError(e.Message);
                Debug.LogError("IsOnline null rồi ");
            }

            return false;
        }

        void ReadFile()
        {
            if (File.Exists(Application.dataPath + "/ImportantMessage"))
            {
                byte[] b = FileToByteArray(Application.dataPath + "/ImportantMessage");
                if (b.Length > 0)
                {
                    try
                    {
                        System.Object o = ByteArrayToObject(b);
                        listMessageImportant = (List<CSMessage>)o;
                        tempListMessageImportant = listMessageImportant;
                    }
                    catch (Exception)
                    {
                        Debug.Log("Error read file");
                        throw;
                    }

                }
            }
        }

        public void PushQueue()
        {
            //foreach (CSMessage message in queueMessages)
            //{
            //    message.Send();
            //}
            while (queueMessages.Count > 0)
            {
                CSMessage current = queueMessages.Dequeue();
                if (current != null)
                    current.Send();
            }
            ReadFile();
            listMessageImportant.Clear();
            WriteToFile();
            Debug.Log("push quere list message important : " + listMessageImportant.Count);
            while (tempListMessageImportant.Count > 0)
            {
                tempListMessageImportant[0].Send();
                tempListMessageImportant.RemoveAt(0);
            }
        }
    }
}
