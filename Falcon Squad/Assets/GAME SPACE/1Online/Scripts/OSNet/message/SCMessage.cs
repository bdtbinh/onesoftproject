﻿namespace OSNet
{
    public abstract class SCMessage : IMessage
    {
        public const int SUCCESS = 0;
        public const int ERROR = 1;
        public const string MESSAGE_SUCCESS = "Success";
        public const string MESSAGE_ERROR = "Error";

        public int status;
        public string message;
        public long timeServer;
        public string localTimeString; // dd / MM / yyyy hh : mm : ss

        public abstract string GetEvent();
        public abstract void OnData();

        override public string ToString()
        {
            return this.GetType() + ": " + message;
        }
    }
}
