﻿namespace OSNet { 
    public interface IMessage {
        string GetEvent();
    }
}
