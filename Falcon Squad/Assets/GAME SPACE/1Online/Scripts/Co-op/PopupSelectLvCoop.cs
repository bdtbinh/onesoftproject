﻿//using System.Collections;
//using System.Collections.Generic;
//using TCore;
//using UnityEngine;
//using UnityEngine.SceneManagement;

//public class PopupSelectLvCoop : MonoBehaviour
//{

//    public UISprite sStar1, sStar2, sStar3;
//    public UILabel lGetStar1, lGetStar2, lGetStar3;
//    public UILabel numItemPowerUp, numItemTimeSphere, numItemLife;
//    public GameObject sChooseItemPowerUp, sChooseItemTimeSphere, sChooseItemLife;
//    public UILabel labelNotifiClickItem;

//    void Start()
//    {
//        SetDataItem();
//    }

//    void SetDataItem()
//    {

//        //CacheGame.numItemPowerUpUse = 0;
//        //CacheGame.numUsingSkill = 0;
//        //CacheGame.numLifeUpUse = 0;

//        numItemPowerUp.text = "" + (CacheGame.GetTotalItemPowerUp() - CacheGame.numItemPowerUpUse);
//        numItemTimeSphere.text = "" + (CacheGame.GetTotalItemActiveSkill() - CacheGame.numUsingSkill);
//        numItemLife.text = "" + (CacheGame.GetTotalItemLife() - CacheGame.numLifeUpUse);

//    }



//    //-------------------Click USE Item-----------------------------------
//    void ShowNotifiClickUseItem(string text)
//    {
//        labelNotifiClickItem.text = text;
//        labelNotifiClickItem.gameObject.SetActive(true);

//        StopCoroutine("HideNotifiClickItem");
//        StartCoroutine("HideNotifiClickItem");
//    }
//    IEnumerator HideNotifiClickItem()
//    {
//        yield return new WaitForSeconds(1.6f);
//        labelNotifiClickItem.gameObject.SetActive(false);
//    }

//    public void Button_UseItemPowerUp()
//    {
//        SoundManager.PlayClickButton();

//        if (CacheGame.GetTotalItemPowerUp() > 0)
//        {
//            if (CacheGame.numItemPowerUpUse == 1)
//            {
//                CacheGame.numItemPowerUpUse = 0;
//                sChooseItemPowerUp.SetActive(false);
//                StopCoroutine("HideNotifiClickItem");
//            }
//            else
//            {
//                CacheGame.numItemPowerUpUse = 1;
//                sChooseItemPowerUp.SetActive(true);
//                ShowNotifiClickUseItem("+1 Power-Up");
//            }
//            numItemPowerUp.text = "" + (CacheGame.GetTotalItemPowerUp() - CacheGame.numItemPowerUpUse);
//        }
//        else
//        {
//            ShowNotifiClickUseItem("+1 Power-Up");
//        }

//    }

//    public void Button_UseItemTimeSphere()
//    {
//        SoundManager.PlayClickButton();

//        if (CacheGame.GetTotalItemActiveSkill() > 0)
//        {
//            if (CacheGame.numUsingSkill == 1)
//            {
//                CacheGame.numUsingSkill = 0;
//                sChooseItemTimeSphere.SetActive(false);
//                StopCoroutine("HideNotifiClickItem");
//            }
//            else
//            {
//                CacheGame.numUsingSkill = 1;
//                sChooseItemTimeSphere.SetActive(true);
//                ShowNotifiClickUseItem("+1 EMP");
//            }
//            numItemTimeSphere.text = "" + (CacheGame.GetTotalItemActiveSkill() - CacheGame.numUsingSkill);
//        }
//        else
//        {
//            ShowNotifiClickUseItem("+1 EMP");
//        }

//    }

//    public void Button_UseItemLife()
//    {
//        SoundManager.PlayClickButton();

//        if (CacheGame.GetTotalItemLife() > 0)
//        {
//            if (CacheGame.numLifeUpUse == 1)
//            {
//                CacheGame.numLifeUpUse = 0;
//                sChooseItemLife.SetActive(false);
//                StopCoroutine("HideNotifiClickItem");
//            }
//            else
//            {
//                CacheGame.numLifeUpUse = 1;
//                sChooseItemLife.SetActive(true);
//                ShowNotifiClickUseItem("+1 Life");
//            }
//            numItemLife.text = "" + (CacheGame.GetTotalItemLife() - CacheGame.numLifeUpUse);
//        }
//        else
//        {
//            ShowNotifiClickUseItem("+1 Life");
//        }

//    }



//    //-------button back----------------------------------------

//    public void Button_Back()
//    {
//        SoundManager.PlayClickButton();
//        StopCoroutine("HideNotifiClickItem");
//        labelNotifiClickItem.gameObject.SetActive(false);
//        gameObject.SetActive(false);
//    }


//    public void Button_Play()
//    {
//        SoundManager.PlayClickButton();

//        CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() - CacheGame.numItemPowerUpUse);
//        CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() - CacheGame.numUsingSkill);
//        CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() - CacheGame.numLifeUpUse);

//        SceneManager.LoadScene("LevelCoopDemo");
//    }


//}
