﻿using DigitalRuby.ThunderAndLightning;
using PathologicalGames;
using SkyGameKit;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinhLightningBolt : MonoBehaviour
{
    [SerializeField]
    private float shootingTime;     //Thời gian mỗi lần giật sét (giật sét trong bao lâu) 

    [SerializeField]
    private float waitingTime;      //Thời gian đợi tới lần giật sét kế tiếp

    [SerializeField]
    private int numberOfEnemies;    // Số lượng enemy tối đa cho phép giật sét cùng 1 lúc

    [SerializeField]
    private LightningBoltPrefabScript[] lightningBoltPrefab;

    [SerializeField]
    private LightningBoltTransformTrackerScript[] tracker;

    [SerializeField]
    private ParticleSystem fxBeforeShoot;

    [SerializeField]
    private Transform startPoint;

    List<GameObject> listEnemies;
    int damage;
    float usingSkillTime;

    private void Awake()
    {
        listEnemies = new List<GameObject>(numberOfEnemies);

        for (int i = 0; i < lightningBoltPrefab.Length; i++)
        {
            lightningBoltPrefab[i].Camera = MainScene.Instance.cameraTk2d;
        }
    }

    public void ActiveLightning(int damage, float usingSkillTime)
    {
        this.damage = damage;
        this.usingSkillTime = usingSkillTime;

        coShowHide = CoShowHide();
        StartCoroutine(coShowHide);
        //Debug.LogError("ActiveLightning powerGunActiveSkill:" + damage);
    }

    void UpdateListEnemy()
    {
        listEnemies.Clear();
        Collider2D[] colliders = Physics2D.OverlapAreaAll(MainScene.Instance.posAnchorTopLeft.position, MainScene.Instance.posAnchorBottomRight.position);

        Array.Sort(colliders, delegate (Collider2D collider1, Collider2D collider2)
        {
            return ((collider1.transform.position - startPoint.position).sqrMagnitude).CompareTo((collider2.transform.position - startPoint.position).sqrMagnitude);
        });

        for (int i = 0; i < colliders.Length; i++)
        {
            if (listEnemies.Count >= numberOfEnemies)
                break;
            if (LayerMask.LayerToName(colliders[i].gameObject.layer) == "Enemy")
            {
                listEnemies.Add(colliders[i].gameObject);
            }
        }
    }


    IEnumerator coShowHide;
    IEnumerator CoShowHide()
    {
        int activeSkillTime = Mathf.CeilToInt(usingSkillTime / (shootingTime + waitingTime));

        for (int j = 0; j < activeSkillTime; j++)
        {
            UpdateListEnemy();

            if (listEnemies.Count > 0)
            {
                PlayParticles();
            }

            for (int i = 0; i < listEnemies.Count; i++)
            {
                lightningBoltPrefab[i].Destination = listEnemies[i];
                tracker[i].EndTarget = listEnemies[i].transform;
                lightningBoltPrefab[i].gameObject.SetActive(true);
                DamageEnemy(listEnemies[i]);
                Pool_LightningBoltFX(listEnemies[i].transform);
            }

            yield return new WaitForSecondsRealtime(shootingTime);

            //Hide target
            StopParticles();
            for (int i = 0; i < listEnemies.Count; i++)
            {
                lightningBoltPrefab[i].gameObject.SetActive(false);
            }

            yield return new WaitForSecondsRealtime(waitingTime);
        }
    }

    void DamageEnemy(GameObject enemy)
    {
        if (enemy.GetComponent<EnemyCollisionManager>() != null)
        {
            if (enemy.GetComponent<EnemyCollisionManager>().enemyCollisionBase != null)
            {
                enemy.GetComponent<EnemyCollisionManager>().enemyCollisionBase.TakeDamage(damage / 3 + 1);
            }
        }
    }

    private void PlayParticles()
    {
        if (fxBeforeShoot != null)
        {
            fxBeforeShoot.gameObject.SetActive(true);
            fxBeforeShoot.Play(true);
        }
    }

    void StopParticles()
    {
        if (fxBeforeShoot != null)
        {
            fxBeforeShoot.gameObject.SetActive(false);
            fxBeforeShoot.Stop(true);
        }
    }

    private void Pool_LightningBoltFX(Transform enemyTrans)
    {
        if (EffectList.Instance.enemyTriggerBulletLightingBolt != null && enemyTrans.gameObject.activeInHierarchy)
        {
            Transform effectIns = PoolManager.Pools[Const.explosiveName].Spawn(EffectList.Instance.enemyTriggerBulletLightingBolt, enemyTrans.position, Quaternion.identity);
            effectIns.GetComponent<ParticleSystem>().Play(true);
            effectIns.transform.parent = enemyTrans;
        }
    }
}
