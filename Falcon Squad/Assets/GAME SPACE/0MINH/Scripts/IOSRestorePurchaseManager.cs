﻿using UnityEngine;

public class IOSRestorePurchaseManager : MonoBehaviour
{

    [SerializeField]
    private GameObject btnRestorePurchase;

    [SerializeField]
    private GameObject chinaBackground;

    [SerializeField]
    private GameObject normalBackground;

    void Awake()
    {

#if UNITY_ANDROID
        btnRestorePurchase.SetActive(false);
#else
        btnRestorePurchase.SetActive(true);
#endif
        chinaBackground.SetActive(GameContext.IS_CHINA_VERSION);
        normalBackground.SetActive(!GameContext.IS_CHINA_VERSION);

    }

}
