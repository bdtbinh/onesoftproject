﻿using System;
using UnityEngine;

public static class Constant {

    public const float ANDROID_CLICK_PURCHASE_WAITING_TIME = 0.5f;
    public const float IOS_CLICK_PURCHASE_WAITING_TIME = 4f;

    public const int UNLOCK_WING_LEVEL = 35; //4
    public const int UNLOCK_LEFT_WINGMAN_LEVEL = 0;
    public const int UNLOCK_RIGHT_WINGMAN_LEVEL = 5; //4
    public const int UNLOCK_WEAPON_LEVEL = 5; //5

    public const int BETTING_AMOUNT_MIN = 100;
    public const int BETTING_AMOUNT_MIDDLE = 300;
    public const int BETTING_AMOUNT_MAX = 500;

    public static Color NORMAL_COLOR_CLOUD = new Color(45 / 255f, 142 / 255f, 1f, 114 / 255f);
    public static Color HARD_COLOR_CLOUD = new Color(143 / 255f, 144 / 255f, 41 / 255f, 152 / 255f);
    public static Color HELL_COLOR_CLOUD = new Color(101 / 255f, 0f, 34 / 255f, 190 / 255f);

    public const Rank MAX_RANK_APPLY = Rank.SSS;

    public const int UNLOCK_LEVEL_LUCKY_BOX = 1;

    public static int[] STAR_UNLOCK_REQUIRE = new int[3] { 7, 14, 21 };      //Số sao cần để mở khóa rương nhận thưởng
    public static int EXTRA_STAR_UNLOCK_REQUIRE = 6;                         //Số sao cần để mở khóa rương thưởng của level extra
}
