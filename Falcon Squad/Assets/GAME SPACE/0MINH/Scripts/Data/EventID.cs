﻿public static class EventID
{

    public const string HANGAR_ITEM_CHANGED = "OnHangarItemChanged";
    public const string SELECT_DEVICE_CHANGE_UI = "OnSelectDeviceItemChange";
    public const string ON_CHANGE_DIFFICULT_MODE = "OnChangeDifficultMode";
    public const string ON_EVOLVE_SUCCESSED = "OnEvolveSuccessed";
    public const string ON_REWARD_SUCCESSED = "OnRewardSuccessed";
    public const string ON_TUTORIAL_UPGRADE_SUCCESSED = "OnTutorialUpgradeSuccessed";
    public const string ON_CHANGE_EVENT_RESOURCE_AMOUNT = "OnChangeEventResourceAmount";
    public const string ON_CHANGE_SENSITIVITY = "OnChangeSensitivity";
    public const string ON_SELECTED_CALLING_BACKUP = "OnCallingBackup";
    public const string ON_CHANGE_CONTROL = "OnChangecontrol";
    public const string ON_CHANGE_SELECT_LEVEL_WORLD = "OnChangeSelectLevelWorld";
    public const string ON_CLAIM_STAR_CHEST_REWARD = "OnClaimStarChestReward";
    public const string ON_CLAIM_STAR_CHEST_REWARD_EXTRA = "OnClaimStarChestRewardExtra";
    public const string ON_CLICK_CHANGE_LANGUAGE = "OnClickChangeLanguage";
    public const string ON_GET_FREE_LUCKY_BOX_GOLD= "OnGetFreeLuckyBoxGold";
    public const string ON_DISABLE_SHOW_RECEIVE_ITEM_FX = "OnDisableShowReceiveItemFX";
}
