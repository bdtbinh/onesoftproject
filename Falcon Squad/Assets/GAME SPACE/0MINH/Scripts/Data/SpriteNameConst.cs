﻿using UnityEngine;

public class SpriteNameConst : MonoBehaviour {

    //Select level star
    public const string NORMAL_STAR_SPRITE = "icon_bluestar_ingame";
    public const string HARD_STAR_SPRITE = "icon_goldstar_ingame";
    public const string HELL_STAR_SPRITE = "icon_redstar_ingame";

    //Button
    public const string BG_BUTTON_CLAIM_DISABLE_1 = "btn_daily_grey";
    public const string BG_BUTTON_CLAIM_ENABLE_1 = "btn_daily_green";

    //Get country flag code
    public static string GetLanguageFlagCode(string language)
    {
        switch (language)
        {
            case "Chinese (Simplified)":
                return "cn";

            case "English":
                return "gb-eng";

            case "Japanese":
                return "jp";

            case "Korean":
                return "kr";

            case "Vietnamese":
                return "vn";

            default:
                return I2.Loc.LocalizationManager.GetLanguageCode(language);
        }
    }
}
