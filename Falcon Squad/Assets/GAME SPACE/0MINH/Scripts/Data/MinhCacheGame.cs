﻿using System;
using UnityEngine;

public class MinhCacheGame : MonoBehaviour
{

    public static void SetAlreadyRate()
    {
        //1 la da rate
        PlayerPrefs.SetInt("AlreadyRateM", 1);
    }

    public static bool IsAlreadyRate()
    {
        //0 la chua rate
        return PlayerPrefs.GetInt("AlreadyRateM", 0) == 1;
    }

    //Ngay show popup rate gan nhat
    public static void SetLastDayRate()
    {
        PlayerPrefs.SetInt("LastDayShowPopupRateM", DateTime.Now.Day);

        //AntiCheat.SetIntConverted("new", 111, 9888);
    }

    public static int GetLastDayRate()
    {
        //-1 la chua hien popup rate lan nao
        return PlayerPrefs.GetInt("LastDayShowPopupRateM", -1);

        //return AntiCheat.GetIntConverted("LastDayShowPopupRateM", "new", 9888, 0);
    }

    public static void SetDayCountRate(int count)
    {
        PlayerPrefs.SetInt("DayCountRate", count);
    }

    public static int GetDayCountRate()
    {
        return PlayerPrefs.GetInt("DayCountRate", 0);
    }

    //Lưu data cho popup Lucky Wheel
    public static void SetAlreadyShowTutorialLuckyWheel()
    {
        PlayerPrefs.SetInt("LuckyWheelTutorialM", 1);
    }

    public static bool IsAlreadyShowTutorialLuckyWheel()
    {
        return PlayerPrefs.GetInt("LuckyWheelTutorialM", 0) == 1;
    }

    public static void SetCoundownVideoTime(DateTime time)
    {
        PlayerPrefs.SetString("CoundownVideoTimeM", time.ToString());
    }

    public static DateTime GetCountdownVideoTime()
    {
        DateTime time;
        DateTime.TryParse(PlayerPrefs.GetString("CoundownVideoTimeM", DateTime.Now.AddDays(-200f).ToString()), out time);
        return time;
    }

    public static void SetCoundownFreeTime(DateTime time)
    {
        PlayerPrefs.SetString("CoundownFreeTimeM_2", time.ToString());
    }

    public static DateTime GetCountdownFreeTime()
    {
        DateTime time;
        DateTime.TryParse(PlayerPrefs.GetString("CoundownFreeTimeM_2", DateTime.Now.AddDays(-200f).ToString()), out time);
        return time;
    }

    public static void SetFacebookShareDate(DateTime time)
    {
        PlayerPrefs.SetString("FacebookShareDateM_2", time.ToString());
    }

    public static DateTime GetFacebookShareDate()
    {
        DateTime time;
        DateTime.TryParse(PlayerPrefs.GetString("FacebookShareDateM_2", DateTime.Now.AddDays(-200f).ToString()), out time);
        return time;
    }

    public static void AddSpinTickets(int amount)
    {
        //PlayerPrefs.SetInt("SpinTicketsM", PlayerPrefs.GetInt("SpinTicketsM") + amount);

        AntiCheat.SetIntConverted("Ve_Choi_Vong_Quay_May_Man", AntiCheat.GetIntConverted("SpinTicketsM", "Ve_Choi_Vong_Quay_May_Man", 1) + amount);
    }

    public static void RemoveSpinTickets(int amount)
    {
        //int newAmount = PlayerPrefs.GetInt("SpinTicketsM", 1) - amount;
        int newAmount = AntiCheat.GetIntConverted("SpinTicketsM", "Ve_Choi_Vong_Quay_May_Man", 1) - amount;

        if (newAmount < 0)
            newAmount = 0;

        //PlayerPrefs.SetInt("SpinTicketsM", newAmount);
        AntiCheat.SetIntConverted("Ve_Choi_Vong_Quay_May_Man", newAmount);
    }

    public static int GetSpinTickets()
    {
        return AntiCheat.GetIntConverted("SpinTicketsM", "Ve_Choi_Vong_Quay_May_Man", 1);
    }

    public static void SetSpinTickets(int amount)
    {
        AntiCheat.SetIntConverted("Ve_Choi_Vong_Quay_May_Man", amount);
    }

    //Premium Pack
    public static void SetAlreadyPurchasePremiumPack(bool isPurchased)
    {
        //PlayerPrefs.SetInt("PremiumPackPurchasedM", isPurchased ? 1 : 0);
        AntiCheat.SetIntConverted("Da_Tra_Tien_Goi_Cao_Cap", isPurchased ? 1 : 0);
    }

    public static bool IsAlreadyPurchasePremiumPack()
    {
        //return PlayerPrefs.GetInt("PremiumPackPurchasedM") == 1 ? true :  false;
        return AntiCheat.GetIntConverted("PremiumPackPurchasedM", "Da_Tra_Tien_Goi_Cao_Cap", 0) == 1 ? true : false;
    }

    public static void SetPremiumPackExpiredTime(DateTime expiredTime)
    {
        PlayerPrefs.SetString("PremiumPackTimeM", expiredTime.ToString());
    }

    public static DateTime GetPremiumPackTimeExpiredTime()
    {
        DateTime time;
        DateTime.TryParse(PlayerPrefs.GetString("PremiumPackTimeM", DateTime.Now.ToString()), out time);
        return time;
    }

    //Lưu data vào fanpage facebook
    public static void SetAlreadyRewardFanpageFB()
    {
        PlayerPrefs.SetInt("AlreadyRewardFanpageFB_M", 1);
    }

    public static bool IsAlreadyRewardFanpageFB()
    {
        return PlayerPrefs.GetInt("AlreadyRewardFanpageFB_M", 0) == 1;
    }

    //Lưu data pvp
    public static void SetPrivateRoomNamePvP(string roomName)
    {
        PlayerPrefs.SetString("SetPrivateRoomNamePvP_M", roomName);
    }

    public static string GetPrivateRoomNamePvP()
    {
        return PlayerPrefs.GetString("SetPrivateRoomNamePvP_M", "");
    }

    public static void SetBettingAmountPvP(int amount)
    {
        PlayerPrefs.SetInt("M_BettingAmountPvP", amount);
    }

    public static int GetBettingAmountPvP()
    {
        return PlayerPrefs.GetInt("M_BettingAmountPvP", Constant.BETTING_AMOUNT_MIN);
    }

    public static void SetBettingAmountPvP2v2(int amount)
    {
        PlayerPrefs.SetInt("M_BettingAmountPvP2v2", amount);
    }

    public static int GetBettingAmountPvP2v2()
    {
        return PlayerPrefs.GetInt("M_BettingAmountPvP2v2", Constant.BETTING_AMOUNT_MIN);
    }

    //Check unlock star trong scene SelectLevel
    public static bool IsAlreadyUnlockLevel(int level, string difficult)
    {
        return AntiCheat.GetInt("M_UnlockStarsLevel" + difficult + level, 0) == 1;
    }

    public static void SetAlreadyUnlockLevel(int level, string difficult)
    {
        AntiCheat.SetInt("M_UnlockStarsLevel" + difficult + level, 1);
    }


    public static bool IsAlreadyNotifyLeftWingman()
    {
        return PlayerPrefs.GetInt("M_AlreadyNotifyLeftWingman", 0) == 1;
    }

    public static void SetAlreadyNotifyLeftWingman()
    {
        PlayerPrefs.SetInt("M_AlreadyNotifyLeftWingman", 1);
    }

    //Lưu data cho xem video theo giờ:
    public static void SetNextTimeReward(DateTime time)
    {
        PlayerPrefs.SetString("M_NextTimeReward", time.ToString());
    }

    public static DateTime GetNextTimeReward()
    {
        DateTime time;
        DateTime.TryParse(PlayerPrefs.GetString("M_NextTimeReward", DateTime.Now.ToString()), out time);
        return time;
    }


    //Lưu data unlock wingman, secondary weapon:
    public static bool IsAlreadyUnlockWeapon()
    {
        return PlayerPrefs.GetInt("M_IsAlreadyUnlockRightWingman", 0) == 1;
    }

    public static void SetAlreadyUnlockWeapon()
    {
        PlayerPrefs.SetInt("M_IsAlreadyUnlockRightWingman", 1);
    }

    //Lưu game version
    public static int GetVersion()
    {
        return PlayerPrefs.GetInt("M_GameVersion", -1);
    }

    public static void SetVersion(int version)
    {
        PlayerPrefs.SetInt("M_GameVersion", version);
    }

    //VipPack remote time
    public static DateTime GetExpiredRemoteVipPackTime(int testDay)
    {
        DateTime time;
        if (!PlayerPrefs.HasKey("M_ExpiredRemoteVipPackTime"))
        {
            time = DateTime.Now.AddDays(testDay);
            PlayerPrefs.SetString("M_ExpiredRemoteVipPackTime", time.ToString());
        }
        else
        {
            DateTime.TryParse(PlayerPrefs.GetString("M_ExpiredRemoteVipPackTime"), out time);
        }

        return time;
    }

    //Lưu cards
    public static void SetSpaceShipCards(AircraftTypeEnum type, int amount, string why, string where, string typeCard)
    {
        if (amount - GetSpaceShipCards(type) > 0)
        {
            FirebaseLogSpaceWar.LogCardIn(amount - GetSpaceShipCards(type), why, where, type.ToString());
        }
        else if (amount - GetSpaceShipCards(type) < 0)
        {
            FirebaseLogSpaceWar.LogCardOut(amount - GetSpaceShipCards(type), why, where, type.ToString());
        }
        AntiCheat.SetInt("sothecuamoi" + type + "conphithuyenspace", amount);
        PvpUtil.SendUpdatePlayer("SetSpaceShipCards:" + type.ToString(), why + "--" + where);
    }

    public static void SetSpaceShipCardsNamNX(AircraftTypeEnum type, int amount)
    {
        AntiCheat.SetInt("sothecuamoi" + type + "conphithuyenspace", amount);
        if (!GameContext.isInGameCampaign)
        {
            PlayerPrefs.Save();
        }
    }

    public static int GetSpaceShipCards(AircraftTypeEnum type)
    {
        return AntiCheat.GetInt("sothecuamoi" + type + "conphithuyenspace", 0);
    }

    public static void SetSpaceShipGeneralCards(int amount, string why, string where)
    {
        if (amount - GetSpaceShipGeneralCards() > 0)
        {
            FirebaseLogSpaceWar.LogCardIn(amount - GetSpaceShipGeneralCards(), why, where, "SpaceShipGeneralCards");
        }
        else if (amount - GetSpaceShipGeneralCards() < 0)
        {
            FirebaseLogSpaceWar.LogCardOut(amount - GetSpaceShipGeneralCards(), why, where, "SpaceShipGeneralCards");
        }
        AntiCheat.SetInt("tongthechungcuaphithuyen", amount);
        PvpUtil.SendUpdatePlayer("SetSpaceShipGeneralCards", why + "--" + where);
    }

    public static void SetSpaceShipGeneralCardsNamNX(int amount)
    {
        AntiCheat.SetInt("tongthechungcuaphithuyen", amount);
    }

    public static int GetSpaceShipGeneralCards()
    {
        return AntiCheat.GetInt("tongthechungcuaphithuyen", 0);
    }

    public static void SetWingmanCards(WingmanTypeEnum type, int amount, string why, string where, string typeCard)
    {
        if (amount - GetWingmanCards(type) > 0)
        {
            FirebaseLogSpaceWar.LogCardIn(amount - GetWingmanCards(type), why, where, type.ToString());
        }
        else if (amount - GetWingmanCards(type) < 0)
        {
            FirebaseLogSpaceWar.LogCardOut(amount - GetWingmanCards(type), why, where, type.ToString());
        }
        AntiCheat.SetInt("sothecuamoimaybayphu" + type + "conwingmanphithuyen", amount);
        PvpUtil.SendUpdatePlayer("SetWingmanCards:" + type.ToString(), why + "--" + where);
    }

    public static void SetWingmanCardsNamNX(WingmanTypeEnum type, int amount)
    {
        AntiCheat.SetInt("sothecuamoimaybayphu" + type + "conwingmanphithuyen", amount);
        if (!GameContext.isInGameCampaign)
        {
            PlayerPrefs.Save();
        }
    }

    public static int GetWingmanCards(WingmanTypeEnum type)
    {
        return AntiCheat.GetInt("sothecuamoimaybayphu" + type + "conwingmanphithuyen", 0);
    }

    public static void SetWingmanGeneralCards(int amount, string why, string where)
    {
        if (amount - GetWingmanGeneralCards() > 0)
        {
            FirebaseLogSpaceWar.LogCardIn(amount - GetWingmanGeneralCards(), why, where, "WingmanGeneralCards");
        }
        else if (amount - GetWingmanGeneralCards() < 0)
        {
            FirebaseLogSpaceWar.LogCardOut(amount - GetWingmanGeneralCards(), why, where, "WingmanGeneralCards");
        }
        AntiCheat.SetInt("tongthechungcuamaybayphu", amount);
        PvpUtil.SendUpdatePlayer("SetWingmanGeneralCards", why + "--" + where);
    }

    public static void SetWingmanGeneralCardsNamNX(int amount)
    {
        AntiCheat.SetInt("tongthechungcuamaybayphu", amount);
        if (!GameContext.isInGameCampaign)
        {
            PlayerPrefs.Save();
        }
    }

    public static int GetWingmanGeneralCards()
    {
        return AntiCheat.GetInt("tongthechungcuamaybayphu", 0);
    }

    public static void SetWingCards(WingTypeEnum type, int amount, string why, string where, string typeCard)
    {
        if (amount - GetWingCards(type) > 0)
        {
            FirebaseLogSpaceWar.LogCardIn(amount - GetWingCards(type), why, where, type.ToString());
        }
        else if (amount - GetWingCards(type) < 0)
        {
            FirebaseLogSpaceWar.LogCardOut(amount - GetWingCards(type), why, where, type.ToString());
        }
        AntiCheat.SetInt("WingCard_" + type, amount);
        PvpUtil.SendUpdatePlayer("SetWingCards:" + type.ToString(), why + "--" + where);
    }

    public static void SetWingCardsNamNX(WingTypeEnum type, int amount)
    {
        AntiCheat.SetInt("WingCard_" + type, amount);
        if (!GameContext.isInGameCampaign)
        {
            PlayerPrefs.Save();
        }
    }

    public static int GetWingCards(WingTypeEnum type)
    {
        return AntiCheat.GetInt("WingCard_" + type, 0);
    }

    public static void SetWingGeneralCards(int amount, string why, string where)
    {
        if (amount - GetWingGeneralCards() > 0)
        {
            FirebaseLogSpaceWar.LogCardIn(amount - GetWingGeneralCards(), why, where, "WingGeneralCards");
        }
        else if (amount - GetWingGeneralCards() < 0)
        {
            FirebaseLogSpaceWar.LogCardOut(amount - GetWingGeneralCards(), why, where, "WingGeneralCards");
        }
        AntiCheat.SetInt("WingGeneralCards", amount);
        PvpUtil.SendUpdatePlayer("SetWingGeneralCards", why + "--" + where);
    }

    public static void SetWingGeneralCardsNamNX(int amount)
    {
        AntiCheat.SetInt("WingGeneralCards", amount);
    }

    public static int GetWingGeneralCards()
    {
        return AntiCheat.GetInt("WingGeneralCards", 0);
    }

    //Đã convert data mới hay chưa (thêm hệ thống evolve máy bay và wingman, bỏ secondary weapon)
    public static bool IsAlreadyDataConverted1()
    {
        return PlayerPrefs.GetInt("M_AlreadyDataConverted1", 0) == 1;
    }

    public static void SetAlreadyDataConverted1()
    {
        PlayerPrefs.SetInt("M_AlreadyDataConverted1", 1);
    }

    //Ngày nhận thưởng hiện tại (PopupThirtyDaysLogin)
    //public static SetCurrentDayLoginRewarded()
    //{
    //    AntiCheat.SetInt();
    //}

    //Countdown lucky box:
    private static void SetLuckyBoxExpiredDate(DateTime time)
    {
        PlayerPrefs.SetString("LuckyBoxExpiredDate_M", time.ToString());
    }

    public static DateTime GetLuckyBoxExpiredDate()
    {
        if (!PlayerPrefs.HasKey("LuckyBoxExpiredDate_M"))
        {
            SetLuckyBoxExpiredDate(DateTime.Now.AddDays(3));
            return DateTime.Now.AddDays(3);
        }

        DateTime time;
        DateTime.TryParse(PlayerPrefs.GetString("LuckyBoxExpiredDate_M"), out time);
        return time;
    }

    //Thirty days login reward
    //Ngay show popup rate gan nhat
    public static void SetLastDayOf30Days()
    {
        AntiCheat.SetInt("LastDayOf30DaysM", CachePvp.dateTime.Day);
    }

    public static int GetLastDayOf30Days()
    {
        return AntiCheat.GetInt("LastDayOf30DaysM", -1);
    }

    //Index phần thưởng sắp nhận được (phần thưởng sẽ nhận được tiếp theo khi đủ điều kiện)
    public static void SetRewardIndexOf30Days(int index)
    {
        AntiCheat.SetInt("RewardIndexOf30DaysM", index);
    }

    public static int GetRewardIndexOf30Days()
    {
        return AntiCheat.GetInt("RewardIndexOf30DaysM", 1);
    }

    public static void SetLastDayOf30DaysForServer(int day)
    {
        AntiCheat.SetInt("LastDayOf30DaysM", day);
    }

    //Event:
    //Thêm bớt event resource
    public static void AddRemoveEventResources(string resourceName, int amount, bool isAccumulated)
    {
        int quantity = GetEventResources(resourceName) + amount;
        if (quantity < 0)
            quantity = 0;

        AntiCheat.SetInt(resourceName + "_resources_M", quantity);

        if (isAccumulated && amount > 0)
        {
            AddEventAccumulatedResources(resourceName, amount);
        }
    }

    //For sếp Nam
    public static void SetEventResources(string resourceName, int quantity)
    {
        AntiCheat.SetInt(resourceName + "_resources_M", quantity);
    }

    public static int GetEventResources(string resourceName)
    {
        return AntiCheat.GetInt(resourceName + "_resources_M", 0);
    }

    private static void AddEventAccumulatedResources(string resourceName, int amount)
    {
        AntiCheat.SetInt(resourceName + "_accumulated_resources_M", GetEventAccumulatedResources(resourceName) + amount);
    }

    public static void SetEventAccumulatedResources(string resourceName, int amount)
    {
        AntiCheat.SetInt(resourceName + "_accumulated_resources_M", amount);
    }

    public static int GetEventAccumulatedResources(string resourceName)
    {
        return AntiCheat.GetInt(resourceName + "_accumulated_resources_M", 0);
    }



    //Set số lần đã dùng để đổi 1 vật phẩm trong 1 sự kiện nào đó. Index là số thứ tự trong bản EventExchangeSheet. Time là số lần đã sử dụng.
    public static void SetExchangeUsedTimesOfEvent(string eventName, int index, int time)
    {
        AntiCheat.SetInt("M_ExchangeUsedTimesOfEvent_" + eventName + "_" + index, time);
    }

    public static int GetExchangeUsedTimesOfEvent(string eventName, int index)
    {
        return AntiCheat.GetInt("M_ExchangeUsedTimesOfEvent_" + eventName + "_" + index, 0);
    }

    public static void SetAlreadyClaimAccumulatedOfEvent(string eventName, int index)
    {
        AntiCheat.SetInt("M_AlreadyClaimAccumulatedOfEvent_" + eventName + "_" + index, 1);
    }

    public static bool IsAlreadyClaimAccumulatedOfEvent(string eventName, int index)
    {
        return AntiCheat.GetInt("M_AlreadyClaimAccumulatedOfEvent_" + eventName + "_" + index, 0) == 1;
    }

    //Set số lần sử dụng còn lại theo máy bay trong sự kiện halloween (mỗi máy bay được sử dụng 2 lần 1 ngày, mỗi lần dùng sẽ bị trừ đi)
    public static void SetAircraftUsedTimeHalloween(AircraftTypeEnum aircraftType, int usedTime)
    {
        AntiCheat.SetInt("M_AircraftUsedTimeHalloween_" + aircraftType, usedTime);
    }

    public static int GetAircraftUsedTimeHalloween(AircraftTypeEnum aircraftType)
    {
        return AntiCheat.GetInt("M_AircraftUsedTimeHalloween_" + aircraftType, 2);
    }

    public static void SetLastDayPlayLevelHalloween()
    {
        AntiCheat.SetInt("M_LastDayPlayLevelHalloween", CachePvp.dateTime.Day);
    }

    public static int GetLastDayPlayLevelHalloween()
    {
        return AntiCheat.GetInt("M_LastDayPlayLevelHalloween", -1);
    }

    //Số lần sử dụng máy bay để chơi tiếp sau khi chết trong ngày (mỗi máy bay được sử dụng 2 lần 1 ngày, mỗi lần dùng sẽ bị trừ đi)
    public static void SetAircraftUsedTimePerDay(AircraftTypeEnum aircraftType, int usedTime)
    {
        AntiCheat.SetInt("M_AircraftUsedTimePerDay_" + aircraftType, usedTime);
    }

    public static int GetAircraftUsedTimePerDay(AircraftTypeEnum aircraftType)
    {
        return AntiCheat.GetInt("M_AircraftUsedTimePerDay_" + aircraftType, GameContext.CALLING_BACKUP_TIMES);
    }

    public static void SetLastCallingBackUpAircraftUsed(int aircraftIndex)
    {
        AntiCheat.SetInt("M_LastCallingBackUpAircraftUsed", aircraftIndex);
    }

    public static int GetLastCallingBackUpAircraftUsed()
    {
        return AntiCheat.GetInt("M_LastCallingBackUpAircraftUsed", 0);
    }

    //Winter Rewards
    //Index: 1: Unlock, 2: Evolve rank S, 3: Evolve rank SS, 4: Evolve rank SSS
    public static bool IsPromoteEventClaimed(int index, string eventName)
    {
        return AntiCheat.GetInt("M_PromoteEventClaimed_" + eventName + index, 0) == 1;
    }

    public static void SetPromoteEventClaimed(int index, string eventName)
    {
        AntiCheat.SetInt("M_PromoteEventClaimed_" + eventName + index, 1);
    }


    //Time finish sale card pack
    public static void SetTimeFinishSaleCardPack()
    {
        PlayerPrefs.SetString("M_TimeFinishSaleCardPack", DateTime.Now.AddHours(24).ToString());

    }

    public static DateTime GetTimeFinishSaleCardPack()
    {
        DateTime time;
        DateTime.TryParse(PlayerPrefs.GetString("M_TimeFinishSaleCardPack", DateTime.Now.AddHours(24).ToString()), out time);
        return time;
    }

    //Star chest ở select level
    public static void SetStarChestClaimed(int world, string difficult, int index)
    {
        AntiCheat.SetInt("M_StarChestClaimed_" + world + "_" + difficult + "_" + index, 1);
    }

    public static bool IsStarChestClaimed(int world, string difficult, int index)
    {
        return AntiCheat.GetInt("M_StarChestClaimed_" + world + "_" + difficult + "_" + index, 0) == 1;
    }

    //StarChest extra:
    public static void SetStarChestExtraClaimed(int world, string difficult)
    {
        AntiCheat.SetInt("M_StarChestExtraClaimed_" + world + "_" + difficult, 1);
    }

    public static bool IsStarChestExtraClaimed(int world, string difficult)
    {
        return AntiCheat.GetInt("M_StarChestExtraClaimed_" + world + "_" + difficult, 0) == 1;
    }

    //Summer Holiday:
    public enum SummerHolidayMission
    {
        Campaign = 1,
        DailyQuest,
        GoldChest,
        DiamondChest
    }

    //2 hàm dưới của sếp Nam
    public static void SetEventMissionProgress(int index, int amount)
    {
        AntiCheat.SetInt("M_SummerHolidayMission_" + index, amount);
    }

    public static int GetEventMissionProgress(int index)
    {
        return AntiCheat.GetInt("M_SummerHolidayMission_" + index, 0);
    }


    //Trả về số lượng còn thiếu theo mission
    public static int GetEventMissionRemainProgress(int index)
    {
        switch (index)
        {
            case 1:
                return 400 - AntiCheat.GetInt("M_SummerHolidayMission_" + index, 0);

            case 2:
                return 200 - AntiCheat.GetInt("M_SummerHolidayMission_" + index, 0);

            case 3:
                return 600 - AntiCheat.GetInt("M_SummerHolidayMission_" + index, 0);

            case 4:
                return 600 - AntiCheat.GetInt("M_SummerHolidayMission_" + index, 0);

            default:
                return 0;
        }
    }

    //Lấy số lượng cần thiết được cộng vào để hiển thị
    public static int GetEventMissionCollectedToShow(int index, int amount)
    {
        int remain = GetEventMissionRemainProgress(index);

        if (amount > remain)
            return remain;
        else
            return amount;
    }

    //Cache lại số lượng mới thu thập được
    public static void AddEventMissionCollected(int index, int amount)
    {
        int remain = GetEventMissionRemainProgress(index);

        if (amount > remain)
            amount = remain;

        AddRemoveEventResources(EventItemType.IceCream_4_2019.ToString(), amount, false);
        AntiCheat.SetInt("M_SummerHolidayMission_" + index, AntiCheat.GetInt("M_SummerHolidayMission_" + index, 0) + amount);
    }

    //Số lượng lớn nhất có thể thu thập được theo mission
    public static int GetEventMissionMaxProgress(int index)
    {
        switch (index)
        {
            case 1:
                return 400;

            case 2:
                return 200;

            case 3:
                return 600;

            case 4:
                return 600;

            default:
                return 0;
        }
    }

    public static void ResetEventMissionProgress(int index)
    {
        AntiCheat.SetInt("M_SummerHolidayMission_" + index, 0);
    }

    //update thông tin đã mua royal pack chưa mỗi khi vào home
    public static void SetAlreadyBuyRoyalPack(bool isBuy)
    {
        AntiCheat.SetInt("M_AlreadyBuyRoyalPack_", isBuy ? 1 : 0);
    }

    public static bool IsAlreadyBuyRoyalPack()
    {
        return AntiCheat.GetInt("M_AlreadyBuyRoyalPack_", 0) == 1;
    }

    //Check xem có đang trong thời gian diễn ra sự kiện hay không:
    //Nhớ gọi hàm UpdateIsInEvent bên dưới trước khi gọi hàm này để đảm bảo độ chính xác
    public static bool IsInEvent(string eventName)
    {
        return AntiCheat.GetInt("M_IsInEvent_" + eventName, 0) == 1;
    }

    public static void UpdateIsInEvent(string eventName)
    {
        DateTime startTime = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayStartEvent, out startTime);

        DateTime endTime = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out endTime);

        if (DateTime.Now >= startTime && DateTime.Now < endTime)
            AntiCheat.SetInt("M_IsInEvent_" + eventName, 1);
        else
            AntiCheat.SetInt("M_IsInEvent_" + eventName, 0);
    }
}
