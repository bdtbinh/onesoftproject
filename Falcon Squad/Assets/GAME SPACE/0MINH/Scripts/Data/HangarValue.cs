﻿using System.Collections.Generic;
using I2.Loc;
using UnityEngine;

public static class HangarValue
{
    public static string AircraftNames(AircraftTypeEnum type)
    {
        switch (type)
        {
            case AircraftTypeEnum.BataFD:
                return ScriptLocalization.aircraft1_name;

            case AircraftTypeEnum.SkyWraith:
                return ScriptLocalization.aircraft2_name;

            case AircraftTypeEnum.FuryOfAres:
                return ScriptLocalization.aircraft3_name;

            case AircraftTypeEnum.CandyPine:
                return "The name CandyPine is not defined";

            case AircraftTypeEnum.RadiantStar:
                return "The name RadianStar is not defined";

            case AircraftTypeEnum.MacBird:
                return ScriptLocalization.aircraft6_name;

            case AircraftTypeEnum.TwilightX:
                return ScriptLocalization.aircraft7_name;

            case AircraftTypeEnum.StarBomb:
                return ScriptLocalization.aircraft8_name;

            case AircraftTypeEnum.IceShard:
                return ScriptLocalization.aircraft9_name;

            case AircraftTypeEnum.ThunderBolt:
                return ScriptLocalization.aircraft10_name;

            default:
                return "The name is not defined";
        }
    }

    public static string AircraftDescriptions(AircraftTypeEnum type)
    {
        switch (type)
        {
            case AircraftTypeEnum.BataFD:
                return ScriptLocalization.msg_aircraft_BataFD01;

            case AircraftTypeEnum.SkyWraith:
                return ScriptLocalization.msg_aircraft_SkyWraith;

            case AircraftTypeEnum.FuryOfAres:
                return ScriptLocalization.msg_aircraft_FuryOfAres;

            case AircraftTypeEnum.CandyPine:
                return "Error: HangarValue_line_13";

            case AircraftTypeEnum.RadiantStar:
                return "Error: HangarValue_line_14";

            case AircraftTypeEnum.MacBird:
                return ScriptLocalization.msg_aircraft_mac_bird;

            case AircraftTypeEnum.TwilightX:
                return ScriptLocalization.msg_aircraft_twilight_x;

            case AircraftTypeEnum.StarBomb:
                return ScriptLocalization.msg_aircraft_starbomb;

            case AircraftTypeEnum.IceShard:
                return ScriptLocalization.msg_aircraft_ice_shard;

            case AircraftTypeEnum.ThunderBolt:
                return ScriptLocalization.msg_aircraft_thunder_bolt;

            default:
                return "Default Description";
        }
    }

    public static string AircraftIconSpriteName(AircraftTypeEnum aircraftType, Rank rank)
    {
        return "aircraft" + (int)aircraftType + "_e" + (int)rank + "_idle_0";
    }

    //Dữ liệu wingman
    public static string WingmanNames(WingmanTypeEnum type)
    {
        switch (type)
        {
            case WingmanTypeEnum.GatlingGun:
                return ScriptLocalization.wingman1_name;

            case WingmanTypeEnum.AutoGatlingGun:
                return ScriptLocalization.wingman2_name;

            case WingmanTypeEnum.Lazer:
                return ScriptLocalization.wingman3_name;

            case WingmanTypeEnum.DoubleGalting:
                return ScriptLocalization.wingman4_name;

            case WingmanTypeEnum.HomingMissile:
                return ScriptLocalization.wingman5_name;

            case WingmanTypeEnum.Splasher:
                return ScriptLocalization.wingman6_name;

            default:
                return "The name is not defined";
        }
    }

    public static string WingmanDescriptions(WingmanTypeEnum type)
    {
        switch (type)
        {
            case WingmanTypeEnum.None:
                return "None Description";

            case WingmanTypeEnum.GatlingGun:
                return ScriptLocalization.msg_wingman_gatling_gun;

            case WingmanTypeEnum.AutoGatlingGun:
                return ScriptLocalization.msg_wingman_auto_galting_gun;

            case WingmanTypeEnum.Lazer:
                return ScriptLocalization.msg_wingman_lazer;

            case WingmanTypeEnum.DoubleGalting:
                return ScriptLocalization.msg_wingman_double_galting;

            case WingmanTypeEnum.HomingMissile:
                return ScriptLocalization.msg_wingman_homing_missile;

            case WingmanTypeEnum.Splasher:
                return ScriptLocalization.msg_wingman_splasher;

            default:
                return "Default Description";
        }
    }

    public static Dictionary<WingmanTypeEnum, string> WingmanIconWithBorderSpriteName = new Dictionary<WingmanTypeEnum, string>
    {
        { WingmanTypeEnum.GatlingGun, "hangar_drone_1" },
        { WingmanTypeEnum.AutoGatlingGun, "hangar_drone_2" },
        { WingmanTypeEnum.Lazer, "hangar_drone_3" },
        { WingmanTypeEnum.DoubleGalting, "hangar_drone_4" },
        { WingmanTypeEnum.HomingMissile, "hangar_drone_5" },
        { WingmanTypeEnum.Splasher, "hangar_drone_6" },
    };

    public static Dictionary<WingTypeEnum, string> WingIconWithBorderSpriteName = new Dictionary<WingTypeEnum, string>
    {
        { WingTypeEnum.WingOfJustice, "hangar_wing_1" },
        { WingTypeEnum.WingOfRedemption, "hangar_wing_2" },
        { WingTypeEnum.WingOfResolution, "hangar_wing_2" },
    };

    public static string WingmanIconSpriteName(WingmanTypeEnum wingmanType, Rank rank)
    {
        return "wingman" + (int)wingmanType + "_e" + (int)rank + "_idle_0";
    }

    //Dữ liệu wing
    public static string WingNames(WingTypeEnum type)
    {
        switch (type)
        {
            case WingTypeEnum.WingOfJustice:
                return ScriptLocalization.wing1_name;

            case WingTypeEnum.WingOfRedemption:
                return ScriptLocalization.wing2_name;

            case WingTypeEnum.WingOfResolution:
                return ScriptLocalization.wing3_name;

            default:
                return "The name is not defined";
        }
    }

    public static string WingIconSpriteName(WingTypeEnum type, Rank rank)
    {
        return "wing" + (int)type + "_e" + (int)rank + "_idle_0";
    }

    public static string WingDescriptions(WingTypeEnum type)
    {
        switch (type)
        {
            case WingTypeEnum.None:
                return "None Description";

            case WingTypeEnum.WingOfJustice:
                return ScriptLocalization.des_wing_of_justice;

            case WingTypeEnum.WingOfRedemption:
                return ScriptLocalization.des_wing_of_redemption;

            case WingTypeEnum.WingOfResolution:
                return ScriptLocalization.des_wing_of_resolution;

            default:
                return "Default Description";
        }
    }

    //Skills
    public static string SkillNames(int index)
    {
        switch (index)
        {
            case 1:
                return ScriptLocalization.skill1_name;
            case 2:
                return ScriptLocalization.skill2_name;
            case 3:
                return ScriptLocalization.skill3_name;
            case 4:
                return ScriptLocalization.skill4_name;
            case 5:
                return ScriptLocalization.skill5_name;
            case 6:
                return ScriptLocalization.skill6_name;
            case 7:
                return ScriptLocalization.skill7_name;
            case 8:
                return ScriptLocalization.skill8_name;
            case 9:
                return ScriptLocalization.skill9_name;
            case 10:
                return ScriptLocalization.skill10_name;
            case 11:
                return ScriptLocalization.skill11_name;
            case 12:
                return ScriptLocalization.skill12_name;
            case 13:
                return ScriptLocalization.skill13_name;
            case 14:
                return ScriptLocalization.skill14_name;
            case 15:
                return ScriptLocalization.skill15_name;
            case 16:
                return ScriptLocalization.skill16_name;
            case 17:
                return ScriptLocalization.skill17_name;
            case 18:
                return ScriptLocalization.skill18_name;
            case 19:
                return ScriptLocalization.skill19_name;
            case 20:
                return ScriptLocalization.skill20_name;
            case 21:
                return ScriptLocalization.skill21_name;
            case 22:
                return ScriptLocalization.skill22_name;
            case 23:
                return ScriptLocalization.skill23_name;
            case 24:
                return ScriptLocalization.skill24_name;
            case 25:
                return ScriptLocalization.skill25_name;
            case 26:
                return ScriptLocalization.skill26_name;
            case 27:
                return ScriptLocalization.skill27_name;
            case 28:
                return ScriptLocalization.skill28_name;
            case 29:
                return ScriptLocalization.skill29_name;
            case 30:
                return ScriptLocalization.skill30_name;
            case 31:
                return ScriptLocalization.skill31_name;
            case 32:
                return ScriptLocalization.skill32_name;

            default:
                return "The skill name is not defined";
        }
    }

    //Dữ liệu chung
    public const string HangarNormalBGName = "hangar_item_BG";
    public const string HangarSelectedBGName = "hangar_item_BG3";
    public const string HangarDisableBGName = "hangar_item_BG_off";

    public static string EventItemName(EventItemType type)
    {
        switch (type)
        {
            case EventItemType.IceCream_4_2019:
                return ScriptLocalization.event_item_ice_cream;

            default:
                return "The EventItemName is not defined";
        }
    }

    public static Dictionary<string, string> ItemSpriteName = new Dictionary<string, string>
    {
        { "Gold", "icon_coin" },
        { "Gold1", "icon_coin" },
        { "Gold2", "icon_coin" },
        { "Gold3", "icon_coin" },
        { "Gem", "icon_daily_quest_gem" },
        { "Life", "icon_dailyquest_life" },
        { "ActiveSkill", "icon_dailyquest_enegy" },
        { "PowerUp", "icon_dailyquest_powerup" },
        { "AircraftGeneralCard", "card_A0" },
        { "Aircraft1Card", "card_A1" },
        { "Aircraft2Card", "card_A2" },
        { "Aircraft3Card", "card_A3" },
        { "Aircraft6Card", "card_A6" },
        { "Aircraft7Card", "card_A7" },
        { "Aircraft8Card", "card_A8" },
        { "Aircraft9Card", "card_A9" },
        { "DroneGeneralCard", "card_D0" },
        { "Drone1Card", "card_D1" },
        { "Drone2Card", "card_D2" },
        { "Drone3Card", "card_D3" },
        { "Drone4Card", "card_D4" },
        { "Drone5Card", "card_D5" },
        { "Drone6Card", "card_D6" },
        { "BoxCard", "card_R"},
        { "BoxCard1", "card_R"},
        { "BoxCard2", "card_R"},
        { "BoxCard3", "card_R"},
        { "BoxCard4", "card_R"},
        { "HalloweenCandy", "Item_candy" },
        { "XMasBell", "Icon_bell_ingame"},
        { "WingGeneralCard", "card_W0" },
        { "Wing1Card", "card_W1" },
        { "Wing2Card", "card_W2" },
        { "Wing3Card", "card_W3" },
        { "Energy", "icon_daily_quest_enegy" },
        { "Aircraft10Card", "card_A10" },

    };

    public static Dictionary<BuyType, string> BuyResourceSpriteName = new Dictionary<BuyType, string>
    {
        { BuyType.Gold, "Gold_small" },
        { BuyType.Gem, "Gem_Green_small" },
        { BuyType.Energy, "Icon_enegy1" }
    };

    public static string GetUnlockDeviceNotify(string deviceName)
    {
        return ScriptLocalization.notifi_unlock_device_name.Replace("%{device_name}", deviceName);
    }

    public static Dictionary<Rank, string> RankSpriteName = new Dictionary<Rank, string>
    {
        { Rank.C, "hangar_rank_C" },
        { Rank.B, "hangar_rank_B" },
        { Rank.A, "hangar_rank_A" },
        { Rank.S, "hangar_rank_S" },
        { Rank.SS, "hangar_rank_SS" },
        { Rank.SSS, "hangar_rank_SSS" },
    };
}
