﻿using I2.Loc;

public static class SkillInfoDescription{

    public static string GetAircraftSkillDes(AircraftTypeEnum aircraftType, Rank rank)
    {
        switch (aircraftType)
        {
            case AircraftTypeEnum.BataFD:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_9_open_fire;

                    case Rank.B:
                        return ScriptLocalization.info_skill_1_upgrade_module.ReplaceSecond("1").ReplaceActiveSkill(9);

                    case Rank.A:
                        return ScriptLocalization.info_skill_2_heavy_shell.ReplacePercent("15");

                    case Rank.S:
                        return ScriptLocalization.info_skill_14_double_riffle;

                    case Rank.SS:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_8_big_cargo.ReplaceActiveSkill(9).ReplaceSkillNumber(1);
                }
                break;

            case AircraftTypeEnum.SkyWraith:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_10_fire_ball;

                    case Rank.B:
                        return ScriptLocalization.info_skill_1_upgrade_module.ReplaceSecond("1").ReplaceActiveSkill(10);

                    case Rank.A:
                        return ScriptLocalization.info_skill_2_heavy_shell.ReplacePercent("15");

                    case Rank.S:
                        return ScriptLocalization.info_skill_15_heavy_missle;

                    case Rank.SS:
                        return ScriptLocalization.info_skill_6_mystic_power.ReplaceSecond("3").ReplaceActiveSkill(10);

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_8_big_cargo.ReplaceActiveSkill(10).ReplaceSkillNumber(1);

                }
                break;

            case AircraftTypeEnum.FuryOfAres:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_11_fury_blade;

                    case Rank.B:
                        return ScriptLocalization.info_skill_1_upgrade_module.ReplaceSecond("1").ReplaceActiveSkill(11);

                    case Rank.A:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");

                    case Rank.S:
                        return ScriptLocalization.info_skill_16_laser_strike;

                    case Rank.SS:
                        return ScriptLocalization.info_skill_2_heavy_shell.ReplacePercent("20");

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_8_big_cargo.ReplaceActiveSkill(11).ReplaceSkillNumber(1);

                }
                break;

            case AircraftTypeEnum.MacBird:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_12_phoenix_rise;

                    case Rank.B:
                        return ScriptLocalization.info_skill_1_upgrade_module.ReplaceSecond("1").ReplaceActiveSkill(12);

                    case Rank.A:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");

                    case Rank.S:
                        return ScriptLocalization.info_skill_17_sonic_wave;

                    case Rank.SS:
                        return ScriptLocalization.info_skill_6_mystic_power.ReplaceSecond("4").ReplaceActiveSkill(12);

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_8_big_cargo.ReplaceActiveSkill(12).ReplaceSkillNumber(1);
                }
                break;

            case AircraftTypeEnum.TwilightX:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_13_x_laser;

                    case Rank.B:
                        return ScriptLocalization.info_skill_1_upgrade_module.ReplaceSecond("1").ReplaceActiveSkill(13);

                    case Rank.A:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");

                    case Rank.S:
                        return ScriptLocalization.info_skill_18_x_galting;

                    case Rank.SS:
                        return ScriptLocalization.info_skill_3_extra_power.ReplacePercent("15").ReplaceExtraGun(18);

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_8_big_cargo.ReplaceActiveSkill(13).ReplaceSkillNumber(1);
                }
                break;

            case AircraftTypeEnum.StarBomb:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_19_rain_of_bomb;

                    case Rank.B:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");

                    case Rank.A:
                        return ScriptLocalization.info_skill_6_mystic_power.ReplaceSecond("4").ReplaceActiveSkill(19);

                    case Rank.S:
                        return ScriptLocalization.info_skill_20_splash_shot;

                    case Rank.SS:
                        return ScriptLocalization.info_skill_3_extra_power.ReplacePercent("20").ReplaceExtraGun(20);

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_8_big_cargo.ReplaceActiveSkill(19).ReplaceSkillNumber(1);
                }
                break;

            case AircraftTypeEnum.IceShard:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_21_ice_storm;

                    case Rank.B:
                        return ScriptLocalization.info_skill_2_heavy_shell.ReplacePercent("15");

                    case Rank.A:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");

                    case Rank.S:
                        return ScriptLocalization.info_skill_22_ice_missile;

                    case Rank.SS:
                        return ScriptLocalization.info_skill_6_mystic_power.ReplaceSecond("4").ReplaceActiveSkill(21);

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_8_big_cargo.ReplaceActiveSkill(21).ReplaceSkillNumber(1);
                }
                break;

            case AircraftTypeEnum.ThunderBolt:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_31_lightning_storm;

                    case Rank.B:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");

                    case Rank.A:
                        return ScriptLocalization.info_skill_2_heavy_shell.ReplacePercent("20");

                    case Rank.S:
                        return ScriptLocalization.info_skill_32_lightning_strike;

                    case Rank.SS:
                        return ScriptLocalization.info_skill_3_extra_power.ReplacePercent("20").ReplaceExtraGun(32);

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_8_big_cargo.ReplaceActiveSkill(31).ReplaceSkillNumber(1);
                }

                break;
        }

        return "unknown";
    }

    public static string GetWingSkillDes(WingTypeEnum wingType, Rank rank)
    {
        switch (wingType)
        {
            case WingTypeEnum.None:
                return "None";

            case WingTypeEnum.WingOfJustice:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_23_judgement;

                    case Rank.B:
                        return ScriptLocalization.info_skill_25_magic_module.ReplaceActiveSkill(23).ReplaceSecond("2");

                    case Rank.A:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");

                    case Rank.S:
                        return ScriptLocalization.info_skill_26_overgrowth_force.ReplaceActiveSkill(23).ReplaceSkillNumber(1);

                    case Rank.SS:
                        return ScriptLocalization.info_skill_27_power_unlock.ReplacePercent("20");

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_26_overgrowth_force.ReplaceActiveSkill(23).ReplaceSkillNumber(1);
                }
                break;

            case WingTypeEnum.WingOfRedemption:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_24_atonement;

                    case Rank.B:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");

                    case Rank.A:
                        return ScriptLocalization.info_skill_28_huge_cargo.ReplaceSkillNumber(1);

                    case Rank.S:
                        return ScriptLocalization.info_skill_26_overgrowth_force.ReplaceActiveSkill(24).ReplaceSkillNumber(1); ;

                    case Rank.SS:
                        return ScriptLocalization.info_skill_27_power_unlock.ReplacePercent("20");

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");
                }
                break;

            case WingTypeEnum.WingOfResolution:
                switch (rank)
                {
                    case Rank.C:
                        return ScriptLocalization.info_skill_29_skywrath_blade;

                    case Rank.B:
                        return ScriptLocalization.info_skill_7_life_surge.ReplaceLife("1");

                    case Rank.A:
                        return ScriptLocalization.info_skill_1_upgrade_module.ReplaceSecond("3").ReplaceActiveSkill(29);

                    case Rank.S:
                        return ScriptLocalization.info_skill_26_overgrowth_force.ReplaceActiveSkill(29).ReplaceSkillNumber(1);

                    case Rank.SS:
                        return ScriptLocalization.info_skill_27_power_unlock.ReplacePercent("20");

                    case Rank.SSS:
                        return ScriptLocalization.info_skill_30_ultra_shell.ReplacePercent("15");
                }
                break;
        }

        return "unknown";
    }

    private static string ReplaceSecond(this string str, string second)
    {
        return str.Replace("%{second_number}", second);
    }

    private static string ReplaceActiveSkill(this string str, int index)
    {
        return str.Replace("%{active_skill}", HangarValue.SkillNames(index));
    }

    private static string ReplaceSkillNumber(this string str, int number)
    {
        return str.Replace("%{skill_number}", number.ToString());
    }

    private static string ReplacePercent(this string str, string percent)
    {
        return str.Replace("%{percent_number}", percent);
    }

    private static string ReplaceLife(this string str, string life)
    {
        return str.Replace("%{life_number}", life);
    }

    private static string ReplaceExtraGun(this string str, int index)
    {
        return str.Replace("%{extra_gun}", HangarValue.SkillNames(index));
    }
}

