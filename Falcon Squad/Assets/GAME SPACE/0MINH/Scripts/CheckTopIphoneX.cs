﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckTopIphoneX : MonoBehaviour
{

    // Use this for initialization
    public Vector3 posTopIPx;
    void Start()
    {
        //#if UNITY_IPHONE
        if ((float)Screen.height / (float)Screen.width > 2.15)
        {
            transform.localPosition = posTopIPx;
        }
        //#endif
    }

}
