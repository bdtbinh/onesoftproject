﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingSceneManager : MonoBehaviour
{
    [SerializeField]
    private UILabel percentText;

    [SerializeField]
    private GameObject homeUI;

    [SerializeField]
    private GameObject mainUI;

    [HideInInspector]
    public bool loadingOpened = false;

    private AsyncOperation progress;

    private void Awake()
    {
        instance = this;
    }

    public void LoadSceneHomeUI(string sceneName)
    {
        homeUI.SetActive(true);
        loadingOpened = true;
        DOTween.KillAll();
        LeanTween.cancelAll();
        StartCoroutine(FakeProgress(sceneName));
    }

    public void LoadSceneMainUI(string sceneName)
    {
        mainUI.SetActive(true);
        loadingOpened = true;
        DOTween.KillAll();
        LeanTween.cancelAll();
        StartCoroutine(FakeProgress(sceneName));
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    IEnumerator FakeProgress(string sceneName)
    {
        progress = SceneManager.LoadSceneAsync(sceneName);
        progress.allowSceneActivation = false;

        while (progress.progress < 0.9f)
        {
            int percentNumber = (int)(progress.progress * 100 / 0.9f);
            percentText.text = percentNumber + "%";

            if (percentText != null)
                yield return null;
        }

        if (percentText != null)
            percentText.text = "100%";

        yield return null;
        progress.allowSceneActivation = true;
    }

    private static LoadingSceneManager instance;
    public static LoadingSceneManager Instance
    {
        get { return instance; }
    }
}

