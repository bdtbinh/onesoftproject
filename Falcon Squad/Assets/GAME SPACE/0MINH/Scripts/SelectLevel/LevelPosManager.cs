﻿using UnityEngine;
using System.Collections.Generic;
using com.ootii.Messages;
using Mp.Pvp;
using System;
using Sirenix.OdinInspector;
using System.Reflection;
//using UnityEditor;

public class LevelPosManager : MonoBehaviour
{
    [SerializeField]
    private ParallaxObject[] parallaxObjects;

    [SerializeField]
    private ParallaxPlanet[] parallaxPlanets;

    [SerializeField]
    private GameObject endNode;

    [SerializeField]
    private GameObject levelPrefab;

    [SerializeField]
    private UIScrollBar scrollbar;

    [SerializeField]
    private UIScrollView scrollview;

    [SerializeField]
    private List<LevelNode> listLevelNode;
    private Transform[] listLevelIconTrans;         //Ảnh icon level. Dùng để tính xem đang ở world nào

    //[SerializeField]
    //private SpriteRenderer[] listCloudEndNode;

    [SerializeField]
    private float offsetToBoundY;               //Distance from update point to nearest boundY of screen. Using for update nodes position.

    private const int DELTA_Y = 3278;           //Distance of 1 node loop
    private float scrollerHeight;               //It's equal to the value 1 of the scrollbar 
    private float offsetY;                      //Distance from update point to center of screen. Using for update nodes position.

    private Vector3[] initLocalPos;              //The 14 nodes position when this scene is loaded
    private float lastY;                        //Scrollview y position at previous update call
    private int currentLevel = 1;

    private LevelNode[] listForReset;       //Use for reset list when change difficult mode

    public GameObject backgroundFriends;
    public List<GameObject> listGameObjectAvatar;
    public List<UI2DSprite> listSpriteAvatar;
    public UILabel lFriendsPass;

    List<Player> listPlayer = new List<Player>();

    //private float scrollviewPanelHeight = 1280.0f;

    private void Awake()
    {
        SetEndNodePosition();
        listForReset = new LevelNode[listLevelNode.Count];

        for (int i = 0; i < listLevelNode.Count; i++)
        {
            LevelItemObj levelObject = NGUITools.AddChild(listLevelNode[i].gameObject, levelPrefab).GetComponent<LevelItemObj>();
            listLevelNode[i].SetLevelItemObject(levelObject);

            listForReset[i] = listLevelNode[i];
        }

        listLevelIconTrans = new Transform[listLevelNode.Count];

        for (int i = 0; i < listLevelNode.Count; i++)
        {
            listLevelIconTrans[i] = MinhUtils.FindChild(listLevelNode[i].transform, "sIconLevel");
        }

        if (GameContext.IS_CHINA_VERSION)
        {
            if (!string.IsNullOrEmpty(CachePvp.GamecenterId))
            {
                new CSLeaderboard(5).Send();
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(CachePvp.FacebookId))
            {
                new CSLeaderboard(5).Send();
            }
        }

        backgroundFriends.SetActive(false);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedFacebookRank.ToString(), OnLoadedFacebookRank, true);
        MessageDispatcher.AddListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.AddListener(EventName.SyncData.SyncFromClientToServer.ToString(), OnSyncFromClientToServer, true);
        MessageDispatcher.AddListener(EventName.SyncData.SyncFromServerToClient.ToString(), OnSyncFromServerToClient, true);
        Debug.Log("Level pos manager -  CachePvp.needSync : " + CachePvp.needSync);
        if (CachePvp.needSync)
        {
            CachePvp.needSync = false;
            if (string.IsNullOrEmpty(CachePvp.deviceId))
            {
#if UNITY_EDITOR
                if (SystemInfo.deviceModel.Equals("MS-7971 (MSI)"))
                {
                    CachePvp.deviceId = "editornamnx";
                    new CSValidateDeviceId(CachePvp.deviceId).Send();
                }
                else if (SystemInfo.deviceModel.Equals("MS-7A70 (MSI)"))
                {
                    CachePvp.deviceId = "editorminhddv2";
                    new CSValidateDeviceId(CachePvp.deviceId).Send();
                }
                else
                {
                    new CSGetToken(CachePvp.deviceId, CachePvp.FacebookId).Send();
                    MessageDispatcher.SendMessage(EventName.SyncData.NoSync.ToString());
                }
#else
                bool check = Application.RequestAdvertisingIdentifierAsync((string advertisingId, bool trackingEnabled, string error) =>
                {
                    CachePvp.deviceId = advertisingId;
                    new CSValidateDeviceId(advertisingId).Send();
                });
                if (!check)
                {
                    new CSGetToken(CachePvp.deviceId, CachePvp.FacebookId).Send();
                    MessageDispatcher.SendMessage(EventName.SyncData.NoSync.ToString());
                }
#endif
            }
            else
            {
                new CSValidateDeviceId(CachePvp.deviceId).Send();
            }
        }
    }

    void OnSyncFromServerToClient(IMessage msg)
    {
        DataSync data = msg.Data as DataSync;
        CachePvp.SetPlayerData(data.player, data.token);
        if (CachePvp.facebookUser != null)
        {
            new CSFacebookConnect(SocialManager.Instance.FBGetUserToken(), CachePvp.facebookUser.id, CachePvp.facebookUser.name, 2).Send();
        }
    }

    void OnSyncFromClientToServer(IMessage msg)
    {
        if (GameContext.IS_CHINA_VERSION)
        {
            if (Social.localUser.authenticated)
            {
                new CSAppCenterConnect("", Social.localUser.id, Social.localUser.userName, 1).Send();
            }
        }
        else
        {
            if (CachePvp.facebookUser != null)
            {
                new CSFacebookConnect(SocialManager.Instance.FBGetUserToken(), CachePvp.facebookUser.id, CachePvp.facebookUser.name, 1).Send();
            }
        }
    }

    private void OnRefreshAvatar(IMessage rMessage)
    {
        for (int i = 0; i < listPlayer.Count; i++)
        {
            bool showFb = true;
            if (GameContext.IS_CHINA_VERSION)
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(listPlayer[i].appCenterId))
                {
                    showFb = false;
                    listSpriteAvatar[i].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[listPlayer[i].appCenterId];
                    break;
                }
            }
            if (showFb)
            {
                if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(listPlayer[i].facebookId))
                {
                    listSpriteAvatar[i].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[listPlayer[i].facebookId];
                    break;
                }
            }
        }
    }

    public void ShowProfile0()
    {
        PopupManager.Instance.ShowProfilePopup(listPlayer[0]);
    }
    public void ShowProfile1()
    {
        PopupManager.Instance.ShowProfilePopup(listPlayer[1]);
    }
    public void ShowProfile2()
    {
        PopupManager.Instance.ShowProfilePopup(listPlayer[2]);
    }
    public void ShowProfile3()
    {
        PopupManager.Instance.ShowProfilePopup(listPlayer[3]);
    }
    public void ShowProfile4()
    {
        PopupManager.Instance.ShowProfilePopup(listPlayer[4]);
    }

    public void ShowFriends(List<Player> list, int level)
    {
        listPlayer.Clear();

        lFriendsPass.text = I2.Loc.ScriptLocalization.msg_friends_passed_level.Replace("%{level_info}", level.ToString());
        backgroundFriends.SetActive(true);
        for (int i = 0; i < listGameObjectAvatar.Count; i++)
        {
            listGameObjectAvatar[i].SetActive(false);
        }
        for (int i = 0; i < list.Count; i++)
        {
            if (i < 5)
            {
                listGameObjectAvatar[i].SetActive(true);
                bool showImageFB = true;
                if (GameContext.IS_CHINA_VERSION)
                {
                    if (!string.IsNullOrEmpty(list[i].appCenterId))
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(list[i].appCenterId))
                        {
                            showImageFB = false;
                            listSpriteAvatar[i].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[list[i].appCenterId];
                        }
                        else
                        {
                            Texture2D t = CacheAvatarManager.Instance.GetTextureByGamecenterId(list[i].appCenterId);
                            if (t != null)
                            {
                                showImageFB = false;
                                CacheAvatarManager.Instance.DownloadImageFormUrl("", list[i].appCenterId, t);
                            }
                        }
                    }
                }
                if (showImageFB)
                {
                    if (string.IsNullOrEmpty(list[i].facebookId))
                    {
                        listSpriteAvatar[i].sprite2D = CacheAvatarManager.Instance.defaultAvatar;
                    }
                    else
                    {
                        if (CacheAvatarManager.Instance.dictImageDownloaded.ContainsKey(list[i].facebookId))
                        {
                            listSpriteAvatar[i].sprite2D = CacheAvatarManager.Instance.dictImageDownloaded[list[i].facebookId];
                        }
                        else
                        {
                            string query = "https://graph.facebook.com/" + list[i].facebookId + "/picture?type=small&width=135&height=135";
                            CacheAvatarManager.Instance.DownloadImageFormUrl(query, list[i].facebookId);
                        }
                    }
                }
                listPlayer.Add(list[i]);
            }
        }
    }

    private void OnLoadedFacebookRank(IMessage rMessage)
    {
        if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
        {
            Leaderboard leaderboard = rMessage.Data as Leaderboard;
            CachePvp.listPlayerLeaderboard.Clear();
            for (int i = 0; i < leaderboard.topRanking.Count; i++)
            {
                CachePvp.listPlayerLeaderboard.Add(leaderboard.topRanking[i]);
            }
        }
        //ReloadScroll();
    }

    void ReloadScroll()
    {
        for (int i = 0; i < listLevelNode.Count; i++)
        {
            listLevelNode[i].ReloadData();
        }
    }

    void SetEndNodePosition()
    {
        Vector3 deltaPos = endNode.transform.localPosition - listLevelNode[listLevelNode.Count - 1].transform.localPosition;
        int totalLevel = GameContext.TOTAL_LEVEL;

        if (totalLevel % 14 == 0)
        {
            endNode.transform.localPosition += new Vector3(0, (totalLevel / 14 - 1) * DELTA_Y, 0);
        }
        else
        {
            endNode.transform.localPosition = listLevelNode[6].transform.localPosition + new Vector3(0, (totalLevel / 14) * DELTA_Y, 0) + deltaPos;
        }
    }

    void Start()
    {
        //Debug.LogError("vao: " + listLevelNode[6].Level + " * " + listLevelNode[6].transform.position.y + " * " + listLevelNode[6].transform.localPosition.y + " * " + scrollview.transform.position.y + " * " + scrollview.transform.localPosition.y);


        MessageDispatcher.AddListener(EventID.ON_CHANGE_DIFFICULT_MODE, OnChangeDifficultMode, true);

        //scrollviewPanelHeight = 720 * Screen.height / Screen.width;
        //Debug.LogError("start: " + scrollviewPanelHeight + " * " + scrollview.panel.GetViewSize() + " * " + scrollview.panel.GetWindowSize());

        scrollerHeight = scrollview.bounds.size.y - scrollview.panel.GetWindowSize().y;
        offsetY = scrollview.panel.GetWindowSize().y / 2 + offsetToBoundY;

        //Set initlocalPosY
        initLocalPos = new Vector3[listLevelNode.Count];

        for (int i = 0; i < initLocalPos.Length; i++)
        {
            initLocalPos[i] = listLevelNode[i].transform.localPosition;
        }
        ReloadScrollview();
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventID.ON_CHANGE_DIFFICULT_MODE, OnChangeDifficultMode, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedFacebookRank.ToString(), OnLoadedFacebookRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadProfile.RefreshAvatar.ToString(), OnRefreshAvatar, true);
        MessageDispatcher.RemoveListener(EventName.SyncData.SyncFromClientToServer.ToString(), OnSyncFromClientToServer, true);
        MessageDispatcher.RemoveListener(EventName.SyncData.SyncFromServerToClient.ToString(), OnSyncFromServerToClient, true);
    }

    private void Update()
    {
        //Debug.LogError(scrollview.transform.position + " * " + scrollview.transform.localPosition);
        float scrollviewY = -scrollview.transform.localPosition.y;

        if (scrollviewY > lastY)
        {
            CheckToMoveNodeUp(scrollviewY);
            backgroundFriends.SetActive(false);
            DoParallax(new Vector2(0, scrollviewY - lastY));

            for (int i = 0; i < parallaxPlanets.Length; i++)
            {
                parallaxPlanets[i].DoParallaxUp(new Vector2(0, scrollviewY - lastY), scrollviewY - offsetY, DELTA_Y);
            }
        }
        else if (scrollviewY < lastY)
        {
            CheckToMoveNodeDown(scrollviewY);
            backgroundFriends.SetActive(false);
            DoParallax(new Vector2(0, scrollviewY - lastY));

            for (int i = 0; i < parallaxPlanets.Length; i++)
            {
                parallaxPlanets[i].DoParallaxDown(new Vector2(0, scrollviewY - lastY), scrollviewY + offsetY, DELTA_Y);
            }
        }

        lastY = scrollviewY;

        //Debug.LogWarning("vaooooooooooooooo: " + scrollview.panel.GetWindowSize().y + ", screensize: " + Screen.height);
    }

    void DoParallax(Vector2 offset)
    {
        for (int i = 0; i < parallaxObjects.Length; i++)
        {
            parallaxObjects[i].DoParalax(offset);
        }
    }


    void CheckToMoveNodeUp(float centerPoint)
    {
        for (int i = 0; i < listLevelNode.Count; i++)
        {
            //Debug.LogError("Move Up: " + listLevelNode[i].Level + ", LocalPos: " + listLevelNode[i].transform.localPosition.y + ", CenterPoint: " + centerPoint + ", offsetY: " + offsetY);

            if (listLevelNode[i].transform.localPosition.y < centerPoint - offsetY)
            {
                if (listLevelNode[i].Level + 14 <= GameContext.TOTAL_LEVEL)
                {
                    listLevelNode[i].transform.localPosition = new Vector3(listLevelNode[i].transform.localPosition.x, listLevelNode[i].transform.localPosition.y + DELTA_Y, listLevelNode[i].transform.localPosition.z);
                    listLevelNode[i].SetLevel(listLevelNode[i].Level + 14);
                    listLevelNode.MoveAnItemToEnd(0);
                }
            }
        }

        CheckToChangeWorld("CheckToMoveNodeUp", centerPoint);
    }

    void CheckToMoveNodeDown(float centerPoint)
    {
        for (int i = listLevelNode.Count - 1; i >= 0; i--)
        {
            //Debug.LogError("Move Down: " + listLevelNode[i].Level + ", LocalPos: " + listLevelNode[i].transform.localPosition.y + ", CenterPoint: " + centerPoint + ", offsetY: " + offsetY);
            if (listLevelNode[i].transform.localPosition.y > centerPoint + offsetY)
            {
                if (listLevelNode[i].Level - 14 >= 1)
                {
                    //Debug.LogError("Down: 1 " + listLevelNode[i].Level + " * " + listLevelNode[i].transform.localPosition.y + " * " + listLevelNode[i].transform.position.y);
                    listLevelNode[i].transform.localPosition = new Vector3(listLevelNode[i].transform.localPosition.x, listLevelNode[i].transform.localPosition.y - DELTA_Y, listLevelNode[i].transform.localPosition.z);
                    listLevelNode[i].SetLevel(listLevelNode[i].Level - 14);
                    //Debug.LogError("Down: 2 " + listLevelNode[i].LevelTest + " * " + listLevelNode[i].Level + " * " + listLevelNode[i].transform.localPosition.y + " * " + listLevelNode[i].transform.position.y);
                    listLevelNode.MoveAnItemToFront(listLevelNode.Count - 1);
                }
            }
        }

        CheckToChangeWorld("CheckToMoveNodeDown", centerPoint);
    }

    void OnChangeDifficultMode(IMessage msg)
    {
        for (int i = 0; i < listLevelNode.Count; i++)
        {
            listLevelNode[i] = listForReset[i];
        }

        ReloadScrollview();
        MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_SELECT_LEVEL_WORLD, currentWorld, 0f);
    }

    void ReloadScrollview()
    {
        //Must call with the following order
        //Debug.LogError("ReloadScrollview");
        UpdateCurrentLevel();
        SetLevelsNumber();
        ReloadScrollUI();
        ReloadEndNodeCloudColor();

        for (int i = 0; i < parallaxPlanets.Length; i++)
        {
            parallaxPlanets[i].ResetPosition();
        }
        //CheckToChangeWorld("ReloadScrollview");
    }

    void UpdateCurrentLevel()
    {
        if (CacheGame.GetDifficultCampaign() == GameContext.DIFFICULT_NOMAL)
        {
            currentLevel = CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL);

        }
        else if (CacheGame.GetDifficultCampaign() == GameContext.DIFFICULT_HARD)
        {
            currentLevel = CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD);

        }
        else
        {
            currentLevel = CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL);
        }

        if (currentLevel > GameContext.TOTAL_LEVEL)
            currentLevel = GameContext.TOTAL_LEVEL;
    }

    void SetLevelsNumber()
    {
        int multiNumber = Mathf.CeilToInt(currentLevel / 14f) - 1;


        for (int i = 0; i < listLevelNode.Count; i++)
        {
            if (i + 1 + multiNumber * 14 <= GameContext.TOTAL_LEVEL)
            {
                listLevelNode[i].transform.localPosition = new Vector3(initLocalPos[i].x, multiNumber * DELTA_Y + initLocalPos[i].y, initLocalPos[i].z);
                listLevelNode[i].SetLevel(i + 1 + multiNumber * 14);
            }
            else
            {
                listLevelNode[i].transform.localPosition = new Vector3(initLocalPos[i].x, (multiNumber - 1) * DELTA_Y + initLocalPos[i].y, initLocalPos[i].z);
                listLevelNode[i].SetLevel(i + 1 + (multiNumber - 1) * 14);
            }
            //Debug.LogError("Down: 3 " + listLevelNode[i].Level + " * " + listLevelNode[i].transform.localPosition.y + " * " + listLevelNode[i].transform.position.y);
        }

    }

    void ReloadScrollUI()
    {
        for (int i = 0; i < listLevelNode.Count; i++)
        {
            if (listLevelNode[i].Level == currentLevel)
            {
                lastY = listLevelNode[i].transform.localPosition.y;
                break;
            }
        }

        //Debug.LogError("MinY: " + scrollview.bounds.min.y + ", MaxY: " + scrollview.bounds.max.y + ", height: " + scrollview.panel.GetWindowSize().y + ", height/2: " + scrollview.panel.GetWindowSize().y / 2);
        float maxY = scrollview.bounds.max.y - scrollview.panel.GetWindowSize().y / 2;
        float minY = scrollview.bounds.min.y + scrollview.panel.GetWindowSize().y / 2;

        //Debug.LogError("CurrentLevel1: " + currentLevel + ", lastY: " + lastY + ", minY: " + minY + ", maxY: " + maxY);

        lastY = Mathf.Clamp(lastY, minY, maxY);
        //Debug.LogError("CurrentLevel2 " + currentLevel + ", lastY: " + lastY + ", minY: " + minY + ", maxY: " + maxY + ", scrollheight: " + scrollerHeight);

        scrollbar.value = (maxY - lastY) / scrollerHeight;
        //Debug.LogError("CurrentLevel3: " + currentLevel + ", lastY: " + lastY + ", minY: " + minY + ", maxY: " + maxY + ", valuebar: " + scrollbar.value);


        if (currentLevel % 14 == 0 || currentLevel % 14 > 7)
        {
            CheckToMoveNodeUp(lastY);
        }
        else
        {
            CheckToMoveNodeDown(lastY);
        }
    }

    void ReloadEndNodeCloudColor()
    {
        //switch (CacheGame.GetDifficultCampaign())
        //{
        //    case "Normal":
        //        for (int i = 0; i < listCloudEndNode.Length; i++)
        //        {
        //            listCloudEndNode[i].color = Constant.NORMAL_COLOR_CLOUD;
        //        }

        //        break;

        //    case "Hard":
        //        for (int i = 0; i < listCloudEndNode.Length; i++)
        //        {
        //            listCloudEndNode[i].color = Constant.HARD_COLOR_CLOUD;
        //        }

        //        break;

        //    case "Hell":
        //        for (int i = 0; i < listCloudEndNode.Length; i++)
        //        {
        //            listCloudEndNode[i].color = Constant.HELL_COLOR_CLOUD;
        //        }

        //        break;
        //    default:
        //        break;
        //}
    }

    LevelNode node = null;
    //[Button]
    public void CheckToChangeWorld(string where, float centerY)
    {
        node = null;
        for (int i = 0; i < listLevelNode.Count; i++)
        {
            //Debug.LogError("vao: " + listLevelNode[i].transform.localPosition.y + ", " + centerY);
            if (listLevelNode[i].transform.localPosition.y >= centerY)
            {
                if (node == null)
                {
                    node = listLevelNode[i];
                }
                else
                {
                    if (listLevelNode[i].transform.localPosition.y < node.transform.localPosition.y)
                    {
                        node = listLevelNode[i];
                    }
                }
            }
        }

        if (node != null)
        {
            //Debug.LogError("Node: " + node.Level + " * " + node.transform.localPosition.y + ", where: " + where);
            //Debug.LogError(node.Level + " * " + Mathf.CeilToInt(node.Level / 7f));
            CheckToChangeWorld(Mathf.CeilToInt(node.Level / 7f));
        }
        else
        {
            //Debug.LogError("Node = null");
        }
    }

    int currentWorld = 0;
    void CheckToChangeWorld(int newWorld)
    {
        if (currentWorld != newWorld)
        {
            currentWorld = newWorld;
            MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_SELECT_LEVEL_WORLD, newWorld, 0f);
            //Debug.LogError("World: CheckToChangeWorld * " + newWorld);
        }
    }

}
