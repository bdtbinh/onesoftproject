﻿using UnityEngine;

public class PanelExLevelRequire : MonoBehaviour {

    [SerializeField]
    private UISprite sAircraftIcon;

    [SerializeField]
    private UISprite sAircraftRank;
    

	public void ShowPanel(AircraftTypeEnum aircraftType, Rank rank)
    {
        sAircraftIcon.spriteName = HangarValue.AircraftIconSpriteName(aircraftType, rank);
        sAircraftRank.spriteName = HangarValue.RankSpriteName[rank];

        Vector2 localScale = sAircraftIcon.transform.localScale;
        sAircraftIcon.MakePixelPerfect();
        sAircraftIcon.transform.localScale = localScale;
        sAircraftRank.MakePixelPerfect();

        gameObject.SetActive(true);
    }
}
