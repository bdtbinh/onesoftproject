﻿using UnityEngine;

public class ParallaxMaterial : ParallaxObject
{
    [SerializeField]
    private float speed;

    private Renderer render;

    private void Awake()
    {
        render = GetComponent<Renderer>();
    }

    public override void DoParalax(Vector2 offset)
    {
        render.material.mainTextureOffset += offset * speed;
    }
}
