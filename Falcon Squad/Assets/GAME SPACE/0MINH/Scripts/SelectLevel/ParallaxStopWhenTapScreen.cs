﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxStopWhenTapScreen : MonoBehaviour
{

    [SerializeField]
    private float speed;

    [SerializeField]
    private Renderer render;
    bool isStop = false;

    private void Update()
    {
        if (!isStop)
        {
            render.material.mainTextureOffset += new Vector2(0, speed * Time.deltaTime);
        }
    }

    void OnPress(bool isPressed)
    {
        if (isPressed)
        {
            isStop = true;
        }
        else
        {
            isStop = false;
        }
    }
}
