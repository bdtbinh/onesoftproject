﻿using UnityEngine;
using com.ootii.Messages;

public class StarChestManager : MonoBehaviour
{
    [SerializeField]
    private UILabel lLevelNumber;

    [SerializeField]
    private UILabel[] lRequireStars;        //Số sao cần thiết để mở từng loại chest.

    [SerializeField]
    private UILabel lRequireStarsExtra;     //Số sao cần thiết để chest extra.

    [SerializeField]
    private UILabel lTotalStars;         //Tổng số sao của world hiện tại

    [SerializeField]
    private UILabel lTotalStarsExtra;            //Tổng số sao level extra của world hiện tại

    [SerializeField]
    private UISprite[] sStars;

    [SerializeField]
    private GameObject[] tickSprites;         //Dấu tick hiện lên chest đã được nhận thưởng

    [SerializeField]
    private GameObject tickSpriteExtra;

    [SerializeField]
    private UISprite[] sChests;

    [SerializeField]
    private UISprite sChestExtra;

    [SerializeField]
    private TweenScale[] tweenScales;

    [SerializeField]
    private TweenScale tweenScaleExtra;

    int currentWorld = 0;
    string difficult = "";
    int minLevelWorld = 0;
    int maxLevelWorld = 0;
    int totalStarWorld = 0;
    int totalStarExtra = 0;

    private void Awake()
    {
        if (CacheGame.GetMaxLevel3Difficult() <= 3)
        {
            gameObject.SetActive(false);
        }

        for (int i = 0; i < Constant.STAR_UNLOCK_REQUIRE.Length; i++)
        {
            lRequireStars[i].text = Constant.STAR_UNLOCK_REQUIRE[i] + "";
        }

        lRequireStarsExtra.text = Constant.EXTRA_STAR_UNLOCK_REQUIRE + "";

        MessageDispatcher.AddListener(EventID.ON_CHANGE_SELECT_LEVEL_WORLD, OnChangeSelectLevelWorld, true);
        MessageDispatcher.AddListener(EventID.ON_CLAIM_STAR_CHEST_REWARD, OnClaimStarChestReward, true);
        MessageDispatcher.AddListener(EventID.ON_CLAIM_STAR_CHEST_REWARD_EXTRA, OnClaimStarChestRewardExtra, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventID.ON_CHANGE_SELECT_LEVEL_WORLD, OnChangeSelectLevelWorld, true);
        MessageDispatcher.RemoveListener(EventID.ON_CLAIM_STAR_CHEST_REWARD, OnClaimStarChestReward, true);
        MessageDispatcher.RemoveListener(EventID.ON_CLAIM_STAR_CHEST_REWARD_EXTRA, OnClaimStarChestRewardExtra, true);
    }

    void OnClaimStarChestReward(IMessage msg)
    {
        int index = (int)msg.Data;
        tweenScales[index - 1].enabled = false;
        tweenScales[index - 1].transform.localScale = Vector3.one;

        if (MinhCacheGame.IsStarChestClaimed(currentWorld, difficult, index))
        {
            tickSprites[index - 1].SetActive(true);
            sChests[index - 1].spriteName = "Box_" + difficult + "_" + index + "_d";
        }
        else
        {
            tickSprites[index - 1].SetActive(false);

            if (Constant.STAR_UNLOCK_REQUIRE[index - 1] > totalStarWorld)
            {
                sChests[index - 1].spriteName = "Box_" + difficult + "_" + index;
            }
            else
            {
                sChests[index - 1].spriteName = "Box_" + difficult + "_" + index + "_a";
            }
        }
    }

    void OnClaimStarChestRewardExtra(IMessage msg)
    {
        tweenScaleExtra.enabled = false;
        tweenScaleExtra.transform.localEulerAngles = Vector3.one;

        if (MinhCacheGame.IsStarChestExtraClaimed(currentWorld, difficult))
        {
            tickSpriteExtra.SetActive(true);
            sChestExtra.spriteName = "Box_Extra_d";
        }
        else
        {
            tickSpriteExtra.SetActive(false);

            if (Constant.EXTRA_STAR_UNLOCK_REQUIRE > totalStarExtra)
            {
                sChestExtra.spriteName = "Box_Extra";
            }
            else
            {
                sChestExtra.spriteName = "Box_Extra_a";
            }
        }


    }

    void OnChangeSelectLevelWorld(IMessage msg)
    {
        currentWorld = (int)msg.Data;
        difficult = CacheGame.GetDifficultCampaign();
        minLevelWorld = currentWorld * 7 - 6;
        maxLevelWorld = currentWorld * 7;

        //Debug.LogError("OnChangeSelectLevelWorld: " + currentWorld + " * " + minLevelWorld + " * " + maxLevelWorld);
        lLevelNumber.text = minLevelWorld + "-" + maxLevelWorld;

        //Tính tổng số sao trong world hiện tại
        totalStarWorld = 0;

        for (int i = minLevelWorld; i <= maxLevelWorld; i++)
        {
            for (int j = 1; j <= 3; j++)
            {
                totalStarWorld += CacheGame.GetNumStarLevel(i, j, difficult, GameContext.LevelCampaignMode.Normal);
            }
        }

        lTotalStars.text = totalStarWorld.ToString();

        //Tính tổng số sao extra trong world hiện tại
        totalStarExtra = 0;

        for (int i = minLevelWorld; i <= maxLevelWorld; i++)
        {
            for (int j = 1; j <= 3; j++)
            {
                totalStarExtra += CacheGame.GetNumStarLevel(i, j, difficult, GameContext.LevelCampaignMode.Extra);
            }
        }

        lTotalStarsExtra.text = totalStarExtra.ToString();

        //Đổi màu sao ở các chest
        string spriteName = "";

        if (difficult == GameContext.DIFFICULT_NOMAL)
        {
            spriteName = SpriteNameConst.NORMAL_STAR_SPRITE;
        }
        else if (difficult == GameContext.DIFFICULT_HARD)
        {
            spriteName = SpriteNameConst.HARD_STAR_SPRITE;
        }
        else
        {
            spriteName = SpriteNameConst.HELL_STAR_SPRITE;
        }

        for (int i = 0; i < sStars.Length; i++)
        {
            sStars[i].spriteName = spriteName;
        }

        //Đổi ảnh của chest
        for (int i = 1; i <= 3; i++)
        {
            tweenScales[i - 1].enabled = false;
            tweenScales[i - 1].transform.localScale = Vector3.one;

            //Debug.LogError(currentWorld + " * " + difficult + " * " + i + " * " + MinhCacheGame.IsStarChestClaimed(currentWorld, difficult, i));
            if (MinhCacheGame.IsStarChestClaimed(currentWorld, difficult, i))
            {
                tickSprites[i - 1].SetActive(true);
                sChests[i - 1].spriteName = "Box_" + difficult + "_" + i + "_d";
            }
            else
            {
                tickSprites[i - 1].SetActive(false);

                if (Constant.STAR_UNLOCK_REQUIRE[i - 1] > totalStarWorld)
                {
                    sChests[i - 1].spriteName = "Box_" + difficult + "_" + i;
                }
                else
                {
                    sChests[i - 1].spriteName = "Box_" + difficult + "_" + i + "_a";
                    tweenScales[i - 1].enabled = true;
                }
            }
        }

        //Đổi ảnh của chest extra:
        tweenScaleExtra.enabled = false;
        tweenScaleExtra.transform.localEulerAngles = Vector3.one;

        if (MinhCacheGame.IsStarChestExtraClaimed(currentWorld, difficult))
        {
            tickSpriteExtra.SetActive(true);
            sChestExtra.spriteName = "Box_Extra_d";
        }
        else
        {
            tickSpriteExtra.SetActive(false);

            if (Constant.EXTRA_STAR_UNLOCK_REQUIRE > totalStarExtra)
            {
                sChestExtra.spriteName = "Box_Extra";
            }
            else
            {
                sChestExtra.spriteName = "Box_Extra_a";
                tweenScaleExtra.enabled = true;
            }
        }
    }

    public void OnClickBtnChest1()
    {
        PopupManagerCuong.Instance.ShowStarChestPopup(currentWorld, 1, totalStarWorld, sStars[0].spriteName, sChests[0].spriteName);
    }

    public void OnClickBtnChest2()
    {
        PopupManagerCuong.Instance.ShowStarChestPopup(currentWorld, 2, totalStarWorld, sStars[0].spriteName, sChests[1].spriteName);
    }

    public void OnClickBtnChest3()
    {
        PopupManagerCuong.Instance.ShowStarChestPopup(currentWorld, 3, totalStarWorld, sStars[0].spriteName, sChests[2].spriteName);
    }

    public void OnClickBtnChestExtra()
    {
        PopupManagerCuong.Instance.ShowStarChestPopupExtra(currentWorld, totalStarExtra, "icon_viostar_ingame", sChestExtra.spriteName);
    }
}
