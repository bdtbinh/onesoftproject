﻿using UnityEngine;

public abstract class ParallaxObject : MonoBehaviour{

    public abstract void DoParalax(Vector2 offset);
}
