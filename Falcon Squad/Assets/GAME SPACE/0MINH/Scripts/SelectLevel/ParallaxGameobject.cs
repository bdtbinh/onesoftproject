﻿using UnityEngine;

public class ParallaxGameobject : ParallaxObject
{
    [SerializeField]
    private float speed;

    public override void DoParalax(Vector2 offset)
    {
        transform.position += new Vector3(offset.x, offset.y, 0) * speed;
    }
}
