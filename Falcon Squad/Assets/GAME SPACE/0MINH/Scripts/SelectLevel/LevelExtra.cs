﻿using TCore;
using UnityEngine;

public class LevelExtra : MonoBehaviour
{
    [SerializeField]
    private UISprite sIconAircraft;

    [SerializeField]
    private GameObject sLock;

    [SerializeField]
    private UISprite[] sStars;

    private const string ICON_STAR_ENABLE = "icon_viostar_ingame";
    private const string ICON_STAR_DISABLE = "icon_blackstar_ingame";

    private int level;

    public void SetLevel(int level)
    {
        this.level = level;

        if (CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) < level)
        {
            sLock.SetActive(true);
            sIconAircraft.gameObject.SetActive(false);

            for (int i = 0; i < sStars.Length; i++)
            {
                sStars[i].gameObject.SetActive(false);
            }
        }
        else
        {
            sIconAircraft.gameObject.SetActive(true);
            sLock.SetActive(false);

            int aircraftIndex = ExtraLevelRequirementSheet.Get(level).aircraft_index;
            Rank aircraftRank = (Rank)ExtraLevelRequirementSheet.Get(level).rank;

            sIconAircraft.spriteName = HangarValue.AircraftIconSpriteName((AircraftTypeEnum)aircraftIndex, aircraftRank);

            Vector2 localScale = sIconAircraft.transform.localScale;
            sIconAircraft.MakePixelPerfect();
            sIconAircraft.transform.localScale = localScale;

            for (int i = 1; i <= 3; i++)
            {
                if (CacheGame.GetNumStarLevel(level, i, CacheGame.GetDifficultCampaign(), GameContext.LevelCampaignMode.Extra) == 1)
                {
                    sStars[i - 1].spriteName = ICON_STAR_ENABLE;
                }
                else
                {
                    sStars[i - 1].spriteName = ICON_STAR_DISABLE;
                }
                sStars[i - 1].gameObject.SetActive(true);
            }
        }


    }

    public void OnClickLevelExtra()
    {
        SoundManager.PlayClickButton();

        if (sLock.activeInHierarchy)
        {
            return;
        }

        CacheGame.SetCurrentLevel(level);
        GameContext.modeGamePlay = GameContext.ModeGamePlay.Campaign;
        GameContext.levelCampaignMode = GameContext.LevelCampaignMode.Extra;

        PopupManagerCuong.Instance.ShowSelectLevelPopup();
    }
}
