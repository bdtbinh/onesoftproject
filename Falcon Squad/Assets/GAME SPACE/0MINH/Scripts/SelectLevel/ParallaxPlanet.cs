﻿using UnityEngine;

public class ParallaxPlanet : MonoBehaviour{

    [SerializeField]
    private float speed = 1f;

    [SerializeField]
    private Transform targetResetPosition;

	public void ResetPosition ()
    {
        transform.position = targetResetPosition.position;
	}
	
	public void DoParallaxUp(Vector2 offset, float offsetPointY, float distance)
    {
        transform.localPosition += new Vector3(offset.x, offset.y, 0) * speed;

        if (transform.localPosition.y < offsetPointY)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + distance, transform.localPosition.z);
        }
    }

    public void DoParallaxDown(Vector2 offset, float offsetPointY, float distance)
    {
        transform.localPosition += new Vector3(offset.x, offset.y, 0) * speed;

        if (transform.localPosition.y > offsetPointY)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - distance, transform.localPosition.z);
        }
    }
}
