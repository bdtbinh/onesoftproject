﻿using UnityEngine;
using System.Collections.Generic;

public class LevelNode : MonoBehaviour
{
    private static Color HARD_COLOR_LINE = new Color(228 / 255f, 144 / 255f, 0f);
    private static Color NORMAL_COLOR_LINE = new Color(28 / 225f, 127 / 225f, 144 / 225f);

    private LineRenderer lineRenderer;
    private UISprite bgNode;
    private LevelItemObj levelObject;
    private List<Transform> listCloud;

    public LevelPosManager levelPosManager;

    public int Level { get { return levelObject.numLevel; } }

    public int LevelTest { get { return level; } }

    public LevelExtra levelExtra;

    private int level;

    private void Awake()
    {
        bgNode = MinhUtils.FindChild(transform, "sIconLevel").GetComponent<UISprite>();
        lineRenderer = transform.GetComponentInChildren<LineRenderer>();
        listCloud = MinhUtils.FindChildren(transform, "Cloud");
    }

    public void ReloadData()
    {
        levelObject.ReloadData(level);
    }

    public void SetLevel(int level)
    {
        this.level = level;
        levelObject.SetLevel(level);

        switch (CacheGame.GetDifficultCampaign())
        {
            case "Normal":
                //bgNode.sprite2D = SelectLevelScene.Instance.spriteItemNomal;
                bgNode.spriteName = "level_blue";
                lineRenderer.startColor = NORMAL_COLOR_LINE;
                lineRenderer.endColor = NORMAL_COLOR_LINE;

                if (levelExtra != null)
                {
                    levelExtra.SetLevel(level);
                }

                for (int i = 0; i < listCloud.Count; i++)
                {
                    listCloud[i].GetComponent<SpriteRenderer>().color = Constant.NORMAL_COLOR_CLOUD;
                }

                break;
            case "Hard":
                //bgNode.sprite2D = SelectLevelScene.Instance.spriteItemHard;
                bgNode.spriteName = "level_yellow";
                lineRenderer.startColor = HARD_COLOR_LINE;
                lineRenderer.endColor = HARD_COLOR_LINE;

                if (levelExtra != null)
                {
                    levelExtra.SetLevel(level);
                }

                for (int i = 0; i < listCloud.Count; i++)
                {
                    listCloud[i].GetComponent<SpriteRenderer>().color = Constant.HARD_COLOR_CLOUD;
                }

                break;
            case "Hell":
                //bgNode.sprite2D = SelectLevelScene.Instance.spriteItemHell;
                bgNode.spriteName = "level_red";
                lineRenderer.startColor = Color.red;
                lineRenderer.endColor = Color.red;

                if (levelExtra != null)
                {
                    levelExtra.SetLevel(level);
                }

                for (int i = 0; i < listCloud.Count; i++)
                {
                    listCloud[i].GetComponent<SpriteRenderer>().color = Constant.HELL_COLOR_CLOUD;
                }

                break;
            default:
                break;
        }
    }

    public void SetLevelItemObject(LevelItemObj levelItemObj)
    {
        levelObject = levelItemObj;
    }
}
