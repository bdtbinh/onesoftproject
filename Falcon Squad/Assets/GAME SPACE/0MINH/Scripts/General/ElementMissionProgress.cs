﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;

public class ElementMissionProgress : MonoBehaviour {

    [SerializeField]
    private UILabel lProgress;

    [SerializeField]
    private GameObject progressBar;

    public void SetProgressValues(int currentValue, int targetValue)
    {
        lProgress.text = ScriptLocalization.up_to_number.Replace("%{number}", targetValue + "(" + currentValue + ")");
        progressBar.transform.localScale = new Vector3(currentValue * 1.0f / targetValue, 1f, 1f);
    }
}
