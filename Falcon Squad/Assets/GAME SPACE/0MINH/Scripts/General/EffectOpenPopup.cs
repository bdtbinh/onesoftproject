﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using System;

public class EffectOpenPopup : MonoBehaviour
{
    //[SerializeField]
    //private float waitingTime = 0.1f;

    [SerializeField]
    [ShowIf("IsMoveListItem")]
    private float betweenTime = 0.05f;

    [SerializeField]
    [ShowIf("IsMoveListItem")]
    private GameObject[] listItems;

    private bool IsMoveListItem()
    {
        return effectType == EffectType.MoveListItemLeft;
    }

    [SerializeField]
    private AnimationCurve curve;

    public float actionTime = 0.36f;

    private Vector3 orginalScale;

    private bool isStarted;

    public enum EffectType
    {
        Scale,
        MoveUp,
        MoveLeft,
        MoveListItemLeft,
        MoveRight,
    }

    public EffectType effectType = EffectType.Scale;

    private void Awake()
    {
        orginalScale = transform.localScale;
    }

    private void Start()
    {
        PlayTween();
        isStarted = true;
    }

    private void OnEnable()
    {
        if (isStarted)
            PlayTween();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void PlayTween()
    {
        //orginalScale = transform.localScale;
        //gameObject.transform.localScale = Vector3.zero;

        ResetStateBeforeTween();

        switch (effectType)
        {
            case EffectType.Scale:
                StartCoroutine(WaitForPlayTween(DoScale));
                //DoScale();
                break;

            case EffectType.MoveUp:
                StartCoroutine(WaitForPlayTween(DoMoveUp));
                //DoMoveUp();
                break;

            case EffectType.MoveLeft:
                StartCoroutine(WaitForPlayTween(DoMoveLeft));
                //DoMoveLeft();
                break;

            case EffectType.MoveListItemLeft:
                StartCoroutine(WaitForPlayTween(DoMoveListItemLeft));
                //DoMoveListItemLeft();
                break;
            case EffectType.MoveRight:
                StartCoroutine(WaitForPlayTween(DoMoveRight));
                //DoMoveRight();
                break;
            default:
                break;
        }
    }

    IEnumerator WaitForPlayTween(Action action)
    {
        //yield return new WaitForSecondsRealtime(waitingTime);
        yield return null;
        action();
    }

    void ResetStateBeforeTween()
    {
        switch (effectType)
        {
            case EffectType.Scale:
                transform.localScale = Vector3.zero;
                break;
            case EffectType.MoveUp:
                transform.localPosition = new Vector3(transform.localScale.x, -Screen.height, 0);
                break;
            case EffectType.MoveLeft:
                transform.localPosition = new Vector3(-Screen.width, transform.localPosition.y, 0);
                break;
            case EffectType.MoveListItemLeft:
                for (int i = 0; i < listItems.Length; i++)
                {
                    listItems[i].transform.localPosition = new Vector3(-Screen.width, listItems[i].transform.localPosition.y, 0);
                    Debug.LogError(listItems[i].name);
                }
                break;
            case EffectType.MoveRight:
                transform.localPosition = new Vector3(Screen.width, transform.localPosition.y, 0);
                break;
        }
    }
    void DoScale()
    {
        transform.DOScale(orginalScale, actionTime).SetEase(curve);
    }

    void DoMoveLeft()
    {
        transform.DOLocalMove(new Vector3(0, transform.localPosition.y, 0), actionTime, true).SetEase(curve);
    }

    void DoMoveUp()
    {
        transform.DOLocalMove(new Vector3(transform.localPosition.x, 0, 0), actionTime, true).SetEase(curve);
    }

    void DoMoveRight()
    {
        transform.DOLocalMove(new Vector3(0, transform.localPosition.y, 0), actionTime, true).SetEase(curve);
    }

    void DoMoveListItemLeft()
    {
        for (int i = 0; i < listItems.Length; i++)
        {
            if (listItems[i].activeSelf)
            {
                Debug.LogError(listItems[i].name);
                listItems[i].transform.DOLocalMoveX(0, actionTime, true).SetEase(curve).SetDelay(i * betweenTime);
            }
        }
    }

    [Button]
    public void DoEffect()
    {
        PlayTween();
    }
}
