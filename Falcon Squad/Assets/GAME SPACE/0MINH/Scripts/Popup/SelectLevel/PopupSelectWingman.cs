﻿using UnityEngine;
using System.Collections.Generic;
using com.ootii.Messages;
using System;
using I2.Loc;
using System.Collections;
using TCore;
using EasyMobile;
using UnityEngine.SceneManagement;
using Firebase.Analytics;
using UnityEngine.Purchasing.Security;
using Sirenix.OdinInspector;
#if EM_UIAP
using UnityEngine.Purchasing;
#endif

public enum WingmanPosType
{
    LeftWingman,
    RightWingman,
    None
}

public class PopupSelectWingman : MonoBehaviour
{
    private const float BUTTON_GOLD_LEFT_X = -200f;
    private const float BUTTON_GEM_RIGHT_X = 200f;

    private const string GOLD_SPRITE_NAME = "Gold_small";
    private const string GEM_SPRITE_NAME = "Gem_Green_small";

    private const string BUTTON_UPGRADE_DISABLE_SPRITE = "btn_upgrade_item";
    private const string BUTTON_UPGRADE_ENABLE_SPRITE = "btn_upgrade_item_t";

    private static WingmanPosType wingmanPosType = WingmanPosType.LeftWingman;

    public static SelectDeviceHandler.OpenSelectFromType openSelectFromType;

    [SerializeField]
    private PanelCongratulations panelCongratulations;

    [SerializeField]
    private PanelEvolve panelEvolve;

    [SerializeField]
    private UISprite sTitleBG;

    [SerializeField]
    private UISprite sPosition;

    [SerializeField]
    private UILabel lWingmanName;

    [SerializeField]
    private UISprite sRankTitle;

    [SerializeField]
    private UILabel lDescription;

    [SerializeField]
    private WingmanAnimations wingmanAnimations;

    //Progress bar region
    [TitleGroup("Progress")]
    [SerializeField]
    private UIButton btnCard;

    [SerializeField]
    private UISprite sGreenProgress;

    [SerializeField]
    private UISprite sBlueProgress;

    [SerializeField]
    private UILabel lCardNumber;

    [SerializeField]
    private GameObject btnEvolve;

    //Button buy, select:
    [TitleGroup("Unlock or Buy")]
    [SerializeField]
    private UILabel lUnlockMessage;

    [SerializeField]
    private GameObject btnUnlock;

    [SerializeField]
    private GameObject btnBuyGold;

    [SerializeField]
    private UILabel lGoldPrice;

    [SerializeField]
    private GameObject btnBuyGem;

    [SerializeField]
    private UILabel lGemPrice;

    [SerializeField]
    private GameObject btnPurchase;

    [SerializeField]
    private UILabel lPurchasePrice;

    //Upgradation
    [TitleGroup("Upgradations")]
    [SerializeField]
    private UILabel lLevel;

    [SerializeField]
    private GameObject[] levelBars;

    [SerializeField]
    private UIButton btnUpgrade;

    [SerializeField]
    private UILabel lUpgradeNumber;

    [SerializeField]
    private UISprite sUpgradeIcon;

    [TitleGroup("List wingman items")]
    [SerializeField]
    private WingmanItem[] wingmanItems;

    [TitleGroup("Equip, Unequip, Try buttons")]
    [SerializeField]
    private GameObject btnEquip;

    [SerializeField]
    private GameObject btnUnequip;

    [SerializeField]
    private GameObject btnTry;

    [SerializeField]
    private GameObject sEvolveRedDot;           //Show khi có thể elvolve được wingman

    [SerializeField]
    private GameObject sUpgradeRedDot;          //Show khi có thể upgrade được wingman

    private Vector3[] itemLocalPositions;

    //Item được select (k phải item được chọn mang vào chơi)
    private WingmanItem focusItem;

    //Item mang vào chơi
    private WingmanItem leftSelectedItem;
    private WingmanItem rightSelectedItem;

    //Cached fields
    private int currentLevel;
    private int maxLevel;
    private int upgradeGoldCost;
    private int upgradeGemCost;
    private int specificCards;
    private int generalCards;
    private int evolveCards;
    private int unlockCards;

    private bool needToLoadFromTry;
    public bool NeedToLoadFromTry
    {
        set { needToLoadFromTry = value; }
    }

    private void Awake()
    {
        itemLocalPositions = new Vector3[wingmanItems.Length];

        for (int i = 0; i < itemLocalPositions.Length; i++)
        {
            itemLocalPositions[i] = wingmanItems[i].transform.localPosition;
        }
    }

    ////OnEnable có trường hợp chạy trước awake của object HangarSelectItem nên phải thêm biến này để lần đầu tiên sẽ chạy UpdateItemStatus trong start. 
    ////Tránh trường hợp có biến của HangarSelectItem chưa được khởi tạo mà đã truy cập.
    bool isStarted;
    private void Start()
    {
        isStarted = true;
        InitItemsStatus();
    }

    private void OnRewardSuccessed(IMessage msg)
    {
        UpdateUnlockUI();
    }

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventID.ON_REWARD_SUCCESSED, OnRewardSuccessed, true);
        MessageDispatcher.AddListener(EventID.HANGAR_ITEM_CHANGED, OnClickItem, true);
        MessageDispatcher.AddListener(EventID.ON_EVOLVE_SUCCESSED, OnEvolveSuccessed, true);

        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;

        if (!isStarted)
            return;

        InitItemsStatus();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_REWARD_SUCCESSED, OnRewardSuccessed, true);
        MessageDispatcher.RemoveListener(EventID.HANGAR_ITEM_CHANGED, OnClickItem, true);
        MessageDispatcher.RemoveListener(EventID.ON_EVOLVE_SUCCESSED, OnEvolveSuccessed, true);

        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;

        MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI, 0f);

        panelEvolve.gameObject.SetActive(false);
    }

    private void OnEvolveSuccessed(IMessage msg)
    {
        focusItem.SetRank(CacheGame.GetWingmanRank(focusItem.WingmanType));
        UpdateInfo();
        UpdateUnlockUI();
    }

    void OnPurchaseSuccessed(string productName)
    {
        if (productName == EM_IAPConstants.Product_unlock_drone_2 || productName == EM_IAPConstants.Product_unlock_drone_3 || productName == EM_IAPConstants.Product_unlock_drone_4
                 || productName == EM_IAPConstants.Product_unlock_drone_5 || productName == EM_IAPConstants.Product_unlock_drone_6)
        {
            CacheGame.SetWingManIsUnlocked((int)focusItem.WingmanType, 1);

            if (wingmanPosType == WingmanPosType.LeftWingman)
                CacheGame.SetWingManLeftUserSelected((int)focusItem.WingmanType);
            else
                CacheGame.SetWingManRightUserSelected((int)focusItem.WingmanType);

            ChangeFocusItemToSelected();
            UpdateUnlockUI();
            UpdateButtomButtons();

            PopupManagerCuong.Instance.ShowTextNotifiToast(HangarValue.GetUnlockDeviceNotify(HangarValue.WingmanNames(focusItem.WingmanType)), true, 1.88f, 0.56f);
        }
    }

    List<WingmanItem> ordinalPosList = new List<WingmanItem>();

    public void ShowPopUp(WingmanPosType type, SelectDeviceHandler.OpenSelectFromType fromType, bool needToLoadFromTry)
    {
        if (type != WingmanPosType.None)
            wingmanPosType = type;

        openSelectFromType = fromType;
        this.needToLoadFromTry = needToLoadFromTry;

        //lTitle.text = type == WingmanPosType.LeftWingman ? "L.Drone" : "R.Drone";
        sPosition.spriteName = wingmanPosType == WingmanPosType.LeftWingman ? "Item_laber_L" : "Item_laber_R";
        sTitleBG.spriteName = wingmanPosType == WingmanPosType.LeftWingman ? "Popup_titler_blue" : "Popup_titler_red";
        gameObject.SetActive(true);
    }

    private void InitItemsStatus()
    {
        focusItem = null;
        leftSelectedItem = null;
        rightSelectedItem = null;

        //Set item left right lên đầu nếu được chọn:
        if (CacheGame.GetWingManLeftUserSelected() > 0)
        {
            leftSelectedItem = wingmanItems[CacheGame.GetWingManLeftUserSelected() - 1];
            ordinalPosList.Add(leftSelectedItem);

            leftSelectedItem.SetAsSelected(WingmanPosType.LeftWingman);

            if (wingmanPosType == WingmanPosType.LeftWingman)
            {
                focusItem = leftSelectedItem;
                focusItem.ShowFocusEffect();
            }
        }

        if (CacheGame.GetWingManRightUserSelected() > 0)
        {
            rightSelectedItem = wingmanItems[CacheGame.GetWingManRightUserSelected() - 1];
            ordinalPosList.Add(rightSelectedItem);

            rightSelectedItem.SetAsSelected(WingmanPosType.RightWingman);

            if (wingmanPosType == WingmanPosType.RightWingman)
            {
                focusItem = rightSelectedItem;
                focusItem.ShowFocusEffect();
            }
        }


        for (int i = 0; i < wingmanItems.Length; i++)
        {
            if (wingmanItems[i] == leftSelectedItem || wingmanItems[i] == rightSelectedItem)
                continue;

            if (CacheGame.GetWingManIsUnlocked((int)wingmanItems[i].WingmanType) == 1)        //Item đã unlock
            {
                wingmanItems[i].SetAsNormal();
                ordinalPosList.Add(wingmanItems[i]);
            }
        }

        //Item chưa unlock
        for (int i = 0; i < wingmanItems.Length; i++)
        {
            if (CacheGame.GetWingManIsUnlocked((int)wingmanItems[i].WingmanType) != 1)
            {
                wingmanItems[i].SetAsLocked();
                if (wingmanItems[i].gameObject.activeInHierarchy)
                {
                    ordinalPosList.Add(wingmanItems[i]);
                }
            }
        }

        if (needToLoadFromTry)
        {
            if (focusItem != null)
                focusItem.HideFocusEffect();

            focusItem = wingmanItems[CacheGame.GetWingManUserTry() - 1];
            focusItem.ShowFocusEffect();

            needToLoadFromTry = false;
        }

        for (int i = 0; i < ordinalPosList.Count; i++)
        {
            ordinalPosList[i].transform.localPosition = itemLocalPositions[i];
        }

        if (focusItem == null)
        {
            if (wingmanPosType == WingmanPosType.LeftWingman)
            {
                for (int i = 0; i < wingmanItems.Length; i++)
                {
                    if (rightSelectedItem != wingmanItems[i])
                    {
                        focusItem = wingmanItems[i];
                        break;
                    }
                }
            }
            else if (wingmanPosType == WingmanPosType.RightWingman)
            {
                for (int i = 0; i < wingmanItems.Length; i++)
                {
                    if (leftSelectedItem != wingmanItems[i])
                    {
                        focusItem = wingmanItems[i];
                        break;
                    }
                }
            }
            else
            {
                focusItem = wingmanItems[0];
            }

            focusItem.ShowFocusEffect();
        }

        ordinalPosList.Clear();
        UpdateInfo();
        UpdateUnlockUI();
        UpdateButtomButtons();
    }

    #region Update UI
    private void UpdateInfo()
    {
        lPurchasePrice.text = CuongUtils.GetIapLocalizedPrice(WingmanSheet.Get((int)focusItem.WingmanType).purchase_id);

        //lWingmanName.gameObject.SetActive(true);
        //sRankTitle.gameObject.SetActive(true);
        //lDescription.gameObject.SetActive(true);
        lWingmanName.text = HangarValue.WingmanNames(focusItem.WingmanType);
        sRankTitle.spriteName = HangarValue.RankSpriteName[focusItem.Rank];
        lDescription.text = HangarValue.WingmanDescriptions(focusItem.WingmanType);

        wingmanAnimations.PlayAnimations(focusItem.WingmanType, focusItem.Rank);
    }

    private void UpdateUnlockUI()
    {
        bool isUnlocked = CacheGame.GetWingManIsUnlocked((int)focusItem.WingmanType) == 1;

        //btnCard.transform.parent.gameObject.SetActive(false);
        btnEvolve.SetActive(false);

        lUnlockMessage.gameObject.SetActive(false);
        btnUnlock.SetActive(false);
        btnBuyGold.SetActive(false);
        btnBuyGem.SetActive(false);

        btnUpgrade.transform.parent.gameObject.SetActive(false);

        specificCards = MinhCacheGame.GetWingmanCards(focusItem.WingmanType);
        generalCards = MinhCacheGame.GetWingmanGeneralCards();

        if (isUnlocked)
        {
            btnCard.normalSprite = "card_0";
            bool isMaxRank = (int)focusItem.Rank >= (int)Constant.MAX_RANK_APPLY;

            if (isMaxRank)
            {
                lCardNumber.text = lCardNumber.text = ScriptLocalization.label_max;
                sBlueProgress.fillAmount = 1f;
            }
            else
            {
                //Load card data
                evolveCards = RankSheet.Get((int)focusItem.Rank + 1).card_evolve;

                int totalCards = specificCards + generalCards;
                lCardNumber.text = totalCards + "/" + evolveCards;

                sBlueProgress.fillAmount = specificCards * 1.0f / evolveCards;
                sGreenProgress.fillAmount = totalCards * 1.0f / evolveCards;
                btnEvolve.SetActive(true);         //Max rank rồi thì tắt nút evolve đi
                CheckShowEvolveRedDot();
            }

            btnUpgrade.transform.parent.gameObject.SetActive(true);
            //sIconUpgrade.spriteName = "hangar_drone_" + (int)focusItem.WingmanType;

            currentLevel = CacheGame.GetWingmanLevel(focusItem.WingmanType);
            maxLevel = RankSheet.Get((int)focusItem.Rank).max_level;

            lLevel.text = I2.Loc.ScriptLocalization.level_upgrade + ": " + currentLevel + "/" + maxLevel;
            SetUpgradeBars(CacheGame.GetWingmanLevel(focusItem.WingmanType));
            SetUpgradePrice();
        }
        else
        {
            sEvolveRedDot.SetActive(false);
            int levelUnlock = WingmanSheet.Get((int)focusItem.WingmanType).level_unlock;
            unlockCards = WingmanSheet.Get((int)focusItem.WingmanType).card_unlock;

            if (unlockCards > 0)
            {
                btnCard.normalSprite = "card_D" + (int)focusItem.WingmanType;
                lCardNumber.text = specificCards + "/" + unlockCards;

                sGreenProgress.fillAmount = 0f;

                if (MinhCacheGame.GetWingmanCards(focusItem.WingmanType) < unlockCards)
                {
                    lUnlockMessage.gameObject.SetActive(true);
                    lUnlockMessage.text = I2.Loc.ScriptLocalization.msg_unlock_by_card;

                    sBlueProgress.fillAmount = MinhCacheGame.GetWingmanCards(focusItem.WingmanType) * 1.0f / unlockCards;
                }
                else
                {
                    sBlueProgress.fillAmount = 1f;
                    btnUnlock.SetActive(true);
                }
            }
            else if (levelUnlock > 0)
            {
                btnCard.normalSprite = "card_0";

                bool isMaxRank = (int)focusItem.Rank >= (int)Constant.MAX_RANK_APPLY;

                if (isMaxRank)
                {
                    lCardNumber.text = lCardNumber.text = ScriptLocalization.label_max;
                    sBlueProgress.fillAmount = 1f;
                }
                else
                {
                    //Load card data
                    evolveCards = RankSheet.Get((int)focusItem.Rank + 1).card_evolve;

                    int totalCards = specificCards + generalCards;
                    lCardNumber.text = totalCards + "/" + evolveCards;

                    sBlueProgress.fillAmount = specificCards * 1.0f / evolveCards;
                    sGreenProgress.fillAmount = totalCards * 1.0f / evolveCards;
                    btnEvolve.SetActive(true);         //Max rank rồi thì tắt nút evolve đi
                }

                if (CacheGame.GetMaxLevel3Difficult() < levelUnlock)
                {
                    lUnlockMessage.gameObject.SetActive(true);
                    string msg = ScriptLocalization.msg_unlock_level_to_purchase;
                    lUnlockMessage.text = msg.Replace("%{level_number}", levelUnlock.ToString());
                }
                else
                {
                    btnBuyGold.transform.parent.gameObject.SetActive(true);

                    int goldUnlock = WingmanSheet.Get((int)focusItem.WingmanType).gold_unlock;
                    int gemUnlock = WingmanSheet.Get((int)focusItem.WingmanType).gem_unlock;

                    lGoldPrice.text = GameContext.FormatNumber(goldUnlock);
                    lGemPrice.text = GameContext.FormatNumber(gemUnlock);

                    if (goldUnlock > 0 && gemUnlock > 0)
                    {
                        btnBuyGold.SetActive(true);
                        btnBuyGem.SetActive(true);

                        btnBuyGold.transform.localPosition = new Vector3(BUTTON_GOLD_LEFT_X, btnBuyGold.transform.localPosition.y, btnBuyGold.transform.localPosition.z);
                        btnBuyGem.transform.localPosition = new Vector3(BUTTON_GEM_RIGHT_X, btnBuyGem.transform.localPosition.y, btnBuyGem.transform.localPosition.z);
                    }
                    else if (goldUnlock > 0)
                    {
                        btnBuyGold.SetActive(true);
                        btnBuyGold.transform.localPosition = new Vector3(0, btnBuyGold.transform.localPosition.y, btnBuyGold.transform.localPosition.z);
                    }
                    else
                    {
                        btnBuyGem.SetActive(true);
                        btnBuyGem.transform.localPosition = new Vector3(0, btnBuyGem.transform.localPosition.y, btnBuyGem.transform.localPosition.z);
                    }
                }
            }
        }
    }

    private void UpdateButtomButtons()
    {
        btnEquip.SetActive(false);
        btnUnequip.SetActive(false);
        btnTry.SetActive(false);
        btnPurchase.SetActive(false);

        bool isUnlocked = CacheGame.GetWingManIsUnlocked((int)focusItem.WingmanType) == 1;

        if (isUnlocked)
        {
            if (wingmanPosType == WingmanPosType.LeftWingman)
            {
                if ((int)focusItem.WingmanType != CacheGame.GetWingManLeftUserSelected())
                {
                    btnEquip.SetActive(true);
                }
                else
                {
                    btnUnequip.SetActive(true);
                }
            }
            else
            {
                if ((int)focusItem.WingmanType != CacheGame.GetWingManRightUserSelected())
                {
                    btnEquip.SetActive(true);
                }
                else
                {
                    btnUnequip.SetActive(true);
                }
            }
        }
        else
        {
            btnTry.SetActive(true);

            if (!string.IsNullOrEmpty(WingmanSheet.Get((int)focusItem.WingmanType).purchase_id))
            {
                btnPurchase.SetActive(true);
            }
        }

    }

    #endregion

    private void SetUpgradeBars(int level)
    {
        int remainder = level % levelBars.Length;

        if (level == 0)
        {
            remainder = 0;
        }
        else
        {
            if (remainder == 0)
                remainder = 15;
        }

        for (int i = 0; i < levelBars.Length; i++)
        {
            levelBars[i].SetActive(i < remainder);
        }
    }

    private void SetUpgradePrice()
    {
        if (currentLevel >= RankSheet.Get((int)Constant.MAX_RANK_APPLY).max_level)
        {
            btnUpgrade.gameObject.SetActive(false);
        }
        else
        {
            btnUpgrade.gameObject.SetActive(true);

            upgradeGoldCost = WingMUpgradeCostSheet.Get(currentLevel + 1).gold;
            upgradeGemCost = WingMUpgradeCostSheet.Get(currentLevel + 1).gem;

            if (upgradeGoldCost > 0)
            {
                lUpgradeNumber.text = upgradeGoldCost.ToString();
                sUpgradeIcon.spriteName = GOLD_SPRITE_NAME;
            }
            else
            {
                lUpgradeNumber.text = upgradeGemCost.ToString();
                sUpgradeIcon.spriteName = GEM_SPRITE_NAME;
            }

            if (currentLevel < maxLevel)
            {
                btnUpgrade.normalSprite = BUTTON_UPGRADE_ENABLE_SPRITE;
            }
            else
            {
                btnUpgrade.normalSprite = BUTTON_UPGRADE_DISABLE_SPRITE;
                //btnUpgrade.gameObject.SetActive(false);                         //Bỏ dòng này đi sau khi mở full chức năng cho wingman.
            }

            CheckShowUpgradeRedDot();
        }
    }

    private void IncreaseLevel(ResourceType type)
    {
        currentLevel++;
        CacheGame.SetWingmanLevel(focusItem.WingmanType, currentLevel);
        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetUpgrade.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetUpgrade.ToString()) + 1);

        if (type == ResourceType.Gold)
        {
            PanelCoinGem.Instance.AddCoinTop(-upgradeGoldCost, FirebaseLogSpaceWar.UpgradeDrone_why, focusItem.WingmanType.ToString());
        }

        if (type == ResourceType.Gem)
        {
            PanelCoinGem.Instance.AddGemTop(-upgradeGemCost, FirebaseLogSpaceWar.UpgradeDrone_why, focusItem.WingmanType.ToString());
        }

        lLevel.text = I2.Loc.ScriptLocalization.level_upgrade + ": " + currentLevel + "/" + maxLevel;
        SetUpgradeBars(currentLevel);
        SetUpgradePrice();

        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_shop_upgraded.Replace("%{level_number}", currentLevel.ToString()), true, 1f);
    }

    private void CheckShowEvolveRedDot()
    {
        int evolveCost = RankSheet.Get((int)focusItem.Rank + 1).gem_evolve;

        if (CacheGame.GetTotalGem() >= evolveCost && specificCards + generalCards >= evolveCards)
        {
            sEvolveRedDot.SetActive(true);
        }
        else
        {
            sEvolveRedDot.SetActive(false);
        }
    }

    private void CheckShowUpgradeRedDot()
    {
        if (currentLevel >= maxLevel) return;

        if (upgradeGoldCost > 0)
        {
            if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
            {
                sUpgradeRedDot.SetActive(true);
            }
            else
            {
                sUpgradeRedDot.SetActive(false);
            }
        }
        else
        {
            if (CacheGame.GetTotalGem() >= upgradeGemCost)
            {
                sUpgradeRedDot.SetActive(true);
            }
            else
            {
                sUpgradeRedDot.SetActive(false);
            }
        }
    }


    public void OnClickItem(IMessage msg)
    {
        SoundManager.PlayClickButton();

        if (focusItem != null)
            focusItem.HideFocusEffect();

        focusItem = (WingmanItem)msg.Data;
        focusItem.ShowFocusEffect();

        UpdateInfo();
        UpdateUnlockUI();
        UpdateButtomButtons();

    }

    public void OnClickBtnEvolve()
    {
        SoundManager.PlayClickButton();
        panelEvolve.ShowWingmanEvolvePanel(focusItem.WingmanType, (Rank)((int)focusItem.Rank + 1));
    }

    public void OnClickBtnUpgrade()
    {
        SoundManager.PlayClickButton();

        if (currentLevel < maxLevel)
        {
            if (upgradeGoldCost > 0)
            {
                if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
                {
                    IncreaseLevel(ResourceType.Gold);
                }
                else
                {
                    PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
                }
            }
            else
            {
                if (CacheGame.GetTotalGem() >= upgradeGemCost)
                {
                    IncreaseLevel(ResourceType.Gem);
                }
                else
                {
                    PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
                }
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_need_evolve_wingman, false, 1.5f);
        }
    }

    public void OnClickBtnPurchase()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(WingmanSheet.Get((int)focusItem.WingmanType).purchase_id, IAPCallbackManager.WherePurchase.shop);
    }

    public void OnClickBtnUnlock()
    {
        SoundManager.PlayClickButton();
        ChangeFocusItemToSelected();
        CacheGame.SetWingManIsUnlocked((int)focusItem.WingmanType, 1);

        if (wingmanPosType == WingmanPosType.LeftWingman)
            CacheGame.SetWingManLeftUserSelected((int)focusItem.WingmanType);
        else
            CacheGame.SetWingManRightUserSelected((int)focusItem.WingmanType);

        MinhCacheGame.SetWingmanCards(focusItem.WingmanType, MinhCacheGame.GetWingmanCards(focusItem.WingmanType) - unlockCards, FirebaseLogSpaceWar.PurchaseDrone_why, focusItem.WingmanType.ToString(), focusItem.WingmanType.ToString());

        UpdateUnlockUI();
        UpdateButtomButtons();

        panelCongratulations.ShowPanelWingman(focusItem.WingmanType, focusItem.Rank, PanelCongratulations.CongraType.NewItem);
    }

    public void OnClickBuyGem()
    {
        SoundManager.PlayClickButton();
        int gemUnlock = WingmanSheet.Get((int)focusItem.WingmanType).gem_unlock;

        if (CacheGame.GetTotalGem() >= gemUnlock)
        {
            ChangeFocusItemToSelected();

            //DontDestroyManager.Instance.AddCoinTop(-HangarValue.WingmanGoldPrice[focusItem.WingmanType]);
            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.AddGemTop(-gemUnlock, FirebaseLogSpaceWar.PurchaseDrone_why, focusItem.WingmanType.ToString());
            }

            CacheGame.SetWingManIsUnlocked((int)focusItem.WingmanType, 1);

            if (wingmanPosType == WingmanPosType.LeftWingman)
                CacheGame.SetWingManLeftUserSelected((int)focusItem.WingmanType);
            else
                CacheGame.SetWingManRightUserSelected((int)focusItem.WingmanType);

            UpdateUnlockUI();
            UpdateButtomButtons();

            panelCongratulations.ShowPanelWingman(focusItem.WingmanType, focusItem.Rank, PanelCongratulations.CongraType.NewItem);
            //PopupManagerCuong.Instance.ShowTextNotifiToast(HangarValue.WingmanUnlockNotifies[focusItem.WingmanType], true, 1.88f);
            //FirebaseAnalytics.LogEvent("Upgrade", new Parameter("ClickUpgrade", HangarValue.WingmanFirebaseUnlock[focusItem.WingmanType]));
            //FirebaseLogSpaceWar.LogBuyByGem("Wingman_" + HangarValue.WingmanFirebaseUnlock[focusItem.WingmanType]);
        }
        else
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
        }
    }

    public void OnClickBtnBuyGold()
    {
        SoundManager.PlayClickButton();
        int goldUnlock = WingmanSheet.Get((int)focusItem.WingmanType).gold_unlock;

        if (CacheGame.GetTotalCoin() >= goldUnlock)
        {
            ChangeFocusItemToSelected();

            //DontDestroyManager.Instance.AddCoinTop(-HangarValue.WingmanGoldPrice[focusItem.WingmanType]);
            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.AddCoinTop(-goldUnlock, FirebaseLogSpaceWar.PurchaseDrone_why, focusItem.WingmanType.ToString());
            }

            CacheGame.SetWingManIsUnlocked((int)focusItem.WingmanType, 1);

            if (wingmanPosType == WingmanPosType.LeftWingman)
                CacheGame.SetWingManLeftUserSelected((int)focusItem.WingmanType);
            else
                CacheGame.SetWingManRightUserSelected((int)focusItem.WingmanType);

            UpdateUnlockUI();
            UpdateButtomButtons();

            panelCongratulations.ShowPanelWingman(focusItem.WingmanType, focusItem.Rank, PanelCongratulations.CongraType.NewItem);
            //PopupManagerCuong.Instance.ShowTextNotifiToast(HangarValue.WingmanUnlockNotifies[focusItem.WingmanType], true, 1.88f);
            //FirebaseAnalytics.LogEvent("Upgrade", new Parameter("ClickUpgrade", HangarValue.WingmanFirebaseUnlock[focusItem.WingmanType]));
            //FirebaseLogSpaceWar.LogBuyByGold("Wingman_" + HangarValue.WingmanFirebaseUnlock[focusItem.WingmanType]);
        }
        else
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
        }
    }

    public void OnClickBtnTry()
    {
        //CacheGame.SetCurrentLevel(0);
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowNoticeInTrialWingmanPopup(focusItem.WingmanType, wingmanPosType);

        //CacheGame.SetSpaceShipUserTry(CacheGame.GetSpaceShipUserSelected());

        //if (wingmanPosType == WingmanPosType.LeftWingman)
        //{
        //    GameContext.typeTryWingman = GameContext.TypeTryWingman.Left;
        //}
        //else
        //{
        //    GameContext.typeTryWingman = GameContext.TypeTryWingman.Right;
        //}


        //CacheGame.SetWingManUserTry((int)focusItem.WingmanType);

        //LoadLevelTryPlane();
        //GameContext.modeGamePlay = GameContext.ModeGamePlay.TryPlane;

        //if (SceneManager.GetActiveScene().name == "SelectLevel")
        //{
        //    GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.SelectLevel;
        //}
        //else if (SceneManager.GetActiveScene().name == "Home")
        //{
        //    if (PopupManager.Instance.IsTournamentPopupActive())
        //    {
        //        GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.Tournament;
        //    }
        //    else
        //    {
        //        GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.Home;
        //    }
        //}

        //GameContext.typeOpenPopupFromTry = GameContext.TypeOpenPopupFromTry.Wingman;
    }

    void LoadLevelTryPlane()
    {
        SoundManager.PlayClickButton();

        LoadingSceneManager.Instance.LoadSceneMainUI("LevelTryPlane");
    }

    public void OnClickBtnEquip()
    {
        SoundManager.PlayClickButton();

        if (wingmanPosType == WingmanPosType.LeftWingman)
            CacheGame.SetWingManLeftUserSelected((int)focusItem.WingmanType);
        else
            CacheGame.SetWingManRightUserSelected((int)focusItem.WingmanType);

        ChangeFocusItemToSelected();

        btnEquip.SetActive(false);
        btnUnequip.SetActive(true);
    }

    public void OnClickBtnUnequip()
    {
        SoundManager.PlayClickButton();

        if (wingmanPosType == WingmanPosType.LeftWingman)
        {
            leftSelectedItem = null;
            CacheGame.SetWingManLeftUserSelected((int)WingmanTypeEnum.None);
        }
        else
        {
            rightSelectedItem = null;
            CacheGame.SetWingManRightUserSelected((int)WingmanTypeEnum.None);
        }

        focusItem.SetAsNormal();

        btnEquip.SetActive(true);
        btnUnequip.SetActive(false);
    }

    void ChangeFocusItemToSelected()
    {
        if (wingmanPosType == WingmanPosType.LeftWingman)
        {
            if (focusItem == rightSelectedItem)
            {
                rightSelectedItem = null;
                CacheGame.SetWingManRightUserSelected((int)WingmanTypeEnum.None);
            }

            if (leftSelectedItem != null)
                leftSelectedItem.SetAsNormal();

            leftSelectedItem = focusItem;
        }

        if (wingmanPosType == WingmanPosType.RightWingman)
        {
            if (focusItem == leftSelectedItem)
            {
                leftSelectedItem = null;
                CacheGame.SetWingManLeftUserSelected((int)WingmanTypeEnum.None);
            }

            if (rightSelectedItem != null)
                rightSelectedItem.SetAsNormal();

            rightSelectedItem = focusItem;
        }

        focusItem.SetAsSelected(wingmanPosType);
        focusItem.ShowFocusEffect();
    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();

        if (panelCongratulations.gameObject.activeInHierarchy)
        {
            panelCongratulations.gameObject.SetActive(false);
            return;
        }

        if (panelEvolve.gameObject.activeInHierarchy)
        {
            panelEvolve.gameObject.SetActive(false);
            return;
        }

        if (openSelectFromType == SelectDeviceHandler.OpenSelectFromType.FromSelectLevel)
        {
            PopupManagerCuong.Instance.ShowSelectLevelPopup();
        }
        else if (openSelectFromType == SelectDeviceHandler.OpenSelectFromType.FromTournament)
        {
            PopupManager.Instance.ShowTournamentPopup();
        }

        gameObject.SetActive(false);
        PopupManager.Instance.ShowChatWorldPopup();
        //switch (GameContext.typeOpenSelectItemFrom)
        //{
        //    case GameContext.TypeOpenSelectItemFrom.EndlessButton:

        //        break;
        //    case GameContext.TypeOpenSelectItemFrom.LevelButton:
        //        break;
        //    default:
        //        break;
        //}
    }
}
