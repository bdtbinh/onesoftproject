﻿using UnityEngine;
using com.ootii.Messages;
using TCore;
using System.Collections;

public class PanelEvolve : MonoBehaviour
{
    private const string BTN_EVOLVE_SPRITE_ENABLE = "PVP_btn_fight";
    private const string BTN_EVOLVE_SPRITE_DISABLE = "PVP_btn_fight_d";

    public enum PanelEvolveType
    {
        Aircraft,
        Wingman,
        Wing
    }

    [SerializeField]
    private UISprite sLowerRank;

    [SerializeField]
    private UISprite sHigherRank;

    [SerializeField]
    private UISprite sLowerRankGlow;

    [SerializeField]
    private UISprite sHigherRankGlow;

    [SerializeField]
    private AircraftAnimations aircraftAnimLowerRank;

    [SerializeField]
    private AircraftAnimations aircraftAnimHigherRank;

    [SerializeField]
    private WingmanAnimations wingmanAnimLowerRank;

    [SerializeField]
    private WingmanAnimations wingmanAnimHigherRank;

    [SerializeField]
    private WingAnimations wingAnimLowerRank;

    [SerializeField]
    private WingAnimations wingAnimHigherRank;

    [SerializeField]
    private PanelCongratulations panelCongratulations;

    [SerializeField]
    private UISprite sSkillIcon;

    [SerializeField]
    private UILabel lCardsNumber;

    [SerializeField]
    private UISprite sBlueProgress;

    [SerializeField]
    private UISprite sGreenProgress;

    //[SerializeField]
    //private UISprite sSpecificCards;

    //[SerializeField]
    //private UILabel lSpecificCards;

    [SerializeField]
    private ElementItemInfo specificCardInfo;

    //[SerializeField]
    //private UISprite sGeneralCards;

    //[SerializeField]
    //private UILabel lGeneralCards;

    [SerializeField]
    private ElementItemInfo generalCardInfo;

    [SerializeField]
    private UISprite sPassiveSkill;

    [SerializeField]
    private UILabel lPassiveName;

    [SerializeField]
    private UILabel lPassiveDes;

    [SerializeField]
    private UILabel lMaxlevel;

    [SerializeField]
    private UILabel lEvolveCost;

    //[SerializeField]
    //private UISprite sIconItem;

    [SerializeField]
    private UISprite sBtnEvolveBG;

    [SerializeField]
    private GameObject sRedDot;         //Bật lên khi aircraft hoặc wingman có thể tiến hóa được

    [SerializeField]
    private UILabel lNeedMoreCard;

    [SerializeField]
    private UILabel lNeedMoreGem;

    [SerializeField]
    private GameObject exclamationMark;

    //Cache fields
    private PanelEvolveType panelType;

    private AircraftTypeEnum aircraftType;
    private WingmanTypeEnum wingmanType;
    private WingTypeEnum wingType;
    private Rank rank;

    private int specificCards;
    private int generalCards;
    private int evolveCards;
    private int evolveCost;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventID.ON_REWARD_SUCCESSED, OnRewardSuccessed, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_REWARD_SUCCESSED, OnRewardSuccessed, true);
    }

    void OnRewardSuccessed(IMessage msg)
    {
        if (panelType == PanelEvolveType.Aircraft)
        {
            specificCards = MinhCacheGame.GetSpaceShipCards(aircraftType);
            generalCards = MinhCacheGame.GetSpaceShipGeneralCards();
        }
        else
        {
            specificCards = MinhCacheGame.GetWingmanCards(wingmanType);
            generalCards = MinhCacheGame.GetWingmanGeneralCards();
        }

        specificCardInfo.UpdateQuantity(specificCards);
        generalCardInfo.UpdateQuantity(generalCards);
        //lGeneralCards.text = generalCards.ToString();

        lCardsNumber.text = specificCards + generalCards + "/" + evolveCards;
    }

    public void ShowAircraftEvolvePanel(AircraftTypeEnum aircraftType, Rank rank)
    {
        this.aircraftType = aircraftType;
        this.rank = rank;
        panelType = PanelEvolveType.Aircraft;

        int lowerRankIndex = (int)rank - 1;

        sLowerRank.spriteName = HangarValue.RankSpriteName[(Rank)lowerRankIndex];
        sHigherRank.spriteName = HangarValue.RankSpriteName[rank];
        sLowerRank.MakePixelPerfect();
        sHigherRank.MakePixelPerfect();

        if (lowerRankIndex > 1)
        {
            sLowerRankGlow.gameObject.SetActive(true);
            sLowerRankGlow.spriteName = "Glow_" + (Rank)(lowerRankIndex);
        }
        else
        {
            sLowerRankGlow.gameObject.SetActive(false);
        }
        sHigherRankGlow.spriteName = "Glow_" + rank;

        aircraftAnimLowerRank.PlayAnimations(aircraftType, (Rank)lowerRankIndex);
        aircraftAnimHigherRank.PlayAnimations(aircraftType, rank);

        specificCards = MinhCacheGame.GetSpaceShipCards(aircraftType);
        generalCards = MinhCacheGame.GetSpaceShipGeneralCards();
        evolveCards = RankSheet.Get((int)rank).card_evolve;

        //sSpecificCards.spriteName = "card_A" + (int)aircraftType;
        //lSpecificCards.text = specificCards.ToString();
        specificCardInfo.CreateItem("Aircraft" + (int)aircraftType + "Card", specificCards, false);

        //lGeneralCards.text = generalCards.ToString();

        generalCardInfo.CreateItem(ItemInfoEnumType.AircraftGeneralCard.ToString(), generalCards, false);
        lCardsNumber.text = specificCards + generalCards + "/" + evolveCards;

        sBlueProgress.fillAmount = specificCards * 1.0f / evolveCards;
        sGreenProgress.fillAmount = (specificCards + generalCards) * 1.0f / evolveCards;

        int skillIndex = GetAircraftSkillIndex(aircraftType, rank);
        sPassiveSkill.spriteName = "Skill" + skillIndex;
        lPassiveName.text = HangarValue.SkillNames(skillIndex);
        lPassiveDes.text = SkillInfoDescription.GetAircraftSkillDes(aircraftType, rank);

        //sIconItem.spriteName = "hangar_main_" + (int)aircraftType;

        int oldMaxLevel = RankSheet.Get(lowerRankIndex).max_level;
        int newMaxlevel = RankSheet.Get((int)rank).max_level;
        lMaxlevel.text = I2.Loc.ScriptLocalization.popup_evolve_max_level.Replace("%{low_level}", "[cc2900][b]" + oldMaxLevel + "[/b][-]").Replace("%{high_level}", "[00b300][b]" + newMaxlevel + "[/b][-]");

        evolveCost = RankSheet.Get((int)rank).gem_evolve;
        lEvolveCost.text = GameContext.FormatNumber(evolveCost);

        if (CacheGame.GetSpaceShipIsUnlocked((int)aircraftType) != 1)
        {
            sBtnEvolveBG.spriteName = BTN_EVOLVE_SPRITE_DISABLE;
            sRedDot.SetActive(false);
        }
        else
        {
            sBtnEvolveBG.spriteName = BTN_EVOLVE_SPRITE_ENABLE;

            if (CacheGame.GetTotalGem() >= evolveCost && specificCards + generalCards >= evolveCards)
            {
                sRedDot.SetActive(true);
            }
            else
            {
                sRedDot.SetActive(false);
            }
        }

        CheckToDisplayNeedMoreText();

        gameObject.SetActive(true);
    }

    public void ShowWingmanEvolvePanel(WingmanTypeEnum wingmanType, Rank rank)
    {
        this.wingmanType = wingmanType;
        this.rank = rank;
        panelType = PanelEvolveType.Wingman;

        int lowerRankIndex = (int)rank - 1;

        sLowerRank.spriteName = HangarValue.RankSpriteName[(Rank)lowerRankIndex];
        sHigherRank.spriteName = HangarValue.RankSpriteName[rank];
        sLowerRank.MakePixelPerfect();
        sHigherRank.MakePixelPerfect();

        if (lowerRankIndex > 1)
        {
            sLowerRankGlow.gameObject.SetActive(true);
            sLowerRankGlow.spriteName = "Glow_" + (Rank)(lowerRankIndex);
        }
        else
        {
            sLowerRankGlow.gameObject.SetActive(false);
        }
        sHigherRankGlow.spriteName = "Glow_" + rank;

        wingmanAnimLowerRank.PlayAnimations(wingmanType, (Rank)lowerRankIndex);
        wingmanAnimHigherRank.PlayAnimations(wingmanType, rank);

        specificCards = MinhCacheGame.GetWingmanCards(wingmanType);
        generalCards = MinhCacheGame.GetWingmanGeneralCards();
        evolveCards = RankSheet.Get((int)rank).card_evolve;

        //sSpecificCards.spriteName = "card_D" + (int)wingmanType;
        //lSpecificCards.text = specificCards.ToString();

        specificCardInfo.CreateItem("Drone" + (int)wingmanType + "Card", specificCards, false);

        //lGeneralCards.text = generalCards.ToString();
        generalCardInfo.CreateItem(ItemInfoEnumType.DroneGeneralCard.ToString(), generalCards, false);
        lCardsNumber.text = specificCards + generalCards + "/" + evolveCards;

        sBlueProgress.fillAmount = specificCards * 1.0f / evolveCards;
        sGreenProgress.fillAmount = (specificCards + generalCards) * 1.0f / evolveCards;

        //sIconItem.spriteName = HangarValue.WingmanIconSpriteName[wingmanType];

        int oldMaxLevel = RankSheet.Get(lowerRankIndex).max_level;
        int newMaxlevel = RankSheet.Get((int)rank).max_level;
        lMaxlevel.text = I2.Loc.ScriptLocalization.popup_evolve_max_level.Replace("%{low_level}", "[cc2900][b]" + oldMaxLevel + "[/b][-]").Replace("%{high_level}", "[00b300][b]" + newMaxlevel + "[/b][-]");

        evolveCost = RankSheet.Get((int)rank).gem_evolve;
        lEvolveCost.text = GameContext.FormatNumber(evolveCost);

        if (CacheGame.GetWingManIsUnlocked((int)wingmanType) != 1)
        {
            sBtnEvolveBG.spriteName = BTN_EVOLVE_SPRITE_DISABLE;
            sRedDot.SetActive(false);
        }
        else
        {
            sBtnEvolveBG.spriteName = BTN_EVOLVE_SPRITE_ENABLE;

            if (CacheGame.GetTotalGem() >= evolveCost && specificCards + generalCards >= evolveCards)
            {
                sRedDot.SetActive(true);
            }
            else
            {
                sRedDot.SetActive(false);
            }
        }

        CheckToDisplayNeedMoreText();

        gameObject.SetActive(true);
    }

    public void ShowWingEvolvePanel(WingTypeEnum wingType, Rank rank)
    {
        this.wingType = wingType;
        this.rank = rank;
        panelType = PanelEvolveType.Wing;

        int lowerRankIndex = (int)rank - 1;

        sLowerRank.spriteName = HangarValue.RankSpriteName[(Rank)lowerRankIndex];
        sHigherRank.spriteName = HangarValue.RankSpriteName[rank];
        sLowerRank.MakePixelPerfect();
        sHigherRank.MakePixelPerfect();

        if (lowerRankIndex > 1)
        {
            sLowerRankGlow.gameObject.SetActive(true);
            sLowerRankGlow.spriteName = "Glow_" + (Rank)(lowerRankIndex);
        }
        else
        {
            sLowerRankGlow.gameObject.SetActive(false);
        }
        sHigherRankGlow.spriteName = "Glow_" + rank;

        wingAnimLowerRank.PlayAnimations(wingType, (Rank)lowerRankIndex);
        wingAnimHigherRank.PlayAnimations(wingType, rank);

        sSkillIcon.spriteName = "Skill" + WingSheet.Get((int)wingType).skill_rank_c;

        specificCards = MinhCacheGame.GetWingCards(wingType);
        generalCards = MinhCacheGame.GetWingGeneralCards();
        evolveCards = RankSheet.Get((int)rank).card_evolve;

        specificCardInfo.CreateItem("Wing" + (int)wingType + "Card", specificCards, false);

        generalCardInfo.CreateItem(ItemInfoEnumType.WingGeneralCard.ToString(), generalCards, false);
        lCardsNumber.text = specificCards + generalCards + "/" + evolveCards;

        sBlueProgress.fillAmount = specificCards * 1.0f / evolveCards;
        sGreenProgress.fillAmount = (specificCards + generalCards) * 1.0f / evolveCards;

        int skillIndex = GetWingSkillIndex(wingType, rank);
        sPassiveSkill.spriteName = "Skill" + skillIndex;
        lPassiveName.text = HangarValue.SkillNames(skillIndex);
        lPassiveDes.text = SkillInfoDescription.GetWingSkillDes(wingType, rank);

        //sIconItem.spriteName = "hangar_main_" + (int)aircraftType;

        int oldMaxLevel = RankSheet.Get(lowerRankIndex).max_level;
        int newMaxlevel = RankSheet.Get((int)rank).max_level;
        lMaxlevel.text = I2.Loc.ScriptLocalization.popup_evolve_max_level.Replace("%{low_level}", "[cc2900][b]" + oldMaxLevel + "[/b][-]").Replace("%{high_level}", "[00b300][b]" + newMaxlevel + "[/b][-]");

        evolveCost = RankSheet.Get((int)rank).gem_evolve;
        lEvolveCost.text = GameContext.FormatNumber(evolveCost);

        if (CacheGame.GetWingIsUnlocked((int)wingType) != 1)
        {
            sBtnEvolveBG.spriteName = BTN_EVOLVE_SPRITE_DISABLE;
            sRedDot.SetActive(false);
        }
        else
        {
            sBtnEvolveBG.spriteName = BTN_EVOLVE_SPRITE_ENABLE;

            if (CacheGame.GetTotalGem() >= evolveCost && specificCards + generalCards >= evolveCards)
            {
                sRedDot.SetActive(true);
            }
            else
            {
                sRedDot.SetActive(false);
            }
        }

        CheckToDisplayNeedMoreText();

        gameObject.SetActive(true);
    }

    private void CheckToDisplayNeedMoreText()
    {
        //Hiển thị text need more cards, more gems
        exclamationMark.SetActive(true);

        if (specificCards + generalCards < evolveCards)
        {
            lNeedMoreCard.gameObject.SetActive(true);

            if (CacheGame.GetTotalGem() < evolveCost)
            {
                lNeedMoreCard.transform.localPosition = new Vector2(lNeedMoreCard.transform.localPosition.x, 13);
            }
            else
            {
                lNeedMoreCard.transform.localPosition = new Vector2(lNeedMoreCard.transform.localPosition.x, 0);
            }

            int moreCards = evolveCards - (specificCards + generalCards);

            lNeedMoreCard.text = I2.Loc.ScriptLocalization.msg_need_more_card_number.Replace("%{card_number}", "[85E0F8FF]" + moreCards.ToString() + "[-]");
        }
        else
        {
            lNeedMoreCard.gameObject.SetActive(false);
        }

        if (CacheGame.GetTotalGem() < evolveCost)
        {
            lNeedMoreGem.gameObject.SetActive(true);
            if (specificCards + generalCards < evolveCards)
            {
                lNeedMoreGem.transform.localPosition = new Vector2(lNeedMoreGem.transform.localPosition.x, -13);
            }
            else
            {
                lNeedMoreGem.transform.localPosition = new Vector2(lNeedMoreGem.transform.localPosition.x, 0);
            }

            int moreGems = evolveCost - CacheGame.GetTotalGem();

            lNeedMoreGem.text = I2.Loc.ScriptLocalization.msg_need_more_gem_number.Replace("%{gem_number}", "[DEFF83FF]" + moreGems.ToString() + "[-]");
        }
        else
        {
            lNeedMoreGem.gameObject.SetActive(false);
        }

        if (specificCards + generalCards >= evolveCards && CacheGame.GetTotalGem() >= evolveCost)
        {
            exclamationMark.SetActive(false);
        }
    }

    private int GetAircraftSkillIndex(AircraftTypeEnum aircraftType, Rank rank)
    {
        switch (rank)
        {
            case Rank.C:
                return AircraftSheet.Get((int)aircraftType).skill_rank_c;

            case Rank.B:
                return AircraftSheet.Get((int)aircraftType).skill_rank_b;

            case Rank.A:
                return AircraftSheet.Get((int)aircraftType).skill_rank_a;

            case Rank.S:
                return AircraftSheet.Get((int)aircraftType).skill_rank_s;

            case Rank.SS:
                return AircraftSheet.Get((int)aircraftType).skill_rank_ss;

            case Rank.SSS:
                return AircraftSheet.Get((int)aircraftType).skill_rank_sss;

            default:
                return AircraftSheet.Get((int)aircraftType).skill_rank_c;
        }
    }

    private int GetWingSkillIndex(WingTypeEnum wingType, Rank rank)
    {
        switch (rank)
        {
            case Rank.C:
                return WingSheet.Get((int)wingType).skill_rank_c;

            case Rank.B:
                return WingSheet.Get((int)wingType).skill_rank_b;

            case Rank.A:
                return WingSheet.Get((int)wingType).skill_rank_a;

            case Rank.S:
                return WingSheet.Get((int)wingType).skill_rank_s;

            case Rank.SS:
                return WingSheet.Get((int)wingType).skill_rank_ss;

            case Rank.SSS:
                return WingSheet.Get((int)wingType).skill_rank_sss;

            default:
                return WingSheet.Get((int)wingType).skill_rank_c;
        }
    }

    public void OnClickBtnEvolve()
    {
        SoundManager.PlayClickButton();

        if (panelType == PanelEvolveType.Aircraft)
        {
            if (CacheGame.GetSpaceShipIsUnlocked((int)aircraftType) != 1)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_unlock_aircraft_first, false, 1.5f);
                return;
            }
        }
        else if(panelType == PanelEvolveType.Wingman)
        {
            if (CacheGame.GetWingManIsUnlocked((int)wingmanType) != 1)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_unlock_wingman_first, false, 1.5f);
                return;
            }
        }
        else
        {
            if (CacheGame.GetWingIsUnlocked((int)wingType) != 1)
            {
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_unlock_wing_first, false, 1.5f);
                return;
            }
        }

        int totalCards = specificCards + generalCards;

        if (totalCards < evolveCards)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_card, false, 1.5f);
            return;
        }

        if (CacheGame.GetTotalGem() < evolveCost)
        {
            //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_gem, false, 1.5f);
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
            return;
        }

        //Tính toán rồi lưu lại card của máy bay và wingman
        if (panelType == PanelEvolveType.Aircraft)
        {
            panelCongratulations.ShowPanelAircraft(aircraftType, rank, PanelCongratulations.CongraType.NewRank);
            gameObject.SetActive(false);                            //Tắt luôn panel evolve khi mở panel congratulation

            if (specificCards >= evolveCards)
            {
                MinhCacheGame.SetSpaceShipCards(aircraftType, specificCards - evolveCards, FirebaseLogSpaceWar.EvolveAircraft_why, aircraftType.ToString(), aircraftType.ToString());
            }
            else
            {
                MinhCacheGame.SetSpaceShipCards(aircraftType, 0, FirebaseLogSpaceWar.EvolveAircraft_why, aircraftType.ToString(), aircraftType.ToString());
                MinhCacheGame.SetSpaceShipGeneralCards(totalCards - evolveCards, FirebaseLogSpaceWar.EvolveAircraft_why, aircraftType.ToString());
            }

            //Nâng rank aircraft
            CacheGame.SetSpaceShipRank(aircraftType, rank);
            PanelCoinGem.Instance.AddGemTop(-evolveCost, FirebaseLogSpaceWar.EvolveAircraft_why, aircraftType.ToString());
        }
        else if (panelType == PanelEvolveType.Wingman)
        {
            panelCongratulations.ShowPanelWingman(wingmanType, rank, PanelCongratulations.CongraType.NewRank);
            gameObject.SetActive(false);                            //Tắt luôn panel evolve khi mở panel congratulation

            if (specificCards >= evolveCards)
            {
                MinhCacheGame.SetWingmanCards(wingmanType, specificCards - evolveCards, FirebaseLogSpaceWar.EvolveDrone_why, wingmanType.ToString(), wingmanType.ToString());
            }
            else
            {
                MinhCacheGame.SetWingmanCards(wingmanType, 0, FirebaseLogSpaceWar.EvolveDrone_why, wingmanType.ToString(), wingmanType.ToString());
                MinhCacheGame.SetWingmanGeneralCards(totalCards - evolveCards, FirebaseLogSpaceWar.EvolveDrone_why, wingmanType.ToString());
            }

            //Nâng rank wingman
            CacheGame.SetWingmanRank(wingmanType, rank);
            PanelCoinGem.Instance.AddGemTop(-evolveCost, FirebaseLogSpaceWar.EvolveDrone_why, wingmanType.ToString());
        }
        else
        {
            panelCongratulations.ShowPanelWing(wingType, rank, PanelCongratulations.CongraType.NewRank);
            gameObject.SetActive(false);                            //Tắt luôn panel evolve khi mở panel congratulation

            if (specificCards >= evolveCards)
            {
                MinhCacheGame.SetWingCards(wingType, specificCards - evolveCards, FirebaseLogSpaceWar.EvolveWing_why, wingType.ToString(), wingType.ToString());
            }
            else
            {
                MinhCacheGame.SetWingCards(wingType, 0, FirebaseLogSpaceWar.EvolveWing_why, wingType.ToString(), wingType.ToString());
                MinhCacheGame.SetWingGeneralCards(totalCards - evolveCards, FirebaseLogSpaceWar.EvolveWing_why, wingType.ToString());
            }

            //Nâng rank wing
            CacheGame.SetWingRank(wingType, rank);
            PanelCoinGem.Instance.AddGemTop(-evolveCost, FirebaseLogSpaceWar.EvolveWing_why, wingType.ToString());
        }

        MessageDispatcher.SendMessage(EventID.ON_EVOLVE_SUCCESSED);
    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
    }

    //private void Update()
    //{
    //    if (Input.GetKey(KeyCode.Escape))
    //    {
    //        gameObject.SetActive(false);
    //    }
    //}
}
