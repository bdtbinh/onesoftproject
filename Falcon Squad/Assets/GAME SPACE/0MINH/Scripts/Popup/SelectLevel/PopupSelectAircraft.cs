﻿using UnityEngine;
using System.Collections.Generic;
using com.ootii.Messages;
using TCore;
using EasyMobile;
using UnityEngine.SceneManagement;
using Sirenix.OdinInspector;
using I2.Loc;
using DG.Tweening;

public class PopupSelectAircraft : MonoBehaviour
{
    public GameObject fxTutorialUpgrade;
    private const float BUTTON_GOLD_LEFT_X = -200f;
    private const float BUTTON_GEM_RIGHT_X = 200f;

    private const string BUTTON_UPGRADE_DISABLE_SPRITE = "btn_upgrade_item";
    private const string BUTTON_UPGRADE_ENABLE_SPRITE = "btn_upgrade_item_t";

    public static SelectDeviceHandler.OpenSelectFromType openSelectFromType;

    [SerializeField]
    private PanelCongratulations panelCongratulations;

    [SerializeField]
    private PanelEvolve panelEvolve;

    [SerializeField]
    private UILabel lAircraftName;

    [SerializeField]
    private UISprite sRankTitle;

    [SerializeField]
    private UILabel lDescription;

    [SerializeField]
    private AircraftAnimations aircraftAnimations;

    [SerializeField]
    private GameObject movingEffect;

    [SerializeField]
    private AnimationCurve animationCurve;

    [SerializeField]
    private float movingTime = 0.5f;

    //[SerializeField]
    //private GameObject between2Lines;

    //Progress bar region
    //[SerializeField]
    //private GameObject progressBar;

    [TitleGroup("Progress")]
    [SerializeField]
    private UIButton btnCard;

    [SerializeField]
    private UISprite sGreenProgress;

    [SerializeField]
    private UISprite sBlueProgress;

    [SerializeField]
    private UILabel lCardNumber;

    [SerializeField]
    private GameObject btnEvolve;

    //Button buy, unlock:
    [TitleGroup("Unlock or Buy")]
    [SerializeField]
    private UILabel lUnlockMessage;

    [SerializeField]
    private GameObject btnUnlock;

    [SerializeField]
    private GameObject btnBuyGold;

    [SerializeField]
    private UILabel lGoldPrice;

    [SerializeField]
    private GameObject btnBuyGem;

    [SerializeField]
    private UILabel lGemPrice;

    [SerializeField]
    private GameObject btnPurchase;

    [SerializeField]
    private UILabel lPurchasePrice;

    //Upgradation
    [TitleGroup("Upgradations")]

    [SerializeField]
    private UILabel lLevel;

    [SerializeField]
    private GameObject[] levelBars;

    [SerializeField]
    private UIButton btnUpgrade;

    [SerializeField]
    private UILabel lUpgradeNumber;

    [SerializeField]
    private UISprite sUpgradeIcon;

    [TitleGroup("List aircraft items")]
    [SerializeField]
    private AircraftItem[] aircraftItems;

    [TitleGroup("Equip, Unequip, Try buttons")]
    [SerializeField]
    private GameObject btnEquip;

    [SerializeField]
    private GameObject btnUnequip;

    [SerializeField]
    private GameObject btnTry;
    //Bật lên
    [SerializeField]
    private PanelSkillUIManager panelSkill;

    [SerializeField]
    private GameObject sEvolveRedDot;           //Show khi có thể elvolve được aircraft

    [SerializeField]
    private GameObject sUpgradeRedDot;          //Show khi có thể upgrade được aircraft

    private Vector3[] itemLocalPositions;

    //Item được select (k phải item được chọn mang vào chơi)
    private AircraftItem focusItem;

    //Item mang vào chơi
    private AircraftItem selectedItem;

    //Cached fields
    private int currentLevel;
    private int maxLevel;
    private int upgradeGoldCost;
    private int upgradeGemCost;
    private int specificCards;
    private int generalCards;
    private int evolveCards;
    private int unlockCards;

    private bool needToLoadFromTry;
    public bool NeedToLoadFromTry
    {
        set
        {
            needToLoadFromTry = value;
        }
    }

    private void Awake()
    {
        itemLocalPositions = new Vector3[aircraftItems.Length];

        for (int i = 0; i < itemLocalPositions.Length; i++)
        {
            itemLocalPositions[i] = aircraftItems[i].transform.localPosition;
        }
    }

    ////OnEnable có trường hợp chạy trước awake của object HangarSelectItem nên phải thêm biến này để lần đầu tiên sẽ chạy UpdateItemStatus trong start. 
    ////Tránh trường hợp có biến của HangarSelectItem chưa được khởi tạo mà đã truy cập.
    bool isStarted;
    private void Start()
    {
        isStarted = true;
        InitItemsStatus();
    }

    private void OnRewardSuccessed(IMessage msg)
    {
        UpdateUnlockUI();
    }

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventID.ON_REWARD_SUCCESSED, OnRewardSuccessed, true);
        MessageDispatcher.AddListener(EventID.HANGAR_ITEM_CHANGED, OnClickItem, true);
        MessageDispatcher.AddListener(EventID.ON_EVOLVE_SUCCESSED, OnEvolveSuccessed, true);

        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;

        if (!isStarted)
            return;

        InitItemsStatus();

    }

    private void OnEvolveSuccessed(IMessage msg)
    {
        focusItem.SetRank(CacheGame.GetSpaceShipRank(focusItem.AircraftType));
        UpdateInfo();
        UpdateUnlockUI();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_REWARD_SUCCESSED, OnRewardSuccessed, true);
        MessageDispatcher.RemoveListener(EventID.HANGAR_ITEM_CHANGED, OnClickItem, true);
        MessageDispatcher.RemoveListener(EventID.ON_EVOLVE_SUCCESSED, OnEvolveSuccessed, true);

        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;

        MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI, 0f);


        //GameContext.typeOpenSelectItemFrom = GameContext.TypeOpenSelectItemFrom.None;

        panelEvolve.gameObject.SetActive(false);
    }

    List<AircraftItem> ordinalPosList = new List<AircraftItem>();

    public void ShowPopUp(SelectDeviceHandler.OpenSelectFromType fromType, bool needToLoadFromTry)
    {
        openSelectFromType = fromType;
        this.needToLoadFromTry = needToLoadFromTry;
        gameObject.SetActive(true);
    }

    private void InitItemsStatus()
    {
        AircraftTypeEnum selectedType = (AircraftTypeEnum)CacheGame.GetSpaceShipUserSelected();

        for (int i = 0; i < aircraftItems.Length; i++)
        {
            if (aircraftItems[i].AircraftType == selectedType)                                        //Item được chọn
            {
                selectedItem = aircraftItems[i];
                aircraftItems[i].SetAsSelected();
                aircraftItems[i].ShowFocusEffect();
                focusItem = aircraftItems[i];
                //UpdateInfo();
                //UpdateUnlockUI();
                ordinalPosList.Insert(0, aircraftItems[i]);
            }
            else if (CacheGame.GetSpaceShipIsUnlocked((int)aircraftItems[i].AircraftType) == 1)        //Item đã unlock
            {
                aircraftItems[i].SetAsNormal();

                if (aircraftItems[i].gameObject.activeInHierarchy)
                {
                    ordinalPosList.Add(aircraftItems[i]);
                }
            }
        }

        //Item chưa unlock
        for (int i = 0; i < aircraftItems.Length; i++)
        {
            if (CacheGame.GetSpaceShipIsUnlocked((int)aircraftItems[i].AircraftType) != 1)
            {
                aircraftItems[i].SetAsLocked();

                if (aircraftItems[i].gameObject.activeInHierarchy)
                {
                    ordinalPosList.Add(aircraftItems[i]);
                }
            }
        }

        if (needToLoadFromTry)
        {
            if (focusItem != null)
                focusItem.HideFocusEffect();

            focusItem = aircraftItems[CacheGame.GetSpaceShipUserTry() - 1];
            focusItem.ShowFocusEffect();

            needToLoadFromTry = false;
        }

        for (int i = 0; i < ordinalPosList.Count; i++)
        {
            ordinalPosList[i].transform.localPosition = itemLocalPositions[i];
        }

        ordinalPosList.Clear();

        UpdateInfo();
        UpdateUnlockUI();
        UpdateButtomButtons();
    }

    #region update UI

    private void UpdateInfo()
    {
        lPurchasePrice.text = CuongUtils.GetIapLocalizedPrice(AircraftSheet.Get((int)focusItem.AircraftType).purchase_id);

        lAircraftName.text = HangarValue.AircraftNames(focusItem.AircraftType);
        sRankTitle.spriteName = HangarValue.RankSpriteName[focusItem.Rank];

        lDescription.text = HangarValue.AircraftDescriptions(focusItem.AircraftType);

        aircraftAnimations.PlayAnimations(focusItem.AircraftType, focusItem.Rank);

        panelSkill.LoadSkills(focusItem.AircraftType);
    }

    private void UpdateUnlockUI()
    {
        bool isUnlocked = CacheGame.GetSpaceShipIsUnlocked((int)focusItem.AircraftType) == 1;

        //btnCard.transform.parent.gameObject.SetActive(false);
        btnEvolve.SetActive(false);

        lUnlockMessage.gameObject.SetActive(false);
        btnUnlock.SetActive(false);
        btnBuyGold.SetActive(false);
        btnBuyGem.SetActive(false);

        btnUpgrade.transform.parent.gameObject.SetActive(false);

        specificCards = MinhCacheGame.GetSpaceShipCards(focusItem.AircraftType);
        generalCards = MinhCacheGame.GetSpaceShipGeneralCards();

        if (isUnlocked)
        {
            btnCard.normalSprite = "card_0";
            bool isMaxRank = (int)focusItem.Rank >= (int)Constant.MAX_RANK_APPLY;

            if (isMaxRank)
            {
                lCardNumber.text = ScriptLocalization.label_max;
                sBlueProgress.fillAmount = 1f;
            }
            else
            {
                //Load card data
                evolveCards = RankSheet.Get((int)focusItem.Rank + 1).card_evolve;

                int totalCards = specificCards + generalCards;
                lCardNumber.text = totalCards + "/" + evolveCards;

                sBlueProgress.fillAmount = specificCards * 1.0f / evolveCards;
                sGreenProgress.fillAmount = totalCards * 1.0f / evolveCards;
                btnEvolve.SetActive(true);         //Chưa xax rank thì bật nút evolve
                CheckShowEvolveRedDot();
            }

            btnUpgrade.transform.parent.gameObject.SetActive(true);

            currentLevel = CacheGame.GetSpaceShipLevel(focusItem.AircraftType);
            maxLevel = RankSheet.Get((int)focusItem.Rank).max_level;

            lLevel.text = I2.Loc.ScriptLocalization.level_upgrade + ": " + currentLevel + "/" + maxLevel;
            SetUpgradeBars(CacheGame.GetSpaceShipLevel(focusItem.AircraftType));
            SetUpgradePrice();
        }
        else
        {
            sEvolveRedDot.SetActive(false);
            int levelUnlock = AircraftSheet.Get((int)focusItem.AircraftType).level_unlock;
            unlockCards = AircraftSheet.Get((int)focusItem.AircraftType).card_unlock;

            if (unlockCards > 0)                                    //Unlock bằng card
            {
                btnCard.normalSprite = "card_A" + (int)focusItem.AircraftType;
                lCardNumber.text = specificCards + "/" + unlockCards;

                sGreenProgress.fillAmount = 0f;

                if (MinhCacheGame.GetSpaceShipCards(focusItem.AircraftType) < unlockCards)
                {
                    lUnlockMessage.gameObject.SetActive(true);
                    lUnlockMessage.text = I2.Loc.ScriptLocalization.msg_unlock_by_card;

                    sBlueProgress.fillAmount = MinhCacheGame.GetSpaceShipCards(focusItem.AircraftType) * 1.0f / unlockCards;
                }
                else
                {
                    sBlueProgress.fillAmount = 1f;
                    btnUnlock.SetActive(true);
                }
            }
            else if (levelUnlock > 0)                               //Unlock khi đủ level
            {
                btnCard.normalSprite = "card_0";

                bool isMaxRank = (int)focusItem.Rank >= (int)Constant.MAX_RANK_APPLY;

                if (isMaxRank)
                {
                    lCardNumber.text = ScriptLocalization.label_max;
                    sBlueProgress.fillAmount = 1f;
                }
                else
                {
                    //Load card data
                    evolveCards = RankSheet.Get((int)focusItem.Rank + 1).card_evolve;

                    int totalCards = specificCards + generalCards;
                    lCardNumber.text = totalCards + "/" + evolveCards;

                    sBlueProgress.fillAmount = specificCards * 1.0f / evolveCards;
                    sGreenProgress.fillAmount = totalCards * 1.0f / evolveCards;
                    btnEvolve.SetActive(true);         //Chưa xax rank thì bật nút evolve
                }

                if (CacheGame.GetMaxLevel3Difficult() < levelUnlock)
                {
                    lUnlockMessage.gameObject.SetActive(true);
                    string msg = I2.Loc.ScriptLocalization.msg_unlock_level_to_purchase;
                    lUnlockMessage.text = msg.Replace("%{level_number}", levelUnlock.ToString());
                }
                else
                {
                    btnBuyGold.transform.parent.gameObject.SetActive(true);

                    int goldUnlock = AircraftSheet.Get((int)focusItem.AircraftType).gold_unlock;
                    int gemUnlock = AircraftSheet.Get((int)focusItem.AircraftType).gem_unlock;

                    lGoldPrice.text = GameContext.FormatNumber(goldUnlock);
                    lGemPrice.text = GameContext.FormatNumber(gemUnlock);

                    if (goldUnlock > 0 && gemUnlock > 0)
                    {
                        btnBuyGold.SetActive(true);
                        btnBuyGem.SetActive(true);

                        btnBuyGold.transform.localPosition = new Vector3(BUTTON_GOLD_LEFT_X, btnBuyGold.transform.localPosition.y, btnBuyGold.transform.localPosition.z);
                        btnBuyGem.transform.localPosition = new Vector3(BUTTON_GEM_RIGHT_X, btnBuyGem.transform.localPosition.y, btnBuyGem.transform.localPosition.z);
                    }
                    else if (goldUnlock > 0)
                    {
                        btnBuyGold.SetActive(true);
                        btnBuyGold.transform.localPosition = new Vector3(0, btnBuyGold.transform.localPosition.y, btnBuyGold.transform.localPosition.z);
                    }
                    else
                    {
                        btnBuyGem.SetActive(true);
                        btnBuyGem.transform.localPosition = new Vector3(0, btnBuyGem.transform.localPosition.y, btnBuyGem.transform.localPosition.z);
                    }
                }
            }
        }
    }

    private void UpdateButtomButtons()
    {
        btnEquip.SetActive(false);
        btnUnequip.SetActive(false);
        btnTry.SetActive(false);
        btnPurchase.SetActive(false);

        bool isUnlocked = CacheGame.GetSpaceShipIsUnlocked((int)focusItem.AircraftType) == 1;

        if (isUnlocked)
        {
            if ((int)focusItem.AircraftType != CacheGame.GetSpaceShipUserSelected())
            {
                btnEquip.SetActive(true);
            }
        }
        else
        {
            btnTry.SetActive(true);

            if (CacheGame.GetMaxLevel3Difficult() >= 3 && !string.IsNullOrEmpty(AircraftSheet.Get((int)focusItem.AircraftType).purchase_id))
            {
                btnPurchase.SetActive(true);
            }
        }
    }

    private void SetUpgradeBars(int level)
    {
        int remainder = level % levelBars.Length;

        if (level == 0)
        {
            remainder = 0;
        }
        else
        {
            if (remainder == 0)
                remainder = 15;
        }

        for (int i = 0; i < levelBars.Length; i++)
        {
            levelBars[i].SetActive(i < remainder);
        }
    }

    private void SetUpgradePrice()
    {
        if (currentLevel >= RankSheet.Get((int)Constant.MAX_RANK_APPLY).max_level)
        {
            btnUpgrade.gameObject.SetActive(false);
        }
        else
        {
            btnUpgrade.gameObject.SetActive(true);

            upgradeGoldCost = AirCUpgradeCostSheet.Get(currentLevel + 1).gold;
            upgradeGemCost = AirCUpgradeCostSheet.Get(currentLevel + 1).gem;

            if (upgradeGoldCost > 0)
            {
                lUpgradeNumber.text = upgradeGoldCost.ToString();
                sUpgradeIcon.spriteName = HangarValue.BuyResourceSpriteName[BuyType.Gold];
            }
            else
            {
                lUpgradeNumber.text = upgradeGemCost.ToString();
                sUpgradeIcon.spriteName = HangarValue.BuyResourceSpriteName[BuyType.Gem];
            }

            if (currentLevel < maxLevel)
            {
                btnUpgrade.normalSprite = BUTTON_UPGRADE_ENABLE_SPRITE;
            }
            else
            {
                btnUpgrade.normalSprite = BUTTON_UPGRADE_DISABLE_SPRITE;
            }

            if (currentLevel < maxLevel)
            {
                if (upgradeGoldCost > 0)
                {
                    if (CacheGame.GetTotalCoin() >= upgradeGoldCost && CacheGame.GetMaxLevel3Difficult() > 0 && CacheGame.GetMaxLevel3Difficult() < 4)
                    {
                        fxTutorialUpgrade.SetActive(true);
                    }
                    else
                    {
                        fxTutorialUpgrade.SetActive(false);
                    }
                }
                else
                {
                    if (CacheGame.GetTotalGem() >= upgradeGemCost && CacheGame.GetMaxLevel3Difficult() > 0 && CacheGame.GetMaxLevel3Difficult() < 4)
                    {
                        fxTutorialUpgrade.SetActive(true);
                    }
                    else
                    {
                        fxTutorialUpgrade.SetActive(false);
                    }
                }
            }

            CheckShowUpgradeRedDot();
        }
    }

    #endregion



    private void IncreaseLevel(ResourceType type)
    {
        currentLevel++;
        CacheGame.SetSpaceShipLevel(focusItem.AircraftType, currentLevel);
        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetUpgrade.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetUpgrade.ToString()) + 1);

        if (type == ResourceType.Gold)
        {
            PanelCoinGem.Instance.AddCoinTop(-upgradeGoldCost, FirebaseLogSpaceWar.UpgradeAircraft_why, focusItem.AircraftType.ToString());
        }

        if (type == ResourceType.Gem)
        {
            PanelCoinGem.Instance.AddGemTop(-upgradeGemCost, FirebaseLogSpaceWar.UpgradeAircraft_why, focusItem.AircraftType.ToString());
        }

        lLevel.text = I2.Loc.ScriptLocalization.level_upgrade + ": " + currentLevel + "/" + maxLevel;
        SetUpgradeBars(currentLevel);
        SetUpgradePrice();

        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_shop_upgraded.Replace("%{level_number}", currentLevel.ToString()), true, 1f);
        //if (CacheGame.GetSpaceShipLevel(AircraftTypeEnum.BataFD) == 5 && focusItem.AircraftType == AircraftTypeEnum.BataFD)
        //{
        //    fxTutorialUpgrade.SetActive(false);
        //    MessageDispatcher.SendMessage(this, EventID.ON_TUTORIAL_UPGRADE_SUCCESSED, true, 0);
        //}


    }

    private void CheckShowEvolveRedDot()
    {
        int evolveCost = RankSheet.Get((int)focusItem.Rank + 1).gem_evolve;

        //Debug.LogError(CacheGame.GetTotalGem() + " * " + evolveCost + " **** " + specificCards + " * " + generalCards + " * " + evolveCards);

        if (CacheGame.GetTotalGem() >= evolveCost && specificCards + generalCards >= evolveCards)
        {
            //Debug.LogError("Show Red Dot");
            sEvolveRedDot.SetActive(true);
        }
        else
        {
            sEvolveRedDot.SetActive(false);
        }
    }

    private void CheckShowUpgradeRedDot()
    {
        if (currentLevel >= maxLevel) return;

        if (upgradeGoldCost > 0)
        {
            if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
            {
                sUpgradeRedDot.SetActive(true);
            }
            else
            {
                sUpgradeRedDot.SetActive(false);
            }
        }
        else
        {
            if (CacheGame.GetTotalGem() >= upgradeGemCost)
            {
                sUpgradeRedDot.SetActive(true);
            }
            else
            {
                sUpgradeRedDot.SetActive(false);
            }
        }
    }


    void LoadLevelTryPlane()
    {
        SoundManager.PlayClickButton();

        //OsAdsManager.Instance.ShowInterstitialAds();
        LoadingSceneManager.Instance.LoadSceneMainUI("LevelTryPlane");

    }

    void OnPurchaseSuccessed(string productName)
    {
        if (productName == EM_IAPConstants.Product_unlock_aircraft_2 || productName == EM_IAPConstants.Product_unlock_aircraft_3 || productName == EM_IAPConstants.Product_unlock_aircraft_6
                || productName == EM_IAPConstants.Product_unlock_aircraft_7 || productName == EM_IAPConstants.Product_unlock_aircraft_8 || productName == EM_IAPConstants.Product_unlock_aircraft_9)
        {
            CacheGame.SetSpaceShipIsUnlocked((int)focusItem.AircraftType, 1);
            CacheGame.SetSpaceShipUserSelected((int)focusItem.AircraftType);

            ChangeFocusItemToSelected();
            UpdateUnlockUI();
            UpdateButtomButtons();

            PopupManagerCuong.Instance.ShowTextNotifiToast(HangarValue.GetUnlockDeviceNotify(HangarValue.AircraftNames(focusItem.AircraftType)), true, 1.88f, 0.56f);
        }
    }



    public void OnClickItem(IMessage msg)
    {
        SoundManager.PlayClickButton();

        if (focusItem != null)
        {
            focusItem.HideFocusEffect();

            ////Debug.LogError(focusItem.transform.localPosition + " * " + ((AircraftItem)msg.Data).transform.localPosition);
            //bool isMovingRight = true;

            //Vector2 pos1 = focusItem.transform.localPosition;
            //Vector2 pos2 = ((AircraftItem)msg.Data).transform.localPosition;

            //if (pos1.y - pos2.y > 10)
            //{
            //    isMovingRight = true;
            //}
            //else if (pos2.y - pos1.y > 10)
            //{
            //    isMovingRight = false;
            //}
            //else
            //{
            //    isMovingRight = pos1.x < pos2.x;
            //}

            //OnClickItemMovingEffect(isMovingRight);
        }

        focusItem = (AircraftItem)msg.Data;
        focusItem.ShowFocusEffect();

        UpdateInfo();
        UpdateUnlockUI();
        UpdateButtomButtons();

    }

    private void OnClickItemMovingEffect(bool isMovingRight)
    {
        if (isMovingRight)
        {
            movingEffect.transform.localPosition = new Vector3(-Screen.width, movingEffect.transform.localPosition.y, 0);
        }
        else
        {
            movingEffect.transform.localPosition = new Vector3(Screen.width, movingEffect.transform.localPosition.y, 0);
        }

        movingEffect.transform.DOLocalMove(new Vector3(0, transform.localPosition.y, 0), movingTime, true).SetEase(animationCurve).Restart();
    }



    public void OnClickBtnEvolve()
    {
        SoundManager.PlayClickButton();
        panelEvolve.ShowAircraftEvolvePanel(focusItem.AircraftType, (Rank)((int)focusItem.Rank + 1));
    }

    public void OnClickBtnUpgrade()
    {
        if (currentLevel < maxLevel)
        {
            if (upgradeGoldCost > 0)
            {
                if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
                {
                    IncreaseLevel(ResourceType.Gold);
                }
                else
                {
                    PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
                }
            }
            else
            {
                if (CacheGame.GetTotalGem() >= upgradeGemCost)
                {
                    IncreaseLevel(ResourceType.Gem);
                }
                else
                {
                    PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
                }
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_need_evolve_spaceship, false, 1.5f);
        }


    }

    public void OnClickBtnPurchase()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(AircraftSheet.Get((int)focusItem.AircraftType).purchase_id, IAPCallbackManager.WherePurchase.shop);
    }

    public void OnClickBtnUnlock()
    {
        SoundManager.PlayClickButton();

        ChangeFocusItemToSelected();
        CacheGame.SetSpaceShipIsUnlocked((int)focusItem.AircraftType, 1);
        CacheGame.SetSpaceShipUserSelected((int)focusItem.AircraftType);
        MinhCacheGame.SetSpaceShipCards(focusItem.AircraftType, MinhCacheGame.GetSpaceShipCards(focusItem.AircraftType) - unlockCards, FirebaseLogSpaceWar.PurchaseAircraft_why, focusItem.AircraftType.ToString(), focusItem.AircraftType.ToString());

        //if (focusItem.AircraftType == AircraftTypeEnum.SkyWraith)
        //    CacheGame.SetPurchasedStarterPack(1);

        UpdateUnlockUI();
        UpdateButtomButtons();

        panelCongratulations.ShowPanelAircraft(focusItem.AircraftType, focusItem.Rank, PanelCongratulations.CongraType.NewItem);
    }

    public void OnClickBtnBuyGem()
    {
        SoundManager.PlayClickButton();
        int gemUnlock = AircraftSheet.Get((int)focusItem.AircraftType).gem_unlock;

        if (CacheGame.GetTotalGem() >= gemUnlock)
        {
            ChangeFocusItemToSelected();

            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.AddGemTop(-gemUnlock, FirebaseLogSpaceWar.PurchaseAircraft_why, focusItem.AircraftType.ToString());
            }

            CacheGame.SetSpaceShipIsUnlocked((int)focusItem.AircraftType, 1);

            CacheGame.SetSpaceShipUserSelected((int)focusItem.AircraftType);

            //if (focusItem.AircraftType == AircraftTypeEnum.SkyWraith)
            //    CacheGame.SetPurchasedStarterPack(1);

            UpdateUnlockUI();
            UpdateButtomButtons();

            panelCongratulations.ShowPanelAircraft(focusItem.AircraftType, focusItem.Rank, PanelCongratulations.CongraType.NewItem);
            //PopupManagerCuong.Instance.ShowTextNotifiToast(HangarValue.AircraftUnlockNotifies[focusItem.AircraftType], true, 1.88f);
            //FirebaseAnalytics.LogEvent("Upgrade", new Parameter("ClickUpgrade", HangarValue.AircraftFirebaseUnlock[focusItem.AircraftType]));
            //FirebaseLogSpaceWar.LogBuyByGem("Aircraft_" + HangarValue.AircraftFirebaseUnlock[focusItem.AircraftType]);
        }
        else
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
        }
    }

    public void OnClickBtnBuyGold()
    {
        SoundManager.PlayClickButton();

        int goldUnlock = AircraftSheet.Get((int)focusItem.AircraftType).gold_unlock;

        if (CacheGame.GetTotalCoin() >= goldUnlock)
        {
            ChangeFocusItemToSelected();

            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.AddCoinTop(-goldUnlock, FirebaseLogSpaceWar.PurchaseAircraft_why, focusItem.AircraftType.ToString());
            }

            CacheGame.SetSpaceShipIsUnlocked((int)focusItem.AircraftType, 1);

            CacheGame.SetSpaceShipUserSelected((int)focusItem.AircraftType);

            //if (focusItem.AircraftType == AircraftTypeEnum.SkyWraith)
            //    CacheGame.SetPurchasedStarterPack(1);

            UpdateUnlockUI();
            UpdateButtomButtons();

            panelCongratulations.ShowPanelAircraft(focusItem.AircraftType, focusItem.Rank, PanelCongratulations.CongraType.NewItem);
            //PopupManagerCuong.Instance.ShowTextNotifiToast(HangarValue.AircraftUnlockNotifies[focusItem.AircraftType], true, 1.88f);
            //FirebaseAnalytics.LogEvent("Upgrade", new Parameter("ClickUpgrade", HangarValue.AircraftFirebaseUnlock[focusItem.AircraftType]));
            //FirebaseLogSpaceWar.LogBuyByGold("Aircraft_" + HangarValue.AircraftFirebaseUnlock[focusItem.AircraftType]);
        }
        else
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
        }
    }

    public void OnClickBtnTry()
    {
        //CacheGame.SetCurrentLevel(0);
        SoundManager.PlayClickButton();

        PopupManagerCuong.Instance.ShowNoticeInTrialAircraftPopup(focusItem.AircraftType);
    }

    public void OnClickBtnEquip()
    {
        SoundManager.PlayClickButton();
        CacheGame.SetSpaceShipUserSelected((int)focusItem.AircraftType);
        ChangeFocusItemToSelected();

        btnEquip.SetActive(false);
    }

    void ChangeFocusItemToSelected()
    {
        selectedItem.SetAsNormal();
        focusItem.SetAsSelected();
        focusItem.ShowFocusEffect();
        selectedItem = focusItem;
    }

    //[SerializeField]
    //private GameObject popupSelectLevel;
    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        //Debug.LogWarning("PopupSelectAircraft: back" + " * " + panelEvolve.gameObject.activeInHierarchy);

        if (panelCongratulations.gameObject.activeInHierarchy)
        {
            panelCongratulations.gameObject.SetActive(false);
            return;
        }

        if (panelEvolve.gameObject.activeInHierarchy)
        {
            panelEvolve.gameObject.SetActive(false);
            return;
        }

        //Debug.LogWarning("PopupSelectAircraft: back" + " * " + 1);

        if (openSelectFromType == SelectDeviceHandler.OpenSelectFromType.FromSelectLevel)
        {
            PopupManagerCuong.Instance.ShowSelectLevelPopup();
        }
        else if (openSelectFromType == SelectDeviceHandler.OpenSelectFromType.FromTournament)
        {
            PopupManager.Instance.ShowTournamentPopup();
        }

        //Debug.LogWarning("PopupSelectAircraft: back" + " * " + 2);
        gameObject.SetActive(false);
        PopupManager.Instance.ShowChatWorldPopup();
        //switch (GameContext.typeOpenSelectItemFrom)
        //{

        //    case GameContext.TypeOpenSelectItemFrom.EndlessButton:

        //        break;
        //    case GameContext.TypeOpenSelectItemFrom.LevelButton:
        //        break;
        //    default:
        //        break;
        //}

    }



}

