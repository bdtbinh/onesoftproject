﻿using UnityEngine;
using I2.Loc;

public class PanelSkillUIManager : MonoBehaviour
{

    [SerializeField]
    private UISprite sRankC;

    [SerializeField]
    private UISprite sRankB;

    [SerializeField]
    private UISprite sRankA;

    [SerializeField]
    private UISprite sRankS;

    [SerializeField]
    private UISprite sRankSS;

    [SerializeField]
    private UISprite sRankSSS;

    [SerializeField]
    private GameObject panelSkillInfo;

    [SerializeField]
    private UISprite sActiveIcon;

    [SerializeField]
    private GameObject circleAround;

    [SerializeField]
    private UILabel lSkillName;

    [SerializeField]
    private UILabel lRankInfo;

    [SerializeField]
    private UILabel lDes;

    [SerializeField]
    private UISprite[] skillSprites;

    [SerializeField]
    private UISprite[] skillMasks;

    private AircraftTypeEnum aircraftType;
    private WingTypeEnum wingType;

    private enum ShowType
    {
        Aircraft,
        Wing
    }

    private ShowType showType;

    public void LoadSkills(AircraftTypeEnum aircraftType)
    {
        this.aircraftType = aircraftType;
        int index = (int)aircraftType;
        showType = ShowType.Aircraft;

        sRankC.spriteName = "Skill" + AircraftSheet.Get(index).skill_rank_c;
        sRankB.spriteName = "Skill" + AircraftSheet.Get(index).skill_rank_b;
        sRankA.spriteName = "Skill" + AircraftSheet.Get(index).skill_rank_a;
        sRankS.spriteName = "Skill" + AircraftSheet.Get(index).skill_rank_s;
        sRankSS.spriteName = "Skill" + AircraftSheet.Get(index).skill_rank_ss;
        sRankSSS.spriteName = "Skill" + AircraftSheet.Get(index).skill_rank_sss;

        int indexRank = (int)CacheGame.GetSpaceShipRank(aircraftType);
        for (int i = 0; i < skillMasks.Length; i++)
        {
            skillMasks[i].gameObject.SetActive(i >= indexRank && i < (int)Constant.MAX_RANK_APPLY);
        }
    }

    public void OnClickBtnSkillC()
    {
        sActiveIcon.transform.position = skillSprites[(int)Rank.C - 1].transform.position;
        sActiveIcon.spriteName = skillSprites[(int)Rank.C - 1].spriteName;

        circleAround.transform.position = new Vector2(sActiveIcon.transform.position.x, circleAround.transform.position.y);

        lRankInfo.text = ScriptLocalization.rank_c;

        if (showType == ShowType.Aircraft)
        {
            lSkillName.text = HangarValue.SkillNames(AircraftSheet.Get((int)aircraftType).skill_rank_c);
            lDes.text = SkillInfoDescription.GetAircraftSkillDes(aircraftType, Rank.C);
        }
        else
        {
            lSkillName.text = HangarValue.SkillNames(WingSheet.Get((int)wingType).skill_rank_c);
            lDes.text = SkillInfoDescription.GetWingSkillDes(wingType, Rank.C);
        }

        panelSkillInfo.SetActive(true);
    }

    public void OnClickBtnSkillB()
    {
        sActiveIcon.transform.position = skillSprites[(int)Rank.B - 1].transform.position;
        sActiveIcon.spriteName = skillSprites[(int)Rank.B - 1].spriteName;

        circleAround.transform.position = new Vector2(sActiveIcon.transform.position.x, circleAround.transform.position.y);

        lRankInfo.text = ScriptLocalization.rank_b;

        if (showType == ShowType.Aircraft)
        {
            lSkillName.text = HangarValue.SkillNames(AircraftSheet.Get((int)aircraftType).skill_rank_b);
            lDes.text = SkillInfoDescription.GetAircraftSkillDes(aircraftType, Rank.B);
        }
        else
        {
            lSkillName.text = HangarValue.SkillNames(WingSheet.Get((int)wingType).skill_rank_b);
            lDes.text = SkillInfoDescription.GetWingSkillDes(wingType, Rank.B);
        }

        panelSkillInfo.SetActive(true);
    }

    public void OnClickBtnSkillA()
    {
        sActiveIcon.transform.position = skillSprites[(int)Rank.A - 1].transform.position;
        sActiveIcon.spriteName = skillSprites[(int)Rank.A - 1].spriteName;

        circleAround.transform.position = new Vector2(sActiveIcon.transform.position.x, circleAround.transform.position.y);

        lRankInfo.text = ScriptLocalization.rank_a;

        if (showType == ShowType.Aircraft)
        {
            lSkillName.text = HangarValue.SkillNames(AircraftSheet.Get((int)aircraftType).skill_rank_a);
            lDes.text = SkillInfoDescription.GetAircraftSkillDes(aircraftType, Rank.A);
        }
        else
        {
            lSkillName.text = HangarValue.SkillNames(WingSheet.Get((int)wingType).skill_rank_a);
            lDes.text = SkillInfoDescription.GetWingSkillDes(wingType, Rank.A);
        }

        panelSkillInfo.SetActive(true);
    }

    public void OnClickBtnSkillS()
    {
        sActiveIcon.transform.position = skillSprites[(int)Rank.S - 1].transform.position;
        sActiveIcon.spriteName = skillSprites[(int)Rank.S - 1].spriteName;

        circleAround.transform.position = new Vector2(sActiveIcon.transform.position.x, circleAround.transform.position.y);

        lRankInfo.text = ScriptLocalization.rank_s;

        if (showType == ShowType.Aircraft)
        {
            lSkillName.text = HangarValue.SkillNames(AircraftSheet.Get((int)aircraftType).skill_rank_s);
            lDes.text = SkillInfoDescription.GetAircraftSkillDes(aircraftType, Rank.S);
        }
        else
        {
            lSkillName.text = HangarValue.SkillNames(WingSheet.Get((int)wingType).skill_rank_s);
            lDes.text = SkillInfoDescription.GetWingSkillDes(wingType, Rank.S);
        }

        panelSkillInfo.SetActive(true);
    }

    public void OnClickBtnSkillSS()
    {
        sActiveIcon.transform.position = skillSprites[(int)Rank.SS - 1].transform.position;
        sActiveIcon.spriteName = skillSprites[(int)Rank.SS - 1].spriteName;

        circleAround.transform.position = new Vector2(sActiveIcon.transform.position.x, circleAround.transform.position.y);

        lRankInfo.text = ScriptLocalization.rank_ss;

        if (showType == ShowType.Aircraft)
        {
            lSkillName.text = HangarValue.SkillNames(AircraftSheet.Get((int)aircraftType).skill_rank_ss);
            lDes.text = SkillInfoDescription.GetAircraftSkillDes(aircraftType, Rank.SS);
        }
        else
        {
            lSkillName.text = HangarValue.SkillNames(WingSheet.Get((int)wingType).skill_rank_ss);
            lDes.text = SkillInfoDescription.GetWingSkillDes(wingType, Rank.SS);
        }

        panelSkillInfo.SetActive(true);
    }

    public void OnClickBtnSkillSSS()
    {
        sActiveIcon.transform.position = skillSprites[(int)Rank.SSS - 1].transform.position;
        sActiveIcon.spriteName = skillSprites[(int)Rank.SSS - 1].spriteName;

        circleAround.transform.position = new Vector2(sActiveIcon.transform.position.x, circleAround.transform.position.y);

        lRankInfo.text = ScriptLocalization.rank_sss;

        if (showType == ShowType.Aircraft)
        {
            lSkillName.text = HangarValue.SkillNames(AircraftSheet.Get((int)aircraftType).skill_rank_sss);
            lDes.text = SkillInfoDescription.GetAircraftSkillDes(aircraftType, Rank.SSS);
        }
        else
        {
            lSkillName.text = HangarValue.SkillNames(WingSheet.Get((int)wingType).skill_rank_sss);
            lDes.text = SkillInfoDescription.GetWingSkillDes(wingType, Rank.SSS);
        }

        panelSkillInfo.SetActive(true);
    }

    //Skill Wing
    public void LoadSkills(WingTypeEnum wingType, UISprite sUpgradeIcon)
    {
        this.wingType = wingType;
        int index = (int)wingType;
        showType = ShowType.Wing;

        sRankC.spriteName = "Skill" + WingSheet.Get(index).skill_rank_c;
        sRankB.spriteName = "Skill" + WingSheet.Get(index).skill_rank_b;
        sRankA.spriteName = "Skill" + WingSheet.Get(index).skill_rank_a;
        sRankS.spriteName = "Skill" + WingSheet.Get(index).skill_rank_s;
        sRankSS.spriteName = "Skill" + WingSheet.Get(index).skill_rank_ss;
        sRankSSS.spriteName = "Skill" + WingSheet.Get(index).skill_rank_sss;

        int indexRank = (int)CacheGame.GetWingRank(wingType);
        for (int i = 0; i < skillMasks.Length; i++)
        {
            skillMasks[i].gameObject.SetActive(i >= indexRank && i < (int)Constant.MAX_RANK_APPLY);
        }

        //Icon upgrade phải giống với icon skill 1
        sUpgradeIcon.spriteName = sRankC.spriteName;
    }

    public void ClosePopup()
    {
        panelSkillInfo.SetActive(false);
    }

    private void OnDisable()
    {
        panelSkillInfo.SetActive(false);
    }

}
