﻿using UnityEngine;
using com.ootii.Messages;

public class AircraftItem : BaseSelectItem
{
    [SerializeField]
    private AircraftTypeEnum aircraftType = AircraftTypeEnum.BataFD;
    public AircraftTypeEnum AircraftType { get { return aircraftType; } }

    private UISprite sIcon;
    private UISprite sRank;
    private UISprite sBG;
    private GameObject sLocked;
    private GameObject sFocus;

    private UIButton btnItem;
    private EventDelegate onClickItemCallback;

    private void Awake()
    {
        sIcon = MinhUtils.FindChild(transform, "sIcon").GetComponent<UISprite>();
        sRank = MinhUtils.FindChild(transform, "sRank").GetComponent<UISprite>();
        sBG = MinhUtils.FindChild(transform, "sBG").GetComponent<UISprite>();
        sLocked = MinhUtils.FindChild(transform, "sLocked").gameObject;
        sFocus = MinhUtils.FindChild(transform, "sLockedAndSelected").gameObject;

        btnItem = GetComponent<UIButton>();
        onClickItemCallback = new EventDelegate(OnClickItem);

        btnItem.onClick.Add(onClickItemCallback);

        LoadData();
    }

    private void Start()
    {
        UpdateRankUI();
    }

    private void OnDestroy()
    {
        btnItem.onClick.Remove(onClickItemCallback);
    }

    public void SetAsNormal()
    {
        stage = ItemStage.Normal;

        sBG.spriteName = HangarValue.HangarNormalBGName;
        sLocked.SetActive(false);
        sFocus.SetActive(false);
    }

    public void SetAsLocked()
    {
        stage = ItemStage.Locked;

        sBG.spriteName = HangarValue.HangarNormalBGName;
        sLocked.SetActive(true);
        sFocus.SetActive(false);

        if (aircraftType == AircraftTypeEnum.CandyPine || aircraftType == AircraftTypeEnum.RadiantStar)
            gameObject.SetActive(false);
    }

    //Được chọn để mang vào chơi
    public void SetAsSelected()
    {
        stage = ItemStage.Selected;

        sBG.spriteName = HangarValue.HangarSelectedBGName;
        sLocked.SetActive(false);
        sFocus.SetActive(false);
    }

    //Được focus để xem thông tin
    public void ShowFocusEffect()
    {
        sFocus.SetActive(true);
    }

    public void HideFocusEffect()
    {
        sFocus.SetActive(false);
    }

    //Khi click button item
    private void OnClickItem()
    {
        if (!sFocus.activeInHierarchy)
            MessageDispatcher.SendMessage(this, EventID.HANGAR_ITEM_CHANGED, this, 0);
    }

    private void LoadData()
    {
        rank = CacheGame.GetSpaceShipRank(aircraftType);
    }

    protected override void UpdateRankUI()
    {
        sIcon.spriteName = HangarValue.AircraftIconSpriteName(aircraftType, rank);
        sRank.spriteName = HangarValue.RankSpriteName[rank];
        Vector2 localScale = sIcon.transform.localScale;
        sIcon.MakePixelPerfect();
        sIcon.transform.localScale = localScale;

        sRank.MakePixelPerfect();

    }
}
