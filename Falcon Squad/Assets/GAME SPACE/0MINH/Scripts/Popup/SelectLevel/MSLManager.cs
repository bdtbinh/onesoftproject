﻿using System.Collections.Generic;
using UnityEngine;

public class MSLManager : MonoBehaviour
{

    private static MSLManager instance;
    public static MSLManager Instance
    {
        get { return instance; }
    }

    private int totalNormalStars = 0;
    private int totalHardStars = 0;
    private int totalHellStars = 0;

    private int totalNormalStarsExtraLevel = 0;
    private int totalHardStarsExtraLevel = 0;
    private int totalHellStarsExtralevel = 0;

    //[SerializeField]
    //private List<int> unlockStarsList;
    //public List<int> UnlockStarsList
    //{
    //    get { return unlockStarsList; }
    //}

    [SerializeField]
    private PopupUnlockLevel popupUnlockLevel;
    public PopupUnlockLevel PopupUnlockLevel
    {
        get { return popupUnlockLevel; }
    }

    [SerializeField]
    private UILabel lTotalStars;

    private int totalStars;
    public int TotalStars
    {
        get { return totalStars; }
    }

    [SerializeField]
    private UILabel lUnlockStars;

    private int unlockStars;
    public int UnlockStars
    {
        get { return unlockStars; }
    }

    private void Awake()
    {
        instance = this;

        for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL); i++)
        {
            for (int j = 1; j <= 3; j++)
            {
                if (CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal) == 1)
                {
                    totalNormalStars++;
                }
                if (CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Extra) == 1)
                {
                    totalNormalStarsExtraLevel++;

                }
            }
        }

        for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD); i++)
        {
            for (int j = 1; j <= 3; j++)
            {
                if (CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HARD, GameContext.LevelCampaignMode.Normal) == 1)
                {
                    totalHardStars++;
                }
                if (CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HARD, GameContext.LevelCampaignMode.Extra) == 1)
                {
                    totalHardStarsExtraLevel++;
                }
            }
        }

        for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL); i++)
        {
            for (int j = 1; j <= 3; j++)
            {
                if (CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HELL, GameContext.LevelCampaignMode.Normal) == 1)
                {
                    totalHellStars++;
                }
                if (CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HELL, GameContext.LevelCampaignMode.Extra) == 1)
                {
                    totalHellStarsExtralevel++;
                }
            }
        }

        //Level Extra:
        //for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL); i++)
        //{
        //    for (int j = 1; j <= 3; j++)
        //    {
        //        if (CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Extra) == 1)
        //        {
        //            totalNormalStarsExtraLevel++;
        //        }
        //    }
        //}

        //for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_HARD); i++)
        //{
        //    for (int j = 1; j <= 3; j++)
        //    {
        //        if (CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HARD, GameContext.LevelCampaignMode.Extra) == 1)
        //        {
        //            totalHardStarsExtraLevel++;
        //        }
        //    }
        //}

        //for (int i = 1; i <= CacheGame.GetMaxLevel(GameContext.DIFFICULT_HELL); i++)
        //{
        //    for (int j = 1; j <= 3; j++)
        //    {
        //        if (CacheGame.GetNumStarLevel(i, j, GameContext.DIFFICULT_HELL, GameContext.LevelCampaignMode.Extra) == 1)
        //        {
        //            totalHellStarsExtralevel++;
        //        }
        //    }
        //}

        UpdateUnlockStarsUI();
    }

    void UpdateUnlockStarsUI()
    {
        if (CacheGame.GetDifficultCampaign() == GameContext.DIFFICULT_NOMAL)
        {
            totalStars = totalNormalStars + totalNormalStarsExtraLevel;
        }
        else if (CacheGame.GetDifficultCampaign() == GameContext.DIFFICULT_HARD)
        {
            totalStars = totalHardStars + totalHardStarsExtraLevel;
        }
        else if (CacheGame.GetDifficultCampaign() == GameContext.DIFFICULT_HELL)
        {
            totalStars = totalHellStars + totalHellStarsExtralevel;
        }

        //int maxLevel = CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign());
        //if (maxLevel % 7 == 0 && MinhCacheGame.IsAlreadyUnlockLevel(maxLevel, CacheGame.GetDifficultCampaign()))
        //{
        //    if(CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) / 7 < unlockStarsList.Count)
        //        unlockStars = unlockStarsList[CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) / 7];
        //    else
        //        unlockStars = unlockStarsList[CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) / 7 - 1];
        //}
        //else
        //{
        //    unlockStars = unlockStarsList[Mathf.CeilToInt(CacheGame.GetMaxLevel(CacheGame.GetDifficultCampaign()) / 7.0f) - 1];
        //}

        lTotalStars.text = totalStars + "";
        lUnlockStars.text = "/" + unlockStars;

        //if (totalStars < unlockStars)
        //{
        //    lTotalStars.color = Color.red;
        //}
        //else
        //{
        //    lTotalStars.color = Color.green;
        //}
    }

    public void OnChangeDifficultMode()
    {
        UpdateUnlockStarsUI();
    }

    public void ShowUnlockLevelEffect()
    {
        UpdateUnlockStarsUI();
    }
}
