﻿using I2.Loc;
using System.Collections;

using System.Collections.Generic;
using UnityEngine;
using System;
using com.ootii.Messages;

public class PopupStarChest : MonoBehaviour
{
    [SerializeField]
    private UILabel lStarChest;

    //Star sprites
    [SerializeField]
    private UISprite[] sStars;

    [SerializeField]
    private UISprite sChest;

    [SerializeField]
    private UILabel lYouHaveNumber;

    [SerializeField]
    private UILabel lStarRequireNum;

    [SerializeField]
    private UILabel lLevel;

    [SerializeField]
    private UILabel lClaim;

    [SerializeField]
    private UISprite sBtnClaim;

    [SerializeField]
    private GameObject sGlow;

    [SerializeField]
    private ElementItemInfo[] elementInfos;

    private enum PopupType
    {
        Normal,
        Extra
    }

    private PopupType popupType;

    private int currentWorld = 0;
    private int indexChest;
    private int yourStars;

    private List<Tuple<string, int>> tupleRewards = new List<Tuple<string, int>>();

    public void ShowPopup(int currentWorld, int indexChest, int yourWorldStars, string starSpriteName, string chestSpriteName)
    {
        popupType = PopupType.Normal;
        this.currentWorld = currentWorld;
        this.indexChest = indexChest;
        yourStars = yourWorldStars;

        lStarChest.text = ScriptLocalization.title_star_chest;
        lYouHaveNumber.text = yourWorldStars.ToString();
        lStarRequireNum.text = Constant.STAR_UNLOCK_REQUIRE[indexChest - 1].ToString();
        lLevel.text = ScriptLocalization.level + ": " + (currentWorld * 7 - 6) + "-" + currentWorld * 7;

        //Đổi màu sao
        for (int i = 0; i < sStars.Length; i++)
        {
            sStars[i].spriteName = starSpriteName;
        }

        //Đổi ảnh của chest
        sChest.spriteName = chestSpriteName;

        UpdateBtnClaimUI();
        LoadData();

        gameObject.SetActive(true);
    }

    public void ShowPopupExtra(int currentWorld, int yourExtraStars, string starSpriteName, string chestSpriteName)
    {
        popupType = PopupType.Extra;
        this.currentWorld = currentWorld;
        yourStars = yourExtraStars;

        lStarChest.text = ScriptLocalization.title_star_chest_extra;
        lYouHaveNumber.text = yourExtraStars.ToString();
        lStarRequireNum.text = Constant.EXTRA_STAR_UNLOCK_REQUIRE.ToString();
        lLevel.text = ScriptLocalization.level + ": " + (currentWorld * 7 - 6) + "-" + currentWorld * 7;

        //Đổi màu sao
        for (int i = 0; i < sStars.Length; i++)
        {
            sStars[i].spriteName = starSpriteName;
        }

        //Đổi ảnh của chest
        sChest.spriteName = chestSpriteName;

        UpdateBtnClaimUIExtra();
        LoadData();

        gameObject.SetActive(true);
    }

    private void UpdateBtnClaimUI()
    {
        sGlow.SetActive(false);

        if (MinhCacheGame.IsStarChestClaimed(currentWorld, CacheGame.GetDifficultCampaign(), indexChest))
        {
            lClaim.text = ScriptLocalization.claimed;
            sBtnClaim.spriteName = SpriteNameConst.BG_BUTTON_CLAIM_DISABLE_1;
        }
        else
        {
            lClaim.text = ScriptLocalization.claim;
            sBtnClaim.spriteName = SpriteNameConst.BG_BUTTON_CLAIM_ENABLE_1;

            sGlow.SetActive(yourStars >= Constant.STAR_UNLOCK_REQUIRE[indexChest - 1]);
        }
    }

    private void UpdateBtnClaimUIExtra()
    {
        sGlow.SetActive(false);

        if(MinhCacheGame.IsStarChestExtraClaimed(currentWorld, CacheGame.GetDifficultCampaign()))
        {
            lClaim.text = ScriptLocalization.claimed;
            sBtnClaim.spriteName = SpriteNameConst.BG_BUTTON_CLAIM_DISABLE_1;
        }
        else
        {
            lClaim.text = ScriptLocalization.claim;
            sBtnClaim.spriteName = SpriteNameConst.BG_BUTTON_CLAIM_ENABLE_1;

            sGlow.SetActive(yourStars >= Constant.EXTRA_STAR_UNLOCK_REQUIRE);
        }
    }

    public void LoadData()
    {
        tupleRewards.Clear();
        string key = CacheGame.GetDifficultCampaign() + "_World" + currentWorld + "_" + (popupType == PopupType.Extra ? "Extra" : indexChest.ToString());

        if (!string.IsNullOrEmpty(StarChestSheet.Get(key).TypeGift1))
        {
            tupleRewards.Add(new Tuple<string, int>(StarChestSheet.Get(key).TypeGift1, StarChestSheet.Get(key).ValueGift1));

            elementInfos[0].gameObject.SetActive(true);
            elementInfos[0].CreateItem(StarChestSheet.Get(key).TypeGift1, StarChestSheet.Get(key).ValueGift1, false);
        }
        else
        {
            elementInfos[0].gameObject.SetActive(false);
        }

        if (!string.IsNullOrEmpty(StarChestSheet.Get(key).TypeGift2))
        {
            tupleRewards.Add(new Tuple<string, int>(StarChestSheet.Get(key).TypeGift2, StarChestSheet.Get(key).ValueGift2));

            elementInfos[1].gameObject.SetActive(true);
            elementInfos[1].CreateItem(StarChestSheet.Get(key).TypeGift2, StarChestSheet.Get(key).ValueGift2, false);
        }
        else
        {
            elementInfos[1].gameObject.SetActive(false);
        }

        if (!string.IsNullOrEmpty(StarChestSheet.Get(key).TypeGift3))
        {
            tupleRewards.Add(new Tuple<string, int>(StarChestSheet.Get(key).TypeGift3, StarChestSheet.Get(key).ValueGift3));

            elementInfos[2].gameObject.SetActive(true);
            elementInfos[2].CreateItem(StarChestSheet.Get(key).TypeGift3, StarChestSheet.Get(key).ValueGift3, false);
        }
        else
        {
            elementInfos[2].gameObject.SetActive(false);
        }
    }

    public void OnClickBtnClaim()
    {
        if(popupType == PopupType.Normal)
        {
            ClaimNormal();
        }
        else
        {
            ClaimExtra();
        }
    }

    private void ClaimNormal()
    {
        string difficult = CacheGame.GetDifficultCampaign();

        if (MinhCacheGame.IsStarChestClaimed(currentWorld, difficult, indexChest))
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.notifi_claimed_reward, false, 1.5f);
            return;
        }

        if (Constant.STAR_UNLOCK_REQUIRE[indexChest - 1] > yourStars)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_Achi_PleaseComplete, false, 1.5f);
            return;
        }

        MinhCacheGame.SetStarChestClaimed(currentWorld, difficult, indexChest);
        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(tupleRewards.Count);

        for (int i = 0; i < tupleRewards.Count; i++)
        {
            GameContext.AddGiftToPlayer(tupleRewards[i].Item1, tupleRewards[i].Item2, true, FirebaseLogSpaceWar.StarChest_Why, "World: " + currentWorld + ", " + difficult);
            popupItemRewardIns.ShowOneItem(i + 1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);
        }

        lClaim.text = ScriptLocalization.claimed;
        sBtnClaim.spriteName = SpriteNameConst.BG_BUTTON_CLAIM_DISABLE_1;
        sGlow.SetActive(false);

        MessageDispatcher.SendMessage(this, EventID.ON_CLAIM_STAR_CHEST_REWARD, indexChest, 0f);
    }

    private void ClaimExtra()
    {
        string difficult = CacheGame.GetDifficultCampaign();

        if (MinhCacheGame.IsStarChestExtraClaimed(currentWorld, difficult))
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.notifi_claimed_reward, false, 1.5f);
            return;
        }

        if (Constant.EXTRA_STAR_UNLOCK_REQUIRE > yourStars)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_Achi_PleaseComplete, false, 1.5f);
            return;
        }

        MinhCacheGame.SetStarChestExtraClaimed(currentWorld, difficult);
        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(tupleRewards.Count);

        for (int i = 0; i < tupleRewards.Count; i++)
        {
            GameContext.AddGiftToPlayer(tupleRewards[i].Item1, tupleRewards[i].Item2, true, FirebaseLogSpaceWar.StarChest_Why, "World: " + currentWorld + ", " + difficult);
            popupItemRewardIns.ShowOneItem(i + 1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);
        }

        lClaim.text = ScriptLocalization.claimed;
        sBtnClaim.spriteName = SpriteNameConst.BG_BUTTON_CLAIM_DISABLE_1;
        sGlow.SetActive(false);

        MessageDispatcher.SendMessage(this, EventID.ON_CLAIM_STAR_CHEST_REWARD_EXTRA, indexChest, 0f);
    }

    public void OnClickOutSidePopUp()
    {
        gameObject.SetActive(false);
    }
}
