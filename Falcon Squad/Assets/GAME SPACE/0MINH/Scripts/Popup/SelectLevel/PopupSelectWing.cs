﻿using UnityEngine;
using System.Collections.Generic;
using com.ootii.Messages;
using TCore;
using EasyMobile;
using Sirenix.OdinInspector;
using I2.Loc;

public class PopupSelectWing : MonoBehaviour
{

    public GameObject fxTutorialUpgrade;
    private const float BUTTON_GOLD_LEFT_X = -200f;
    private const float BUTTON_GEM_RIGHT_X = 200f;

    private const string BUTTON_UPGRADE_DISABLE_SPRITE = "btn_upgrade_item";
    private const string BUTTON_UPGRADE_ENABLE_SPRITE = "btn_upgrade_item_t";

    public static SelectDeviceHandler.OpenSelectFromType openSelectFromType;

    [SerializeField]
    private PanelCongratulations panelCongratulations;

    [SerializeField]
    private PanelEvolve panelEvolve;

    [SerializeField]
    private UILabel lWingName;

    [SerializeField]
    private UISprite sRankTitle;

    [SerializeField]
    private UILabel lDescription;

    [SerializeField]
    private WingAnimations wingAnimations;

    //Progress bar region
    [TitleGroup("Progress")]
    [SerializeField]
    private UIButton btnCard;

    [SerializeField]
    private UISprite sGreenProgress;

    [SerializeField]
    private UISprite sBlueProgress;

    [SerializeField]
    private UILabel lCardNumber;

    [SerializeField]
    private GameObject btnEvolve;

    //Button buy, unlock:
    [TitleGroup("Unlock or Buy")]
    [SerializeField]
    private UILabel lUnlockMessage;

    [SerializeField]
    private GameObject btnUnlock;

    [SerializeField]
    private GameObject btnBuyGold;

    [SerializeField]
    private UILabel lGoldPrice;

    [SerializeField]
    private GameObject btnBuyGem;

    [SerializeField]
    private UILabel lGemPrice;

    [SerializeField]
    private GameObject btnPurchase;

    [SerializeField]
    private UILabel lPurchasePrice;

    //Upgradation
    [TitleGroup("Upgradations")]

    [SerializeField]
    private UISprite sUpgradeSkillIcon;

    [SerializeField]
    private UILabel lLevel;

    [SerializeField]
    private GameObject[] levelBars;

    [SerializeField]
    private UIButton btnUpgrade;

    [SerializeField]
    private UILabel lUpgradeNumber;

    [SerializeField]
    private UISprite sUpgradeIcon;  //Icon button upgrade

    [TitleGroup("List wing items")]
    [SerializeField]
    private WingItem[] wingItems;

    [TitleGroup("Equip, Unequip, Try buttons")]
    [SerializeField]
    private GameObject btnEquip;

    [SerializeField]
    private GameObject btnUnequip;

    [SerializeField]
    private GameObject btnTry;

    [SerializeField]
    private PanelSkillUIManager panelSkill;

    [SerializeField]
    private GameObject sEvolveRedDot;           //Show khi có thể elvolve được wing

    [SerializeField]
    private GameObject sUpgradeRedDot;          //Show khi có thể upgrade được wing

    private Vector3[] itemLocalPositions;

    //Item được select (k phải item được chọn mang vào chơi)
    private WingItem focusItem;

    //Item mang vào chơi
    private WingItem selectedItem;

    //Cached fields
    private int currentLevel;
    private int maxLevel;
    private int upgradeGoldCost;
    private int upgradeGemCost;
    private int upgradeEnergyCost;
    private int specificCards;
    private int generalCards;
    private int evolveCards;
    private int unlockCards;

    private bool needToLoadFromTry;
    public bool NeedToLoadFromTry
    {
        set
        {
            needToLoadFromTry = value;
        }
    }

    private void Awake()
    {
        itemLocalPositions = new Vector3[wingItems.Length];

        for (int i = 0; i < itemLocalPositions.Length; i++)
        {
            itemLocalPositions[i] = wingItems[i].transform.localPosition;
        }
    }

    ////OnEnable có trường hợp chạy trước awake của object HangarSelectItem nên phải thêm biến này để lần đầu tiên sẽ chạy UpdateItemStatus trong start. 
    ////Tránh trường hợp có biến của HangarSelectItem chưa được khởi tạo mà đã truy cập.
    bool isStarted;
    private void Start()
    {
        isStarted = true;
        InitItemsStatus();
    }

    private void OnRewardSuccessed(IMessage msg)
    {
        UpdateUnlockUI();
    }

    private void OnEnable()
    {
        PanelCoinGem.Instance.ShowEnergyButton();

        if (PopupManagerCuong.Instance.IsActivePanelBtnVideoTop())
        {
            PopupManagerCuong.Instance.HidePanelBtnVideoTop();
        }

        MessageDispatcher.AddListener(EventID.ON_REWARD_SUCCESSED, OnRewardSuccessed, true);
        MessageDispatcher.AddListener(EventID.HANGAR_ITEM_CHANGED, OnClickItem, true);
        MessageDispatcher.AddListener(EventID.ON_EVOLVE_SUCCESSED, OnEvolveSuccessed, true);

        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;

        if (!isStarted)
            return;

        InitItemsStatus();

    }

    private void OnEvolveSuccessed(IMessage msg)
    {
        focusItem.SetRank(CacheGame.GetWingRank(focusItem.WingType));
        UpdateInfo();
        UpdateUnlockUI();
    }

    private void OnDisable()
    {
        PanelCoinGem.Instance.HideEnergyButton();
        PopupManagerCuong.Instance.ShowPanelBtnVideoTop();

        MessageDispatcher.RemoveListener(EventID.ON_REWARD_SUCCESSED, OnRewardSuccessed, true);
        MessageDispatcher.RemoveListener(EventID.HANGAR_ITEM_CHANGED, OnClickItem, true);
        MessageDispatcher.RemoveListener(EventID.ON_EVOLVE_SUCCESSED, OnEvolveSuccessed, true);

        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;

        MessageDispatcher.SendMessage(EventID.SELECT_DEVICE_CHANGE_UI, 0f);


        //GameContext.typeOpenSelectItemFrom = GameContext.TypeOpenSelectItemFrom.None;

        panelEvolve.gameObject.SetActive(false);

    }

    List<WingItem> ordinalPosList = new List<WingItem>();

    public void ShowPopUp(SelectDeviceHandler.OpenSelectFromType fromType, bool needToLoadFromTry)
    {
        openSelectFromType = fromType;
        this.needToLoadFromTry = needToLoadFromTry;
        gameObject.SetActive(true);
    }

    private void InitItemsStatus()
    {
        WingTypeEnum selectedType = (WingTypeEnum)CacheGame.GetWingUserSelected();

        for (int i = 0; i < wingItems.Length; i++)
        {
            if (wingItems[i].WingType == selectedType)                                        //Item được chọn
            {
                selectedItem = wingItems[i];
                wingItems[i].SetAsSelected();
                wingItems[i].ShowFocusEffect();
                focusItem = wingItems[i];
                //UpdateInfo();
                //UpdateUnlockUI();
                ordinalPosList.Insert(0, wingItems[i]);
            }
            else if (CacheGame.GetWingIsUnlocked((int)wingItems[i].WingType) == 1)        //Item đã unlock
            {
                wingItems[i].SetAsNormal();

                if (wingItems[i].gameObject.activeInHierarchy)
                {
                    ordinalPosList.Add(wingItems[i]);
                }
            }
        }

        //Item chưa unlock
        for (int i = 0; i < wingItems.Length; i++)
        {
            if (CacheGame.GetWingIsUnlocked((int)wingItems[i].WingType) != 1)
            {
                wingItems[i].SetAsLocked();

                if (wingItems[i].gameObject.activeInHierarchy)
                {
                    ordinalPosList.Add(wingItems[i]);
                }
            }
        }

        if (needToLoadFromTry)
        {
            if (focusItem != null)
                focusItem.HideFocusEffect();

            focusItem = wingItems[CacheGame.GetWingUserTry() - 1];
            focusItem.ShowFocusEffect();

            needToLoadFromTry = false;
        }

        for (int i = 0; i < ordinalPosList.Count; i++)
        {
            ordinalPosList[i].transform.localPosition = itemLocalPositions[i];
        }

        if (focusItem == null)
        {
            focusItem = wingItems[0];
            focusItem.ShowFocusEffect();
        }

        ordinalPosList.Clear();

        UpdateInfo();
        UpdateUnlockUI();
        UpdateButtomButtons();
    }

    #region update UI

    private void UpdateInfo()
    {
        lPurchasePrice.text = CuongUtils.GetIapLocalizedPrice(WingSheet.Get((int)focusItem.WingType).purchase_id);

        lWingName.text = HangarValue.WingNames(focusItem.WingType);
        sRankTitle.spriteName = HangarValue.RankSpriteName[focusItem.Rank];
        lDescription.text = HangarValue.WingDescriptions(focusItem.WingType);

        wingAnimations.PlayAnimations(focusItem.WingType, focusItem.Rank);

        panelSkill.LoadSkills(focusItem.WingType, sUpgradeSkillIcon);
    }

    private void UpdateUnlockUI()
    {
        bool isUnlocked = CacheGame.GetWingIsUnlocked((int)focusItem.WingType) == 1;

        //btnCard.transform.parent.gameObject.SetActive(false);
        btnEvolve.SetActive(false);

        lUnlockMessage.gameObject.SetActive(false);
        btnUnlock.SetActive(false);
        btnBuyGold.SetActive(false);
        btnBuyGem.SetActive(false);

        btnUpgrade.transform.parent.gameObject.SetActive(false);

        specificCards = MinhCacheGame.GetWingCards(focusItem.WingType);
        generalCards = MinhCacheGame.GetWingGeneralCards();

        if (isUnlocked)
        {
            btnCard.normalSprite = "card_0";
            bool isMaxRank = (int)focusItem.Rank >= (int)Constant.MAX_RANK_APPLY;

            if (isMaxRank)
            {
                lCardNumber.text = ScriptLocalization.label_max;
                sBlueProgress.fillAmount = 1f;
            }
            else
            {
                //Load card data
                evolveCards = RankSheet.Get((int)focusItem.Rank + 1).card_evolve;

                int totalCards = specificCards + generalCards;
                lCardNumber.text = totalCards + "/" + evolveCards;

                sBlueProgress.fillAmount = specificCards * 1.0f / evolveCards;
                sGreenProgress.fillAmount = totalCards * 1.0f / evolveCards;
                btnEvolve.SetActive(true);         //Chưa xax rank thì bật nút evolve
                CheckShowEvolveRedDot();
            }

            btnUpgrade.transform.parent.gameObject.SetActive(true);

            currentLevel = CacheGame.GetWingLevel(focusItem.WingType);
            maxLevel = RankSheet.Get((int)focusItem.Rank).max_level;

            lLevel.text = I2.Loc.ScriptLocalization.level_upgrade + ": " + currentLevel + "/" + maxLevel;
            SetUpgradeBars(CacheGame.GetWingLevel(focusItem.WingType));
            SetUpgradePrice();
        }
        else
        {
            sEvolveRedDot.SetActive(false);
            int levelUnlock = WingSheet.Get((int)focusItem.WingType).level_unlock;
            unlockCards = WingSheet.Get((int)focusItem.WingType).card_unlock;

            if (unlockCards > 0)                                    //Unlock bằng card
            {
                btnCard.normalSprite = "card_W" + (int)focusItem.WingType;
                lCardNumber.text = specificCards + "/" + unlockCards;

                sGreenProgress.fillAmount = 0f;

                if (MinhCacheGame.GetWingCards(focusItem.WingType) < unlockCards)
                {
                    lUnlockMessage.gameObject.SetActive(true);
                    lUnlockMessage.text = I2.Loc.ScriptLocalization.msg_unlock_by_card;

                    sBlueProgress.fillAmount = MinhCacheGame.GetWingCards(focusItem.WingType) * 1.0f / unlockCards;
                }
                else
                {
                    sBlueProgress.fillAmount = 1f;
                    btnUnlock.SetActive(true);
                }
            }
            else if (levelUnlock > 0)                               //Unlock khi đủ level
            {
                btnCard.normalSprite = "card_0";

                bool isMaxRank = (int)focusItem.Rank >= (int)Constant.MAX_RANK_APPLY;

                if (isMaxRank)
                {
                    lCardNumber.text = lCardNumber.text = ScriptLocalization.label_max;
                    sBlueProgress.fillAmount = 1f;
                }
                else
                {
                    //Load card data
                    evolveCards = RankSheet.Get((int)focusItem.Rank + 1).card_evolve;

                    int totalCards = specificCards + generalCards;
                    lCardNumber.text = totalCards + "/" + evolveCards;

                    sBlueProgress.fillAmount = specificCards * 1.0f / evolveCards;
                    sGreenProgress.fillAmount = totalCards * 1.0f / evolveCards;
                    btnEvolve.SetActive(true);         //Chưa xax rank thì bật nút evolve
                }

                if (CacheGame.GetMaxLevel3Difficult() < levelUnlock)
                {
                    lUnlockMessage.gameObject.SetActive(true);
                    string msg = I2.Loc.ScriptLocalization.msg_unlock_level_to_purchase;
                    lUnlockMessage.text = msg.Replace("%{level_number}", levelUnlock.ToString());
                }
                else
                {
                    btnBuyGold.transform.parent.gameObject.SetActive(true);

                    int goldUnlock = WingSheet.Get((int)focusItem.WingType).gold_unlock;
                    int gemUnlock = WingSheet.Get((int)focusItem.WingType).gem_unlock;

                    lGoldPrice.text = GameContext.FormatNumber(goldUnlock);
                    lGemPrice.text = GameContext.FormatNumber(gemUnlock);

                    if (goldUnlock > 0 && gemUnlock > 0)
                    {
                        btnBuyGold.SetActive(true);
                        btnBuyGem.SetActive(true);

                        btnBuyGold.transform.localPosition = new Vector3(BUTTON_GOLD_LEFT_X, btnBuyGold.transform.localPosition.y, btnBuyGold.transform.localPosition.z);
                        btnBuyGem.transform.localPosition = new Vector3(BUTTON_GEM_RIGHT_X, btnBuyGem.transform.localPosition.y, btnBuyGem.transform.localPosition.z);
                    }
                    else if (goldUnlock > 0)
                    {
                        btnBuyGold.SetActive(true);
                        btnBuyGold.transform.localPosition = new Vector3(0, btnBuyGold.transform.localPosition.y, btnBuyGold.transform.localPosition.z);
                    }
                    else
                    {
                        btnBuyGem.SetActive(true);
                        btnBuyGem.transform.localPosition = new Vector3(0, btnBuyGem.transform.localPosition.y, btnBuyGem.transform.localPosition.z);
                    }
                }
            }
        }
    }

    private void UpdateButtomButtons()
    {
        btnEquip.SetActive(false);
        btnUnequip.SetActive(false);
        btnTry.SetActive(false);
        btnPurchase.SetActive(false);

        bool isUnlocked = CacheGame.GetWingIsUnlocked((int)focusItem.WingType) == 1;

        if (isUnlocked)
        {
            if ((int)focusItem.WingType != CacheGame.GetWingUserSelected())
            {
                btnEquip.SetActive(true);
            }
            else
            {
                btnUnequip.SetActive(true);
            }
        }
        else
        {
            btnTry.SetActive(true);

            if (CacheGame.GetMaxLevel3Difficult() >= 3 && !string.IsNullOrEmpty(WingSheet.Get((int)focusItem.WingType).purchase_id))
            {
                btnPurchase.SetActive(true);
            }
        }
    }

    private void SetUpgradeBars(int level)
    {
        int remainder = level % levelBars.Length;

        if (level == 0)
        {
            remainder = 0;
        }
        else
        {
            if (remainder == 0)
                remainder = 15;
        }

        for (int i = 0; i < levelBars.Length; i++)
        {
            levelBars[i].SetActive(i < remainder);
        }
    }

    private void SetUpgradePrice()
    {
        if (currentLevel >= RankSheet.Get((int)Constant.MAX_RANK_APPLY).max_level)
        {
            btnUpgrade.gameObject.SetActive(false);
        }
        else
        {
            btnUpgrade.gameObject.SetActive(true);

            upgradeGoldCost = WingUpgradeCostSheet.Get(currentLevel + 1).gold;
            upgradeGemCost = WingUpgradeCostSheet.Get(currentLevel + 1).gem;
            upgradeEnergyCost = WingUpgradeCostSheet.Get(currentLevel + 1).energy;

            if (upgradeGoldCost > 0)
            {
                lUpgradeNumber.text = upgradeGoldCost.ToString();
                sUpgradeIcon.spriteName = HangarValue.BuyResourceSpriteName[BuyType.Gold];
            }
            else if (upgradeGemCost > 0)
            {
                lUpgradeNumber.text = upgradeGemCost.ToString();
                sUpgradeIcon.spriteName = HangarValue.BuyResourceSpriteName[BuyType.Gem];
            }
            else
            {
                lUpgradeNumber.text = upgradeEnergyCost.ToString();
                sUpgradeIcon.spriteName = HangarValue.BuyResourceSpriteName[BuyType.Energy];
            }

            if (currentLevel < maxLevel)
            {
                btnUpgrade.normalSprite = BUTTON_UPGRADE_ENABLE_SPRITE;
            }
            else
            {
                btnUpgrade.normalSprite = BUTTON_UPGRADE_DISABLE_SPRITE;
            }

            if (currentLevel < maxLevel)
            {
                if (upgradeGoldCost > 0)
                {
                    if (CacheGame.GetTotalCoin() >= upgradeGoldCost && CacheGame.GetMaxLevel3Difficult() > 0 && CacheGame.GetMaxLevel3Difficult() < 4)
                    {
                        fxTutorialUpgrade.SetActive(true);
                    }
                    else
                    {
                        fxTutorialUpgrade.SetActive(false);
                    }
                }
                else if (upgradeGemCost > 0)
                {
                    if (CacheGame.GetTotalGem() >= upgradeGemCost && CacheGame.GetMaxLevel3Difficult() > 0 && CacheGame.GetMaxLevel3Difficult() < 4)
                    {
                        fxTutorialUpgrade.SetActive(true);
                    }
                    else
                    {
                        fxTutorialUpgrade.SetActive(false);
                    }
                }
                else
                {
                    if (CacheGame.GetTotalEnergy() >= upgradeEnergyCost && CacheGame.GetMaxLevel3Difficult() > 0 && CacheGame.GetMaxLevel3Difficult() < 4)
                    {
                        fxTutorialUpgrade.SetActive(true);
                    }
                    else
                    {
                        fxTutorialUpgrade.SetActive(false);
                    }
                }
            }

            CheckShowUpgradeRedDot();
        }
    }

    #endregion



    private void IncreaseLevel(ResourceType type)
    {
        currentLevel++;
        CacheGame.SetWingLevel(focusItem.WingType, currentLevel);
        CacheGame.SetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetUpgrade.ToString(), CacheGame.GetNumberAchievementAchieved(GameContext.TypeItemAchievements.GetUpgrade.ToString()) + 1);

        if (type == ResourceType.Gold)
        {
            PanelCoinGem.Instance.AddCoinTop(-upgradeGoldCost, FirebaseLogSpaceWar.UpgradeWing_why, focusItem.WingType.ToString());
        }

        if (type == ResourceType.Gem)
        {
            PanelCoinGem.Instance.AddGemTop(-upgradeGemCost, FirebaseLogSpaceWar.UpgradeWing_why, focusItem.WingType.ToString());
        }

        if (type == ResourceType.Energy)
        {
            PanelCoinGem.Instance.AddEnergyTop(-upgradeEnergyCost, FirebaseLogSpaceWar.UpgradeWing_why, focusItem.WingType.ToString());
        }

        lLevel.text = I2.Loc.ScriptLocalization.level_upgrade + ": " + currentLevel + "/" + maxLevel;
        SetUpgradeBars(currentLevel);
        SetUpgradePrice();

        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_shop_upgraded.Replace("%{level_number}", currentLevel.ToString()), true, 1f);
    }

    private void CheckShowEvolveRedDot()
    {
        int evolveCost = RankSheet.Get((int)focusItem.Rank + 1).gem_evolve;

        //Debug.LogError(CacheGame.GetTotalGem() + " * " + evolveCost + " **** " + specificCards + " * " + generalCards + " * " + evolveCards);

        if (CacheGame.GetTotalGem() >= evolveCost && specificCards + generalCards >= evolveCards)
        {
            //Debug.LogError("Show Red Dot");
            sEvolveRedDot.SetActive(true);
        }
        else
        {
            sEvolveRedDot.SetActive(false);
        }
    }

    private void CheckShowUpgradeRedDot()
    {
        if (currentLevel >= maxLevel) return;

        if (upgradeGoldCost > 0)
        {
            if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
            {
                sUpgradeRedDot.SetActive(true);
            }
            else
            {
                sUpgradeRedDot.SetActive(false);
            }
        }
        else if (upgradeGemCost > 0)
        {
            if (CacheGame.GetTotalGem() >= upgradeGemCost)
            {
                sUpgradeRedDot.SetActive(true);
            }
            else
            {
                sUpgradeRedDot.SetActive(false);
            }
        }
        else
        {
            if (CacheGame.GetTotalEnergy() >= upgradeEnergyCost)
            {
                sUpgradeRedDot.SetActive(true);
            }
            else
            {
                sUpgradeRedDot.SetActive(false);
            }
        }
    }


    void LoadLevelTryPlane()
    {
        SoundManager.PlayClickButton();

        //OsAdsManager.Instance.ShowInterstitialAds();
        LoadingSceneManager.Instance.LoadSceneMainUI("LevelTryPlane");

    }

    void OnPurchaseSuccessed(string productName)
    {
        if (productName == EM_IAPConstants.Product_unlock_wing_1 || productName == EM_IAPConstants.Product_unlock_wing_2 || productName == EM_IAPConstants.Product_unlock_wing_3)
        {
            CacheGame.SetWingIsUnlocked((int)focusItem.WingType, 1);
            CacheGame.SetWingUserSelected((int)focusItem.WingType);

            ChangeFocusItemToSelected();
            UpdateUnlockUI();
            UpdateButtomButtons();

            PopupManagerCuong.Instance.ShowTextNotifiToast(HangarValue.GetUnlockDeviceNotify(HangarValue.WingNames(focusItem.WingType)), true, 1.88f, 0.56f);
        }
    }

    public void OnClickItem(IMessage msg)
    {
        SoundManager.PlayClickButton();

        if (focusItem != null)
            focusItem.HideFocusEffect();

        focusItem = (WingItem)msg.Data;
        focusItem.ShowFocusEffect();

        UpdateInfo();
        UpdateUnlockUI();
        UpdateButtomButtons();
    }

    public void OnClickBtnEvolve()
    {
        SoundManager.PlayClickButton();
        panelEvolve.ShowWingEvolvePanel(focusItem.WingType, (Rank)((int)focusItem.Rank + 1));
    }

    public void OnClickBtnUpgrade()
    {
        if (currentLevel < maxLevel)
        {
            if (upgradeGoldCost > 0)
            {
                if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
                {
                    IncreaseLevel(ResourceType.Gold);
                }
                else
                {
                    PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
                }
            }
            else if (upgradeGemCost > 0)
            {
                if (CacheGame.GetTotalGem() >= upgradeGemCost)
                {
                    IncreaseLevel(ResourceType.Gem);
                }
                else
                {
                    PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
                }
            }
            else
            {
                if (CacheGame.GetTotalEnergy() >= upgradeEnergyCost)
                {
                    IncreaseLevel(ResourceType.Energy);
                }
                else
                {
                    //PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Energy);
                    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_resources, false, 1.5f);
                }
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_need_evolve_wing, false, 1.5f);
        }


    }

    public void OnClickBtnPurchase()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(WingSheet.Get((int)focusItem.WingType).purchase_id, IAPCallbackManager.WherePurchase.shop);
    }

    public void OnClickBtnUnlock()
    {
        SoundManager.PlayClickButton();

        ChangeFocusItemToSelected();
        CacheGame.SetWingIsUnlocked((int)focusItem.WingType, 1);
        CacheGame.SetWingUserSelected((int)focusItem.WingType);
        MinhCacheGame.SetWingCards(focusItem.WingType, MinhCacheGame.GetWingCards(focusItem.WingType) - unlockCards, FirebaseLogSpaceWar.PurchaseWing_why, focusItem.WingType.ToString(), focusItem.WingType.ToString());

        UpdateUnlockUI();
        UpdateButtomButtons();

        panelCongratulations.ShowPanelWing(focusItem.WingType, focusItem.Rank, PanelCongratulations.CongraType.NewItem);
    }

    public void OnClickBtnBuyGem()
    {
        SoundManager.PlayClickButton();
        int gemUnlock = WingSheet.Get((int)focusItem.WingType).gem_unlock;

        if (CacheGame.GetTotalGem() >= gemUnlock)
        {
            ChangeFocusItemToSelected();

            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.AddGemTop(-gemUnlock, FirebaseLogSpaceWar.PurchaseWing_why, focusItem.WingType.ToString());
            }

            CacheGame.SetWingIsUnlocked((int)focusItem.WingType, 1);

            CacheGame.SetWingUserSelected((int)focusItem.WingType);

            UpdateUnlockUI();
            UpdateButtomButtons();

            panelCongratulations.ShowPanelWing(focusItem.WingType, focusItem.Rank, PanelCongratulations.CongraType.NewItem);
        }
        else
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
        }
    }

    public void OnClickBtnBuyGold()
    {
        SoundManager.PlayClickButton();

        int goldUnlock = WingSheet.Get((int)focusItem.WingType).gold_unlock;

        if (CacheGame.GetTotalCoin() >= goldUnlock)
        {
            ChangeFocusItemToSelected();

            if (PanelCoinGem.Instance != null)
            {
                PanelCoinGem.Instance.AddCoinTop(-goldUnlock, FirebaseLogSpaceWar.PurchaseWing_why, focusItem.WingType.ToString());
            }

            CacheGame.SetWingIsUnlocked((int)focusItem.WingType, 1);

            CacheGame.SetWingUserSelected((int)focusItem.WingType);

            UpdateUnlockUI();
            UpdateButtomButtons();

            panelCongratulations.ShowPanelWing(focusItem.WingType, focusItem.Rank, PanelCongratulations.CongraType.NewItem);
        }
        else
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
        }
    }

    public void OnClickBtnTry()
    {
        //CacheGame.SetCurrentLevel(0);
        SoundManager.PlayClickButton();

        PopupManagerCuong.Instance.ShowNoticeInTrialWingPopup(focusItem.WingType);
    }

    public void OnClickBtnEquip()
    {
        SoundManager.PlayClickButton();
        CacheGame.SetWingUserSelected((int)focusItem.WingType);
        ChangeFocusItemToSelected();

        btnEquip.SetActive(false);
        btnUnequip.SetActive(true);
    }

    public void OnClickBtnUnequip()
    {
        SoundManager.PlayClickButton();

        CacheGame.SetWingUserSelected((int)WingTypeEnum.None);
        focusItem.SetAsNormal();

        btnEquip.SetActive(true);
        btnUnequip.SetActive(false);
    }

    void ChangeFocusItemToSelected()
    {
        if (selectedItem)
            selectedItem.SetAsNormal();

        focusItem.SetAsSelected();
        focusItem.ShowFocusEffect();
        selectedItem = focusItem;
        Debug.LogError("ChangeFocusItemToSelected * " + selectedItem.WingType);
    }

    //[SerializeField]
    //private GameObject popupSelectLevel;
    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();

        if (panelCongratulations.gameObject.activeInHierarchy)
        {
            panelCongratulations.gameObject.SetActive(false);
            return;
        }

        if (panelEvolve.gameObject.activeInHierarchy)
        {
            panelEvolve.gameObject.SetActive(false);
            return;
        }

        if (openSelectFromType == SelectDeviceHandler.OpenSelectFromType.FromSelectLevel)
        {
            PopupManagerCuong.Instance.ShowSelectLevelPopup();
        }
        else if (openSelectFromType == SelectDeviceHandler.OpenSelectFromType.FromTournament)
        {
            PopupManager.Instance.ShowTournamentPopup();
        }

        gameObject.SetActive(false);
        PopupManager.Instance.ShowChatWorldPopup();
        //switch (GameContext.typeOpenSelectItemFrom)
        //{

        //    case GameContext.TypeOpenSelectItemFrom.EndlessButton:

        //        break;
        //    case GameContext.TypeOpenSelectItemFrom.LevelButton:
        //        break;
        //    default:
        //        break;
        //}

    }
}

