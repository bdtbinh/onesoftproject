﻿using UnityEngine;

public class PanelCongratulations : MonoBehaviour {

    [SerializeField]
    private AircraftAnimations aircraftAnimations;

    [SerializeField]
    private WingmanAnimations wingmanAnimations;

    [SerializeField]
    private WingAnimations wingAnimations;

    [SerializeField]
    private UILabel lName;

    [SerializeField]
    private UISprite sRank;

    [SerializeField]
    private UISprite sGlowRank;

    [SerializeField]
    private UILabel lMessage;

    public enum CongraType
    {
        NewItem,
        NewRank
    }

    public void ShowPanelAircraft(AircraftTypeEnum aircraftType, Rank rank, CongraType congraType)
    {
        aircraftAnimations.PlayAnimations(aircraftType, rank);
        lName.text = HangarValue.AircraftNames(aircraftType);
        sRank.spriteName = HangarValue.RankSpriteName[rank];

        if((int)rank > 1)
        {
            sGlowRank.spriteName = "Glow_" + rank.ToString();
        }
        else
        {
            sGlowRank.spriteName = "Glow_B";
        }

        if (congraType == CongraType.NewRank)
        {
            lMessage.text = I2.Loc.ScriptLocalization.panel_congrat_new_rank_aircraft;
        }
        else if(congraType == CongraType.NewItem)
        {
            lMessage.text = I2.Loc.ScriptLocalization.panel_congrat_new_aircraft;
        }

        gameObject.SetActive(true);
    }

    public void ShowPanelWingman(WingmanTypeEnum wingmanType, Rank rank, CongraType congraType)
    {
        wingmanAnimations.PlayAnimations(wingmanType, rank);
        lName.text = HangarValue.WingmanNames(wingmanType);
        sRank.spriteName = HangarValue.RankSpriteName[rank];

        if ((int)rank > 1)
        {
            sGlowRank.gameObject.SetActive(true);
            sGlowRank.spriteName = "Glow_" + rank.ToString();
        }
        else
        {
            sGlowRank.gameObject.SetActive(false);
        }

        if (congraType == CongraType.NewRank)
        {
            lMessage.text = I2.Loc.ScriptLocalization.panel_congrat_new_rank_drone;
        }
        else if (congraType == CongraType.NewItem)
        {
            lMessage.text = I2.Loc.ScriptLocalization.panel_congrat_new_drone;
        }

        gameObject.SetActive(true);
    }

    public void ShowPanelWing(WingTypeEnum wingType, Rank rank, CongraType congraType)
    {
        wingAnimations.PlayAnimations(wingType, rank);                               
        lName.text = HangarValue.WingNames(wingType);
        sRank.spriteName = HangarValue.RankSpriteName[rank];

        if ((int)rank > 1)
        {
            sGlowRank.spriteName = "Glow_" + rank.ToString();
        }
        else
        {
            sGlowRank.spriteName = "Glow_B";
        }

        if (congraType == CongraType.NewRank)
        {
            lMessage.text = I2.Loc.ScriptLocalization.panel_congrat_new_rank_wing;
        }
        else if (congraType == CongraType.NewItem)
        {
            lMessage.text = I2.Loc.ScriptLocalization.panel_congrat_new_wing;
        }

        gameObject.SetActive(true);
    }

    public void OnClickBtnBack()
    {
        gameObject.SetActive(false);

        Debug.LogWarning("Back PanelCongratulation");
    }
}
