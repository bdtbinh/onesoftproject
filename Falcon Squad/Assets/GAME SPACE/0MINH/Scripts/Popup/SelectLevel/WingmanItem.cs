﻿using com.ootii.Messages;
using Sirenix.OdinInspector;
using UnityEngine;

public class WingmanItem : BaseSelectItem
{

    private const string LEFT_POS_SPRITE = "Item_laber_L";
    private const string RIGHT_POS_SPRITE = "Item_laber_R";

    [SerializeField]
    private WingmanTypeEnum wingmanType = WingmanTypeEnum.GatlingGun;

    public WingmanTypeEnum WingmanType { get { return wingmanType; } }

    private UISprite sIcon;
    private UISprite sRank;
    private UISprite sBG;
    private GameObject sLocked;
    private GameObject sFocus;

    //Sprite của icon left right
    private UISprite sPosition;

    private UIButton btnItem;
    private EventDelegate onClickItemCallback;

    //Dành cho những máy bay mua giới hạn trong 1 event nào đó
    //private bool available;
    //public bool Available { get { return available; } }

    private void Awake()
    {
        sIcon = MinhUtils.FindChild(transform, "sIcon").GetComponent<UISprite>();
        sRank = MinhUtils.FindChild(transform, "sRank").GetComponent<UISprite>();
        sBG = MinhUtils.FindChild(transform, "sBG").GetComponent<UISprite>();
        sLocked = MinhUtils.FindChild(transform, "sLocked").gameObject;
        sFocus = MinhUtils.FindChild(transform, "sLockedAndSelected").gameObject;
        sPosition = MinhUtils.FindChild(transform, "sPosition").GetComponent<UISprite>();

        btnItem = GetComponent<UIButton>();
        onClickItemCallback = new EventDelegate(OnClickItem);

        btnItem.onClick.Add(onClickItemCallback);

        LoadData();
    }

    private void Start()
    {
        UpdateRankUI();
    }

    private void OnDestroy()
    {
        btnItem.onClick.Remove(onClickItemCallback);
    }

    public void SetAsNormal()
    {
        stage = ItemStage.Normal;

        sBG.spriteName = HangarValue.HangarNormalBGName;
        sLocked.SetActive(false);
        sFocus.SetActive(false);
        sPosition.gameObject.SetActive(false);
    }

    public void SetAsLocked()
    {
        stage = ItemStage.Locked;

        sBG.spriteName = HangarValue.HangarNormalBGName;
        sLocked.SetActive(true);
        sFocus.SetActive(false);
    }

    //Được chọn để mang vào chơi
    public void SetAsSelected(WingmanPosType posType)
    {
        stage = ItemStage.Selected;

        sBG.spriteName = HangarValue.HangarSelectedBGName;
        sLocked.SetActive(false);
        sFocus.SetActive(false);

        if (posType == WingmanPosType.LeftWingman)
            sPosition.spriteName = LEFT_POS_SPRITE;
        else
            sPosition.spriteName = RIGHT_POS_SPRITE;

        sPosition.gameObject.SetActive(true);
    }

    //Được focus để xem thông tin
    public void ShowFocusEffect()
    {
        sFocus.SetActive(true);
    }

    public void HideFocusEffect()
    {
        sFocus.SetActive(false);
    }

    //Khi click button item
    private void OnClickItem()
    {
        if (!sFocus.activeInHierarchy)
            MessageDispatcher.SendMessage(this, EventID.HANGAR_ITEM_CHANGED, this, 0);
    }

    private void LoadData()
    {
        rank = CacheGame.GetWingmanRank(wingmanType);

        switch (wingmanType)
        {
            case WingmanTypeEnum.None:
                break;
            case WingmanTypeEnum.GatlingGun:
                break;
            case WingmanTypeEnum.AutoGatlingGun:
                break;
            case WingmanTypeEnum.Lazer:
                break;
            case WingmanTypeEnum.DoubleGalting:
                break;
            case WingmanTypeEnum.HomingMissile:
                break;
            default:
                break;
        }
    }

    protected override void UpdateRankUI()
    {
        sIcon.spriteName = "wingman" + (int)wingmanType + "_e" + (int)rank + "_idle_0";
        sRank.spriteName = "hangar_rank_" + rank;

        Vector2 localScale = sIcon.transform.localScale;
        sIcon.MakePixelPerfect();
        sIcon.transform.localScale = localScale;

        sRank.MakePixelPerfect();
    }
}
