﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupUnlockLevel : MonoBehaviour
{
    private const string BTN_UNLOCK_ENABLE_SPRITE = "btn_buy_by_gold";
    private const string BTN_UNLOCK_DISABLE_SPRITE = "IAP_btn_dollar_d";

    [SerializeField]
    private UILabel lTitle;

    [SerializeField]
    private UILabel lNumStarLeft;

    [SerializeField]
    private UILabel lNumStarRight;

    [SerializeField]
    private UIButton btnUnlock;

    [SerializeField]
    private UISprite sBtnUnlock;

    private int levelUnlock;

    public void ShowPopup(int level)
    {
        //levelUnlock = level;
        //gameObject.SetActive(true);
        //lTitle.text = "Level " + level;

        //int currentStars = MSLManager.Instance.TotalStars;
        //int unlockStars = MSLManager.Instance.UnlockStarsList[level / 7 - 1];

        //lNumStarLeft.text = currentStars + "";
        //lNumStarRight.text = "/" + unlockStars;

        //if (currentStars < unlockStars)
        //{
        //    lNumStarLeft.color = Color.red;
        //    btnUnlock.enabled = false;
        //    sBtnUnlock.spriteName = BTN_UNLOCK_DISABLE_SPRITE;
        //}
        //else
        //{
        //    lNumStarLeft.color = Color.green;
        //    btnUnlock.enabled = true;
        //    sBtnUnlock.spriteName = BTN_UNLOCK_ENABLE_SPRITE;
        //}
    }

    public void OnClickBtnClose()
    {
        gameObject.SetActive(false);
    }

    public void OnClickBtnUnlock()
    {
        gameObject.SetActive(false);
        MinhCacheGame.SetAlreadyUnlockLevel(levelUnlock, CacheGame.GetDifficultCampaign());
        MSLManager.Instance.ShowUnlockLevelEffect();

        //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_just_unlock_level + " " + levelUnlock, true, 1.5f);
    }
}
