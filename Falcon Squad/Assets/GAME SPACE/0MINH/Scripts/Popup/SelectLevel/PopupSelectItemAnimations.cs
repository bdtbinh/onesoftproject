﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupSelectItemAnimations : MonoBehaviour {

    public GameObject[] leftWingmanAnimations;
    public GameObject[] rightWingmanAnimations;
    public GameObject[] weaponAnimation;

    public void HideAllAnimations()
    {
        for (int i = 0; i < leftWingmanAnimations.Length; i++)
        {
            leftWingmanAnimations[i].SetActive(false);
        }

        for (int i = 0; i < rightWingmanAnimations.Length; i++)
        {
            rightWingmanAnimations[i].SetActive(false);
        }

        for (int i = 0; i < weaponAnimation.Length; i++)
        {
            weaponAnimation[i].SetActive(false);
        }
    }

}
