﻿using UnityEngine;

public abstract class BaseSelectItem : MonoBehaviour
{
    protected Rank rank;
    public Rank Rank { get { return rank; } }

    public enum ItemStage
    {
        Normal,
        Locked,
        Selected,
        LockedAndSelected,
    }

    protected ItemStage stage;
    public ItemStage Stage { get { return stage; } }

    public enum PriceType
    {
        None,
        Gold,
        Purchase
    }

    public void SetRank(Rank rank)
    {
        this.rank = rank;
        UpdateRankUI();
    }

    protected abstract void UpdateRankUI();
}
