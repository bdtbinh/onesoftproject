﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using OneSoftGame.Tools;
using I2.Loc;

public class PopupLeaderboard : PersistentSingleton<PopupLeaderboard>
{

    private const string SELECTED_SPRITE_NAME = "PVP_local_titler_a";
    private const string UNSELECTED_SPRITE_NAME = "PVP_local_titler_d";
    public string loadRank = "";

    public GameObject loading;
    public UILabel lTitle;
    public GameObject scrollviewWorld;
    public GameObject scrollviewLocal;
    public GameObject scrollviewFacebook;

    protected override void Awake()
    {
        base.Awake();
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadWorldRank.ToString(), OnLoadWorldRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadLocalRank.ToString(), OnLoadLocalRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadFacebookRank.ToString(), OnLoadFacebookRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedWorldRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedLocalRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedFacebookRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedCacheWorldRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedCacheLocalRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedCacheFacebookRank.ToString(), OnLoadedRank, true);

        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadWorldMegaRank.ToString(), OnLoadWorldMegaRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadLocalMegaRank.ToString(), OnLoadLocalMegaRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadFacebookMegaRank.ToString(), OnLoadFacebookMegaRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedWorldMegaRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedLocalMegaRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedFacebookMegaRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedCacheWorldMegaRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedCacheLocalMegaRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.AddListener(EventName.LoadLeaderboard.LoadedCacheFacebookMegaRank.ToString(), OnLoadedRank, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadWorldRank.ToString(), OnLoadWorldRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadLocalRank.ToString(), OnLoadLocalRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadFacebookRank.ToString(), OnLoadFacebookRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedWorldRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedLocalRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedFacebookRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedCacheWorldRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedCacheLocalRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedCacheFacebookRank.ToString(), OnLoadedRank, true);

        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadWorldMegaRank.ToString(), OnLoadWorldMegaRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadLocalMegaRank.ToString(), OnLoadLocalMegaRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadFacebookMegaRank.ToString(), OnLoadFacebookMegaRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedWorldMegaRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedLocalMegaRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedFacebookMegaRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedCacheWorldMegaRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedCacheLocalMegaRank.ToString(), OnLoadedRank, true);
        MessageDispatcher.RemoveListener(EventName.LoadLeaderboard.LoadedCacheFacebookMegaRank.ToString(), OnLoadedRank, true);
    }

    void OnEnable()
    {
        for (int i = 0; i < scrollviewWorld.transform.GetChild(0).childCount; i++)
        {
            scrollviewWorld.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < scrollviewLocal.transform.GetChild(0).childCount; i++)
        {
            scrollviewLocal.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < scrollviewFacebook.transform.GetChild(0).childCount; i++)
        {
            scrollviewFacebook.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        if (PopupManager.Instance.typeRank == 1 || CachePvp.LoadMegaRank)
        {
            lTitle.text = "PVP " + ScriptLocalization.leaderboard;
        }
        else
        {
            lTitle.text = ScriptLocalization.leaderboard;
        }
    }

    void OnLoadedRank(IMessage msg)
    {
        if (loading != null)
        {
            loading.SetActive(false);
        }
    }

    void OnLoadWorldRank(IMessage msg)
    {
        loading.SetActive(true);
        if ((msg.Data != null && (int)msg.Data == 1))
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                loadRank = CachePvp.RANK_WORLD;
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                loadRank = CachePvp.RANK_WORLD_PVP;
            }
        }
        else
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                if (!loadRank.Equals(CachePvp.RANK_WORLD))
                {
                    loadRank = CachePvp.RANK_WORLD;
                }
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                if (!loadRank.Equals(CachePvp.RANK_WORLD_PVP))
                {
                    loadRank = CachePvp.RANK_WORLD_PVP;
                }
            }
        }
        sBtnWorld.spriteName = SELECTED_SPRITE_NAME;
        sBtnLocal.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnFacebook.spriteName = UNSELECTED_SPRITE_NAME;
    }

    void OnLoadLocalRank(IMessage msg)
    {
        loading.SetActive(true);
        if ((msg.Data != null && (int)msg.Data == 1))
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                loadRank = CachePvp.RANK_LOCAL;
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                loadRank = CachePvp.RANK_LOCAL_PVP;
            }
        }
        else
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                if (!loadRank.Equals(CachePvp.RANK_LOCAL))
                {
                    loadRank = CachePvp.RANK_LOCAL;
                }
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                if (!loadRank.Equals(CachePvp.RANK_LOCAL_PVP))
                {
                    loadRank = CachePvp.RANK_LOCAL_PVP;
                }
            }
        }
        sBtnWorld.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnLocal.spriteName = SELECTED_SPRITE_NAME;
        sBtnFacebook.spriteName = UNSELECTED_SPRITE_NAME;
    }

    void OnLoadFacebookRank(IMessage msg)
    {
        loading.SetActive(true);
        sBtnWorld.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnLocal.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnFacebook.spriteName = SELECTED_SPRITE_NAME;
    }

    void OnLoadWorldRank2v2(IMessage msg)
    {
        loading.SetActive(true);
        if ((msg.Data != null && (int)msg.Data == 1))
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                loadRank = CachePvp.RANK_WORLD;
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                loadRank = CachePvp.RANK_WORLD_PVP;
            }
        }
        else
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                if (!loadRank.Equals(CachePvp.RANK_WORLD))
                {
                    loadRank = CachePvp.RANK_WORLD;
                }
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                if (!loadRank.Equals(CachePvp.RANK_WORLD_PVP))
                {
                    loadRank = CachePvp.RANK_WORLD_PVP;
                }
            }
        }
        sBtnWorld.spriteName = SELECTED_SPRITE_NAME;
        sBtnLocal.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnFacebook.spriteName = UNSELECTED_SPRITE_NAME;
    }

    void OnLoadLocalRank2v2(IMessage msg)
    {
        loading.SetActive(true);
        if ((msg.Data != null && (int)msg.Data == 1))
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                loadRank = CachePvp.RANK_LOCAL;
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                loadRank = CachePvp.RANK_LOCAL_PVP;
            }
        }
        else
        {
            if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                if (!loadRank.Equals(CachePvp.RANK_LOCAL))
                {
                    loadRank = CachePvp.RANK_LOCAL;
                }
            }
            else if (PopupManager.Instance.typeRank == 1)
            {
                if (!loadRank.Equals(CachePvp.RANK_LOCAL_PVP))
                {
                    loadRank = CachePvp.RANK_LOCAL_PVP;
                }
            }
        }
        sBtnWorld.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnLocal.spriteName = SELECTED_SPRITE_NAME;
        sBtnFacebook.spriteName = UNSELECTED_SPRITE_NAME;
    }

    void OnLoadFacebookRank2v2(IMessage msg)
    {
        loading.SetActive(true);
        sBtnWorld.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnLocal.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnFacebook.spriteName = SELECTED_SPRITE_NAME;
    }

    void OnLoadWorldMegaRank(IMessage msg)
    {
        loading.SetActive(true);
        if ((msg.Data != null && (int)msg.Data == 1))
        {
            loadRank = CachePvp.RANK_WORLD_PVP;
        }
        else
        {
            if (!loadRank.Equals(CachePvp.RANK_WORLD_PVP))
            {
                loadRank = CachePvp.RANK_WORLD_PVP;
            }
        }
        sBtnWorld.spriteName = SELECTED_SPRITE_NAME;
        sBtnLocal.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnFacebook.spriteName = UNSELECTED_SPRITE_NAME;
    }

    void OnLoadLocalMegaRank(IMessage msg)
    {
        loading.SetActive(true);
        if ((msg.Data != null && (int)msg.Data == 1))
        {
            loadRank = CachePvp.RANK_LOCAL_PVP;
        }
        else
        {
            if (!loadRank.Equals(CachePvp.RANK_LOCAL_PVP))
            {
                loadRank = CachePvp.RANK_LOCAL_PVP;
            }
        }
        sBtnWorld.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnLocal.spriteName = SELECTED_SPRITE_NAME;
        sBtnFacebook.spriteName = UNSELECTED_SPRITE_NAME;
    }

    void OnLoadFacebookMegaRank(IMessage msg)
    {
        loading.SetActive(true);
        sBtnWorld.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnLocal.spriteName = UNSELECTED_SPRITE_NAME;
        sBtnFacebook.spriteName = SELECTED_SPRITE_NAME;
    }

    [SerializeField]
    private UISprite sBtnWorld;

    [SerializeField]
    private UISprite sBtnLocal;

    [SerializeField]
    private UISprite sBtnFacebook;

    public void OnClickBtnWorld()
    {
        if (CachePvp.LoadMegaRank)
        {
            if (loadRank.Equals(CachePvp.RANK_WORLD_PVP))
            {
                return;
            }
            loadRank = CachePvp.RANK_WORLD_PVP;
        }
        else
        {
            if (PopupManager.Instance.typeRank == 1)
            {
                if (loadRank.Equals(CachePvp.RANK_WORLD_PVP))
                {
                    return;
                }
                loadRank = CachePvp.RANK_WORLD_PVP;
            }
            else if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                if (loadRank.Equals(CachePvp.RANK_WORLD))
                {
                    return;
                }
                loadRank = CachePvp.RANK_WORLD;
            }
        }
        scrollviewLocal.SetActive(false);
        scrollviewFacebook.SetActive(false);
        //scrollviewWorld.SetActive(true);
        if (CachePvp.LoadMegaRank)
        {
            MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadWorldMegaRank.ToString());
        }
        else
        {
            MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadWorldRank.ToString());
        }
    }

    public void OnClickBtnLocal()
    {
        if (CachePvp.LoadMegaRank)
        {
            if (loadRank.Equals(CachePvp.RANK_LOCAL_PVP))
            {
                return;
            }
            loadRank = CachePvp.RANK_LOCAL_PVP;
        }
        else
        {
            if (PopupManager.Instance.typeRank == 1)
            {
                if (loadRank.Equals(CachePvp.RANK_LOCAL_PVP))
                {
                    return;
                }
                loadRank = CachePvp.RANK_LOCAL_PVP;
            }
            else if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                if (loadRank.Equals(CachePvp.RANK_LOCAL))
                {
                    return;
                }
                loadRank = CachePvp.RANK_LOCAL;
            }
        }
        //scrollviewLocal.SetActive(true);
        scrollviewFacebook.SetActive(false);
        scrollviewWorld.SetActive(false);
        if (CachePvp.LoadMegaRank)
        {
            MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadLocalMegaRank.ToString());
        }
        else
        {
            MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadLocalRank.ToString());
        }
    }

    public void OnClickBtnFacebook()
    {
        if (CachePvp.LoadMegaRank)
        {
            if (loadRank.Equals(CachePvp.RANK_FACEBOOK_PVP))
            {
                return;
            }
            loadRank = CachePvp.RANK_FACEBOOK_PVP;
        }
        else
        {
            if (PopupManager.Instance.typeRank == 1)
            {
                if (loadRank.Equals(CachePvp.RANK_FACEBOOK_PVP))
                {
                    return;
                }
                loadRank = CachePvp.RANK_FACEBOOK_PVP;
            }
            else if (PopupManager.Instance.typeRank == 0 || PopupManager.Instance.typeRank == 2)
            {
                if (loadRank.Equals(CachePvp.RANK_FACEBOOK))
                {
                    return;
                }
                loadRank = CachePvp.RANK_FACEBOOK;
            }
        }
        scrollviewLocal.SetActive(false);
        //scrollviewFacebook.SetActive(true);
        scrollviewWorld.SetActive(false);
        if (CachePvp.LoadMegaRank)
        {
            MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadFacebookMegaRank.ToString());
        }
        else
        {
            MessageDispatcher.SendMessage(EventName.LoadLeaderboard.LoadFacebookRank.ToString());
        }
    }

    public void OnClickClosePopup()
    {
        PopupManager.Instance.HideRankingPopup();
    }
}
