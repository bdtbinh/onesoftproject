﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardRecord : MonoBehaviour {

    [SerializeField]
    private UILabel lOrdinalNumber;

    [SerializeField]
    private UILabel lName;

    [SerializeField]
    private UILabel lRankNumber;

    [SerializeField]
    private UILabel lWinRate;

    [SerializeField]
    private UILabel lRankTitle;

    [SerializeField]
    private UI2DSprite sAvatar;

    [SerializeField]
    private UISprite sBigFrame;

    [SerializeField]
    private UISprite sSmallFrame;

    [SerializeField]
    private UISprite sBackground;



}
