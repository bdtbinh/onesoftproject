﻿using UnityEngine;
using Sirenix.OdinInspector;
using com.ootii.Messages;
using TCore;

public class PopupHalloween : MonoBehaviour
{
    [SerializeField]
    private GameObject glowBtnCandy;

    [SerializeField]
    private GameObject glowBtnPumpkin;

    [Button]
    public void AddHalloweenCandy()
    {
        MinhCacheGame.AddRemoveEventResources(GameContext.EVENT_HALLOWEEN_2018, 21, true);
        MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, GameContext.EVENT_HALLOWEEN_2018, 0);
    }

    [SerializeField]
    private GameObject panelAccumulated;

    [SerializeField]
    private GameObject panelExchange;

    private void OnEnable()
    {
        panelExchange.SetActive(true);
        panelAccumulated.SetActive(false);
        glowBtnCandy.SetActive(true);
        glowBtnPumpkin.SetActive(false);
    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
    }

    public void OnClickBtnCandy()
    {
        SoundManager.PlayClickButton();
        panelExchange.SetActive(true);
        panelAccumulated.SetActive(false);

        glowBtnCandy.SetActive(true);
        glowBtnPumpkin.SetActive(false);
    }

    public void OnClickBtnPumpkin()
    {
        SoundManager.PlayClickButton();
        panelExchange.SetActive(false);
        panelAccumulated.SetActive(true);

        glowBtnCandy.SetActive(false);
        glowBtnPumpkin.SetActive(true);
    }
   
    public void OnClickBtnPrepare()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowSelectLevelPopup();
    }

    public void OnClickBtnAdd()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowBuyEventItemPopup();
    }
}
