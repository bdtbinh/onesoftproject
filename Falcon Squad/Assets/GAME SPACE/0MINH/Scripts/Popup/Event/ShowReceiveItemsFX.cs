﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class ShowReceiveItemsFX : MonoBehaviour {

    [SerializeField]
    private ParticleSystem particlesFX;

    [SerializeField]
    private TweenAlpha tweenAlphaText;

    [SerializeField]
    private TweenPosition tweenPositionText;

    [SerializeField]
    private UILabel amountText;

    [SerializeField]
    private GameObject objectDisableCallback;

    private void Start()
    {
        MessageDispatcher.AddListener(EventID.ON_DISABLE_SHOW_RECEIVE_ITEM_FX, OnDisableShowReceiveItemFX, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventID.ON_DISABLE_SHOW_RECEIVE_ITEM_FX, OnDisableShowReceiveItemFX, true);
    }

    private void OnDisableShowReceiveItemFX(IMessage msg)
    {
        gameObject.SetActive(false);
    }

    public void PlayFX(int amount)
    {
        amountText.text = "+" + amount.ToString();
        gameObject.SetActive(true);

        tweenAlphaText.ResetToBeginning();
        tweenPositionText.ResetToBeginning();
        tweenAlphaText.PlayForward();
        tweenPositionText.PlayForward();

        particlesFX.Play(true);
    }
}
