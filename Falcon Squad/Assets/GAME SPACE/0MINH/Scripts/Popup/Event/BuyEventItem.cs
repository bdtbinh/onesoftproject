﻿using UnityEngine;
using Sirenix.OdinInspector;
using com.ootii.Messages;
using TCore;
using EasyMobile;

public class BuyEventItemByDollar : MonoBehaviour
{
    [SerializeField]
    private EventItemType eventItem = EventItemType.IceCream_4_2019;

    public enum InappDollarPack
    {
        Dollar_1 = 1,
        Dollar_2 = 2,
        Dollar_5 = 5,
        Dollar_10 = 10,
        Dollar_20 = 20,
        Dollar_50 = 50,
        Dollar_100 = 100,
    }

    [SerializeField]
    [OnValueChanged("OnChangeInappDollarPack")]
    private InappDollarPack inappDollarPack;

    public void OnChangeInappDollarPack()
    {
        lVip.text = "+" + (int)inappDollarPack;
    }

    [SerializeField]
    private UILabel lItemAmount;

    [SerializeField]
    private UILabel lVip;

    [SerializeField]
    private UILabel lPrice;

    [SerializeField]
    [OnValueChanged("OnChangeItemAmount")]
    private int targetItemAmount;           //Số lượng item có thể đổi được mỗi lần.

    public void OnChangeItemAmount()
    {
        lItemAmount.text = "+" + targetItemAmount.ToString();
    }

    private string productname;

    private void Start()
    {
        switch (inappDollarPack)
        {
            case InappDollarPack.Dollar_1:
                lPrice.text = CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_1usd);
                productname = EM_IAPConstants.Product_inapp_event_black_1usd;
                lVip.text = (int)InappDollarPack.Dollar_1 + "";
                break;

            case InappDollarPack.Dollar_2:
                lPrice.text = CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_2usd);
                productname = EM_IAPConstants.Product_inapp_event_black_2usd;
                lVip.text = (int)InappDollarPack.Dollar_2 + "";
                break;

            case InappDollarPack.Dollar_5:
                lPrice.text = CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_5usd);
                productname = EM_IAPConstants.Product_inapp_event_black_5usd;
                lVip.text = (int)InappDollarPack.Dollar_5 + "";
                break;

            case InappDollarPack.Dollar_10:
                lPrice.text = CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_10usd);
                productname = EM_IAPConstants.Product_inapp_event_black_10usd;
                lVip.text = (int)InappDollarPack.Dollar_10 + "";
                break;

            case InappDollarPack.Dollar_20:
                lPrice.text = CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_20usd);
                productname = EM_IAPConstants.Product_inapp_event_black_20usd;
                lVip.text = (int)InappDollarPack.Dollar_20 + "";
                break;

            case InappDollarPack.Dollar_50:
                lPrice.text = CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_50usd);
                productname = EM_IAPConstants.Product_inapp_event_black_50usd;
                lVip.text = (int)InappDollarPack.Dollar_50 + "";
                break;

            case InappDollarPack.Dollar_100:
                lPrice.text = CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_event_black_100usd);
                productname = EM_IAPConstants.Product_inapp_event_black_100usd;
                lVip.text = (int)InappDollarPack.Dollar_100 + "";
                break;

            default:
                break;
        }
    }

    private void OnEnable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
    }

    private void OnDisable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
    }

    void OnPurchaseSuccessed(string productName)
    {
        switch (productName)
        {
            case EM_IAPConstants.Product_inapp_event_black_2usd:
                if (inappDollarPack == InappDollarPack.Dollar_2)
                {
                    OnBuySuccessed();
                }
                break;

            case EM_IAPConstants.Product_inapp_event_black_5usd:
                if (inappDollarPack == InappDollarPack.Dollar_5)
                {
                    OnBuySuccessed();
                }
                break;

            case EM_IAPConstants.Product_inapp_event_black_10usd:
                if (inappDollarPack == InappDollarPack.Dollar_10)
                {
                    OnBuySuccessed();
                }
                break;

            case EM_IAPConstants.Product_inapp_event_black_20usd:
                if (inappDollarPack == InappDollarPack.Dollar_20)
                {
                    OnBuySuccessed();
                }
                break;

            case EM_IAPConstants.Product_inapp_event_black_50usd:
                if (inappDollarPack == InappDollarPack.Dollar_50)
                {
                    OnBuySuccessed();
                }
                break;

            default:
                break;
        }
    }

    private void OnBuySuccessed()
    {
        MinhCacheGame.AddRemoveEventResources(eventItem.ToString(), targetItemAmount, true);
        MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, eventItem, 0);
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + targetItemAmount + " " + HangarValue.EventItemName(eventItem)), true, 1.8f);
    }

    public void OnClickBtnBuy()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(productname, IAPCallbackManager.WherePurchase.shop);
    }
}
