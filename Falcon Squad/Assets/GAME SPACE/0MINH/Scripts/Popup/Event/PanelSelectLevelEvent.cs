﻿using com.ootii.Messages;
using Mp.Pvp;
using System;
using System.Collections;
using UnityEngine;
using I2.Loc;

public class PanelSelectLevelEvent : MonoBehaviour {

    [SerializeField]
    private UILabel lCountdown;

    [SerializeField]
    private UILabel lGainTicket;

    [SerializeField]
    private UILabel lStart;

    SCSelectLvEventInfo ci;
    public SCSelectLvEventInfo Ci
    {
        get { return ci; }
    }

    IEnumerator countDownGainTicket;
    int targetGainTicket;
    int timeRemainingGainTicket;

    private IEnumerator CountDownGainTicket()
    {
        TimeSpan span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        targetGainTicket = (int)span.TotalSeconds + timeRemainingGainTicket;
        while (timeRemainingGainTicket >= 0)
        {
            lCountdown.text = "[be3439]" + (timeRemainingGainTicket / 3600).ToString("00") + ":" + ((timeRemainingGainTicket % 3600) / 60).ToString("00") + ":" + (timeRemainingGainTicket % 60).ToString("00") + "[-]";
            TimeSpan temp_span = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            timeRemainingGainTicket = targetGainTicket - (int)temp_span.TotalSeconds;
            yield return new WaitForSecondsRealtime(1);
        }
        yield return new WaitForSecondsRealtime(1);
        //new CSChristmasInfo().Send();
    }

    private void OnEnable()
    {
        lGainTicket.color = Color.clear;
        lCountdown.color = Color.clear;

        MessageDispatcher.AddListener(EventName.SelectLevelEvent.SelectLvEventTicket.ToString(), OnChrismastTicket, true);
        MessageDispatcher.AddListener(EventName.SelectLevelEvent.SelectLvEventInfo.ToString(), OnChrismastInfo, true);
        new CSSelectLvEventInfo().Send();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.SelectLevelEvent.SelectLvEventTicket.ToString(), OnChrismastTicket, true);
        MessageDispatcher.RemoveListener(EventName.SelectLevelEvent.SelectLvEventInfo.ToString(), OnChrismastInfo, true);
    }

    private void OnChrismastTicket(IMessage rMessage)
    {
        SCSelectLvEventTicket ct = rMessage.Data as SCSelectLvEventTicket;

        if(ct.status == SCSelectLvEventTicket.SUCCESS)
        {
            CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() - CacheGame.numItemPowerUpUse);
            CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() - CacheGame.numUsingSkill);
            CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() - CacheGame.numLifeUpUse);

            LoadingSceneManager.Instance.LoadSceneMainUI("LevelSummer");
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ct.message, false, 1.6f);
        }
    }

    private void OnChrismastInfo(IMessage rMessage)
    {
        //Debug.LogError("OnChrismastInfo" + " vaoooooooooooooo");

        ci = rMessage.Data as SCSelectLvEventInfo;
        if (ci.status == SCSelectLvEventInfo.SUCCESS)
        {
            //Debug.LogError("OnChrismastInfo" + " vaoooooooooooooo 1");

            timeRemainingGainTicket = (int)ci.info.refillRemain;
            if (countDownGainTicket != null)
            {
                StopCoroutine(countDownGainTicket);
            }

            Debug.LogError("OnChrismastInfo" + countDownGainTicket + " * " + timeRemainingGainTicket);

            if (timeRemainingGainTicket != 0)
            {
                if (timeRemainingGainTicket > 0)
                {
                    countDownGainTicket = CountDownGainTicket();
                    //Debug.LogError("OnChrismastInfo" + " vaoooooooooooooo 2");

                    StartCoroutine(countDownGainTicket);
                }
                lGainTicket.color = Color.white;
                lCountdown.color = Color.white;
            }
            else
            {
                lGainTicket.color = Color.clear;
                lCountdown.color = Color.clear;
            }

            lStart.text = I2.Loc.ScriptLocalization.start.ToUpper() + " x" + ci.info.ticket;
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ci.message, false, 1.6f);
        }
    }
}
