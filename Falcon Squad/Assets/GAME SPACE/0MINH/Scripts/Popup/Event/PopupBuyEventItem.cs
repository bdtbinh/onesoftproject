﻿using com.ootii.Messages;
using EasyMobile;
using TCore;
using UnityEngine;

public class PopupBuyEventItem : MonoBehaviour
{
    [SerializeField]
    private EventItemType eventItemType = EventItemType.IceCream_4_2019;

    [SerializeField]
    private UILabel lTotalItem;

    private void OnEnable()
    {
        lTotalItem.text = MinhCacheGame.GetEventResources(eventItemType.ToString()).ToString();
        MessageDispatcher.AddListener(EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, OnChangeResourceAmount, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, OnChangeResourceAmount, true);
    }


    private void OnChangeResourceAmount(IMessage msg)
    {
        if (eventItemType.Equals((EventItemType)msg.Data))
        {
            lTotalItem.text = MinhCacheGame.GetEventResources(eventItemType.ToString()).ToString();
        }
    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
    }
}
