﻿using TCore;
using UnityEngine;

public class ElementAccumulated : MonoBehaviour {
    [SerializeField]
    private UISprite sRewardIcon;

    private string rewardName;

    [SerializeField]
    private UILabel lRewardAmount;
    private int rewardAmount;

    [SerializeField]
    private UILabel lRequirement;
    private int requirement;

    [SerializeField]
    private UILabel lClaim;

    [SerializeField]
    private UISprite sBtnClaim;

    private string eventName;
    private int index; 

    public void CreateItem(string rewardName, int rewardAmount, int requirement, string eventName, int index)
    {
        this.rewardName = rewardName;
        sRewardIcon.spriteName = HangarValue.ItemSpriteName[rewardName];

        this.rewardAmount = rewardAmount;

        if (rewardAmount == 0)
        {
            lRewardAmount.gameObject.SetActive(false);
        }
        else
        {
            lRewardAmount.gameObject.SetActive(true);
            lRewardAmount.text = GameContext.FormatNumber(rewardAmount);
        }

        this.requirement = requirement;
        lRequirement.text = GameContext.FormatNumber(requirement);

        this.eventName = eventName;
        this.index = index;

        UpdateBtnClaimUI();
    }

    private void UpdateBtnClaimUI()
    {
        if (MinhCacheGame.IsAlreadyClaimAccumulatedOfEvent(eventName, index))
        {
            lClaim.text = I2.Loc.ScriptLocalization.claimed;
            sBtnClaim.spriteName = "btn_green_off";
        }
        else
        {
            lClaim.text = I2.Loc.ScriptLocalization.claim;
            sBtnClaim.spriteName = "btn_green_on";
        }
    }

    public void OnClickBtnClaim()
    {
        SoundManager.PlayClickButton();

        if (MinhCacheGame.IsAlreadyClaimAccumulatedOfEvent(eventName, index))
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_claimed_reward, false, 1.8f);
            return;
        }

        if(MinhCacheGame.GetEventAccumulatedResources(eventName) < requirement)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_resources, false, 1.8f);
            return;
        }

        MinhCacheGame.SetAlreadyClaimAccumulatedOfEvent(eventName, index);
        UpdateBtnClaimUI();

        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(1);

        GameContext.AddGiftToPlayer(rewardName, rewardAmount, true, FirebaseLogSpaceWar.Event_why + "_" + eventName, "ElementAccumulated");
        //Debug.LogError(rewardName + " ******* " + rewardAmount);
        //Debug.LogError(GameContext.typeItemPopupItemReward + " *** " + GameContext.numItemPopupItemReward);

        popupItemRewardIns.ShowOneItem(1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);
    }
}
