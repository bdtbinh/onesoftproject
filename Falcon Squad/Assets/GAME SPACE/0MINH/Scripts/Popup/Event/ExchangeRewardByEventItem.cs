﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using TCore;
using com.ootii.Messages;

public class ExchangeRewardByEventItem : MonoBehaviour {

    [SerializeField]
    private UILabel lRewardAmount;

    [SerializeField]
    private UILabel lEventItemAmount;

    [SerializeField]
    private GameContext.TypeItemInPopupItemReward rewardType;

    [SerializeField]
    private EventItemType eventItemType;

    [SerializeField]
    [OnValueChanged("OnChangeRewardAmount")]
    private int rewardAmount;

    private void OnChangeRewardAmount()
    {
        lRewardAmount.text = "+" + rewardAmount.ToString();
    }

    [SerializeField]
    [OnValueChanged("OnChangeEventItemAmount")]
    private int eventItemAmount;

    private void OnChangeEventItemAmount()
    {
        lEventItemAmount.text = eventItemAmount.ToString();
    }

    public void OnClickBtnExchange()
    {
        SoundManager.PlayClickButton();
        
        if (MinhCacheGame.GetEventResources(eventItemType.ToString()) < eventItemAmount)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_resources, false, 1.8f);
            return;
        }

        MinhCacheGame.AddRemoveEventResources(eventItemType.ToString(), -eventItemAmount, false);
        MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, eventItemType, 0);

        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(1);

        GameContext.AddGiftToPlayer(rewardType.ToString(), rewardAmount, true, FirebaseLogSpaceWar.Event_why + "_" + eventItemType.ToString(), "ExchangeRewardByEventItem");

        popupItemRewardIns.ShowOneItem(1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);
    }
}
