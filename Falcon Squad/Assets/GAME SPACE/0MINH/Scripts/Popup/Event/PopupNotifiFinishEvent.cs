﻿using System;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class PopupNotifiFinishEvent : MonoBehaviour
{
    public UILabel textNotifi;

    void Start()
    {
        StartCoroutine(CountDownEndTime());
    }


    public void CancelButton()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.HideNotifiFinishEventPopup();
    }

    public void StartButton()
    {
        SoundManager.PlayClickButton();
        GameContext.modeGamePlay = GameContext.ModeGamePlay.Events;
        PopupManagerCuong.Instance.ShowEventPopup();
        PopupManagerCuong.Instance.HideNotifiFinishEventPopup();
    }

    IEnumerator CountDownEndTime()
    {
        DateTime expiredTime = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out expiredTime);

        while (DateTime.Now < expiredTime)
        {
            TimeSpan interval = expiredTime - DateTime.Now;

            string time = "";

            if (interval.Days > 0)
                time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
            else
                time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds) + "[-]";

            textNotifi.text = I2.Loc.ScriptLocalization.ms_notifi_finish_event.Replace("%{time}", time);
            yield return new WaitForSeconds(1f);
        }
    }
}
