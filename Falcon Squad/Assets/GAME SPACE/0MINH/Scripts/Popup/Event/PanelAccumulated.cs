﻿using UnityEngine;
using com.ootii.Messages;

public class PanelAccumulated : MonoBehaviour
{

    [SerializeField]
    private string eventName = GameContext.EVENT_XMAS_2018;

    [SerializeField]
    private ElementAccumulated elementPrefab;

    [SerializeField]
    private Transform listElementsTrans;

    [SerializeField]
    private UILabel lAccumulatedresources;

    [SerializeField]
    private UIScrollBar scrollBar;

    [SerializeField]
    private UIScrollView scrollView;

    private bool isStarted;             //Cache xem đã vào hàm start hay chưa

    private void Start()
    {
        InitElements();
        isStarted = true;

        MessageDispatcher.AddListener(EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, OnChangeResourceAmount, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, OnChangeResourceAmount, true);
    }

    private void OnEnable()
    {
        if(isStarted)
            ResetScrollViewPosition();
    }

    private void InitElements()
    {
        lAccumulatedresources.text = MinhCacheGame.GetEventAccumulatedResources(eventName).ToString();

        for (int i = 0; i < XMasAccumulatedSheet.GetDictionary().Count; i++)
        {
            ElementAccumulated e = Instantiate(elementPrefab, listElementsTrans);
            e.transform.localPosition = new Vector3(0, 46 - i * 127, 0);
            e.CreateItem(XMasAccumulatedSheet.Get(i + 1).name, XMasAccumulatedSheet.Get(i + 1).amount, XMasAccumulatedSheet.Get(i + 1).requirement, eventName, i + 1);
        }

        ResetScrollViewPosition();
    }

    private void ResetScrollViewPosition()
    {
        float cacheY = 0f;       //Cache lại tọa độ y của item cần scroll đến:
        for (int i = 0; i < listElementsTrans.childCount; i++)
        {
            //Scroll đến item đầu tiên chưa được claim:
            if (!MinhCacheGame.IsAlreadyClaimAccumulatedOfEvent(eventName, i + 1))
            {
                cacheY = listElementsTrans.GetChild(i).localPosition.y - 244;
                break;
            }
        }

        float scrollerHeight = scrollView.bounds.size.y - scrollView.panel.height;
        float maxY = scrollView.bounds.max.y - scrollView.panel.height / 2;
        float minY = scrollView.bounds.min.y + scrollView.panel.height / 2;

        cacheY = Mathf.Clamp(cacheY, minY, maxY);

        if (cacheY == 0f)
        {
            scrollBar.value = 0f;
        }
        else
        {
            scrollBar.value = (maxY - cacheY) / scrollerHeight;
        }
    }

    private void OnChangeResourceAmount(IMessage msg)
    {
        if (eventName.Equals((string)msg.Data))
        {
            lAccumulatedresources.text = MinhCacheGame.GetEventAccumulatedResources(eventName).ToString();
        }
    }
}
