﻿using UnityEngine;
using com.ootii.Messages;

public class PanelExchange : MonoBehaviour
{

    [SerializeField]
    private string eventName = GameContext.EVENT_XMAS_2018;               //Lưu ý: tên event mới sau này phải đặt khác với tên event cũ (nên đặt tên kèm ngày tháng năm cho dễ phân biệt).

    [SerializeField]
    private ElementExchange elementPrefab;

    [SerializeField]
    private Transform listElementsTrans;

    [SerializeField]
    private UILabel lTotalresources;

    private void Start()
    {
        InitElements();
        MessageDispatcher.AddListener(EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, OnChangeResourceAmount, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, OnChangeResourceAmount, true);
    }

    private void InitElements()
    {
        lTotalresources.text = MinhCacheGame.GetEventResources(eventName).ToString();

        for (int i = 0; i < HalloweenExchangeSheet.GetDictionary().Count; i++)
        {
            ElementExchange e = Instantiate(elementPrefab, listElementsTrans);
            e.transform.localPosition = new Vector3(0, 46 - i * 127, 0);
            e.CreateItem(HalloweenExchangeSheet.Get(i + 1).name, HalloweenExchangeSheet.Get(i + 1).amount, HalloweenExchangeSheet.Get(i + 1).quantity, HalloweenExchangeSheet.Get(i + 1).requirement, eventName, i + 1);
        }
    }

    private void OnChangeResourceAmount(IMessage msg)
    {
        if (eventName.Equals((string)msg.Data))
        {
            lTotalresources.text = MinhCacheGame.GetEventResources(eventName).ToString();
        }
    }
}
