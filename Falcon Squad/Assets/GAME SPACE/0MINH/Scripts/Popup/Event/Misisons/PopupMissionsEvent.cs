﻿using com.ootii.Messages;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class PopupMissionsEvent : MonoBehaviour
{
    [SerializeField]
    private UILabel lCountdown;

    [SerializeField]
    private UILabel lTotalresources;

    [SerializeField]
    private GameObject btnLeftGlow;

    [SerializeField]
    private GameObject btnRightGlow;

    [SerializeField]
    [FoldoutGroup("Editor", 1)]
    private EventItemType eventItemType = EventItemType.IceCream_4_2019;

    [SerializeField]
    [FoldoutGroup("Editor", 2)]
    private int addResourceAmount = 10;

    [Button]
    [FoldoutGroup("Editor", 3)]
    public void AddEventResources()
    {
        MinhCacheGame.AddRemoveEventResources(eventItemType.ToString(), addResourceAmount, false);
        MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, eventItemType, 0);
    }

    [SerializeField]
    private GameObject panelBoxExchangeEvent;

    [SerializeField]
    private GameObject panelMissionsEvent;

    private void OnEnable()
    {
        panelMissionsEvent.SetActive(true);
        panelBoxExchangeEvent.SetActive(false);
        btnLeftGlow.SetActive(true);
        btnRightGlow.SetActive(false);

        lTotalresources.text = I2.Loc.ScriptLocalization.label_total.Replace("%{numer}", MinhCacheGame.GetEventResources(eventItemType.ToString()).ToString());

        MessageDispatcher.AddListener(EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, OnChangeResourceAmount, true);

        StartCoroutine(CountDownEndTime());
    }

    IEnumerator CountDownEndTime()
    {
        DateTime expiredTime = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out expiredTime);

        while (DateTime.Now < expiredTime)
        {
            TimeSpan interval = expiredTime - DateTime.Now;

            string time = "";

            if (interval.Days > 0)
                time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
            else
                time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds) + "[-]";

            lCountdown.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", time);
            yield return new WaitForSeconds(1f);
        }
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, OnChangeResourceAmount, true);
    }

    private void OnChangeResourceAmount(IMessage msg)
    {
        if (eventItemType.Equals((EventItemType)msg.Data))
        {
            lTotalresources.text = I2.Loc.ScriptLocalization.label_total.Replace("%{numer}", MinhCacheGame.GetEventResources(eventItemType.ToString()).ToString());
        }
    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
    }

    public void OnClickBtnLeft()
    {
        SoundManager.PlayClickButton();
        panelMissionsEvent.SetActive(true);
        panelBoxExchangeEvent.SetActive(false);

        btnLeftGlow.SetActive(true);
        btnRightGlow.SetActive(false);
    }

    public void OnClickBtnRight()
    {
        SoundManager.PlayClickButton();
        panelMissionsEvent.SetActive(false);
        panelBoxExchangeEvent.SetActive(true);

        btnLeftGlow.SetActive(false);
        btnRightGlow.SetActive(true);
    }

    public void OnClickBtnPlay()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowSelectLevelPopup();
    }

    public void OnClickBtnAdd()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowBuyEventItemPopup();
    }
}
