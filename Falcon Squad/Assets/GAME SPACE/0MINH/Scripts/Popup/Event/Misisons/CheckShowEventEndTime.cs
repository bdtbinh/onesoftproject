﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckShowEventEndTime : MonoBehaviour {

    private void Awake()
    {
        if ((float)Screen.height / (float)Screen.width > 2.15)
        {
            gameObject.SetActive(false);
        }
    }
}
