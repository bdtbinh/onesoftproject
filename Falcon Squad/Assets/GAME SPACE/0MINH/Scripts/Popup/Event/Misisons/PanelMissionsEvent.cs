﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelMissionsEvent : MonoBehaviour {

    [SerializeField]
    private ElementMissionProgress[] elementMissions;

    private void OnEnable()
    {
        for (int i = 0; i < elementMissions.Length; i++)
        {
            int maxValue = MinhCacheGame.GetEventMissionMaxProgress(i + 1);
            int currentValue = maxValue - MinhCacheGame.GetEventMissionRemainProgress(i + 1);

            elementMissions[i].SetProgressValues(currentValue, maxValue);
        }
    }
}
