﻿using TCore;
using UnityEngine;
using Sirenix.OdinInspector;
using com.ootii.Messages;

public class PanelBoxExchangeEvent : MonoBehaviour {

    [SerializeField]
    private PanelRewardsInfoEvent panelRewardsInfoEvent;

    [SerializeField]
    private UILabel lBox1Des;

    [SerializeField]
    private UILabel lBox2Des;

    [SerializeField]
    private UILabel lBox3Des;

    [SerializeField]
    private UILabel lBox1Price;

    [SerializeField]
    private UILabel lBox2Price;

    [SerializeField]
    private UILabel lBox3Price;

    [SerializeField]
    [OnValueChanged("OnChangeBox1Price")]
    private int box1Price = 1200;

    private void OnChangeBox1Price()
    {
        lBox1Price.text = box1Price + "";
    }

    [SerializeField]
    [OnValueChanged("OnChangeBox2Price")]
    private int box2Price = 1200;

    private void OnChangeBox2Price()
    {
        lBox2Price.text = box2Price + "";
    }

    [SerializeField]
    [OnValueChanged("OnChangeBox3Price")]
    private int box3Price = 1200;

    private void OnChangeBox3Price()
    {
        lBox3Price.text = box3Price + "";
    }

    [SerializeField]
    private EventItemType eventItemType;

    private void Awake()
    {
        lBox1Des.text = I2.Loc.ScriptLocalization.get_amount_item.Replace("%{number}", 3.ToString());
        lBox2Des.text = I2.Loc.ScriptLocalization.get_amount_item.Replace("%{number}", 4.ToString());
        lBox3Des.text = I2.Loc.ScriptLocalization.get_amount_item.Replace("%{number}", 5.ToString());
    }

    public void OnClickBtnExchangeBox1()
    {
        SoundManager.PlayClickButton();

        if (MinhCacheGame.GetEventResources(eventItemType.ToString()) < box1Price)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_resources, false, 1.8f);
            return;
        }

        MinhCacheGame.AddRemoveEventResources(eventItemType.ToString(), -box1Price, false);
        MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, eventItemType, 0);

        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(3);

        for (int i = 0; i < 3; i++)
        {
            int rewardIndex = GetRandomIndexBox1();
            string rewardName = SummerHoliday_Box1Sheet.Get(rewardIndex).name;
            int rewardAmount = SummerHoliday_Box1Sheet.Get(rewardIndex).amount;

            GameContext.AddGiftToPlayer(rewardName, rewardAmount, true, FirebaseLogSpaceWar.Event_why, "Summer Holiday Box 1 Exchange");
            popupItemRewardIns.ShowOneItem(i + 1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);
        }
    }

    public void OnClickBtnExchangeBox2()
    {
        SoundManager.PlayClickButton();

        if (MinhCacheGame.GetEventResources(eventItemType.ToString()) < box2Price)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_resources, false, 1.8f);
            return;
        }

        MinhCacheGame.AddRemoveEventResources(eventItemType.ToString(), -box2Price, false);
        MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, eventItemType, 0);

        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(4);

        for (int i = 0; i < 4; i++)
        {
            int rewardIndex = GetRandomIndexBox2();
            string rewardName = SummerHoliday_Box2Sheet.Get(rewardIndex).name;
            int rewardAmount = SummerHoliday_Box2Sheet.Get(rewardIndex).amount;

            GameContext.AddGiftToPlayer(rewardName, rewardAmount, true, FirebaseLogSpaceWar.Event_why, "Summer Holiday Box 2 Exchange");
            popupItemRewardIns.ShowOneItem(i + 1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);
        }
    }

    public void OnClickBtnExchangeBox3()
    {
        SoundManager.PlayClickButton();

        if (MinhCacheGame.GetEventResources(eventItemType.ToString()) < box3Price)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_resources, false, 1.8f);
            return;
        }

        MinhCacheGame.AddRemoveEventResources(eventItemType.ToString(), -box3Price, false);
        MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, eventItemType, 0);

        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(5);

        for (int i = 0; i < 5; i++)
        {
            int rewardIndex = GetRandomIndexBox3();
            string rewardName = SummerHoliday_Box3Sheet.Get(rewardIndex).name;
            int rewardAmount = SummerHoliday_Box3Sheet.Get(rewardIndex).amount;

            GameContext.AddGiftToPlayer(rewardName, rewardAmount, true, FirebaseLogSpaceWar.Event_why, "Summer Holiday Box 3 Exchange");
            popupItemRewardIns.ShowOneItem(i + 1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);
        }
    }


    private int GetRandomIndexBox1()
    {
        float totalPercent = Random.Range(0f, 100f);

        for (int i = 1; i <= SummerHoliday_Box1Sheet.GetDictionary().Count; i++)
        {
            if (totalPercent <= SummerHoliday_Box1Sheet.Get(i).total_percent)
            {
                return i;
            }
        }

        return 1;
    }

    private int GetRandomIndexBox2()
    {
        float totalPercent = Random.Range(0f, 100f);

        for (int i = 1; i <= SummerHoliday_Box2Sheet.GetDictionary().Count; i++)
        {
            if (totalPercent <= SummerHoliday_Box2Sheet.Get(i).total_percent)
            {
                return i;
            }
        }

        return 1;
    }

    private int GetRandomIndexBox3()
    {
        float totalPercent = Random.Range(0f, 100f);

        for (int i = 1; i <= SummerHoliday_Box3Sheet.GetDictionary().Count; i++)
        {
            if (totalPercent <= SummerHoliday_Box3Sheet.Get(i).total_percent)
            {
                return i;
            }
        }

        return 1;
    }

    public void ShowBox1PanelRewardsInfo()
    {
        SoundManager.PlayClickButton();
        panelRewardsInfoEvent.ShowBox1RewardsInfo();
    }

    public void ShowBox2PanelRewardsInfo()
    {
        SoundManager.PlayClickButton();
        panelRewardsInfoEvent.ShowBox2RewardsInfo();
    }

    public void ShowBox3PanelRewardsInfo()
    {
        SoundManager.PlayClickButton();
        panelRewardsInfoEvent.ShowBox3RewardsInfo();
    }
}
