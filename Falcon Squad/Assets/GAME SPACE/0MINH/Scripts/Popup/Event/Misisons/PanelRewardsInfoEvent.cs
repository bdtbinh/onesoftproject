﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelRewardsInfoEvent : MonoBehaviour
{

    [SerializeField]
    private ElementItemInfo elementPrefab;

    [SerializeField]
    private Transform listElementsTrans;

    private float offsetX = 104;            //Khoảng cách giữa 2 element theo trục x
    private float offsetY = 110;            //Khoảng cách giữa 2 element theo trục y

    private List<ElementItemInfo> listRewards;

    public void OnClickClosePanel()
    {
        gameObject.SetActive(false);
    }

    private void InitElements()
    {
        Vector2 firstPos = new Vector2(-2.5f * offsetX, 1.5f * offsetY);
        int maxListCount = 0;



        if (SummerHoliday_Box1Sheet.GetDictionary().Count > maxListCount)
        {
            maxListCount = SummerHoliday_Box1Sheet.GetDictionary().Count;
        }

        if (SummerHoliday_Box2Sheet.GetDictionary().Count > maxListCount)
        {
            maxListCount = SummerHoliday_Box2Sheet.GetDictionary().Count;
        }

        if (SummerHoliday_Box3Sheet.GetDictionary().Count > maxListCount)
        {
            maxListCount = SummerHoliday_Box3Sheet.GetDictionary().Count;
        }

        listRewards = new List<ElementItemInfo>(maxListCount);
        for (int i = 0; i < maxListCount; i++)
        {
            ElementItemInfo itemInfo = Instantiate(elementPrefab, listElementsTrans);
            itemInfo.transform.localPosition = new Vector3(firstPos.x + (i % 6) * offsetX, firstPos.y - (i / 6) * offsetY, 0);
            listRewards.Add(itemInfo);
        }
    }

    public void ShowBox1RewardsInfo()
    {
        gameObject.SetActive(true);

        if (listRewards == null)
            InitElements();

        for (int i = 0; i < listRewards.Count; i++)
        {
            listRewards[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < SummerHoliday_Box1Sheet.GetDictionary().Count; i++)
        {
            listRewards[i].CreateItem(SummerHoliday_Box1Sheet.Get(i + 1).name, SummerHoliday_Box1Sheet.Get(i + 1).amount, false, true, SummerHoliday_Box1Sheet.Get(i + 1).event_tag == 1);
            listRewards[i].gameObject.SetActive(true);
        }
    }

    public void ShowBox2RewardsInfo()
    {
        gameObject.SetActive(true);

        if (listRewards == null)
            InitElements();

        for (int i = 0; i < listRewards.Count; i++)
        {
            listRewards[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < SummerHoliday_Box2Sheet.GetDictionary().Count; i++)
        {
            listRewards[i].CreateItem(SummerHoliday_Box2Sheet.Get(i + 1).name, SummerHoliday_Box2Sheet.Get(i + 1).amount, false, true, SummerHoliday_Box2Sheet.Get(i + 1).event_tag == 1);
            listRewards[i].gameObject.SetActive(true);
        }
    }

    public void ShowBox3RewardsInfo()
    {
        gameObject.SetActive(true);

        if (listRewards == null)
            InitElements();

        for (int i = 0; i < listRewards.Count; i++)
        {
            listRewards[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < SummerHoliday_Box3Sheet.GetDictionary().Count; i++)
        {
            listRewards[i].CreateItem(SummerHoliday_Box3Sheet.Get(i + 1).name, SummerHoliday_Box3Sheet.Get(i + 1).amount, false, true, SummerHoliday_Box3Sheet.Get(i + 1).event_tag == 1);
            listRewards[i].gameObject.SetActive(true);
        }
    }
}
