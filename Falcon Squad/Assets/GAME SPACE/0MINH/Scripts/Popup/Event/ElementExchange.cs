﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using TCore;

public class ElementExchange : MonoBehaviour
{

    [SerializeField]
    private UISprite sRewardIcon;

    private string rewardName;

    [SerializeField]
    private UILabel lRewardAmount;
    private int rewardAmount;

    [SerializeField]
    private UILabel lQuantity;
    private int quantity;   //Số lần exchange tối đa có thể dùng
    private int usedTime;   //Số lần exchange đã dùng

    [SerializeField]
    private UILabel lRequirement;
    private int requirement;

    private string eventItem;
    private int index;      //Index là số thứ tự trong bản EventExchangeSheet

    public void CreateItem(string rewardName, int rewardAmount, int quantity, int requirement, string eventName, int index)
    {
        this.rewardName = rewardName;
        sRewardIcon.spriteName = HangarValue.ItemSpriteName[rewardName];

        this.rewardAmount = rewardAmount;

        if (rewardAmount == 0)
        {
            lRewardAmount.gameObject.SetActive(false);
        }
        else
        {
            lRewardAmount.gameObject.SetActive(true);
            lRewardAmount.text = rewardAmount.ToString();
        }

        usedTime = MinhCacheGame.GetExchangeUsedTimesOfEvent(eventName, index);
        this.quantity = quantity;
        lQuantity.text = usedTime + "/" + quantity;

        this.requirement = requirement;
        lRequirement.text = requirement.ToString();

        this.eventItem = eventName;
        this.index = index;
    }

    public void OnClickBtnExchange()
    {
        SoundManager.PlayClickButton();

        if (usedTime >= quantity)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.no_available_exchange, false, 1.8f);
            return;
        }

        if (MinhCacheGame.GetEventResources(eventItem) < requirement)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_resources, false, 1.8f);
            return;
        }

        MinhCacheGame.AddRemoveEventResources(eventItem, -requirement, false);
        MessageDispatcher.SendMessage(this, EventID.ON_CHANGE_EVENT_RESOURCE_AMOUNT, eventItem, 0);

        usedTime++;
        lQuantity.text = usedTime + "/" + quantity;
        MinhCacheGame.SetExchangeUsedTimesOfEvent(eventItem, index, usedTime);

        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(1);

        GameContext.AddGiftToPlayer(rewardName, rewardAmount, true, FirebaseLogSpaceWar.Event_why + "_" + eventItem, "ElementExchange");
        //Debug.LogError(rewardName + " ******* " + rewardAmount);
        //Debug.LogError(GameContext.typeItemPopupItemReward + " *** " + GameContext.numItemPopupItemReward);

        popupItemRewardIns.ShowOneItem(1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);
    }
}
