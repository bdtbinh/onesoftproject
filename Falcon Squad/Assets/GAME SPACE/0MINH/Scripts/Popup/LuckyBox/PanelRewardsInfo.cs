﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelRewardsInfo : MonoBehaviour
{
    [SerializeField]
    private ElementItemInfo elementPrefab;

    [SerializeField]
    private Transform listElementsTrans;

    private float offsetX = 104;            //Khoảng cách giữa 2 element theo trục x
    private float offsetY = 110;            //Khoảng cách giữa 2 element theo trục y

    private List<ElementItemInfo> listRewards;

    public void OnClickClosePanel()
    {
        gameObject.SetActive(false);
    }

    private void InitElements()
    {
        Vector2 firstPos = new Vector2(-2.5f * offsetX, 1.5f * offsetY);
        int maxListCount = 0;

        if (LuckyBoxGoldSheet.GetDictionary().Count > LuckyBoxGemSheet.GetDictionary().Count)
        {
            listRewards = new List<ElementItemInfo>(LuckyBoxGoldSheet.GetDictionary().Count);
            maxListCount = LuckyBoxGoldSheet.GetDictionary().Count;
        }
        else
        {
            listRewards = new List<ElementItemInfo>(LuckyBoxGemSheet.GetDictionary().Count);
            maxListCount = LuckyBoxGemSheet.GetDictionary().Count;
        }

        for (int i = 0; i < maxListCount; i++)
        {
            ElementItemInfo itemInfo = Instantiate(elementPrefab, listElementsTrans);
            itemInfo.transform.localPosition = new Vector3(firstPos.x + (i % 6) * offsetX, firstPos.y - (i / 6) * offsetY, 0);
            listRewards.Add(itemInfo);
        }
    }

    public void ShowGoldRewardsInfo()
    {
        gameObject.SetActive(true);

        if (listRewards == null)
            InitElements();

        for (int i = 0; i < listRewards.Count; i++)
        {
            listRewards[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < LuckyBoxGoldSheet.GetDictionary().Count; i++)
        {
            listRewards[i].CreateItem(LuckyBoxGoldSheet.Get(i + 1).name, LuckyBoxGoldSheet.Get(i + 1).amount, false);
            listRewards[i].gameObject.SetActive(true);
        }
    }

    public void ShowGemRewardsInfo()
    {
        gameObject.SetActive(true);

        if (listRewards == null)
            InitElements();

        for (int i = 0; i < listRewards.Count; i++)
        {
            listRewards[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < LuckyBoxGemSheet.GetDictionary().Count; i++)
        {
            listRewards[i].CreateItem(LuckyBoxGemSheet.Get(i + 1).name, LuckyBoxGemSheet.Get(i + 1).amount, false);
            listRewards[i].gameObject.SetActive(true);
        }
    }

}



