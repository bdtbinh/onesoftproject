﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using TCore;

public class PanelReward : MonoBehaviour
{
    [SerializeField]
    private UISprite sFade;

    [SerializeField]
    private GameObject insideObjects;

    [SerializeField]
    private GameObject listItemObject;

    [SerializeField]
    private GameObject itemObject;

    [SerializeField]
    private TweenAlpha tweenAlpha;

    [SerializeField]
    private ParticleSystem openGoldChestFX;

    [SerializeField]
    private ParticleSystem openGemChestFX;

    [SerializeField]
    private ShowReceiveItemsFX showReceiveItemsFx;

    private bool isOneReward;
    private ResourceType resourceType;

    private void ResetUI()
    {
        gameObject.SetActive(false);
        insideObjects.SetActive(false);
        listItemObject.SetActive(false);
        itemObject.gameObject.SetActive(false);
        openGoldChestFX.transform.parent.gameObject.SetActive(false);
        openGemChestFX.transform.parent.gameObject.SetActive(false);

        sFade.alpha = 0;
    }

    public void ShowPopupOneReward(ResourceType resourceType)
    {
        this.resourceType = resourceType;
        isOneReward = true;
        ResetUI();
        tweenAlpha.enabled = true;
        tweenAlpha.ResetToBeginning();
        gameObject.SetActive(true);
    }

    public void ShowPopupListReward(ResourceType resourceType)
    {
        this.resourceType = resourceType;
        isOneReward = false;
        ResetUI();
        tweenAlpha.enabled = true;
        tweenAlpha.ResetToBeginning();
        gameObject.SetActive(true);
    }

    public void OnFadeInSuccessed()
    {
        if (resourceType == ResourceType.Gold)
        {
            coWaitForShowItem = CoWaitForShowItem(2.1f);
            openGoldChestFX.transform.parent.gameObject.SetActive(true);
            //openGoldChestFX.Stop();
            openGoldChestFX.Play();
            openGoldChestFX.transform.parent.GetComponent<Animator>().Play("Open_Box_Shake", -1, 0);
        }
        else
        {
            coWaitForShowItem = CoWaitForShowItem(2.1f);
            openGemChestFX.transform.parent.gameObject.SetActive(true);
            openGemChestFX.Play();
            openGemChestFX.transform.parent.GetComponent<Animator>().Play("Open_Box_Shake", -1, 0);
        }

        StartCoroutine(coWaitForShowItem);
    }

    public void OnClickBtnConfirm()
    {
        SoundManager.PlayClickButton();

        MinhCacheGame.UpdateIsInEvent(GameContext.EVENT_SUMMER_HOLIDAY_2019);

        if (MinhCacheGame.IsInEvent(GameContext.EVENT_SUMMER_HOLIDAY_2019))
        {
            if (resourceType == ResourceType.Gold)
            {
                if (isOneReward)
                {
                    int num = MinhCacheGame.GetEventMissionCollectedToShow(3, 10);
                    if (num > 0)
                    {
                        MinhCacheGame.AddEventMissionCollected(3, num);
                        showReceiveItemsFx.PlayFX(num);
                    }
                }
                else
                {
                    int num = MinhCacheGame.GetEventMissionCollectedToShow(3, 100);
                    if (num > 0)
                    {
                        MinhCacheGame.AddEventMissionCollected(3, num);
                        showReceiveItemsFx.PlayFX(num);
                    }
                }
            }
            else if (resourceType == ResourceType.Gem)
            {
                if (isOneReward)
                {
                    int num = MinhCacheGame.GetEventMissionCollectedToShow(4, 20);
                    if (num > 0)
                    {
                        MinhCacheGame.AddEventMissionCollected(4, num);
                        showReceiveItemsFx.PlayFX(num);
                    }
                }
                else
                {
                    int num = MinhCacheGame.GetEventMissionCollectedToShow(4, 200);
                    if (num > 0)
                    {
                        MinhCacheGame.AddEventMissionCollected(4, num);
                        showReceiveItemsFx.PlayFX(num);
                    }
                }
            }
        }

        PanelCoinGem.Instance.AddCoinTop();
        PanelCoinGem.Instance.AddGemTop();

        PopupLuckyBox.isClickBtnOpenBox = false;
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        if (coWaitForShowItem != null)
            StopCoroutine(coWaitForShowItem);
    }

    IEnumerator coWaitForShowItem;
    IEnumerator CoWaitForShowItem(float time)
    {
        yield return new WaitForSeconds(time);

        if (isOneReward)
        {
            itemObject.SetActive(true);
            insideObjects.SetActive(true);
        }
        else
        {
            listItemObject.SetActive(true);
            insideObjects.SetActive(true);
        }
    }
}
