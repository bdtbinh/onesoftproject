﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuckyBoxItem : MonoBehaviour {

    private int amount;
    public int Amount
    {
        get { return amount; }
    }

    [SerializeField]
    private UISprite sIcon;

    [SerializeField]
    private UILabel lAmount;

    [SerializeField]
    private GameObject isValueableEffect;


    public void CreateItem(string rewardName, int amount, bool isValuable)
    {
        this.amount = amount;

        sIcon.spriteName = HangarValue.ItemSpriteName[rewardName];
        lAmount.text = amount + "";

        if(isValueableEffect != null)
            isValueableEffect.SetActive(isValuable);
    }
}
