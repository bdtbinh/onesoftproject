﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using TCore;
using I2.Loc;
using Mp.Pvp;
using com.ootii.Messages;

public class PopupLuckyBox : MonoBehaviour
{

    private const int SMALL_GOLD_COST = 10000;
    private const int BIG_GOLD_COST = 90000;
    private const int SMALL_GEM_COST = 30;
    private const int BIG_GEM_COST = 270;

    [SerializeField]
    private GameObject popupLuckyBoxInvi;

    [SerializeField]
    private PanelReward panelReward;

    [SerializeField]
    private PanelRewardsInfo panelRewardsInfo;

    [SerializeField]
    private List<ElementItemInfo> listItem;

    [SerializeField]
    private ElementItemInfo item;

    [SerializeField]
    private GameObject btnLuckyBox;

    [SerializeField]
    private UILabel lCountDown;

    [SerializeField]
    private UILabel lCountDownBtn;

    public static bool isClickBtnOpenBox = false;           //Check xem user đã bấm open chưa, tránh trường hợp spam nút open 

    [SerializeField]
    private ElementItemInfo[] goldItemsValuable;

    [SerializeField]
    private ElementItemInfo[] gemItemsValuable;

    public bool hasFreeGoldChest = false;

    private void Start()
    {
        //if (!PlayerPrefs.HasKey("LuckyBoxExpiredDate_M"))
        //{
        //    btnLuckyBox.SetActive(false);
        //    return;
        //}

        isClickBtnOpenBox = false;

        if (CacheGame.GetMaxLevel3Difficult() >= Constant.UNLOCK_LEVEL_LUCKY_BOX)
        {
            btnLuckyBox.SetActive(true);
        }
        else
        {
            btnLuckyBox.SetActive(false);
            return;
        }

        //if (DateTime.Now < MinhCacheGame.GetLuckyBoxExpiredDate())
        //{
        //    btnLuckyBox.SetActive(true);

        //    coCountdown = CoCountDown();
        //    StartCoroutine(coCountdown);
        //}
        //else
        //{
        //    btnLuckyBox.SetActive(false);
        //}

        DisplayValuableItems();


    }

    private void DisplayValuableItems()
    {
        for (int i = 1; i <= goldItemsValuable.Length; i++)
        {
            for (int j = 1; j <= LuckyBoxGoldSheet.GetDictionary().Count; j++)
            {
                if (LuckyBoxGoldSheet.Get(j).special_item_order == i)
                {
                    int rewardAmount = NeedRewardSpecialAmount() ? LuckyBoxGoldSheet.Get(j).special_amount : LuckyBoxGoldSheet.Get(j).amount;
                    goldItemsValuable[i - 1].CreateItem(LuckyBoxGoldSheet.Get(j).name, rewardAmount, false);
                }
            }
        }

        for (int i = 1; i <= gemItemsValuable.Length; i++)
        {
            for (int j = 1; j <= LuckyBoxGemSheet.GetDictionary().Count; j++)
            {
                if (LuckyBoxGemSheet.Get(j).special_item_order == i)
                {
                    int rewardAmount = NeedRewardSpecialAmount() ? LuckyBoxGemSheet.Get(j).special_amount : LuckyBoxGemSheet.Get(j).amount;
                    gemItemsValuable[i - 1].CreateItem(LuckyBoxGemSheet.Get(j).name, rewardAmount, false);
                }
            }
        }
    }

    private void OnDestroy()
    {
        //if (coCountdown != null)
        //    StopCoroutine(coCountdown);
    }

    public void ShowPopup()
    {
        SoundManager.PlayShowPopup();
        popupLuckyBoxInvi.SetActive(true);
        FirebaseLogSpaceWar.LogClickButton("LuckyBox");
        PopupManager.Instance.HideChatWorldPopup();
    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        popupLuckyBoxInvi.SetActive(false);
        PopupManager.Instance.ShowChatWorldPopup();
    }

    public void OnClickBtnSmallGold()
    {
        SoundManager.PlayClickButton();

        if (hasFreeGoldChest)
        {
            if (!OSNet.NetManager.Instance.IsOnline())
            {
                PopupManager.Instance.ShowToast(ScriptLocalization.notifi_pvp_disconnect_server);
                return;
            }

            if (isClickBtnOpenBox) return;

            new CSRoyaltyPackOpenChest().Send();

            GetRewardsByGold(1);

            panelReward.ShowPopupOneReward(ResourceType.Gold);
            isClickBtnOpenBox = true;

            MessageDispatcher.SendMessage(this, EventID.ON_GET_FREE_LUCKY_BOX_GOLD, this, 0);

            return;
        }

        if (CacheGame.GetTotalCoin() < SMALL_GOLD_COST)
        {
            //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_coin, false, 1.5f);
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
            return;
        }

        if (isClickBtnOpenBox) return;

        PanelCoinGem.Instance.AddCoinTop(-SMALL_GOLD_COST, FirebaseLogSpaceWar.MysticChest_why);
        GetRewardsByGold(1);

        panelReward.ShowPopupOneReward(ResourceType.Gold);
        isClickBtnOpenBox = true;

        //Debug.LogError("small_gold");
    }

    public void OnclickBtnBigGold()
    {
        SoundManager.PlayClickButton();

        if (CacheGame.GetTotalCoin() < BIG_GOLD_COST)
        {
            //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_coin, false, 1.5f);
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gold);
            return;
        }

        if (isClickBtnOpenBox) return;

        PanelCoinGem.Instance.AddCoinTop(-BIG_GOLD_COST, FirebaseLogSpaceWar.MysticChest_why);
        GetRewardsByGold(10);

        panelReward.ShowPopupListReward(ResourceType.Gold);
        isClickBtnOpenBox = true;
        new CSOpenLuckyBoxGoldX10().Send();

        //Debug.LogError("big_gold");
    }

    public void OnClickBtnSmallGem()
    {
        SoundManager.PlayClickButton();

        if (CacheGame.GetTotalGem() < SMALL_GEM_COST)
        {
            //PopupManagerCuong.Instance.Show.TextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_gem, false, 1.5f);
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
            return;
        }

        if (isClickBtnOpenBox) return;

        PanelCoinGem.Instance.AddGemTop(-SMALL_GEM_COST, FirebaseLogSpaceWar.MysticChest_why);
        GetRewardsByGem(1);

        panelReward.ShowPopupOneReward(ResourceType.Gem);
        isClickBtnOpenBox = true;

        //Debug.LogError("small_gem");
    }

    public void OnClickBtnBigGem()
    {
        SoundManager.PlayClickButton();

        if (CacheGame.GetTotalGem() < BIG_GEM_COST)
        {
            //PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_not_enough_gem, false, 1.5f);
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
            return;
        }

        if (isClickBtnOpenBox) return;

        PanelCoinGem.Instance.AddGemTop(-BIG_GEM_COST, FirebaseLogSpaceWar.MysticChest_why);
        GetRewardsByGem(10);

        panelReward.ShowPopupListReward(ResourceType.Gem);
        isClickBtnOpenBox = true;

        //Debug.LogError("big_gem");
    }

    private void GetRewardsByGold(int amount)
    {
        if (amount == 1)
        {
            int rewardIndex = GetRandomIndexGold();
            string rewardName = LuckyBoxGoldSheet.Get(rewardIndex).name;
            int rewardAmount = NeedRewardSpecialAmount() ? LuckyBoxGoldSheet.Get(rewardIndex).special_amount : LuckyBoxGoldSheet.Get(rewardIndex).amount;
            bool isValuable = LuckyBoxGoldSheet.Get(rewardIndex).valuable == 1;

            item.CreateItem(rewardName, rewardAmount, isValuable);
            GameContext.AddGiftToPlayer(rewardName, rewardAmount, true, FirebaseLogSpaceWar.MysticChest_why);

            return;
        }

        for (int i = 0; i < amount; i++)
        {
            int rewardIndex = GetRandomIndexGold();
            string rewardName = LuckyBoxGoldSheet.Get(rewardIndex).name;
            int rewardAmount = NeedRewardSpecialAmount() ? LuckyBoxGoldSheet.Get(rewardIndex).special_amount : LuckyBoxGoldSheet.Get(rewardIndex).amount;
            bool isValuable = LuckyBoxGoldSheet.Get(rewardIndex).valuable == 1;

            listItem[i].CreateItem(rewardName, rewardAmount, isValuable);

            GameContext.AddGiftToPlayer(rewardName, rewardAmount, true, FirebaseLogSpaceWar.MysticChest_why);
        }
    }

    private void GetRewardsByGem(int amount)
    {
        if (amount == 1)
        {
            int rewardIndex = GetRandomIndexGem();
            string rewardName = LuckyBoxGemSheet.Get(rewardIndex).name;
            int rewardAmount = NeedRewardSpecialAmount() ? LuckyBoxGemSheet.Get(rewardIndex).special_amount : LuckyBoxGemSheet.Get(rewardIndex).amount;
            bool isValuable = LuckyBoxGemSheet.Get(rewardIndex).valuable == 1;

            item.CreateItem(rewardName, rewardAmount, isValuable);
            GameContext.AddGiftToPlayer(rewardName, rewardAmount, true, FirebaseLogSpaceWar.MysticChest_why);
            return;
        }


        for (int i = 0; i < amount; i++)
        {
            int rewardIndex = GetRandomIndexGem();
            string rewardName = LuckyBoxGemSheet.Get(rewardIndex).name;
            int rewardAmount = NeedRewardSpecialAmount() ? LuckyBoxGemSheet.Get(rewardIndex).special_amount : LuckyBoxGemSheet.Get(rewardIndex).amount;
            bool isValuable = LuckyBoxGemSheet.Get(rewardIndex).valuable == 1;

            listItem[i].CreateItem(rewardName, rewardAmount, isValuable);
            GameContext.AddGiftToPlayer(rewardName, rewardAmount, true, FirebaseLogSpaceWar.MysticChest_why);
        }
    }

    private int GetRandomIndexGold()
    {
        float totalPercent = UnityEngine.Random.Range(0f, 100f);

        for (int i = 1; i <= LuckyBoxGoldSheet.GetDictionary().Count; i++)
        {
            if (totalPercent <= LuckyBoxGoldSheet.Get(i).total_percent)
            {
                return i;
            }
        }

        return 1;
    }

    private int GetRandomIndexGem()
    {
        float totalPercent = UnityEngine.Random.Range(0f, 100f);

        for (int i = 1; i <= LuckyBoxGemSheet.GetDictionary().Count; i++)
        {
            if (totalPercent <= LuckyBoxGemSheet.Get(i).total_percent)
            {
                return i;
            }
        }

        return 1;
    }

    private bool NeedRewardSpecialAmount()
    {
        return false;
        //DateTime endDate = DateTime.Now;
        //DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out endDate);

        ////Debug.LogError("CachePvp.dateTime" + CachePvp.dateTime + ", " + "EndDate: " + endDate);

        //if (CachePvp.dateTime < endDate && DateTime.Now < endDate)
        //{
        //    return true;
        //}
        //else
        //{
        //    return false;
        //}
    }

    public void OnClickBtnBackOnDevice()
    {
        SoundManager.PlayClickButton();

        if (panelReward.gameObject.activeInHierarchy)
        {
            return;
        }

        if (panelRewardsInfo.gameObject.activeInHierarchy)
        {
            panelRewardsInfo.gameObject.SetActive(false);
        }
        else
        {
            popupLuckyBoxInvi.SetActive(false);
            PopupManager.Instance.ShowChatWorldPopup();
        }
    }

    public void ShowGoldPanelRewardsInfo()
    {
        SoundManager.PlayClickButton();
        panelRewardsInfo.ShowGoldRewardsInfo();
    }

    public void ShowGemPanelRewardsInfo()
    {
        SoundManager.PlayClickButton();
        panelRewardsInfo.ShowGemRewardsInfo();
    }

    IEnumerator coCountdown;
    IEnumerator CoCountDown()
    {
        DateTime expiredTime = MinhCacheGame.GetLuckyBoxExpiredDate();

        while (DateTime.Now < expiredTime)
        {
            TimeSpan interval = expiredTime - DateTime.Now;
            string str = string.Format("{0:00}:{1:00}:{2:00}", interval.Hours + interval.Days * 24, interval.Minutes, interval.Seconds);
            lCountDown.text = I2.Loc.ScriptLocalization.msg_event_will_end_in.Replace("%{duration_number}", "[00FF00]" + str + "[-]");
            lCountDownBtn.text = str;
            yield return new WaitForSeconds(1f);
        }

        lCountDown.text = I2.Loc.ScriptLocalization.msg_event_will_end_in.Replace("%{duration_number}", "[00ff00]00:00:00[-]");
        btnLuckyBox.SetActive(false);
    }
}
