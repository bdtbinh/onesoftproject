﻿using UnityEngine;
using I2.Loc;

public class LoadingSceneTooltip : MonoBehaviour {

    [SerializeField]
    private UILabel lToolTip;

    private void OnEnable()
    {
        int index = Random.Range(1, 12);

        switch (index)
        {
            case 1:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_1;
                break;

            case 2:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_2;
                break;

            case 3:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_3;
                break;

            case 4:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_4;
                break;

            case 5:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_5;
                break;

            case 6:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_6;
                break;

            case 7:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_7;
                break;

            case 8:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_8;
                break;

            case 9:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_9;
                break;

            case 10:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_10;
                break;

            default:
                lToolTip.text = ScriptLocalization.tooltip_loading_scene_11;
                break;
        }
    }
}
