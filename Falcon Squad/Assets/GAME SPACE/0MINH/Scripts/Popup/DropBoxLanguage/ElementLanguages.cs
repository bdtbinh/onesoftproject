﻿using UnityEngine;
using com.ootii.Messages;
using Mp.Pvp;
using TCore;

public class ElementLanguages : MonoBehaviour
{

    [SerializeField]
    private UISprite sBackground;

    [SerializeField]
    private UILabel lLanguage;

    [SerializeField]
    private UISprite sFlag;

    public string language;

    public void CreateItem(Color color, string language)
    {
        sBackground.color = color;
        this.language = language;
        lLanguage.text = ChangeLanguageManager.GetLanguageConverted(language);
        sFlag.spriteName = SpriteNameConst.GetLanguageFlagCode(language);
    }

    public void OnClickLanguage()
    {
        SoundManager.PlayClickButton();
        new CSChangeLanguage(sFlag.spriteName).Send();

        MessageDispatcher.SendMessage(this, EventID.ON_CLICK_CHANGE_LANGUAGE, language, 0);
    }

    
}
