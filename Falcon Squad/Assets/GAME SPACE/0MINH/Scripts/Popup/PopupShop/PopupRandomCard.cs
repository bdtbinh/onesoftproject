﻿using com.ootii.Messages;
using Mp.Pvp;
using System;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class PopupRandomCard : MonoBehaviour
{

    [SerializeField]
    private ElementRandomCard[] elementRandomCards;

    [SerializeField]
    private UILabel lPriceReset;

    [SerializeField]
    private UILabel lResetTime;

    [SerializeField]
    private GameObject fxLoading;

    [SerializeField]
    private GameObject canReset;

    [SerializeField]
    private GameObject cannotReset;

    [SerializeField]
    private GameObject showHideObjects;

    private int resetPrice;
    private RandomShop randomShop;
    private bool canClickBtnReset = true;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.RandomShop.MyRandomShop.ToString(), OnMyRandomShop, true);
        MessageDispatcher.AddListener(EventName.RandomShop.RandomShopReset.ToString(), OnRandomShopReset, true);

        fxLoading.SetActive(true);
        showHideObjects.SetActive(false);

        new CSMyRandomShop().Send();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.RandomShop.MyRandomShop.ToString(), OnMyRandomShop, true);
        MessageDispatcher.RemoveListener(EventName.RandomShop.RandomShopReset.ToString(), OnRandomShopReset, true);

        fxLoading.SetActive(false);

        if (coCountdown != null)
            StopCoroutine(coCountdown);
    }

    private void OnMyRandomShop(IMessage rMessage)
    {
        SCMyRandomShop mrs = rMessage.Data as SCMyRandomShop;
        if (mrs.status == OSNet.SCMessage.SUCCESS)
        {
            fxLoading.SetActive(false);
            showHideObjects.SetActive(true);

            randomShop = mrs.shop;
            ReloadShop(randomShop);
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(mrs.message, false, 1.5f);
        }
    }

    private void OnRandomShopReset(IMessage rMessage)
    {
        SCRandomShopReset rsr = rMessage.Data as SCRandomShopReset;
        switch (rsr.status)
        {
            case OSNet.SCMessage.SUCCESS:
                PanelCoinGem.Instance.AddGemTop(-randomShop.resetPrice, FirebaseLogSpaceWar.RandomCardShop_Why, "RandomCardShopReset");
                randomShop = rsr.shop;
                ReloadShop(randomShop);
                break;

            case SCShopReset.NOT_ENOUGH_RESET_QUOTA:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_enough_reset_quota, false, 1.5f);
                break;

            default:
                PopupManagerCuong.Instance.ShowTextNotifiToast(rsr.message, false, 1.5f);
                break;
        }

        canClickBtnReset = true;
    }

    void ReloadShop(RandomShop randomShop)
    {
        if (randomShop.resetRemain <= 0)
        {
            canReset.SetActive(false);
            cannotReset.SetActive(true);
        }
        else
        {
            canReset.SetActive(true);
            cannotReset.SetActive(false);

            lPriceReset.text = GameContext.FormatNumber(randomShop.resetPrice);
            resetPrice = randomShop.resetPrice;
        }

        if (coCountdown != null)
        {
            StopCoroutine(coCountdown);
        }

        coCountdown = Countdown(randomShop.timeToReset);
        StartCoroutine(coCountdown);

        List<RandomShopItem> items = randomShop.shopItems;

        for (int i = 0; i < elementRandomCards.Length; i++)
        {
            if (i < items.Count)
            {
                elementRandomCards[i].gameObject.SetActive(true);
                elementRandomCards[i].CreateItem(items[i].id, items[i].name, items[i].quantity, items[i].price, items[i].extraAmount, items[i].sale, items[i].soldOut);
            }
            else
            {
                elementRandomCards[i].gameObject.SetActive(false);
            }
        }
    }

    public void OnClickBtnReset()
    {
        SoundManager.PlayClickButton();

        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }

        if (randomShop.resetRemain == 0)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_have_used_reset, false, 1.5f);
            return;
        }

        if (CacheGame.GetTotalGem() >= resetPrice)
        {
            if (canClickBtnReset)
            {
                new CSRandomShopReset().Send();
                canClickBtnReset = false;
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
        }
    }

    IEnumerator coCountdown;
    IEnumerator Countdown(float time)
    {
        DateTime endDate = DateTime.Now.AddSeconds(time);

        while (endDate > DateTime.Now)
        {
            TimeSpan interval = endDate - DateTime.Now;
            string resetTime = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
            lResetTime.text = I2.Loc.ScriptLocalization.reset_in.Replace("%{time}", resetTime);
            yield return new WaitForSeconds(1f);
        }

        fxLoading.SetActive(true);
        showHideObjects.SetActive(false);
    }
}
