﻿using com.ootii.Messages;
using Mp.Pvp;
using TCore;
using UnityEngine;

public class ElementRandomCard : MonoBehaviour
{

    [SerializeField]
    private UISprite sItem;

    [SerializeField]
    private UILabel lQuantity;

    [SerializeField]
    private UILabel lQuantitySale;

    [SerializeField]
    private UILabel lPrice;

    [SerializeField]
    private GameObject enableBtnBuy;

    [SerializeField]
    private GameObject disableBtnBuy;

    [SerializeField]
    private GameObject saleObjects;

    private int id;
    private string itemName;
    private int finalQuantity;
    private int price;
    private bool soldOut;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.RandomShop.RandomShopBuyItem.ToString(), OnRandomShopBuyItem, true);
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.RandomShop.RandomShopBuyItem.ToString(), OnRandomShopBuyItem, true);
    }

    private void OnRandomShopBuyItem(IMessage rMessage)
    {
        SCRandomShopBuyItem rsbi = rMessage.Data as SCRandomShopBuyItem;

        if (rsbi.itemId != id)
            return;

        //Debug.LogError("ItemID: " + rsbi.itemId + " * " + id + ", status: " + rsbi.status);
        switch (rsbi.status)
        {
            case OSNet.SCMessage.SUCCESS:
                PopupManagerCuong.Instance.ShowItemRewardPopup();
                PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
                popupItemRewardIns.ShowListItem(1);

                GameContext.AddGiftToPlayer(itemName, finalQuantity, true, FirebaseLogSpaceWar.RandomCardShop_Why, sItem.spriteName);
                popupItemRewardIns.ShowOneItem(1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

                PanelCoinGem.Instance.AddGemTop(-price, FirebaseLogSpaceWar.RandomCardShop_Why, sItem.spriteName);

                enableBtnBuy.SetActive(false);
                disableBtnBuy.SetActive(true);
                break;

            case SCShopBuyItem.ITEM_NOT_IN_SHOP:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_item_not_in_shop, false, 1.5f);
                break;
            case SCShopBuyItem.ITEM_SOLD_OUT:
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_item_sold_out, false, 1.5f);
                break;

            default:
                PopupManagerCuong.Instance.ShowTextNotifiToast(rsbi.message, false, 1.5f);
                break;
        }
    }

    public void CreateItem(int id, string name, int quantity, int price, int extraAmount, bool sale, bool soldOut)
    {
        //Debug.LogError("id: " + id + ", name: " + name + ", quantity: " + quantity + ", price: " + price + ", extraAmount: " + extraAmount + ", sale: " + sale + ", soldOut: " + soldOut);

        this.id = id;
        itemName = name;
        finalQuantity = sale ? quantity + extraAmount : quantity;
        this.price = price;
        this.soldOut = soldOut;

        sItem.spriteName = "Card_" + this.id;
        lQuantity.text = "+" + quantity.ToString();
        lQuantitySale.text = finalQuantity.ToString();
        lPrice.text = this.price.ToString();

        enableBtnBuy.SetActive(!this.soldOut);
        disableBtnBuy.SetActive(this.soldOut);

        saleObjects.SetActive(sale);
    }

    public void OnClickBtnBuy()
    {
        SoundManager.PlayClickButton();

        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }

        if (soldOut)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_item_sold_out, false, 1.5f);
            return;
        }

        if (CacheGame.GetTotalGem() >= price)
        {
            if (PanelCoinGem.Instance != null)
            {
                new CSRandomShopBuyItem(id).Send();
                //Debug.LogError("OnClickBtnBuy: " + id);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowQuestOpenIAPPopup(ResourceType.Gem);
        }
    }
}
