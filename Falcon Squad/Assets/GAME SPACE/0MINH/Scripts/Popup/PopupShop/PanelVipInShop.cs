﻿using UnityEngine;
using I2.Loc;
using com.ootii.Messages;
using TCore;

public class PanelVipInShop : MonoBehaviour
{

    [SerializeField]
    private UILabel lNotVip;

    [SerializeField]
    private UILabel lMorePoints;

    [SerializeField]
    private UILabel lProgressBar;

    [SerializeField]
    private UISprite sVipIcon;

    [SerializeField]
    private UISprite sProgressBar;

    private void Start()
    {
        MessageDispatcher.AddListener(EventName.ChangeProperties.ChangeVip.ToString(), OnChangeVipPoint, true);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.ChangeProperties.ChangeVip.ToString(), OnChangeVipPoint, true);
    }

    private void OnChangeVipPoint(IMessage msg)
    {
        ReloadUI();
    }

    private void OnEnable()
    {
        ReloadUI();
    }

    void ReloadUI()
    {
        if (CachePvp.myVipPoint < 1)
        {
            lNotVip.gameObject.SetActive(true);
            lMorePoints.gameObject.SetActive(false);
            sProgressBar.transform.parent.gameObject.SetActive(false);

            lNotVip.text = ScriptLocalization.purchaseToGetVip.Replace("VIP", "[FFFF00]VIP[-]");
            sVipIcon.spriteName = "btn_VIP";
        }
        else
        {
            lNotVip.gameObject.SetActive(false);
            lMorePoints.gameObject.SetActive(true);
            sProgressBar.transform.parent.gameObject.SetActive(true);

            //lMorePoints.text = ScriptLocalization.msg_more_point_to_vip.Replace("")
            sVipIcon.spriteName = "Icon__VIP_" + (CachePvp.myVip - 1);

            if (CachePvp.myVip >= PopupVip.listTarget.Count)
            {
                lMorePoints.text = ScriptLocalization.msg_reach_max_vip;

                float fillAmount = CachePvp.myVipPoint * 1.0f / PopupVip.listTarget[PopupVip.listTarget.Count - 1];
                lProgressBar.text = CachePvp.myVipPoint + "/" + PopupVip.listTarget[PopupVip.listTarget.Count - 1];
                sProgressBar.fillAmount = fillAmount;
            }
            else
            {
                string moreVipPoint = ScriptLocalization.msg_more_point_to_vip;
                lMorePoints.text = moreVipPoint.Replace("%{point_number}", "[ffff00]" + (PopupVip.listTarget[CachePvp.myVip] - CachePvp.myVipPoint).ToString() + "[-]").Replace("%{vip_number}", "[ffff00]" + (CachePvp.myVip + 1).ToString() + "[-]");

                float fillAmount = CachePvp.myVipPoint * 1.0f / PopupVip.listTarget[CachePvp.myVip];
                lProgressBar.text = CachePvp.myVipPoint + "/" + PopupVip.listTarget[CachePvp.myVip];
                sProgressBar.fillAmount = fillAmount;
            }

        }
    }

    public void OnClickBtnInfo()
    {
        SoundManager.PlayClickButton();
        PopupManager.Instance.ShowVipPopup(false);
        PopupManager.Instance.ChangePopupVipDepth(12000);
    }
}
