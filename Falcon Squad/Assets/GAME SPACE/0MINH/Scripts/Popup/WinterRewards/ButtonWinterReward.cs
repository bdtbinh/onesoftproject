﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPromoteEvent : MonoBehaviour
{

    [SerializeField]
    private UILabel lCountdown;

    [SerializeField]
    private GameObject insideObjects;

    void Awake()
    {
        DateTime expiredTime = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayFinishPromoteEvent, out expiredTime);

        //Debug.LogError(CachePvp.dateTime + " * " + expiredTime);

        if (CachePvp.dateTime >= expiredTime || DateTime.Now >= expiredTime)
        {
            gameObject.SetActive(false);
        }
        else
        {
            insideObjects.SetActive(true);
            StartCoroutine(CountDownEndTime());
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    IEnumerator CountDownEndTime()
    {
        DateTime expiredTime = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayFinishPromoteEvent, out expiredTime);

        while (DateTime.Now < expiredTime)
        {
            TimeSpan interval = expiredTime - DateTime.Now;

            string time = "";

            if (interval.Days > 0)
                //time = "[FF0000FF]" + string.Format("{0:00} days", interval.Days) + "[-]";
                time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
            else
                time = "[FF0000FF]" + string.Format("{0:00}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds) + "[-]";

            lCountdown.text = time;
            yield return new WaitForSeconds(1f);
        }

        gameObject.SetActive(false);
        //Debug.LogError("aaaa2: " + CachePvp.dateTime + " * " + expiredTime + " * " + DateTime.Now);
    }
}
