﻿using UnityEngine;
using System;
using TCore;
using UnityEngine.SceneManagement;

public class PopupNoticeInTrial : MonoBehaviour {

    private enum NoticeInTrialType
    {
        Aircraft,
        Wingman,
        Wing
    }

    private NoticeInTrialType noticeType;

    private AircraftTypeEnum aircraftType;
    private WingmanTypeEnum wingmanType;
    private WingTypeEnum wingType;
    private WingmanPosType wingmanPosType;

    [SerializeField]
    private UILabel lMessage;

    public void ShowPopupAircraftNotice(AircraftTypeEnum aircraftType)
    {
        lMessage.text = I2.Loc.ScriptLocalization.msg_notice_in_trial.Replace("%{name}", HangarValue.AircraftNames(aircraftType));

        noticeType = NoticeInTrialType.Aircraft;
        this.aircraftType = aircraftType;

        gameObject.SetActive(true);
    }

    public void ShowPopupWingmanNotice(WingmanTypeEnum wingmanType, WingmanPosType wingmanPosType)
    {
        lMessage.text = I2.Loc.ScriptLocalization.msg_notice_in_trial.Replace("%{name}", HangarValue.WingmanNames(wingmanType));

        noticeType = NoticeInTrialType.Wingman;
        this.wingmanType = wingmanType;
        this.wingmanPosType = wingmanPosType;

        gameObject.SetActive(true);
    }

    public void ShowPopupWingNotice(WingTypeEnum wingType)
    {
        lMessage.text = I2.Loc.ScriptLocalization.msg_notice_in_trial.Replace("%{name}", HangarValue.WingNames(wingType));

        noticeType = NoticeInTrialType.Wing;
        this.wingType = wingType;

        gameObject.SetActive(true);
    }

    public void OnClickBtnConfirm()
    {
        if(noticeType == NoticeInTrialType.Aircraft)
        {
            //CacheGame.SetCurrentLevel(0);
            SoundManager.PlayClickButton();
            CacheGame.SetSpaceShipUserTry((int)aircraftType);

            LoadingSceneManager.Instance.LoadSceneMainUI("LevelTryPlane");

            GameContext.modeGamePlay = GameContext.ModeGamePlay.TryPlane;

            if (SceneManager.GetActiveScene().name == "SelectLevel")
            {
                GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.SelectLevel;
            }
            else if (SceneManager.GetActiveScene().name == "Home")
            {
                if (PopupManager.Instance.IsTournamentPopupActive())
                {
                    GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.Tournament;
                }
                else
                {
                    GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.Home;
                }
            }

            GameContext.typeOpenPopupFromTry = GameContext.TypeOpenPopupFromTry.Aircraft;
        }
        else if(noticeType == NoticeInTrialType.Wingman)
        {
            CacheGame.SetSpaceShipUserTry(CacheGame.GetSpaceShipUserSelected());

            if (wingmanPosType == WingmanPosType.LeftWingman)
            {
                GameContext.typeTryWingman = GameContext.TypeTryWingman.Left;
            }
            else
            {
                GameContext.typeTryWingman = GameContext.TypeTryWingman.Right;
            }


            CacheGame.SetWingManUserTry((int)wingmanType);

            LoadingSceneManager.Instance.LoadSceneMainUI("LevelTryPlane");

            GameContext.modeGamePlay = GameContext.ModeGamePlay.TryPlane;

            if (SceneManager.GetActiveScene().name == "SelectLevel")
            {
                GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.SelectLevel;
            }
            else if (SceneManager.GetActiveScene().name == "Home")
            {
                if (PopupManager.Instance.IsTournamentPopupActive())
                {
                    GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.Tournament;
                }
                else
                {
                    GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.Home;
                }
            }

            GameContext.typeOpenPopupFromTry = GameContext.TypeOpenPopupFromTry.Wingman;
        }
        else
        {
            SoundManager.PlayClickButton();
            CacheGame.SetWingUserTry((int)wingType);

            LoadingSceneManager.Instance.LoadSceneMainUI("LevelTryPlane");

            GameContext.modeGamePlay = GameContext.ModeGamePlay.TryPlane;

            if (SceneManager.GetActiveScene().name == "SelectLevel")
            {
                GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.SelectLevel;
            }
            else if (SceneManager.GetActiveScene().name == "Home")
            {
                if (PopupManager.Instance.IsTournamentPopupActive())
                {
                    GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.Tournament;
                }
                else
                {
                    GameContext.typeOpenSceneFromTry = GameContext.TypeOpenSceneFromTry.Home;
                }
            }

            GameContext.typeOpenPopupFromTry = GameContext.TypeOpenPopupFromTry.Wing;
        }
    }

    public void OnClickBtnClose()
    {
        gameObject.SetActive(false);
    }


}
