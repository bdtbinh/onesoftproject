﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupRate : MonoBehaviour
{

    //[SerializeField]
    //private GameObject popupRate;

    [SerializeField]
    private GameObject popupThanks;

    public void OnClick14Stars()
    {
        MinhCacheGame.SetAlreadyRate();
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.RateGame.ToString(), 2);
        ShowPopupThanks();
        CachePvp.Instance.LogStarRateToServer(1);
    }

    public void OnClick5Stars()
    {
        MinhCacheGame.SetAlreadyRate();
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.RateGame.ToString(), 2);
        gameObject.SetActive(false);
        CachePvp.Instance.LogStarRateToServer(5);
#if UNITY_ANDROID
        string bundleID = "" + Application.identifier;
        Application.OpenURL("market://details?id=" + bundleID);
#elif UNITY_IPHONE
        if (GameContext.IS_CHINA_VERSION)
        {
            Application.OpenURL("itms-apps://itunes.apple.com/app/id1448799684");
        }
        else
        {
            Application.OpenURL("itms-apps://itunes.apple.com/app/id1337514468");
        }
#endif

    }

    public void PressButtonBack()
    {
        gameObject.SetActive(false);
    }

    private void ShowPopupThanks()
    {
        //popupRate.SetActive(false);
        popupThanks.SetActive(true);
    }

    public void ClosePopupThanks()
    {

        //popupThanks.SetActive(false);
        gameObject.SetActive(false);
    }

    private bool IsTheSameDay()
    {
        return DateTime.Now.Day == MinhCacheGame.GetLastDayRate();
    }



    //Phan cua Cuong
    //Check xem da rate lan nao chua de hien popup rate
    //public bool CanShowPopupRate()
    //{
    //    if (Application.internetReachability == NetworkReachability.NotReachable)
    //    {
    //        print("Network not found");
    //        return false;
    //    }

    //    if (DateTime.Now.Day == MinhCacheGame.GetLastDayRate())
    //    {
    //        print("Is the same day");
    //        return false;
    //    }

    //    if (MinhCacheGame.GetDayCountRate() >= 3)
    //    {
    //        print("DayCount > 3");
    //        return false;
    //    }

    //    return !MinhCacheGame.IsAlreadyRate();
    //}

    public void ShowPopupRate()
    {
        gameObject.SetActive(true);
        MinhCacheGame.SetLastDayRate();
        MinhCacheGame.SetDayCountRate(MinhCacheGame.GetDayCountRate() + 1);
    }




}
