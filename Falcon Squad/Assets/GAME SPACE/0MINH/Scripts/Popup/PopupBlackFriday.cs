﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;

public class PopupBlackFriday : MonoBehaviour
{

    [SerializeField]
    private UILabel lUnlockCard;

    [SerializeField]
    private UILabel lCountDown;

    [SerializeField]
    private GameObject btnBlackFriday;

    [SerializeField]
    private GameObject popupBlackFridayInvi;

    private void Start()
    {
        lUnlockCard.text = ScriptLocalization.msg_unlock_aircraft_by_collect_card.Replace("%{aircraft_name}", HangarValue.AircraftNames(AircraftTypeEnum.StarBomb));

        DateTime expiredTime = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out expiredTime);


        if (CachePvp.dateTime >= expiredTime || DateTime.Now >= expiredTime)
        {
            btnBlackFriday.SetActive(false);
        }
        else
        {
            btnBlackFriday.SetActive(true);
        }

        StartCoroutine(CountDown());
    }

    public void ShowPopup()
    {
        SoundManager.PlayShowPopup();
        popupBlackFridayInvi.SetActive(true);
    }

    public void OnClickBtnBuy()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.ShowShopPopup();
        if (PopupShop.Instance != null)
        {
            PopupShop.Instance.ShowPanelInShop(TypePanelShowInShop.ComboEvent);
        }
        popupBlackFridayInvi.SetActive(false);
    }

    public void OnClickBtnChest()
    {
        SoundManager.PlayClickButton();
        HomeScene.Instance.popupLuckyBox_Invi.transform.parent.GetComponent<PopupLuckyBox>().ShowPopup();
        popupBlackFridayInvi.SetActive(false);
    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        popupBlackFridayInvi.SetActive(false);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    IEnumerator CountDown()
    {
        DateTime expiredTime = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out expiredTime);

        while (DateTime.Now < expiredTime)
        {
            TimeSpan interval = expiredTime - DateTime.Now;

            string time = "";

            if (interval.Days > 0)
                time = "[FF0000]" + string.Format("{0:0} days:{1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
            else
                time = "[FF0000]" + string.Format("{0:0}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds) + "[-]";

            lCountDown.text = ScriptLocalization.msg_premium_pack_time_expired.Replace("%{time}", time);
            yield return new WaitForSeconds(1f);
        }


        btnBlackFriday.SetActive(false);
        popupBlackFridayInvi.SetActive(false);

    }
}
