﻿using UnityEngine;
using I2.Loc;
using TCore;

public class PopupItemInfo : MonoBehaviour
{
    [SerializeField]
    private UISprite sIcon;

    [SerializeField]
    private UILabel lName;

    [SerializeField]
    private UILabel lQuantity;

    [SerializeField]
    private UILabel lDescription;

    public void ShowPopup(string itemInfoStringType, int quantity)
    {
        SoundManager.PlayShowPopup();

        sIcon.spriteName = HangarValue.ItemSpriteName[itemInfoStringType];

        if(quantity > 0)
        {
            lQuantity.gameObject.SetActive(true);
            lQuantity.text = "[A6A6A6]" + ScriptLocalization.quantity_key.Replace("%{number}", "[FFFFFF]" + quantity.ToString() + "[-]") + "[-]";
        }
        else
        {
            lQuantity.gameObject.SetActive(false);
        }

        switch (itemInfoStringType)
        {
            case ItemInfoStringType.Gold:
                lName.text = ScriptLocalization.gold;
                lDescription.text = ScriptLocalization.info_item_gold;
                break;

            case ItemInfoStringType.Gold1:
                lName.text = ScriptLocalization.gold;
                lDescription.text = ScriptLocalization.info_item_gold;
                break;

            case ItemInfoStringType.Gold2:
                lName.text = ScriptLocalization.gold;
                lDescription.text = ScriptLocalization.info_item_gold;
                break;

            case ItemInfoStringType.Gold3:
                lName.text = ScriptLocalization.gold;
                lDescription.text = ScriptLocalization.info_item_gold;
                break;

            case ItemInfoStringType.Gem:
                lName.text = ScriptLocalization.gem;
                lDescription.text = ScriptLocalization.info_item_gem;
                break;

            case ItemInfoStringType.Life:
                lName.text = ScriptLocalization.life;
                lDescription.text = ScriptLocalization.info_item_life;
                break;

            case ItemInfoStringType.ActiveSkill:
                lName.text = ScriptLocalization.ActiveSkill;
                lDescription.text = ScriptLocalization.info_item_active_skill;
                break;

            case ItemInfoStringType.PowerUp:
                lName.text = ScriptLocalization.power_up;
                lDescription.text = ScriptLocalization.info_item_power_up;
                break;

            case ItemInfoStringType.BoxCard:
                lName.text = ScriptLocalization.item_name_card_box;
                lDescription.text = ScriptLocalization.info_item_card_box;
                break;

            case ItemInfoStringType.BoxCard1:
                lName.text = ScriptLocalization.item_name_card_box;
                lDescription.text = ScriptLocalization.info_item_card_box;
                break;

            case ItemInfoStringType.BoxCard2:
                lName.text = ScriptLocalization.item_name_card_box;
                lDescription.text = ScriptLocalization.info_item_card_box;
                break;

            case ItemInfoStringType.BoxCard3:
                lName.text = ScriptLocalization.item_name_card_box;
                lDescription.text = ScriptLocalization.info_item_card_box;
                break;

            case ItemInfoStringType.BoxCard4:
                lName.text = ScriptLocalization.item_name_card_box;
                lDescription.text = ScriptLocalization.info_item_card_box;
                break;

            case ItemInfoStringType.AircraftGeneralCard:
                lName.text = ScriptLocalization.item_name_uni_aircraft_card;
                lDescription.text = ScriptLocalization.info_item_aircraft_general_card;
                break;

            case ItemInfoStringType.Aircraft1Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.AircraftNames(AircraftTypeEnum.BataFD));
                lDescription.text = ScriptLocalization.info_item_aircraft_specific_card.Replace("%{aircraft_name}", HangarValue.AircraftNames(AircraftTypeEnum.BataFD));
                break;

            case ItemInfoStringType.Aircraft2Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.AircraftNames(AircraftTypeEnum.SkyWraith));
                lDescription.text = ScriptLocalization.info_item_aircraft_specific_card.Replace("%{aircraft_name}", HangarValue.AircraftNames(AircraftTypeEnum.SkyWraith));
                break;

            case ItemInfoStringType.Aircraft3Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.AircraftNames(AircraftTypeEnum.FuryOfAres));
                lDescription.text = ScriptLocalization.info_item_aircraft_specific_card.Replace("%{aircraft_name}", HangarValue.AircraftNames(AircraftTypeEnum.FuryOfAres));
                break;

            case ItemInfoStringType.Aircraft6Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.AircraftNames(AircraftTypeEnum.MacBird));
                lDescription.text = ScriptLocalization.info_item_aircraft_specific_card.Replace("%{aircraft_name}", HangarValue.AircraftNames(AircraftTypeEnum.MacBird));
                break;

            case ItemInfoStringType.Aircraft7Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.AircraftNames(AircraftTypeEnum.TwilightX));
                lDescription.text = ScriptLocalization.info_item_aircraft_specific_card.Replace("%{aircraft_name}", HangarValue.AircraftNames(AircraftTypeEnum.TwilightX));
                break;

            case ItemInfoStringType.Aircraft8Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.AircraftNames(AircraftTypeEnum.StarBomb));
                lDescription.text = ScriptLocalization.info_item_aircraft_specific_card.Replace("%{aircraft_name}", HangarValue.AircraftNames(AircraftTypeEnum.StarBomb));
                break;

            case ItemInfoStringType.Aircraft9Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.AircraftNames(AircraftTypeEnum.IceShard));
                lDescription.text = ScriptLocalization.info_item_aircraft_specific_card.Replace("%{aircraft_name}", HangarValue.AircraftNames(AircraftTypeEnum.IceShard));
                break;

            case ItemInfoStringType.Aircraft10Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.AircraftNames(AircraftTypeEnum.ThunderBolt));
                lDescription.text = ScriptLocalization.info_item_aircraft_specific_card.Replace("%{aircraft_name}", HangarValue.AircraftNames(AircraftTypeEnum.ThunderBolt));
                break;

            case ItemInfoStringType.DroneGeneralCard:
                lName.text = ScriptLocalization.item_name_uni_drone_card;
                lDescription.text = ScriptLocalization.info_item_drone_general_card;
                break;

            case ItemInfoStringType.Drone1Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.WingmanNames(WingmanTypeEnum.GatlingGun));
                lDescription.text = ScriptLocalization.info_item_drone_specific_card.Replace("%{drone_name}", HangarValue.WingmanNames(WingmanTypeEnum.GatlingGun));
                break;

            case ItemInfoStringType.Drone2Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.WingmanNames(WingmanTypeEnum.AutoGatlingGun));
                lDescription.text = ScriptLocalization.info_item_drone_specific_card.Replace("%{drone_name}", HangarValue.WingmanNames(WingmanTypeEnum.AutoGatlingGun));
                break;

            case ItemInfoStringType.Drone3Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.WingmanNames(WingmanTypeEnum.Lazer));
                lDescription.text = ScriptLocalization.info_item_drone_specific_card.Replace("%{drone_name}", HangarValue.WingmanNames(WingmanTypeEnum.Lazer));
                break;

            case ItemInfoStringType.Drone4Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.WingmanNames(WingmanTypeEnum.DoubleGalting));
                lDescription.text = ScriptLocalization.info_item_drone_specific_card.Replace("%{drone_name}", HangarValue.WingmanNames(WingmanTypeEnum.DoubleGalting));
                break;

            case ItemInfoStringType.Drone5Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.WingmanNames(WingmanTypeEnum.HomingMissile));
                lDescription.text = ScriptLocalization.info_item_drone_specific_card.Replace("%{drone_name}", HangarValue.WingmanNames(WingmanTypeEnum.HomingMissile));
                break;

            case ItemInfoStringType.Drone6Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.WingmanNames(WingmanTypeEnum.Splasher));
                lDescription.text = ScriptLocalization.info_item_drone_specific_card.Replace("%{drone_name}", HangarValue.WingmanNames(WingmanTypeEnum.Splasher));
                break;

            case ItemInfoStringType.HalloweenCandy:
                break;

            case ItemInfoStringType.XMasBell:
                break;

            case ItemInfoStringType.WingGeneralCard:
                lName.text = ScriptLocalization.item_name_uni_wing_card;
                lDescription.text = ScriptLocalization.info_item_wing_general_card;
                break;

            case ItemInfoStringType.Wing1Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.WingNames(WingTypeEnum.WingOfJustice));
                lDescription.text = ScriptLocalization.info_item_wing_specific_card.Replace("%{wing_name}", HangarValue.WingNames(WingTypeEnum.WingOfJustice));
                break;

            case ItemInfoStringType.Wing2Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.WingNames(WingTypeEnum.WingOfRedemption));
                lDescription.text = ScriptLocalization.info_item_wing_specific_card.Replace("%{wing_name}", HangarValue.WingNames(WingTypeEnum.WingOfRedemption));
                break;

            case ItemInfoStringType.Wing3Card:
                lName.text = ScriptLocalization.item_name_card_replace.Replace("%{name}", HangarValue.WingNames(WingTypeEnum.WingOfResolution));
                lDescription.text = ScriptLocalization.info_item_wing_specific_card.Replace("%{wing_name}", HangarValue.WingNames(WingTypeEnum.WingOfResolution));
                break;

            case ItemInfoStringType.Energy:
                lName.text = ScriptLocalization.energy;
                lDescription.text = ScriptLocalization.info_item_energy;
                break;

            default:
                break;
        }

        gameObject.SetActive(true);
    }

    public void OnClickOutSide()
    {
        PopupManagerCuong.Instance.HideItemInfoPopup();
        SoundManager.PlayShowPopup();
    }

}
