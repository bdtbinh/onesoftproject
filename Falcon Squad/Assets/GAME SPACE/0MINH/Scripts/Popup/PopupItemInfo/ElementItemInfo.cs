﻿using TCore;
using UnityEngine;
using Sirenix.OdinInspector;

public class ElementItemInfo : MonoBehaviour
{
    [SerializeField]
    private bool useEditor = false;

    [SerializeField]
    [ShowIf("useEditor")]
    private ItemInfoEnumType itemInfoType;

    [SerializeField]
    [ShowIf("useEditor")]
    private int inputQuantity;

    [SerializeField]
    private UISprite sIcon;

    [SerializeField]
    private UILabel lQuantity;

    [SerializeField]
    private GameObject sIconEventTag;

    private string itemInfoStringType;
    private int quantity;

    [SerializeField]
    private GameObject isValueableEffect;

    public void CreateItem(string itemInfoStringType, int quantity, bool isValuable, bool needChangeIcon = true, bool needEventIcon = false)
    {
        this.itemInfoStringType = itemInfoStringType;

        if (sIcon != null && needChangeIcon)
        {
            sIcon.spriteName = HangarValue.ItemSpriteName[itemInfoStringType];
        }

        this.quantity = quantity;

        if (lQuantity != null)
        {
            if (quantity < 0)
            {
                lQuantity.gameObject.SetActive(false);
            }
            else
            {
                lQuantity.gameObject.SetActive(true);
                lQuantity.text = GameContext.FormatNumber(quantity);
            }
        }

        if (isValueableEffect != null)
            isValueableEffect.SetActive(isValuable);

        if(sIconEventTag != null)
            sIconEventTag.SetActive(needEventIcon);
    }



    public void UpdateQuantity(int quantity)
    {
        this.quantity = quantity;
        lQuantity.text = quantity.ToString();
    }

    public void OnClickItem()
    {
        SoundManager.PlayClickButton();

        if (useEditor)
        {
            PopupManagerCuong.Instance.ShowItemInfoPopup(itemInfoType.ToString(), inputQuantity);
        }
        else
        {
            PopupManagerCuong.Instance.ShowItemInfoPopup(itemInfoStringType, quantity);
        }
    }
}
