﻿using UnityEngine;
using Facebook.Unity;
using TCore;
using SkyGameKit;
using System;
using System.Collections;
using Mp.Pvp;
using com.ootii.Messages;
using I2.Loc;

public class PopupSocial : MonoBehaviour
{

    private const int OLD_LIKE_FB_REWARD = 2000;
    private const int OLD_JOIN_FB_REWARD = 1000;
    private const int OLD_INSTAGRAM_REWARD = 1000;
    private const int OLD_TWITTER_REWARD = 1000;

    private const int OLD_SHARE_FB_REWARD = 1000;
    private const int OLD_ONE_FRIEND_REWARD = 1000;
    private const int OLD_FIVE_FRIENDS_REWARD = 2000;
    private const int OLD_TEN_FRIENDS_REWARD = 4000;
    private const int OLD_TWENTY_FRIENDS_REWARD = 10000;


    private const int LIKE_FB_GOLD = 2000;
    private const int JOIN_FB_GOLD = 1000;
    private const int INSTAGRAM_GOLD = 1000;
    private const int TWITTER_GOLD = 1000;

    private const int SHARE_FB_GOLD = 1000;
    private const int ONE_FRIEND_GOLD = 1000;
    private const int FIVE_FRIENDS_GOLD = 2000;
    private const int TEN_FRIENDS_GOLD = 4000;
    private const int TWENTY_FRIENDS_GOLD = 10000;

    private const int LIKE_FB_GEM = 10;
    private const int JOIN_FB_GEM = 10;
    private const int INSTAGRAM_GEM = 10;
    private const int TWITTER_GEM = 10;

    private const int SHARE_FB_GEM = 10;
    private const int ONE_FRIEND_GEM = 4;
    private const int FIVE_FRIENDS_GEM = 20;
    private const int TEN_FRIENDS_GEM = 40;
    private const int TWENTY_FRIENDS_GEM = 100;

    [SerializeField]
    private GameObject popupSocialInvi;

    [SerializeField]
    private UILabel lFriends1;

    [SerializeField]
    private UILabel lFriends5;

    [SerializeField]
    private UILabel lFriends10;

    [SerializeField]
    private UILabel lFriends20;

    [SerializeField]
    private UILabel labelLikeFB;

    [SerializeField]
    private UISprite sLikeFB;

    [SerializeField]
    private GameObject giftLikeFB;

    [SerializeField]
    private UILabel labelJoinGroupFB;

    [SerializeField]
    private UISprite sJoinGroupFB;

    [SerializeField]
    private GameObject giftJoinGroupFB;

    //[SerializeField]
    //private UISprite sFollowInstagram;

    [SerializeField]
    private GameObject sIconFollowInstagram;

    //[SerializeField]
    //private UISprite sFollowTwitter;

    [SerializeField]
    private GameObject sIconFollowTwitter;

    [SerializeField]
    private UILabel labelTotalFriendsFB;

    [SerializeField]
    private UILabel labelFriendsFB1;

    [SerializeField]
    private UISprite sFriendsFB1;

    [SerializeField]
    private GameObject glowFriendsFB1;

    [SerializeField]
    private GameObject giftFriendsFB1;

    [SerializeField]
    private UILabel labelFriendsFB5;

    [SerializeField]
    private UISprite sFriendsFB5;

    [SerializeField]
    private GameObject glowFriendsFB5;

    [SerializeField]
    private GameObject giftFriendsFB5;

    [SerializeField]
    private UILabel labelFriendsFB10;

    [SerializeField]
    private UISprite sFriendsFB10;

    [SerializeField]
    private GameObject glowFriendsFB10;

    [SerializeField]
    private GameObject giftFriendsFB10;

    [SerializeField]
    private UILabel labelFriendsFB20;

    [SerializeField]
    private UISprite sFriendsFB20;

    [SerializeField]
    private GameObject glowFriendsFB20;

    [SerializeField]
    private GameObject giftFriendsFB20;

    [SerializeField]
    private UILabel labelShareFB;

    [SerializeField]
    private UISprite sShareFB;

    [SerializeField]

    private GameObject sIconShareGift;

    [SerializeField]
    private UILabel labelCountdownShareFB;

    //Thời gian đếm ngược sau mỗi lần share facebook (tính bằng giờ)
    [SerializeField]
    private float timeCountDownShareFB = 72f;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventName.LoadProfile.LoadFriendshipStats.ToString(), OnLoadFriendshipStats, true);
        //SocialManager.Instance.OnFacebookLoginSuccessed = null;
        //SocialManager.Instance.OnFacebookLoginFail = null;
        SocialManager.Instance.OnFacebookLoginSuccessed += OnFacebookLoginSuccessed;
        SocialManager.Instance.OnFacebookLoginFail += OnFacebookLoginFailed;
        SocialManager.Instance.OnLikeFBReward += OnLikeFBReward;
        SocialManager.Instance.OnJoinGroupFBReward += OnJoinGroupFBReward;
        SocialManager.Instance.OnFollowInstagramReward += OnFollowInstagramReward;
        SocialManager.Instance.OnFollowTwitterReward += OnFollowTwitterReward;
        SocialManager.Instance.OnFacebookShareSuccessed += OnFacebookShareSuccessed;

        MessageDispatcher.AddListener(EventName.LoginFacebook.LoggedFacebook.ToString(), OnLoggedFacebook, true);
    }

    private void OnLoadFriendshipStats(IMessage rMessage)
    {
        ReloadUI();
    }

    void OnLoggedFacebook(IMessage msg)
    {
        ReloadUI();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.LoadProfile.LoadFriendshipStats.ToString(), OnLoadFriendshipStats, true);
        SocialManager.Instance.OnFacebookLoginSuccessed -= OnFacebookLoginSuccessed;
        SocialManager.Instance.OnFacebookLoginFail -= OnFacebookLoginFailed;
        SocialManager.Instance.OnLikeFBReward -= OnLikeFBReward;
        SocialManager.Instance.OnJoinGroupFBReward -= OnJoinGroupFBReward;
        SocialManager.Instance.OnFollowInstagramReward -= OnFollowInstagramReward;
        SocialManager.Instance.OnFollowTwitterReward -= OnFollowTwitterReward;
        SocialManager.Instance.OnFacebookShareSuccessed -= OnFacebookShareSuccessed;

        MessageDispatcher.RemoveListener(EventName.LoginFacebook.LoggedFacebook.ToString(), OnLoggedFacebook, true);
    }

    private void OnFacebookLoginSuccessed()
    {
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_login_facebook_succeeded, true, 1.5f);
        ReloadUI();
        string query = "/me?fields=name,picture.width(135).height(135).type(normal)";
        FB.API(query, HttpMethod.GET, result =>
        {
            FBUser user = JsonFx.Json.JsonReader.Deserialize<FBUser>(result.RawResult);
            CachePvp.facebookUser = user;
            new CSValidateFacebookId(user.id).Send();
        });
        SocialManager.Instance.FBRequestFriendsData
            (
                () =>
                {
                    ReloadUI();
                },
                () =>
                {
                    ReloadUI();
                }
            );
    }

    private void OnFacebookLoginFailed()
    {
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_login_facebook_fail, false, 1.5f);
    }

    private void OnFacebookShareSuccessed()
    {
        (this).Delay(0.8f, (Action)(() =>
        {
            int goldReward = MinhCacheGame.GetVersion() > 0 ? SHARE_FB_GOLD : OLD_SHARE_FB_REWARD;

            CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.ShareFacebook.ToString(), 2);
            //DontDestroyManager.Instance.AddCoinTop(reward);
            if (PanelCoinGem.Instance != null)
            {
                MinhCacheGame.SetFacebookShareDate(CachePvp.dateTime);

                PanelCoinGem.Instance.AddCoinTop(goldReward, FirebaseLogSpaceWar.SocialReward_why);
                PanelCoinGem.Instance.AddGemTop(SHARE_FB_GEM, FirebaseLogSpaceWar.SocialReward_why);
                PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", Environment.NewLine + goldReward + " " + ScriptLocalization.gold + Environment.NewLine + SHARE_FB_GEM + " " + ScriptLocalization.gem), true);

                labelShareFB.text = ScriptLocalization.wait;
                sShareFB.spriteName = "btn_daily_grey";
            }
        }));


        //coCountdownShareFB = CountdownShareFB();
        //StartCoroutine(coCountdownShareFB);
    }

    private void OnLikeFBReward()
    {

        if (SocialManager.Instance.FBIsAlreadyLike())
            return;

        int goldReward = MinhCacheGame.GetVersion() > 0 ? LIKE_FB_GOLD : OLD_LIKE_FB_REWARD;

        (this).Delay(0.8f, (Action)(() =>
        {
            //DontDestroyManager.Instance.AddCoinTop(reward);
            if (PanelCoinGem.Instance != null)
            {
                SocialManager.Instance.FBSetAlreadyLike();
                PanelCoinGem.Instance.AddCoinTop(goldReward, FirebaseLogSpaceWar.SocialReward_why);
                PanelCoinGem.Instance.AddGemTop(LIKE_FB_GEM, FirebaseLogSpaceWar.SocialReward_why);
                giftLikeFB.gameObject.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", Environment.NewLine + goldReward + " " + ScriptLocalization.gold + Environment.NewLine + LIKE_FB_GEM + " " + ScriptLocalization.gem), true);
            }
        }));

        //labelLikeFB.text = txtDone;
        //sLikeFB.spriteName = "btn_daily_grey";
    }

    private void OnJoinGroupFBReward()
    {
        if (SocialManager.Instance.FBIsAlreadyJoinGroup())
            return;

        int goldReward = MinhCacheGame.GetVersion() > 0 ? JOIN_FB_GOLD : OLD_JOIN_FB_REWARD;

        (this).Delay(0.8f, (Action)(() =>
        {
            //DontDestroyManager.Instance.AddCoinTop(reward);
            if (PanelCoinGem.Instance != null)
            {
                SocialManager.Instance.FBSetAlreadyJoinGroup();
                PanelCoinGem.Instance.AddCoinTop(goldReward, FirebaseLogSpaceWar.SocialReward_why);
                PanelCoinGem.Instance.AddGemTop(JOIN_FB_GEM, FirebaseLogSpaceWar.SocialReward_why);
                giftJoinGroupFB.gameObject.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", Environment.NewLine + goldReward + " " + ScriptLocalization.gold + Environment.NewLine + JOIN_FB_GEM + " " + ScriptLocalization.gem), true);
            }
        }));

        //labelJoinGroupFB.text = txtDone;
        //sJoinGroupFB.spriteName = "btn_daily_grey";
    }

    private void OnFollowInstagramReward()
    {
        if (SocialManager.Instance.InstagramIsAlreadyFollow())
            return;

        int goldReward = MinhCacheGame.GetVersion() > 0 ? INSTAGRAM_GOLD : OLD_INSTAGRAM_REWARD;

        (this).Delay(0.8f, (Action)(() =>
        {
            //DontDestroyManager.Instance.AddCoinTop(reward);
            if (PanelCoinGem.Instance != null)
            {
                SocialManager.Instance.InstagramSetAlreadyFollow();
                PanelCoinGem.Instance.AddCoinTop(goldReward, FirebaseLogSpaceWar.SocialReward_why);
                PanelCoinGem.Instance.AddGemTop(INSTAGRAM_GEM, FirebaseLogSpaceWar.SocialReward_why);
                sIconFollowInstagram.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", Environment.NewLine + goldReward + " " + ScriptLocalization.gold + Environment.NewLine + INSTAGRAM_GEM + " " + ScriptLocalization.gem), true);
            }
        }));

        //sFollowInstagram.spriteName = "btn_daily_grey";
    }

    private void OnFollowTwitterReward()
    {
        if (SocialManager.Instance.TwitterIsAlreadyFollow())
            return;

        int goldReward = MinhCacheGame.GetVersion() > 0 ? TWITTER_GOLD : OLD_TWITTER_REWARD;

        (this).Delay(0.8f, (Action)(() =>
        {
            //DontDestroyManager.Instance.AddCoinTop(reward);
            if (PanelCoinGem.Instance != null)
            {
                SocialManager.Instance.TwitterSetAlreadyFollow();
                PanelCoinGem.Instance.AddCoinTop(goldReward, FirebaseLogSpaceWar.SocialReward_why);
                PanelCoinGem.Instance.AddGemTop(TWITTER_GEM, FirebaseLogSpaceWar.SocialReward_why);
                sIconFollowTwitter.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", Environment.NewLine + goldReward + " " + ScriptLocalization.gold + Environment.NewLine + TWITTER_GEM + " " + ScriptLocalization.gem), true);
            }
        }));

        //sFollowTwitter.spriteName = "btn_daily_grey";
    }

    public void OnClickBtnLikeFB()
    {
        SoundManager.PlayClickButton();

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }

        if (!FB.IsLoggedIn)
        {
            SocialManager.Instance.FBLogIn();
            Debug.Log("return like");

            return;
        }

        SocialManager.Instance.FBOnClickBtnLike();
    }

    public void OnClickBtnJoinGroupFB()
    {
        SoundManager.PlayClickButton();

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }

        if (!FB.IsLoggedIn)
        {
            SocialManager.Instance.FBLogIn();
            Debug.Log("return Join Group FB");

            return;
        }

        SocialManager.Instance.FBOnClickBtnJoinGroup();

    }

    public void OnClickBtnFollowInstagram()
    {
        SoundManager.PlayClickButton();

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }

        SocialManager.Instance.InstagramOnClickBtnFollow();
    }

    public void OnClickBtnFollowTwitter()
    {
        SoundManager.PlayClickButton();

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }

        SocialManager.Instance.TwitterOnClickBtnFollow();
    }

    public void OnClickBtnFriends1()
    {
        SoundManager.PlayClickButton();

        if (SocialManager.Instance.FBIsAlreadyClaimFriendsReward(1))
            return;

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }

        if (!FB.IsLoggedIn)
        {
            SocialManager.Instance.FBLogIn();
            Debug.Log("return friends 1");

            return;
        }

        if (SocialManager.Instance.FBGetNumberOfFriends() >= 1)
        {
            int goldReward = MinhCacheGame.GetVersion() > 0 ? ONE_FRIEND_GOLD : OLD_ONE_FRIEND_REWARD;

            //DontDestroyManager.Instance.AddCoinTop(reward);
            if (PanelCoinGem.Instance != null)
            {
                SocialManager.Instance.FBSetAlreadyClaimFriendsReward(1);
                PanelCoinGem.Instance.AddCoinTop(goldReward, FirebaseLogSpaceWar.SocialReward_why);
                PanelCoinGem.Instance.AddGemTop(ONE_FRIEND_GEM, FirebaseLogSpaceWar.SocialReward_why);

                labelFriendsFB1.text = ScriptLocalization.done;
                sFriendsFB1.spriteName = "btn_daily_grey";
                giftFriendsFB1.gameObject.SetActive(false);

                glowFriendsFB1.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", Environment.NewLine + goldReward + " " + ScriptLocalization.gold + Environment.NewLine + ONE_FRIEND_GEM + " " + ScriptLocalization.gem), true);
            }

        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_need_more_friend, false, 1.5f);
        }
    }

    public void OnClickBtnFriends5()
    {
        SoundManager.PlayClickButton();

        if (SocialManager.Instance.FBIsAlreadyClaimFriendsReward(5))
            return;

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }

        if (!FB.IsLoggedIn)
        {
            SocialManager.Instance.FBLogIn();
            Debug.Log("return friends 5");

            return;
        }

        if (SocialManager.Instance.FBGetNumberOfFriends() >= 5)
        {
            int goldReward = MinhCacheGame.GetVersion() > 0 ? FIVE_FRIENDS_GOLD : OLD_FIVE_FRIENDS_REWARD;

            //DontDestroyManager.Instance.AddCoinTop(reward);
            if (PanelCoinGem.Instance != null)
            {
                SocialManager.Instance.FBSetAlreadyClaimFriendsReward(5);
                PanelCoinGem.Instance.AddCoinTop(goldReward, FirebaseLogSpaceWar.SocialReward_why);
                PanelCoinGem.Instance.AddGemTop(FIVE_FRIENDS_GEM, FirebaseLogSpaceWar.SocialReward_why);

                labelFriendsFB5.text = ScriptLocalization.done;
                sFriendsFB5.spriteName = "btn_daily_grey";
                giftFriendsFB5.gameObject.SetActive(false);

                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", Environment.NewLine + goldReward + " " + ScriptLocalization.gold + Environment.NewLine + FIVE_FRIENDS_GEM + " " + ScriptLocalization.gem), true);
            }

            glowFriendsFB5.SetActive(false);
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_need_more_friend, false, 1.5f);
        }
    }

    public void OnClickBtnFriends10()
    {
        SoundManager.PlayClickButton();

        if (SocialManager.Instance.FBIsAlreadyClaimFriendsReward(10))
            return;

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }

        if (!FB.IsLoggedIn)
        {
            SocialManager.Instance.FBLogIn();
            Debug.Log("return friends 10");

            return;
        }

        if (SocialManager.Instance.FBGetNumberOfFriends() >= 10)
        {
            int goldReward = MinhCacheGame.GetVersion() > 0 ? TEN_FRIENDS_GOLD : OLD_TEN_FRIENDS_REWARD;

            //DontDestroyManager.Instance.AddCoinTop(reward);
            if (PanelCoinGem.Instance != null)
            {
                SocialManager.Instance.FBSetAlreadyClaimFriendsReward(10);
                PanelCoinGem.Instance.AddCoinTop(goldReward, FirebaseLogSpaceWar.SocialReward_why);
                PanelCoinGem.Instance.AddGemTop(TEN_FRIENDS_GEM, FirebaseLogSpaceWar.SocialReward_why);

                labelFriendsFB10.text = ScriptLocalization.done;
                sFriendsFB10.spriteName = "btn_daily_grey";
                giftFriendsFB10.gameObject.SetActive(false);

                glowFriendsFB10.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", Environment.NewLine + goldReward + " " + ScriptLocalization.gold + Environment.NewLine + TEN_FRIENDS_GEM + " " + ScriptLocalization.gem), true);

            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_need_more_friend, false, 1.5f);
        }
    }

    public void OnClickBtnFriends20()
    {
        SoundManager.PlayClickButton();

        if (SocialManager.Instance.FBIsAlreadyClaimFriendsReward(20))
            return;

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }

        if (!FB.IsLoggedIn)
        {
            SocialManager.Instance.FBLogIn();
            Debug.Log("return friends 20");

            return;
        }

        if (SocialManager.Instance.FBGetNumberOfFriends() >= 20)
        {
            int goldReward = MinhCacheGame.GetVersion() > 0 ? TWENTY_FRIENDS_GOLD : OLD_TWENTY_FRIENDS_REWARD;

            //DontDestroyManager.Instance.AddCoinTop(reward);
            if (PanelCoinGem.Instance != null)
            {
                SocialManager.Instance.FBSetAlreadyClaimFriendsReward(20);
                PanelCoinGem.Instance.AddCoinTop(goldReward, FirebaseLogSpaceWar.SocialReward_why);
                PanelCoinGem.Instance.AddGemTop(TWENTY_FRIENDS_GEM, FirebaseLogSpaceWar.SocialReward_why);

                labelFriendsFB20.text = ScriptLocalization.done;
                sFriendsFB20.spriteName = "btn_daily_grey";
                giftFriendsFB20.gameObject.SetActive(false);

                glowFriendsFB20.SetActive(false);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", Environment.NewLine + goldReward + " " + ScriptLocalization.gold + Environment.NewLine + TWENTY_FRIENDS_GEM + " " + ScriptLocalization.gem), true);
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_need_more_friend, false, 1.5f);
        }
    }

    public void OnClickBtnShareFB()
    {
        SoundManager.PlayClickButton();

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }

        if (!CanShareFacebook())
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_comeback_tomorrow, false, 1.5f);
            return;
        }

        if (!FB.IsLoggedIn)
        {
            SocialManager.Instance.FBLogIn();
            Debug.Log("return friends share");

            return;
        }

        SocialManager.Instance.FBShare();
    }

    private void ReloadUI()
    {
        //Debug.Log("ReloadUI: " + FB.IsLoggedIn + ", " + SocialManager.Instance.FBGetNumberOfFriends());
        lFriends1.text = ScriptLocalization.community_friends.Replace("%{friend_number}", "1");
        lFriends5.text = ScriptLocalization.community_friends.Replace("%{friend_number}", "5");
        lFriends10.text = ScriptLocalization.community_friends.Replace("%{friend_number}", "10");
        lFriends20.text = ScriptLocalization.community_friends.Replace("%{friend_number}", "20");

        labelLikeFB.text = FB.IsLoggedIn ? ScriptLocalization.go : ScriptLocalization.login;
        labelJoinGroupFB.text = FB.IsLoggedIn ? ScriptLocalization.go : ScriptLocalization.login;

        labelTotalFriendsFB.text = ScriptLocalization.msg_community_total_friends.Replace("%{friend_number}", (FB.IsLoggedIn ? CachePvp.CountFriendsMinh.ToString() : "0"));

        labelFriendsFB1.text = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(1) ? ScriptLocalization.done : (FB.IsLoggedIn ? ScriptLocalization.claim : ScriptLocalization.login);
        labelFriendsFB5.text = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(5) ? ScriptLocalization.done : (FB.IsLoggedIn ? ScriptLocalization.claim : ScriptLocalization.login);
        labelFriendsFB10.text = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(10) ? ScriptLocalization.done : (FB.IsLoggedIn ? ScriptLocalization.claim : ScriptLocalization.login);
        labelFriendsFB20.text = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(20) ? ScriptLocalization.done : (FB.IsLoggedIn ? ScriptLocalization.claim : ScriptLocalization.login);

        labelShareFB.text = !CanShareFacebook() ? ScriptLocalization.wait : (FB.IsLoggedIn ? ScriptLocalization.share : ScriptLocalization.login);

        //Done sprite
        //sLikeFB.spriteName = SocialManager.Instance.FBIsAlreadyLike() ? "btn_daily_grey" : "btn_blue";
        //sJoinGroupFB.spriteName = SocialManager.Instance.FBIsAlreadyJoinGroup() ? "btn_daily_grey" : "btn_blue";
        //sFollowInstagram.spriteName = SocialManager.Instance.InstagramIsAlreadyFollow() ? "btn_daily_grey" : "btn_blue";
        //sFollowTwitter.spriteName = SocialManager.Instance.TwitterIsAlreadyFollow() ? "btn_daily_grey" : "btn_blue";

        sFriendsFB1.spriteName = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(1) ? "btn_daily_grey" : "btn_blue";
        sFriendsFB5.spriteName = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(5) ? "btn_daily_grey" : "btn_blue";
        sFriendsFB10.spriteName = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(10) ? "btn_daily_grey" : "btn_blue";
        sFriendsFB20.spriteName = SocialManager.Instance.FBIsAlreadyClaimFriendsReward(20) ? "btn_daily_grey" : "btn_blue";
        sShareFB.spriteName = !CanShareFacebook() ? "btn_daily_grey" : "btn_blue";

        //if (!CanShareFacebook())
        //{
        //    coCountdownShareFB = CountdownShareFB();
        //    StartCoroutine(coCountdownShareFB);
        //}
        //else
        //{
        //    labelCountdownShareFB.gameObject.SetActive(false);
        //}

        giftLikeFB.gameObject.SetActive(!SocialManager.Instance.FBIsAlreadyLike());
        giftJoinGroupFB.gameObject.SetActive(!SocialManager.Instance.FBIsAlreadyJoinGroup());
        sIconFollowInstagram.SetActive(!SocialManager.Instance.InstagramIsAlreadyFollow());
        sIconFollowTwitter.SetActive(!SocialManager.Instance.TwitterIsAlreadyFollow());

        sIconShareGift.SetActive(true);

        giftFriendsFB1.gameObject.SetActive(!SocialManager.Instance.FBIsAlreadyClaimFriendsReward(1));
        giftFriendsFB5.gameObject.SetActive(!SocialManager.Instance.FBIsAlreadyClaimFriendsReward(5));
        giftFriendsFB10.gameObject.SetActive(!SocialManager.Instance.FBIsAlreadyClaimFriendsReward(10));
        giftFriendsFB20.gameObject.SetActive(!SocialManager.Instance.FBIsAlreadyClaimFriendsReward(20));


        if (!SocialManager.Instance.FBIsAlreadyClaimFriendsReward(1) && FB.IsLoggedIn)
        {
            if (SocialManager.Instance.FBGetNumberOfFriends() >= 1)
            {
                glowFriendsFB1.SetActive(true);
            }
        }
        else
        {
            glowFriendsFB1.SetActive(false);
        }

        if (!SocialManager.Instance.FBIsAlreadyClaimFriendsReward(5) && FB.IsLoggedIn)
        {
            if (SocialManager.Instance.FBGetNumberOfFriends() >= 5)
            {
                glowFriendsFB5.SetActive(true);
            }
        }
        else
        {
            glowFriendsFB5.SetActive(false);
        }

        if (!SocialManager.Instance.FBIsAlreadyClaimFriendsReward(10) && FB.IsLoggedIn)
        {
            if (SocialManager.Instance.FBGetNumberOfFriends() >= 10)
            {
                glowFriendsFB10.SetActive(true);
            }
        }
        else
        {
            glowFriendsFB10.SetActive(false);
        }

        if (!SocialManager.Instance.FBIsAlreadyClaimFriendsReward(20) && FB.IsLoggedIn)
        {
            if (SocialManager.Instance.FBGetNumberOfFriends() >= 20)
            {
                glowFriendsFB20.SetActive(true);
            }
        }
        else
        {
            glowFriendsFB20.SetActive(false);
        }
    }

    private bool CanShareFacebook()
    {
        //TimeSpan interval = CachePvp.dateTime - MinhCacheGame.GetFacebookShareDate();

        //return interval.TotalHours > timeCountDownShareFB;

        //Debug.LogError(DateTime.Now.Subtract(DateTime.Now.AddDays(-0.1)).TotalDays);

        //return CachePvp.dateTime.Subtract(MinhCacheGame.GetFacebookShareDate()).TotalDays >= 1;

        return CachePvp.dateTime.Day != MinhCacheGame.GetFacebookShareDate().Day;
    }

    public void OpenPopup()
    {
        SoundManager.PlayClickButton();
        popupSocialInvi.SetActive(true);
        ReloadUI();
        PopupManager.Instance.HideChatWorldPopup();
        FirebaseLogSpaceWar.LogClickButton("Social");

        if (!FB.IsInitialized)
            return;

        if (SocialManager.Instance == null)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_pvp_connect_to_server, false, 1.8f);
            return;
        }

        //SocialManager.Instance.FBRequestFriendsData
        //    (
        //        () =>
        //        {
        //            ReloadUI();
        //        },
        //        () =>
        //        {
        //            ReloadUI();
        //        }
        //    );

        new CSFriendshipStats(CachePvp.Code).Send();
    }

    public void ClosePopup()

    {
        SoundManager.PlayClickButton();
        popupSocialInvi.SetActive(false);
        PopupManager.Instance.ShowChatWorldPopup();
        //if (coCountdownShareFB != null)
        //    StopCoroutine(coCountdownShareFB);
    }

    //IEnumerator coCountdownShareFB;
    //IEnumerator CountdownShareFB()
    //{
    //    labelCountdownShareFB.gameObject.SetActive(true);
    //    DateTime shareDate = MinhCacheGame.GetFacebookShareDate();

    //    while (shareDate.AddHours(timeCountDownShareFB) > DateTime.Now)
    //    {
    //        TimeSpan interval = shareDate.AddHours(timeCountDownShareFB) - DateTime.Now;
    //        //labelCountdownShareFB.text = string.Format("{0:00} days {1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds);

    //        labelCountdownShareFB.text = string.Format("{0:00}:{1:00}:{2:00}", interval.Hours + interval.Days * 24, interval.Minutes, interval.Seconds);
    //        yield return new WaitForSeconds(1f);
    //    }

    //    labelCountdownShareFB.gameObject.SetActive(false);
    //    labelShareFB.text = txtShare;
    //    sShareFB.spriteName = "btn_blue";
    //}
}
