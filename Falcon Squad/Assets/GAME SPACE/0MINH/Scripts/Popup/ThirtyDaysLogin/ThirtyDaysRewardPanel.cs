﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using TCore;

public class ThirtyDaysRewardPanel : MonoBehaviour
{
    [SerializeField]
    private UISprite sFade;

    [SerializeField]
    private GameObject insideObjects;

    [SerializeField]
    private UISprite sIcon;

    [SerializeField]
    private TweenAlpha tweenAlpha;

    [SerializeField]
    private UILabel lAmount;

    [SerializeField]
    private GameObject valuableFX;

    //Xu ly nut back
    public static bool isShowed;


    private void ResetUI()
    {
        gameObject.SetActive(false);
        insideObjects.SetActive(false);

        sFade.alpha = 0;
    }

    public void ShowPopupOneReward(string rewardName, int amount, bool isValueable)
    {
        //ResetUI();
        tweenAlpha.enabled = true;
        tweenAlpha.ResetToBeginning();
        gameObject.SetActive(true);

        sIcon.spriteName = HangarValue.ItemSpriteName[rewardName];
        lAmount.text = amount + "";
        valuableFX.SetActive(isValueable);
    }

    public void OnClickBtnConfirm()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);

        PanelCoinGem.Instance.AddCoinTop();
        PanelCoinGem.Instance.AddGemTop();
    }

    private void OnEnable()
    {
        isShowed = true;
    }

    private void OnDisable()
    {
        isShowed = false; 
    }
}
