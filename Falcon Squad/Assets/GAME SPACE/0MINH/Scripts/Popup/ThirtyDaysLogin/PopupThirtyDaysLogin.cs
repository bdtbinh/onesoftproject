﻿using UnityEngine;
using System.Collections.Generic;
using TCore;

public class PopupThirtyDaysLogin : MonoBehaviour
{

    private const string BTN_CLAIM_ENABLE_SPRITE = "PVP_btn_fight";
    private const string BTN_CLAIM_DISABLE_SPRITE = "PVP_btn_fight_d";

    [SerializeField]
    private List<ThirtyDaysItem> listItems;

    [SerializeField]
    private UISprite sBtnClaim;

    [SerializeField]
    private ThirtyDaysRewardPanel rewardPanel;

    private static bool isShowed;              //Cache lại xem lần mở app này đã tự động show popup chưa
    private int rewardedDay;            //Ngày gần nhất đã nhận thưởng

    public void CheckToAutoShowPopup()
    {
        if (isShowed) return;
        if (IsTheSameDay()) return;

        gameObject.SetActive(true);
        isShowed = true;
    }

    private void OnEnable()
    {
        if (IsTheSameDay())
        {
            sBtnClaim.spriteName = BTN_CLAIM_DISABLE_SPRITE;
        }
        else
        {
            sBtnClaim.spriteName = BTN_CLAIM_ENABLE_SPRITE;

            if (MinhCacheGame.GetRewardIndexOf30Days() > 30)
            {
                MinhCacheGame.SetRewardIndexOf30Days(1);
            }
        }

        for (int i = 0; i < listItems.Count; i++)
        {
            int day = i + 1;
            listItems[i].InitData(day);

            if (day < MinhCacheGame.GetRewardIndexOf30Days())
            {
                listItems[i].SetAsClaimed();
            }
            else
            {
                listItems[i].SetAsNormal();

                if (day == MinhCacheGame.GetRewardIndexOf30Days())
                {
                    if (!IsTheSameDay())
                    {
                        listItems[i].SetAsCurrentReward();
                    }
                }

                if (DailyLogin30Sheet.Get(day).valuable == 1)
                    listItems[i].ShowValuableFX();
            }
        }
    }

    public void ShowPopup()
    {
        gameObject.SetActive(true);
    }

    //Check xem ngày đã nhận quà gần nhất có phải là ngày hiện tại không
    private bool IsTheSameDay()
    {
        //Debug.LogError("IsTheSameDay: " + MinhCacheGame.GetLastDayOf30Days() + " * " + CachePvp.dateTime.Day);
        return MinhCacheGame.GetLastDayOf30Days() == CachePvp.dateTime.Day;
    }

    public void OnClickBtnClaim()
    {
        SoundManager.PlayClickButton();

        if (sBtnClaim.spriteName == BTN_CLAIM_DISABLE_SPRITE)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.popup_30_days_already_earned, false, 1.5f);
            return;
        }

        CollectReward();
    }

    private void CollectReward()
    {
        int index = MinhCacheGame.GetRewardIndexOf30Days();

        MinhCacheGame.SetRewardIndexOf30Days(index + 1);
        MinhCacheGame.SetLastDayOf30Days();

        GiveRewards(DailyLogin30Sheet.Get(index).name, DailyLogin30Sheet.Get(index).amount);
        rewardPanel.ShowPopupOneReward(DailyLogin30Sheet.Get(index).name, DailyLogin30Sheet.Get(index).amount, DailyLogin30Sheet.Get(index).valuable == 1);

        listItems[index - 1].SetAsClaimed();

        index++;

        sBtnClaim.spriteName = BTN_CLAIM_DISABLE_SPRITE;

    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
    }

    private void GiveRewards(string rewardName, int amount)
    {
        switch (rewardName)
        {
            case "PowerUp":
                CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + amount);
                break;

            case "ActiveSkill":
                CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + amount);
                break;

            case "Life":
                CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + amount);
                break;

            case "Gold":
                CacheGame.SetTotalCoin(CacheGame.GetTotalCoin() + amount, FirebaseLogSpaceWar.DailyLogin_why, "");
                //FirebaseLogSpaceWar.LogGoldIn(amount, FirebaseLogSpaceWar.DailyLogin_why, "");
                break;

            case "Gem":
                CacheGame.SetTotalGem(CacheGame.GetTotalGem() + amount, FirebaseLogSpaceWar.DailyLogin_why, FirebaseLogSpaceWar.Unknown_why);
                FirebaseLogSpaceWar.LogGemIn(amount, FirebaseLogSpaceWar.DailyLogin_why, "");
                break;

            case "AircraftGeneralCard":
                MinhCacheGame.SetSpaceShipGeneralCards(MinhCacheGame.GetSpaceShipGeneralCards() + amount, FirebaseLogSpaceWar.DailyLogin_why, "");
                break;

            case "Aircraft1Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.BataFD, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.BataFD) + amount, FirebaseLogSpaceWar.DailyLogin_why, "", AircraftTypeEnum.BataFD.ToString());
                break;

            case "Aircraft2Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.SkyWraith, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.SkyWraith) + amount, FirebaseLogSpaceWar.DailyLogin_why, "", AircraftTypeEnum.SkyWraith.ToString());
                break;

            case "Aircraft3Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.FuryOfAres, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.FuryOfAres) + amount, FirebaseLogSpaceWar.DailyLogin_why, "", AircraftTypeEnum.FuryOfAres.ToString());
                break;

            case "Aircraft6Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.MacBird, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.MacBird) + amount, FirebaseLogSpaceWar.DailyLogin_why, "", AircraftTypeEnum.MacBird.ToString());
                break;

            case "Aircraft7Card":
                MinhCacheGame.SetSpaceShipCards(AircraftTypeEnum.TwilightX, MinhCacheGame.GetSpaceShipCards(AircraftTypeEnum.TwilightX) + amount, FirebaseLogSpaceWar.DailyLogin_why, "", AircraftTypeEnum.TwilightX.ToString());
                break;

            default:
                break;
        }
    }
}
