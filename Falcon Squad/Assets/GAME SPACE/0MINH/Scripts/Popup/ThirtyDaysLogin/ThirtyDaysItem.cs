﻿using UnityEngine;

public class ThirtyDaysItem : MonoBehaviour
{
    //Constant
    private const string ICON_NAME = "sIcon";
    private const string MASK_NAME = "sMask";
    private const string CHECK_NAME = "sCheck";
    private const string AMOUNT_NAME = "lAmount";
    private const string TITLE_NAME = "lTitle";
    private const string FX_NAME = "Fx_Outline_30days";
    private const string CURRENT_SPRITE_NAME = "sCurrentSprite";

    private UISprite sIcon;
    private GameObject sMask;
    private GameObject sCheck;
    private UILabel lAmount;
    private UILabel lTitle;
    private GameObject fx;
    private GameObject sCurrentSprite;
    private ElementItemInfo elementItemInfo;

    private void InitComponents()
    {
        if(sIcon == null)
            sIcon = MinhUtils.FindChild(transform, ICON_NAME).GetComponent<UISprite>();

        if(sMask == null)
            sMask = MinhUtils.FindChild(transform, MASK_NAME).gameObject;

        if(sCheck == null)
            sCheck = MinhUtils.FindChild(transform, CHECK_NAME).gameObject;

        if(lAmount == null) 
            lAmount = MinhUtils.FindChild(transform, AMOUNT_NAME).GetComponent<UILabel>();

        if(lTitle == null)
            lTitle = MinhUtils.FindChild(transform, TITLE_NAME).GetComponent<UILabel>();

        if(fx == null)
            fx = MinhUtils.FindChild(transform, FX_NAME).gameObject;

        if(sCurrentSprite == null)
            sCurrentSprite = MinhUtils.FindChild(transform, CURRENT_SPRITE_NAME).gameObject;

        if (elementItemInfo == null)
            elementItemInfo = GetComponent<ElementItemInfo>();
    }

    public void InitData(int day)
    {
        InitComponents();

        string rewardType = DailyLogin30Sheet.Get(day).name;
        int amount = DailyLogin30Sheet.Get(day).amount;

        sIcon.spriteName = HangarValue.ItemSpriteName[rewardType];
        lAmount.text = amount + "";
        lTitle.text = I2.Loc.ScriptLocalization.day_dailyLogin.Replace("%{number_day}", day.ToString());

        elementItemInfo.CreateItem(rewardType, amount, false);
    }

    public void SetAsClaimed()
    {
        sMask.SetActive(true);
        sCheck.SetActive(true);
        fx.SetActive(false);
        sCurrentSprite.SetActive(false);
    }

    public void SetAsNormal()
    {
        sMask.SetActive(false);
        sCheck.SetActive(false);
    }

    public void SetAsCurrentReward()
    {
        sCurrentSprite.SetActive(true);
    }
    public void ShowValuableFX()
    {
        fx.SetActive(true);
    }
}
