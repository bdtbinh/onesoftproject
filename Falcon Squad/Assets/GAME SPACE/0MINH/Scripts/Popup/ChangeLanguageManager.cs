﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;
using Sirenix.OdinInspector;
using com.ootii.Messages;
using TCore;

public class ChangeLanguageManager : MonoBehaviour
{
    [SerializeField]
    private ElementLanguages elementPrefab;

    [SerializeField]
    private UILabel lCurrentLanguage;

    [SerializeField]
    private Transform listElementsTrans;

    [SerializeField]
    private GameObject panelScrollview;

    [SerializeField]
    private UIScrollView scrollview;

    [SerializeField]
    private UISprite sFlag;

    private Color bgColor1 = new Color(64 / 255f, 60 / 255f, 79 / 255f, 255f);
    private Color bgColor2 = new Color(53 / 255f, 51 / 255f, 72 / 255f, 255f);

    private List<ElementLanguages> listElementLangs;

    //[ValueDropdown("LanguageDropBox")]
    //[OnValueChanged("OnLanguageChanged")]
    //public string language;

    //private string[] languages;

    //public string[] LanguageDropBox()
    //{
    //    if (languages == null)
    //    {
    //        Debug.LogError("LanguageDropBox: " + language);
    //        LocalizationManager.UpdateSources();
    //        languages = LocalizationManager.GetAllLanguages().ToArray();
    //        System.Array.Sort(languages);
    //    }

    //    return languages;
    //}

    private void Start()
    {
        MessageDispatcher.AddListener(EventID.ON_CLICK_CHANGE_LANGUAGE, OnClickChangeLanguage, true);

        InitElements();

    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventID.ON_CLICK_CHANGE_LANGUAGE, OnClickChangeLanguage, true);
    }

    private void OnClickChangeLanguage(IMessage msg)
    {
        panelScrollview.SetActive(false);
        LocalizationManager.CurrentLanguage = (string)msg.Data;

        //LocalizationManager.LocalizeAll(true);
        InitElements();
    }

    private void InitElements()
    {
        lCurrentLanguage.text = GetLanguageConverted(LocalizationManager.CurrentLanguage);
        sFlag.spriteName = SpriteNameConst.GetLanguageFlagCode(LocalizationManager.CurrentLanguage);

        LocalizationManager.UpdateSources();
        List<string> listLangs = LocalizationManager.GetAllLanguages();

        for (int i = 0; i < listLangs.Count; i++)
        {
            if (listLangs[i] == LocalizationManager.CurrentLanguage)
            {
                listLangs.RemoveAt(i);
                break;
            }
        }

        listLangs.Sort();

        if(listElementLangs == null)
        {
            listElementLangs = new List<ElementLanguages>(listLangs.Count);

            for (int i = 0; i < listLangs.Count; i++)
            {
                ElementLanguages e = Instantiate(elementPrefab, listElementsTrans);
                e.transform.localPosition = new Vector3(0, -8 - i * 50, 0);
                listElementLangs.Add(e);
            }
        }

        for (int i = 0; i < listElementLangs.Count; i++)
        {
            listElementLangs[i].CreateItem(i % 2 == 0 ? bgColor1 : bgColor2, listLangs[i]);
        }
    }

    public void OnClickBtnChangeLanguage()
    {
        SoundManager.PlayClickButton();
        panelScrollview.SetActive(!panelScrollview.activeInHierarchy);
        scrollview.ResetPosition();
    }

    public void OnClickOutSidePanel()
    {
        panelScrollview.SetActive(false);
    }

    //[Button]
    //public void ShowLanguage()
    //{

    //    Debug.LogError(LocalizationManager.GetAllLanguages().ToArray());

    //    for (int i = 0; i < LocalizationManager.GetAllLanguages().ToArray().Length; i++)
    //    {
    //        Debug.LogError(LocalizationManager.GetAllLanguages().ToArray()[i]);
    //    }
    //}


    //// Use this for initialization
    //void Start()
    //{
    //    Debug.LogError(LocalizationManager.CurrentLanguage + " * " + LocalizationManager.CurrentLanguageCode + " * " + LocalizationManager.CurrentRegion + " * " + LocalizationManager.CurrentRegionCode);

    //    List<string> langs = LocalizationManager.GetCategories();
    //    for (int i = 0; i < langs.Count; i++)
    //    {
    //        Debug.LogError(langs[i]);
    //    }

    //    Debug.LogError("=============================================================");

    //    //List<string> langs1 = LocalizationManager;
    //    //for (int i = 0; i < langs1.Count; i++)
    //    //{
    //    //    Debug.LogError(langs1[i]);
    //    //}
    //}

    //// Update is called once per frame
    //void Update()
    //{

    //}

    public static string GetLanguageConverted(string language)
    {
        switch (language)
        {
            case "Chinese (Simplified)":
                return "中文";

            case "French":
                return "Français";

            case "German":
                return "Deutsch";

            case "Indonesian":
                return "Bahasa Indonesia";

            case "Italian":
                return "Italiano";

            case "Japanese":
                return "日本語";

            case "Korean":
                return "한국어";

            case "Portuguese":
                return "Português";

            case "Russian":
                return "Русский язык";

            case "Spanish":
                return "Español";

            case "Thai":
                return "ภาษาไทย";

            case "Turkish":
                return "Türkçe";

            case "Vietnamese":
                return "Tiếng Việt";

            case "English":
                return "English";

            default:
                return language;
        }
    }
}
