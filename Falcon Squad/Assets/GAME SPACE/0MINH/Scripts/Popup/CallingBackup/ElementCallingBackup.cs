﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class ElementCallingBackup : MonoBehaviour
{

    [SerializeField]
    private UILabel lRemainTimes;

    [SerializeField]
    private UISprite sIcon;

    [SerializeField]
    private UISprite sRank;

    [SerializeField]
    private UISprite sBG;

    [SerializeField]
    private GameObject sLock;

    [SerializeField]
    private UILabel lLevel;

    [SerializeField]
    private GameObject sSelected;

    [SerializeField]
    private GameObject sBGMask;

    public AircraftTypeEnum AircraftType { get; private set; }

    private int remainTimes;

    public void CreateItem(AircraftTypeEnum aircraftType)
    {
        this.AircraftType = aircraftType;
        Rank rank = CacheGame.GetSpaceShipRank(aircraftType);

        sBG.spriteName = HangarValue.HangarNormalBGName;
        sLock.SetActive(false);
        sSelected.SetActive(false);
        lRemainTimes.gameObject.SetActive(true);
        sBGMask.SetActive(false);

        remainTimes = GameContext.CALLING_BACKUP_TIMES - MinhCacheGame.GetAircraftUsedTimePerDay(aircraftType);          //Số lần sử dụng còn lại.

        if (remainTimes > 0)
        {
            lRemainTimes.text = "[FFFFFFFF]" + remainTimes + "[-]" + "/" + GameContext.CALLING_BACKUP_TIMES;
            sBGMask.SetActive(false);
        }
        else
        {
            lRemainTimes.text = "[FF0000FF]" + remainTimes + "[-]" + "/" + GameContext.CALLING_BACKUP_TIMES;
            sBG.spriteName = HangarValue.HangarDisableBGName;
            sBGMask.SetActive(true);
        }

        sIcon.spriteName = HangarValue.AircraftIconSpriteName(aircraftType, rank);
        Vector2 localScale = sIcon.transform.localScale;
        sIcon.MakePixelPerfect();
        sIcon.transform.localScale = localScale;

        sRank.spriteName = HangarValue.RankSpriteName[rank];
        sRank.MakePixelPerfect();

        if (CacheGame.GetSpaceShipIsUnlocked((int)aircraftType) != 1)
        {
            sBG.spriteName = HangarValue.HangarDisableBGName;
            sLock.SetActive(true);
            lRemainTimes.gameObject.SetActive(false);
        }

        lLevel.text = CacheGame.GetSpaceShipLevel(aircraftType).ToString();
    }

    public void SetAsSelected()
    {
        sSelected.SetActive(true);
        MessageDispatcher.SendMessage(this, EventID.ON_SELECTED_CALLING_BACKUP, AircraftType, 0);
    }

    public void SetAsDeselected()
    {
        sSelected.SetActive(false);
    }

    public void OnClickAircraft()
    {
        if (sSelected.activeInHierarchy)
            return;

        if (CacheGame.GetSpaceShipIsUnlocked((int)AircraftType) == 0)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_unlock_aircraft_first, false, 1.5f);
        }
        else if (remainTimes <= 0)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_use_another_aircraft, false, 1.5f);
        }
        else
        {
            SetAsSelected();
        }
    }
}
