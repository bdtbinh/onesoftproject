﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class PopupCallingBackup : MonoBehaviour
{
    [SerializeField]
    private ElementCallingBackup[] listElements;

    [SerializeField]
    private UISprite btnConfirm;

    [SerializeField]
    private UILabel lTimePerDay;

    [SerializeField]
    private UILabel lReduceLife;

    private List<int> listSortedAircraftType = new List<int>();

    private ElementCallingBackup selectedCallingBackup;

    private void OnEnable()
    {
        MessageDispatcher.AddListener(EventID.ON_SELECTED_CALLING_BACKUP, OnSelectedCallingBackup, true);

        lTimePerDay.text = I2.Loc.ScriptLocalization.msg_aircraft_used_time_per_day.Replace("%{use_time}", GameContext.CALLING_BACKUP_TIMES + "");
        lReduceLife.text = I2.Loc.ScriptLocalization.calling_backup_reduce_life.Replace("%{life_number}", "2");

        InitListElements();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventID.ON_SELECTED_CALLING_BACKUP, OnSelectedCallingBackup, true);
    }

    private void OnSelectedCallingBackup(IMessage msg)
    {
        btnConfirm.spriteName = "PVP_btn_fight";

        if (selectedCallingBackup != null)
        {
            selectedCallingBackup.SetAsDeselected();
        }

        selectedCallingBackup = (ElementCallingBackup)msg.Sender;
    }

    private void InitListElements()
    {
        int totalAircraft = System.Enum.GetValues(typeof(AircraftTypeEnum)).Length;
        listSortedAircraftType.Clear();
        btnConfirm.spriteName = "PVP_btn_fight_d";
        selectedCallingBackup = null;

        ////Reset số lần sử dụng khi sang ngày mới
        //if (!IsTheSameDay())
        //{
        //    for (int i = 1; i <= totalAircraft; i++)
        //    {
        //        if (i != 4 && i != 5)
        //        {
        //            MinhCacheGame.SetAircraftUsedTimePerDay((AircraftTypeEnum)i, 2);
        //        }
        //    }

        //    MinhCacheGame.SetLastDayUsedTimePerDay();
        //}

        for (int i = 1; i <= totalAircraft; i++)
        {
            if (i != 4 && i != 5 && i != CacheGame.GetSpaceShipUserSelected())
            {
                if (listSortedAircraftType.Count == 0)
                {
                    listSortedAircraftType.Add(i);
                    //Debug.LogError("A1: " + (AircraftTypeEnum)i);
                    continue;
                }
                else
                {
                    //Debug.LogError("A2: " + (AircraftTypeEnum)i);
                    int iLevel = CacheGame.GetSpaceShipLevel((AircraftTypeEnum)i);

                    for (int j = 0; j < listSortedAircraftType.Count; j++)
                    {
                        int jLevel = CacheGame.GetSpaceShipLevel((AircraftTypeEnum)listSortedAircraftType[j]);
                        //Debug.LogError("A3: " + (AircraftTypeEnum)listSortedAircraftType[j] + " *** " + iLevel + " * " + jLevel);


                        if (iLevel > jLevel)
                        {
                            listSortedAircraftType.Insert(j, i);
                            break;
                        }
                    }

                    if (!listSortedAircraftType.Contains(i))
                    {
                        listSortedAircraftType.Add(i);
                        //Debug.LogError("A4: " + (AircraftTypeEnum)i);
                    }
                }
            }
        }

        //Select calling backup
        for (int i = 0; i < listElements.Length; i++)
        {
            listElements[i].CreateItem((AircraftTypeEnum)listSortedAircraftType[i]);
        }

        int indexI = 0;
        for (; indexI < listSortedAircraftType.Count; indexI++)
        {
            if (CacheGame.GetSpaceShipIsUnlocked(listSortedAircraftType[indexI]) == 1 && 
                (GameContext.CALLING_BACKUP_TIMES - MinhCacheGame.GetAircraftUsedTimePerDay((AircraftTypeEnum)listSortedAircraftType[indexI])) > 0)
            {
                Debug.LogError("vao: " + MinhCacheGame.GetLastCallingBackUpAircraftUsed() + " * " + listSortedAircraftType[indexI]);
                if (MinhCacheGame.GetLastCallingBackUpAircraftUsed() == listSortedAircraftType[indexI])
                {
                    listElements[indexI].SetAsSelected();
                    break;
                }
            }
        }

        if (indexI >= listSortedAircraftType.Count)
        {
            for (int i = 0; i < listSortedAircraftType.Count; i++)
            {
                if (CacheGame.GetSpaceShipIsUnlocked(listSortedAircraftType[i]) == 1 && 
                    (GameContext.CALLING_BACKUP_TIMES - MinhCacheGame.GetAircraftUsedTimePerDay((AircraftTypeEnum)listSortedAircraftType[i])) > 0)
                {
                    listElements[i].SetAsSelected();
                    break;
                }
            }
        }
    }

    //private bool IsTheSameDay()
    //{
    //    return MinhCacheGame.GetLastDayUsedTimePerDay() == CachePvp.dateTime.Day;
    //}


    public void OnClickBtnConfirm()
    {
        if (selectedCallingBackup != null)
        {
            MinhCacheGame.SetAircraftUsedTimePerDay(selectedCallingBackup.AircraftType, MinhCacheGame.GetAircraftUsedTimePerDay(selectedCallingBackup.AircraftType) + 1);
            gameObject.SetActive(false);
            AllPlayerManager.Instance.playerCampaign.InitAircraftBackup(selectedCallingBackup.AircraftType);
            Time.timeScale = 1f;
            MainScene.Instance.clockPlayer.localTimeScale = 1f;
            MainScene.Instance.gameFinished = false;

            MinhCacheGame.SetLastCallingBackUpAircraftUsed((int)selectedCallingBackup.AircraftType);
        }
        else
        {
            //show text
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_use_another_aircraft, false, 1.5f);
        }
    }

    public void OnClickOutsidePopUp()
    {
        MainScene.Instance.popupLose.SetActive(true);
        gameObject.SetActive(false);
    }
}
