﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;
using EasyMobile;
using TCore;
using Mp.Pvp;
using com.ootii.Messages;
using System;
using DG.Tweening;

public class PopupRoyalty : MonoBehaviour
{

    [SerializeField]
    private UILabel lGemInstantly;

    [SerializeField]
    private UILabel lGemDaily;

    [SerializeField]
    private UILabel lGemTotal;

    [SerializeField]
    private UILabel lReviveDiscount;

    [SerializeField]
    private UILabel lAircraftSkill;

    [SerializeField]
    private UILabel lFreeGoldChest;

    [SerializeField]
    private UILabel lOriginalPrice;

    [SerializeField]
    private UILabel lDiscountPrice;

    [SerializeField]
    private GameObject btnBuy;

    [SerializeField]
    private UILabel lRemainTime;

    private void Start()
    {
            lOriginalPrice.text = CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_gem_20usd);
            lDiscountPrice.text = CuongUtils.GetIapLocalizedPrice(EM_IAPConstants.Product_inapp_royalpack);
    }

    private void OnEnable()
    {
        lRemainTime.gameObject.SetActive(false);
        btnBuy.SetActive(false);

        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;
        MessageDispatcher.AddListener(EventName.RoyaltyPack.BuyRoyaltyPack.ToString(), OnBuyRoyaltyPack, true);
        MessageDispatcher.AddListener(EventName.RoyaltyPack.GetRoyaltyPackInfo.ToString(), OnGetRoyaltyPackInfo, true);

        lGemInstantly.text = ScriptLocalization.royal_pack_gem_instantly.Replace("%{number}", "400");
        lGemDaily.text = ScriptLocalization.royal_pack_gem_daily.Replace("%{number}", "70");
        lGemTotal.text = ScriptLocalization.royal_pack_gem_total.Replace("%{number}", "2500");

        lReviveDiscount.text = ScriptLocalization.royal_pack_revive_discount.Replace("%{percent}", "30");
        lAircraftSkill.text = ScriptLocalization.royal_pack_extra_aircraft_skill.Replace("%{number}", "1");
        lFreeGoldChest.text = ScriptLocalization.royal_pack_free_golden_chest.Replace("%{number}", "1");

        if (!OSNet.NetManager.Instance.IsOnline())
        {
            lRemainTime.gameObject.SetActive(true);
            lRemainTime.text = ScriptLocalization.notifi_pvp_disconnect_server;
        }
        else
        {
            new CSGetRoyaltyPackInfo(CSGetRoyaltyPackInfo.POP_ROYALTY_PACK).Send();
        }

    }

    private void OnDisable()
    {
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;
        MessageDispatcher.RemoveListener(EventName.RoyaltyPack.BuyRoyaltyPack.ToString(), OnBuyRoyaltyPack, true);
        MessageDispatcher.RemoveListener(EventName.RoyaltyPack.GetRoyaltyPackInfo.ToString(), OnGetRoyaltyPackInfo, true);

        StopAllCoroutines();
    }

    public void OnClickBtnClose()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.HideRoyaltyPopup();
    }

    public void OnClickBtnBuy()
    {
        SoundManager.PlayClickButton();

        if (!OSNet.NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }

        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_royalpack, IAPCallbackManager.WherePurchase.shop, "Royalty Pack");

        //new CSBuyRoyaltyPack().Send();
        //btnBuy.SetActive(false);
        //PanelCoinGem.Instance.AddGemTop(400, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_royalpack);

    }

    void OnBuyRoyaltyPack(IMessage msg)
    {
        SCBuyRoyaltyPack BRP = msg.Data as SCBuyRoyaltyPack;
        Debug.LogError("OnBuyRoyaltyPack");

        if (BRP.status == OSNet.SCMessage.SUCCESS)
        {
            RoyaltyPackData data = BRP.data;
            if (data != null)
            {
                Debug.LogError("OnBuyRoyaltyPack: " + data.timeRemain);
                if (data.timeRemain > 0)
                {
                    lRemainTime.gameObject.SetActive(true);
                    StartCoroutine(CountDown(data.timeRemain));
                }
            }
            else
            {
                Debug.LogError("OnBuyRoyaltyPack Data Null");
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(BRP.message, false, 1.5f);
        }
    }

    private void OnGetRoyaltyPackInfo(IMessage rMessage)
    {
        SCGetRoyaltyPackInfo GRPI = rMessage.Data as SCGetRoyaltyPackInfo;

        Debug.LogError("PopupRoyalty: " + GRPI.type);
        if (GRPI.type != CSGetRoyaltyPackInfo.POP_ROYALTY_PACK)
            return;

        if (GRPI.status == OSNet.SCMessage.SUCCESS)
        {
            RoyaltyPackData data = GRPI.data;
            if (data != null)
            {
                Debug.LogError("PopupRoyalty: " + data.status);
                if (data.status == RoyaltyPackData.ACTIVE)
                {
                    MinhCacheGame.SetAlreadyBuyRoyalPack(true);

                    Debug.LogError("PopupRoyalty: OnGetRoyaltyPackInfo: " + data.timeRemain);
                    if (data.timeRemain > 0)
                    {
                        btnBuy.SetActive(false);
                        lRemainTime.gameObject.SetActive(true);
                        StartCoroutine(CountDown(data.timeRemain));
                    }
                    else
                    {
                        btnBuy.SetActive(true);
                        lRemainTime.gameObject.SetActive(false);
                    }
                }
                else
                {
                    MinhCacheGame.SetAlreadyBuyRoyalPack(false);

                    btnBuy.SetActive(true);
                    lRemainTime.gameObject.SetActive(false);
                }
            }
            else
            {
                MinhCacheGame.SetAlreadyBuyRoyalPack(false);

                btnBuy.SetActive(true);
                lRemainTime.gameObject.SetActive(false);
                Debug.LogError("PopupRoyalty: OnGetRoyaltyPackInfo Data Null");
            }
        }
        else
        {
            Debug.LogError(GRPI.status);
            PopupManagerCuong.Instance.ShowTextNotifiToast(GRPI.message, false, 1.5f);
        }
    }

    void OnPurchaseSuccessed(string productName)
    {
        if (productName == EM_IAPConstants.Product_inapp_royalpack)
        {
            new CSBuyRoyaltyPack().Send(true);
            MinhCacheGame.SetAlreadyBuyRoyalPack(true);
            btnBuy.SetActive(false);
            PanelCoinGem.Instance.AddGemTop(400, FirebaseLogSpaceWar.IAP_why, EM_IAPConstants.Product_inapp_royalpack);

            ShowNotifiComplete(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}",
            Environment.NewLine + "400 " + ScriptLocalization.gem));
        }
    }

    IEnumerator CountDown(double remainTime)
    {
        DateTime endTime = DateTime.Now.AddSeconds(remainTime);

        while (DateTime.Now < endTime)
        {
            TimeSpan interval = endTime - DateTime.Now;

            string time = "";

            if (interval.Days > 0)
                time = "[FF0000]" + string.Format("{0:0}:{1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
            else
                time = "[FF0000]" + string.Format("{0:0}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds) + "[-]";

            lRemainTime.text = ScriptLocalization.msg_premium_pack_time_again.Replace("%{time}", time);
            yield return new WaitForSeconds(1f);
        }

        btnBuy.SetActive(true);
        lRemainTime.gameObject.SetActive(false);
    }

    void ShowNotifiComplete(string text)
    {
        float ftest = 0;
        DOTween.To(() => ftest, x => ftest = x, 1f, 0.56f).SetUpdate(true).OnComplete((TweenCallback)(() =>
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast((string)text, (bool)true, (float)1.8f);
        }));
    }
}
