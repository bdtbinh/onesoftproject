﻿using com.ootii.Messages;
using Mp.Pvp;
using UnityEngine;

public class RoyaltyLuckyBoxFree : MonoBehaviour {

    [SerializeField]
    private GameObject lPrice;

    [SerializeField]
    private GameObject lFree;

    private PopupLuckyBox popupLuckyBox;

    private void Awake()
    {
        popupLuckyBox = transform.parent.GetComponent<PopupLuckyBox>();
    }

    private void OnEnable()
    {
        lPrice.SetActive(true);
        lFree.SetActive(false);
        popupLuckyBox.hasFreeGoldChest = false;

        MessageDispatcher.AddListener(EventName.RoyaltyPack.GetRoyaltyPackInfo.ToString(), OnGetRoyaltyPackInfo, true);
        MessageDispatcher.AddListener(EventID.ON_GET_FREE_LUCKY_BOX_GOLD, OnGetFreeLuckyBoxGold, true);
        new CSGetRoyaltyPackInfo(CSGetRoyaltyPackInfo.POP_LUCKY_BOX).Send();
    }

    private void OnDisable()
    {
        MessageDispatcher.RemoveListener(EventName.RoyaltyPack.GetRoyaltyPackInfo.ToString(), OnGetRoyaltyPackInfo, true);
        MessageDispatcher.RemoveListener(EventID.ON_GET_FREE_LUCKY_BOX_GOLD, OnGetFreeLuckyBoxGold, true);

        MessageDispatcher.SendMessage(this, EventID.ON_DISABLE_SHOW_RECEIVE_ITEM_FX, this, 0);
    }

    private void OnGetRoyaltyPackInfo(IMessage rMessage)
    {
        SCGetRoyaltyPackInfo GRPI = rMessage.Data as SCGetRoyaltyPackInfo;

        //Debug.LogError("PopupLuckyBox: " + GRPI.type);

        if (GRPI.type != CSGetRoyaltyPackInfo.POP_LUCKY_BOX)
            return;

        if (GRPI.status == OSNet.SCMessage.SUCCESS)
        {
            RoyaltyPackData data = GRPI.data;
            if (data != null)
            {
                //Debug.LogError("PopupLuckyBox: " + data.status);

                if (data.status == RoyaltyPackData.ACTIVE)
                {
                    MinhCacheGame.SetAlreadyBuyRoyalPack(true);

                    if (!data.goldenChest)
                    {
                        lPrice.SetActive(false);
                        lFree.SetActive(true);
                        popupLuckyBox.hasFreeGoldChest = true;
                    }
                    else
                    {
                        lPrice.SetActive(true);
                        lFree.SetActive(false);
                        popupLuckyBox.hasFreeGoldChest = false;
                    }
                }
                else
                {
                    MinhCacheGame.SetAlreadyBuyRoyalPack(false);
                }
            }
            else
            {
                MinhCacheGame.SetAlreadyBuyRoyalPack(false);
                //Debug.LogError("OnGetRoyaltyPackInfo Data Null");
            }
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(GRPI.message, false, 1.5f);
        }
    }

    private void OnGetFreeLuckyBoxGold(IMessage msg)
    {
        lPrice.SetActive(true);
        lFree.SetActive(false);
        popupLuckyBox.hasFreeGoldChest = false;
    }

}
