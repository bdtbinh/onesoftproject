﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using EasyMobile;
using I2.MiniGames;
using Sirenix.OdinInspector;
using TCore;
using Firebase.Analytics;
using I2.Loc;

public class PopupLuckyWheel : MonoBehaviour
{
    //Countdown time cho video reward và free spin (tính bằng giờ)
    [SerializeField]
    private int videoCountdownTime = 8;
    [SerializeField]
    private int freeCountdownTime = 24;

    //Countdown cho video ads:
    //[SerializeField]
    //private GameObject countdownVideoPanel;
    //[SerializeField]
    //private UILabel countdownVideoLabel;

    //Countdown cho free spin:
    //[SerializeField]
    //private GameObject countdownFreePanel;
    //[SerializeField]
    //private UILabel countdownFreeLabel;

    private bool isEnableVideoSpin;
    private bool isEnableFreeSpin;

    [SerializeField]
    private UILabel priceIap;

    //Buttons
    //[SerializeField]
    //private UIButton btnVideoSpin;
    //[SerializeField]
    //private UIButton btnFreeSpin;

    //Button Sprites
    [SerializeField]
    private UISprite btnVideoSpinSprite;
    [SerializeField]
    private UISprite btnFreeSpinSprite;

    //Labels
    [SerializeField]
    private UILabel labelCurrentTickets;

    [SerializeField]
    private PrizeWheel prizeWheel;

    [SerializeField]
    private MiniGame_Controller minigameController;

    [SerializeField]
    private TweenScale btnSpinTweenScale;

    [SerializeField]
    private GameObject highLightReward;

    [SerializeField]
    private UILabel labelBtnVideo;

    [SerializeField]
    private Animator tutAnimator;

    //private DateTime videoEndTime;
    //private DateTime freeEndTime;

    private bool isSpinning = false;

    private LuckyWheelConfig[] rewardInfos;

    public GameObject fxVipItem;


    //private void Awake()
    //{
    //    // make a new TimeSpan for 3 days, 5 hours, 21 minutes, 6 seconds
    //    var dateTime = DateTime.Now;
    //    var timespan1 = new TimeSpan(7, 0, 0);


    //    //add 20 mintues to it
    //    dateTime.AddHours(8);

    //    print(dateTime.AddHours(8));

    //    ////get the number of hours (will be 6 now since you added 20 minutes)
    //    //var hours = timespan.Hours;

    //    //print(timespan);
    //    //print(timespan.Hours);
    //    //print(timespan.TotalHours);
    //    //print(timespan.TotalMinutes + " * " + timespan.Minutes);
    //}

    private void Start()
    {
        //MinhCacheGame.AddSpinTickets(66);

        minigameController._OnStartPlaying.AddListener(InitData);


        if (InAppPurchasing.GetProductLocalizedData(EM_IAPConstants.Product_inapp_luckywheel) != null)
        {
            priceIap.text = "" + InAppPurchasing.GetProductLocalizedData(EM_IAPConstants.Product_inapp_luckywheel).localizedPriceString;
        }

        ChangeBtnVideoByPPack();
    }



    //---cương - thay đổi hiện thị nút video khi đã mua gói p.pack

    void ChangeBtnVideoByPPack()
    {

        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            labelBtnVideo.text = I2.Loc.ScriptLocalization.free;
        }
        else
        {
            labelBtnVideo.text = I2.Loc.ScriptLocalization.video;
        }
    }


    private void OnDestroy()
    {
        minigameController._OnStartPlaying.RemoveListener(InitData);
    }

    private void InitData()
    {
        rewardInfos = new LuckyWheelConfig[prizeWheel.mRewards.Count];

        for (int i = 0; i < prizeWheel.mRewards.Count; i++)
        {
            rewardInfos[i] = LuckyWheelConfig.Get("Slot" + (i + 1));
            prizeWheel.mRewards[i].Probability = rewardInfos[i].rate;

            LuckyWheelElement element = prizeWheel.mRewards[i].GetComponent<LuckyWheelElement>();
            element.SetSprite(GetRewardSpriteName(rewardInfos[i].nameItem));

            element.SetNumber(rewardInfos[i].numItem);
            element.SetType(rewardInfos[i].nameItem);

            element.GetComponent<ElementItemInfo>().CreateItem(rewardInfos[i].nameItem, rewardInfos[i].numItem, false, false);

            if (rewardInfos[i].nameItem == "Gold3")
            {
                //GameObject fxVipItemIns = NGUITools.AddChild(element.gameObject, fxVipItem);
                fxVipItem.SetActive(true);
                fxVipItem.transform.parent = element.gameObject.transform;
                fxVipItem.transform.localPosition = new Vector3(31f, 111f, 0);
            }
        }

        labelCurrentTickets.text = MinhCacheGame.GetSpinTickets().ToString();
    }

    private string GetRewardSpriteName(string type)
    {
        switch (type)
        {
            case "Gold1":
                return "item_lw_coin1";

            case "Gold2":
                return "item_lw_coin2";

            case "Gold3":
                return "item_lw_coin3";

            case "PowerUp":
                return "item_lw_pwup";

            case "ActiveSkill":
                return "item_lw_ENG";

            case "Life":
                return "item_lw_life";

            case "Gem":
                return "3gem";
            case "BoxCard1":
                return "item_lw_card";
            case "BoxCard2":
                return "item_lw_card";
            case "BoxCard3":
                return "item_lw_card";
            case "BoxCard4":
                return "item_lw_card";
            case "Energy":
                return "LW_enegys_item";
            default:
                return "";
        }
    }

    private void Update()
    {
        //if (countdownVideoPanel.activeInHierarchy)
        //{
        //    if (videoEndTime > DateTime.Now)
        //    {
        //        TimeSpan interval = videoEndTime - DateTime.Now;
        //        countdownVideoLabel.text = string.Format("{0:00}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds);
        //    }
        //    else
        //    {
        //        ActiveVideoSpin();
        //    }
        //}

        //if (countdownFreePanel.activeInHierarchy)
        //{
        //    if (freeEndTime > DateTime.Now)
        //    {
        //        TimeSpan interval = freeEndTime - DateTime.Now;
        //        countdownFreeLabel.text = string.Format("{0:00}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds);
        //    }
        //    else
        //    {
        //        ActiveFreeSpin();
        //    }
        //}
    }

    private void OnEnable()
    {
        //AdManager.RewardedAdCompleted += OnRewardedVideoCompleted;
        IAPCallbackManager.Instance.OnPurchaseSuccessed += OnPurchaseSuccessed;

        //Debug.LogError(CachePvp.dateTime + " * " + MinhCacheGame.GetCountdownVideoTime());
        //if (CachePvp.dateTime.Subtract(MinhCacheGame.GetCountdownVideoTime()).TotalDays >= 1)


        if (CacheGame.GetDayUseVideoLuckyWheel() != CachePvp.dateTime.Day)
        {
            ActiveVideoSpin();
        }
        else
        {
            DeactiveVideoSpin();
        }

        //Debug.LogError(CachePvp.dateTime + " * " + MinhCacheGame.GetCountdownFreeTime());

        //if (CachePvp.dateTime.Subtract(MinhCacheGame.GetCountdownFreeTime()).TotalDays >= 1)
        //Debug.LogError("GetDayUseFreeLuckyWheel: " + CacheGame.GetDayUseFreeLuckyWheel() + "----CachePvp.dateTime:" + CachePvp.dateTime);
        if (CacheGame.GetDayUseFreeLuckyWheel() != CachePvp.dateTime.Day)
        {
            ActiveFreeSpin();
        }
        else
        {
            DeactiveFreeSpin();
        }

        CheckToShowTutorial();
    }

    private void OnDisable()
    {
        //AdManager.RewardedAdCompleted -= OnRewardedVideoCompleted;
        IAPCallbackManager.Instance.OnPurchaseSuccessed -= OnPurchaseSuccessed;

        isSpinning = false;
        prizeWheel.StopPlay();
    }

    public void ShowPopupLuckyWheel()
    {
        SoundManager.PlayShowPopup();
        gameObject.SetActive(true);
        //FirebaseAnalytics.LogEvent("Click", new Parameter("Button", "LuckyWheel"));
        FirebaseLogSpaceWar.LogClickButton("LuckyWheel");
        PopupManager.Instance.HideChatWorldPopup();
    }

    private void DoSpin()
    {
        isSpinning = true;

        btnSpinTweenScale.enabled = false;
        highLightReward.SetActive(false);

        minigameController.AllowRound();

        //Xử lý tắt animation tutorial
        MinhCacheGame.SetAlreadyShowTutorialLuckyWheel();
        tutAnimator.gameObject.SetActive(false);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.LuckyWheelEasy.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.LuckyWheelEasy.ToString()) + 1);
        CacheGame.SetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.LuckyWheelHard.ToString(), CacheGame.GetNumberDailyQuestAchieved(GameContext.TypeItemDailyQuest.LuckyWheelHard.ToString()) + 1);
    }

    //Active spin:
    private void ActiveVideoSpin()
    {
        //countdownVideoPanel.SetActive(false);
        //btnVideoSpin.enabled = true;
        isEnableVideoSpin = true;
        btnVideoSpinSprite.spriteName = "btn_video_ads";
    }

    private void ActiveFreeSpin()
    {
        //countdownFreePanel.SetActive(false);
        //btnFreeSpin.enabled = true;
        isEnableFreeSpin = true;
        btnFreeSpinSprite.spriteName = "btn_free";
    }

    //Deactive spin:
    private void DeactiveVideoSpin()
    {
        //countdownVideoPanel.SetActive(true);
        //btnVideoSpin.enabled = false;
        isEnableVideoSpin = false;
        btnVideoSpinSprite.spriteName = "btn_free_d";
        //videoEndTime = MinhCacheGame.GetCountdownVideoTime();
    }

    private void DeactiveFreeSpin()
    {
        //countdownFreePanel.SetActive(true);
        //btnFreeSpin.enabled = false;
        isEnableFreeSpin = false;
        btnFreeSpinSprite.spriteName = "btn_free_d";
        //freeEndTime = MinhCacheGame.GetCountdownFreeTime();
    }

    //Xử lý button:
    public void OnclickButtonSpin()
    {
        SoundManager.PlayClickButton();
        if (isSpinning)
            return;

        if (MinhCacheGame.GetSpinTickets() > 0)
        {
            MinhCacheGame.RemoveSpinTickets(1);
            labelCurrentTickets.text = MinhCacheGame.GetSpinTickets().ToString();
            DoSpin();
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_luckywheel_need_more_ticket, false, 1.5f);
        }
    }


    //bool clickVideoSpin;
    public void OnClickButtonVideoSpin()
    {
        SoundManager.PlayClickButton();

        if (!isEnableVideoSpin)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_comeback_tomorrow, false, 1.5f);
            return;
        }

        if (isSpinning)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_luckywheel_wait_spin, false, 1.5f);
            return;
        }

        if (MinhCacheGame.IsAlreadyPurchasePremiumPack())
        {
            CallBackFinishVideoAds();
            return;
        }

        if (OsAdsManager.Instance.isRewardedVideoAvailable())
        {
            //AdManager.ShowRewardedAd();
            OsAdsManager.Instance.AddRewardedListenner(CallBackFinishVideoAds);
            OsAdsManager.Instance.AddClosedListenner(CallBackClosedVideoAds);
            //
            OsAdsManager.Instance.ShowRewardedAds(OsAdsManager.LocationClickVideo.btn_video_LuckyWheel);
            //clickVideoSpin = true;
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_ads_not_available, false, 1.5f);
        }
    }

    public void OnClickButtonFreeSpin()
    {
        SoundManager.PlayClickButton();

        if (!isEnableFreeSpin)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_comeback_tomorrow, false, 1.5f);
            return;
        }

        if (isSpinning)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_luckywheel_wait_spin, false, 1.5f);
            return;
        }

        MinhCacheGame.SetCoundownFreeTime(CachePvp.dateTime);

        CacheGame.SetDayUseFreeLuckyWheel(CachePvp.dateTime.Day);
        DeactiveFreeSpin();

        DoSpin();
    }

    public void OnClickButtonPurchase()
    {
        SoundManager.PlayClickButton();
        IAPCallbackManager.Instance.BuyProduct(EM_IAPConstants.Product_inapp_luckywheel, IAPCallbackManager.WherePurchase.shop);
    }

    public void OnClickButtonBack()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
        PopupManager.Instance.ShowChatWorldPopup();
    }

    //Xử lý show tutorial
    private void CheckToShowTutorial()
    {
        if (!MinhCacheGame.IsAlreadyShowTutorialLuckyWheel())
        {
            tutAnimator.gameObject.SetActive(true);
        }
    }


    //Xử lý xem video reward spin

    void CallBackFinishVideoAds()
    {
        //if (clickVideoSpin)
        //{
        MinhCacheGame.SetCoundownVideoTime(CachePvp.dateTime);

        CacheGame.SetDayUseVideoLuckyWheel(CachePvp.dateTime.Day);

        DeactiveVideoSpin();
        DoSpin();
        //}
        //clickVideoSpin = false;


    }

    void CallBackClosedVideoAds()
    {

    }

    void OnPurchaseSuccessed(string productName)
    {
        if (productName == EM_IAPConstants.Product_inapp_luckywheel)
        {
            MinhCacheGame.AddSpinTickets(6);
            labelCurrentTickets.text = MinhCacheGame.GetSpinTickets().ToString();
            //PvpUtil.SendUpdatePlayer();
            PvpUtil.SendUpdatePlayer("OnPurchaseSuccessed_Tickets");
            //PlayerPrefs.Save();
        }
    }

    //Xử lý reward cho người chơi

    public void OnSpinReward(int index)
    {
        isSpinning = false;
        btnSpinTweenScale.enabled = true;
        highLightReward.SetActive(true);

        MiniGame_Reward reward = prizeWheel.mRewards[index];
        AddGiftToPlayer(reward.GetComponent<LuckyWheelElement>().ElementType, reward.GetComponent<LuckyWheelElement>().ElementNumber);
    }

    private void AddGiftToPlayer(string giftType, int numGift)
    {
        switch (giftType)
        {
            case "Gold1":
                //DontDestroyManager.Instance.AddCoinTop(numGift);
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddCoinTop(numGift, FirebaseLogSpaceWar.LuckyWheel_why);
                }
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " " + numGift + " " + I2.Loc.ScriptLocalization.gold), true, 1.8f);
                break;

            case "Gold2":
                //DontDestroyManager.Instance.AddCoinTop(numGift);
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddCoinTop(numGift, FirebaseLogSpaceWar.LuckyWheel_why);
                }
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " " + numGift + " " + I2.Loc.ScriptLocalization.gold), true, 1.8f);
                break;

            case "Gold3":
                //DontDestroyManager.Instance.AddCoinTop(numGift);
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddCoinTop(numGift, FirebaseLogSpaceWar.LuckyWheel_why);
                }
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " " + numGift + " " + I2.Loc.ScriptLocalization.gold), true, 1.8f);

                break;
            case "PowerUp":
                CacheGame.SetTotalItemPowerUp(CacheGame.GetTotalItemPowerUp() + numGift);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " " + numGift + " " + I2.Loc.ScriptLocalization.power_up), true, 1.8f);
                SoundManager.SoundItemShow();
                break;
            case "ActiveSkill":
                CacheGame.SetTotalItemActiveSkill(CacheGame.GetTotalItemActiveSkill() + numGift);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " " + numGift + " " + I2.Loc.ScriptLocalization.ActiveSkill), true, 1.8f);
                SoundManager.SoundItemShow();
                break;
            case "Life":
                CacheGame.SetTotalItemLife(CacheGame.GetTotalItemLife() + numGift);
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " " + numGift + " " + I2.Loc.ScriptLocalization.life), true, 1.8f);
                SoundManager.SoundItemShow();
                break;
            case "Gem":
                //DontDestroyManager.Instance.AddGemTop(numGift);
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddGemTop(numGift, FirebaseLogSpaceWar.LuckyWheel_why);
                }
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " " + numGift + " " + ScriptLocalization.gem), true, 1.8f);
                SoundManager.SoundItemShow();
                break;
            case "BoxCard1":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.LuckyWheel_why, "BoxCard1");
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + GetCardInBox.textCardShowToast), true, 3f);
                break;
            case "BoxCard2":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.LuckyWheel_why, "BoxCard2");
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + GetCardInBox.textCardShowToast), true, 3f);
                break;
            case "BoxCard3":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.LuckyWheel_why, "BoxCard3");
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + GetCardInBox.textCardShowToast), true, 3f);
                break;
            case "BoxCard4":
                GetCardInBox.GetCardInBoxByRate(FirebaseLogSpaceWar.LuckyWheel_why, "BoxCard4");
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +" + GetCardInBox.textCardShowToast), true, 3f);
                break;
            case "Energy":
                if (PanelCoinGem.Instance != null)
                {
                    PanelCoinGem.Instance.AddEnergyTop(numGift, FirebaseLogSpaceWar.LuckyWheel_why);
                }
                PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " " + numGift + " " + ScriptLocalization.energy), true, 1.8f);

                break;

        }

        //  listTypeGift.Clear ();
        //  ShowGiftInReward ();
    }

    [Button]
    public void ClearData()
    {
        PlayerPrefs.DeleteAll();
    }
}
