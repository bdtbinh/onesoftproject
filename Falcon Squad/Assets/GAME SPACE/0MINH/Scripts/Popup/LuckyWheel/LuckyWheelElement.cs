﻿using System;
using UnityEngine;

public class LuckyWheelElement : MonoBehaviour
{

    [SerializeField]
    private UISprite spriteIcon;

    [SerializeField]
    private UILabel labelNumber;

    [SerializeField]
    private string type;
    public string ElementType
    { get { return type; } }

    [SerializeField]
    private int number;
    public int ElementNumber
    {
        get { return Convert.ToInt32(number); }
    }

    public void SetSprite(string name)
    {
        spriteIcon.spriteName = name;
    }

    public void SetNumber(int number)
    {
        this.number = number;

        if (number < 0)
        {
            labelNumber.gameObject.SetActive(false);
            return;
        }
        labelNumber.text = GameContext.FormatNumber((int)number);
    }

    public void SetType(string type)
    {
        this.type = type;
        gameObject.name = type;
    }
}
