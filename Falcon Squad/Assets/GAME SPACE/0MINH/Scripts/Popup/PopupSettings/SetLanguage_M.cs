﻿using I2.Loc;
using UnityEngine;

public class SetLanguage_M : MonoBehaviour {

    public string _Language;

    void OnClick()
    {
        ApplyLanguage();
    }

    public void ApplyLanguage()
    {
        if (LocalizationManager.HasLanguage(_Language))
        {
            LocalizationManager.CurrentLanguage = _Language;
        }
    }
}
