﻿using UnityEditor;
using UnityEngine;

namespace I2.Loc
{
    [CustomEditor(typeof(SetLanguage))]
    public class SetLanguageInspector_M : Editor
    {
        public SetLanguage_M setLan;
        public SerializedProperty mProp_Language;

        public void OnEnable()
        {
            setLan = (SetLanguage_M)target;
            mProp_Language = serializedObject.FindProperty("_Language");
        }

        public override void OnInspectorGUI()
        {
            string[] Languages;
            LocalizationManager.UpdateSources();
            Languages = LocalizationManager.GetAllLanguages().ToArray();
            System.Array.Sort(Languages);

            int index = System.Array.IndexOf(Languages, mProp_Language.stringValue);

            GUI.changed = false;
            index = EditorGUILayout.Popup("Language", index, Languages);
            if (GUI.changed)
            {
                if (index < 0 || index >= Languages.Length)
                    mProp_Language.stringValue = string.Empty;
                else
                    mProp_Language.stringValue = Languages[index];
                GUI.changed = false;
                serializedObject.ApplyModifiedProperties();
            }

            GUILayout.Space(5);
            GUI.contentColor = Color.white;


            serializedObject.ApplyModifiedProperties();
        }
    }
}