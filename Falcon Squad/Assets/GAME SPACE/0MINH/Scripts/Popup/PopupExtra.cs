﻿using com.ootii.Messages;
using Mp.Pvp;
using OSNet;
using System;
using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PopupExtra : MonoBehaviour
{
    List<Transform> listMenu = new List<Transform>();
    bool isMegaTournamentStart;
    bool is2vs2TournamentStart;

    [SerializeField]
    private UILabel lHighestWave;

    public UILabel lWinrate;
    public UILabel lRank;

    [SerializeField]
    private GameObject popupLoadingPvP;

    public UILabel lProgress;
    public UI2DSprite sProgress;
    public GameObject sProgressBackground;

    //Halloween event
    //public GameObject btnEvent;
    //public UILabel lEventCountdown;
    public UIScrollView scrollView;

    //Tournament
    public UILabel lEndIn;
    public UILabel lStartX4;
    public UILabel lClaimedInReward;

    //2vs2

    public UILabel lEndIn2VS2;
    //public UILabel lStartX4;
    //public UILabel lClaimedInReward;

    //private void Start()
    //{

    //if (dab == null)
    //{
    //    dab = new DownloadAssetBundle(FireBaseRemote.Instance.GetUrlAssetBundle());
    //}
    //}

    Vector3 vt3;
    private void OnEnable()
    {
        if (listMenu.Count == 0)
        {
            int start = -300;
            int j = 0;
            for (int i = 0; i < scrollView.transform.GetChild(0).childCount; i++)
            {
                if (scrollView.transform.GetChild(0).GetChild(i).gameObject.activeSelf)
                {
                    listMenu.Add(scrollView.transform.GetChild(0).GetChild(i));
                    vt3.x = 0;
                    vt3.z = 0;
                    vt3.y = start;
                    listMenu[j].transform.localPosition = vt3;
                    j++;
                    start -= 250;
                }
            }
        }
        MessageDispatcher.AddListener(EventName.PopupExtra.GoToPVP.ToString(), OnGoToPVP, true);
        MessageDispatcher.AddListener(EventName.PopupExtra.DisablePopupExtra.ToString(), OnDisablePopupExtra, true);
        MessageDispatcher.AddListener(EventName.Tournament.MegaTournament.ToString(), OnMegaTournament, true);
        MessageDispatcher.AddListener(EventName.PVP.CoopPvPTournamentInfo.ToString(), OnCoopPvPTournamentInfo, true);

        lHighestWave.text = I2.Loc.ScriptLocalization.highest_wave.Replace("%{wave_number}", CacheGame.GetMaxWavesEndlessReached().ToString());
        lWinrate.text = I2.Loc.ScriptLocalization.win_rate.Replace("%{percent_number}", CachePvp.GetWinrateText());
        lRank.text = I2.Loc.ScriptLocalization.rank + ": " + CachePvp.Ranking;
        lEndIn.text = "";

        //lEndIn2VS2.text = "";

        lStartX4.color = Color.clear;
        lClaimedInReward.color = Color.white;
        lClaimedInReward.text = "";
        //lProgress.gameObject.SetActive(false);
        //sProgress.gameObject.SetActive(false);
        //sProgressBackground.SetActive(false);
        //CheckShowHalloweenUI();

        new CSExpendsion().Send();

        //tournament
        GetTournamentInfo();

        //=================================================================
        //Event Summer Holiday 2019
        //=================================================================
        //new CSChristmas().Send();
        //DateTime expiredTime = DateTime.Now;
        //DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out expiredTime);


        ////Debug.LogError(CachePvp.dateTime + " * " + expiredTime);

        //if (CachePvp.dateTime >= expiredTime || DateTime.Now >= expiredTime)
        //{
        //    btnEvent.SetActive(false);
        //}
        //else

        //{
        //    btnEvent.SetActive(true);
        //    //StartCoroutine(CountDown());

        //    DateTime startTime = DateTime.Now;
        //    DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out startTime);

        //    //Debug.LogError(CachePvp.dateTime + " * " + startTime);


        //    if (CachePvp.dateTime < startTime && DateTime.Now < startTime)
        //    {
        //        StartCoroutine(CountDownStartTime());
        //    }
        //    else
        //    {
        //        StartCoroutine(CountDownEndTime());
        //    }
        //}
    }

    void RefreshPositionMenu()
    {
        int start = -550;
        for (int i = 1; i < listMenu.Count; i++)
        {
            if ((listMenu[i].name.Equals("B_Tournament") && isMegaTournamentStart) || (listMenu[i].name.Equals("B_2vs2") && is2vs2TournamentStart))
            {
                vt3.x = 0;
                vt3.z = 0;
                vt3.y = start;
                listMenu[i].transform.localPosition = vt3;
                start -= 250;
            }
        }

        for (int i = 1; i < listMenu.Count; i++)
        {
            if ((listMenu[i].name.Equals("B_Tournament") && !isMegaTournamentStart) || (listMenu[i].name.Equals("B_2vs2") && !is2vs2TournamentStart) || (!listMenu[i].name.Equals("B_Tournament") && !listMenu[i].name.Equals("B_2vs2")))
            {
                vt3.x = 0;
                vt3.z = 0;
                vt3.y = start;
                listMenu[i].transform.localPosition = vt3;
                start -= 250;
            }
        }
    }

    private void OnCoopPvPTournamentInfo(com.ootii.Messages.IMessage rMessage)
    {
        SCCoopPvPTournamentInfo m = rMessage.Data as SCCoopPvPTournamentInfo;
        if (m.status == SCCoopPvPTournamentInfo.SUCCESS)
        {
            if (m.info.state == PvP2vs2TournamentInfo.START) //đang diễn ra
            {
                is2vs2TournamentStart = true;
                int day = 0;
                if (m.info.endTimeRemain > 86400)
                {
                    day = Mathf.FloorToInt(m.info.endTimeRemain / 86400f);
                }
                if (day == 0)
                {
                    string time = "";
                    time = (m.info.endTimeRemain / 3600).ToString("00") + ":" + ((m.info.endTimeRemain % 3600) / 60).ToString("00");
                    lEndIn2VS2.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", time);
                }
                else
                {
                    if (day > 1)
                    {
                        lEndIn2VS2.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", day + " " + I2.Loc.ScriptLocalization.days);
                    }
                    else
                    {
                        lEndIn2VS2.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", day + " " + I2.Loc.ScriptLocalization.day);
                    }
                }
            }
            else
            {
                //da ket thuc
                is2vs2TournamentStart = false;
                int day = 0;
                if (m.info.startTimeRemain > 86400)
                {
                    day = Mathf.FloorToInt(m.info.startTimeRemain / 86400f);
                }
                if (day == 0)
                {
                    string time = "";
                    time = (m.info.startTimeRemain / 3600).ToString("00") + ":" + ((m.info.startTimeRemain % 3600) / 60).ToString("00");
                    lEndIn2VS2.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", time);
                }
                else
                {
                    if (day > 1)
                    {
                        lEndIn2VS2.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", day + " " + I2.Loc.ScriptLocalization.days);
                    }
                    else
                    {
                        lEndIn2VS2.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", day + " " + I2.Loc.ScriptLocalization.day);
                    }
                }
            }

            RefreshPositionMenu();
        }
    }


    //IEnumerator CountDownStartTime()
    //{
    //    DateTime startTime = DateTime.Now;
    //    DateTime.TryParse(CacheFireBase.GetDayStartEvent, out startTime);

    //    while (DateTime.Now < startTime)
    //    {
    //        TimeSpan interval = startTime - DateTime.Now;

    //        string time = "";

    //        if (interval.Days > 0)
    //            time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
    //        else
    //            time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds) + "[-]";

    //        lEventCountdown.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", time);
    //        yield return new WaitForSeconds(1f);
    //    }

    //    StartCoroutine(CountDownEndTime());
    //}

    //IEnumerator CountDownEndTime()
    //{
    //    DateTime expiredTime = DateTime.Now;
    //    DateTime.TryParse(CacheFireBase.GetDayFinishEvent, out expiredTime);

    //    while (DateTime.Now < expiredTime)
    //    {
    //        TimeSpan interval = expiredTime - DateTime.Now;

    //        string time = "";

    //        if (interval.Days > 0)
    //            time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
    //        else
    //            time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds) + "[-]";

    //        lEventCountdown.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", time);
    //        yield return new WaitForSeconds(1f);
    //    }

    //    btnEvent.SetActive(false);
    //    scrollView.ResetPosition();

    //    //Debug.LogError("aaaa2: " + CachePvp.dateTime + " * " + expiredTime + " * " + DateTime.Now);
    //}

    private void GetTournamentInfo()
    {
        new CSMegaTournament().Send();
        new CSCoopPvPTournamentInfo().Send();
    }

    private void OnMegaTournament(com.ootii.Messages.IMessage rMessage)
    {
        SCMegaTournament m = rMessage.Data as SCMegaTournament;
        if (m.status == SCMegaTournament.SUCCESS)
        {
            if (m.tournament.startTimeRemain == 0) //đang diễn ra
            {
                isMegaTournamentStart = true;
                if (m.tournament.countRewarded == m.tournament.totalReward)
                {
                    lStartX4.color = Color.clear;
                    lClaimedInReward.color = Color.white;
                    lClaimedInReward.text = I2.Loc.ScriptLocalization.complete;
                }
                else
                {
                    lStartX4.color = Color.white;
                    lClaimedInReward.color = Color.white;
                    lClaimedInReward.text = I2.Loc.ScriptLocalization.claimed_reward + " " + m.tournament.countRewarded + "/" + m.tournament.totalReward;
                }
                lStartX4.text = I2.Loc.ScriptLocalization.start.ToUpper() + " x" + m.tournament.ticket;
                int day = 0;
                if (m.tournament.endTimeRemain > 86400)
                {
                    day = Mathf.FloorToInt(m.tournament.endTimeRemain / 86400f);
                }
                if (day == 0)
                {
                    string time = "";
                    time = (m.tournament.endTimeRemain / 3600).ToString("00") + ":" + ((m.tournament.endTimeRemain % 3600) / 60).ToString("00");
                    lEndIn.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", time);
                }
                else
                {
                    if (day > 1)
                    {
                        lEndIn.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", day + " " + I2.Loc.ScriptLocalization.days);
                    }
                    else
                    {
                        lEndIn.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", day + " " + I2.Loc.ScriptLocalization.day);
                    }
                }
            }
            else
            {
                isMegaTournamentStart = false;
                //da ket thuc
                lStartX4.color = Color.clear;
                lClaimedInReward.color = Color.clear;
                int day = 0;
                if (m.tournament.startTimeRemain > 86400)
                {
                    day = Mathf.FloorToInt(m.tournament.startTimeRemain / 86400f);
                }
                if (day == 0)
                {
                    string time = "";
                    time = (m.tournament.startTimeRemain / 3600).ToString("00") + ":" + ((m.tournament.startTimeRemain % 3600) / 60).ToString("00");
                    lEndIn.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", time);
                }
                else
                {
                    if (day > 1)
                    {
                        lEndIn.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", day + " " + I2.Loc.ScriptLocalization.days);
                    }
                    else
                    {
                        lEndIn.text = I2.Loc.ScriptLocalization.open_in.Replace("%{time}", day + " " + I2.Loc.ScriptLocalization.day);
                    }
                }
            }
        }
    }

    public void OnClickBtnTournament()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }
        if (!NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        //if (CachePvp.megaTournamentInfo == null || CachePvp.megaTournamentInfo.info.startTimeRemain > 0)
        //{
        //    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_tourmanet_not_start_yet, false, 1.5f);
        //}
        //else
        //{
        if (!CachePvp.sCVersionConfig.canTournament)
        {
            PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainTournament);
            return;
        }
        PopupManager.Instance.ShowTournamentPopup();
        //}
    }

    //private void CheckShowHalloweenUI()
    //{
    //    DateTime endDate = new DateTime(2018, 11, CacheFireBase.GetDayFinishEvent + 1, 0, 0, 0);
    //    Debug.LogError("Pvp Datetime: " + CachePvp.dateTime + ", Datetime.Now: " + DateTime.Now);
    //    if (CachePvp.dateTime < endDate && DateTime.Now < endDate)
    //    {
    //        panelHalloween.SetActive(true);
    //        lEndsInHLW.text = I2.Loc.ScriptLocalization.end_in.Replace("%{time}", CacheFireBase.GetDayFinishEvent + "/11" + "/2018");
    //    }
    //    else
    //    {
    //        panelHalloween.SetActive(false);
    //    }
    //}

    private void OnDisable()
    {
        waitFromServer = false;
        MessageDispatcher.RemoveListener(EventName.PopupExtra.GoToPVP.ToString(), OnGoToPVP, true);
        MessageDispatcher.RemoveListener(EventName.PopupExtra.DisablePopupExtra.ToString(), OnDisablePopupExtra, true);
        MessageDispatcher.RemoveListener(EventName.Tournament.MegaTournament.ToString(), OnMegaTournament, true);
        MessageDispatcher.RemoveListener(EventName.PVP.CoopPvPTournamentInfo.ToString(), OnCoopPvPTournamentInfo, true);

        StopAllCoroutines();
    }

    void OnDisablePopupExtra(com.ootii.Messages.IMessage msg)
    {
        waitFromServer = false;
    }

    void OnGoToPVP(com.ootii.Messages.IMessage msg)
    {
        if (waitFromServer)
        {
            canGoToPVP = true;
            if (CachePvp.Name == "")
            {
                PopupManager.Instance.ShowRegisterPopup("pvp");
            }
            else
            {
                //PvpUtil.SendUpdatePlayer();
                //PvpUtil.SendUpdatePlayer("OnGoToPVP");
                SceneManager.LoadScene("PVPMain");
            }
        }
    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);
        PopupManager.Instance.ShowChatWorldPopup();
    }

    bool waitFromServer = false;
    bool canGoToPVP = false;



    public void OnClickBtnPvP()
    {
        Debug.Log("canPlayPVP extra: " + CachePvp.CanPlayPvp);
        Debug.Log("RequireLevelPvP extra: " + CachePvp.RequireLevelPlayPvp);
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }

        if (CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL) <= CachePvp.RequireLevelPlayPvp)
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", CachePvp.RequireLevelPlayPvp.ToString()));
            return;
        }
        if (!CachePvp.sCVersionConfig.canPVP)
        {
            PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainPVP);
            return;
        }

        if (CacheGame.GetTotalCoin() < Constant.BETTING_AMOUNT_MIN)
        {
            if (CachePvp.isShowPopupVideo)
            {
                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_more_gold);
            }
            else
            {
                CachePvp.isShowPopupVideo = true;
                List<PromotionItem> list = new List<PromotionItem>();
                PromotionItem pi = new PromotionItem();
                pi.itemIndex = 1;
                pi.volume = 300;
                list.Add(pi);
                PopupManager.Instance.ShowNotifyPopup(0, "21", "Xem video ngắn để nhận 300 vàng", "", 0, list);
            }
            return;
        }
        if (CachePvp.CanPlayPvp == 1)
        {
            if (!NetManager.Instance.IsOnline())
            {
                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            }
            else
            {
                if (canGoToPVP)
                {
                    if (CachePvp.Name == "")
                    {
                        PopupManager.Instance.ShowRegisterPopup("pvp");
                    }
                    else
                    {
                        //PvpUtil.SendUpdatePlayer();
                        //PvpUtil.SendUpdatePlayer("OnClickBtnPvP");
                        SceneManager.LoadScene("PVPMain");
                    }
                }
                else
                {
                    if (!waitFromServer)
                    {
                        waitFromServer = true;
                        new CSCanPlayPVP().Send();
                    }
                }
            }
        }
        else
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_coming_soon);
            return;
        }
    }


    public void OnClickBtnEvent()
    {
        SoundManager.PlayClickButton();

        DateTime startTime = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayStartEvent, out startTime);

        if (CachePvp.dateTime < startTime && DateTime.Now < startTime)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_not_started_yet, false, 1.5f);
            return;
        }

        GameContext.modeGamePlay = GameContext.ModeGamePlay.Events;
        //PopupManagerCuong.Instance.ShowEventPopup();
        PopupManagerCuong.Instance.ShowEventPopup();

    }

    //public void OnClickBtnPvP()
    //{
    //    if (Application.internetReachability == NetworkReachability.NotReachable)
    //    {
    //        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
    //        return;
    //    }
    //    if (CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL) < CachePvp.RequireLevelPlayPvp)
    //    {
    //        PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_unlock_at_level + " " + CachePvp.RequireLevelPlayPvp);
    //        return;
    //    }

    //    if (CacheGame.GetTotalCoin() < Constant.BETTING_AMOUNT_MIN)
    //    {
    //        PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_more_gold);
    //        return;
    //    }

    //    StartCoroutine(OnClickBtnPvPCoroutine());
    //    StartCoroutine(ShowDowloadProcess());
    //}

    //private IEnumerator ShowDowloadProcess()
    //{
    //    yield return null;
    //    if (!dab.IsDone)
    //    {
    //        lProgress.gameObject.SetActive(true);
    //        sProgress.gameObject.SetActive(true);
    //        sProgressBackground.SetActive(true);
    //    }
    //    while (!dab.IsDone)
    //    {
    //        //text = dab.DownloadProgress;//From 0->1
    //        //text2 = dab.DownloadedBytes;
    //        lProgress.text = Mathf.RoundToInt(dab.DownloadProgress * 100) + "%";
    //        sProgress.fillAmount = dab.DownloadProgress;
    //        yield return null;
    //    }


    //}



    //public static DownloadAssetBundle dab;
    //private IEnumerator OnClickBtnPvPCoroutine()
    //{
    //    Debug.Log("dab.IsDone:" + dab.IsDone);
    //    if (!dab.IsDone)
    //    {
    //        yield return dab.Start();
    //        FirebaseLogSpaceWar.LogTimeLoadAssetBundle(dab.CompleteTime.ToString());
    //    }
    //    if (dab.IsDone)
    //    {
    //        Debug.Log("canPlayPVP extra: " + CachePvp.CanPlayPvp);
    //        Debug.Log("RequireLevelPvP extra: " + CachePvp.RequireLevelPlayPvp);
    //        if (dab.Result != null)
    //        {
    //            PvPDataHelper.Instance.waveCaches = dab.Result.ToArray();
    //        }
    //        if (CachePvp.CanPlayPvp == 1)
    //        {
    //            if (!NetManager.Instance.IsOnline())
    //            {
    //                PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
    //            }
    //            else
    //            {
    //                if (canGoToPVP)
    //                {
    //                    if (CachePvp.Name == "")
    //                    {
    //                        PopupManager.Instance.ShowRegisterPopup("pvp");
    //                    }
    //                    else
    //                    {
    //                        PvpUtil.SendUpdatePlayer();
    //                        SceneManager.LoadScene("PVPMain");
    //                    }
    //                }
    //                else
    //                {
    //                    if (!waitFromServer)
    //                    {
    //                        waitFromServer = true;
    //                        new CSCanPlayPVP().Send();
    //                    }
    //                }
    //            }
    //        }
    //        else
    //        {
    //            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_coming_soon);
    //            yield break;
    //        }
    //    }
    //    else
    //    {
    //        //print(dab.Error);
    //        PopupManager.Instance.ShowToast("Download error! Try again later.");
    //    }
    //}

    private void OnConnectSuccessed()
    {
        //popupLoadingPvP.SetActive(false);
        SceneManager.LoadScene("UIPVP");
    }

    private void OnConnectFailed()
    {
        //popupLoadingPvP.SetActive(false);
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_failed_to_connect, false, 1.5f);
    }

    public void OnClickBtnEndless()
    {

        SoundManager.PlayClickButton();

        if (CacheGame.GetMaxLevel3Difficult() > GameContext.LEVEL_UNLOCK_ENDLESS_MODE)
        {
            GameContext.modeGamePlay = GameContext.ModeGamePlay.EndLess;

            gameObject.SetActive(false);
            //DontDestroyManager.Instance.popupSelectLevel.SetActive(true);
            //DontDestroyManager.Instance.popupSelectLevel.GetComponent<PopupSelectLvCampaign>().SetPopupOpen();
            PopupManagerCuong.Instance.ShowSelectLevelPopup();
            GameContext.levelCampaignMode = GameContext.LevelCampaignMode.Normal;

            //GameContext.typeOpenSelectItemFrom = GameContext.TypeOpenSelectItemFrom.EndlessButton;
        }
        else
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", GameContext.LEVEL_UNLOCK_ENDLESS_MODE + ""), false, 1.38f);
        }
    }



    public void OnClickBtnCoOp()
    {
        SoundManager.PlayClickButton();
        gameObject.SetActive(false);

        //GameContext.typeOpenSelectItemFrom = GameContext.TypeOpenSelectItemFrom.CoOpButton;


        if (CoOpModeManager.Instance != null)
        {
            CoOpModeManager.Instance.coopPopupObj.SetActive(true);
        }
    }

    public void OnClickBtn2VS2()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }
        if (!NetManager.Instance.IsOnline())
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.notifi_pvp_disconnect_server);
            return;
        }
        if (CacheGame.GetMaxLevel(GameContext.DIFFICULT_NOMAL) <= CachePvp.RequireLevelPlay2v2)
        {
            PopupManager.Instance.ShowToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", CachePvp.RequireLevelPlay2v2.ToString()));
            return;
        }
        if (!CachePvp.sCVersionConfig.canPVP2v2)
        {
            PopupManager.Instance.ShowForceUpdatePopup(CachePvp.sCVersionConfig.isMaintainPVP2v2);
            return;
        }
        PopupManager.Instance.ShowPVP2V2Popup();
    }
}
