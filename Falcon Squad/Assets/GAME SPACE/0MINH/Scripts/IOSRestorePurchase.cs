﻿using EasyMobile;
using Firebase.Analytics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IOSRestorePurchase : MonoBehaviour
{

    public static bool clickIOSRestorePurchase = false;
    bool IsAlreadyRestorePurchase()
    {
        return PlayerPrefs.GetInt("AlreadyRestorePurchase", 0) == 1;
    }

    void SetAlreadyRestorePurchase()
    {
        PlayerPrefs.SetInt("AlreadyRestorePurchase", 1);
    }

    void Start()
    {
        if (IsAlreadyRestorePurchase())
            gameObject.SetActive(false);
    }

    public void OnClickBtnRestorePurchase()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_network_not_found, false, 1.5f);
            return;
        }
        clickIOSRestorePurchase = true;
        InAppPurchasing.RestorePurchases();
    }

    // Subscribe to IAP purchase events
    void OnEnable()
    {
        InAppPurchasing.PurchaseCompleted += PurchaseCompletedHandler;

        InAppPurchasing.RestoreCompleted += RestoreCompletedHandler;
        InAppPurchasing.RestoreFailed += RestoreFailedHandler;
    }

    // Unsubscribe when the game object is disabled
    void OnDisable()
    {
        InAppPurchasing.PurchaseCompleted -= PurchaseCompletedHandler;

        InAppPurchasing.RestoreCompleted -= RestoreCompletedHandler;
        InAppPurchasing.RestoreFailed -= RestoreFailedHandler;
    }

    void RestoreCompletedHandler()
    {
		clickIOSRestorePurchase = false;
        gameObject.SetActive(false);
        SetAlreadyRestorePurchase();
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_purchases_restored_success, false, 2f);
    }

    void RestoreFailedHandler()
    {
        PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.notifi_purchases_restored_failed, false, 2f);
    }


    void PurchaseCompletedHandler(IAPProduct product)
    {
        // Compare product name to the generated name constants to determine which product was bought
        //Debug.LogError("IOSRestorePurchase PurchaseCompletedHandler product:" + product);
        switch (product.Name)
        {
            case EM_IAPConstants.Product_inapp_gold_infinitypack_new:
                CacheGame.SetBuyInfinityPack(1);
                break;

            case EM_IAPConstants.Product_inapp_starterpack_v2:
                CacheGame.SetUsedInapp(1);
                //CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.SkyWraith, 1);
                CacheGame.SetPurchasedStarterPack(1);
                break;

            case EM_IAPConstants.Product_inapp_starterpack_v3:
                break;

            case EM_IAPConstants.Product_inapp_vippack:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.TwilightX, 1);
                CacheGame.SetWingManIsUnlocked((int)WingmanTypeEnum.Lazer, 1);
                //CacheGame.SetSecondaryWeaponIsUnlocked((int)SecondaryWeaponTypeEnum.ChainSaw, 1);
                CacheGame.SetPurchasedVIPPack(1);
                break;

            case EM_IAPConstants.Product_inapp_vippack_sale_lv2:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.TwilightX, 1);
                CacheGame.SetWingManIsUnlocked((int)WingmanTypeEnum.Lazer, 1);
                //CacheGame.SetSecondaryWeaponIsUnlocked((int)SecondaryWeaponTypeEnum.ChainSaw, 1);
                CacheGame.SetPurchasedVIPPack(1);
                break;

            case EM_IAPConstants.Product_inapp_vippack_sale_lv3:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.TwilightX, 1);
                CacheGame.SetWingManIsUnlocked((int)WingmanTypeEnum.Lazer, 1);
                //CacheGame.SetSecondaryWeaponIsUnlocked((int)SecondaryWeaponTypeEnum.ChainSaw, 1);
                CacheGame.SetPurchasedVIPPack(1);
                break;

            case EM_IAPConstants.Product_inapp_plane3_v2:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.FuryOfAres, 1);
                break;

            case EM_IAPConstants.Product_inapp_plane_noel:
                break;

            case EM_IAPConstants.Product_inapp_plane7:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.TwilightX, 1);
                break;

            case EM_IAPConstants.Product_inapp_drone3laser:
                CacheGame.SetWingManIsUnlocked((int)WingmanTypeEnum.Lazer, 1);
                break;

            case EM_IAPConstants.Product_inapp_gun_chainsaw:
                //CacheGame.SetSecondaryWeaponIsUnlocked((int)SecondaryWeaponTypeEnum.ChainSaw, 1);
                //CacheGame.SetSecondaryWeaponSelected((int)SecondaryWeaponTypeEnum.ChainSaw);
                break;

            case EM_IAPConstants.Product_inapp_drone5homing:
                CacheGame.SetWingManIsUnlocked((int)WingmanTypeEnum.HomingMissile, 1);
                break;

            case EM_IAPConstants.Product_inapp_gun_lightingbolt:
                //CacheGame.SetSecondaryWeaponIsUnlocked((int)SecondaryWeaponTypeEnum.LightningBolt, 1);
                //CacheGame.SetSecondaryWeaponSelected((int)SecondaryWeaponTypeEnum.LightningBolt);
                break;

            case EM_IAPConstants.Product_inapp_removeads:
                CacheGame.SetUsedInapp(1);
                break;

            case EM_IAPConstants.Product_inapp_event_black_50usd:
                CacheGame.ValueBuyedStandardCombo = 1;
                break;

            case EM_IAPConstants.Product_inapp_event_black_100usd:
                CacheGame.ValueBuyedMegaCombo = 1;
                break;

            case EM_IAPConstants.Product_unlock_aircraft_2:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.SkyWraith, 1);
                break;

            case EM_IAPConstants.Product_unlock_aircraft_3:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.FuryOfAres, 1);
                break;

            case EM_IAPConstants.Product_unlock_aircraft_6:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.MacBird, 1);
                break;

            case EM_IAPConstants.Product_unlock_aircraft_7:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.TwilightX, 1);
                break;

            case EM_IAPConstants.Product_unlock_aircraft_8:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.StarBomb, 1);
                break;

            case EM_IAPConstants.Product_unlock_aircraft_9:
                CacheGame.SetSpaceShipIsUnlocked((int)AircraftTypeEnum.IceShard, 1);
                break;

            case EM_IAPConstants.Product_unlock_drone_2:
                CacheGame.SetWingManIsUnlocked((int)WingmanTypeEnum.AutoGatlingGun, 1);
                break;

            case EM_IAPConstants.Product_unlock_drone_3:
                CacheGame.SetWingManIsUnlocked((int)WingmanTypeEnum.Lazer, 1);
                break;

            case EM_IAPConstants.Product_unlock_drone_4:
                CacheGame.SetWingManIsUnlocked((int)WingmanTypeEnum.DoubleGalting, 1);
                break;

            case EM_IAPConstants.Product_unlock_drone_5:
                CacheGame.SetWingManIsUnlocked((int)WingmanTypeEnum.HomingMissile, 1);
                break;

            case EM_IAPConstants.Product_unlock_drone_6:
                CacheGame.SetWingManIsUnlocked((int)WingmanTypeEnum.Splasher, 1);
                break;

            case EM_IAPConstants.Product_aircraft_2_sale_25:
                CacheGame.SetSpaceShipIsUnlocked(2, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.SkyWraith, 60);
                break;

            case EM_IAPConstants.Product_aircraft_2_sale_50:
                CacheGame.SetSpaceShipIsUnlocked(2, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.SkyWraith, 60);
                break;

            case EM_IAPConstants.Product_aircraft_3_sale_25:
                CacheGame.SetSpaceShipIsUnlocked(3, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.FuryOfAres, 60);
                break;

            case EM_IAPConstants.Product_aircraft_3_sale_50:
                CacheGame.SetSpaceShipIsUnlocked(3, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.FuryOfAres, 60);
                break;

            case EM_IAPConstants.Product_aircraft_6_sale_25:
                CacheGame.SetSpaceShipIsUnlocked(6, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.MacBird, 60);
                break;

            case EM_IAPConstants.Product_aircraft_6_sale_50:
                CacheGame.SetSpaceShipIsUnlocked(6, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.MacBird, 60);
                break;

            case EM_IAPConstants.Product_aircraft_7_sale_25:
                CacheGame.SetSpaceShipIsUnlocked(7, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.TwilightX, 60);
                break;

            case EM_IAPConstants.Product_aircraft_7_sale_50:
                CacheGame.SetSpaceShipIsUnlocked(7, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.TwilightX, 60);
                break;

            case EM_IAPConstants.Product_aircraft_8_sale_25:
                CacheGame.SetSpaceShipIsUnlocked(8, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.StarBomb, 60);
                break;

            case EM_IAPConstants.Product_aircraft_8_sale_50:
                CacheGame.SetSpaceShipIsUnlocked(8, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.StarBomb, 60);
                break;

            case EM_IAPConstants.Product_aircraft_9_sale_25:
                CacheGame.SetSpaceShipIsUnlocked(9, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.IceShard, 60);
                break;

            case EM_IAPConstants.Product_aircraft_9_sale_50:
                CacheGame.SetSpaceShipIsUnlocked(9, 1);
                //CacheGame.SetSpaceShipLevel(AircraftTypeEnum.IceShard, 60);
                break;

            case EM_IAPConstants.Product_unlock_wing_1:
                CacheGame.SetWingIsUnlocked((int)WingTypeEnum.WingOfJustice, 1);
                break;

            case EM_IAPConstants.Product_unlock_wing_2:
                CacheGame.SetWingIsUnlocked((int)WingTypeEnum.WingOfRedemption, 1);
                break;

            case EM_IAPConstants.Product_unlock_wing_3:
                CacheGame.SetWingIsUnlocked((int)WingTypeEnum.WingOfResolution, 1);
                break;

            default:
                break;
                // More products here...
        }
        //clickIOSRestorePurchase = false;
    }
}
