﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MinhUtils{

    //Tìm kiếm trong cấp đầu tiên
    public static Transform FindChild(Transform parent, string name)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (child.name == name) return child;
        }
        return null;
    }

    public static List<Transform> FindChildren(Transform parent, string name)
    {
        List<Transform> list = new List<Transform>();

        for (int i = 0; i < parent.childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (child.name == name) list.Add(child);
        }

        return list;
    }

    public static void MoveAnItemToFront<T>(this List<T> list, int index)
    {
        T item = list[index];
        for (int i = index; i > 0; i--)
            list[i] = list[i - 1];
        list[0] = item;
    }

    public static void MoveAnItemToEnd<T>(this List<T> list, int index)
    {
        T item = list[index];
        for (int i = index; i < list.Count - 1; i++)
            list[i] = list[i + 1];
        list[list.Count - 1] = item;
    }

    public static void Shuffle<T>(T[] array)
    {
        System.Random rng = new System.Random();
        int n = array.Length;
        while (n > 1)
        {
            int k = rng.Next(n--);
            T temp = array[n];
            array[n] = array[k];
            array[k] = temp;
        }
    }
}
