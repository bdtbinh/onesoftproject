﻿using UnityEngine;
using System.Collections;

public class MinhLaser : MonoBehaviour
{

    //Khoang thoi gian check va cham voi target 
    [SerializeField]
    private float checkTime = 0.1f;

    [SerializeField]
    private float maxLaserDistance = 20f;

    //Layermask cua target can check va cham
    [SerializeField]
    private LayerMask layermask;

    private void OnEnable()
    {
        intervalCheckHit = IntervalCheckHit();
        StartCoroutine(intervalCheckHit);
    }

    private void OnDisable()
    {
        StopCoroutine(intervalCheckHit);
    }

    private IEnumerator intervalCheckHit;
    private IEnumerator IntervalCheckHit()
    {
        while (true)
        {
            var targetForward = transform.rotation * Vector3.right;

            RaycastHit2D hitInfo = Physics2D.Raycast(this.transform.position, targetForward, maxLaserDistance, layermask);

            //if (OnLaserDamageTarget == null)
            //    Debug.Log("OnLaserDamageTarget null");

            //if (hitInfo.collider == null)
            //    Debug.Log("hitInfo.collider null");

            if (hitInfo.collider != null && OnLaserDamageTarget != null)
            {
                OnLaserDamageTarget(hitInfo);
                //Debug.Log("laser hit");
            }

            yield return new WaitForSeconds(checkTime);
        }
    }

    //private void Update()
    //{
    //    var targetForward = transform.rotation * Vector3.right * maxLaserDistance;
    //    Debug.DrawRay(transform.position, targetForward, Color.yellow);
    //}


    public delegate void OnDamageTarget(RaycastHit2D hitInfo);
    public event OnDamageTarget OnLaserDamageTarget;
}
