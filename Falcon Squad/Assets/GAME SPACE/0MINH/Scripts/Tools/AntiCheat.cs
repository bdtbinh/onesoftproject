﻿using Mp.Pvp;
using UnityEngine;

public static class AntiCheat
{

    public const string PRIVATE_KEY = "falconsquad_2018_Lrwa2WQV";
    public const string CHECKSUM_KEY = "CHECKSUM";

    //public static void SetInt(string key, int value, int fakeNumber = -95)
    //{
    //    PlayerPrefs.SetInt(key, value * fakeNumber + fakeNumber);
    //}

    public static void SetInt(string key, int value, int fakeNumber = -95)
    {
        int fakeValue = value * fakeNumber + fakeNumber;
        PlayerPrefs.SetInt(key, fakeValue);
        PlayerPrefs.SetString(CHECKSUM_KEY + key, md5(key, fakeValue));
    }

    

    public static int GetInt(string key, int defaultValue, int fakeNumber = -95)
    {
        if (!PlayerPrefs.HasKey(key))
            return defaultValue;

        int fakeValue = PlayerPrefs.GetInt(key);

        if (PlayerPrefs.HasKey(CHECKSUM_KEY + key) && PlayerPrefs.GetString(CHECKSUM_KEY + key).CompareTo(md5(key, fakeValue)) != 0)
        {
            //Hacked 
            new CSHack(key, ((fakeValue - fakeNumber) / fakeNumber).ToString()).Send();
            return 0;
        }
        else
        {
            return ((fakeValue - fakeNumber) / fakeNumber);
        }
    }


    public static int GetIntConverted(string oldKey, string newKey, int defaultValue, int fakeNumber = -95)
    {
        if (PlayerPrefs.HasKey(newKey))
        {
            int fakeValue = PlayerPrefs.GetInt(newKey);

            if (PlayerPrefs.HasKey(CHECKSUM_KEY + newKey) && PlayerPrefs.GetString(CHECKSUM_KEY + newKey).CompareTo(md5(newKey, fakeValue)) != 0)
            {
                //Hacked 
                new CSHack(newKey, ((fakeValue - fakeNumber) / fakeNumber).ToString()).Send();
                return 0;
            }
            else
            {
                return ((fakeValue - fakeNumber) / fakeNumber);
            }
        }
        else
        {
            return PlayerPrefs.GetInt(oldKey, defaultValue);
        }
    }

    public static void SetIntConverted(string newKey, int value, int fakeNumber = -95)
    {

        int fakeValue = value * fakeNumber + fakeNumber;
        PlayerPrefs.SetInt(newKey, fakeValue);
        PlayerPrefs.SetString(CHECKSUM_KEY + newKey, md5(newKey, fakeValue));
    }


    public static string GetString3Difficult(string difficult)
    {
        switch (difficult)
        {
            case "Normal":
                return "Che_Do_De";
            case "Hard":
                return "Che_Do_BinhThuong";
            case "Hell":
                return "Che_Do_Kho";
            default:
                return "Che_Do_De";
        }
    }

    /**
     * Code thêm cho anh các điều kiện để check tồn tại key, phiên bản cũ mới, v..v
     */

    //public static void SetIntSafe(string key, int value)
    //   {
    //	PlayerPrefs.SetInt(key, value);
    //	PlayerPrefs.SetString(CHECKSUM_KEY + key, md5(key, value));
    //   }

    //  public static int GetIntSafe(string key, int defaultValue)
    //  {
    //int value = PlayerPrefs.GetInt(key);

    //if (PlayerPrefs.GetString(CHECKSUM_KEY + key).CompareTo(md5(key, value)) != 0){
    //	//Hacked 
    //	return 0;
    //}else{
    //	return value;
    //}
    //  }

    //public static void SetFloatSafe(string key, float value)
    //{
    //    PlayerPrefs.SetFloat(key, value);
    //    PlayerPrefs.SetString(CHECKSUM_KEY + key, md5(key, value));
    //}

    //public static float GetFloatSafe(string key, float defaultValue)
    //{
    //    float value = PlayerPrefs.GetFloat(key);

    //    if (PlayerPrefs.GetString(CHECKSUM_KEY + key).CompareTo(md5(key, value)) != 0)
    //    {
    //        //Hacked 
    //        return 0;
    //    }
    //    else
    //    {
    //        return value;
    //    }

    //}


    public static string md5(string key, int value)
    {
        string str = key + PRIVATE_KEY + value;
        System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
        byte[] bytes = encoding.GetBytes(str);
        var sha = new System.Security.Cryptography.MD5CryptoServiceProvider();
        return System.BitConverter.ToString(sha.ComputeHash(bytes));
    }

    public static string md5(string key, float value)
    {
        string str = key + PRIVATE_KEY + value;
        System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
        byte[] bytes = encoding.GetBytes(str);
        var sha = new System.Security.Cryptography.MD5CryptoServiceProvider();
        return System.BitConverter.ToString(sha.ComputeHash(bytes));
    }

}
