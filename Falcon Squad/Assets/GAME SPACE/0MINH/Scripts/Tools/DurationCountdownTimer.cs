﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DurationCountdownTimer : MonoBehaviour
{

    //Time bắt đầu countdown:
    [SerializeField]
    private int year;

    [SerializeField]
    private int month;

    [SerializeField]
    private int day;

    [SerializeField]
    private int hour;

    [SerializeField]
    private int minute;

    [SerializeField]
    private int second;

    //Khoảng thời gian countdown (tính bằng giờ)
    [SerializeField]
    private float duration;

    private bool isCountdown;
    public bool IsCountdown { get { return isCountdown; } }

    private TimeSpan countdownTime;
    public TimeSpan CountdownTime { get { return countdownTime; } }

    private void OnEnable()
    {
        coundownCoroutine = Countdown();
        StartCoroutine(coundownCoroutine);
    }

    private void OnDisable()
    {
        StopCoroutine(coundownCoroutine);
    }

    IEnumerator coundownCoroutine;
    IEnumerator Countdown()
    {
        DateTime startDate = new DateTime(year, month, day, hour, minute, second);
        DateTime endDate = startDate.AddHours(duration);

        while (true)
        {
            if (DateTime.Now >= startDate && DateTime.Now < endDate)
            {
                isCountdown = true;
                countdownTime = endDate - DateTime.Now;
            }
            else
            {
                isCountdown = false;
            }

            yield return new WaitForSeconds(1f);
        }

    }
}
