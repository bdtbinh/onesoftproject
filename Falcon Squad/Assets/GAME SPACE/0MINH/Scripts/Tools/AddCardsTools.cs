﻿using UnityEngine;
using Sirenix.OdinInspector;

public class AddCardsTools : MonoBehaviour
{

    public AircraftTypeEnum aircraftType;
    public int specificAircraftCards;
    public int generalAircraftCards;

    public WingmanTypeEnum wingmanType;
    public int specificWingmanCards;
    public int generalWingmanCards;

    public WingTypeEnum wingType;
    public int specificWingCards;
    public int generalWingCards;

    [Button("Add Aircraft Cards")]
    public void AddAircraftCards()
    {
        SetSpaceShipCards(aircraftType, specificAircraftCards, "test", "test", "test");
        SetSpaceShipGeneralCards(generalAircraftCards, "test", "test");
    }

    [Button("Add Wingman Cards")]
    public void AddWingmanCards()
    {
        SetWingmanCards(wingmanType, specificWingmanCards, "test", "test", "test");
        SetWingmanGeneralCards(generalWingmanCards, "test", "test");
    }

    [Button("Add Wing Cards")]
    public void AddWingCards()
    {
        SetWingCards(wingType, specificWingCards, "test", "test", "test");
    }

    void SetSpaceShipCards(AircraftTypeEnum type, int amount, string why, string where, string typeCard)
    {
        AntiCheat.SetInt("sothecuamoi" + type + "conphithuyenspace", amount);
    }

    void SetSpaceShipGeneralCards(int amount, string why, string where)
    {
        AntiCheat.SetInt("tongthechungcuaphithuyen", amount);
    }

    void SetWingmanCards(WingmanTypeEnum type, int amount, string why, string where, string typeCard)
    {
        AntiCheat.SetInt("sothecuamoimaybayphu" + type + "conwingmanphithuyen", amount);
    }

    void SetWingmanGeneralCards(int amount, string why, string where)
    {
        AntiCheat.SetInt("tongthechungcuamaybayphu", amount);
    }

    void SetWingCards(WingTypeEnum type, int amount, string why, string where, string typeCard)
    {
        AntiCheat.SetInt("WingCard_" + type, amount);

    }

    void SetWingGeneralCards(int amount, string why, string where)
    {
        AntiCheat.SetInt("WingGeneralCards", amount);
    }
}
