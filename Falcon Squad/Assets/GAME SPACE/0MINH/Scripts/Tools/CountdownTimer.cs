﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour
{
	[SerializeField]
	private UILabel countdownLabel;

	[SerializeField]
	private int year;

	[SerializeField]
	private int month;

	[SerializeField]
	private int day;

	[SerializeField]
	private int hour;

	[SerializeField]
	private int minute;

	[SerializeField]
	private int second;

	private void OnEnable ()
	{
		StartCoroutine (Countdown ());
	}

	private void OnDisable ()
	{
		StopAllCoroutines ();
	}

	IEnumerator Countdown ()
	{
		DateTime endDate = new DateTime (year, month, day, hour, minute, second);

		while (endDate > DateTime.Now) {
			TimeSpan interval = endDate - DateTime.Now;
			countdownLabel.text = string.Format ("Ends in: {0:00} days {1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds);
			yield return new WaitForSeconds (1f);
		}

		OnExpired ();
	}

	public Action OnExpired;
}

