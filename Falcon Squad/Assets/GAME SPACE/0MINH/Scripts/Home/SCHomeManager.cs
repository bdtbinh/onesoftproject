﻿using com.ootii.Messages;
using EasyMobile;
using Mp.Pvp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCHomeManager : MonoBehaviour
{

    void Start()
    {
        MessageDispatcher.AddListener(EventName.RoyaltyPack.GetRoyaltyPackInfo.ToString(), OnGetRoyaltyPackInfo, true);
        new CSGetRoyaltyPackInfo(CSGetRoyaltyPackInfo.HOME_SCREEN).Send();
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventName.RoyaltyPack.GetRoyaltyPackInfo.ToString(), OnGetRoyaltyPackInfo, true);
        StopAllCoroutines();
    }

    private void OnGetRoyaltyPackInfo(IMessage rMessage)
    {
        SCGetRoyaltyPackInfo GRPI = rMessage.Data as SCGetRoyaltyPackInfo;

        //Debug.LogError("Home: " + GRPI.type);

        if (GRPI.type != CSGetRoyaltyPackInfo.HOME_SCREEN)
            return;

        #region Daily Gem
        //if (GRPI.status == OSNet.SCMessage.SUCCESS)
        //{
        //    RoyaltyPackData data = GRPI.data;
        //    if (data != null)
        //    {
        //        if (data.status == RoyaltyPackData.ACTIVE)
        //        {
        //            GameContext.isRoyalPack = true;

        //            Debug.LogError("OnGetRoyaltyPackInfo GemDaily: " + data.gemDaily);
        //            if (!data.gemDaily)
        //            {
        //                PanelCoinGem.Instance.AddGemTop(70);
        //                StartCoroutine(WaitToToastRoyaltyDailyGem());
        //            }
        //        }
        //        else
        //        {
        //            GameContext.isRoyalPack = false;
        //        }
        //    }
        //    else
        //    {
        //        GameContext.isRoyalPack = false;
        //        Debug.LogError("OnGetRoyaltyPackInfo Data Null");
        //    }
        //}
        //else
        //{
        //    PopupManagerCuong.Instance.ShowTextNotifiToast(GRPI.message, false, 1.5f);
        //} 
        #endregion

        if (GRPI.status == OSNet.SCMessage.SUCCESS)
        {
            RoyaltyPackData data = GRPI.data;
            if (data != null)
            {
                if (data.status == RoyaltyPackData.ACTIVE)
                {
                    MinhCacheGame.SetAlreadyBuyRoyalPack(true);
                }
                else
                {
                    MinhCacheGame.SetAlreadyBuyRoyalPack(false);
                }
            }
            else
            {
                MinhCacheGame.SetAlreadyBuyRoyalPack(false);
            }
        }
    }

    //IEnumerator WaitToToastRoyaltyDailyGem()
    //{
    //    yield return new WaitForSeconds(2f);
    //    PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.Notifi_HaveReceived_ChangeKey.Replace("%{reward}", " +70 " + I2.Loc.ScriptLocalization.gem), true, 1.8f);
    //}

}
