﻿using System.Collections;
using System.Collections.Generic;
using TCore;
using UnityEngine;
using UnityEngine.SceneManagement;
using com.ootii.Messages;
using Firebase.Analytics;

public class SelectDeviceHandler : MonoBehaviour
{
    public enum OpenSelectFromType
    {
        FromHome,
        FromSelectLevel,
        FromTournament
    }

    [SerializeField]
    private OpenSelectFromType openSelectFromType = OpenSelectFromType.FromHome;            //Kiểu mở popup select item từ Home hay từ Endless/SelectLevel

    private const string SPRITE_LOCK = "home_locked_slot";
    private const string SPRITE_WINGMAN_NOT_CHOSEN = "home_Dron_empty_slot";
    private const string SPRITE_WEAPON_NOT_CHOSEN = "home_SWeapon_empty_slot";
    private const string SPRITE_WING_NOT_CHOSEN = "home_Wing_empty_slot";
    private const string SPRITE_WING_CHOOSEN_BG = "hangar_bullet_0";

    [SerializeField]
    private AircraftAnimations aircraftAnimations;

    [SerializeField]
    private UISprite rightWingmanIcon;

    [SerializeField]
    private UISprite leftWingmanIcon;

    [SerializeField]
    private WingAnimations wingAnimations;

    [SerializeField]
    private UISprite btnWingBG;     //Background của button wing

    [SerializeField]
    private GameObject sNotify;     //Notify nếu left wingman chưa được chọn lần nào

    [SerializeField]
    private GameObject aircraftArrow;

    [SerializeField]
    private GameObject leftWingmanArrow;

    [SerializeField]
    private GameObject rightWingmanArrow;

    [SerializeField]
    private GameObject wingArrow;

    [SerializeField]
    private GameObject extraLevelUI;

    [SerializeField]
    private UILabel lHaveToUseExtra;

    [SerializeField]
    private GameObject eventUI;

    [SerializeField]
    private UILabel lHaveToUseEvent;

    private void Start()
    {
        //sNotify.SetActive(false);
        MessageDispatcher.AddListener(EventID.SELECT_DEVICE_CHANGE_UI, OnChangeUI, true);
        LoadUI();
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(EventID.SELECT_DEVICE_CHANGE_UI, OnChangeUI, true);
    }

    void OnChangeUI(IMessage msg)
    {
        LoadUI();
    }

    public void OnClickBtnAircraft()
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra)
        {
            //int aircraftIndex = ExtraLevelRequirementSheet.Get(CacheGame.GetCurrentLevel()).aircraft_index;
            //string str = I2.Loc.ScriptLocalization.msg_extra_level_have_to_use.Replace("%{aircraft_name}", "[FFFF00FF]" + HangarValue.AircraftNames((AircraftTypeEnum)aircraftIndex) + "[-]");
            PopupManagerCuong.Instance.ShowTextNotifiToast(lHaveToUseExtra.text, false, 1.8f);
            return;
        }

        if (GameContext.readyClickShowPopup)
        {
            SoundManager.PlayShowPopup();
            PopupManagerCuong.Instance.HideSelectLevelPopup();
            PopupManagerCuong.Instance.ShowSelectAircraftPopup(openSelectFromType, false);
            FirebaseLogSpaceWar.LogClickButton("Aircraft");
        }

    }

    public void OnClickBtnLeftWingman()
    {
        if (GameContext.readyClickShowPopup)
        {
            MinhCacheGame.SetAlreadyNotifyLeftWingman();
            sNotify.SetActive(false);
            if (CacheGame.GetMaxLevel3Difficult() <= Constant.UNLOCK_LEFT_WINGMAN_LEVEL)
            {
                SoundManager.PlayClickButton();
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", Constant.UNLOCK_LEFT_WINGMAN_LEVEL + ""), false, 1.5f);
                return;
            }

            SoundManager.PlayShowPopup();
            PopupManagerCuong.Instance.HideSelectLevelPopup();
            PopupManagerCuong.Instance.ShowSelectWingmanPopup(WingmanPosType.LeftWingman, openSelectFromType, false);
            FirebaseLogSpaceWar.LogClickButton("LeftWingman");
        }
    }

    public void OnClickBtnRightWingman()
    {
        if (GameContext.readyClickShowPopup)
        {
            if (CacheGame.GetMaxLevel3Difficult() <= Constant.UNLOCK_RIGHT_WINGMAN_LEVEL)
            {
                SoundManager.PlayClickButton();
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", Constant.UNLOCK_RIGHT_WINGMAN_LEVEL + ""), false, 1.5f);
                return;
            }

            SoundManager.PlayShowPopup();
            PopupManagerCuong.Instance.HideSelectLevelPopup();
            PopupManagerCuong.Instance.ShowSelectWingmanPopup(WingmanPosType.RightWingman, openSelectFromType, false);
            FirebaseLogSpaceWar.LogClickButton("RightWingman");
        }
    }

    public void OnClickBtnWing()
    {
        if(GameContext.modeGamePlay == GameContext.ModeGamePlay.Events && eventUI != null)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(lHaveToUseEvent.text, false, 1.8f);
            return;
        }

        if (GameContext.readyClickShowPopup)
        {
            if (CacheGame.GetMaxLevel3Difficult() <= Constant.UNLOCK_WING_LEVEL)
            {
                SoundManager.PlayClickButton();
                PopupManagerCuong.Instance.ShowTextNotifiToast(I2.Loc.ScriptLocalization.msg_unlock_by_level.Replace("%{level_number}", Constant.UNLOCK_WING_LEVEL + ""), false, 1.5f);
                return;
            }

            SoundManager.PlayShowPopup();
            PopupManagerCuong.Instance.HideSelectLevelPopup();
            PopupManagerCuong.Instance.ShowSelectWingPopup(openSelectFromType, false);
            FirebaseLogSpaceWar.LogClickButton("Wing");
        }
    }

    void LoadUI()
    {
        SetAircraftAnimation();
        SetLeftWingmanIcon();
        SetRightWingmanIcon();
        SetWingIcon();

        CheckToShowAircraftUpgradeArrow();
        CheckToShowLeftWingmanUpgradeArrow();
        CheckToShowRightWingmanUpgradeArrow();
        CheckToShowWingUpgradeArrow();
        CheckToShowExtraLevelUI();
        CheckToShowEventUI();
    }

    private void SetAircraftAnimation()
    {
        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra)
        {
            AircraftTypeEnum selectedType = (AircraftTypeEnum)CacheGame.GetSpaceShipUserSelectedExtraLevel();
            aircraftAnimations.PlayAnimations(selectedType, CacheGame.GetSpaceShipRank(selectedType));
        }
        else
        {
            AircraftTypeEnum selectedType = (AircraftTypeEnum)CacheGame.GetSpaceShipUserSelected();
            aircraftAnimations.PlayAnimations(selectedType, CacheGame.GetSpaceShipRank(selectedType));
        }
    }

    private void SetLeftWingmanIcon()
    {
        if (CacheGame.GetWingManLeftUserSelected() == 0 && !MinhCacheGame.IsAlreadyNotifyLeftWingman())
        {
            sNotify.SetActive(true);
        }

        if (CacheGame.GetMaxLevel3Difficult() < Constant.UNLOCK_LEFT_WINGMAN_LEVEL)
        {
            leftWingmanIcon.spriteName = SPRITE_LOCK;
        }
        else if (CacheGame.GetWingManLeftUserSelected() == 0)
        {
            leftWingmanIcon.spriteName = SPRITE_WINGMAN_NOT_CHOSEN;
        }
        else
        {
            leftWingmanIcon.spriteName = HangarValue.WingmanIconWithBorderSpriteName[(WingmanTypeEnum)CacheGame.GetWingManLeftUserSelected()];
        }
    }

    private void SetRightWingmanIcon()
    {
        if (CacheGame.GetMaxLevel3Difficult() <= Constant.UNLOCK_RIGHT_WINGMAN_LEVEL)
        {
            rightWingmanIcon.spriteName = SPRITE_LOCK;
        }
        else if (CacheGame.GetWingManRightUserSelected() == 0)
        {
            rightWingmanIcon.spriteName = SPRITE_WINGMAN_NOT_CHOSEN;
        }
        else
        {
            rightWingmanIcon.spriteName = HangarValue.WingmanIconWithBorderSpriteName[(WingmanTypeEnum)CacheGame.GetWingManRightUserSelected()];
        }
    }

    private void SetWingIcon()
    {
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events && eventUI != null)
        {
            btnWingBG.spriteName = SPRITE_WING_CHOOSEN_BG;
            wingAnimations.gameObject.SetActive(true);
            wingAnimations.PlayAnimations(WingTypeEnum.WingOfResolution, Rank.S);
            return;
        }

        if (CacheGame.GetMaxLevel3Difficult() <= Constant.UNLOCK_WING_LEVEL)
        {
            btnWingBG.spriteName = SPRITE_LOCK;
            wingAnimations.gameObject.SetActive(false);
        }
        else if (CacheGame.GetWingUserSelected() == 0)
        {
            btnWingBG.spriteName = SPRITE_WING_NOT_CHOSEN;
            wingAnimations.gameObject.SetActive(false);
        }
        else
        {
            btnWingBG.spriteName = SPRITE_WING_CHOOSEN_BG;
            wingAnimations.gameObject.SetActive(true);
            WingTypeEnum wingType = (WingTypeEnum)CacheGame.GetWingUserSelected();
            wingAnimations.PlayAnimations(wingType, CacheGame.GetWingRank(wingType));
        }
    }

    private void CheckToShowExtraLevelUI()
    {
        if (extraLevelUI == null)
            return;

        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra && GameContext.modeGamePlay == GameContext.ModeGamePlay.Campaign)
        {
            extraLevelUI.SetActive(true);

            int aircraftIndex = ExtraLevelRequirementSheet.Get(CacheGame.GetCurrentLevel()).aircraft_index;
            lHaveToUseExtra.text = I2.Loc.ScriptLocalization.msg_extra_level_have_to_use.Replace("%{aircraft_name}", "[FFFF00FF]" + HangarValue.AircraftNames((AircraftTypeEnum)aircraftIndex) + "[-]");
        }
        else
        {
            extraLevelUI.SetActive(false);
        }
    }

    private void CheckToShowEventUI()
    {
        if (eventUI == null)
            return;

        Debug.LogError(GameContext.modeGamePlay);
        if (GameContext.modeGamePlay == GameContext.ModeGamePlay.Events)
        {
            eventUI.SetActive(true);
            lHaveToUseEvent.text = I2.Loc.ScriptLocalization.msg_extra_level_have_to_use.Replace("%{aircraft_name}", "[FFFF00FF]" + HangarValue.WingNames(WingTypeEnum.WingOfResolution) + "[-]");
        }
        else
        {
            eventUI.SetActive(false);
        }
    }

    private void CheckToShowWingUpgradeArrow()
    {
        wingArrow.SetActive(false);
        WingTypeEnum selectedType = (WingTypeEnum)CacheGame.GetWingUserSelected();
        if (selectedType <= 0) return;

        Rank rank = CacheGame.GetWingRank(selectedType);

        //Check xem có đủ điều kiện để evolve không
        if ((int)rank < (int)Constant.MAX_RANK_APPLY)
        {
            int evolveCost = RankSheet.Get((int)rank + 1).gem_evolve;
            int evolveCards = RankSheet.Get((int)rank + 1).card_evolve;
            int specificCards = MinhCacheGame.GetWingCards(selectedType);
            int generalCards = MinhCacheGame.GetWingGeneralCards();

            if (CacheGame.GetTotalGem() >= evolveCost && specificCards + generalCards >= evolveCards)
            {
                wingArrow.SetActive(true);
                return;
            }
        }

        //Check xem có đủ điều kiện để nâng cấp không
        int currentLevel = CacheGame.GetWingLevel(selectedType);
        int maxLevel = RankSheet.Get((int)rank).max_level;

        if (currentLevel >= maxLevel) return;

        int upgradeGoldCost = WingUpgradeCostSheet.Get(currentLevel + 1).gold;
        int upgradeGemCost = WingUpgradeCostSheet.Get(currentLevel + 1).gem;
        int upgradeEnergyCost = WingUpgradeCostSheet.Get(currentLevel + 1).energy;

        if (upgradeGoldCost > 0)
        {
            if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
            {
                wingArrow.SetActive(true);
            }
        }
        else if (upgradeGemCost > 0)
        {
            if (CacheGame.GetTotalGem() >= upgradeGemCost)
            {
                wingArrow.SetActive(true);
            }
        }
        else if (upgradeEnergyCost > 0)
        {
            if (CacheGame.GetTotalEnergy() >= upgradeEnergyCost)
            {
                wingArrow.SetActive(true);
            }
        }
    }

    private void CheckToShowAircraftUpgradeArrow()
    {
        aircraftArrow.SetActive(false);

        if (GameContext.levelCampaignMode == GameContext.LevelCampaignMode.Extra)
        {
            return;
        }

        AircraftTypeEnum selectedType = (AircraftTypeEnum)CacheGame.GetSpaceShipUserSelected();
        Rank rank = CacheGame.GetSpaceShipRank(selectedType);

        //Check xem có đủ điều kiện để evolve không
        if ((int)rank < (int)Constant.MAX_RANK_APPLY)
        {
            int evolveCost = RankSheet.Get((int)rank + 1).gem_evolve;
            int evolveCards = RankSheet.Get((int)rank + 1).card_evolve;
            int specificCards = MinhCacheGame.GetSpaceShipCards(selectedType);
            int generalCards = MinhCacheGame.GetSpaceShipGeneralCards();

            if (CacheGame.GetTotalGem() >= evolveCost && specificCards + generalCards >= evolveCards)
            {
                aircraftArrow.SetActive(true);
                return;
            }
        }

        //Check xem có đủ điều kiện để nâng cấp không
        int currentLevel = CacheGame.GetSpaceShipLevel(selectedType);
        int maxLevel = RankSheet.Get((int)rank).max_level;

        if (currentLevel >= maxLevel) return;

        int upgradeGoldCost = AirCUpgradeCostSheet.Get(currentLevel + 1).gold;
        int upgradeGemCost = AirCUpgradeCostSheet.Get(currentLevel + 1).gem;

        if (upgradeGoldCost > 0)
        {
            if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
            {
                aircraftArrow.SetActive(true);
            }
        }
        else
        {
            if (CacheGame.GetTotalGem() >= upgradeGemCost)
            {
                aircraftArrow.SetActive(true);
            }
        }
    }

    private void CheckToShowLeftWingmanUpgradeArrow()
    {
        leftWingmanArrow.SetActive(false);

        WingmanTypeEnum selectedType = (WingmanTypeEnum)CacheGame.GetWingManLeftUserSelected();

        if (selectedType <= 0) return;

        Rank rank = CacheGame.GetWingmanRank(selectedType);

        //Check xem có đủ điều kiện để evolve không
        if ((int)rank < (int)Constant.MAX_RANK_APPLY)
        {
            int evolveCost = RankSheet.Get((int)rank + 1).gem_evolve;
            int evolveCards = RankSheet.Get((int)rank + 1).card_evolve;
            int specificCards = MinhCacheGame.GetWingmanCards(selectedType);
            int generalCards = MinhCacheGame.GetWingmanGeneralCards();

            if (CacheGame.GetTotalGem() >= evolveCost && specificCards + generalCards >= evolveCards)
            {
                leftWingmanArrow.SetActive(true);
                return;
            }
        }

        //Check xem có đủ điều kiện để nâng cấp không
        int currentLevel = CacheGame.GetWingmanLevel(selectedType);
        int maxLevel = RankSheet.Get((int)rank).max_level;

        if (currentLevel >= maxLevel) return;

        int upgradeGoldCost = WingMUpgradeCostSheet.Get(currentLevel + 1).gold;
        int upgradeGemCost = WingMUpgradeCostSheet.Get(currentLevel + 1).gem;

        if (upgradeGoldCost > 0)
        {
            if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
            {
                leftWingmanArrow.SetActive(true);
            }
        }
        else
        {
            if (CacheGame.GetTotalGem() >= upgradeGemCost)
            {
                leftWingmanArrow.SetActive(true);
            }
        }
    }

    private void CheckToShowRightWingmanUpgradeArrow()
    {
        rightWingmanArrow.SetActive(false);

        WingmanTypeEnum selectedType = (WingmanTypeEnum)CacheGame.GetWingManRightUserSelected();

        if (selectedType <= 0) return;

        Rank rank = CacheGame.GetWingmanRank(selectedType);

        //Check xem có đủ điều kiện để evolve không
        if ((int)rank < (int)Constant.MAX_RANK_APPLY)
        {
            int evolveCost = RankSheet.Get((int)rank + 1).gem_evolve;
            int evolveCards = RankSheet.Get((int)rank + 1).card_evolve;
            int specificCards = MinhCacheGame.GetWingmanCards(selectedType);
            int generalCards = MinhCacheGame.GetWingmanGeneralCards();

            if (CacheGame.GetTotalGem() >= evolveCost && specificCards + generalCards >= evolveCards)
            {
                rightWingmanArrow.SetActive(true);
                return;
            }
        }

        //Check xem có đủ điều kiện để nâng cấp không
        int currentLevel = CacheGame.GetWingmanLevel(selectedType);
        int maxLevel = RankSheet.Get((int)rank).max_level;

        if (currentLevel >= maxLevel) return;

        int upgradeGoldCost = WingMUpgradeCostSheet.Get(currentLevel + 1).gold;
        int upgradeGemCost = WingMUpgradeCostSheet.Get(currentLevel + 1).gem;

        if (upgradeGoldCost > 0)
        {
            if (CacheGame.GetTotalCoin() >= upgradeGoldCost)
            {
                rightWingmanArrow.SetActive(true);
            }
        }
        else
        {
            if (CacheGame.GetTotalGem() >= upgradeGemCost)
            {
                rightWingmanArrow.SetActive(true);
            }
        }
    }

    public void ShowPanel()
    {
        LoadUI();
        gameObject.SetActive(true);
    }
}
