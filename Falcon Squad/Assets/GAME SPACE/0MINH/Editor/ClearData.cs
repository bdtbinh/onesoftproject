﻿using UnityEditor;
using UnityEngine;
using SkyGameKit.Multiplayer;
using OSNet;

public class ClearData : MonoBehaviour
{

    [MenuItem("Tools/Cheat/Clear PlayerPrefs Data")]
    static public void ClearPlayerPrefsData()
    {
        PlayerPrefs.DeleteAll();
    }
    [MenuItem("Tools/Cheat/Get 2triệu Gold + 100.000 Gem + 100.000 Energy")]
    static public void Get2mGold()
    {
        //PlayerPrefs.DeleteAll();
        CacheGame.SetTotalCoin(2000000, "test", "test");
        CacheGame.SetTotalGem(100000, "test", "test");
        CacheGame.SetTotalEnergy(100000, "test", "test");
    }

    [MenuItem("Tools/Cheat/Add 50.000 Gold + 2.000 Gem")]
    static public void Get500Gold()
    {
        //PlayerPrefs.DeleteAll();
        CacheGame.SetTotalCoin(50000, "test", "test");
        CacheGame.SetTotalGem(2000, "test", "test");
    }

    [MenuItem("Tools/Cheat/Add 900 gem")]
    static public void Add900Gem()
    {
        //PlayerPrefs.DeleteAll();
        CacheGame.SetTotalGem(CacheGame.GetTotalGem() + 900, "test", "test");
    }

    [MenuItem("Tools/Cheat/Unlock All Level")]
    static public void SetMaxLevel()
    {
        CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, GameContext.TOTAL_LEVEL);
        for (int i = 1; i <= GameContext.TOTAL_LEVEL; i++)
        {
            for (int j = 0; j < Random.Range(4, 4); j++)
            {
                CacheGame.SetNumStarLevel(i, j + 1, 1, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal);

                if (i % 7 == 0 || i % 7 == 3)
                    CacheGame.SetNumStarLevel(i, j + 1, 1, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Extra);
            }
        }
    }

    [MenuItem("Tools/Cheat/Unlock  Level 36")]
    static public void SetMaxLevel36()
    {
        CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, 36);
        for (int i = 1; i <= 36; i++)
        {
            CacheGame.SetNumStarLevel(i, 1, 1, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal);
        }
        PvpUtil.SendUpdatePlayer();
    }

    [MenuItem("Tools/Cheat/Unlock  Level 21")]
    static public void SetMaxLevel21()
    {
        CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, 21);
        for (int i = 1; i <= 21; i++)
        {
            CacheGame.SetNumStarLevel(i, 1, 1, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal);
        }
        PvpUtil.SendUpdatePlayer();
    }

    [MenuItem("Tools/Cheat/Unlock  Level 14")]
    static public void SetMaxLevel14()
    {
        CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, 14);
        for (int i = 1; i <= 14; i++)
        {
            CacheGame.SetNumStarLevel(i, 1, 1, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal);
        }
        PvpUtil.SendUpdatePlayer();
    }

    [MenuItem("Tools/Cheat/Unlock  Level 7")]
    static public void SetMaxLevel7()
    {
        CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, 7);
        for (int i = 1; i <= 7; i++)
        {
            CacheGame.SetNumStarLevel(i, 1, 1, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal);
        }
        PvpUtil.SendUpdatePlayer();
    }

    [MenuItem("Tools/Cheat/Unlock  Level 3")]
    static public void SetMaxLevel3()
    {
        CacheGame.SetMaxLevel(GameContext.DIFFICULT_NOMAL, 3);
        for (int i = 1; i <= 3; i++)
        {
            CacheGame.SetNumStarLevel(i, 1, 1, GameContext.DIFFICULT_NOMAL, GameContext.LevelCampaignMode.Normal);
        }
        PvpUtil.SendUpdatePlayer();
    }

    [MenuItem("Tools/Cheat/Get 500 card")]
    static public void Get500Card()
    {
        for (int i = 0; i < 11; i++)
        {
            MinhCacheGame.SetSpaceShipCards((AircraftTypeEnum)i, MinhCacheGame.GetSpaceShipCards((AircraftTypeEnum)i) + 500, "test", "test", "test");
        }
        MinhCacheGame.SetSpaceShipGeneralCards(MinhCacheGame.GetSpaceShipGeneralCards() + 100, "test", "test");
    }

    [MenuItem("Tools/Cheat/Set SpaceShip Max Level")]
    static public void SetSpaceShipMaxLevel()
    {
        for (int i = 0; i < GameContext.TOTAL_AIRCRAFT + 1; i++)
        {
            CacheGame.SetSpaceShipIsUnlocked(i, 1);
            CacheGame.SetSpaceShipRank((AircraftTypeEnum)i, Rank.SSS);
            CacheGame.SetSpaceShipLevel((AircraftTypeEnum)i, 179);
        }
    }
    [MenuItem("Tools/Cheat/Set Drones Max Level")]
    static public void SetDronesMaxLevel()
    {
        for (int i = 0; i < GameContext.TOTAL_WINGMAN + 1; i++)
        {
            CacheGame.SetWingManIsUnlocked(i, 1);
            CacheGame.SetWingmanRank((WingmanTypeEnum)i, Rank.SSS);
            CacheGame.SetWingmanLevel((WingmanTypeEnum)i, 179);
        }
    }

    [MenuItem("Tools/Cheat/Set Wing Max Level")]
    static public void SetWingMaxLevel()
    {
        for (int i = 0; i < GameContext.TOTAL_WING + 1; i++)
        {
            CacheGame.SetWingIsUnlocked(i, 1);
            CacheGame.SetWingRank((WingTypeEnum)i, Rank.SSS);
            CacheGame.SetWingLevel((WingTypeEnum)i, 179);
        }
    }

    [MenuItem("Tools/Cheat/Destroy static")]
    static public void DestroyStatic()
    {
        API.Destroy();
        NetManager.Destroy();
        CachePvp.Destroy();
    }


    [MenuItem("Tools/Cheat/Unlock ThirtyDaysLogin")]
    static public void UnlockThirtyDaysLogin()
    {
        CacheGame.SetClaimedAllGiftDailyLogin(1);
    }

    [MenuItem("Tools/Cheat/Add 1 Vip Point")]
    static public void Add1VipPoint()
    {
        CachePvp.myVipPoint += 1;
    }

    [MenuItem("Tools/Cheat/Add 10 Vip Point")]
    static public void Add10VipPoint()
    {
        CachePvp.myVipPoint += 10;
    }
}
