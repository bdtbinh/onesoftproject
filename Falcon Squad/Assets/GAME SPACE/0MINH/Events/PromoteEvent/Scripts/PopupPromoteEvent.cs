﻿using System.Collections;
using UnityEngine;
using I2.Loc;
using System;
using TCore;

public class PopupPromoteEvent : MonoBehaviour
{

    [SerializeField]
    private UILabel lTitleS;

    [SerializeField]
    private UILabel lTitleSS;

    [SerializeField]
    private UILabel lTitleSSS;

    [SerializeField]
    private UILabel[] lClaims;

    [SerializeField]
    private UISprite[] sBGButtons;

    [SerializeField]
    private GameObject[] sGlows;

    [SerializeField]
    private UILabel lCountdown;

    [SerializeField]
    private UILabel lUnlockEvolve;

    private void OnEnable()
    {
        LoadUI();
        StartCoroutine(CountDownEndTime());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    IEnumerator CountDownEndTime()
    {
        DateTime expiredTime = DateTime.Now;
        DateTime.TryParse(CacheFireBase.GetDayFinishPromoteEvent, out expiredTime);

        while (DateTime.Now < expiredTime)
        {
            TimeSpan interval = expiredTime - DateTime.Now;

            string time = "";

            if (interval.Days > 0)
                time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
            //time = "[FFFF00FF]" + string.Format("{0:00} days:{1:00}:{2:00}:{3:00}", interval.Days, interval.Hours, interval.Minutes, interval.Seconds) + "[-]";
            else
                time = "[FFFF00FF]" + string.Format("{0:00}:{1:00}:{2:00}", interval.Hours, interval.Minutes, interval.Seconds) + "[-]";

            lCountdown.text = ScriptLocalization.end_in.Replace("%{time}", time);
            yield return new WaitForSeconds(1f);
        }

        gameObject.SetActive(false);
        //Debug.LogError("aaaa2: " + CachePvp.dateTime + " * " + expiredTime + " * " + DateTime.Now);
    }

    void LoadUI()
    {
        lTitleS.text = ScriptLocalization.title_rank_rewards.Replace("%{rank}", ScriptLocalization.rank_s);
        lTitleSS.text = ScriptLocalization.title_rank_rewards.Replace("%{rank}", ScriptLocalization.rank_ss);
        lTitleSSS.text = ScriptLocalization.title_rank_rewards.Replace("%{rank}", ScriptLocalization.rank_sss);

        lUnlockEvolve.text = ScriptLocalization.msg_unlock_evolve_claim_reward.Replace("%{name}", HangarValue.WingmanNames(WingmanTypeEnum.Splasher));

        for (int i = 0; i < lClaims.Length; i++)
        {
            if (MinhCacheGame.IsPromoteEventClaimed(i + 1, EventName.PROMOTE_EVENT_NAME))
            {
                lClaims[i].text = ScriptLocalization.claimed;
                sBGButtons[i].spriteName = SpriteNameConst.BG_BUTTON_CLAIM_DISABLE_1;
            }
            else
            {
                lClaims[i].text = ScriptLocalization.claim;
                sBGButtons[i].spriteName = SpriteNameConst.BG_BUTTON_CLAIM_ENABLE_1;
            }
        }

        if (!MinhCacheGame.IsPromoteEventClaimed(1, EventName.PROMOTE_EVENT_NAME) && CacheGame.GetWingManIsUnlocked((int)WingmanTypeEnum.Splasher) == 1)
        {
            sGlows[0].SetActive(true);
        }
        else
        {
            sGlows[0].SetActive(false);
        }

        if (!MinhCacheGame.IsPromoteEventClaimed(2, EventName.PROMOTE_EVENT_NAME) && CacheGame.GetWingmanRank(WingmanTypeEnum.Splasher) >= Rank.S)
        {
            sGlows[1].SetActive(true);
        }
        else
        {
            sGlows[1].SetActive(false);
        }

        if (!MinhCacheGame.IsPromoteEventClaimed(3, EventName.PROMOTE_EVENT_NAME) && CacheGame.GetWingmanRank(WingmanTypeEnum.Splasher) >= Rank.SS)
        {
            sGlows[2].SetActive(true);
        }
        else
        {
            sGlows[2].SetActive(false);
        }

        if (!MinhCacheGame.IsPromoteEventClaimed(4, EventName.PROMOTE_EVENT_NAME) && CacheGame.GetWingmanRank(WingmanTypeEnum.Splasher) >= Rank.SSS)
        {
            sGlows[3].SetActive(true);
        }
        else
        {
            sGlows[3].SetActive(false);
        }
    }

    public void OnClickBtnUnlockClaim()
    {
        SoundManager.PlayClickButton();

        if (MinhCacheGame.IsPromoteEventClaimed(1, EventName.PROMOTE_EVENT_NAME))
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.notifi_claimed_reward, false, 1.8f);
            return;
        }

        if (CacheGame.GetWingManIsUnlocked((int)WingmanTypeEnum.Splasher) != 1)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_Achi_PleaseComplete, false, 1.8f);
            return;
        }

        MinhCacheGame.SetPromoteEventClaimed(1, EventName.PROMOTE_EVENT_NAME);
        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(2);

        GameContext.AddGiftToPlayer(ItemInfoStringType.Gem, 75, true, FirebaseLogSpaceWar.Event_why + "_" + EventName.PROMOTE_EVENT_NAME, "Unlock");
        popupItemRewardIns.ShowOneItem(1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

        GameContext.AddGiftToPlayer(ItemInfoStringType.Gold, 20000, true, FirebaseLogSpaceWar.Event_why + "_" + EventName.PROMOTE_EVENT_NAME, "Unlock");
        popupItemRewardIns.ShowOneItem(2, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

        lClaims[0].text = ScriptLocalization.claimed;
        sBGButtons[0].spriteName = SpriteNameConst.BG_BUTTON_CLAIM_DISABLE_1;
        sGlows[0].SetActive(false);
    }

    public void OnClickBtnEvolveSClaim()
    {
        SoundManager.PlayClickButton();

        if (MinhCacheGame.IsPromoteEventClaimed(2, EventName.PROMOTE_EVENT_NAME))
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.notifi_claimed_reward, false, 1.8f);
            return;
        }

        if (CacheGame.GetWingmanRank(WingmanTypeEnum.Splasher) < Rank.S)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_Achi_PleaseComplete, false, 1.8f);
            return;
        }

        MinhCacheGame.SetPromoteEventClaimed(2, EventName.PROMOTE_EVENT_NAME);
        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(2);

        GameContext.AddGiftToPlayer(ItemInfoStringType.Gem, 150, true, FirebaseLogSpaceWar.Event_why + "_" + EventName.PROMOTE_EVENT_NAME, "Evolve Rank S");
        popupItemRewardIns.ShowOneItem(1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

        GameContext.AddGiftToPlayer(ItemInfoStringType.Gold, 50000, true, FirebaseLogSpaceWar.Event_why + "_" + EventName.PROMOTE_EVENT_NAME, "Evolve Rank S");
        popupItemRewardIns.ShowOneItem(2, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

        lClaims[1].text = ScriptLocalization.claimed;
        sBGButtons[1].spriteName = SpriteNameConst.BG_BUTTON_CLAIM_DISABLE_1;
        sGlows[1].SetActive(false);
    }

    public void OnClickBtnEvolveSSClaim()
    {
        SoundManager.PlayClickButton();

        if (MinhCacheGame.IsPromoteEventClaimed(3, EventName.PROMOTE_EVENT_NAME))
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.notifi_claimed_reward, false, 1.8f);
            return;
        }

        if (CacheGame.GetWingmanRank(WingmanTypeEnum.Splasher) < Rank.SS)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_Achi_PleaseComplete, false, 1.8f);
            return;
        }

        MinhCacheGame.SetPromoteEventClaimed(3, EventName.PROMOTE_EVENT_NAME);
        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(3);

        GameContext.AddGiftToPlayer(ItemInfoStringType.Gem, 350, true, FirebaseLogSpaceWar.Event_why + "_" + EventName.PROMOTE_EVENT_NAME, "Evolve Rank SS");
        popupItemRewardIns.ShowOneItem(3, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

        GameContext.AddGiftToPlayer(ItemInfoStringType.Gold, 100000, true, FirebaseLogSpaceWar.Event_why + "_" + EventName.PROMOTE_EVENT_NAME, "Evolve Rank SS");
        popupItemRewardIns.ShowOneItem(1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

        GameContext.AddGiftToPlayer(ItemInfoStringType.DroneGeneralCard, 15, true, FirebaseLogSpaceWar.Event_why + "_" + EventName.PROMOTE_EVENT_NAME, "Evolve Rank SS");
        popupItemRewardIns.ShowOneItem(2, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

        lClaims[2].text = ScriptLocalization.claimed;
        sBGButtons[2].spriteName = SpriteNameConst.BG_BUTTON_CLAIM_DISABLE_1;
        sGlows[2].SetActive(false);
    }

    public void OnClickBtnEvolveSSSClaim()
    {
        SoundManager.PlayClickButton();

        if (MinhCacheGame.IsPromoteEventClaimed(4, EventName.PROMOTE_EVENT_NAME))
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.notifi_claimed_reward, false, 1.8f);
            return;
        }

        if (CacheGame.GetWingmanRank(WingmanTypeEnum.Splasher) < Rank.SSS)
        {
            PopupManagerCuong.Instance.ShowTextNotifiToast(ScriptLocalization.Notifi_Achi_PleaseComplete, false, 1.8f);
            return;
        }

        MinhCacheGame.SetPromoteEventClaimed(4, EventName.PROMOTE_EVENT_NAME);
        PopupManagerCuong.Instance.ShowItemRewardPopup();
        PopupItemReward popupItemRewardIns = PopupManagerCuong.Instance.popupsDictionary[PopupManagerCuong.POPUP_SHOW_ITEM_REWARD].GetComponent<PopupItemReward>();
        popupItemRewardIns.ShowListItem(3);

        GameContext.AddGiftToPlayer(ItemInfoStringType.Gem, 750, true, FirebaseLogSpaceWar.Event_why + "_" + EventName.PROMOTE_EVENT_NAME, "Evolve Rank SSS");
        popupItemRewardIns.ShowOneItem(3, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

        GameContext.AddGiftToPlayer(ItemInfoStringType.Gold, 200000, true, FirebaseLogSpaceWar.Event_why + "_" + EventName.PROMOTE_EVENT_NAME, "Evolve Rank SSS");
        popupItemRewardIns.ShowOneItem(1, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

        GameContext.AddGiftToPlayer(ItemInfoStringType.DroneGeneralCard, 30, true, FirebaseLogSpaceWar.Event_why + "_" + EventName.PROMOTE_EVENT_NAME, "Evolve Rank SSS");
        popupItemRewardIns.ShowOneItem(2, GameContext.typeItemPopupItemReward, GameContext.numItemPopupItemReward);

        lClaims[3].text = ScriptLocalization.claimed;
        sBGButtons[3].spriteName = SpriteNameConst.BG_BUTTON_CLAIM_DISABLE_1;
        sGlows[3].SetActive(false);
    }

    public void OnClickBtnBack()
    {
        SoundManager.PlayClickButton();
        PopupManagerCuong.Instance.HidePromoteEventPopup();
    }

    public void OnClickBtnPlay()
    {
        SoundManager.PlayClickButton();
        GameContext.modeGamePlay = GameContext.ModeGamePlay.Events;
        PopupManagerCuong.Instance.ShowEventPopup();
    }
}
