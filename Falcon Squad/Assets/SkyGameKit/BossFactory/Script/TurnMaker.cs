﻿#if UNITY_EDITOR
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace SkyGameKit.Demo {
    public class TurnMaker : MonoBehaviour {
        public string turnName;
        private Dictionary<GameObject, List<Vector2>> gunPosition = new Dictionary<GameObject, List<Vector2>>();
        [Button("Create Turn")]
        public void CreateTurn() {
            gunPosition.Clear();
            GameObject newTurn = new GameObject(turnName);
            BattleshipAsTurn newTurnComponent = newTurn.AddComponent<BattleshipAsTurn>();
            newTurnComponent.battleshipBody = new MiniTurn { enemyPrefab = PrefabUtility.GetCorrespondingObjectFromSource(gameObject) as GameObject };
            foreach (Transform child in transform) {
                GameObject gunPrefab = PrefabUtility.GetCorrespondingObjectFromSource(child.gameObject) as GameObject;
                if (gunPosition.ContainsKey(gunPrefab)) {
                    gunPosition[gunPrefab].Add(child.localPosition);
                } else {
                    gunPosition[gunPrefab] = new List<Vector2> { child.localPosition };
                }
            }
            newTurnComponent.listGunTurret = new MiniTurnEx2[gunPosition.Count];
            int i = 0;
            foreach (var item in gunPosition) {
                newTurnComponent.listGunTurret[i] = new MiniTurnEx2 {
                    enemyPrefab = item.Key,
                    startPosition = item.Value
                };
                i++;
            }
        }
    }
}
#endif
