﻿#if UNITY_EDITOR
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEditor;
using System;
using System.Collections.Generic;
using Object = UnityEngine.Object;
namespace SkyGameKit.Demo {
    public class GunMaker : MonoBehaviour {
        public string savePath = "Assets/BossSystem/";
        public string prefabName;
        public Sprite gunSprite;

        [TableList]
        public List<ShootTableColumn> shootList = new List<ShootTableColumn>();

        [Button("Create")]
        public void CreateGun() {
            string localPath = savePath + (savePath.EndsWith("/") ? "" : "/") + prefabName + ".prefab";
            GameObject newGo = Instantiate(gameObject);
            newGo.name = prefabName;
            newGo.GetComponent<SpriteRenderer>().sprite = gunSprite;
            GameObject newShotCtrl = new GameObject("ShotCtrl");
            newShotCtrl.transform.parent = newGo.transform;
            UbhShotCtrl newShotCtrlComponent = newShotCtrl.AddComponent<UbhShotCtrl>();
            foreach (var item in shootList) {
                GameObject itemGo = Instantiate(item.shotPatternPrefab, newShotCtrl.transform);
                UbhBaseShot itemBaseShot = itemGo.GetComponent<UbhBaseShot>();
                itemBaseShot.m_bulletPrefab = item.bulletPrefab;
                newShotCtrlComponent.m_shotList.Add(new UbhShotCtrl.ShotInfo {
                    m_afterDelay = item.delay,
                    m_shotObj = itemBaseShot
                });
            }
            DestroyImmediate(newGo.GetComponent<GunMaker>());
            if (AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject))) {
                if (EditorUtility.DisplayDialog("Are you sure?",
                        "The prefab already exists. Do you want to overwrite it?",
                        "Yes",
                        "No")) {
                    CreateNewGun(newGo, localPath);
                }
            } else {
                Debug.Log(prefabName + " created");
                CreateNewGun(newGo, localPath);
            }
        }

        private void CreateNewGun(GameObject obj, string localPath) {
            Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
            PrefabUtility.ReplacePrefab(obj, prefab, ReplacePrefabOptions.ConnectToPrefab);
        }
    }

    [Serializable]
    public class ShootTableColumn {
        public GameObject shotPatternPrefab;
        public float delay = 3;
        public GameObject bulletPrefab;
    }
}
#endif

