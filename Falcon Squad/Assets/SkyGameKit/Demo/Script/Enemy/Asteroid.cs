﻿using SWS;
using UnityEngine;
using PathologicalGames;

namespace SkyGameKit.Demo {
    public class Asteroid : BaseEnemy {
        public override void SetPath(PathManager path, int framePauseAfterSetPath = 0) {
            if (properties.Length > 0) {
                newPathTransform = PoolManager.Pools[Const.pathPool].Spawn(path.name, transform.position, FuCore.LockAt2D(Vector2.up, properties[0]));
                var bezier = newPathTransform.GetComponent<BezierPathManager>();
                if (bezier != null) bezier.CalculatePath();
                var newPathManager = newPathTransform.GetComponent<PathManager>();
                M_SplineMove.SetPath(newPathManager);
            } else {
                base.SetPath(path, framePauseAfterSetPath);
            }
        }
        public void DelayRandom(float time) {
            M_SplineMove.Pause(UnityEngine.Random.value * time);
        }
    }
}
