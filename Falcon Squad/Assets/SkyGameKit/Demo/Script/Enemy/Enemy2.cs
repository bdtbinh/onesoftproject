﻿using DG.Tweening;
using UnityEngine;
using UniRx;

namespace SkyGameKit.Demo {
    public class Enemy2 : BaseEnemy {
        private Vector2 dir = Vector2.zero;

        public void GoAway() {
            dir = FuCore.RotateVector2(dir, -90);
            transform.DOMove((Vector3)dir.normalized * 20 + transform.position, 4);
        }

        public void MoveCircle() {
            M_SplineMove.Pause();
            float a = 0;
            Observable.EveryUpdate().TakeWhile(x => { return !isRemoved && currentHP > 0 && gameObject.activeInHierarchy == true; }).Subscribe(x => {
                if (properties[0] < 0) {
                    dir = new Vector2(Mathf.Cos(a) * 2, Mathf.Sin(a) * 2);
                } else {
                    dir = new Vector2(Mathf.Cos(a) * 2, -Mathf.Sin(a) * 2);
                }
                transform.Translate(dir * Time.deltaTime);
                a += Time.deltaTime;
            }).AddTo(this);
        }
    }
}