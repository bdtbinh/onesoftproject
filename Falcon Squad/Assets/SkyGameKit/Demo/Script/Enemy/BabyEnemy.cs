﻿using SWS;
using DG.Tweening;
using UnityEngine;
using PathologicalGames;
using System.Collections;
using UniRx;
using System;

namespace SkyGameKit.Demo {
    public class BabyEnemy : Enemy1 {
        private float delay = 0.5f;
        public void RestartTurn() {
            Observable.Timer(TimeSpan.FromSeconds(delay)).Subscribe(x => { FreeWave.Instance.StartTurn("Baby"); });
        }
    }
}