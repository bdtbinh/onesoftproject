﻿using SWS;
using DG.Tweening;
using UnityEngine;
using PathologicalGames;

namespace SkyGameKit.Demo {
    public class Enemy1 : BaseEnemy {
        public void PrintStart() {
            print("Enemy: " + gameObject.name + " start" + index);
        }

        public void PrintIndex() {
            print("Enemy: " + gameObject.name + " index: " + index);
        }

        public void PrintDie() {
            print("Enemy: " + gameObject.name + " die" + index);
        }

        public void LookDown() {
            transform.DORotate(90 * Vector3.back, 1f).SetEase(Ease.InQuad);
        }

        public void DropItem(GameObject item) {
            PoolManager.Pools[Const.itemPoolName].Spawn(item, transform.position, Quaternion.identity);
        }

        private Animator anim;
        private void Start() {
            anim = GetComponentInChildren<Animator>();
        }

        public bool syncAnimation = true;

        protected override void RestartAnimator() {
            if (syncAnimation) {
                anim.Play(anim.GetCurrentAnimatorStateInfo(0).fullPathHash, -1, 0f);
            }
        }
        public override void ForceRemove() {
            base.ForceRemove();
            Die(EnemyKilledBy.Player);
        }
    }
}