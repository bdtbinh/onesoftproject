﻿using UnityEngine;
using SWS;
using DG.Tweening;
namespace SkyGameKit.Demo {
    public class Enemy3 : SkyGameKit.BaseEnemy {
        public PathManager[] listPath;
        public override void Restart() {
            base.Restart();
            PathManager myPath = listPath[Random.Range(0, listPath.Length)];
            M_SplineMove.SetPath(myPath);
            M_SplineMove.events[myPath.GetWaypointCount() - 1].AddListener(() => {
                transform.DOMove(new Vector2(properties[0], properties[1]), 2f);
            });
        }
    }
}