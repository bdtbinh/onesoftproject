﻿using PathologicalGames;
using UnityEngine;
using System.Collections.Generic;
namespace SkyGameKit.Demo {
    public class TurnManagerEx2 : TurnManager {
        public MiniTurnEx2[] listMiniTurn;
        protected virtual void Start() {
            totalEnemy = 0;
            if (listMiniTurn.Length > 0) {
                foreach (var miniTurn in listMiniTurn) {
                    if (miniTurn.startPosition.Count <= 0) {
                        miniTurn.startPosition.Add(Vector3.zero);
                    }
                    totalEnemy += miniTurn.startPosition.Count;
                }
            }
        }
        protected override void BeginTurn() {
            int i = 0;
            this.DoActionEveryTime(timeToNextEnemy, totalEnemy, () => {
                Transform myInstance = PoolManager.Pools[Const.enemyPoolName].Spawn(
                    listMiniTurn[i].enemyPrefab,
                    transform.position + (Vector3)listMiniTurn[i].startPosition[0],
                    transform.rotation,
                    transform
                    );
                BaseEnemy enemy = myInstance.GetComponent<BaseEnemy>();
                InitEnemy(enemy, listMiniTurn[i]);
                listMiniTurn[i].startPosition.RemoveAt(0);
                if (listMiniTurn[i].startPosition.Count <= 0) i++;
            }, true);
            base.BeginTurn();
        }
    }
    [System.Serializable]
    public class MiniTurnEx2 : MiniTurn {
        [Tooltip("Vị trí enemy được sinh ra")]
        public List<Vector2> startPosition;
    }
}
