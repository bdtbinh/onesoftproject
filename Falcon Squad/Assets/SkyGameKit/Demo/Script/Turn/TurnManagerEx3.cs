﻿using PathologicalGames;
using UnityEngine;
using System.Collections.Generic;
namespace SkyGameKit.Demo {
    public class TurnManagerEx3 : TurnManager {
        public MiniTurnEx3[] listMiniTurn;
        protected virtual void Start() {
            totalEnemy = 0;
            if (listMiniTurn.Length > 0) {
                foreach (var miniTurn in listMiniTurn) {
                    if (miniTurn.endPosition.Count <= 0) {
                        miniTurn.endPosition.Add(Vector3.zero);
                    }
                    totalEnemy += miniTurn.endPosition.Count;
                }
            }
        }
        protected override void BeginTurn() {
            int i = 0;
            int enemyIndex = 0;
            this.DoActionEveryTime(timeToNextEnemy, totalEnemy, () => {
                Transform myInstance = PoolManager.Pools[Const.enemyPoolName].Spawn(listMiniTurn[i].enemyPrefab, transform.position, transform.rotation, transform);
                BaseEnemy enemy = myInstance.GetComponent<BaseEnemy>();
                enemy.properties = new float[2];
                enemy.properties[0] = listMiniTurn[i].endPosition[0].x;
                enemy.properties[1] = listMiniTurn[i].endPosition[0].y;
                enemy.Restart();
                enemy.index = enemyIndex;
                enemyIndex++;
                enemyList.Add(enemy);
                listMiniTurn[i].endPosition.RemoveAt(0);
                if (listMiniTurn[i].endPosition.Count <= 0) i++;
            }, true);
            base.BeginTurn();
        }
    }

    [System.Serializable]
    public class MiniTurnEx3 : MiniTurn {
        public List<Vector2> endPosition;
    }
}

