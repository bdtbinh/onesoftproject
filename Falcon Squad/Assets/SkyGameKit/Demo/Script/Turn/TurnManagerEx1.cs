﻿using PathologicalGames;
using UnityEngine;
namespace SkyGameKit.Demo {
    public class TurnManagerEx1 : TurnManager {
        public MiniTurnEx1[] listMiniTurn;
        protected virtual void Start() {
            totalEnemy = 0;
            if (listMiniTurn.Length > 0) {
                foreach (var miniTurn in listMiniTurn) {
                    if (miniTurn.number <= 0) {
                        miniTurn.number = 1;
                    }
                    totalEnemy += miniTurn.number;
                }
            }
        }
        protected override void BeginTurn() {
            int i = 0;
            this.DoActionEveryTime(timeToNextEnemy, totalEnemy, () => {
                Transform myInstance = PoolManager.Pools[Const.enemyPoolName].Spawn(listMiniTurn[i].enemyPrefab, transform.position, transform.rotation, transform);
                BaseEnemy enemy = myInstance.GetComponent<BaseEnemy>();
                InitEnemy(enemy, listMiniTurn[i]);
                listMiniTurn[i].number--;
                if (listMiniTurn[i].number <= 0) i++;
            }, true);
            base.BeginTurn();
        }
    }

    [System.Serializable]
    public class MiniTurnEx1 : MiniTurn {
        [Tooltip("Số lượng enemy được sinh ra")]
        public int number;
    }
}
