﻿using UnityEngine;
using System;
namespace SkyGameKit.Demo {
    public class GunDemo1 : GunTurret {
        private int _countToShow = 1;
        public int CountToShow {
            get { return _countToShow; }
            set {
                _countToShow = value;
                if (value == 0 && IsRunning) {
                    ShowUp();
                }
            }
        }

        protected SpriteRenderer myRenderer;
        protected Collider2D myCollider;
        protected virtual void Awake() {
            myRenderer = GetComponent<SpriteRenderer>();
            myCollider = GetComponent<Collider2D>();
        }
        protected virtual void ShowUp() {
            myCollider.enabled = true;
            myRenderer.enabled = true;
            _shotCtrl.StartShotRoutine();
        }

        protected virtual void Hide() {
            myCollider.enabled = false;
            myRenderer.enabled = false;
            _shotCtrl.StopShotRoutine();
        }

        public override void Restart() {
            startShotOnRestart = false;
            base.Restart();
            try {
                CountToShow = (int)properties[0];
            } catch (Exception) {
                CountToShow = 0;
            }
            if (CountToShow > 0) Hide();
        }
    }
}
