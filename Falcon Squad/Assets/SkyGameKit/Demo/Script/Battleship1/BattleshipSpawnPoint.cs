﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SkyGameKit.Demo {
    public class BattleshipSpawnPoint : GunTurret {
        public override void Restart() {
            startShotOnRestart = false;
            base.Restart();
        }
        public void StartSpawnEnemy() {
           this.DoActionEveryTime(10f, ()=> FreeWave.Instance.StartTurn("FreeTurn1", transform), true);
        }
    }
}
