﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkyGameKit.Demo;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;
namespace SkyGameKit.Demo {
    public class BossDemo1 : Battleship {
        public float speed = 2f;
        public float radius = 4f;
        private Vector2 stopPathPosition;

        public void MovePatrol() {
            M_SplineMove.Stop();
            transform.DOMoveX(-radius, (radius / speed) / 2).OnComplete(() => transform.DOMoveX(radius, radius / speed).SetLoops(-1, LoopType.Yoyo));
        }
        public void MoveRandom() {
            M_SplineMove.Stop();
            stopPathPosition = transform.position;
            Vector3 targetPosition = stopPathPosition;
            this.UpdateAsObservable().Subscribe(x => {
                if (Vector3.Distance(transform.position, targetPosition) < 0.3f) {
                    targetPosition = GetRandomTargetValue(stopPathPosition);
                }
                transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
            });
        }

        public void SpawnBabyEnemy() {
            for (int i = 0; i < 5; i++) {
                FreeWave.Instance.StartTurn("Baby", transform);
            }
        }

        private Vector2 GetRandomTargetValue(Vector2 stopPathPosition) {
            float a = Random.Range(0, 2 * Mathf.PI);
            float d = Random.Range(0, radius);
            return stopPathPosition + new Vector2(d * Mathf.Cos(a), d * Mathf.Sin(a));
        }
        public override void OnGunDie() {
            base.OnGunDie();
            foreach (var gun in allGun) {
                var g = gun as GunDemo1;
                if (g != null) {
                    g.CountToShow--;
                }
            }
        }
    }
}
