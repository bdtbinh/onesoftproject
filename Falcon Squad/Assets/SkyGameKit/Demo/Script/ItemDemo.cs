﻿using PathologicalGames;
using UnityEngine;
using Sirenix.OdinInspector;
using UniRx;
using SkyGameKit.QuickAccess;
namespace SkyGameKit.Demo {
    public class ItemDemo : SgkItem {
        [MinMaxSlider(0, 10)]
        public Vector2 force;
        [MinMaxSlider(-45, 45)]
        public Vector2 angle;

        public float timeScale = 2f;
        private const float g = 1f;

        private Vector2 startVector;
        private float startTime;
        private Vector3 startPosition;
        private float t;
        private bool directV = true;//Đánh dấu hướng bên trái hay phải để tính đập tường
        private bool goToPlayer = false;
        public float goToPlayerSpeed = 10f;

        private Vector3 bottomLeft;
        private Vector3 topRight;
        void Start() {
            bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.nearClipPlane));
            topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.nearClipPlane));
        }

        protected virtual void OnSpawned() {
            goToPlayer = false;
            directV = true;
            var myForce = Random.Range(force.x, force.y);
            var myAngle = Random.Range(angle.x, angle.y);
            startVector = FuCore.RotateVector2(Vector2.up * myForce, myAngle);
            startPosition = transform.position;
            newPosition = startPosition;
            startTime = Time.time;
        }

        protected virtual void OnTriggerStay2D(Collider2D c) {
            if (c.name.Equals("ItemCheck")) {
                if (!goToPlayer) goToPlayer = true;
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D c) {
            if (c.name.Equals("BulletCheck")) {
                OnItemHitPlayer();
                Despawn();
            } else if (c.name.Equals("ItemCheck")) {
                if (!goToPlayer) goToPlayer = true;
            }
        }

        private Vector3 newPosition;
        protected virtual void Update() {
            if (transform.position.x > topRight.x || transform.position.x < bottomLeft.x) directV = !directV;
            //TODO tối ưu
            t = timeScale * (Time.time - startTime);
            if (goToPlayer) {
                goToPlayerSpeed += 50f * Time.deltaTime;//Tốc độ tăng đần theo thời gian
                transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, goToPlayerSpeed * Time.deltaTime);
            } else {
                //Bay theo quỹ đạo parabol 
                newPosition.x += (directV ? 1 : -1) * startVector.x * timeScale * Time.deltaTime;
                newPosition.y = startPosition.y + startVector.y * t - 0.5f * g * t * t;
                transform.position = newPosition;
            }
            //Xóa item khi đi quá màn hình
            if (transform.position.y < bottomLeft.y - Const.offsetCamera) {
                Despawn();
            }
        }
    }
}
