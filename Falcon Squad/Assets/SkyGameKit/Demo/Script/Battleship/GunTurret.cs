﻿using PathologicalGames;
using Sirenix.OdinInspector;
using UnityEngine;
using SkyGameKit.QuickAccess;
namespace SkyGameKit.Demo {
    public class GunTurret : BaseEnemy {
        [HideInInspector]
        public Battleship battleshipBody;
        [HideInInspector]
        public Vector2 offsetToBody;
        /// <summary>
        /// Súng đã được đưa ra khỏi pool và chưa chết
        /// </summary>
        public bool IsRunning { get; private set; }
        /// <summary>
        /// Khi được đưa ra khỏi pool là bắn luôn
        /// </summary>
        public bool startShotOnRestart = true;
        public bool LookAtPlayer = true;

        public UbhShotCtrl _shotCtrl;
        private void Update() {
            transform.position = battleshipBody.transform.TransformPoint(offsetToBody);
            if (LookAtPlayer) transform.rotation = FuCore.LockAt2D(Player.transform.position - transform.position, 180);
        }
        public override void Restart() {
            base.Restart();
            IsRunning = true;
            if (startShotOnRestart) {
                if (_shotCtrl != null) {
                    _shotCtrl.StartShotRoutine();
                }
            }
            battleshipBody.allGun.Add(this);
        }
        public override void Die(EnemyKilledBy type = EnemyKilledBy.Player) {
            if (PoolManager.Pools[Const.enemyPoolName].IsSpawned(transform)) {
                transform.parent.GetComponent<TurnManager>().enemyRemain--;
                Transform explosionTransform = Fu.SpawnExplosion(dieExplosion, transform.position, Quaternion.identity);
                if (explosionTransform != null) {
                    SgkExplosion explosion = explosionTransform.GetComponent<SgkExplosion>();
                    if (explosion != null) {
                        explosion.target = battleshipBody.transform;
                        explosion.offset = offsetToBody;
                    }
                }
                if (type == EnemyKilledBy.Player) {
                    LevelManager.OnEnemyDie(ID, score);
                    deadEvent.Invoke();
                }
                if (PoolManager.Pools[Const.pathPool].IsSpawned(newPathTransform)) {
                    PoolManager.Pools[Const.pathPool].Despawn(newPathTransform);
                }
                M_SplineMove.Stop();
                PoolManager.Pools[Const.enemyPoolName].Despawn(transform, Const.EnemyPoolTransform);
                LevelManager.aliveEnemy.Remove(this);
            }
            battleshipBody.allGun.Remove(this);
            battleshipBody.OnGunDie();
        }
        public override void ForceRemove() {
            Die(EnemyKilledBy.TimeOut);
            IsRunning = false;
            base.ForceRemove();
        }
    }
}
