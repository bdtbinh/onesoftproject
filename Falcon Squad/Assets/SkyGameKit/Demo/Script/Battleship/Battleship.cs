﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SkyGameKit.Demo {
    public class Battleship : BaseEnemy {
        [HideInInspector]
        public List<GunTurret> allGun;
        public bool DieWhenAllGunDie = false;
        public float DieDelay = 2f;
        public override void Restart() {
            base.Restart();
            currentHP = int.MaxValue;//Vô hạn máu
        }
        public override void ForceRemove() {
            Die(EnemyKilledBy.TimeOut);
            base.ForceRemove();
        }

        public virtual void OnGunDie() {
            if (DieWhenAllGunDie) {
                if (allGun.Count <= 0) {
                    this.Delay(DieDelay, () => Die(EnemyKilledBy.Player));
                }
            }
        }
    }
}
