﻿using PathologicalGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SkyGameKit.Demo {
    public class BattleshipAsTurn : TurnManager {
        public MiniTurn battleshipBody;
        public MiniTurnEx2[] listGunTurret;
        void Start() {
            totalEnemy = 0;
            if (listGunTurret.Length > 0) {
                foreach (var miniTurn in listGunTurret) {
                    if (miniTurn.startPosition.Count <= 0) {
                        miniTurn.startPosition.Add(Vector3.zero);
                    }
                    totalEnemy += miniTurn.startPosition.Count;
                }
                totalEnemy++;//Cộng thêm body nữa
            }
        }
        protected override void BeginTurn() {
            Transform battleshipBodyTrans = PoolManager.Pools[Const.enemyPoolName].Spawn(battleshipBody.enemyPrefab, transform.position, transform.rotation, transform);
            Battleship battleshipBodyComponent = battleshipBodyTrans.GetComponent<Battleship>();
            InitEnemy(battleshipBodyComponent, battleshipBody);
            foreach (var gunTurretType in listGunTurret) {
                for (int i = 0; i < gunTurretType.startPosition.Count; i++) {
                    Transform myInstance = PoolManager.Pools[Const.enemyPoolName].Spawn(gunTurretType.enemyPrefab, transform.position, transform.rotation, transform);
                    GunTurret turret = myInstance.GetComponent<GunTurret>();
                    turret.battleshipBody = battleshipBodyComponent;
                    turret.offsetToBody = gunTurretType.startPosition[i];
                    InitEnemy(turret, gunTurretType);
                }
            }
            base.BeginTurn();
        }
    }
}
