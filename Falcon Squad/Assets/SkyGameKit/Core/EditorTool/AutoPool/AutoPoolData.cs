﻿#if UNITY_EDITOR
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
namespace SkyGameKit
{
    public class AutoPoolData : SerializedScriptableObject
    {
        //public static readonly string[] allPoolName = new string[] { Const.enemyPoolName, Const.pathPool };
        public static readonly string[] allPoolName = new string[] { Const.enemyPoolName, Const.pathPool };

        public const string assetPath = "Assets/SkyGameKit/AutoPoolData.asset";
        public const string ubhPoolName = "UbhObjectPool";

        public List<PoolData> fixedDataList = new List<PoolData>();
        public List<PoolData> newDataList = new List<PoolData>();

        public static AutoPoolData Load()
        {
            //Load dữ liệu từ file
            AutoPoolData autoPoolData = AssetDatabase.LoadAssetAtPath<AutoPoolData>(assetPath);
            //Nếu không có thì tạo mới
            if (autoPoolData == null)
            {
                autoPoolData = CreateInstance<AutoPoolData>();
                AssetDatabase.CreateAsset(autoPoolData, assetPath);
            }
            //autoPoolData.poolDataList bị xóa null khi đã thống kê xong, chứ không xóa file asset
            if (autoPoolData.newDataList.Count == 0)
            {
                for (int i = 0; i < allPoolName.Length; i++)
                {
                    autoPoolData.newDataList.Add(new PoolData(allPoolName[i]));
                }
                autoPoolData.newDataList.Add(new PoolData(ubhPoolName));
            }
            return autoPoolData;
        }
    }

    [Serializable]
    public class PoolData
    {
        public string poolName;
        public List<PoolItem> data = new List<PoolItem>();

        /// <summary>
        /// Nếu chưa có thì thêm mới, nếu đã có thì lấy số lượng lớn hơn
        /// </summary>
        /// <param name="newPoolData">Đầu vào để so sánh</param>
        public void AddOrIncrease(List<PoolItem> newData)
        {
            foreach (var newItem in newData)
            {
                int foundIndex = -1;
                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].prefab == newItem.prefab)
                    {
                        foundIndex = i;
                        break;
                    }
                }
                if (foundIndex < 0)
                {
                    data.Add(new PoolItem(newItem.prefab, newItem.preloadAmount));
                }
                else
                {
                    if (data[foundIndex].preloadAmount < newItem.preloadAmount) data[foundIndex].preloadAmount = newItem.preloadAmount;
                }
            }
        }

        public PoolData(string poolName, List<PoolItem> data)
        {
            this.poolName = poolName;
            this.data = data.Select(item => item.Clone()).ToList();
        }

        public PoolData(string poolName)
        {
            this.poolName = poolName;
        }

        public PoolData Clone() { return new PoolData(poolName, data); }
    }

    [Serializable]
    public class PoolItem
    {
        public Transform prefab;
        public int preloadAmount;

        public PoolItem(Transform prefab, int preloadAmount)
        {
            this.prefab = prefab;
            this.preloadAmount = preloadAmount;
        }

        public PoolItem Clone() { return new PoolItem(prefab, preloadAmount); }
    }
}
#endif
