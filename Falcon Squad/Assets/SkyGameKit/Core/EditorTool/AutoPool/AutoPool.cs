﻿#if UNITY_EDITOR
using PathologicalGames;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace SkyGameKit
{
    public class AutoPool : MonoBehaviour
    {
        private void Start()
        {
            this.Delay(1f, () =>
            {
                Debug.LogError("Cẩn thận AutoPool bắt đầu hoạt động");
                LevelManagerCore.Instance.OnGameStateChange += OnVictory;
            });
        }

        [MenuItem("Tools/Click Finish Auto Pool")]
        static public void FinishAutoPool()
        {
            LevelManager.Instance.OnGameStateChange(GameStateType.Victory);
        }


        private void OnVictory(GameStateType state)
        {
            if (state != GameStateType.Victory) return;
            Dictionary<Transform, int> countTmp = new Dictionary<Transform, int>();//Biến tạm để đếm pool 
            AutoPoolData autoPoolData = AutoPoolData.Load();
            foreach (var poolData in autoPoolData.newDataList)
            {
                countTmp.Clear();
                var poolName = poolData.poolName;

                //Đếm số Prefab trong pool sau khi đã chạy xong                
                if (poolName == AutoPoolData.ubhPoolName)
                {
                    CountUbhObjectPool sgkUbhObjectPool = FindObjectOfType<CountUbhObjectPool>();
                    foreach (Transform child in sgkUbhObjectPool.transform)
                    {
                        Transform childPrefab = sgkUbhObjectPool.GetPrefab(child);
                        if (countTmp.ContainsKey(childPrefab))
                        {
                            countTmp[childPrefab]++;
                        }
                        else
                        {
                            countTmp[childPrefab] = 1;
                        }
                    }
                }
                else
                {
                    SpawnPool pool = PoolManager.Pools[poolName];
                    foreach (Transform child in pool.transform)
                    {
                        Transform childPrefab = pool.GetPrefab(child);
                        if (countTmp.ContainsKey(childPrefab))
                        {
                            countTmp[childPrefab]++;
                        }
                        else
                        {
                            countTmp[childPrefab] = 1;
                        }
                    }
                }

                //Nếu chưa có thì thêm mới, nếu đã có thì lấy số lượng lớn hơn
                foreach (var item in countTmp)
                {
                    int foundIndex = -1;
                    for (int i = 0; i < poolData.data.Count; i++)
                    {
                        if (poolData.data[i].prefab == item.Key)
                        {
                            foundIndex = i;
                            break;
                        }
                    }
                    if (foundIndex < 0)
                    {
                        poolData.data.Add(new PoolItem(item.Key, item.Value));
                    }
                    else
                    {
                        if (poolData.data[foundIndex].preloadAmount < item.Value) poolData.data[foundIndex].preloadAmount = item.Value;
                    }
                }
            }
            AssetDatabase.SaveAssets();
        }
    }
}
#endif
