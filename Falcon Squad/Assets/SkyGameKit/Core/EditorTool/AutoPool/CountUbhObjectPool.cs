﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Đây là class tạm để đếm số lượng object của UbhObjectPool
/// </summary>
[DisallowMultipleComponent]
public class CountUbhObjectPool : UbhObjectPool {
    private Dictionary<Transform, Transform> cachePrefab = new Dictionary<Transform, Transform>();

    public override UbhBullet GetBullet(GameObject goPrefab, Vector3 position, bool forceInstantiate = false) {
        UbhBullet result = base.GetBullet(goPrefab, position, forceInstantiate);
        cachePrefab[result.transform] = goPrefab.transform;
        return result;
    }

    public Transform GetPrefab(Transform instance) {
        return cachePrefab[instance];
    }
}
