﻿#if UNITY_EDITOR
using PathologicalGames;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SkyGameKit {
    public class AutoPoolMenuItem : MonoBehaviour {
        [MenuItem("Sky Game Kit/Add AutoPool", true)]
        private static bool ValidateAddAutoPool() {
            return FindObjectsOfType<AutoPool>().Length == 0;
        }
        [MenuItem("Sky Game Kit/Remove AutoPool", true)]
        private static bool ValidateRemoveAutoPool() {
            return !ValidateAddAutoPool();
        }

        [MenuItem("Sky Game Kit/Select AutoPoolData", false, 700)]
        private static void SelectAutoPoolData() {
            Selection.activeObject = AutoPoolData.Load();
            AssetDatabase.SaveAssets();
        }

        [MenuItem("Sky Game Kit/Add AutoPool", false, 700)]
        private static void AddAutoPool() {
            //Xóa pool cũ
            GameObject allPool = GameObject.Find("/Pool");
            if (allPool != null) DestroyImmediate(allPool);

            //Lấy hết pool đã tạo và xóa đi
            SpawnPool[] allSpawnPool = Object.FindObjectsOfType<SpawnPool>();
            foreach (var item in allSpawnPool) {
                if (AutoPoolData.allPoolName.Contains(item.poolName)) {
                    DestroyImmediate(item.gameObject);
                }
            }
            UbhObjectPool[] allUbhPool = Object.FindObjectsOfType<UbhObjectPool>();
            foreach (var item in allUbhPool) {
                Object.DestroyImmediate(item.gameObject);
            }

            //Tạo autoPool mới
            GameObject autoPool = new GameObject("AutoPool", typeof(AutoPool));
            for (int i = 0; i < AutoPoolData.allPoolName.Length; i++) {
                CreatePool(AutoPoolData.allPoolName[i], autoPool.transform);
            }
            CreateSgkUbhObjectPool(autoPool.transform);

            //Khởi tạo Assets nếu không có
            AutoPoolData.Load();
            AssetDatabase.SaveAssets();

            UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
        }

        [MenuItem("Sky Game Kit/Remove AutoPool", false, 701)]
        private static void RemoveAutoPool() {
            //Xóa AutoPool đi
            AutoPool[] apList = Object.FindObjectsOfType<AutoPool>();
            foreach (var spm in apList) {
                Object.DestroyImmediate(spm.gameObject);
            }
            //Lấy dữ liệu gán vào pool mới
            AutoPoolData autoPoolData = AutoPoolData.Load();

            List<PoolData> finalDataList = CalculateFinalDataList(autoPoolData.fixedDataList, autoPoolData.newDataList);

            Transform poolContainer = new GameObject("Pool").transform;
            foreach (var poolData in finalDataList) {
                if (poolData.poolName == AutoPoolData.ubhPoolName) {
                    UbhObjectPool pool = CreateUbhObjectPool(poolContainer);
                    if (pool.m_initializePoolList == null) pool.m_initializePoolList = new List<UbhObjectPool.InitializePool>();
                    foreach (var item in poolData.data) {
                        UbhObjectPool.InitializePool prefabPool = new UbhObjectPool.InitializePool() { m_bulletPrefab = item.prefab.gameObject, m_initialPoolNum = item.preloadAmount };
                        pool.m_initializePoolList.Add(prefabPool);
                    }
                } else {
                    SpawnPool pool = CreatePool(poolData.poolName, poolContainer);
                    foreach (var item in poolData.data) {
                        PrefabPool prefabPool = new PrefabPool(item.prefab) { preloadAmount = item.preloadAmount };
                        pool._perPrefabPoolOptions.Add(prefabPool);
                    }
                }
            }

            //Xóa dữ liệu đã lưu
            autoPoolData.newDataList.Clear();
            AssetDatabase.SaveAssets();

            Debug.Log("Đã cập nhật EnemyPool");
            UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
        }

        private static List<PoolData> CalculateFinalDataList(List<PoolData> fixedDataList, List<PoolData> newDataList) {
            List<PoolData> result = new List<PoolData>();
            result.AddRange(fixedDataList.Select(item => item.Clone()));
            foreach (var newPool in newDataList) {
                bool containsItem = fixedDataList.Any(i => i.poolName == newPool.poolName);
                if (!containsItem) result.Add(newPool.Clone());
            }
            foreach (var d1 in result) {
                foreach (var d2 in newDataList) {
                    if (d1.poolName == d2.poolName) {
                        d1.AddOrIncrease(d2.data);
                    }
                }
            }
            return result;
        }

        private static SpawnPool CreatePool(string name, Transform parent) {
            GameObject newPoolGo = new GameObject(name);
            newPoolGo.transform.parent = parent;
            SpawnPool pool = newPoolGo.AddComponent<SpawnPool>();
            pool.poolName = name;
            return pool;
        }

        private static void CreateSgkUbhObjectPool(Transform parent) {
            GameObject newPoolGo = new GameObject(AutoPoolData.ubhPoolName, typeof(CountUbhObjectPool));
            newPoolGo.transform.parent = parent;
        }

        private static UbhObjectPool CreateUbhObjectPool(Transform parent) {
            GameObject newPoolGo = new GameObject(AutoPoolData.ubhPoolName);
            newPoolGo.transform.parent = parent;
            return newPoolGo.AddComponent<UbhObjectPool>();
        }
    }
}
#endif
