﻿using System.Collections.Generic;
using UnityEngine;

namespace SkyGameKit {
    public static class RandomPoolUtils {
#if UNITY_EDITOR
        [UnityEditor.MenuItem("Assets/Count Enemy")]
        private static void CountEnemy() {
            var wave = UnityEditor.Selection.activeGameObject.GetComponent<WaveManager>();
            var numberOfEnemy = CountEnemy(wave);
            if (numberOfEnemy != null) {
                foreach (var item in numberOfEnemy) {
                    Debug.Log(item.Key.name + " --- " + item.Value);
                }
            }
        }

        [UnityEditor.MenuItem("Assets/Count Enemy", true)]
        private static bool CountEnemyValidation() {
            return UnityEditor.Selection.activeGameObject != null &&
                UnityEditor.Selection.activeGameObject.GetComponent<WaveManager>() != null;
        }
#endif

        public static Dictionary<BaseEnemy, int> CountEnemy(WaveManager wave) {
            if (wave == null) return null;

            var listTurn = wave.GetComponentsInChildren<TurnDesignCuongvt1>();
            if (listTurn == null || listTurn.Length == 0) return null;

            Dictionary<BaseEnemy, int> numberOfEnemy = new Dictionary<BaseEnemy, int>();

            foreach (var turn in listTurn) {
                foreach (var miniTurn in turn.listMiniTurn) {
                    if (miniTurn.enemyPrefab != null) {
                        var enemy = miniTurn.enemyPrefab.GetComponent<BaseEnemy>();

                        //Phải lấy Max vì anh Cương mặc định ko điền gì thì sẽ ra 1 con
                        var number = Mathf.Max(1, miniTurn.enemyStartByNumber ? miniTurn.number : miniTurn.startPosition.Count);

                        if (miniTurn.enemyStartByNumber && miniTurn.enemyStartByPosition) {
                            Debug.LogError("Turn: " + turn.name + " có cả 2 loại Start By Number và Position, tự động tính theo Number");
                        } else if (!miniTurn.enemyStartByNumber && !miniTurn.enemyStartByPosition) {
                            Debug.LogError("Turn: " + turn.name + " không có Start By Number hay Position");
                        }

                        if (numberOfEnemy.ContainsKey(enemy)) {
                            numberOfEnemy[enemy] += number;
                        } else {
                            numberOfEnemy[enemy] = number;
                        }
                    }
                }
            }

            return numberOfEnemy;
        }
    }
}
