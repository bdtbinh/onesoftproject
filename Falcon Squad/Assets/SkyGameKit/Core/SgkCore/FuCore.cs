﻿using System;
using System.Linq;
using System.Net;
using UnityEngine;

namespace SkyGameKit {
    public class FuCore {
        public static bool th(params int[] a) {
            int x = a[0];
            for (int i = 1; i < a.Length; i++) {
                if (x == a[i]) {
                    return true;
                }
            }
            return false;
        }

        public static float GoTo0(float value, float speed) {
            speed = Math.Abs(speed);
            if (Math.Abs(value) <= speed) return 0;
            else return value + (value > 0 ? -speed : speed);
        }

        public static float GoToX(float x, float currentValue, float speed) {
            speed = Math.Abs(speed);
            var a = x - currentValue;
            if (Math.Abs(a) <= speed) return x;
            else return currentValue + (a < 0 ? -speed : speed);
        }


        public static Quaternion LockAt2D(Vector2 worldPosition, float angleOffset = 0) {
            float rot_z = Mathf.Atan2(worldPosition.y, worldPosition.x) * Mathf.Rad2Deg;
            return Quaternion.Euler(0f, 0f, rot_z - 90 - angleOffset);
        }

        /// <summary>
        /// Trả về k số tự nhiên khác nhau ngẫu nhiên thứ tự trong khoảng 0..n 
        /// </summary>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static int[] RandomElementsArray(int k, int n) {
            int[] arr = Enumerable.Range(0, n + 1).ToArray();
            for (int i = 0; i < n; i++) {
                Swap(ref arr[UnityEngine.Random.Range(0, n)], ref arr[UnityEngine.Random.Range(0, n)]);
            }
            return arr.Take(k).ToArray();
        }

        public static void Swap<T>(ref T lhs, ref T rhs) {
            T temp = lhs;
            lhs = rhs;
            rhs = temp;
        }
        public static Vector2 RotateVector2(Vector2 v, float degrees) {
            float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
            float tx = v.x;
            float ty = v.y;
            v.x = (cos * tx) - (sin * ty);
            v.y = (sin * tx) + (cos * ty);
            return v;
        }        
    }
}
