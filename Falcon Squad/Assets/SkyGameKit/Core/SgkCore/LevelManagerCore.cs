﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SkyGameKit {
    public abstract class LevelManagerCore : SgkSingleton<LevelManagerCore> {
        private static int _score = 0;
        public static int Score {
            get {
                return _score;
            }
            set {
                _score = value;
                if (Instance != null && Instance.OnScoreChange != null) {
                    Instance.OnScoreChange(value);
                }
            }
        }
        public Action<int> OnScoreChange;

        private GameStateType _gameState = GameStateType.Playing;

        public GameStateType GameState {
            get { return _gameState; }
            set {
                _gameState = value;
                if (OnGameStateChange != null) {
                    OnGameStateChange(value);
                }
            }
        }
        public Action<GameStateType> OnGameStateChange;


        /// <summary>
        /// Danh sách enemy đã giết
        /// </summary>
        public static Dictionary<string, int> enemyKilled = new Dictionary<string, int>();
        /// <summary>
        /// Danh sách enemy đã được sinh ra
        /// </summary>
        public static Dictionary<string, int> enemySpawned = new Dictionary<string, int>();
        /// <summary>
        /// Thời gian enemy ngay trước đó chết
        /// </summary>
        public static float lastTimeEnemyDie;

        protected IWaveManager[] mWM;
        protected override void Awake() {
            base.Awake();
            //Reset biến static mỗi khi bắt đầu một scene mới
            lastTimeEnemyDie = -1;
            Score = 0;
            enemyKilled.Clear();
            enemySpawned.Clear();
        }
        protected virtual void Start() {
            mWM = GetComponentsInChildren<IWaveManager>();
            StartCoroutine(StartWaveOnFirstUpdate());//Turn đếm enemy trong start, Nếu gọi NextWave ngay start có thể chưa đếm xong            
        }
        protected abstract IEnumerator StartWaveOnFirstUpdate();
        public abstract void NextWave();
        public abstract void NextWaveTime();
        public abstract void EndLevel();
        public abstract IWaveManager GetCurrentWave();
    }

    public enum GameStateType {
        Playing,
        GameOver,
        Victory
    }
}
