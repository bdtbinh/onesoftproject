﻿using System;

public interface IWaveManager {
    Action OnStartWave { get; set; }
    Action OnEndWave { get; set; }
    int TurnRemain { get; }
    bool IsRunning { get; }
    bool IsDelayWave { get; }
    void StartWave();
    void EndWave();
}