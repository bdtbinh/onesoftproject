﻿using UnityEngine;

namespace SkyGameKit {
    public class CatmullRom {
        private Vector2 p0, p1, p2, p3;

        public CatmullRom(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3) {
            this.p0 = p0;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
        }

        public Vector2 GetPoint2(float t) {
            float i = (1f - t);
            float a0 = i * i * i;
            float a1 = 3f * t * i * i;
            float a2 = 3f * t * t * i;
            float a3 = t * t * t;
            float x = p0.x * a0;
            x += p1.x * a1;
            x += p2.x * a2;
            x += p3.x * a3;
            float y = p0.y * a0;
            y += p1.y * a1;
            y += p2.y * a2;
            y += p3.y * a3;
            return new Vector2(x, y);
        }
    }
}
