﻿#if UNITY_EDITOR
using SWS;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace SkyGameKit {
    public class ShowPathManager : MonoBehaviour {
        public static Color[] Colors = new Color[] { Color.magenta, Color.red, Color.cyan, Color.blue, Color.green, Color.yellow, new Color(0.33f, 0.11f, 0.55f), new Color(0.552f, 0.11f, 0.18f), new Color(0.75f, 1f, 0f) };
        public static List<PathManager> pathList = new List<PathManager>();

        private void OnDrawGizmos() {
            foreach (var path in pathList) {
                ShowPathInScene(path, Vector3.zero);
            }
        }

        public static void ShowPathInScene(PathManager path, Vector3 offset) {
            if (path == null) return;
            BezierPathManager bezierPath = path as BezierPathManager;
            Color myColor = Colors[Mathf.Abs(path.GetInstanceID() % Colors.Length)];
            GUIStyle textStyle = new GUIStyle();
            textStyle.normal.textColor = myColor;
            Handles.Label(path.GetPathPoints()[0] + TransformVector(path, offset), path.name, textStyle);
            if (bezierPath != null) {
                DrawBezierPath(bezierPath, myColor, offset);
            } else {
                DrawPath(path, myColor, offset);
            }
        }

        private static void DrawBezierPath(BezierPathManager bezierPath, Color pathColor, Vector3 offset) {
            var bPoints = bezierPath.bPoints;
            if (bPoints.Count <= 0) return;
            Vector3[] pathPoints = new Vector3[bezierPath.pathPoints.Length];
            for (int i = 0; i < pathPoints.Length; i++) {
                pathPoints[i] = bezierPath.pathPoints[i] + TransformVector(bezierPath, offset);
            }
            //assign line and waypoints color
            Gizmos.color = pathColor;
            for (int i = 1; i < bPoints.Count - 1; i++)
                Gizmos.DrawWireSphere(bPoints[i].wp.position + TransformVector(bezierPath, offset), 0.1f);
            //draw linear or curved lines with the same color
            if (bPoints.Count >= 2)
                WaypointManager.DrawCurved(pathPoints);
            else
                WaypointManager.DrawStraight(pathPoints);
        }

        private static void DrawPath(PathManager path, Color pathColor, Vector3 offset) {
            if (path.waypoints.Length <= 0) return;
            //get positions
            Vector3[] wpPositions = path.GetPathPoints();
            for (int i = 0; i < wpPositions.Length; i++) {
                wpPositions[i] = wpPositions[i] + TransformVector(path, offset);
            }
            //assign path ends color
            Vector3 start = wpPositions[0];
            Vector3 end = wpPositions[wpPositions.Length - 1];
            Gizmos.color = Color.black;
            Gizmos.DrawWireCube(start, Vector3.one * 0.2f);
            Gizmos.DrawWireCube(end, Vector3.one * 0.2f);
            //assign line and waypoints color
            Gizmos.color = pathColor;
            for (int i = 1; i < wpPositions.Length - 1; i++)
                Gizmos.DrawWireSphere(wpPositions[i], 0.1f);
            //draw linear or curved lines with the same color
            if (wpPositions.Length >= 2)
                WaypointManager.DrawCurved(wpPositions);
            else
                WaypointManager.DrawStraight(wpPositions);
        }

        private static Vector3 TransformVector(PathManager path, Vector3 offset) {
            return offset - path.transform.position;
        }
    }
}
#endif
