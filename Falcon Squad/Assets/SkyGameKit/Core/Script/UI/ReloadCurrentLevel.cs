﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class ReloadCurrentLevel {
    [MenuItem("GameObject/SkyGameKit/Reload Current Level %l", false, 0)]
    public static void Reload() {
        if (Application.isPlaying) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
#endif
