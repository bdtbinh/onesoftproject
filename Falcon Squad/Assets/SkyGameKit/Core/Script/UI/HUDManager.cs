﻿using UnityEngine.UI;
using UnityEngine;
using SkyGameKit.QuickAccess;
namespace SkyGameKit {
    public class HUDManager : MonoBehaviour {
        public Text scoreText;
        public Text liveText;
        public Text bulletLevelText;
        public Text starText;
        public Text endGameNotification;

        private void Start() {
            LevelManager.Instance.OnScoreChange += x => scoreText.text = "Score: " + x;
            LevelManager.Instance.OnGameStateChange += x => {
                if (x != GameStateType.Playing) {
                    endGameNotification.gameObject.SetActive(true);
                    endGameNotification.text = x == GameStateType.GameOver ? "GAME OVER" : "VICTORY";
                }
            };
            Player.State.onAddStar += x => starText.text = "Star: " + Player.State.Star;
            Player.Health.onCurrentHPChange += x => liveText.text = "Live: " + Player.Health.CurrentHP;
            Player.Attack.onBulletLevelChange += x => bulletLevelText.text = "Bullet: " + Player.Attack.BulletLevel;
        }

        public void ChangeColor(Image image) {
            image.color = Random.ColorHSV();
        }

        public void ChangeMoveMode(Text text) {
            Player.Move.moveMode = 1 - Player.Move.moveMode;
            text.text = Player.Move.moveMode.ToString();
        }
    }
}


