﻿using System.Collections.Generic;
using SWS;
using SkyGameKit.Demo;
using PathologicalGames;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
namespace SkyGameKit
{
    public class CustomHierarchyView : MonoBehaviour
    {
#if UNITY_EDITOR
        private static Texture2D pauseTexture;
        private static Texture2D playTexture;
        public static Dictionary<int, float> runningTurn = new Dictionary<int, float>();

        private void Awake()
        {
            pauseTexture = Resources.Load<Texture2D>("Hierarchy/pause");
            playTexture = Resources.Load<Texture2D>("Hierarchy/play");
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;
        }

        private static void HierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
        {
            if (EditorApplication.isPlaying)
            {
                if (LevelManager.Instance != null)
                {
                    Rect r = new Rect(selectionRect);
                    Rect r2 = new Rect(selectionRect);
                    r.x = r.width - 20;
                    r2.x = 10;
                    if (LevelManager.Instance != null)
                    {
                        WaveManager wm = LevelManager.Instance.GetCurrentWave() as WaveManager;
                        if (wm != null && wm.gameObject.GetInstanceID() == instanceID)
                        {
                            GUI.Label(r2, playTexture);
                        }
                        else if (runningTurn.ContainsKey(instanceID))
                        {
                            if (runningTurn[instanceID] > 0)
                            {
                                GUI.Label(r, runningTurn[instanceID].ToString("0.00"));
                                GUI.Label(r2, pauseTexture);
                            }
                            else
                            {
                                GUI.Label(r, (-runningTurn[instanceID]).ToString("0.00"));
                                GUI.Label(r2, playTexture);
                            }
                        }
                    }
                }
            }
        }

        [MenuItem("GameObject/SkyGameKit/New Level %#l", false, 0)]
        private static void CreateNewLevel(MenuCommand menuCommand)
        {
            if (LevelManager.Instance != null)
            {
                Debug.LogError("Đã có level manager rồi");
            }
            else
            {
                GameObject newLevel = new GameObject("New Level");
                newLevel.AddComponent<LevelManager>();
                newLevel.AddComponent<CustomHierarchyView>();
                GameObjectUtility.SetParentAndAlign(newLevel, menuCommand.context as GameObject);
                Undo.RegisterCreatedObjectUndo(newLevel, "Create " + newLevel.name);
                Selection.activeObject = newLevel;
            }
        }


        private static readonly System.Type[] allType = { typeof(BaseEnemy), typeof(PathManager), typeof(SgkItem), typeof(SgkExplosion), typeof(ParticleSystem) };
        private static readonly string[] allName = { Const.enemyPoolName, Const.pathPool, Const.itemPoolName, Const.explosiveName, Const.explosiveName };
        [MenuItem("GameObject/SkyGameKit/Add To Pool %#a", false, 0)]
        private static void AddToPool(MenuCommand menuCommand)
        {
            Dictionary<string, SpawnPool> dictionaryPool = new Dictionary<string, SpawnPool>();
            SpawnPool[] allPool = FindObjectsOfType<SpawnPool>();
            foreach (var item in allPool)
            {
                dictionaryPool[item.poolName] = item;
            }
            foreach (var obj in Selection.objects)
            {
                GameObject go = obj as GameObject;
                if (go != null)
                {
                    for (int i = 0; i < allType.Length; i++)
                    {
                        if (AddToPool(dictionaryPool, go, allType[i], allName[i])) break;
                    }
                }
            }
        }

        private static bool AddToPool(Dictionary<string, SpawnPool> dictionaryPool, GameObject go, System.Type type, string poolName)
        {
            if (go.GetComponent(type) != null)
            {
                if (!dictionaryPool.ContainsKey(poolName))
                {
                    GameObject newPoolGo = new GameObject(poolName);
                    SpawnPool newGoSpawnPool = newPoolGo.AddComponent<SpawnPool>();
                    newGoSpawnPool.poolName = poolName;
                    dictionaryPool.Add(poolName, newGoSpawnPool);
                }
                bool isExist = false;
                foreach (var item in dictionaryPool[poolName]._perPrefabPoolOptions)
                {
                    if (item.prefab == go.transform)
                    {
                        isExist = true;
                    }
                }
                if (!isExist)
                {
                    dictionaryPool[poolName]._perPrefabPoolOptions.Add(new PrefabPool(go.transform));
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        [MenuItem("GameObject/SkyGameKit/New Wave %#w", false, 0)]
        private static void CreateNewWave(MenuCommand menuCommand)
        {
            GameObject newWave = new GameObject("New Wave");
            if (LevelManager.Instance == null)
            {
                GameObject newLevel = new GameObject("New Level");
                newLevel.AddComponent<LevelManager>();
                newLevel.AddComponent<CustomHierarchyView>();
            }
            newWave.transform.parent = LevelManager.Instance.transform;
            newWave.AddComponent<WaveManager>();
            GameObjectUtility.SetParentAndAlign(newWave, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(newWave, "Create " + newWave.name);
        }

        [MenuItem("GameObject/SkyGameKit/New Turn Ex1 #F1", false, 0)]
        private static void CreateTurnEx1(MenuCommand menuCommand)
        {
            CreateTurn<TurnManagerEx1>(menuCommand);
            //TODO thêm path vào đây
        }

        [MenuItem("GameObject/SkyGameKit/New Turn Ex2 #F2", false, 0)]
        private static void CreateTurnEx2(MenuCommand menuCommand)
        {
            CreateTurn<TurnManagerEx2>(menuCommand);
        }

        public static void CreateTurn<T>(MenuCommand menuCommand) where T : BaseTurnManager
        {
            GameObject newTurn = new GameObject("New Turn");
            newTurn.AddComponent<T>();
            GameObject waveParent = Selection.activeObject as GameObject;
            if (waveParent != null)
            {
                if (waveParent.activeInHierarchy)
                {
                    newTurn.transform.parent = waveParent.transform;
                }
            }
            else
            {
                Debug.Log("Hãy nhớ click vào wave, rồi hãy nhấn phím tắt :(");
            }
            GameObjectUtility.SetParentAndAlign(newTurn, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(newTurn, "Create " + newTurn.name);
            Selection.activeObject = newTurn;
        }
#endif
    }
}
