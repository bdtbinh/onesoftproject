﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MyMenu : MonoBehaviour {
    [MenuItem("MyMenu/Build BackGround")]
    static void BuildBG() {
        Transform backGroundParent = (new GameObject("BackgroundTmp")).transform;
        GameObject selectionObj = Selection.activeObject as GameObject;
        for (int i = 1; i < 30; i++) {
            GameObject objectBG = PrefabUtility.InstantiatePrefab(selectionObj) as GameObject;
            objectBG.name = "BG" + i;
            objectBG.transform.position = i * objectBG.transform.localScale.y * Vector3.up;
            objectBG.transform.parent = backGroundParent;
        }
    }
}
#endif
