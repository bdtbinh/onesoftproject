﻿#if UNITY_EDITOR
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;
using SWS;
using PathologicalGames;
using System.Linq;

namespace SkyGameKit {
    [InitializeOnLoad]
    public class SgkSettingWindows : OdinEditorWindow {
        public bool showPathWhenSelected = false;

        static SgkSettingWindows() {
            Selection.selectionChanged -= ShowPath;
            Selection.selectionChanged += ShowPath;
        }

        [MenuItem("Sky Game Kit/Add or Remove ShowPathManager")]
        private static void AddOrRemoveShowPathManager() {
            ShowPathManager[] spmList = FindObjectsOfType<ShowPathManager>();
            if (spmList.Length == 0) {
                (new GameObject("ShowPathManager")).AddComponent<ShowPathManager>();
            } else {
                foreach (var spm in spmList) {
                    DestroyImmediate(spm.gameObject);
                }
            }
        }

        //[MenuItem("Sky Game Kit/Add or Remove AutoPool")]
        //private static void AddOrRemoveAutoPool() {
        //    AutoPool[] apList = FindObjectsOfType<AutoPool>();
        //    GameObject allPool = GameObject.Find("/Pool");
        //    if (allPool != null) DestroyImmediate(allPool);
        //    if (apList.Length == 0) {
        //        GameObject autoPool = new GameObject("AutoPool", typeof(AutoPool));
        //        SpawnPool[] allSpawnPool = FindObjectsOfType<SpawnPool>();
        //        foreach (var item in allSpawnPool) {
        //            if (AutoPool.allName.Contains(item.poolName)) {
        //                DestroyImmediate(item.gameObject);
        //            }
        //        }
        //        for (int i = 0; i < AutoPool.allName.Length; i++) {
        //            GameObject poolTransform = new GameObject(AutoPool.allName[i]);
        //            poolTransform.transform.parent = autoPool.transform;
        //            SpawnPool pool = poolTransform.AddComponent<SpawnPool>();
        //            pool.poolName = AutoPool.allName[i];
        //        }
        //    } else {
        //        allPool = new GameObject("Pool");
        //        for (int i = 0; i < AutoPool.allName.Length; i++) {
        //            GameObject poolTransform = new GameObject(AutoPool.allName[i]);
        //            poolTransform.transform.parent = allPool.transform;
        //            SpawnPool pool = poolTransform.AddComponent<SpawnPool>();
        //            pool.poolName = AutoPool.allName[i];
        //            foreach (var item in AutoPool.count[i]) {
        //                PrefabPool prefabPool = new PrefabPool(item.Key) {
        //                    preloadAmount = item.Value
        //                };
        //                pool._perPrefabPoolOptions.Add(prefabPool);
        //            }
        //        }
        //        foreach (var spm in apList) {
        //            DestroyImmediate(spm.gameObject);
        //        }
        //        for (int i = 0; i < AutoPool.allName.Length; i++) {
        //            AssetDatabase.DeleteAsset("Assets/SkyGameKit/AutoPoolData/" + AutoPool.allName[i] + ".asset");
        //        }
        //        Debug.Log("Đã cập nhật EnemyPool");
        //    }
        //}

        public static void ShowPath() {
            ShowPathManager.pathList.Clear();
            foreach (var obj in Selection.objects) {
                GameObject go = obj as GameObject;
                if (go != null) {
                    PathManager path = go.GetComponent<PathManager>();
                    if (path != null) {
                        ShowPathManager.pathList.Add(path);
                    }
                }
            }
        }
    }
}
#endif
