﻿using UnityEngine;
using SkyGameKit.QuickAccess;

namespace SkyGameKit {
    public class SgkWingman : MonoBehaviour {
        [Range(10, 50)]
        public float wingmanSpeed = 20;

        void Update() {
            var targetpos = Player.transform.position;
            targetpos.y -= 0.5f;
            transform.position = Vector3.Lerp(transform.position, targetpos, Time.deltaTime * wingmanSpeed);
        }
    }
}
