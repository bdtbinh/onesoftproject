﻿using UniRx;
using UnityEngine;
using SkyGameKit.QuickAccess;
using Sirenix.OdinInspector;
using System;

namespace SkyGameKit {
    public class SgkPlayerState : APlayerState {
        public GameObject shieldObj;
        private APlayerHealth playerH;
        void Start() {
            playerH = Player.Health;
            if (itemActiveTime <= 0) itemActiveTime = 8f;
        }

        public override void PickUpItem(SgkItem item) {
            base.PickUpItem(item);
            if (item.name.Contains("Item")) {
                if (item.name.Contains("Shield")) {
                    SetShieldActive(itemActiveTime);
                } else if (item.name.Contains("Live")) {
                    playerH.CurrentHP++;
                } else if (item.name.Contains("PowerUp")) {
                    Player.Attack.BulletLevel++;
                } else if (item.name.Contains("Star")) {
                    AddStar(item.value);
                };
            }
        }

        public virtual void AddStar(int number = 1) {
            Star += number;
        }

        public override void SetShieldActive(float activeTime = 8f) {
            shieldObj.SetActive(true);
            playerH.playerCanTakeDamage = false;
            this.Delay(activeTime, () => {
                shieldObj.SetActive(false);
                playerH.playerCanTakeDamage = true;
            });
        }
    }
}
