﻿using UnityEngine;
using DG.Tweening;
using SkyGameKit.QuickAccess;
using Sirenix.OdinInspector;

namespace SkyGameKit
{
    public class SgkPlayerController : APlayerMove
    {
        private float step;

        private Vector3 lastMousePos;
        private float playerTransformZ;

        private Vector3 bottomLeft;
        private Vector3 topRight;

        private int currentTouchId = -1;
        private Touch currentTouch;
        private bool currentTouchChanged = false;

        public override bool LockMove
        {
            get { return base.LockMove; }
            set
            {
                if (onAutoMove)
                {
                    Debug.Log("Player đang tự di chuyển, không được set LookMove");
                }
                else
                {
                    base.LockMove = value;
                }
            }
        }

        [HideIf("IsInRelativeMode")]
        public Vector2 offsetTouch = Vector2.down;
        public bool IsInRelativeMode()
        {
            return moveMode == MoveMode.Relative;
        }

        public bool lerpMove = true;
        public float marginTop = 0f;
        public float marginBottom = 0f;
        public float marginRightAndLeft = 0f;

        [Button]
        public void TestLockMove()
        {
            LockMove = !LockMove;
        }

        private void Awake()
        {
            onLockMoveChange += () => currentTouchId = -1;
        }

        private void Start()
        {
            playerTransformZ = transform.position.z;
            bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.nearClipPlane));
            bottomLeft.x += marginRightAndLeft;
            bottomLeft.y += marginBottom;
            topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.nearClipPlane));
            topRight.x -= marginRightAndLeft;
            topRight.y -= marginTop;
            if (speed <= 0) speed = 10f;
        }

        private bool overUI = false;
        private Vector3 mousePos;
        private void Update()
        {
            if (LockMove) return;
            if (Input.GetMouseButton(0) || Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
            {
                if (Input.touchCount > 0)
                {
                    if (currentTouchId < 0)
                    {
                        currentTouchId = Input.touches[0].fingerId;
                        currentTouchChanged = true;
                    }
                    currentTouch = GetTouch(currentTouchId);
                    if (currentTouch.phase == TouchPhase.Ended || currentTouch.phase == TouchPhase.Canceled) currentTouchId = -1;
                    mousePos = Camera.main.ScreenToWorldPoint(currentTouch.position);
                }
                else
                {
                    mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                }
                mousePos.z = playerTransformZ;
                step = speed * Time.deltaTime;
                if (moveMode == MoveMode.Touch)
                {
                    TouchMove();
                }
                else
                {
                    RelativeMove();
                }
            }
        }

        public static Touch GetTouch(int fingerId)
        {
            foreach (var touch in Input.touches)
            {
                if (touch.fingerId == fingerId)
                {
                    return touch;
                }
            }
            return new Touch() { fingerId = -1 };
        }
        private void TouchMove()
        {
            mousePos += (Vector3)offsetTouch;
            if (Input.GetMouseButtonDown(0))
            {
                if (Fu.IsPointerOverUIObject())
                {
                    overUI = true;
                }
            }
            if (overUI)
            {
                if (Input.GetMouseButton(0))
                {
                    if (!Fu.IsPointerOverUIObject())
                    {
                        overUI = false;
                    }
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    overUI = false;
                }
            }
            else
            {
                if (lerpMove)
                {
                    transform.position = Vector3.Lerp(transform.position, mousePos, 0.4f * step < 1 ? 0.5f * step : 1);
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, mousePos, step);
                }
            }
        }
        private void RelativeMove()
        {
            //Xác định điểm đầu tiên của di chuyển
            if (Input.touchCount == 0)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    lastMousePos = mousePos;
                    return;
                }
            }
            else
            {
                if (currentTouchChanged)
                {
                    lastMousePos = mousePos;
                    currentTouchChanged = false;
                    return;
                }
            }
            Vector3 offsetMouse = mousePos - lastMousePos;
            Vector3 trans;
            if (lerpMove)
            {
                trans = transform.position + offsetMouse * (0.4f * step < 1 ? 0.5f * step : 1);
            }
            else
            {
                trans = Vector3.MoveTowards(transform.position, transform.position + offsetMouse, step);
            }
            lastMousePos += trans - transform.position;//lastMousePos phải cập nhật trước khi Clamp
            trans.x = Mathf.Clamp(trans.x, bottomLeft.x, topRight.x);
            trans.y = Mathf.Clamp(trans.y, bottomLeft.y, topRight.y);
            transform.position = trans;
        }

        private bool onAutoMove = false;
        private bool lockMoveSaveValue;
        public override Tweener AutoMove(Vector2 destination, float t = 1f, TweenCallback onComplete = null)
        {
            lockMoveSaveValue = LockMove;
            LockMove = true;
            onAutoMove = true;
            Vector3 destinationVector3 = new Vector3(destination.x, destination.y, transform.position.z);
            onComplete += () => { onAutoMove = false; LockMove = lockMoveSaveValue; };
            return transform.DOMove(destinationVector3, t).OnComplete(onComplete);
        }

        public void TransformTo(Vector2 d)
        {
            transform.position = new Vector3(d.x, d.y, transform.position.z);
        }
    }
}
