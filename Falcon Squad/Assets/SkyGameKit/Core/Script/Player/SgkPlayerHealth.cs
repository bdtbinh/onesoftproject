﻿using System;
using PathologicalGames;
using UnityEngine;
using UniRx;
using Sirenix.OdinInspector;
using SkyGameKit.QuickAccess;

namespace SkyGameKit {
    public class SgkPlayerHealth : APlayerHealth {
        public GameObject explosionPrefab;

        [Tooltip("Vị trí player bắt đầu đi từ dưới lên sau khi chết")]
        public Vector2 spawnPos;

        private Vector3 startPos;

        private SgkPlayerController playerC;
        private SgkPlayerState playerS;
        private SgkPlayerAttack playerA;

        void Start() {
            Transform player = transform.parent;
            playerC = player.GetComponent<SgkPlayerController>();
            playerS = player.GetComponent<SgkPlayerState>();
            playerA = player.GetComponent<SgkPlayerAttack>();
            startPos = player.position;
            playerCanTakeDamage = true;
            if (maxHP <= 0) maxHP = 3;
            CurrentHP = maxHP;
        }

        protected virtual void OnTriggerEnter2D(Collider2D c) {
            if (c.tag.Contains("Enemy")) {
                switch (c.tag) {
                    case "Enemy":
                        BaseEnemy enemy = c.transform.GetComponent<BaseEnemy>();
                        if (enemy != null) {
                            enemy.Die();
                        }
                        break;
                    case "EnemyBullet":
                        UbhBullet bullet = c.GetComponent<UbhBullet>();
                        if (bullet != null) UbhObjectPool.instance.ReleaseBullet(bullet);
                        break;
                }
                if (playerCanTakeDamage) {
                    if (CurrentHP > 0) {
                        Respawn();
                    } else {
                        Die();
                    }
                    CurrentHP--;
                }
            }
        }

        protected virtual void Die() {
            GameOver();
        }

        public override void Respawn() {
            Fu.SpawnExplosion(explosionPrefab, transform.position, Quaternion.identity);
            playerC.AutoMove(spawnPos, startPos);
            playerS.SetShieldActive(playerS.itemActiveTime);
        }

        protected virtual void GameOver() {
            try {
                if (LevelManager.Instance.GameState == GameStateType.Playing) {
                    LevelManager.Instance.OnGameStateChange(GameStateType.GameOver);
                }
            } catch (NullReferenceException) {
                print("GameOver and NullReferenceException");
            }
            playerC.TransformTo(spawnPos);
            playerC.enabled = false;
            playerA.shotCtrl.StopShotRoutine();
        }
    }
}
