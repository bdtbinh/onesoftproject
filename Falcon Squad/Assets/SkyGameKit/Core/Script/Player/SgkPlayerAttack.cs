﻿using UnityEngine;
using UniRx;
using SkyGameKit.QuickAccess;
using System;

namespace SkyGameKit {
    public class SgkPlayerAttack : APlayerAttack {
        public UbhShotCtrl shotCtrl;
        public override bool LockShot {
            get {
                return shotCtrl.shooting;
            }
            set {
                if (value) {
                    shotCtrl.StopShotRoutine();
                } else {
                    shotCtrl.StartShotRoutine();
                }
            }
        }
    }
}

