﻿using Spine;
using Spine.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Animation = Spine.Animation;
using AnimationState = Spine.AnimationState;
namespace SkyGameKit {
    public class PlaneSpineCtrl : MonoBehaviour {
        [Tooltip("Tỉ lệ độ nghiêng chia tốc độ")]
        public float inclinationPerSpeed = 1f;
        private float oldPosX;
        private SkeletonAnimation skeletonAnimation;
        private AnimationState spineAnimationState;
        private SkeletonData skeletonData;
        private Skeleton skeleton;
        private List<Animation> right = new List<Animation>();
        private List<Skin> skins = new List<Skin>();
        private float inclination = 0;

        public int MinRank { get; private set; }
        public int MaxRank { get; private set; }
        private int rank;
        public int Rank {
            get { return rank; }
            set {
                if (value < MinRank || value > MaxRank) Debug.LogError("Máy bay hiện tại không có rank " + value);
                rank = Mathf.Clamp(value, MinRank, MaxRank);
                skeleton.SetSkin(skins[rank - MinRank]);
            }
        }

        private void Start() {
            oldPosX = transform.position.x;
            skeletonAnimation = GetComponent<SkeletonAnimation>();
            spineAnimationState = skeletonAnimation.AnimationState;
            skeletonData = skeletonAnimation.SkeletonDataAsset.GetSkeletonData(false);
            skeleton = skeletonAnimation.Skeleton;
            LoadAnimation();
            LoadSkin();
#if UNITY_EDITOR
            string messSkin = "Min = " + MinRank + ", Max = " + MaxRank + ",  Skin: ";
            for (int i = 0; i < skins.Count; i++) {
                messSkin += skins[i].Name + " ";
            }
            Debug.Log(messSkin);

            string messAnim = "Animation: ";
            for (int i = 0; i < right.Count; i++) {
                messAnim += right[i].Name + " ";
            }
            Debug.Log(messAnim);
#endif
        }

        private Animation idle;
        private void LoadAnimation() {
            foreach (var animation in skeletonData.Animations) {
                if (animation.Name == "idle") {
                    idle = animation;
                } else if (animation.Name.StartsWith("right", StringComparison.OrdinalIgnoreCase)) {
                    right.Add(animation);
                }
            }
            right = right.OrderBy(o => o.Name).ToList();
            right.Insert(0, idle);//Để tiện cho tính toán idle được gán cho right0
        }
        private void LoadSkin() {
            skins = skeletonData.Skins.Where(s => s.Name.StartsWith("E", StringComparison.OrdinalIgnoreCase)).OrderBy(s => s.Name).ToList();
            MinRank = int.Parse(skins[0].Name.Substring(1));
            MaxRank = MinRank + skins.Count - 1;
        }

        private void Update() {
            var deltaX = transform.position.x - oldPosX;
            var speed = deltaX / Time.deltaTime;
            var deltaInclination = speed * inclinationPerSpeed * 0.01f;
            oldPosX = transform.position.x;
            if (Mathf.Abs(deltaX) > 0.01f) {
                if (inclination * deltaX < 0) {
                    deltaInclination *= 5;//Nếu player nghiêng và di chuyển khác phía nhau độ nghiêng sẽ thay đổi nhanh hơn
                }
                inclination += deltaInclination;
            } else {
                inclination = Fu.GoTo0(inclination, inclinationPerSpeed * 0.15f);
            }
            var index = Mathf.RoundToInt(Mathf.Clamp(Mathf.Abs(inclination), 0, right.Count - 1));
            skeleton.FlipX = inclination < 0;
            spineAnimationState.SetAnimation(0, right[index], true);
        }

#if UNITY_EDITOR
        //Test
        [Sirenix.OdinInspector.Button]
        public void ChangeRankTestEditor(int rank) {
            Rank = rank;
        }
#endif
    }
}
