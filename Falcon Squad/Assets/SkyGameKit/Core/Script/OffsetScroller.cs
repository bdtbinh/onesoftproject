﻿using UnityEngine;
using System.Collections;
namespace SkyGameKit {
    public class OffsetScroller : MonoBehaviour {

        public float scrollSpeed;
        private Vector2 savedOffset;
        private Renderer myRenderer;
        void Start() {
            myRenderer = GetComponent<Renderer>();
            savedOffset = myRenderer.sharedMaterial.GetTextureOffset("_MainTex");
        }

        void Update() {
            float y = Mathf.Repeat(Time.time * scrollSpeed, 1);
            Vector2 offset = new Vector2(savedOffset.x, y);
            myRenderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
        }

        void OnDisable() {
            myRenderer.sharedMaterial.SetTextureOffset("_MainTex", savedOffset);
        }
    }
}