﻿using System;
using UnityEngine;
namespace SkyGameKit
{
    public class ReflectiveLaser : MonoBehaviour
    {
        public LineRenderer[] lines;
        public Transform hitFx;
        public LayerMask layerMask;
        private static Vector2 bottomLeft;
        private static Vector2 topRight;
        public Action<LaserHitInfo> onHit;
        private void Awake()
        {
            if (bottomLeft.x == 0)
            {
                //Chỉ tính 1 lần
                CalculateCamera();
            }
        }

        public static void CalculateCamera()
        {
            topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.nearClipPlane));
            bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.nearClipPlane));
        }

        private void OnEnable()
        {
            if (Fu.IsNullOrEmpty(lines))
            {
                Debug.LogError("Không có LineRenderer");
                return;
            }
            for (int i = 0; i < lines.Length; i++) lines[i].positionCount = 2;
            positions = new Vector2[lines.Length + 1];
        }

        private const float MIN_ANGLE = 15;
        private Vector2 current;
        private float angle;
        private Vector2 direct;
        private Vector2[] positions;
        private int endPointIndex;
        private Collider2D lastCollider;
        private RaycastHit2D hit;
        private LaserHitInfo laserHitInfo;
        private void Update()
        {
            angle = Clamp0360(transform.rotation.eulerAngles.z);
            if (angle < MIN_ANGLE || angle > (360 - MIN_ANGLE) || (angle > (180 - MIN_ANGLE) && angle < (180 + MIN_ANGLE)))
            {
                //Các góc quá nhỏ không vẽ gì cả
                SetPositonForLine(true);
                return;
            }

            //Tính toán phản xạ
            current = positions[0] = transform.position;
            for (endPointIndex = 1; endPointIndex < positions.Length; endPointIndex++)
            {
                CalculateDirect();

                //Tính toàn va chạm
                hit = Physics2D.Raycast(current, direct, direct.magnitude, layerMask);
                if (hit.collider != null)
                {
                    positions[endPointIndex] = hit.point;
                    break;
                }
                else
                {
                    current += direct;
                    angle = Clamp0360(180 - angle);
                    positions[endPointIndex] = current;
                }

                //Kiểm tra vượt quá màn hình, thêm 0.1f để tránh trường hợp kết thúc ở sát sạt màn hình vẫn vẽ thêm 1 lần nữa
                if (current.y < bottomLeft.y + 0.1f || current.y > topRight.y - 0.1f) break;
            }

            if (hit.collider != null)
            {
                if (hit.collider == lastCollider)
                {
                    laserHitInfo.SetValue(Time.deltaTime, hit.point, hit.collider);
                    if (onHit != null) onHit(laserHitInfo);
                }
                else
                {
                    lastCollider = hit.collider;
                }
            }
            else
            {
                lastCollider = null;
            }

            SetPositonForLine();
        }

        private void CalculateDirect()
        {
            var directOne = Fu.RotateVector2(Vector2.right, angle);
            direct = directOne * ((directOne.x > 0 ? topRight.x : bottomLeft.x) - current.x) / directOne.x;
            var nextY = current.y + direct.y;
            if (nextY < bottomLeft.y || nextY > topRight.y)
            {
                direct = directOne * ((directOne.y > 0 ? topRight.y : bottomLeft.y) - current.y) / directOne.y;
            }
        }

        public static float Clamp0360(float eulerAngles)
        {
            float result = eulerAngles - Mathf.CeilToInt(eulerAngles / 360f) * 360f;
            if (result < 0)
            {
                result += 360f;
            }
            return result;
        }

        public void SetPositonForLine(bool clear = false)
        {
            if (clear)
            {
                for (int i = 0; i < lines.Length; i++) lines[i].positionCount = 0;
            }
            else
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    if (i < endPointIndex)
                    {
                        lines[i].positionCount = 2;
                        lines[i].SetPosition(0, positions[i]);
                        lines[i].SetPosition(1, positions[i + 1]);
                    }
                    else
                    {
                        lines[i].positionCount = 0;
                    }
                }
            }
            if (hitFx != null)
            {
                hitFx.position = positions[endPointIndex];
            }
        }
    }

    public struct LaserHitInfo
    {
        public float deltaTime;
        public Vector2 point;
        public Collider2D collider;

        public void SetValue(float deltaTime, Vector2 point, Collider2D collider)
        {
            this.deltaTime = deltaTime;
            this.point = point;
            this.collider = collider;
        }
    }
}
