﻿using PathologicalGames;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SkyGameKit
{
    public static class Fu
    {
        /// <summary>
        /// Kiểm tra xem con trỏ có đang ở trên UI không (chỉ dùng với canvas)
        /// </summary>
        /// <returns></returns>
        public static bool IsPointerOverUIObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        /// <summary>
        /// Sinh vụ nổ bằng Pool, nếu nó là particle nó sẽ tự mất
        /// </summary>
        /// <param name="explosion"></param>
        /// <param name="pos"></param>
        /// <param name="rot"></param>
        /// <returns></returns>
        public static Transform SpawnExplosion(GameObject explosion, Vector3 pos, Quaternion rot)
        {
            if (explosion != null)
            {
                ParticleSystem particleExplosion = explosion.GetComponent<ParticleSystem>();
                if (particleExplosion != null)
                {
                    return PoolManager.Pools[Const.explosiveName].Spawn(particleExplosion, pos, rot).transform;
                }
                else
                {
                    return PoolManager.Pools[Const.explosiveName].Spawn(explosion, pos, rot);
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Kiểm tra xem số đầu tiên có thuộc tập hợp các số tiếp theo không
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static bool TH(params int[] a)
        {
            int x = a[0];
            for (int i = 1; i < a.Length; i++)
            {
                if (x == a[i])
                {
                    return true;
                }
            }
            return false;
        }

        public static float GoTo0(float value, float speed)
        {
            speed = Math.Abs(speed);
            if (Math.Abs(value) <= speed) return 0;
            else return value + (value > 0 ? -speed : speed);
        }

        public static float GoToX(float x, float currentValue, float speed)
        {
            speed = Math.Abs(speed);
            var a = x - currentValue;
            if (Math.Abs(a) <= speed) return x;
            else return currentValue + (a < 0 ? -speed : speed);
        }

        /// <summary>
        /// Trả về k số tự nhiên ngẫu nhiên khác nhau trong khoảng 0..n 
        /// </summary>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static int[] RandomElementsArray(int k, int n)
        {
            int[] arr = Enumerable.Range(0, n + 1).ToArray();
            for (int i = 0; i < n; i++)
            {
                Swap(ref arr[UnityEngine.Random.Range(0, n)], ref arr[UnityEngine.Random.Range(0, n)]);
            }
            return arr.Take(k).ToArray();
        }

        /// <summary>
        /// Đổ giá trị 2 biến bất kỳ
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        /// <summary>
        /// Đổ vị trí 2 phần tử trong List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="indexA"></param>
        /// <param name="indexB"></param>
        /// <returns></returns>
        public static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
            return list;
        }

        /// <summary>
        /// Quay Vector2 một góc cho trước
        /// </summary>
        /// <param name="v"></param>
        /// <param name="degrees"></param>
        /// <returns></returns>
        public static Vector2 RotateVector2(Vector2 v, float degrees)
        {
            float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
            float tx = v.x;
            float ty = v.y;
            v.x = (cos * tx) - (sin * ty);
            v.y = (sin * tx) + (cos * ty);
            return v;
        }

        /// <summary>
        /// Trả về góc quay Quaternion theo hướng 1 Vector + 1 góc
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <param name="degreesOffset"></param>
        /// <returns></returns>
        public static Quaternion LockAt2D(Vector2 worldPosition, float degreesOffset = 0)
        {
            float rot_z = Mathf.Atan2(worldPosition.y, worldPosition.x) * Mathf.Rad2Deg;
            return Quaternion.Euler(0f, 0f, rot_z - 90 - degreesOffset);
        }

        /// <summary>
        /// Cho biết một mảng là null hoặc rỗng
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(Array array)
        {
            return (array == null || array.Length == 0);
        }

        /// <summary>
        /// Dịch chuyển path về vị trí đã chọn
        /// </summary>
        /// <param name="waypoints"></param>
        /// <param name="pointPosition"></param>
        /// <returns></returns>
        public static Vector3[] MovePathToPoint(Vector3[] waypoints, Vector3 pointPosition)
        {
            Vector3 offset = pointPosition - waypoints[0];
            if (offset.magnitude > 0.05f)
            {
                for (int k = 0; k < waypoints.Length; k++)
                {
                    waypoints[k] += offset;
                }
            }
            return waypoints;
        }
    }

    public class DisposableCoroutine : IDisposable
    {
        private readonly MonoBehaviour target;
        private readonly Coroutine routine;

        public DisposableCoroutine(MonoBehaviour target, Coroutine routine)
        {
            this.target = target;
            this.routine = routine;
        }

        public void Dispose()
        {
            target.StopCoroutine(routine);
        }
    }

    public static class ExtensionMethods
    {
        /// <summary>
        /// Dừng chạy khi gameObject bị disable, MonoBehaviour bị disable không ảnh hưởng
        /// </summary>
        public static IDisposable Delay<T>(this T t, float timeSec, Action action, bool ignoreTimeScale = false) where T : MonoBehaviour
        {
            if (!t.gameObject.activeInHierarchy) return null;
            if (action == null) return null;
            if (timeSec < 0)
            {
                return null;//Không làm gì cả khi thời gian < 0
            }
            else if (timeSec == 0)
            {
                action();//Thực hiện ngay lập tức nếu thời gian bằng 0
                return null;
            }
            else
            {
                return new DisposableCoroutine(t, t.StartCoroutine(DelayCoroutine(timeSec, action, ignoreTimeScale)));
            }
        }

        private static IEnumerator DelayCoroutine(float timeSec, Action action, bool ignoreTimeScale = false)
        {
            if (ignoreTimeScale)
            {
                yield return new WaitForSecondsRealtime(timeSec);
            }
            else
            {
                yield return new WaitForSeconds(timeSec);
            }
            action();
        }

        public static IDisposable DoActionEveryTime<T>(this T t, float timeSec, int count, Action action, bool doOnStart = false, bool ignoreTimeScale = false) where T : MonoBehaviour
        {
            return t.DoActionEveryTime(timeSec, count, action, null, doOnStart, ignoreTimeScale);
        }
        public static IDisposable DoActionEveryTime<T>(this T t, float timeSec, Action action, Action compeleteAction, bool doOnStart = false, bool ignoreTimeScale = false) where T : MonoBehaviour
        {
            return t.DoActionEveryTime(timeSec, -1, action, compeleteAction, doOnStart, ignoreTimeScale);
        }
        public static IDisposable DoActionEveryTime<T>(this T t, float timeSec, Action action, bool doOnStart = false, bool ignoreTimeScale = false) where T : MonoBehaviour
        {
            return t.DoActionEveryTime(timeSec, -1, action, null, doOnStart, ignoreTimeScale);
        }
        public static IDisposable DoActionEveryTime<T>(this T t, float timeSec, int count, Action action, Action compeleteAction, bool doOnStart = false, bool ignoreTimeScale = false) where T : MonoBehaviour
        {
            if (action == null) return null;
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
            {
                timeSec = Mathf.Max(timeSec, 0.01f);
            }
            if (timeSec == 0 && count > 0)
            {
                for (int i = 0; i < count; i++) action();
                if (compeleteAction != null) compeleteAction.Invoke();
                return null;
            }
            else if (timeSec > 0)
            {
                if (doOnStart)
                {
                    count--;
                    action();
                }
                return new DisposableCoroutine(t, t.StartCoroutine(DoActionEveryTimeCoroutine(timeSec, count, action, compeleteAction, ignoreTimeScale)));
            }
            else
            {
                //Không làm gì cả nếu timeSec<0 hoặc timeSec==0 mà count<=0
                return null;
            }
        }

        private static IEnumerator DoActionEveryTimeCoroutine(float timeSec, int count, Action action, Action compeleteAction, bool ignoreTimeScale = false)
        {
            //Nếu count<0 sẽ lặp vô tận
            while (count != 0)
            {
                if (ignoreTimeScale)
                {
                    yield return new WaitForSecondsRealtime(timeSec);
                }
                else
                {
                    yield return new WaitForSeconds(timeSec);
                }
                if (count > 0) count--;
                action();
            }
            if (compeleteAction != null) compeleteAction.Invoke();

        }
    }
}
