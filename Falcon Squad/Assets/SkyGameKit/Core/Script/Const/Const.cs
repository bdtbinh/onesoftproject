﻿using PathologicalGames;
using UnityEngine;
namespace SkyGameKit {
    public static class Const {
        public const string enemyPoolName = "EnemyPool";
        public const string explosiveName = "ExplosionPool";
        public const string itemPoolName = "ItemPool";
        public const string pathPool = "PathPool";
        public const string deadZone = "DeadZone";

        public static Transform _enemyPool;
        public static Transform EnemyPoolTransform {
            get {
                if (_enemyPool == null) {
                    _enemyPool = PoolManager.Pools[enemyPoolName].transform;
                }
                return _enemyPool;
            }
        }

        public static Transform _explosionPool;
        public static Transform ExplosionPoolTransform {
            get {
                if (_explosionPool == null) {
                    _explosionPool = PoolManager.Pools[explosiveName].transform;
                }
                return _explosionPool;
            }
        }
        /// <summary>
        /// Khoảng chênh lệch với cạnh màn hình mà object đi quá sẽ bị đưa về pool
        /// </summary>
        public const float offsetCamera = 1f;
        
        public const int WAVE_ID_BASE = 1000000;
        public const int TURN_ID_BASE = 1000;
        public const int ENEMY_ID_BASE = 1;
        public static int GetWaveID(int index) {
            index++;
            return WAVE_ID_BASE * index;
        }        
        public static int GetTurnID(int index, int waveID) {
            index++;
            return waveID + TURN_ID_BASE * index;
        }
        public static int GetEnemyID(int index, int TurnID) {
            index++;
            return TurnID + index;
        }
    }
    public enum MoveMode {
        Touch,
        Relative
    }
}


