﻿using UnityEngine;
namespace SkyGameKit {
    public class PlaneState : StateMachineBehaviour {
        private float oldPosX;

        [Tooltip("Tỉ lệ độ nghiêng chia tốc độ")]
        public float inclinationPerSpeed = 0.008f;

        [Tooltip("Tốc độ qua về trạng thái cân bằng khi đứng yên")]
        public float goTo0Speed = 15;

        [Tooltip("Nếu nhỏ hơn tốc độ này máy bay sẽ tính là đứng yên")]
        public float minSpeed = 0.01f;

        // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
            oldPosX = animator.transform.position.x;
        }
        
        //OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
            //stateInfo

            var offsetX = animator.transform.position.x - oldPosX;
            var speed = offsetX / Time.deltaTime;
            var inclination = speed * inclinationPerSpeed;
            oldPosX = animator.transform.position.x;
            if (Mathf.Abs(speed) > minSpeed) {
                if (animator.GetFloat("PlaneState") * offsetX < 0) {
                    inclination *= 5;//Nếu player nghiêng và di chuyển khác phía nhau độ nghiêng sẽ thay đổi nhanh hơn
                }
                animator.SetFloat("PlaneState", animator.GetFloat("PlaneState") + inclination);
            } else {
                animator.SetFloat("PlaneState", FuCore.GoTo0(animator.GetFloat("PlaneState"), inclinationPerSpeed * goTo0Speed));
            }
            animator.SetFloat("PlaneState", Mathf.Clamp(animator.GetFloat("PlaneState"), -1, 1));
        }
    }
}
