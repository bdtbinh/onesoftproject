﻿using System.Collections.Generic;
using UnityEngine;
namespace SkyGameKit {
    public class SgkBullet : MonoBehaviour {
        public static List<SgkBullet> onScreenBulletList = new List<SgkBullet>();
        public int power;
        public GameObject explosionPrefab;
        /// <summary>
        /// Hàm này nổ, chỉ chạy đúng với UniBulletHell
        /// </summary>
        public virtual void Explosion() {
            if (gameObject.activeInHierarchy) {
                Fu.SpawnExplosion(explosionPrefab, transform.position, Quaternion.identity);
                UbhObjectPool.instance.ReleaseBullet(GetComponent<UbhBullet>());
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D c) {
            if (c.name.Equals(Const.deadZone)) {
                UbhObjectPool.instance.ReleaseBullet(GetComponent<UbhBullet>());
            }
        }

        private Vector3 bottomLeft;
        private Vector3 topRight;
        private int randomNumber;
        protected virtual void Awake() {
            bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.nearClipPlane));
            topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.nearClipPlane));
            randomNumber = Random.Range(0, 4);
        }

        protected virtual void Update() {
            //Check 5 frame 1 lần cho đỡ giật
            if (Time.frameCount % 5 == randomNumber) {
                if (transform.position.x > topRight.x + Const.offsetCamera ||
                    transform.position.y > topRight.y + Const.offsetCamera ||
                    transform.position.x < bottomLeft.x - Const.offsetCamera ||
                    transform.position.y < bottomLeft.y - Const.offsetCamera) {
                    UbhObjectPool.instance.ReleaseBullet(GetComponent<UbhBullet>());
                }
            }
        }
        private void OnEnable() {
            onScreenBulletList.Add(this);
        }
        private void OnDisable() {
            onScreenBulletList.Remove(this);
        }
    }
}
