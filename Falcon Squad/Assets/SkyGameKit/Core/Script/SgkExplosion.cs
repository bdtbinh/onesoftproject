﻿using PathologicalGames;
using UnityEngine;
namespace SkyGameKit {
    public class SgkExplosion : MonoBehaviour {
        [HideInInspector]
        public Transform target;
        [HideInInspector]
        public Vector2 offset;
        public void Despawn(float delay) {
            this.Delay(delay, () => OnAnimationExplosionFinish());
        }
        public void OnAnimationExplosionFinish() {
            if (PoolManager.Pools[Const.explosiveName].IsSpawned(transform)) {
                PoolManager.Pools[Const.explosiveName].Despawn(transform);
            }
        }
        private void Update() {
            if (target != null) {
                transform.position = target.TransformPoint(offset);
            }
        }
        private void OnDespawned() {
            target = null;
        }
    }
}
