﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
//https://forum.unity.com/threads/apply-changes-to-prefab-keyboard-shortcut.29251/
namespace SkyGameKit {
    public class ApplyPrefabChanges : MonoBehaviour {
        [MenuItem("Tools/Apply Prefab Changes %&a")]
        public static void DoApplyPrefabChanges() {
            DoApplyPrefabChanges(Selection.activeGameObject);
        }

        [MenuItem("Tools/Apply All Prefab Changes %&s")]
        public static void DoApplyAllPrefabChanges() {
            foreach (GameObject obj in Selection.gameObjects) {
                DoApplyPrefabChanges(obj);
            }
        }

        private static void DoApplyPrefabChanges(GameObject obj) {
            if (obj != null) {
                var prefab_root = PrefabUtility.FindPrefabRoot(obj);
                var prefab_src = PrefabUtility.GetCorrespondingObjectFromSource(prefab_root);
                if (prefab_src != null) {
                    PrefabUtility.ReplacePrefab(prefab_root, prefab_src, ReplacePrefabOptions.ConnectToPrefab);
                    Debug.Log("Updating prefab : " + AssetDatabase.GetAssetPath(prefab_src));
                } else {
                    Debug.Log(obj.name + " has no prefab");
                }
            } else {
                Debug.Log("Nothing selected");
            }
        }
    }
}
#endif