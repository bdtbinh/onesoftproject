﻿using Sirenix.OdinInspector;
using SWS;
using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace SkyGameKit {
    public abstract class BaseTurnManager : MonoBehaviour {
        [Tooltip("Tất cả enemy trong turn đi theo path này")]
        public PathManager path;

#if UNITY_EDITOR
        protected virtual void OnDrawGizmosSelected() {
            if (Selection.activeGameObject.GetComponent<LevelManager>() != null) return;//Không hiện khi chọn vào LevelManager
            ShowPathManager.ShowPathInScene(path, transform.position);
        }
#endif
    }

    [Serializable]
    public class MiniTurn {
        public GameObject enemyPrefab;
        [Tooltip("Các thuộc tính thêm của enemy")]
        public float[] properties;
        public PointAndAction[] pointAndAction;
        [Button, GUIColor(0, 1, 0, 1)]
        public virtual void LoadAction() {
            string[] listAcitonName = enemyPrefab.GetComponent<BaseEnemy>().GetListActionName();
            foreach (var item in pointAndAction) {
                item.listAcitonName = listAcitonName;
            }
        }
    }

    [Serializable]
    public class PointAndAction {
        [HideInInspector]
        public string[] listAcitonName;
        public static string[] listType = {
            "Point = X",
            "Started",
            "Dead by player",
            "Point mod X = 0, Point != 0",
            "Point = Index + X",
            "Point = LastPoint - Index + X",
            "Time Out",
            "After X Millisecond",
        };

        [ValueDropdown("listType")]
        public string type;

        [HideIf("HideX")]
        public int x;

        private bool HideX() {
            return type == listType[1] || type == listType[2] || type == listType[6];
        }

        [ValueDropdown("listAcitonName")]
        public string actionName;
    }
}
