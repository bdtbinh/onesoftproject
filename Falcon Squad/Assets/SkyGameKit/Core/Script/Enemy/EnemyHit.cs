﻿using UnityEngine;
namespace SkyGameKit {
    public class EnemyHit : MonoBehaviour {
        public BaseEnemy m_BaseEnemy;
        protected virtual void OnTriggerEnter2D(Collider2D col) {
            switch (col.tag) {
                case Const.deadZone:
                    m_BaseEnemy.Die(EnemyKilledBy.DeadZone);
                    break;
                case "PlayerBullet":
                    var bullet = col.GetComponent<SgkBullet>();
                    m_BaseEnemy.OnTakeDamage(bullet.power);
                    bullet.Explosion();
                    break;
            }
        }
    }
}
