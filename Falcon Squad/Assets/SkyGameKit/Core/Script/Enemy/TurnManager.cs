﻿using PathologicalGames;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
namespace SkyGameKit
{
    public class TurnManager : BaseTurnManager
    {
        [ValidateInput("ValidateGreaterOrEqual0", "Phải >=0, nếu <0 enemy sẽ không ra")]
        [Tooltip("Thời gian bắt đầu tính từ khi bắt đầu wave")]
        public float timeToBegin = 0;

        [Tooltip("Thời gian kết thúc tính từ khi bắt đầu turn")]
        public float timeToEnd = -1f;

        [ValidateInput("ValidateGreaterOrEqual0", "Phải >=0, nếu <0 enemy sẽ không ra")]
        [Tooltip("Thời gian cách nhau giữa các enemy")]
        public float timeToNextEnemy = 0.5f;

        [DisplayAsString]
        public int totalEnemy;

        [DisplayAsString]
        [ShowInInspector]
        private int _enemyRemain = -1;
        public int enemyRemain
        {
            get
            {
                return _enemyRemain;
            }
            set
            {
                _enemyRemain = value;
                if (value == 0 && isRunning)
                {
                    if (checkEndTurnByTime != null)
                    {
                        checkEndTurnByTime.Dispose();
                    }
                    EndTurn();
                }
            }
        }

        [ShowInInspector, DisplayAsString]
        public int uid;

        public Action endTurnEvent;
        public Action<BaseEnemy> OnInitEnemy;

        protected bool isRunning = false;
        public bool IsRunning
        {
            get
            {
                return isRunning;
            }
        }

        protected List<BaseEnemy> enemyList = new List<BaseEnemy>();
        public List<BaseEnemy> GetEnemyList
        {
            get
            {
                return enemyList;
            }
        }

        protected IDisposable checkEndTurnByTime;

        public virtual int StartCountDelayTurn()
        {
            if (enabled)
            {
#if UNITY_EDITOR
                IDisposable countTime = Observable.EveryFixedUpdate()
                      .Subscribe(x =>
                      {
                          float t = x * Time.fixedDeltaTime;
                          if (t < timeToBegin)
                          {
                              CustomHierarchyView.runningTurn[gameObject.GetInstanceID()] = timeToBegin - t;
                          }
                          else
                          {
                              if (timeToEnd > 0)
                              {
                                  CustomHierarchyView.runningTurn[gameObject.GetInstanceID()] = -(timeToBegin + timeToEnd - t);
                              }
                              else
                              {
                                  CustomHierarchyView.runningTurn[gameObject.GetInstanceID()] = 0;
                              }
                          }
                      }).AddTo(this);
                endTurnEvent += () =>
                {
                    countTime.Dispose();
                    CustomHierarchyView.runningTurn.Remove(gameObject.GetInstanceID());
                };
#endif
                this.Delay(timeToBegin, () => BeginTurn());
            }
            return totalEnemy;
        }

        protected virtual void BeginTurn()
        {
            enemyRemain = totalEnemy;
            isRunning = true;
            if (timeToEnd == 0) timeToEnd = -1;
            //test
            //if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
            //{
            //    timeToEnd = 8;
            //}
            //end test
            checkEndTurnByTime = this.Delay(timeToEnd, () => EndTurn());
        }

        protected virtual void EndTurn()
        {
            if (isRunning)
            {
                isRunning = false;
                if (enemyRemain > 0)
                {
                    for (int i = enemyList.Count - 1; i >= 0; i--)
                    {
                        if (PoolManager.Pools[Const.enemyPoolName].IsSpawned(enemyList[i].transform))
                        {
                            if (!enemyList[i].isRemoved)
                            {
                                enemyList[i].ForceRemove();
                            }
                        }
                    }
                }
                enemyList.Clear();
                transform.parent.GetComponent<WaveManager>().TurnRemain--;
                if (endTurnEvent != null) endTurnEvent();
                enabled = false;
            }
        }

        private int enemyIndex = 0;
        /// <summary>
        /// Khởi tạo các giá trị cho enemy
        /// </summary>
        /// <param name="enemy"></param>
        /// <param name="miniTurn"></param>
        protected virtual void InitEnemy(BaseEnemy enemy, MiniTurn miniTurn)
        {
            enemy.index = enemyIndex;
            enemy.uid = Const.GetEnemyID(enemy.index, uid);
            enemyIndex++;
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2 && ConnectManager.hashSetIdDieEnemey.ContainsKey(enemy.uid))
            {
                enemy.enemyCollisionBase.TakeDamage(enemy.currentHP);
                Debug.Log("enemy.uid : " + enemy.uid);
                return;
            }
            enemy.properties = miniTurn.properties;
            enemy.Restart();
            enemy.SetPathAndAction(path, miniTurn.pointAndAction, timeToNextEnemy > 0 ? 0 : totalEnemy - enemyIndex + 2);//Vì các enemy ra cách nhau 1 frame nên con đẻ trước phải đánh số cao hơn.
            enemyList.Remove(enemy);//Tránh lỗi do pool dùng lại
            enemyList.Add(enemy);
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2 && !ConnectManager.dictionaryEnemy.ContainsKey(enemy.uid))
            {
                ConnectManager.dictionaryEnemy.Add(enemy.uid, enemy);
            }
            if (OnInitEnemy != null) OnInitEnemy(enemy);
        }

        private bool ValidateGreaterOrEqual0(float number)
        {
            return number >= 0;
        }
    }
}