﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;

namespace SkyGameKit
{
    public class WaveManager : MonoBehaviour, IWaveManager
    {
        [Tooltip("True chạy các turn cùng 1 lúc, false chạy các turn từng frame một")]
        public bool StartTurnAsync = false;
        public Action OnStartWave { get; set; }
        public Action OnEndWave { get; set; }
        private int turnRemainTmp = -1;
        [ShowInInspector]
        [DisplayAsString]
        public int TurnRemain
        {
            get
            {
                return turnRemainTmp;
            }
            set
            {
                turnRemainTmp = value;
                if (TurnRemain == 0 && IsRunning)
                {
                    EndWave();
                    IsRunning = false;
                }
            }
        }
        public bool IsRunning { get; private set; }
        public bool IsDelayWave { get; private set; }

        [ShowInInspector, DisplayAsString]
        public int uid;
        public TurnManager[] mTM;

        private void Awake()
        {
            IsRunning = false;
            mTM = GetComponentsInChildren<TurnManager>();
            TurnRemain = mTM.Length;
            if (TurnRemain == 0)
            {
                IsDelayWave = true;
            }
        }

        public virtual void StartWave()
        {
            if (OnStartWave != null) OnStartWave();
            uid = Const.GetWaveID((LevelManagerCore.Instance as LevelManager).currentWave);
            if (!IsDelayWave)
            {
                IsRunning = true;
                StartCoroutine(StartAllTurn());
            }
        }

        public IEnumerator StartAllTurn()
        {
            for (int i = 0; i < mTM.Length; i++)
            {
                mTM[i].uid = Const.GetTurnID(i, uid);
                mTM[i].StartCountDelayTurn();
                if (StartTurnAsync) yield return null;//Chạy từng frame cho đỡ giật;
            }
        }

        public virtual void EndWave()
        {
            LevelManager.Instance.NextWave();
            if (OnEndWave != null) OnEndWave();
        }
    }
}