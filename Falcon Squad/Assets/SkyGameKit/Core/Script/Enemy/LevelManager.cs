﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace SkyGameKit
{
    public class LevelManager : LevelManagerCore
    {

        public static List<BaseEnemy> aliveEnemy = new List<BaseEnemy>();
        public static Action<int> OnNextWave;
        public Action<int> OnStartWave;
#if UNITY_EDITOR
        [DisplayAsString]
        public int m_AliveEnemy;
        [DisplayAsString]
        public int m_TotalEnemyHp;
#endif
        [DisplayAsString]
        public int currentWave = -1;

        protected override void Awake()
        {
            base.Awake();
            //Reset biến static mỗi khi bắt đầu một scene mới            
            aliveEnemy.Clear();
            waveWithTime = null;
        }

        protected override void Start()
        {
            currentWave = -1;
            base.Start();
        }

#if UNITY_EDITOR
        protected virtual void Update()
        {
            m_AliveEnemy = aliveEnemy.Count;
            m_TotalEnemyHp = 0;
            foreach (var enemy in aliveEnemy)
            {
                m_TotalEnemyHp += enemy.currentHP;
            }
        }
#endif

        protected override IEnumerator StartWaveOnFirstUpdate()
        {
            yield return null;
            if (waveWithTime == null)
            {
                NextWave();
            }
            else
            {
                //đã gọi ở chỗ khác
                //NextWaveTime();
            }
        }

        private IDisposable countWaveTimeStream;
        private System.Tuple<WaveManager, float> currentWaveWithTime;
        private Queue<System.Tuple<WaveManager, float>> waveWithTime = null;
        /// <summary>
        /// Thêm wave vào level, khi hết thời gian waveTime sẽ chuyển wave tiếp theo (ko theo logic cũ)
        /// </summary>
        public void AddWave(WaveManager wave, float waveTime)
        {
            if (waveTime > 0)
            {
                if (waveWithTime == null)
                {
                    waveWithTime = new Queue<System.Tuple<WaveManager, float>>();
                }
                waveWithTime.Enqueue(new System.Tuple<WaveManager, float>(wave, waveTime));
            }
            else
            {
                AddWave(wave);
            }
        }

        public void AddWave(WaveManager wave)
        {
            wave.gameObject.transform.parent = transform;
            IWaveManager[] tmp = mWM;
            if (tmp == null) tmp = new IWaveManager[0];
            mWM = new IWaveManager[tmp.Length + 1];
            for (int i = 0; i < tmp.Length; i++)
                mWM[i] = tmp[i];
            mWM[tmp.Length] = wave;
        }

        public WaveManager GetWave(int index)
        {
            return (WaveManager)mWM[index];
        }

        private IDisposable countEnemy;
        public override void NextWave()
        {
            if (waveWithTime == null)
            {
                if (OnNextWave != null) OnNextWave(currentWave);
                if (currentWave + 1 < mWM.Length)
                {
                    currentWave++;
                    mWM[currentWave].StartWave();
                    if (OnStartWave != null) OnStartWave(currentWave);
                }
                else
                {
                    //Hết wave đến số enemy còn lại để báo kết thúc
                    countEnemy = Observable.EveryUpdate().Subscribe(x =>
                    {
                        if (aliveEnemy.Count == 0)
                        {
                            countEnemy.Dispose();
                            EndLevel();
                        }
                    }).AddTo(this);
                }
            }
        }

        public override void NextWaveTime()
        {
            if (waveWithTime != null)
            {
                if (OnNextWave != null) OnNextWave(currentWave);
                if (waveWithTime.Count > 0)
                {
                    currentWave++;
                    currentWaveWithTime = waveWithTime.Dequeue();
                    currentWaveWithTime.Item1.StartWave();
                    if (OnStartWave != null) OnStartWave(currentWave);
                    if (currentWaveWithTime.Item2 > 0)
                    {
                        if (countWaveTimeStream != null)
                        {
                            countWaveTimeStream.Dispose();
                        }
                        countWaveTimeStream = this.Delay(currentWaveWithTime.Item2, NextWaveTime, true);
                    }
                }
                else
                {
                    EndLevel();
                }
            }
        }

        public override void EndLevel()
        {
            if (GameState == GameStateType.Playing)
            {
                if (OnGameStateChange != null) OnGameStateChange(GameStateType.Victory);
            }
#if UNITY_EDITOR
            print("End game: VICTORY");
            foreach (var item in enemyKilled)
            {
                print(item.Key + " ** " + item.Value);
            }
#endif
        }
        public static void OnEnemySpawned(string id)
        {
            if (enemySpawned.ContainsKey(id))
            {
                enemySpawned[id]++;
            }
            else
            {
                enemySpawned.Add(id, 1);
            }
        }

        public static void OnEnemyDie(string id, int enemyScore)
        {
            if (enemyKilled.ContainsKey(id))
            {
                enemyKilled[id]++;
            }
            else
            {
                enemyKilled.Add(id, 1);
            }
            lastTimeEnemyDie = Time.time;
            Score += enemyScore;
        }

        public override IWaveManager GetCurrentWave()
        {
            if (waveWithTime == null)
            {
                if (currentWave >= 0 && currentWave < mWM.Length)
                {
                    return mWM[currentWave];
                }
                else
                {
                    return mWM[0];
                }
            }
            else
            {
                if (currentWaveWithTime != null)
                {
                    return currentWaveWithTime.Item1;
                }
                else
                {
                    return null;
                }
            }
        }

        private void OnDestroy()
        {
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2)
            {
                ConnectManager.dictionaryEnemy.Clear();
                ConnectManager.dictionaryCurrentHPEnemy.Clear();
                ConnectManager.hashSetIdDieEnemey.Clear();

            }
        }
    }
}