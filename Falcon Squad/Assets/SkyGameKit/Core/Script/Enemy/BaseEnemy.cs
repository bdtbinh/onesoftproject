﻿using PathologicalGames;
using SWS;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using System.Collections;
using Mp.Pvp;

namespace SkyGameKit
{
    [RequireComponent(typeof(splineMove))]
    public abstract class BaseEnemy : MonoBehaviour
    {
        public EnemyCollisionBase enemyCollisionBase;
        public string ID;
        [ShowInInspector, DisplayAsString]
        public int uid;
        [Tooltip("Điểm số của enemy")]
        public int score;
        public int maxHP;
        [DisplayAsString]
        public int currentHP;
        [DisplayAsString]
        public int index;
        public GameObject dieExplosion;
        public bool fxDie;

        [HideInInspector]
        public UnityEvent deadEvent = new UnityEvent();
        [HideInInspector]
        public UnityEvent forceRemoveEvent = new UnityEvent();

        public List<EnemyAction> m_Action = new List<EnemyAction>();
        public string[] GetListActionName()
        {
            string[] listAcitonName = new string[m_Action.Count];
            for (int i = 0; i < m_Action.Count; i++)
            {
                listAcitonName[i] = m_Action[i].displayName;
            }
            return listAcitonName;
        }

        private void OnEnable()
        {
            if (enemyCollisionBase == null)
            {
                enemyCollisionBase = GetComponent<EnemyCollisionBase>();
            }
        }

        public UnityEvent GetActionByName(string name)
        {
            foreach (var item in m_Action)
            {
                if (item.displayName == name)
                {
                    return item.action;
                }
            }
            return null;
        }

        public virtual void SetPathAndAction(PathManager path, PointAndAction[] pointAndAction, int framePauseAfterSetPath = 0)
        {
            if (path != null)
            {
                SetPath(path, framePauseAfterSetPath);
            }
            foreach (var item in pointAndAction)
            {
                SetAction(item.type, item.x, item.actionName);
            }
        }

        bool isHp;
        int hpFromServer;

        bool isDie;

        public void SetDie()
        {
            isDie = true;
        }

        public void SetHPFromServer(int hp)
        {
            isHp = true;
            hpFromServer = hp;
        }

        public void Update()
        {
            if (isDie)
            {
                isDie = false;
                enemyCollisionBase.TakeDamage(currentHP, true);
            }
            if (isHp)
            {
                isHp = false;
                enemyCollisionBase.TakeDamage(hpFromServer, true);
            }
        }

        /// <summary>
        /// Gán các action
        /// </summary>
        /// <param name="x"></param>
        /// <param name="actionName"></param>
        public virtual void SetAction(string type, int x, string actionName)
        {
            UnityEvent action = GetActionByName(actionName);
            if (action == null)
            {
                Debug.LogError("Không thấy action với tên: " + actionName);
                return;
            }
            int lastPoint = M_SplineMove.pathContainer.GetWaypointCount() - 1;
            if (type == PointAndAction.listType[0])
            {
                if (x < 0) x = 0;
                if (x > lastPoint) x = lastPoint;
                M_SplineMove.events[x].AddListener(() => action.Invoke());
            }
            else if (type == PointAndAction.listType[1])
            {
                action.Invoke();
            }
            else if (type == PointAndAction.listType[2])
            {
                deadEvent.AddListener(() => action.Invoke());
            }
            else if (type == PointAndAction.listType[3])
            {
                int i = 1;
                while (i * x <= lastPoint)
                {
                    M_SplineMove.events[i * x].AddListener(() => action.Invoke());
                    i++;
                }
            }
            else if (type == PointAndAction.listType[4])
            {
                M_SplineMove.events[index + x].AddListener(() => action.Invoke());
            }
            else if (type == PointAndAction.listType[5])
            {
                M_SplineMove.events[lastPoint - index + x].AddListener(() => action.Invoke());
            }
            else if (type == PointAndAction.listType[6])
            {
                forceRemoveEvent.AddListener(() => action.Invoke());
            }
            else
            {
                this.Delay(x / 1000, () => action.Invoke());
            }
        }

        /// <summary>
        /// Đánh dấu enemy đã bị ForceRemove, không phải bị chết rồi
        /// </summary>
        [HideInInspector]
        public bool isRemoved;

        private splineMove smtmp;//Chỉ là biến để get set đừng đụng vào
        public splineMove M_SplineMove
        {
            get
            {
                if (smtmp == null)
                {
                    smtmp = GetComponent<splineMove>();
                }
                return smtmp;
            }
            set
            {
                smtmp = value;
            }
        }

        [HideInInspector]
        public float[] properties;

        /// <summary>
        /// Gọi hàm này khi hết thời gian mà enemy chưa chết
        /// </summary>
        public virtual void ForceRemove()
        {
            isRemoved = true;
            forceRemoveEvent.Invoke();
        }

        /// <summary>
        /// Chạy mỗi khi enemy được lấy từ pool ra
        /// </summary>
        public virtual void Restart()
        {
            if (maxHP > 0)
            {
                currentHP = maxHP;
            }
            else
            {
                Debug.LogError("Máu enemy <= 0, tự gán = 5");
                currentHP = 5;
            }
            if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2 && ConnectManager.dictionaryCurrentHPEnemy.ContainsKey(uid))
            {
                currentHP = ConnectManager.dictionaryCurrentHPEnemy[uid];
            }
            fxDie = false;
            isRemoved = false;
            LevelManager.aliveEnemy.Add(this);
            LevelManager.OnEnemySpawned(ID);
            forceRemoveEvent.RemoveAllListeners();
            deadEvent.RemoveAllListeners();
            M_SplineMove.events.Clear();
        }

        Vector3 tempVector3;

        /// <summary>
        /// Chạy mỗi khi enemy chết, các hàm override cần chạy lại hàm này
        /// </summary>
        public virtual void Die(EnemyKilledBy type = EnemyKilledBy.Player)
        {
            try
            {
                if (GameContext.modeGamePlay == GameContext.ModeGamePlay.PVP2vs2 && !AllPlayerManager.Instance.playerCampaign.isTestingMode)
                {
                    ConnectManager.Instance.SendData(CachePvp.ENEMY_DIE, tempVector3, uid, score, currentHP);
                    if (!ConnectManager.hashSetIdDieEnemey.ContainsKey(uid))
                    {
                        ConnectManager.hashSetIdDieEnemey.Add(uid, uid);
                    }
                }
            }
            catch (Exception)
            {

            }
            if (PoolManager.Pools[Const.enemyPoolName].IsSpawned(transform))
            {
                try
                {
                    transform.parent.GetComponent<TurnManager>().enemyRemain--;
                    Fu.SpawnExplosion(dieExplosion, transform.position, Quaternion.identity);
                    if (type == EnemyKilledBy.Player)
                    {
                        LevelManager.OnEnemyDie(ID, score);
                        deadEvent.Invoke();
                    }
                    if (PoolManager.Pools[Const.pathPool].IsSpawned(newPathTransform))
                    {
                        PoolManager.Pools[Const.pathPool].Despawn(newPathTransform);
                    }
                    M_SplineMove.Stop();
                    PoolManager.Pools[Const.enemyPoolName].Despawn(transform, Const.EnemyPoolTransform);
                    LevelManager.aliveEnemy.Remove(this);
                }
                catch (Exception)
                {

                }
            }
        }
        public virtual void Die(int type)
        {
            Die((EnemyKilledBy)type);
        }
        protected Transform newPathTransform;

        /// <summary>
        /// Gán path và chạy luôn
        /// </summary>
        /// <param name="path"></param>
        public virtual void SetPath(PathManager path, int framePauseAfterSetPath = 0)
        {
            newPathTransform = PoolManager.Pools[Const.pathPool].Spawn(
                path.transform,
                transform.position,
                path.transform.rotation
                );
            var bezier = newPathTransform.GetComponent<BezierPathManager>();
            if (bezier != null)
            {
                bezier.CalculatePath();
                M_SplineMove.SetPath(bezier);
            }
            else
            {
                var newPathManager = newPathTransform.GetComponent<PathManager>();
                M_SplineMove.SetPath(newPathManager);
            }
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine(PauseAndResumeMoveAfterFrame(M_SplineMove.speed, framePauseAfterSetPath));
            }
        }

        protected virtual IEnumerator PauseAndResumeMoveAfterFrame(float startSpeed, int frame = 0)
        {
            if (frame > 0)
            {
                M_SplineMove.ChangeSpeed(0.01f);
                for (int i = 0; i < frame; i++)
                {
                    yield return null;
                }
                M_SplineMove.ChangeSpeed(startSpeed);
                RestartAnimator();
            }
        }

        protected virtual void RestartAnimator()
        {

        }

        public virtual void OnTakeDamage(int damage)
        {
            currentHP -= damage;
            if (currentHP <= 0)
            {
                Die(EnemyKilledBy.Player);
            }
        }

        //GUIStyle guiStyle = new GUIStyle();

        //private void OnGUI()
        //{
        //    if (gameObject.activeInHierarchy)
        //    {
        //        guiStyle.fontSize = (int)(Screen.width / 36f);
        //        guiStyle.normal.textColor = Color.red;
        //        Vector2 worldNamePos = MainScene.Instance.cameraTk2d.WorldToScreenPoint(transform.position);
        //        GUI.Label(new Rect(worldNamePos.x - 20, Screen.height - worldNamePos.y + 20, 100, 20), uid + " - " + score, guiStyle);
        //    }
        //}
    }

    public enum EnemyKilledBy
    {
        Player,
        DeadZone,
        TimeOut,
        Server
    }
}
