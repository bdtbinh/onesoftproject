﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SkyGameKit {
    [Serializable]
    public class EnemyAction {
        public string displayName = "";
        public UnityEvent action;
    }
}
