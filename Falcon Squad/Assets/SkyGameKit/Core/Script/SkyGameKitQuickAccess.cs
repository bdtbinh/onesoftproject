﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
namespace SkyGameKit.QuickAccess {
    public class Player {
        public const string playerTag = "Player";
        private static GameObject _playerGameObject;
        /// <summary>
        /// Gameobject của player được tìm bằng tag "Player"
        /// </summary>
        public static GameObject gameObject {
            get {
                if (_playerGameObject == null) {
#if UNITY_EDITOR
                    var allPlayerTag = GameObject.FindGameObjectsWithTag(playerTag);
                    if (allPlayerTag.Length == 0) {
                        Debug.LogError("Không thấy GameObject có tag " + playerTag);
                    } else if (allPlayerTag.Length > 1) {
                        Debug.LogError("Có nhiều hơn 1 GameObject có tag " + playerTag);
                    }
#endif
                    _playerGameObject = GameObject.FindWithTag(playerTag);
                }
                return _playerGameObject;
            }
        }

        public static Transform transform {
            get {
                if (gameObject == null) return null;
                else return gameObject.transform;
            }
        }

        private static APlayerAttack _attack;
        public static APlayerAttack Attack {
            get {
                if (_attack == null) {
                    _attack = GetPlayerComponent<APlayerAttack>();
                }
                return _attack;
            }
        }
        private static APlayerMove _move;
        public static APlayerMove Move {
            get {
                if (_move == null) {
                    _move = GetPlayerComponent<APlayerMove>();
                }
                return _move;
            }
        }
        private static APlayerState _state;
        public static APlayerState State {
            get {
                if (_state == null) {
                    _state = GetPlayerComponent<APlayerState>();
                }
                return _state;
            }
        }
        private static APlayerHealth _health;
        public static APlayerHealth Health {
            get {
                if (_health == null) {
                    _health = GetPlayerComponent<APlayerHealth>();
                }
                return _health;
            }
        }
        /// <summary>
        /// Lấy Component của player hoặc con dưới 1 lớp của player
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetPlayerComponent<T>() {
            if (gameObject == null) return default(T);
            T component = gameObject.GetComponent<T>();
            if (component == null) {
                component = gameObject.GetComponentInChildren<T>();
            }
            return component;
        }
    }

    public abstract class APlayerAttack : MonoBehaviour {
        public virtual bool LockShot { get; set; }
        /// <summary>
        /// Gọi sau khi thay đổi cấp độ đạn, Tham số là cấp độ đạn đã thay đổi
        /// </summary>
        public Action<int> onBulletLevelChange;
        private int _bulletLevel = 0;
        public virtual int BulletLevel {
            get { return _bulletLevel; }
            set {
                var valueChange = value - _bulletLevel;
                _bulletLevel = value;
                if (onBulletLevelChange != null) onBulletLevelChange(valueChange);
            }
        }
        public Action<string> onUseSkill;
        public virtual void UseSkill(string skillName = "default") {
            if (onUseSkill != null) onUseSkill(skillName);
        }
    }
    public abstract class APlayerMove : MonoBehaviour {
        public float speed;
        public MoveMode moveMode = 0;
        private bool _lockMove = false;
        public Action onLockMoveChange;
        public virtual bool LockMove {
            get { return _lockMove; }
            set {
                _lockMove = value;
                if (onLockMoveChange != null) onLockMoveChange();
            }
        }
        public virtual Tweener AutoMove(Vector2 s, Vector2 d, float t = 1f, TweenCallback onComplete = null) {
            transform.position = new Vector3(s.x, s.y, transform.position.z);
            return AutoMove(d, t, onComplete);
        }
        public abstract Tweener AutoMove(Vector2 destination, float t = 1f, TweenCallback onComplete = null);
    }
    public abstract class APlayerState : MonoBehaviour {
        public virtual void PickUpItem(SgkItem item) {
            if (onPickUpItem != null) onPickUpItem(item);
        }
        public Action<SgkItem> onPickUpItem;
        public float itemActiveTime;
        private int _star = 0;
        /// <summary>
        /// Gọi sau khi số sao thay đổi, Tham số là số lượng sao đã thay đổi
        /// </summary>
        public Action<int> onAddStar;
        public virtual int Star {
            get { return _star; }
            set {
                var valueChange = value - _star;
                _star = value;
                if (onAddStar != null) onAddStar(valueChange);
            }
        }
        public abstract void SetShieldActive(float activeTime = 8f);
    }
    public abstract class APlayerHealth : MonoBehaviour {
        public int maxHP;
        private int _currentHP;
        /// <summary>
        /// Gọi sau khi số máu thay đổi, Tham số là số lượng máu đã thay đổi, nếu mất máu tham số sẽ âm
        /// </summary>
        public Action<int> onCurrentHPChange;
        public virtual int CurrentHP {
            get { return _currentHP; }
            set {
                var valueChange = value - _currentHP;
                _currentHP = value;
                if (onCurrentHPChange != null) onCurrentHPChange(valueChange);
            }
        }
        public bool playerCanTakeDamage;
        public int armor;
        public virtual void Respawn() {
        }
    }
}
