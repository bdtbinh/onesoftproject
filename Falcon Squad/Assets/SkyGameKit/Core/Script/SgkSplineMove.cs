﻿using SWS;
using UnityEngine;
namespace SkyGameKit {
    public class SgkSplineMove : MonoBehaviour {
        public BezierPathManager bezierPath;
        private BezierPoint bp0, bp1;
        public float speed = 1f;
        public int CurrentPoint { get; private set; }
        private float t = 0;
        CatmullRom cp;
        private void Start() {
            CurrentPoint = -1;
            NextPoint();
        }

        private void Update() {
            if (t < 1) {
                t += speed * Time.deltaTime;
                transform.position = cp.GetPoint2(t);
            } else {
                if (CurrentPoint < bezierPath.bPoints.Count - 2) {
                    NextPoint();
                }
            }
        }

        private void NextPoint() {
            CurrentPoint++;
            bp0 = bezierPath.bPoints[CurrentPoint];
            bp1 = bezierPath.bPoints[CurrentPoint + 1];
            cp = new CatmullRom(bp0.wp.position, bp0.cp[1].position, bp1.cp[0].position, bp1.wp.position);
            t = 0;
        }
    }
}
