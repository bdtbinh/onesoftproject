﻿using PathologicalGames;
using UnityEngine;
using Sirenix.OdinInspector;
using UniRx;
using SkyGameKit.QuickAccess;
using System;

namespace SkyGameKit {
    public class SgkItem : MonoBehaviour {
        public string itemName;
        public ItemType itemType = ItemType.Star;
        public int value;
        public string description;
        public virtual void Despawn() {
            if (PoolManager.Pools[Const.itemPoolName].IsSpawned(transform)) {
                PoolManager.Pools[Const.itemPoolName].Despawn(transform);
            }
        }
        public virtual void OnItemHitPlayer() {
            Player.State.PickUpItem(this);
        }
    }

    public enum ItemType {
        Star,
        PowerUp,
        Weapon,
    }
}
