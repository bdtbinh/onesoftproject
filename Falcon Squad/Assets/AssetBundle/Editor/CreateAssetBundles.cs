﻿#if UNITY_EDITOR
using System.IO;
using UnityEditor;

public class CreateAssetBundles {
    [MenuItem("Assets/Build Asset Bundles/Android")]
    static void BuildAllAssetBundlesAndroid() {
        string assetBundleDirectory = "Assets/AssetBundle/Build/Android";
        if (!Directory.Exists(assetBundleDirectory)) {
            Directory.CreateDirectory(assetBundleDirectory);
        }
        BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, BuildTarget.Android);
    }

    [MenuItem("Assets/Build Asset Bundles/IOS")]
    static void BuildAllAssetBundlesIOS() {
        string assetBundleDirectory = "Assets/AssetBundle/Build/IOS";
        if (!Directory.Exists(assetBundleDirectory)) {
            Directory.CreateDirectory(assetBundleDirectory);
        }
        BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, BuildTarget.iOS);
    }

    [MenuItem("Assets/Build Asset Bundles/Windows")]
    static void BuildAllAssetBundlesWindows() {
        string assetBundleDirectory = "Assets/AssetBundle/Build/Windows";
        if (!Directory.Exists(assetBundleDirectory)) {
            Directory.CreateDirectory(assetBundleDirectory);
        }
        BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
    }
}
#endif