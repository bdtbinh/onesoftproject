﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

public class DownloadAssetBundle {
    private const string defaultUrl = "http://battlecity.io/";
    private const string dataFile = "pvpdata";
    private const string versionFile = "pvpversion";
    private static bool isRunning = false;
    private readonly string Url;
    private UnityWebRequest uwr;
    public ulong DownloadedBytes {
        get {
            if (uwr == null) return 0;
            if (uwr.downloadedBytes < 0) return 0;
            return uwr.downloadedBytes;
        }
    }
    public float DownloadProgress {
        get {
            if (uwr == null) return 0;
            if (uwr.downloadProgress < 0) return 0;
            return uwr.downloadProgress;
        }
    }
    public string Error { get; private set; }
    public bool IsDone { get; private set; }
    public bool UseBuildin { get; private set; }
    public List<WaveCacheManager> Result { get; private set; }

    public DownloadAssetBundle(string Url) {
        this.Url = Url;
    }

    public float CompleteTime { get; private set; }
    private float startTime = 0;
    public IEnumerator Start() {
        if (isRunning) {
            Debug.Log("<color=#fb8c00>DownloadAssetBundle: </color> Is Running");
            yield break;
        }
        isRunning = true;
        startTime = Time.time;
        Uri baseUri = new Uri(string.IsNullOrEmpty(Url) ? defaultUrl : Url);
        Uri dataUri = new Uri(baseUri, dataFile);
        Uri versionUri = new Uri(baseUri, versionFile);

        //Lấy phiên bản của data
        uint version = 1;
        using (WWW www = new WWW(versionUri.ToString())) {
            yield return www;
            if (!string.IsNullOrEmpty(www.error)) {
                Error = www.error;
                Debug.LogError("<color=#fb8c00>DownloadAssetBundle: </color>" + Error);
                isRunning = false;
                yield break;
            } else {
                if (!uint.TryParse(www.text, out version)) {
                    Error = "Parse version error";
                    Debug.LogError("<color=#fb8c00>DownloadAssetBundle: </color>" + Error);
                    isRunning = false;
                    yield break;
                }
            }
        }

        //Chờ cache sẵn sàng
        while (!Caching.ready) {
            yield return null;
        }

        //So sánh Application version với version lấy từ server
        uint appVersion = uint.Parse(Regex.Replace(Application.version, "[^0-9]+", "")) * 1000;
        if (appVersion > version) {
            Result = null;
            IsDone = true;
            Debug.Log("<color=#fb8c00>DownloadAssetBundle: </color>appVersion > version");
            UseBuildin = true;
            isRunning = false;
            yield break;
        }

        Result = new List<WaveCacheManager>();
        //Tải data
        using (uwr = UnityWebRequestAssetBundle.GetAssetBundle(dataUri.ToString(), version, 0)) {
            yield return uwr.SendWebRequest();
            CompleteTime = Time.time - startTime;
            Debug.Log("<color=#fb8c00>DownloadAssetBundle: </color>Complete download time: " + (Time.time - startTime));

            if (uwr.isNetworkError || uwr.isHttpError) {
                Error = uwr.error;
            } else {
                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);
                string[] allAssetNames = bundle.GetAllAssetNames();
                Array.Sort(allAssetNames, StringComparer.Ordinal);
                foreach (var assetName in allAssetNames) {
                    GameObject tmpGo = bundle.LoadAsset<GameObject>(assetName);
                    Result.Add(tmpGo.GetComponent<WaveCacheManager>());
                }
                IsDone = true;
				CachePvp.PvpVersion = (int)version;
				new CSClientInfo().Send();
                yield return null;
            }
        }
        isRunning = false;
    }
}
