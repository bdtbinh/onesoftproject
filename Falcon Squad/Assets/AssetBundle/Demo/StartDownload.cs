﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartDownload : MonoBehaviour {
    public static DownloadAssetBundle dab = new DownloadAssetBundle(null);
    private IEnumerator Start() {
        yield return dab.Start();
        if (dab.IsDone) {
            SceneManager.LoadScene("PVPLevel");
        } else {
            Debug.LogError(dab.Error);
        }
    }
}
