﻿using System.Collections.Generic;
namespace SkyGameKit.Multiplayer {
    public struct PlayerInfo {
        public string id;
        public string facebookId;
        public string name;
        public string country;

        // tổng số trận thắng
        public int win;

        // tổng số trận thua
        public int lose;

        public float winrate {
            get {
                if (win == 0) return 0;
                else return (float)win / (win + lose);
            }
        }

        public int bonus {
            get {
                return win % 10;
            }
        }

        // điểm kinh nghiệm, mỗi trận thắng sẽ được cộng số điểm kinh nghiệm theo kịch bản
        public int exp;

        // số sao (medal)
        public int medal;

        public int ranking;

        public int status;
        public bool offline;

        public int score;
        public int wave;

        public void Import(Dictionary<string, object> player) {
            id = player["id"].ToString();
            if (player.ContainsKey("facebookId")) facebookId = player["facebookId"].ToString();
            var firstName = "";
            var lastName = "";
            if (player.ContainsKey("firstName")) firstName = player["firstName"].ToString();
            if (player.ContainsKey("lastName")) lastName = player["lastName"].ToString();
            if (!string.IsNullOrEmpty(firstName + lastName)) {
                name = firstName + " " + lastName;
            }
            if (player.ContainsKey("country")) country = player["country"].ToString();
            if (player.ContainsKey("ranking")) ranking = int.Parse(player["ranking"].ToString());
            win = int.Parse(player["win"].ToString());
            lose = int.Parse(player["lose"].ToString());
            exp = int.Parse(player["exp"].ToString());
            medal = int.Parse(player["medal"].ToString());
        }

        /// <summary>
        /// Sau khi kết thúc game biến này sẽ được cập nhật
        /// </summary>
        public EndGameResult endGame;

        public void ImportResult(Dictionary<string, object> playerResult) {
            if (id == playerResult["accountId"].ToString()) {
                endGame.state = (EndGamePlayerState)int.Parse(playerResult["state"].ToString());
                endGame.exp = int.Parse(playerResult["exp"].ToString());
                exp += endGame.exp;
                endGame.result = (EndGameState)int.Parse(playerResult["result"].ToString());
                if (endGame.result == EndGameState.WIN) win++;
                else if (endGame.result == EndGameState.LOSE) lose++;
                endGame.medal = int.Parse(playerResult["medal"].ToString());
                medal = endGame.medal;
                Fu.LogSC("ImportResult name = " + name);
            }
        }
    }

    public struct EndGameResult {
        /// <summary>
        /// Trạng thái của player
        /// </summary>
        public EndGamePlayerState state;
        /// <summary>
        /// Số kinh nghiệm nhận được
        /// </summary>
        public int exp;
        /// <summary>
        /// Kết quả thắng thua
        /// </summary>
        public EndGameState result;
        /// <summary>
        /// Dùng để tính medal 
        /// </summary>
        public int medal;
    }
}
