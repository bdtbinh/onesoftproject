﻿using Mp.Pvp;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkyGameKit.Multiplayer {
    public class API {
        private static Client client = null;
        private static Dictionary<string, object> temp = new Dictionary<string, object>();
        /// <summary>
        /// Có hiện log hay không
        /// </summary>
        public static bool log = true;
        /// <summary>
        /// Có hiện log lỗi hay không
        /// </summary>
        public static bool logError = true;
        /// <summary>
        /// Kết nối hoặc kết nối lại thành công
        /// </summary>
        public static Action onServerConnect;
        /// <summary>
        /// Mất kết nối
        /// </summary>
        public static Action onServerDisconnect;
        /// <summary>
        /// Bắt đầu chuyển scene sang nàm chơi
        /// </summary>
        public static Action<int, int> onStartLoading;
        /// <summary>
        /// Dừng việc chuyển scene khi 2 player ko ready kịp
        /// </summary>
        public static Action onStopLoading;
        public static Player Me {
            get {
                return CachePvp.MyInfo;
            }
        }
        public static Player Opponent { get; set; }
        public static PlayerInfo MeOnFB {
            get {
                if (client != null) return client.meOnFB;
                return default(PlayerInfo);
            }
        }


        private static IEnumerator countTimeout;
        /// <summary>
        /// Kết nối đến server và đăng ký PlayerName với server
        /// </summary>
        /// <param name="successCallback">Thành công</param>
        /// <param name="errorCallback">Thất bại</param>
        /// <param name="gameObject">GameObject để chạy Coroutine đếm thời gian timeout</param>
        /// <param name="timeout">Hết thời gian này errorCallback sẽ được gọi nếu chưa kết nối được</param>
        public static void ConnectToServer(Action successCallback, Action errorCallback, MonoBehaviour gameObject, float timeout = 10f) {
            if (client == null) {
                if (string.IsNullOrEmpty(PlayerName)) {
                    Fu.LogWarning("Player không có tên, kết nối với tên rỗng");
                }
                client = new Client();
                countTimeout = WaitTimeout(errorCallback, timeout);
                client.connectSuccess += successCallback;
                client.connectSuccess += () => {
                    gameObject.StopCoroutine(countTimeout);
                };
                gameObject.StartCoroutine(countTimeout);
            }
        }

        private static IEnumerator WaitTimeout(Action errorCallback, float timeout) {
            yield return new WaitForSeconds(timeout);
            if (client != null) {
                client.Close();
                client = null;
            }
            if (errorCallback != null) errorCallback();
        }

        /// <summary>
        /// Xóa hết dữ liệu PvP của người chơi
        /// </summary>
        public static void ClearMultiplayerData() {
            PlayerPrefs.DeleteKey(Const.TOKEN_PLAYERPREFS_KEY);
            PlayerPrefs.DeleteKey(Const.NAME_PLAYERPREFS_KEY);
            PlayerPrefs.DeleteKey(Const.USE_FB_PLAYERPREFS_KEY);
        }

        /// <summary>
        /// Dùng để ngắt kết nối
        /// </summary>
        public static void Destroy() {
            if (client != null) {
                client.Close();
                client = null;
            }
            onServerConnect = null;
            onServerDisconnect = null;
            if (temp != null) temp.Clear();
        }

        /// <summary>
        /// Lấy dữ liệu fb về biến API.MeOnFB
        /// </summary>
        /// <param name="facebookToken"></param>
        /// <param name="facebookId"></param>
        /// <param name="fbConnectCallback">Lấy xong, chỉ 2 trường hợp SUCCESS và FACEBOOK_USED_ON_ANOTHER_DEVICE là có dữ liêu vào API.MeOnFB</param>
        public static void GetFBData(string facebookToken, string facebookId, Action<FacebookConnectState> fbConnectCallback) {
            client.facebookId = facebookId;
            client.fbConnectCallback = fbConnectCallback;
            temp.Clear();
            temp.Add("systemToken", client.token);
            temp.Add("facebookToken", facebookToken);
            temp.Add("facebookId", facebookId);
            client.Emit(Const.FACEBOOK_CONNECT, temp);
        }

        /// <summary>
        /// Gửi quyết định dùng dữ liệu local hay FB lên server
        /// Lưu ý: Sẽ tự gán me = meOnFB hoặc meOnFB = me và biến UseFB sẽ thay đổi
        /// </summary>
        /// <param name="useFB"></param>
        public static void FBDataSync(bool useFB) {
            temp.Clear();
            temp.Add("facebookId", client.facebookId);
            temp.Add("syncType", useFB ? 0 : 1);
            client.Emit(Const.FACEBOOK_DATA_SYNC, temp);
            if (useFB) {
                client.me = client.meOnFB;
                UseFB = UseFBState.Yes;
            } else {
                client.meOnFB = client.me;
                UseFB = UseFBState.No;
            }
        }

        private static string _playerName;
        public static string PlayerName {
            get {
                if (!string.IsNullOrEmpty(Me.name)) return client.me.name;
                if (string.IsNullOrEmpty(_playerName)) {
                    _playerName = PlayerPrefs.GetString(Const.NAME_PLAYERPREFS_KEY, "");
                }
                return _playerName;
            }
            set {
                _playerName = value;
                PlayerPrefs.SetString(Const.NAME_PLAYERPREFS_KEY, value);
                if (client != null) client.SendDisplayName(value);
            }
        }

        private static UseFBState _useFB;
        public static UseFBState UseFB {
            get {
                if (_useFB == UseFBState.NotChoose) _useFB = (UseFBState)PlayerPrefs.GetInt(Const.USE_FB_PLAYERPREFS_KEY, 0);
                return _useFB;
            }
            set {
                _useFB = value;
                PlayerPrefs.SetInt(Const.USE_FB_PLAYERPREFS_KEY, (int)value);
            }
        }

        public class Global {
            /// <summary>
            /// Thời gian chờ khi tìm đối thủ (cập nhật khi nhận được FindState.WAITING) hết thời gian chưa tìm thấy sẽ báo FindState.TIMEOUT
            /// </summary>
            public static int FindOpponentTimeout {
                get {
                    if (client != null) return client.findOpponentTimeout;
                    return 0;
                }
            }

            /// <summary>
            /// Tìm đối thủ
            /// </summary>
            /// <param name="goldBet">Số tiền đặt cược</param>
            /// <param name="findOpponentCallback">Lưu ý khi báo WAITING sẽ trả về thời gian sẽ chờ ở FindOpponentTimeout</param>
            /// <param name="onStartLoading">Khi lấy được thông tin players, map, delayTime. Nên bắt đầu Load scene ở đây, param1 là map, param2 là thời gian server chờ 2 người chơi ready</param>
            public static void FindOpponent(int goldBet, Action<FindState> findOpponentCallback) {
                if (client != null) {
                    client.findOpponentCallback = findOpponentCallback;
                    temp.Clear();
                    temp.Add("goldBet", goldBet);
                    client.Emit(Const.FIND_OPPONENT, temp);
                }
            }

            /// <summary>
            /// Dừng việc tìm đối thủ
            /// </summary>
            public static void CancelFindOpponent() {
                if (client != null) {
                    Fu.LogCS("CancelFindOpponent");
                    client.Emit(Const.CANCEL_FIND_OPPONENT);
                }
            }
        }
        public class Private {
            public static void CreateRoom(string name, Action<CreateRoomState> createRoomCallback) {
                if (client != null) {
                    client.createRoomCallback = createRoomCallback;
                    temp.Clear();
                    temp.Add("name", name);
                    client.Emit(Const.CREATE_ROOM, temp);
                }
            }
            public static void LeaveRoom() {
                if (client != null) {
                    client.Emit(Const.LEAVE_ROOM);
                }
            }
            public static void JoinRoom(string name, Action<JoinRoomState> joinRoomCallback) {
                if (client != null) {
                    client.joinRoomCallback = joinRoomCallback;
                    temp.Clear();
                    temp.Add("name", name);
                    client.Emit(Const.JOIN_ROOM, temp);
                }
            }
        }
        public class InGame {
            /// <summary>
            /// Gửi sẵn sàng lên server 
            /// </summary>
            /// <param name="onStartGame">Khi game bắt đầu</param>
            /// <param name="onRoomDestroy">Khi game bị hủy do có người không Ready</param>
            /// <param name="onOpponentDied">Khi đối thử chết, param là số thời gian để vượt qua số điểm của đối thủ</param>
            /// <param name="onEndGame">Khi game kết thúc, biến Me.endGame và Opponent.endGame lưu kết quả</param>
            public static void Ready(Action onStartGame, Action onRoomDestroy, Action<int> onOpponentDied, Action onEndGame) {
                if (client != null) {
                    client.onStartGame = onStartGame;
                    client.onRoomDestroy = onRoomDestroy;
                    client.onOpponentDied = onOpponentDied;
                    client.onEndGame = onEndGame;
                    Fu.LogCS("Ready");
                    client.Emit(Const.PLAYER_READY);
                }
            }

            public static void Die() {
                if (client != null) {
                    Fu.LogCS("Die");
                    client.Emit(Const.PLAYER_DIED);
                }
            }

            /// <summary>
            /// Gọi khi trường hợp server không biết thắng, như đi đến đích trước.
            /// </summary>
            public static void Win() {
                if (client != null) {
                    Fu.LogCS("Win");
                    client.Emit(Const.PLAYER_WIN);
                }
            }

            /// <summary>
            /// Gửi điểm và wave đã đạt đến
            /// </summary>
            /// <param name="score"></param>
            /// <param name="wave"></param>
            public static void SendScore(int score, int wave = -1) {
                if (client != null) {
                    temp.Clear();
                    temp.Add("score", score);
                    temp.Add("wave", wave);
                    client.Emit(Const.SCORE, temp);
                    Fu.LogCS("Score: " + score + " -- wave: " + wave);
                }
            }
        }

        public class LeaderBoard {
            public static void Get(RankType by, Action<List<PlayerInfo>> successCallback) {
                if (client != null) {
                    client.onRefreshLeaderBoardSuccess = successCallback;
                    temp.Clear();
                    temp.Add("by", (int)by);
                    client.Emit(Const.LEADERBOARD, temp);
                    Fu.LogCS("Refresh LeaderBoard: by " + by.ToString());
                }
            }
        }
    }
}
