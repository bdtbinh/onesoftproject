﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace SkyGameKit.Multiplayer.Demo {
    public class GlobalScreen : MonoBehaviour {
        private const string fbToken = "EAAZAzUAOb9dgBANei2CgfDeZC25cZBdKSKheDFCg6GCxcGum92QVhoXFI7uJ6nAPAZBn0yZCE0wmTggX29sZBGr91yqAehbpOJnwApUyPyxIMEgXMYhX9ZAXiBrzYZCuMHn6iSZCuNyESeZCeRcF4EUwvhAQ74PMmPIOQmHVq6STZB8cXsWL1sBUj9l5imYoqFa4ZCQZD";
        private const string fbid = "2000583823496396";
        void OnGUI() {
            if (GUI.Button(Fu.GetRect(1), "LeaderBoard")) {
                API.LeaderBoard.Get(RankType.country, x => {
                    foreach (var item in x) {
                        print("name: " + item.name + " rank: " + item.ranking);
                    }
                });
            }
            if (GUI.Button(Fu.GetRect(2), "Clear Multiplayer Data")) {
                API.ClearMultiplayerData();
            }
            if (GUI.Button(Fu.GetRect(3), "Connect Anh Minh FB")) {
                API.GetFBData(fbToken, fbid,
                    x => {
                        print("Status: " + x.ToString());
                        if (x == FacebookConnectState.SUCCESS) {
                            API.FBDataSync(true);
                        }
                    });
            }
            if (GUI.Button(Fu.GetRect(4), "Find Opponent")) {
                API.Global.FindOpponent(100,
                    x => {
                        print(x);
                        if (x == FindState.WAITING) {
                            print("TIME: " + API.Global.FindOpponentTimeout);
                        }
                    });
            }
            if (GUI.Button(Fu.GetRect(5), "Cancel Find Opponent")) {
                API.Global.CancelFindOpponent();
            }
            if (GUI.Button(Fu.GetRect(6), "Ready")) {
                API.InGame.Ready(() => print("Start Game"), () => print("Room Destroy"), x => print("Opponent player die: Time to end game = " + x), () => print("End Game"));
            }
            if (GUI.Button(Fu.GetRect(7), "Send Score")) {
                API.InGame.SendScore(Random.Range(100, 100000));
            }
            if (GUI.Button(Fu.GetRect(8), "Die")) {
                API.InGame.Die();
            }
            if (GUI.Button(Fu.GetRect(9), "Win")) {
                API.InGame.Win();
            }
            if (GUI.Button(Fu.GetRect(10), "Private")) {
                SceneManager.LoadScene("Private");
            }
        }
        private void Start() {
            if (string.IsNullOrEmpty(API.PlayerName)) API.PlayerName = "P" + Random.Range(1000, 100000).ToString("X5");
            API.ConnectToServer(() => print("OK name:" + API.Me.name), () => print("Fail"), this);
            API.onStartLoading = (x, y) => print("map: " + x + " delayTime: " + y);
        }

        private void OnDestroy() {
            API.Destroy();
        }
    }
}

