﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace SkyGameKit.Multiplayer.Demo {
    public class PrivateScreen : MonoBehaviour {
        string roomName = "RoomName";
        void OnGUI() {
            roomName = GUI.TextField(Fu.GetRect(1), roomName, 30);
            if (GUI.Button(Fu.GetRect(2), "Create Room")) {
                API.Private.CreateRoom(roomName, x => print(x));
            }
            if (GUI.Button(Fu.GetRect(3), "Leave Room")) {
                API.Private.LeaveRoom();
            }
            if (GUI.Button(Fu.GetRect(4), "Join Room")) {
                API.Private.JoinRoom(roomName, x => print(x));
            }
            if (GUI.Button(Fu.GetRect(5), "Ready")) {
                API.InGame.Ready(() => print("Start Game"), () => print("Room Destroy"), x => print("Opponent player die: Time to end game = " + x), () => print("End Game"));
            }
            if (GUI.Button(Fu.GetRect(6), "Send Score")) {
                API.InGame.SendScore(Random.Range(100, 100000));
            }
            if (GUI.Button(Fu.GetRect(7), "Die")) {
                API.InGame.Die();
            }
            if (GUI.Button(Fu.GetRect(8), "Win")) {
                API.InGame.Win();
            }
            if (GUI.Button(Fu.GetRect(10), "Global")) {
                SceneManager.LoadScene("Global");
            }
        }
        private void Start() {
            if (string.IsNullOrEmpty(API.PlayerName)) API.PlayerName = "P" + Random.Range(1000, 100000).ToString("X5");
            API.ConnectToServer(() => print("OK name:" + API.Me.name), () => print("Fail"), this);
            API.onStartLoading = (x, y) => print("map: " + x + " delayTime: " + y);
        }

        private void OnDestroy() {
            API.Destroy();
        }
    }
}

