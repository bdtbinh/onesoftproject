﻿using BestHTTP.SocketIO;
using System;
using System.Collections.Generic;
using UnityEngine;
namespace SkyGameKit.Multiplayer {
    public class Client {
        public Action connectSuccess;

        public Action<FacebookConnectState> fbConnectCallback;
        public Action<FindState> findOpponentCallback;
        public Action<CreateRoomState> createRoomCallback;
        public Action<JoinRoomState> joinRoomCallback;
        public int findOpponentTimeout;
        public Action<int, int> onServerWaitForReady;
        public Action onRoomDestroy;
        public Action onStartGame;
        public Action<int> onOpponentScoreChange;
        public Action<int> onOpponentDied;
        public Action onEndGame;
        public Action<List<PlayerInfo>> onRefreshLeaderBoardSuccess;

        private Dictionary<string, object> temp = new Dictionary<string, object>();
        private SocketOptions options;
        private SocketManager Manager;

        public PlayerInfo me;
        public PlayerInfo meOnFB;
        public PlayerInfo opponent;
        public string token;
        public string facebookId;


        public Client() {
            options = new SocketOptions {
                AutoConnect = false,
                ConnectWith = BestHTTP.SocketIO.Transports.TransportTypes.WebSocket,
                QueryParamsOnlyForHandshake = true,
                AdditionalQueryParams = new PlatformSupport.Collections.ObjectModel.ObservableDictionary<string, string>()
            };
            Manager = new SocketManager(new Uri(Const.defaultURI), options);

            Manager.Socket.On(SocketIOEventTypes.Connect, OnServerConnect);
            Manager.Socket.On(SocketIOEventTypes.Disconnect, OnServerDisconnect);

            Manager.Socket.On(Const.GET_TOKEN, GetTokenCallback);
            Manager.Socket.On(Const.VALIDATE_TOKEN, ValidateTokenCallback);

            Manager.Socket.On(Const.FIND_OPPONENT, FindOpponentCallback);
            Manager.Socket.On(Const.CANCEL_FIND_OPPONENT, CancelFindCallback);

            Manager.Socket.On(Const.CREATE_ROOM, CreateRoomCallback);
            Manager.Socket.On(Const.JOIN_ROOM, JoinRoomCallback);
            Manager.Socket.On(Const.LEAVE_ROOM, LeaveRoomCallback);

            Manager.Socket.On(Const.DISPLAY_NAME, ChangeNameSuccessCallback);

            Manager.Socket.On(Const.FACEBOOK_CONNECT, FBConnectCallback);
            Manager.Socket.On(Const.FACEBOOK_DATA_SYNC, FBDataSyncCallback);

            Manager.Socket.On(Const.WAIT_FOR_READY, WaitForReadyCallback);
            Manager.Socket.On(Const.PLAYER_READY, PlayerReadyCallback);
            Manager.Socket.On(Const.PLAYER_WIN, PlayerWinCallback);
            Manager.Socket.On(Const.PLAYER_DIED, PlayerDiedCallback);
            Manager.Socket.On(Const.ROOM_DESTROY, RoomDestroyCallback);
            Manager.Socket.On(Const.START_GAME, StartGameCallback);
            Manager.Socket.On(Const.END_GAME, EndGameCallback);
            Manager.Socket.On(Const.SCORE, SocreCallback);

            Manager.Socket.On(Const.LEADERBOARD, LeaderBoardCallback);


            Manager.Open();

            if (PlayerPrefs.HasKey(Const.TOKEN_PLAYERPREFS_KEY)) {
                temp.Clear();
                token = PlayerPrefs.GetString(Const.TOKEN_PLAYERPREFS_KEY);
                temp.Add("token", token);
                Manager.Socket.Emit(Const.VALIDATE_TOKEN, temp);
            } else {
                Manager.Socket.Emit(Const.GET_TOKEN);
            }
        }

        public void Close() {
            Manager.Close();
        }

        public Socket Emit(string eventName, params object[] args) {
            return Manager.Socket.Emit(eventName, args);
        }

        public void SendDisplayName(string name) {
            temp.Clear();
            temp.Add("name", name);
            Manager.Socket.Emit(Const.DISPLAY_NAME, temp);
            Fu.LogCS("SendDisplayName: " + name);
        }

        private void LeaveRoomCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("LeaveRoom");
        }

        private void JoinRoomCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("JoinRoom success");
            int status = int.Parse(temp["status"].ToString());
            if (joinRoomCallback != null) joinRoomCallback((JoinRoomState)status);
        }

        private void CreateRoomCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("CreateRoom success");
            int status = int.Parse(temp["status"].ToString());
            if (createRoomCallback != null) createRoomCallback((CreateRoomState)status);
        }

        private void CancelFindCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("Cancel find opponent success");
        }

        private void ValidateTokenCallback(Socket socket, Packet packet, object[] args) {
            temp = args[0] as Dictionary<string, object>;
            int status = int.Parse(temp["status"].ToString());
            if (status == 0) {
                Fu.LogSC("ValidateToken OK");
                ConnectSuccess(temp["player"] as Dictionary<string, object>);
            } else {
                Fu.LogError("ValidateToken Fail: status = " + status);
            }
        }

        private void GetTokenCallback(Socket socket, Packet packet, object[] args) {
            temp = args[0] as Dictionary<string, object>;
            int status = int.Parse(temp["status"].ToString());
            if (status == 0) {
                token = temp["token"].ToString();
                Fu.LogSC("GetToken: " + token);
                PlayerPrefs.SetString(Const.TOKEN_PLAYERPREFS_KEY, token);
                me.name = API.PlayerName;
                ConnectSuccess(temp["player"] as Dictionary<string, object>);
            } else {
                Fu.LogError("GetToken Fail: status = " + status);
            }
        }

        private void ConnectSuccess(Dictionary<string, object> player) {
            me.Import(player);
            Fu.LogDictionary("Me:", player);
            if (me.name != API.PlayerName) {
                if (!string.IsNullOrEmpty(API.PlayerName)) {
                    SendDisplayName(API.PlayerName);
                    me.name = API.PlayerName;
                }
            }
            if (connectSuccess != null) connectSuccess();
        }

        private void OnServerConnect(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("OnServerConnect");
            var session = socket.Id;
            Manager.Options.AdditionalQueryParams.Remove("session");
            Manager.Options.AdditionalQueryParams.Add("session", session);
            if (API.onServerConnect != null) API.onServerConnect();
        }

        private void OnServerDisconnect(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("OnServerDisconnect");
            if (API.onServerDisconnect != null) API.onServerDisconnect();
        }

        private void ChangeNameSuccessCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("Change player name success");
        }

        private void FBConnectCallback(Socket socket, Packet packet, object[] args) {
            temp = args[0] as Dictionary<string, object>;
            int fbConnectStatus = int.Parse(temp["status"].ToString());
            Fu.LogSC("FBConnect: status = " + fbConnectStatus);
            if ((FacebookConnectState)fbConnectStatus == FacebookConnectState.SUCCESS ||
                (FacebookConnectState)fbConnectStatus == FacebookConnectState.FACEBOOK_USED_ON_ANOTHER_DEVICE) {
                Fu.LogDictionary("meOnFB:", temp["player"] as Dictionary<string, object>);
                meOnFB.Import(temp["player"] as Dictionary<string, object>);
            }
            if (fbConnectCallback != null) fbConnectCallback((FacebookConnectState)fbConnectStatus);
        }

        private void FBDataSyncCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("FBDataSync success");
        }

        private void FindOpponentCallback(Socket socket, Packet packet, object[] args) {
            temp = args[0] as Dictionary<string, object>;
            FindState findState = (FindState)(int.Parse(temp["status"].ToString()));
            if (findState == FindState.WAITING) {
                findOpponentTimeout = int.Parse(temp["timeout"].ToString());
            }
            findOpponentCallback(findState);
        }

        private void SocreCallback(Socket socket, Packet packet, object[] args) {
            temp = args[0] as Dictionary<string, object>;
            string id = temp["accountId"].ToString();
            if (id == opponent.id) {
                opponent.score = int.Parse(temp["score"].ToString());
                int wave = int.Parse(temp["wave"].ToString());
                if (wave > -1) opponent.wave = wave;
                if (onOpponentScoreChange != null) onOpponentScoreChange(opponent.score);
                Fu.LogSC("Score: " + opponent.score + " -- wave: " + wave);
            }
        }

        private void EndGameCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("EndGame");
            temp = args[0] as Dictionary<string, object>;
            List<object> infos = temp["infos"] as List<object>;
            foreach (var playerResult in infos) {
                me.ImportResult(playerResult as Dictionary<string, object>);
                opponent.ImportResult(playerResult as Dictionary<string, object>);
                Fu.LogDictionary("playerResult:", playerResult as Dictionary<string, object>);
            }
            if (onEndGame != null) onEndGame();
        }

        private void StartGameCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("StartGame");
            if (onStartGame != null) onStartGame();
        }

        private void RoomDestroyCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("RoomDestroy");
            if (onRoomDestroy != null) onRoomDestroy();
        }

        private void WaitForReadyCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("WaitForReady");
            temp = args[0] as Dictionary<string, object>;
            int map = int.Parse(temp["map"].ToString());
            int delayTime = int.Parse(temp["delayTime"].ToString());
            //Lấy dữ liệu player
            List<object> players = temp["players"] as List<object>;
            foreach (var player in players) {
                PlayerInfo tmp = new PlayerInfo();
                Fu.LogDictionary("Players:", player as Dictionary<string, object>);
                tmp.Import(player as Dictionary<string, object>);
                if (tmp.id != me.id) {
                    opponent = tmp;
                } else {
                    me = tmp;
                }
            }
            if (onServerWaitForReady != null) onServerWaitForReady(map, delayTime);
        }

        private void PlayerDiedCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("PlayerDied");
            temp = args[0] as Dictionary<string, object>;
            string id = temp["accountId"].ToString();
            int waitingTime = int.Parse(temp["waitingTime"].ToString());
            if (id == opponent.id) {
                if (onOpponentDied != null) onOpponentDied(waitingTime);
            }
        }

        private void PlayerWinCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("PlayerWin");
        }

        private void PlayerReadyCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("PlayerReady");
        }


        private void LeaderBoardCallback(Socket socket, Packet packet, object[] args) {
            Fu.LogSC("LeaderBoardCallback");
            temp = args[0] as Dictionary<string, object>;
            temp = temp["leaderboard"] as Dictionary<string, object>;
            int type = int.Parse(temp["type"].ToString());
            List<object> players = temp["players"] as List<object>;
            var leaderBoard = new List<PlayerInfo>();
            foreach (var player in players) {
                PlayerInfo tmp = new PlayerInfo();
                Fu.LogDictionary("Players:", player as Dictionary<string, object>);
                tmp.Import(player as Dictionary<string, object>);
                leaderBoard.Add(tmp);
            }
            if (onRefreshLeaderBoardSuccess != null) onRefreshLeaderBoardSuccess(leaderBoard);
        }
    }
}
