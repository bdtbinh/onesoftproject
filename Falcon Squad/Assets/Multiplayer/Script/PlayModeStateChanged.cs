﻿#if UNITY_EDITOR
using UnityEditor;
using SkyGameKit.Multiplayer;
using OSNet;

// ensure class initializer is called whenever scripts recompile
[InitializeOnLoad]
public static class PlayModeStateChanged {
    // register an event handler when the class is initialized
    static PlayModeStateChanged() {
        EditorApplication.playModeStateChanged += PlayModeStateChangedHandler;
    }

    private static void PlayModeStateChangedHandler(PlayModeStateChange state) {
        if(state == PlayModeStateChange.EnteredEditMode) {
            API.Destroy();
            NetManager.Destroy();
            CachePvp.Destroy();
            Fu.LogWarning("API auto destroy");
        }
    }
}
#endif