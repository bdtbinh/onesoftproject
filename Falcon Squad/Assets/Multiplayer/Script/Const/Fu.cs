﻿using UnityEngine;
using System.Collections.Generic;
namespace SkyGameKit.Multiplayer {
    public class Fu {
        public static void LogCS(string text) {
            if (API.log) Debug.Log("Multiplayer C==>: " + text);
        }

        public static void LogSC(string text) {
            if (API.log) Debug.Log("Multiplayer <==S: " + text);
        }

        public static void LogDictionary(string startString, Dictionary<string, object> dictionary, bool isSC = true) {
            string log = startString;
            foreach (var item in dictionary) {
                log += " " + item.Key + "=" + item.Value;
            }
            if (isSC) LogSC(log); else LogCS(log);
        }

        public static void LogError(string text) {
            if (API.logError) Debug.LogError("Multiplayer: " + text);
        }

        public static void LogWarning(string text) {
            if (API.logError) Debug.LogWarning("Multiplayer: " + text);
        }

        public static Rect GetRect(int x) {
            int marginX = (int)(0.3 * Screen.width);
            int marginY = (int)(0.15 * Screen.height);
            Vector2 size = new Vector2(0.4f * Screen.width, 0.063f * Screen.height);
            Vector2 position = new Vector2 {
                x = marginX,
                y = marginY + x * 0.07f * Screen.height
            };
            return new Rect(position, size);
        }
    }
}
