﻿namespace SkyGameKit.Multiplayer {
    public class Const {
        public const string SESSION_INFORMATION = "session_information";

        //rank
        public const string RANKINGS = "rankings";
        public const string MY_RANK = "my ranking";
        public const string LEADERBOARD = "leaderboard";
        //end rank

        //profile
        public const string PROFILE = "profile";
        public const string PROFILE_ME = "profile me";
        //end profile

        //achievement
        public const string LIST_ACHIEVEMENT = "list achievement";
        public const string ACHIEVEMENT = "achievement";
        public const string ACHIEVEMENT_INFO = "achievement info";
        public const string ACHIEVEMENT_COMPLETE = "achievement complete";

        //history
        public const string HISTORY = "history";

        //friend
        public const string LIST_FRIEND_REQUEST = "list friend request";
        public const string FRIEND = "friend";
        //end friend

        public const string FIND_OPPONENT = "find_opponent";

        public const string START_GAME = "start_game";
        public const string END_GAME = "end_game";
        public const string WAIT_FOR_READY = "wait_for_ready";
        public const string PLAYER_DIED = "player_died";
        public const string PLAYER_WIN = "player_win";
        public const string PLAYER_READY = "player_ready";
        public const string SCORE = "score";
        public const string ROOM_INFO = "room_info";

        public const string SYSTEM_PROPERTIES = "system_properties";
        public const string CANCEL_FIND_OPPONENT = "cancel_find_opponent";
        public const string NETWORK_PROBLEM = "network problem";

        public const string GET_TOKEN = "get_token";
        public const string VALIDATE_TOKEN = "validate_token";
        public const string FACEBOOK_CONNECT = "facebook_connect";
        public const string FACEBOOK_DATA_SYNC = "facebook_data_sync";
        public const string CREATE_ROOM = "create_room";
        public const string JOIN_ROOM = "join_room";
        public const string LEAVE_ROOM = "leave_room";
        public const string DISPLAY_NAME = "display_name";
        public const string ROOM_DESTROY = "room_destroy";

        public const string defaultURI = "http://123.30.186.226:7788/falcon-squad/";

        public const string TOKEN_PLAYERPREFS_KEY = "MultiplayerToken";
        public const string NAME_PLAYERPREFS_KEY = "MultiplayerName";
        public const string USE_FB_PLAYERPREFS_KEY = "MultiplayerUseFB";
        public const string DEFAULT_NAME = "User";
    }

    public enum EndGamePlayerState {
        DIED = 1,
        OUT = 2,
        READY = 3,
        UNREADY = 4
    }

    public enum EndGameState {
        WIN = 1,
        LOSE = -1,
        DRAWN = 0
    }

    public enum FindState {
        SUCCESS = 0,
        ERROR = 1,
        WAITING = 2,
        TIMEOUT = 3
    }

    public enum FacebookConnectState {
        SUCCESS = 0,
        ERROR = 1,
        SYSTEM_TOKEN_INVALID = 2,
        FACEBOOK_USED_ON_ANOTHER_DEVICE = 3,
        FACEBOOK_TOKEN_INVALID = 4
    }

    public enum UseFBState {
        NotChoose,
        Yes,
        No
    }

    public enum CreateRoomState {
        SUCCESS = 0,
        ERROR = 1,
        /// <summary>
        /// User đang trong room nên không thể tạo room
        /// </summary>
        JOINED_ROOM = 2,
        /// <summary>
        /// Tên room đã tồng tại
        /// </summary>
        EXISTED_ROOM = 3
    }

    public enum JoinRoomState {
        SUCCESS = 0,
        ERROR = 1,
        /// <summary>
        /// User đã join room khác trước đó
        /// </summary>
        JOINED_ROOM = 2,
        /// <summary>
        /// Room không tồn tại
        /// </summary>
        NOT_EXIST_ROOM = 3
    }

    public enum RankType {
        world,
        country,
        friend
    }
}
