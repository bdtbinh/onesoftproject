﻿using System;
using UnityEngine;

namespace FalconSDK {
    public static class MediationInfo {
        private static string GetAdsInformationString() {
            string result = null;
#if UNITY_ANDROID
            string AndroidBridge = "falcon.mediationinfo.AdsInformation";
            using (var pluginClass = new AndroidJavaObject(AndroidBridge)) {
                result = pluginClass.CallStatic<string>("GetAdsInformation");
            }
#endif
            return result;
        }

        /// <summary>
        /// Lấy toàn bộ thông tin của các mạng quảng cáo
        /// </summary>
        public static AdsInformation[] GetAdsInformation() {
            string infoString = GetAdsInformationString();
            if (string.IsNullOrEmpty(infoString)) return null;
            string[] info = infoString.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            AdsInformation[] result = new AdsInformation[info.Length / 3];
            for (int i = 0; i < result.Length; i++) {
                result[i] = new AdsInformation(info[i * 3 + 0], info[i * 3 + 1], info[i * 3 + 2]);
            }
            return result;
        }

        /// <summary>
        /// Lấy thông tin mạng quảng cáo sẽ được hiển thị
        /// </summary>
        public static AdsInformation GetAvailableAdsInformation() {
            AdsInformation[] info = GetAdsInformation();
            if (info == null) return null;
            for (int i = 0; i < info.Length; i++) {
                if (info[i].Available) {
                    return info[i];
                }
            }
            return null;
        }
    }

    public class AdsInformation {
        public MediationAdsNetwork Mediation { get; private set; }
        public string Name { get; private set; }
        public bool Available { get; private set; }
        public string Id { get; private set; }
        public AdsInformation(string name, string state, string id) {
            Name = name;
            Available = state == "1";
            Id = id;
            if (name.Contains("Supersonic")) {
                Mediation = MediationAdsNetwork.IronSource;
            } else if (name.Contains("AdMob")) {
                Mediation = MediationAdsNetwork.AdMob;
            } else if (name.Contains("Facebook")) {
                Mediation = MediationAdsNetwork.Facebook;
            } else if (name.Contains("Unity")) {
                Mediation = MediationAdsNetwork.UnityAds;
            } else if (name.Contains("Vungle")) {
                Mediation = MediationAdsNetwork.Vungle;
            } else {
                Mediation = MediationAdsNetwork.Other;
            }
        }
        public override string ToString() {
            return Name + ";" + Available + ";" + Id + ";" + Mediation;
        }
    }

    public enum MediationAdsNetwork {
        IronSource,
        AdMob,
        Facebook,
        UnityAds,
        Vungle,
        Other
    }
}
