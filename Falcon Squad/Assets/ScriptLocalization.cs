﻿using UnityEngine;

namespace I2.Loc
{
	public static class ScriptLocalization
	{

		public static string ActiveSkill 		{ get{ return LocalizationManager.GetTranslation ("ActiveSkill"); } }
		public static string App_Name 		{ get{ return LocalizationManager.GetTranslation ("App_Name"); } }
		public static string Font_Localization 		{ get{ return LocalizationManager.GetTranslation ("Font Localization"); } }
		public static string MESS_1DAY_1 		{ get{ return LocalizationManager.GetTranslation ("MESS_1DAY_1"); } }
		public static string MESS_1DAY_2 		{ get{ return LocalizationManager.GetTranslation ("MESS_1DAY_2"); } }
		public static string MESS_1DAY_3 		{ get{ return LocalizationManager.GetTranslation ("MESS_1DAY_3"); } }
		public static string MESS_3DAY_1 		{ get{ return LocalizationManager.GetTranslation ("MESS_3DAY_1"); } }
		public static string MESS_3DAY_2 		{ get{ return LocalizationManager.GetTranslation ("MESS_3DAY_2"); } }
		public static string MESS_3DAY_3 		{ get{ return LocalizationManager.GetTranslation ("MESS_3DAY_3"); } }
		public static string MESS_7DAY_1 		{ get{ return LocalizationManager.GetTranslation ("MESS_7DAY_1"); } }
		public static string MESS_7DAY_2 		{ get{ return LocalizationManager.GetTranslation ("MESS_7DAY_2"); } }
		public static string MESS_7DAY_3 		{ get{ return LocalizationManager.GetTranslation ("MESS_7DAY_3"); } }
		public static string Notifi_Achi_PleaseComplete 		{ get{ return LocalizationManager.GetTranslation ("Notifi_Achi_PleaseComplete"); } }
		public static string Notifi_DailyQuest_PleaseComplete 		{ get{ return LocalizationManager.GetTranslation ("Notifi_DailyQuest_PleaseComplete"); } }
		public static string Notifi_HaveReceived 		{ get{ return LocalizationManager.GetTranslation ("Notifi_HaveReceived"); } }
		public static string Notifi_HaveReceived_ChangeKey 		{ get{ return LocalizationManager.GetTranslation ("Notifi_HaveReceived_ChangeKey"); } }
		public static string SkillActivated 		{ get{ return LocalizationManager.GetTranslation ("SkillActivated"); } }
		public static string accept 		{ get{ return LocalizationManager.GetTranslation ("accept"); } }
		public static string accepted 		{ get{ return LocalizationManager.GetTranslation ("accepted"); } }
		public static string accumulate 		{ get{ return LocalizationManager.GetTranslation ("accumulate"); } }
		public static string accumulated_bell 		{ get{ return LocalizationManager.GetTranslation ("accumulated_bell"); } }
		public static string accumulated_candy 		{ get{ return LocalizationManager.GetTranslation ("accumulated_candy"); } }
		public static string achi_collect_gold 		{ get{ return LocalizationManager.GetTranslation ("achi_collect_gold"); } }
		public static string achi_collect_items 		{ get{ return LocalizationManager.GetTranslation ("achi_collect_items"); } }
		public static string achi_defeat_bosses 		{ get{ return LocalizationManager.GetTranslation ("achi_defeat_bosses"); } }
		public static string achi_have_upgrades 		{ get{ return LocalizationManager.GetTranslation ("achi_have_upgrades"); } }
		public static string achi_kill_enemies 		{ get{ return LocalizationManager.GetTranslation ("achi_kill_enemies"); } }
		public static string achi_use_activeskill 		{ get{ return LocalizationManager.GetTranslation ("achi_use_activeskill"); } }
		public static string achi_use_emp 		{ get{ return LocalizationManager.GetTranslation ("achi_use_emp"); } }
		public static string achievements 		{ get{ return LocalizationManager.GetTranslation ("achievements"); } }
		public static string add_friend 		{ get{ return LocalizationManager.GetTranslation ("add_friend"); } }
		public static string air_craft 		{ get{ return LocalizationManager.GetTranslation ("air_craft"); } }
		public static string aircraft10_name 		{ get{ return LocalizationManager.GetTranslation ("aircraft10_name"); } }
		public static string aircraft1_name 		{ get{ return LocalizationManager.GetTranslation ("aircraft1_name"); } }
		public static string aircraft2_name 		{ get{ return LocalizationManager.GetTranslation ("aircraft2_name"); } }
		public static string aircraft3_name 		{ get{ return LocalizationManager.GetTranslation ("aircraft3_name"); } }
		public static string aircraft6_name 		{ get{ return LocalizationManager.GetTranslation ("aircraft6_name"); } }
		public static string aircraft7_name 		{ get{ return LocalizationManager.GetTranslation ("aircraft7_name"); } }
		public static string aircraft8_name 		{ get{ return LocalizationManager.GetTranslation ("aircraft8_name"); } }
		public static string aircraft9_name 		{ get{ return LocalizationManager.GetTranslation ("aircraft9_name"); } }
		public static string announcement 		{ get{ return LocalizationManager.GetTranslation ("announcement"); } }
		public static string available_exchange 		{ get{ return LocalizationManager.GetTranslation ("available_exchange"); } }
		public static string becom_rich 		{ get{ return LocalizationManager.GetTranslation ("becom_rich"); } }
		public static string bell_exchange 		{ get{ return LocalizationManager.GetTranslation ("bell_exchange"); } }
		public static string betting_amount 		{ get{ return LocalizationManager.GetTranslation ("betting_amount"); } }
		public static string bonus 		{ get{ return LocalizationManager.GetTranslation ("bonus"); } }
		public static string bonus_medal 		{ get{ return LocalizationManager.GetTranslation ("bonus_medal"); } }
		public static string boss 		{ get{ return LocalizationManager.GetTranslation ("boss"); } }
		public static string boss_time 		{ get{ return LocalizationManager.GetTranslation ("boss_time"); } }
		public static string buy 		{ get{ return LocalizationManager.GetTranslation ("buy"); } }
		public static string calling_backup_reduce_life 		{ get{ return LocalizationManager.GetTranslation ("calling_backup_reduce_life"); } }
		public static string calling_backup_title 		{ get{ return LocalizationManager.GetTranslation ("calling_backup_title"); } }
		public static string campain 		{ get{ return LocalizationManager.GetTranslation ("campain"); } }
		public static string cancel 		{ get{ return LocalizationManager.GetTranslation ("cancel"); } }
		public static string candy_exchange 		{ get{ return LocalizationManager.GetTranslation ("candy_exchange"); } }
		public static string card_from_friend 		{ get{ return LocalizationManager.GetTranslation ("card_from_friend"); } }
		public static string charging 		{ get{ return LocalizationManager.GetTranslation ("charging"); } }
		public static string chat 		{ get{ return LocalizationManager.GetTranslation ("chat"); } }
		public static string claim 		{ get{ return LocalizationManager.GetTranslation ("claim"); } }
		public static string claimed 		{ get{ return LocalizationManager.GetTranslation ("claimed"); } }
		public static string claimed_reward 		{ get{ return LocalizationManager.GetTranslation ("claimed_reward"); } }
		public static string clan 		{ get{ return LocalizationManager.GetTranslation ("clan"); } }
		public static string clan_info 		{ get{ return LocalizationManager.GetTranslation ("clan_info"); } }
		public static string clan_invitation 		{ get{ return LocalizationManager.GetTranslation ("clan_invitation"); } }
		public static string clan_master 		{ get{ return LocalizationManager.GetTranslation ("clan_master"); } }
		public static string clan_member 		{ get{ return LocalizationManager.GetTranslation ("clan_member"); } }
		public static string clan_name 		{ get{ return LocalizationManager.GetTranslation ("clan_name"); } }
		public static string clan_vice 		{ get{ return LocalizationManager.GetTranslation ("clan_vice"); } }
		public static string close 		{ get{ return LocalizationManager.GetTranslation ("close"); } }
		public static string community 		{ get{ return LocalizationManager.GetTranslation ("community"); } }
		public static string community_follow 		{ get{ return LocalizationManager.GetTranslation ("community_follow"); } }
		public static string community_friends 		{ get{ return LocalizationManager.GetTranslation ("community_friends"); } }
		public static string community_join 		{ get{ return LocalizationManager.GetTranslation ("community_join"); } }
		public static string community_like 		{ get{ return LocalizationManager.GetTranslation ("community_like"); } }
		public static string community_share 		{ get{ return LocalizationManager.GetTranslation ("community_share"); } }
		public static string complete 		{ get{ return LocalizationManager.GetTranslation ("complete"); } }
		public static string completed 		{ get{ return LocalizationManager.GetTranslation ("completed"); } }
		public static string confirm 		{ get{ return LocalizationManager.GetTranslation ("confirm"); } }
		public static string continue_key 		{ get{ return LocalizationManager.GetTranslation ("continue_key"); } }
		public static string control_scheme 		{ get{ return LocalizationManager.GetTranslation ("control_scheme"); } }
		public static string create 		{ get{ return LocalizationManager.GetTranslation ("create"); } }
		public static string current 		{ get{ return LocalizationManager.GetTranslation ("current"); } }
		public static string current_candy 		{ get{ return LocalizationManager.GetTranslation ("current_candy"); } }
		public static string current_win 		{ get{ return LocalizationManager.GetTranslation ("current_win"); } }
		public static string day 		{ get{ return LocalizationManager.GetTranslation ("day"); } }
		public static string day_dailyLogin 		{ get{ return LocalizationManager.GetTranslation ("day_dailyLogin"); } }
		public static string days 		{ get{ return LocalizationManager.GetTranslation ("days"); } }
		public static string deal_end_in 		{ get{ return LocalizationManager.GetTranslation ("deal_end_in"); } }
		public static string defeat 		{ get{ return LocalizationManager.GetTranslation ("defeat"); } }
		public static string delete 		{ get{ return LocalizationManager.GetTranslation ("delete"); } }
		public static string denied 		{ get{ return LocalizationManager.GetTranslation ("denied"); } }
		public static string des_card_pack 		{ get{ return LocalizationManager.GetTranslation ("des_card_pack"); } }
		public static string des_level_aircraft_sale 		{ get{ return LocalizationManager.GetTranslation ("des_level_aircraft_sale"); } }
		public static string des_name_aircraft_sale 		{ get{ return LocalizationManager.GetTranslation ("des_name_aircraft_sale"); } }
		public static string des_wing_of_justice 		{ get{ return LocalizationManager.GetTranslation ("des_wing_of_justice"); } }
		public static string des_wing_of_redemption 		{ get{ return LocalizationManager.GetTranslation ("des_wing_of_redemption"); } }
		public static string des_wing_of_resolution 		{ get{ return LocalizationManager.GetTranslation ("des_wing_of_resolution"); } }
		public static string discount_percent 		{ get{ return LocalizationManager.GetTranslation ("discount_percent"); } }
		public static string donate 		{ get{ return LocalizationManager.GetTranslation ("donate"); } }
		public static string donate_info 		{ get{ return LocalizationManager.GetTranslation ("donate_info"); } }
		public static string done 		{ get{ return LocalizationManager.GetTranslation ("done"); } }
		public static string dont_save 		{ get{ return LocalizationManager.GetTranslation ("dont_save"); } }
		public static string down_to_member 		{ get{ return LocalizationManager.GetTranslation ("down_to_member"); } }
		public static string draw 		{ get{ return LocalizationManager.GetTranslation ("draw"); } }
		public static string duration 		{ get{ return LocalizationManager.GetTranslation ("duration"); } }
		public static string elo_key 		{ get{ return LocalizationManager.GetTranslation ("elo_key"); } }
		public static string end_in 		{ get{ return LocalizationManager.GetTranslation ("end_in"); } }
		public static string endless 		{ get{ return LocalizationManager.GetTranslation ("endless"); } }
		public static string energy 		{ get{ return LocalizationManager.GetTranslation ("energy"); } }
		public static string enter_your_giftcode 		{ get{ return LocalizationManager.GetTranslation ("enter_your_giftcode"); } }
		public static string enter_your_name 		{ get{ return LocalizationManager.GetTranslation ("enter_your_name"); } }
		public static string equip 		{ get{ return LocalizationManager.GetTranslation ("equip"); } }
		public static string error 		{ get{ return LocalizationManager.GetTranslation ("error"); } }
		public static string event_item_ice_cream 		{ get{ return LocalizationManager.GetTranslation ("event_item_ice_cream"); } }
		public static string evolve 		{ get{ return LocalizationManager.GetTranslation ("evolve"); } }
		public static string exit 		{ get{ return LocalizationManager.GetTranslation ("exit"); } }
		public static string extra 		{ get{ return LocalizationManager.GetTranslation ("extra"); } }
		public static string fight 		{ get{ return LocalizationManager.GetTranslation ("fight"); } }
		public static string find 		{ get{ return LocalizationManager.GetTranslation ("find"); } }
		public static string follow 		{ get{ return LocalizationManager.GetTranslation ("follow"); } }
		public static string follow_us 		{ get{ return LocalizationManager.GetTranslation ("follow_us"); } }
		public static string following 		{ get{ return LocalizationManager.GetTranslation ("following"); } }
		public static string free 		{ get{ return LocalizationManager.GetTranslation ("free"); } }
		public static string friend 		{ get{ return LocalizationManager.GetTranslation ("friend"); } }
		public static string friend_request 		{ get{ return LocalizationManager.GetTranslation ("friend_request"); } }
		public static string friends 		{ get{ return LocalizationManager.GetTranslation ("friends"); } }
		public static string game_match 		{ get{ return LocalizationManager.GetTranslation ("game_match"); } }
		public static string gem 		{ get{ return LocalizationManager.GetTranslation ("gem"); } }
		public static string get_amount_item 		{ get{ return LocalizationManager.GetTranslation ("get_amount_item"); } }
		public static string giftcode 		{ get{ return LocalizationManager.GetTranslation ("giftcode"); } }
		public static string global 		{ get{ return LocalizationManager.GetTranslation ("global"); } }
		public static string glory 		{ get{ return LocalizationManager.GetTranslation ("glory"); } }
		public static string glory_chest 		{ get{ return LocalizationManager.GetTranslation ("glory_chest"); } }
		public static string glory_point 		{ get{ return LocalizationManager.GetTranslation ("glory_point"); } }
		public static string go 		{ get{ return LocalizationManager.GetTranslation ("go"); } }
		public static string gold 		{ get{ return LocalizationManager.GetTranslation ("gold"); } }
		public static string hangar 		{ get{ return LocalizationManager.GetTranslation ("hangar"); } }
		public static string hard 		{ get{ return LocalizationManager.GetTranslation ("hard"); } }
		public static string have_been_outrun 		{ get{ return LocalizationManager.GetTranslation ("have_been_outrun"); } }
		public static string hell 		{ get{ return LocalizationManager.GetTranslation ("hell"); } }
		public static string highest_wave 		{ get{ return LocalizationManager.GetTranslation ("highest_wave"); } }
		public static string iap_infinity__forever 		{ get{ return LocalizationManager.GetTranslation ("iap_infinity_ forever"); } }
		public static string ice_cream_purchase 		{ get{ return LocalizationManager.GetTranslation ("ice_cream_purchase"); } }
		public static string icon 		{ get{ return LocalizationManager.GetTranslation ("icon"); } }
		public static string id_key 		{ get{ return LocalizationManager.GetTranslation ("id_key"); } }
		public static string info_item_active_skill 		{ get{ return LocalizationManager.GetTranslation ("info_item_active_skill"); } }
		public static string info_item_aircraft_general_card 		{ get{ return LocalizationManager.GetTranslation ("info_item_aircraft_general_card"); } }
		public static string info_item_aircraft_specific_card 		{ get{ return LocalizationManager.GetTranslation ("info_item_aircraft_specific_card"); } }
		public static string info_item_card_box 		{ get{ return LocalizationManager.GetTranslation ("info_item_card_box"); } }
		public static string info_item_drone_general_card 		{ get{ return LocalizationManager.GetTranslation ("info_item_drone_general_card"); } }
		public static string info_item_drone_specific_card 		{ get{ return LocalizationManager.GetTranslation ("info_item_drone_specific_card"); } }
		public static string info_item_energy 		{ get{ return LocalizationManager.GetTranslation ("info_item_energy"); } }
		public static string info_item_gem 		{ get{ return LocalizationManager.GetTranslation ("info_item_gem"); } }
		public static string info_item_gold 		{ get{ return LocalizationManager.GetTranslation ("info_item_gold"); } }
		public static string info_item_life 		{ get{ return LocalizationManager.GetTranslation ("info_item_life"); } }
		public static string info_item_power_up 		{ get{ return LocalizationManager.GetTranslation ("info_item_power_up"); } }
		public static string info_item_wing_general_card 		{ get{ return LocalizationManager.GetTranslation ("info_item_wing_general_card"); } }
		public static string info_item_wing_specific_card 		{ get{ return LocalizationManager.GetTranslation ("info_item_wing_specific_card"); } }
		public static string info_season_end 		{ get{ return LocalizationManager.GetTranslation ("info_season_end"); } }
		public static string info_season_start 		{ get{ return LocalizationManager.GetTranslation ("info_season_start"); } }
		public static string info_skill_10_fire_ball 		{ get{ return LocalizationManager.GetTranslation ("info_skill_10_fire_ball"); } }
		public static string info_skill_11_fury_blade 		{ get{ return LocalizationManager.GetTranslation ("info_skill_11_fury_blade"); } }
		public static string info_skill_12_phoenix_rise 		{ get{ return LocalizationManager.GetTranslation ("info_skill_12_phoenix_rise"); } }
		public static string info_skill_13_x_laser 		{ get{ return LocalizationManager.GetTranslation ("info_skill_13_x_laser"); } }
		public static string info_skill_14_double_riffle 		{ get{ return LocalizationManager.GetTranslation ("info_skill_14_double_riffle"); } }
		public static string info_skill_15_heavy_missle 		{ get{ return LocalizationManager.GetTranslation ("info_skill_15_heavy_missle"); } }
		public static string info_skill_16_laser_strike 		{ get{ return LocalizationManager.GetTranslation ("info_skill_16_laser_strike"); } }
		public static string info_skill_17_sonic_wave 		{ get{ return LocalizationManager.GetTranslation ("info_skill_17_sonic_wave"); } }
		public static string info_skill_18_x_galting 		{ get{ return LocalizationManager.GetTranslation ("info_skill_18_x_galting"); } }
		public static string info_skill_19_rain_of_bomb 		{ get{ return LocalizationManager.GetTranslation ("info_skill_19_rain_of_bomb"); } }
		public static string info_skill_1_upgrade_module 		{ get{ return LocalizationManager.GetTranslation ("info_skill_1_upgrade_module"); } }
		public static string info_skill_20_splash_shot 		{ get{ return LocalizationManager.GetTranslation ("info_skill_20_splash_shot"); } }
		public static string info_skill_21_ice_storm 		{ get{ return LocalizationManager.GetTranslation ("info_skill_21_ice_storm"); } }
		public static string info_skill_22_ice_missile 		{ get{ return LocalizationManager.GetTranslation ("info_skill_22_ice_missile"); } }
		public static string info_skill_23_judgement 		{ get{ return LocalizationManager.GetTranslation ("info_skill_23_judgement"); } }
		public static string info_skill_24_atonement 		{ get{ return LocalizationManager.GetTranslation ("info_skill_24_atonement"); } }
		public static string info_skill_25_magic_module 		{ get{ return LocalizationManager.GetTranslation ("info_skill_25_magic_module"); } }
		public static string info_skill_26_overgrowth_force 		{ get{ return LocalizationManager.GetTranslation ("info_skill_26_overgrowth_force"); } }
		public static string info_skill_27_power_unlock 		{ get{ return LocalizationManager.GetTranslation ("info_skill_27_power_unlock"); } }
		public static string info_skill_28_huge_cargo 		{ get{ return LocalizationManager.GetTranslation ("info_skill_28_huge_cargo"); } }
		public static string info_skill_29_skywrath_blade 		{ get{ return LocalizationManager.GetTranslation ("info_skill_29_skywrath_blade"); } }
		public static string info_skill_2_heavy_shell 		{ get{ return LocalizationManager.GetTranslation ("info_skill_2_heavy_shell"); } }
		public static string info_skill_30_ultra_shell 		{ get{ return LocalizationManager.GetTranslation ("info_skill_30_ultra_shell"); } }
		public static string info_skill_31_lightning_storm 		{ get{ return LocalizationManager.GetTranslation ("info_skill_31_lightning_storm"); } }
		public static string info_skill_32_lightning_strike 		{ get{ return LocalizationManager.GetTranslation ("info_skill_32_lightning_strike"); } }
		public static string info_skill_3_extra_power 		{ get{ return LocalizationManager.GetTranslation ("info_skill_3_extra_power"); } }
		public static string info_skill_4_fast_bullet 		{ get{ return LocalizationManager.GetTranslation ("info_skill_4_fast_bullet"); } }
		public static string info_skill_5_speed_fire 		{ get{ return LocalizationManager.GetTranslation ("info_skill_5_speed_fire"); } }
		public static string info_skill_6_mystic_power 		{ get{ return LocalizationManager.GetTranslation ("info_skill_6_mystic_power"); } }
		public static string info_skill_7_life_surge 		{ get{ return LocalizationManager.GetTranslation ("info_skill_7_life_surge"); } }
		public static string info_skill_8_big_cargo 		{ get{ return LocalizationManager.GetTranslation ("info_skill_8_big_cargo"); } }
		public static string info_skill_9_open_fire 		{ get{ return LocalizationManager.GetTranslation ("info_skill_9_open_fire"); } }
		public static string invite 		{ get{ return LocalizationManager.GetTranslation ("invite"); } }
		public static string invite_friends 		{ get{ return LocalizationManager.GetTranslation ("invite_friends"); } }
		public static string invited_to_2vs2 		{ get{ return LocalizationManager.GetTranslation ("invited_to_2vs2"); } }
		public static string item 		{ get{ return LocalizationManager.GetTranslation ("item"); } }
		public static string item_name_card_box 		{ get{ return LocalizationManager.GetTranslation ("item_name_card_box"); } }
		public static string item_name_card_replace 		{ get{ return LocalizationManager.GetTranslation ("item_name_card_replace"); } }
		public static string item_name_uni_aircraft_card 		{ get{ return LocalizationManager.GetTranslation ("item_name_uni_aircraft_card"); } }
		public static string item_name_uni_drone_card 		{ get{ return LocalizationManager.GetTranslation ("item_name_uni_drone_card"); } }
		public static string item_name_uni_wing_card 		{ get{ return LocalizationManager.GetTranslation ("item_name_uni_wing_card"); } }
		public static string join 		{ get{ return LocalizationManager.GetTranslation ("join"); } }
		public static string join_group 		{ get{ return LocalizationManager.GetTranslation ("join_group"); } }
		public static string join_request 		{ get{ return LocalizationManager.GetTranslation ("join_request"); } }
		public static string key_event 		{ get{ return LocalizationManager.GetTranslation ("key_event"); } }
		public static string key_pvp 		{ get{ return LocalizationManager.GetTranslation ("key_pvp"); } }
		public static string kick 		{ get{ return LocalizationManager.GetTranslation ("kick"); } }
		public static string lTry 		{ get{ return LocalizationManager.GetTranslation ("lTry"); } }
		public static string label_max 		{ get{ return LocalizationManager.GetTranslation ("label_max"); } }
		public static string label_notice 		{ get{ return LocalizationManager.GetTranslation ("label_notice"); } }
		public static string label_require 		{ get{ return LocalizationManager.GetTranslation ("label_require"); } }
		public static string label_total 		{ get{ return LocalizationManager.GetTranslation ("label_total"); } }
		public static string label_you_have 		{ get{ return LocalizationManager.GetTranslation ("label_you_have"); } }
		public static string leaderboard 		{ get{ return LocalizationManager.GetTranslation ("leaderboard"); } }
		public static string level 		{ get{ return LocalizationManager.GetTranslation ("level"); } }
		public static string level_extra_name 		{ get{ return LocalizationManager.GetTranslation ("level_extra_name"); } }
		public static string level_upgrade 		{ get{ return LocalizationManager.GetTranslation ("level_upgrade"); } }
		public static string life 		{ get{ return LocalizationManager.GetTranslation ("life"); } }
		public static string like_us 		{ get{ return LocalizationManager.GetTranslation ("like_us"); } }
		public static string live_score 		{ get{ return LocalizationManager.GetTranslation ("live_score"); } }
		public static string loading 		{ get{ return LocalizationManager.GetTranslation ("loading"); } }
		public static string loading_progress 		{ get{ return LocalizationManager.GetTranslation ("loading_progress"); } }
		public static string local 		{ get{ return LocalizationManager.GetTranslation ("local"); } }
		public static string login 		{ get{ return LocalizationManager.GetTranslation ("login"); } }
		public static string login_facebook 		{ get{ return LocalizationManager.GetTranslation ("login_facebook"); } }
		public static string login_gamecenter 		{ get{ return LocalizationManager.GetTranslation ("login_gamecenter"); } }
		public static string lose 		{ get{ return LocalizationManager.GetTranslation ("lose"); } }
		public static string losses 		{ get{ return LocalizationManager.GetTranslation ("losses"); } }
		public static string mail_to_system 		{ get{ return LocalizationManager.GetTranslation ("mail_to_system"); } }
		public static string max_reward 		{ get{ return LocalizationManager.GetTranslation ("max_reward"); } }
		public static string medal 		{ get{ return LocalizationManager.GetTranslation ("medal"); } }
		public static string member_invite 		{ get{ return LocalizationManager.GetTranslation ("member_invite"); } }
		public static string member_setting 		{ get{ return LocalizationManager.GetTranslation ("member_setting"); } }
		public static string mission_complete_the_map 		{ get{ return LocalizationManager.GetTranslation ("mission_complete_the_map"); } }
		public static string mission_defeat_all_enemies 		{ get{ return LocalizationManager.GetTranslation ("mission_defeat_all_enemies"); } }
		public static string mission_endless_bonusgold 		{ get{ return LocalizationManager.GetTranslation ("mission_endless_bonusgold"); } }
		public static string mission_endless_playtime 		{ get{ return LocalizationManager.GetTranslation ("mission_endless_playtime"); } }
		public static string mission_endless_wavereached 		{ get{ return LocalizationManager.GetTranslation ("mission_endless_wavereached"); } }
		public static string mission_stay_untouched 		{ get{ return LocalizationManager.GetTranslation ("mission_stay_untouched"); } }
		public static string ms_notifi_finish_event 		{ get{ return LocalizationManager.GetTranslation ("ms_notifi_finish_event"); } }
		public static string msg_2vs2_invitation 		{ get{ return LocalizationManager.GetTranslation ("msg_2vs2_invitation"); } }
		public static string msg_achievements 		{ get{ return LocalizationManager.GetTranslation ("msg_achievements"); } }
		public static string msg_aircraft_BataFD01 		{ get{ return LocalizationManager.GetTranslation ("msg_aircraft_BataFD01"); } }
		public static string msg_aircraft_FuryOfAres 		{ get{ return LocalizationManager.GetTranslation ("msg_aircraft_FuryOfAres"); } }
		public static string msg_aircraft_SkyWraith 		{ get{ return LocalizationManager.GetTranslation ("msg_aircraft_SkyWraith"); } }
		public static string msg_aircraft_ice_shard 		{ get{ return LocalizationManager.GetTranslation ("msg_aircraft_ice_shard"); } }
		public static string msg_aircraft_mac_bird 		{ get{ return LocalizationManager.GetTranslation ("msg_aircraft_mac_bird"); } }
		public static string msg_aircraft_starbomb 		{ get{ return LocalizationManager.GetTranslation ("msg_aircraft_starbomb"); } }
		public static string msg_aircraft_thunder_bolt 		{ get{ return LocalizationManager.GetTranslation ("msg_aircraft_thunder_bolt"); } }
		public static string msg_aircraft_twilight_x 		{ get{ return LocalizationManager.GetTranslation ("msg_aircraft_twilight_x"); } }
		public static string msg_aircraft_used_time_per_day 		{ get{ return LocalizationManager.GetTranslation ("msg_aircraft_used_time_per_day"); } }
		public static string msg_buy_ticket 		{ get{ return LocalizationManager.GetTranslation ("msg_buy_ticket"); } }
		public static string msg_buy_ticket_success 		{ get{ return LocalizationManager.GetTranslation ("msg_buy_ticket_success"); } }
		public static string msg_can_claim_after 		{ get{ return LocalizationManager.GetTranslation ("msg_can_claim_after"); } }
		public static string msg_cant_claim 		{ get{ return LocalizationManager.GetTranslation ("msg_cant_claim"); } }
		public static string msg_cant_donate_yourself 		{ get{ return LocalizationManager.GetTranslation ("msg_cant_donate_yourself"); } }
		public static string msg_card_double_in_chest 		{ get{ return LocalizationManager.GetTranslation ("msg_card_double_in_chest"); } }
		public static string msg_card_purchase_in_shop 		{ get{ return LocalizationManager.GetTranslation ("msg_card_purchase_in_shop"); } }
		public static string msg_challenge_friend 		{ get{ return LocalizationManager.GetTranslation ("msg_challenge_friend"); } }
		public static string msg_challenge_tournament 		{ get{ return LocalizationManager.GetTranslation ("msg_challenge_tournament"); } }
		public static string msg_change_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_change_clan"); } }
		public static string msg_choose_any_aircraft 		{ get{ return LocalizationManager.GetTranslation ("msg_choose_any_aircraft"); } }
		public static string msg_clan_cant_have_more_vice 		{ get{ return LocalizationManager.GetTranslation ("msg_clan_cant_have_more_vice"); } }
		public static string msg_clan_have_full_member 		{ get{ return LocalizationManager.GetTranslation ("msg_clan_have_full_member"); } }
		public static string msg_clan_not_exist 		{ get{ return LocalizationManager.GetTranslation ("msg_clan_not_exist"); } }
		public static string msg_code_invalid 		{ get{ return LocalizationManager.GetTranslation ("msg_code_invalid"); } }
		public static string msg_community_like_and_follow 		{ get{ return LocalizationManager.GetTranslation ("msg_community_like_and_follow"); } }
		public static string msg_community_more_friends 		{ get{ return LocalizationManager.GetTranslation ("msg_community_more_friends"); } }
		public static string msg_community_total_friends 		{ get{ return LocalizationManager.GetTranslation ("msg_community_total_friends"); } }
		public static string msg_confirm_command 		{ get{ return LocalizationManager.GetTranslation ("msg_confirm_command"); } }
		public static string msg_congratulation_create_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_congratulation_create_clan"); } }
		public static string msg_congratulation_vip 		{ get{ return LocalizationManager.GetTranslation ("msg_congratulation_vip"); } }
		public static string msg_content_cant_empty 		{ get{ return LocalizationManager.GetTranslation ("msg_content_cant_empty"); } }
		public static string msg_create_and_lead 		{ get{ return LocalizationManager.GetTranslation ("msg_create_and_lead"); } }
		public static string msg_daily_login 		{ get{ return LocalizationManager.GetTranslation ("msg_daily_login"); } }
		public static string msg_daily_quest 		{ get{ return LocalizationManager.GetTranslation ("msg_daily_quest"); } }
		public static string msg_description_glory_chest 		{ get{ return LocalizationManager.GetTranslation ("msg_description_glory_chest"); } }
		public static string msg_description_point_glory 		{ get{ return LocalizationManager.GetTranslation ("msg_description_point_glory"); } }
		public static string msg_dont_have_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_dont_have_clan"); } }
		public static string msg_downgrading_member 		{ get{ return LocalizationManager.GetTranslation ("msg_downgrading_member"); } }
		public static string msg_end_season_rank_reward 		{ get{ return LocalizationManager.GetTranslation ("msg_end_season_rank_reward"); } }
		public static string msg_endless_objective 		{ get{ return LocalizationManager.GetTranslation ("msg_endless_objective"); } }
		public static string msg_enter_id 		{ get{ return LocalizationManager.GetTranslation ("msg_enter_id"); } }
		public static string msg_enter_user_id 		{ get{ return LocalizationManager.GetTranslation ("msg_enter_user_id"); } }
		public static string msg_event_will_end_in 		{ get{ return LocalizationManager.GetTranslation ("msg_event_will_end_in"); } }
		public static string msg_exit_popoup 		{ get{ return LocalizationManager.GetTranslation ("msg_exit_popoup"); } }
		public static string msg_extra_level_have_to_use 		{ get{ return LocalizationManager.GetTranslation ("msg_extra_level_have_to_use"); } }
		public static string msg_force_update_version 		{ get{ return LocalizationManager.GetTranslation ("msg_force_update_version"); } }
		public static string msg_friends_passed_level 		{ get{ return LocalizationManager.GetTranslation ("msg_friends_passed_level"); } }
		public static string msg_gain_a_ticket 		{ get{ return LocalizationManager.GetTranslation ("msg_gain_a_ticket"); } }
		public static string msg_have_used_reset 		{ get{ return LocalizationManager.GetTranslation ("msg_have_used_reset"); } }
		public static string msg_invitation_not_exist 		{ get{ return LocalizationManager.GetTranslation ("msg_invitation_not_exist"); } }
		public static string msg_invite_duplicate_invite 		{ get{ return LocalizationManager.GetTranslation ("msg_invite_duplicate_invite"); } }
		public static string msg_invite_server_maintain 		{ get{ return LocalizationManager.GetTranslation ("msg_invite_server_maintain"); } }
		public static string msg_invite_timeout 		{ get{ return LocalizationManager.GetTranslation ("msg_invite_timeout"); } }
		public static string msg_invite_user_busy 		{ get{ return LocalizationManager.GetTranslation ("msg_invite_user_busy"); } }
		public static string msg_invite_user_offline 		{ get{ return LocalizationManager.GetTranslation ("msg_invite_user_offline"); } }
		public static string msg_item_not_in_shop 		{ get{ return LocalizationManager.GetTranslation ("msg_item_not_in_shop"); } }
		public static string msg_item_sold_out 		{ get{ return LocalizationManager.GetTranslation ("msg_item_sold_out"); } }
		public static string msg_join_team_to_open 		{ get{ return LocalizationManager.GetTranslation ("msg_join_team_to_open"); } }
		public static string msg_kick_member 		{ get{ return LocalizationManager.GetTranslation ("msg_kick_member"); } }
		public static string msg_level_halloween_please 		{ get{ return LocalizationManager.GetTranslation ("msg_level_halloween_please"); } }
		public static string msg_level_halloween_rules_1 		{ get{ return LocalizationManager.GetTranslation ("msg_level_halloween_rules_1"); } }
		public static string msg_level_halloween_rules_2 		{ get{ return LocalizationManager.GetTranslation ("msg_level_halloween_rules_2"); } }
		public static string msg_level_halloween_rules_3 		{ get{ return LocalizationManager.GetTranslation ("msg_level_halloween_rules_3"); } }
		public static string msg_level_halloween_rules_4 		{ get{ return LocalizationManager.GetTranslation ("msg_level_halloween_rules_4"); } }
		public static string msg_level_xmas_rule_1 		{ get{ return LocalizationManager.GetTranslation ("msg_level_xmas_rule_1"); } }
		public static string msg_level_xmas_rule_2 		{ get{ return LocalizationManager.GetTranslation ("msg_level_xmas_rule_2"); } }
		public static string msg_level_xmas_rule_3 		{ get{ return LocalizationManager.GetTranslation ("msg_level_xmas_rule_3"); } }
		public static string msg_load_chat_history_error 		{ get{ return LocalizationManager.GetTranslation ("msg_load_chat_history_error"); } }
		public static string msg_log_in_game_online_to_receive 		{ get{ return LocalizationManager.GetTranslation ("msg_log_in_game_online_to_receive"); } }
		public static string msg_login_facebook 		{ get{ return LocalizationManager.GetTranslation ("msg_login_facebook"); } }
		public static string msg_lose_3_battle_in_a_row 		{ get{ return LocalizationManager.GetTranslation ("msg_lose_3_battle_in_a_row"); } }
		public static string msg_more_gold 		{ get{ return LocalizationManager.GetTranslation ("msg_more_gold"); } }
		public static string msg_more_point_to_vip 		{ get{ return LocalizationManager.GetTranslation ("msg_more_point_to_vip"); } }
		public static string msg_name_exists_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_name_exists_clan"); } }
		public static string msg_name_invalid 		{ get{ return LocalizationManager.GetTranslation ("msg_name_invalid"); } }
		public static string msg_need_more_card_number 		{ get{ return LocalizationManager.GetTranslation ("msg_need_more_card_number"); } }
		public static string msg_need_more_gem_number 		{ get{ return LocalizationManager.GetTranslation ("msg_need_more_gem_number"); } }
		public static string msg_need_set_to_master 		{ get{ return LocalizationManager.GetTranslation ("msg_need_set_to_master"); } }
		public static string msg_no_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_no_clan"); } }
		public static string msg_no_clan_suggest 		{ get{ return LocalizationManager.GetTranslation ("msg_no_clan_suggest"); } }
		public static string msg_no_donate_info 		{ get{ return LocalizationManager.GetTranslation ("msg_no_donate_info"); } }
		public static string msg_no_follow 		{ get{ return LocalizationManager.GetTranslation ("msg_no_follow"); } }
		public static string msg_no_friends 		{ get{ return LocalizationManager.GetTranslation ("msg_no_friends"); } }
		public static string msg_no_request 		{ get{ return LocalizationManager.GetTranslation ("msg_no_request"); } }
		public static string msg_not_available_yet 		{ get{ return LocalizationManager.GetTranslation ("msg_not_available_yet"); } }
		public static string msg_not_enough_card 		{ get{ return LocalizationManager.GetTranslation ("msg_not_enough_card"); } }
		public static string msg_not_enough_level 		{ get{ return LocalizationManager.GetTranslation ("msg_not_enough_level"); } }
		public static string msg_not_enough_medal 		{ get{ return LocalizationManager.GetTranslation ("msg_not_enough_medal"); } }
		public static string msg_not_enough_reset_quota 		{ get{ return LocalizationManager.GetTranslation ("msg_not_enough_reset_quota"); } }
		public static string msg_not_enough_ticket 		{ get{ return LocalizationManager.GetTranslation ("msg_not_enough_ticket"); } }
		public static string msg_not_member_in_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_not_member_in_clan"); } }
		public static string msg_not_started_yet 		{ get{ return LocalizationManager.GetTranslation ("msg_not_started_yet"); } }
		public static string msg_notice_in_trial 		{ get{ return LocalizationManager.GetTranslation ("msg_notice_in_trial"); } }
		public static string msg_npc_videoendgame 		{ get{ return LocalizationManager.GetTranslation ("msg_npc_videoendgame"); } }
		public static string msg_permission_error 		{ get{ return LocalizationManager.GetTranslation ("msg_permission_error"); } }
		public static string msg_play_with_friend 		{ get{ return LocalizationManager.GetTranslation ("msg_play_with_friend"); } }
		public static string msg_player_joined_other_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_player_joined_other_clan"); } }
		public static string msg_player_not_exist 		{ get{ return LocalizationManager.GetTranslation ("msg_player_not_exist"); } }
		public static string msg_player_was_member_your_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_player_was_member_your_clan"); } }
		public static string msg_please_wait_from_server 		{ get{ return LocalizationManager.GetTranslation ("msg_please_wait_from_server"); } }
		public static string msg_premium_pack 		{ get{ return LocalizationManager.GetTranslation ("msg_premium_pack"); } }
		public static string msg_premium_pack_time_again 		{ get{ return LocalizationManager.GetTranslation ("msg_premium_pack_time_again"); } }
		public static string msg_premium_pack_time_expired 		{ get{ return LocalizationManager.GetTranslation ("msg_premium_pack_time_expired"); } }
		public static string msg_purchase 		{ get{ return LocalizationManager.GetTranslation ("msg_purchase"); } }
		public static string msg_purchase_got_extra_discount 		{ get{ return LocalizationManager.GetTranslation ("msg_purchase_got_extra_discount"); } }
		public static string msg_purchase_to_get_extra_discount 		{ get{ return LocalizationManager.GetTranslation ("msg_purchase_to_get_extra_discount"); } }
		public static string msg_pvp_connect_to_server 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_connect_to_server"); } }
		public static string msg_pvp_empty_mail_box 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_empty_mail_box"); } }
		public static string msg_pvp_finding_opponents 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_finding_opponents"); } }
		public static string msg_pvp_invitation 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_invitation"); } }
		public static string msg_pvp_no_opponents 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_no_opponents"); } }
		public static string msg_pvp_rank_down 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_rank_down"); } }
		public static string msg_pvp_rank_up 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_rank_up"); } }
		public static string msg_pvp_remove_data_client 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_remove_data_client"); } }
		public static string msg_pvp_remove_data_server 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_remove_data_server"); } }
		public static string msg_pvp_sure_sync_data 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_sure_sync_data"); } }
		public static string msg_pvp_sync_data 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_sync_data"); } }
		public static string msg_pvp_sync_data_other 		{ get{ return LocalizationManager.GetTranslation ("msg_pvp_sync_data_other"); } }
		public static string msg_quit_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_quit_clan"); } }
		public static string msg_rate_enjoy_ques 		{ get{ return LocalizationManager.GetTranslation ("msg_rate_enjoy_ques"); } }
		public static string msg_rate_free_updates 		{ get{ return LocalizationManager.GetTranslation ("msg_rate_free_updates"); } }
		public static string msg_rate_rate_ques 		{ get{ return LocalizationManager.GetTranslation ("msg_rate_rate_ques"); } }
		public static string msg_rate_thanks 		{ get{ return LocalizationManager.GetTranslation ("msg_rate_thanks"); } }
		public static string msg_reach_max_vip 		{ get{ return LocalizationManager.GetTranslation ("msg_reach_max_vip"); } }
		public static string msg_receive_reward 		{ get{ return LocalizationManager.GetTranslation ("msg_receive_reward"); } }
		public static string msg_receive_up_to 		{ get{ return LocalizationManager.GetTranslation ("msg_receive_up_to"); } }
		public static string msg_replace_all_current_item_with_random 		{ get{ return LocalizationManager.GetTranslation ("msg_replace_all_current_item_with_random"); } }
		public static string msg_request_cooldown_not_over_yet 		{ get{ return LocalizationManager.GetTranslation ("msg_request_cooldown_not_over_yet"); } }
		public static string msg_request_duplicate 		{ get{ return LocalizationManager.GetTranslation ("msg_request_duplicate"); } }
		public static string msg_request_fully 		{ get{ return LocalizationManager.GetTranslation ("msg_request_fully"); } }
		public static string msg_request_no_longer_available 		{ get{ return LocalizationManager.GetTranslation ("msg_request_no_longer_available"); } }
		public static string msg_request_not_enough_quota 		{ get{ return LocalizationManager.GetTranslation ("msg_request_not_enough_quota"); } }
		public static string msg_reset_more 		{ get{ return LocalizationManager.GetTranslation ("msg_reset_more"); } }
		public static string msg_reward_can_claim_after 		{ get{ return LocalizationManager.GetTranslation ("msg_reward_can_claim_after"); } }
		public static string msg_reward_can_only_claimed_once 		{ get{ return LocalizationManager.GetTranslation ("msg_reward_can_only_claimed_once"); } }
		public static string msg_reward_is_base 		{ get{ return LocalizationManager.GetTranslation ("msg_reward_is_base"); } }
		public static string msg_season_score_will_be_reset 		{ get{ return LocalizationManager.GetTranslation ("msg_season_score_will_be_reset"); } }
		public static string msg_select_card_type 		{ get{ return LocalizationManager.GetTranslation ("msg_select_card_type"); } }
		public static string msg_select_power_up 		{ get{ return LocalizationManager.GetTranslation ("msg_select_power_up"); } }
		public static string msg_shop_will_be_reset 		{ get{ return LocalizationManager.GetTranslation ("msg_shop_will_be_reset"); } }
		public static string msg_stars_to_unlock 		{ get{ return LocalizationManager.GetTranslation ("msg_stars_to_unlock"); } }
		public static string msg_starter_pack 		{ get{ return LocalizationManager.GetTranslation ("msg_starter_pack"); } }
		public static string msg_starter_pack_warning 		{ get{ return LocalizationManager.GetTranslation ("msg_starter_pack_warning"); } }
		public static string msg_thank_send_mail 		{ get{ return LocalizationManager.GetTranslation ("msg_thank_send_mail"); } }
		public static string msg_the_more_win_get 		{ get{ return LocalizationManager.GetTranslation ("msg_the_more_win_get"); } }
		public static string msg_to_build_team 		{ get{ return LocalizationManager.GetTranslation ("msg_to_build_team"); } }
		public static string msg_tourmanet_not_start_yet 		{ get{ return LocalizationManager.GetTranslation ("msg_tourmanet_not_start_yet"); } }
		public static string msg_unlock_aircraft_by_collect_card 		{ get{ return LocalizationManager.GetTranslation ("msg_unlock_aircraft_by_collect_card"); } }
		public static string msg_unlock_and_win_battles 		{ get{ return LocalizationManager.GetTranslation ("msg_unlock_and_win_battles"); } }
		public static string msg_unlock_bonus_reward_first 		{ get{ return LocalizationManager.GetTranslation ("msg_unlock_bonus_reward_first"); } }
		public static string msg_unlock_by_card 		{ get{ return LocalizationManager.GetTranslation ("msg_unlock_by_card"); } }
		public static string msg_unlock_by_level 		{ get{ return LocalizationManager.GetTranslation ("msg_unlock_by_level"); } }
		public static string msg_unlock_evolve_claim_reward 		{ get{ return LocalizationManager.GetTranslation ("msg_unlock_evolve_claim_reward"); } }
		public static string msg_unlock_level_to_purchase 		{ get{ return LocalizationManager.GetTranslation ("msg_unlock_level_to_purchase"); } }
		public static string msg_upgrade 		{ get{ return LocalizationManager.GetTranslation ("msg_upgrade"); } }
		public static string msg_upgrading_member 		{ get{ return LocalizationManager.GetTranslation ("msg_upgrading_member"); } }
		public static string msg_use_another_aircraft 		{ get{ return LocalizationManager.GetTranslation ("msg_use_another_aircraft"); } }
		public static string msg_video_reward 		{ get{ return LocalizationManager.GetTranslation ("msg_video_reward"); } }
		public static string msg_vip_pack 		{ get{ return LocalizationManager.GetTranslation ("msg_vip_pack"); } }
		public static string msg_wait_opponent 		{ get{ return LocalizationManager.GetTranslation ("msg_wait_opponent"); } }
		public static string msg_wait_until_battle_end 		{ get{ return LocalizationManager.GetTranslation ("msg_wait_until_battle_end"); } }
		public static string msg_wait_until_ready 		{ get{ return LocalizationManager.GetTranslation ("msg_wait_until_ready"); } }
		public static string msg_win_battle_first 		{ get{ return LocalizationManager.GetTranslation ("msg_win_battle_first"); } }
		public static string msg_win_need_next_reward 		{ get{ return LocalizationManager.GetTranslation ("msg_win_need_next_reward"); } }
		public static string msg_win_the_battle_to_get_reward 		{ get{ return LocalizationManager.GetTranslation ("msg_win_the_battle_to_get_reward"); } }
		public static string msg_wingman_auto_galting_gun 		{ get{ return LocalizationManager.GetTranslation ("msg_wingman_auto_galting_gun"); } }
		public static string msg_wingman_double_galting 		{ get{ return LocalizationManager.GetTranslation ("msg_wingman_double_galting"); } }
		public static string msg_wingman_gatling_gun 		{ get{ return LocalizationManager.GetTranslation ("msg_wingman_gatling_gun"); } }
		public static string msg_wingman_homing_missile 		{ get{ return LocalizationManager.GetTranslation ("msg_wingman_homing_missile"); } }
		public static string msg_wingman_lazer 		{ get{ return LocalizationManager.GetTranslation ("msg_wingman_lazer"); } }
		public static string msg_wingman_splasher 		{ get{ return LocalizationManager.GetTranslation ("msg_wingman_splasher"); } }
		public static string msg_you_can_request_8_hours 		{ get{ return LocalizationManager.GetTranslation ("msg_you_can_request_8_hours"); } }
		public static string msg_you_claimed 		{ get{ return LocalizationManager.GetTranslation ("msg_you_claimed"); } }
		public static string msg_you_had_in_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_you_had_in_clan"); } }
		public static string msg_you_had_requested 		{ get{ return LocalizationManager.GetTranslation ("msg_you_had_requested"); } }
		public static string msg_you_have_invited_to_friend 		{ get{ return LocalizationManager.GetTranslation ("msg_you_have_invited_to_friend"); } }
		public static string msg_you_have_to_set_master 		{ get{ return LocalizationManager.GetTranslation ("msg_you_have_to_set_master"); } }
		public static string msg_you_not_member_in_clan 		{ get{ return LocalizationManager.GetTranslation ("msg_you_not_member_in_clan"); } }
		public static string msg_your_clan_full 		{ get{ return LocalizationManager.GetTranslation ("msg_your_clan_full"); } }
		public static string msg_your_rank 		{ get{ return LocalizationManager.GetTranslation ("msg_your_rank"); } }
		public static string msg_your_teammate_out 		{ get{ return LocalizationManager.GetTranslation ("msg_your_teammate_out"); } }
		public static string music 		{ get{ return LocalizationManager.GetTranslation ("music"); } }
		public static string name_request 		{ get{ return LocalizationManager.GetTranslation ("name_request"); } }
		public static string next 		{ get{ return LocalizationManager.GetTranslation ("next"); } }
		public static string no 		{ get{ return LocalizationManager.GetTranslation ("no"); } }
		public static string no_available_exchange 		{ get{ return LocalizationManager.GetTranslation ("no_available_exchange"); } }
		public static string no_one_has_donate 		{ get{ return LocalizationManager.GetTranslation ("no_one_has_donate"); } }
		public static string normal 		{ get{ return LocalizationManager.GetTranslation ("normal"); } }
		public static string notifi_ads_not_available 		{ get{ return LocalizationManager.GetTranslation ("notifi_ads_not_available"); } }
		public static string notifi_claimed_reward 		{ get{ return LocalizationManager.GetTranslation ("notifi_claimed_reward"); } }
		public static string notifi_comeback_tomorrow 		{ get{ return LocalizationManager.GetTranslation ("notifi_comeback_tomorrow"); } }
		public static string notifi_emp_activated 		{ get{ return LocalizationManager.GetTranslation ("notifi_emp_activated"); } }
		public static string notifi_enter_giftcode 		{ get{ return LocalizationManager.GetTranslation ("notifi_enter_giftcode"); } }
		public static string notifi_failed_to_connect 		{ get{ return LocalizationManager.GetTranslation ("notifi_failed_to_connect"); } }
		public static string notifi_get_gold_infinity_pack 		{ get{ return LocalizationManager.GetTranslation ("notifi_get_gold_infinity_pack"); } }
		public static string notifi_giftcode_notfound 		{ get{ return LocalizationManager.GetTranslation ("notifi_giftcode_notfound"); } }
		public static string notifi_giftcode_used 		{ get{ return LocalizationManager.GetTranslation ("notifi_giftcode_used"); } }
		public static string notifi_login_facebook_fail 		{ get{ return LocalizationManager.GetTranslation ("notifi_login_facebook_fail"); } }
		public static string notifi_login_facebook_succeeded 		{ get{ return LocalizationManager.GetTranslation ("notifi_login_facebook_succeeded"); } }
		public static string notifi_login_gamecenter_fail 		{ get{ return LocalizationManager.GetTranslation ("notifi_login_gamecenter_fail"); } }
		public static string notifi_login_gamecenter_succeeded 		{ get{ return LocalizationManager.GetTranslation ("notifi_login_gamecenter_succeeded"); } }
		public static string notifi_luckywheel_need_more_ticket 		{ get{ return LocalizationManager.GetTranslation ("notifi_luckywheel_need_more_ticket"); } }
		public static string notifi_luckywheel_wait_spin 		{ get{ return LocalizationManager.GetTranslation ("notifi_luckywheel_wait_spin"); } }
		public static string notifi_need_evolve_spaceship 		{ get{ return LocalizationManager.GetTranslation ("notifi_need_evolve_spaceship"); } }
		public static string notifi_need_evolve_wing 		{ get{ return LocalizationManager.GetTranslation ("notifi_need_evolve_wing"); } }
		public static string notifi_need_evolve_wingman 		{ get{ return LocalizationManager.GetTranslation ("notifi_need_evolve_wingman"); } }
		public static string notifi_need_more_friend 		{ get{ return LocalizationManager.GetTranslation ("notifi_need_more_friend"); } }
		public static string notifi_need_more_loginday 		{ get{ return LocalizationManager.GetTranslation ("notifi_need_more_loginday"); } }
		public static string notifi_network_error 		{ get{ return LocalizationManager.GetTranslation ("notifi_network_error"); } }
		public static string notifi_network_not_found 		{ get{ return LocalizationManager.GetTranslation ("notifi_network_not_found"); } }
		public static string notifi_not_enough_card 		{ get{ return LocalizationManager.GetTranslation ("notifi_not_enough_card"); } }
		public static string notifi_not_enough_coin 		{ get{ return LocalizationManager.GetTranslation ("notifi_not_enough_coin"); } }
		public static string notifi_not_enough_gem 		{ get{ return LocalizationManager.GetTranslation ("notifi_not_enough_gem"); } }
		public static string notifi_not_enough_resources 		{ get{ return LocalizationManager.GetTranslation ("notifi_not_enough_resources"); } }
		public static string notifi_purchases_restored_failed 		{ get{ return LocalizationManager.GetTranslation ("notifi_purchases_restored_failed"); } }
		public static string notifi_purchases_restored_success 		{ get{ return LocalizationManager.GetTranslation ("notifi_purchases_restored_success"); } }
		public static string notifi_pvp_character 		{ get{ return LocalizationManager.GetTranslation ("notifi_pvp_character"); } }
		public static string notifi_pvp_coming_soon 		{ get{ return LocalizationManager.GetTranslation ("notifi_pvp_coming_soon"); } }
		public static string notifi_pvp_disconnect_server 		{ get{ return LocalizationManager.GetTranslation ("notifi_pvp_disconnect_server"); } }
		public static string notifi_pvp_more_gold 		{ get{ return LocalizationManager.GetTranslation ("notifi_pvp_more_gold"); } }
		public static string notifi_pvp_room_destroyed 		{ get{ return LocalizationManager.GetTranslation ("notifi_pvp_room_destroyed"); } }
		public static string notifi_shop_upgraded 		{ get{ return LocalizationManager.GetTranslation ("notifi_shop_upgraded"); } }
		public static string notifi_unlock_aircraft_first 		{ get{ return LocalizationManager.GetTranslation ("notifi_unlock_aircraft_first"); } }
		public static string notifi_unlock_device_name 		{ get{ return LocalizationManager.GetTranslation ("notifi_unlock_device_name"); } }
		public static string notifi_unlock_wing_first 		{ get{ return LocalizationManager.GetTranslation ("notifi_unlock_wing_first"); } }
		public static string notifi_unlock_wingman_first 		{ get{ return LocalizationManager.GetTranslation ("notifi_unlock_wingman_first"); } }
		public static string notifi_videoads_tomorrow 		{ get{ return LocalizationManager.GetTranslation ("notifi_videoads_tomorrow"); } }
		public static string notifi_videoads_wait 		{ get{ return LocalizationManager.GetTranslation ("notifi_videoads_wait"); } }
		public static string notification 		{ get{ return LocalizationManager.GetTranslation ("notification"); } }
		public static string npc_popup_tutorial_mess 		{ get{ return LocalizationManager.GetTranslation ("npc_popup_tutorial_mess"); } }
		public static string npc_popup_tutorial_title 		{ get{ return LocalizationManager.GetTranslation ("npc_popup_tutorial_title"); } }
		public static string objective 		{ get{ return LocalizationManager.GetTranslation ("objective"); } }
		public static string offline 		{ get{ return LocalizationManager.GetTranslation ("offline"); } }
		public static string old 		{ get{ return LocalizationManager.GetTranslation ("old"); } }
		public static string oneTimeOffer 		{ get{ return LocalizationManager.GetTranslation ("oneTimeOffer"); } }
		public static string ongoing 		{ get{ return LocalizationManager.GetTranslation ("ongoing"); } }
		public static string online 		{ get{ return LocalizationManager.GetTranslation ("online"); } }
		public static string only_today 		{ get{ return LocalizationManager.GetTranslation ("only_today"); } }
		public static string open 		{ get{ return LocalizationManager.GetTranslation ("open"); } }
		public static string open_in 		{ get{ return LocalizationManager.GetTranslation ("open_in"); } }
		public static string opponent_disconnect 		{ get{ return LocalizationManager.GetTranslation ("opponent_disconnect"); } }
		public static string options 		{ get{ return LocalizationManager.GetTranslation ("options"); } }
		public static string or 		{ get{ return LocalizationManager.GetTranslation ("or"); } }
		public static string or_reset_now 		{ get{ return LocalizationManager.GetTranslation ("or_reset_now"); } }
		public static string out_of_reset 		{ get{ return LocalizationManager.GetTranslation ("out_of_reset"); } }
		public static string panel_congrat_new_aircraft 		{ get{ return LocalizationManager.GetTranslation ("panel_congrat_new_aircraft"); } }
		public static string panel_congrat_new_drone 		{ get{ return LocalizationManager.GetTranslation ("panel_congrat_new_drone"); } }
		public static string panel_congrat_new_rank_aircraft 		{ get{ return LocalizationManager.GetTranslation ("panel_congrat_new_rank_aircraft"); } }
		public static string panel_congrat_new_rank_drone 		{ get{ return LocalizationManager.GetTranslation ("panel_congrat_new_rank_drone"); } }
		public static string panel_congrat_new_rank_wing 		{ get{ return LocalizationManager.GetTranslation ("panel_congrat_new_rank_wing"); } }
		public static string panel_congrat_new_wing 		{ get{ return LocalizationManager.GetTranslation ("panel_congrat_new_wing"); } }
		public static string pass_level 		{ get{ return LocalizationManager.GetTranslation ("pass_level"); } }
		public static string pause 		{ get{ return LocalizationManager.GetTranslation ("pause"); } }
		public static string play 		{ get{ return LocalizationManager.GetTranslation ("play"); } }
		public static string player 		{ get{ return LocalizationManager.GetTranslation ("player"); } }
		public static string popup_30_days_already_earned 		{ get{ return LocalizationManager.GetTranslation ("popup_30_days_already_earned"); } }
		public static string popup_evolve_max_level 		{ get{ return LocalizationManager.GetTranslation ("popup_evolve_max_level"); } }
		public static string popup_iap_purchase_remove_ads 		{ get{ return LocalizationManager.GetTranslation ("popup_iap_purchase_remove_ads"); } }
		public static string popup_vip_100_percent_drop_gold 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_100_percent_drop_gold"); } }
		public static string popup_vip_10_percent_plane_damage 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_10_percent_plane_damage"); } }
		public static string popup_vip_1_life_per_day 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_1_life_per_day"); } }
		public static string popup_vip_1_power_up_per_day 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_1_power_up_per_day"); } }
		public static string popup_vip_1_spin_per_day 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_1_spin_per_day"); } }
		public static string popup_vip_20_percent_drone_damage 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_20_percent_drone_damage"); } }
		public static string popup_vip_2_percent_plane_damage 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_2_percent_plane_damage"); } }
		public static string popup_vip_30_percent_drop_gold 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_30_percent_drop_gold"); } }
		public static string popup_vip_50_percent_drop_gold 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_50_percent_drop_gold"); } }
		public static string popup_vip_5_percent_plane_damage 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_5_percent_plane_damage"); } }
		public static string popup_vip_benefit 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_benefit"); } }
		public static string popup_vip_buy_to_get_vip_points 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_buy_to_get_vip_points"); } }
		public static string popup_vip_features 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_features"); } }
		public static string popup_vip_need_more_points 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_need_more_points"); } }
		public static string popup_vip_progress 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_progress"); } }
		public static string popup_vip_title 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_title"); } }
		public static string popup_vip_x2_reward_daily_task 		{ get{ return LocalizationManager.GetTranslation ("popup_vip_x2_reward_daily_task"); } }
		public static string power 		{ get{ return LocalizationManager.GetTranslation ("power"); } }
		public static string power_up 		{ get{ return LocalizationManager.GetTranslation ("power_up"); } }
		public static string prepare 		{ get{ return LocalizationManager.GetTranslation ("prepare"); } }
		public static string promote_event 		{ get{ return LocalizationManager.GetTranslation ("promote_event"); } }
		public static string purchase 		{ get{ return LocalizationManager.GetTranslation ("purchase"); } }
		public static string purchaseToGetVip 		{ get{ return LocalizationManager.GetTranslation ("purchaseToGetVip"); } }
		public static string pvp_rank_reward 		{ get{ return LocalizationManager.GetTranslation ("pvp_rank_reward"); } }
		public static string quantity 		{ get{ return LocalizationManager.GetTranslation ("quantity"); } }
		public static string quantity_key 		{ get{ return LocalizationManager.GetTranslation ("quantity_key"); } }
		public static string quit 		{ get{ return LocalizationManager.GetTranslation ("quit"); } }
		public static string rank 		{ get{ return LocalizationManager.GetTranslation ("rank"); } }
		public static string rank_1st 		{ get{ return LocalizationManager.GetTranslation ("rank_1st"); } }
		public static string rank_2nd 		{ get{ return LocalizationManager.GetTranslation ("rank_2nd"); } }
		public static string rank_3rd 		{ get{ return LocalizationManager.GetTranslation ("rank_3rd"); } }
		public static string rank_4th 		{ get{ return LocalizationManager.GetTranslation ("rank_4th"); } }
		public static string rank_a 		{ get{ return LocalizationManager.GetTranslation ("rank_a"); } }
		public static string rank_b 		{ get{ return LocalizationManager.GetTranslation ("rank_b"); } }
		public static string rank_c 		{ get{ return LocalizationManager.GetTranslation ("rank_c"); } }
		public static string rank_pvp_bronze 		{ get{ return LocalizationManager.GetTranslation ("rank_pvp_bronze"); } }
		public static string rank_pvp_challenger 		{ get{ return LocalizationManager.GetTranslation ("rank_pvp_challenger"); } }
		public static string rank_pvp_diamond 		{ get{ return LocalizationManager.GetTranslation ("rank_pvp_diamond"); } }
		public static string rank_pvp_gold 		{ get{ return LocalizationManager.GetTranslation ("rank_pvp_gold"); } }
		public static string rank_pvp_legend 		{ get{ return LocalizationManager.GetTranslation ("rank_pvp_legend"); } }
		public static string rank_pvp_master 		{ get{ return LocalizationManager.GetTranslation ("rank_pvp_master"); } }
		public static string rank_pvp_platinum 		{ get{ return LocalizationManager.GetTranslation ("rank_pvp_platinum"); } }
		public static string rank_pvp_silver 		{ get{ return LocalizationManager.GetTranslation ("rank_pvp_silver"); } }
		public static string rank_s 		{ get{ return LocalizationManager.GetTranslation ("rank_s"); } }
		public static string rank_ss 		{ get{ return LocalizationManager.GetTranslation ("rank_ss"); } }
		public static string rank_sss 		{ get{ return LocalizationManager.GetTranslation ("rank_sss"); } }
		public static string read_less 		{ get{ return LocalizationManager.GetTranslation ("read_less"); } }
		public static string read_more 		{ get{ return LocalizationManager.GetTranslation ("read_more"); } }
		public static string ready 		{ get{ return LocalizationManager.GetTranslation ("ready"); } }
		public static string receive_when_tournament_end 		{ get{ return LocalizationManager.GetTranslation ("receive_when_tournament_end"); } }
		public static string register 		{ get{ return LocalizationManager.GetTranslation ("register"); } }
		public static string reload 		{ get{ return LocalizationManager.GetTranslation ("reload"); } }
		public static string replay 		{ get{ return LocalizationManager.GetTranslation ("replay"); } }
		public static string request 		{ get{ return LocalizationManager.GetTranslation ("request"); } }
		public static string request_sent 		{ get{ return LocalizationManager.GetTranslation ("request_sent"); } }
		public static string required_level 		{ get{ return LocalizationManager.GetTranslation ("required_level"); } }
		public static string requirements 		{ get{ return LocalizationManager.GetTranslation ("requirements"); } }
		public static string reset_in 		{ get{ return LocalizationManager.GetTranslation ("reset_in"); } }
		public static string reset_shop 		{ get{ return LocalizationManager.GetTranslation ("reset_shop"); } }
		public static string result 		{ get{ return LocalizationManager.GetTranslation ("result"); } }
		public static string reward 		{ get{ return LocalizationManager.GetTranslation ("reward"); } }
		public static string rewards_are_unlocked 		{ get{ return LocalizationManager.GetTranslation ("rewards_are_unlocked"); } }
		public static string royal_pack_extra_aircraft_skill 		{ get{ return LocalizationManager.GetTranslation ("royal_pack_extra_aircraft_skill"); } }
		public static string royal_pack_free_golden_chest 		{ get{ return LocalizationManager.GetTranslation ("royal_pack_free_golden_chest"); } }
		public static string royal_pack_gem_daily 		{ get{ return LocalizationManager.GetTranslation ("royal_pack_gem_daily"); } }
		public static string royal_pack_gem_instantly 		{ get{ return LocalizationManager.GetTranslation ("royal_pack_gem_instantly"); } }
		public static string royal_pack_gem_total 		{ get{ return LocalizationManager.GetTranslation ("royal_pack_gem_total"); } }
		public static string royal_pack_revive_discount 		{ get{ return LocalizationManager.GetTranslation ("royal_pack_revive_discount"); } }
		public static string royalty_pack_title 		{ get{ return LocalizationManager.GetTranslation ("royalty_pack_title"); } }
		public static string rule 		{ get{ return LocalizationManager.GetTranslation ("rule"); } }
		public static string save 		{ get{ return LocalizationManager.GetTranslation ("save"); } }
		public static string save_change 		{ get{ return LocalizationManager.GetTranslation ("save_change"); } }
		public static string score 		{ get{ return LocalizationManager.GetTranslation ("score"); } }
		public static string search 		{ get{ return LocalizationManager.GetTranslation ("search"); } }
		public static string search_for 		{ get{ return LocalizationManager.GetTranslation ("search_for"); } }
		public static string season_score 		{ get{ return LocalizationManager.GetTranslation ("season_score"); } }
		public static string select 		{ get{ return LocalizationManager.GetTranslation ("select"); } }
		public static string selected 		{ get{ return LocalizationManager.GetTranslation ("selected"); } }
		public static string send 		{ get{ return LocalizationManager.GetTranslation ("send"); } }
		public static string sensitivity 		{ get{ return LocalizationManager.GetTranslation ("sensitivity"); } }
		public static string settings 		{ get{ return LocalizationManager.GetTranslation ("settings"); } }
		public static string share 		{ get{ return LocalizationManager.GetTranslation ("share"); } }
		public static string shop 		{ get{ return LocalizationManager.GetTranslation ("shop"); } }
		public static string skill10_name 		{ get{ return LocalizationManager.GetTranslation ("skill10_name"); } }
		public static string skill11_name 		{ get{ return LocalizationManager.GetTranslation ("skill11_name"); } }
		public static string skill12_name 		{ get{ return LocalizationManager.GetTranslation ("skill12_name"); } }
		public static string skill13_name 		{ get{ return LocalizationManager.GetTranslation ("skill13_name"); } }
		public static string skill14_name 		{ get{ return LocalizationManager.GetTranslation ("skill14_name"); } }
		public static string skill15_name 		{ get{ return LocalizationManager.GetTranslation ("skill15_name"); } }
		public static string skill16_name 		{ get{ return LocalizationManager.GetTranslation ("skill16_name"); } }
		public static string skill17_name 		{ get{ return LocalizationManager.GetTranslation ("skill17_name"); } }
		public static string skill18_name 		{ get{ return LocalizationManager.GetTranslation ("skill18_name"); } }
		public static string skill19_name 		{ get{ return LocalizationManager.GetTranslation ("skill19_name"); } }
		public static string skill1_name 		{ get{ return LocalizationManager.GetTranslation ("skill1_name"); } }
		public static string skill20_name 		{ get{ return LocalizationManager.GetTranslation ("skill20_name"); } }
		public static string skill21_name 		{ get{ return LocalizationManager.GetTranslation ("skill21_name"); } }
		public static string skill22_name 		{ get{ return LocalizationManager.GetTranslation ("skill22_name"); } }
		public static string skill23_name 		{ get{ return LocalizationManager.GetTranslation ("skill23_name"); } }
		public static string skill24_name 		{ get{ return LocalizationManager.GetTranslation ("skill24_name"); } }
		public static string skill25_name 		{ get{ return LocalizationManager.GetTranslation ("skill25_name"); } }
		public static string skill26_name 		{ get{ return LocalizationManager.GetTranslation ("skill26_name"); } }
		public static string skill27_name 		{ get{ return LocalizationManager.GetTranslation ("skill27_name"); } }
		public static string skill28_name 		{ get{ return LocalizationManager.GetTranslation ("skill28_name"); } }
		public static string skill29_name 		{ get{ return LocalizationManager.GetTranslation ("skill29_name"); } }
		public static string skill2_name 		{ get{ return LocalizationManager.GetTranslation ("skill2_name"); } }
		public static string skill30_name 		{ get{ return LocalizationManager.GetTranslation ("skill30_name"); } }
		public static string skill31_name 		{ get{ return LocalizationManager.GetTranslation ("skill31_name"); } }
		public static string skill32_name 		{ get{ return LocalizationManager.GetTranslation ("skill32_name"); } }
		public static string skill3_name 		{ get{ return LocalizationManager.GetTranslation ("skill3_name"); } }
		public static string skill4_name 		{ get{ return LocalizationManager.GetTranslation ("skill4_name"); } }
		public static string skill5_name 		{ get{ return LocalizationManager.GetTranslation ("skill5_name"); } }
		public static string skill6_name 		{ get{ return LocalizationManager.GetTranslation ("skill6_name"); } }
		public static string skill7_name 		{ get{ return LocalizationManager.GetTranslation ("skill7_name"); } }
		public static string skill8_name 		{ get{ return LocalizationManager.GetTranslation ("skill8_name"); } }
		public static string skill9_name 		{ get{ return LocalizationManager.GetTranslation ("skill9_name"); } }
		public static string sold_out 		{ get{ return LocalizationManager.GetTranslation ("sold_out"); } }
		public static string sound 		{ get{ return LocalizationManager.GetTranslation ("sound"); } }
		public static string speed 		{ get{ return LocalizationManager.GetTranslation ("speed"); } }
		public static string star 		{ get{ return LocalizationManager.GetTranslation ("star"); } }
		public static string start 		{ get{ return LocalizationManager.GetTranslation ("start"); } }
		public static string submit 		{ get{ return LocalizationManager.GetTranslation ("submit"); } }
		public static string success 		{ get{ return LocalizationManager.GetTranslation ("success"); } }
		public static string suggest_team 		{ get{ return LocalizationManager.GetTranslation ("suggest_team"); } }
		public static string summary 		{ get{ return LocalizationManager.GetTranslation ("summary"); } }
		public static string summer_holiday_exchange_title_1 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_exchange_title_1"); } }
		public static string summer_holiday_exchange_title_2 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_exchange_title_2"); } }
		public static string summer_holiday_exchange_title_3 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_exchange_title_3"); } }
		public static string summer_holiday_mission_progress_1 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_mission_progress_1"); } }
		public static string summer_holiday_mission_progress_2 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_mission_progress_2"); } }
		public static string summer_holiday_mission_progress_3 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_mission_progress_3"); } }
		public static string summer_holiday_mission_progress_4 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_mission_progress_4"); } }
		public static string summer_holiday_mission_rule_1 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_mission_rule_1"); } }
		public static string summer_holiday_mission_rule_2 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_mission_rule_2"); } }
		public static string summer_holiday_mission_rule_3 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_mission_rule_3"); } }
		public static string summer_holiday_msg_exchange_1 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_msg_exchange_1"); } }
		public static string summer_holiday_msg_exchange_2 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_msg_exchange_2"); } }
		public static string summer_holiday_msg_mission_1 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_msg_mission_1"); } }
		public static string summer_holiday_msg_mission_2 		{ get{ return LocalizationManager.GetTranslation ("summer_holiday_msg_mission_2"); } }
		public static string system 		{ get{ return LocalizationManager.GetTranslation ("system"); } }
		public static string tap_to_close 		{ get{ return LocalizationManager.GetTranslation ("tap_to_close"); } }
		public static string team_score 		{ get{ return LocalizationManager.GetTranslation ("team_score"); } }
		public static string team_type 		{ get{ return LocalizationManager.GetTranslation ("team_type"); } }
		public static string text_double_ticket 		{ get{ return LocalizationManager.GetTranslation ("text_double_ticket"); } }
		public static string text_only_purchase_one 		{ get{ return LocalizationManager.GetTranslation ("text_only_purchase_one"); } }
		public static string ticket 		{ get{ return LocalizationManager.GetTranslation ("ticket"); } }
		public static string time 		{ get{ return LocalizationManager.GetTranslation ("time"); } }
		public static string title_2vs2_invitation 		{ get{ return LocalizationManager.GetTranslation ("title_2vs2_invitation"); } }
		public static string title_betting 		{ get{ return LocalizationManager.GetTranslation ("title_betting"); } }
		public static string title_bonus_reward 		{ get{ return LocalizationManager.GetTranslation ("title_bonus_reward"); } }
		public static string title_buy_ticket 		{ get{ return LocalizationManager.GetTranslation ("title_buy_ticket"); } }
		public static string title_clan_have_no_member 		{ get{ return LocalizationManager.GetTranslation ("title_clan_have_no_member"); } }
		public static string title_daily_login 		{ get{ return LocalizationManager.GetTranslation ("title_daily_login"); } }
		public static string title_daily_quest 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest"); } }
		public static string title_daily_quest_collect_gold 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_collect_gold"); } }
		public static string title_daily_quest_collect_items 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_collect_items"); } }
		public static string title_daily_quest_complete_any_map 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_complete_any_map"); } }
		public static string title_daily_quest_defeat_bosses 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_defeat_bosses"); } }
		public static string title_daily_quest_kill_enemies 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_kill_enemies"); } }
		public static string title_daily_quest_lucky_wheel 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_lucky_wheel"); } }
		public static string title_daily_quest_play_endless 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_play_endless"); } }
		public static string title_daily_quest_play_pvp 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_play_pvp"); } }
		public static string title_daily_quest_rate_game 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_rate_game"); } }
		public static string title_daily_quest_share_fb 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_share_fb"); } }
		public static string title_daily_quest_use_activeskill 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_use_activeskill"); } }
		public static string title_daily_quest_use_emp 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_use_emp"); } }
		public static string title_daily_quest_use_power_up 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_use_power_up"); } }
		public static string title_daily_quest_win_pvp 		{ get{ return LocalizationManager.GetTranslation ("title_daily_quest_win_pvp"); } }
		public static string title_donate_card_10_times 		{ get{ return LocalizationManager.GetTranslation ("title_donate_card_10_times"); } }
		public static string title_encourage 		{ get{ return LocalizationManager.GetTranslation ("title_encourage"); } }
		public static string title_enter_id 		{ get{ return LocalizationManager.GetTranslation ("title_enter_id"); } }
		public static string title_evolve_max_upgrade 		{ get{ return LocalizationManager.GetTranslation ("title_evolve_max_upgrade"); } }
		public static string title_evolve_new_changes 		{ get{ return LocalizationManager.GetTranslation ("title_evolve_new_changes"); } }
		public static string title_evolve_new_passive 		{ get{ return LocalizationManager.GetTranslation ("title_evolve_new_passive"); } }
		public static string title_free_reward 		{ get{ return LocalizationManager.GetTranslation ("title_free_reward"); } }
		public static string title_friends_info 		{ get{ return LocalizationManager.GetTranslation ("title_friends_info"); } }
		public static string title_great_offer 		{ get{ return LocalizationManager.GetTranslation ("title_great_offer"); } }
		public static string title_iap_captain 		{ get{ return LocalizationManager.GetTranslation ("title_iap_captain"); } }
		public static string title_iap_challenger 		{ get{ return LocalizationManager.GetTranslation ("title_iap_challenger"); } }
		public static string title_iap_commander 		{ get{ return LocalizationManager.GetTranslation ("title_iap_commander"); } }
		public static string title_iap_conqueror 		{ get{ return LocalizationManager.GetTranslation ("title_iap_conqueror"); } }
		public static string title_iap_gem_pack 		{ get{ return LocalizationManager.GetTranslation ("title_iap_gem_pack"); } }
		public static string title_iap_infinity_gold 		{ get{ return LocalizationManager.GetTranslation ("title_iap_infinity_gold"); } }
		public static string title_iap_remove_ads 		{ get{ return LocalizationManager.GetTranslation ("title_iap_remove_ads"); } }
		public static string title_iap_starter 		{ get{ return LocalizationManager.GetTranslation ("title_iap_starter"); } }
		public static string title_iap_super_offer 		{ get{ return LocalizationManager.GetTranslation ("title_iap_super_offer"); } }
		public static string title_iap_young_pilot 		{ get{ return LocalizationManager.GetTranslation ("title_iap_young_pilot"); } }
		public static string title_in_progress 		{ get{ return LocalizationManager.GetTranslation ("title_in_progress"); } }
		public static string title_invite_to_team 		{ get{ return LocalizationManager.GetTranslation ("title_invite_to_team"); } }
		public static string title_is_friend 		{ get{ return LocalizationManager.GetTranslation ("title_is_friend"); } }
		public static string title_lucky_box 		{ get{ return LocalizationManager.GetTranslation ("title_lucky_box"); } }
		public static string title_lucky_box_best_rewards 		{ get{ return LocalizationManager.GetTranslation ("title_lucky_box_best_rewards"); } }
		public static string title_lucky_box_diamond_chest 		{ get{ return LocalizationManager.GetTranslation ("title_lucky_box_diamond_chest"); } }
		public static string title_lucky_box_golden_chest 		{ get{ return LocalizationManager.GetTranslation ("title_lucky_box_golden_chest"); } }
		public static string title_lucky_box_your_reward 		{ get{ return LocalizationManager.GetTranslation ("title_lucky_box_your_reward"); } }
		public static string title_lucky_wheel 		{ get{ return LocalizationManager.GetTranslation ("title_lucky_wheel"); } }
		public static string title_mail_sent 		{ get{ return LocalizationManager.GetTranslation ("title_mail_sent"); } }
		public static string title_open_golden_chest 		{ get{ return LocalizationManager.GetTranslation ("title_open_golden_chest"); } }
		public static string title_play_with_friend 		{ get{ return LocalizationManager.GetTranslation ("title_play_with_friend"); } }
		public static string title_player_already_in_a_team 		{ get{ return LocalizationManager.GetTranslation ("title_player_already_in_a_team"); } }
		public static string title_player_found 		{ get{ return LocalizationManager.GetTranslation ("title_player_found"); } }
		public static string title_post_war_boost 		{ get{ return LocalizationManager.GetTranslation ("title_post_war_boost"); } }
		public static string title_premium_pack 		{ get{ return LocalizationManager.GetTranslation ("title_premium_pack"); } }
		public static string title_private 		{ get{ return LocalizationManager.GetTranslation ("title_private"); } }
		public static string title_pvp_current_account 		{ get{ return LocalizationManager.GetTranslation ("title_pvp_current_account"); } }
		public static string title_pvp_invitation 		{ get{ return LocalizationManager.GetTranslation ("title_pvp_invitation"); } }
		public static string title_pvp_mail_box 		{ get{ return LocalizationManager.GetTranslation ("title_pvp_mail_box"); } }
		public static string title_pvp_old_account 		{ get{ return LocalizationManager.GetTranslation ("title_pvp_old_account"); } }
		public static string title_pvp_player_info 		{ get{ return LocalizationManager.GetTranslation ("title_pvp_player_info"); } }
		public static string title_pvp_rank_down 		{ get{ return LocalizationManager.GetTranslation ("title_pvp_rank_down"); } }
		public static string title_pvp_rank_up 		{ get{ return LocalizationManager.GetTranslation ("title_pvp_rank_up"); } }
		public static string title_quick_match 		{ get{ return LocalizationManager.GetTranslation ("title_quick_match"); } }
		public static string title_rank_rewards 		{ get{ return LocalizationManager.GetTranslation ("title_rank_rewards"); } }
		public static string title_request_card_2_times 		{ get{ return LocalizationManager.GetTranslation ("title_request_card_2_times"); } }
		public static string title_search_friend 		{ get{ return LocalizationManager.GetTranslation ("title_search_friend"); } }
		public static string title_select_all 		{ get{ return LocalizationManager.GetTranslation ("title_select_all"); } }
		public static string title_select_content 		{ get{ return LocalizationManager.GetTranslation ("title_select_content"); } }
		public static string title_select_team_member 		{ get{ return LocalizationManager.GetTranslation ("title_select_team_member"); } }
		public static string title_star_chest 		{ get{ return LocalizationManager.GetTranslation ("title_star_chest"); } }
		public static string title_star_chest_extra 		{ get{ return LocalizationManager.GetTranslation ("title_star_chest_extra"); } }
		public static string title_starter_pack 		{ get{ return LocalizationManager.GetTranslation ("title_starter_pack"); } }
		public static string title_summer_holiday 		{ get{ return LocalizationManager.GetTranslation ("title_summer_holiday"); } }
		public static string title_team_mail 		{ get{ return LocalizationManager.GetTranslation ("title_team_mail"); } }
		public static string title_team_mission 		{ get{ return LocalizationManager.GetTranslation ("title_team_mission"); } }
		public static string title_threat 		{ get{ return LocalizationManager.GetTranslation ("title_threat"); } }
		public static string title_unfriend 		{ get{ return LocalizationManager.GetTranslation ("title_unfriend"); } }
		public static string title_unlock_reward 		{ get{ return LocalizationManager.GetTranslation ("title_unlock_reward"); } }
		public static string title_upgrade_Wingman 		{ get{ return LocalizationManager.GetTranslation ("title_upgrade_Wingman"); } }
		public static string title_upgrade_drop_chance 		{ get{ return LocalizationManager.GetTranslation ("title_upgrade_drop_chance"); } }
		public static string title_upgrade_laser_power 		{ get{ return LocalizationManager.GetTranslation ("title_upgrade_laser_power"); } }
		public static string title_upgrade_main_weapon 		{ get{ return LocalizationManager.GetTranslation ("title_upgrade_main_weapon"); } }
		public static string title_upgrade_missile_power 		{ get{ return LocalizationManager.GetTranslation ("title_upgrade_missile_power"); } }
		public static string title_upgrade_secondary_weapon 		{ get{ return LocalizationManager.GetTranslation ("title_upgrade_secondary_weapon"); } }
		public static string title_upgrade_super_weapon 		{ get{ return LocalizationManager.GetTranslation ("title_upgrade_super_weapon"); } }
		public static string title_upgrade_wind_slash_power 		{ get{ return LocalizationManager.GetTranslation ("title_upgrade_wind_slash_power"); } }
		public static string title_vip_pack 		{ get{ return LocalizationManager.GetTranslation ("title_vip_pack"); } }
		public static string title_wait_3_seconds 		{ get{ return LocalizationManager.GetTranslation ("title_wait_3_seconds"); } }
		public static string title_war_notify 		{ get{ return LocalizationManager.GetTranslation ("title_war_notify"); } }
		public static string title_warning 		{ get{ return LocalizationManager.GetTranslation ("title_warning"); } }
		public static string title_win_pvp_5_times 		{ get{ return LocalizationManager.GetTranslation ("title_win_pvp_5_times"); } }
		public static string title_wing 		{ get{ return LocalizationManager.GetTranslation ("title_wing"); } }
		public static string tooltip_loading_scene_1 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_1"); } }
		public static string tooltip_loading_scene_10 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_10"); } }
		public static string tooltip_loading_scene_11 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_11"); } }
		public static string tooltip_loading_scene_2 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_2"); } }
		public static string tooltip_loading_scene_3 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_3"); } }
		public static string tooltip_loading_scene_4 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_4"); } }
		public static string tooltip_loading_scene_5 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_5"); } }
		public static string tooltip_loading_scene_6 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_6"); } }
		public static string tooltip_loading_scene_7 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_7"); } }
		public static string tooltip_loading_scene_8 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_8"); } }
		public static string tooltip_loading_scene_9 		{ get{ return LocalizationManager.GetTranslation ("tooltip_loading_scene_9"); } }
		public static string tooltip_touch_to_change 		{ get{ return LocalizationManager.GetTranslation ("tooltip_touch_to_change"); } }
		public static string top_10_ranking_reward 		{ get{ return LocalizationManager.GetTranslation ("top_10_ranking_reward"); } }
		public static string tournament 		{ get{ return LocalizationManager.GetTranslation ("tournament"); } }
		public static string unequip 		{ get{ return LocalizationManager.GetTranslation ("unequip"); } }
		public static string unfollow 		{ get{ return LocalizationManager.GetTranslation ("unfollow"); } }
		public static string unlock 		{ get{ return LocalizationManager.GetTranslation ("unlock"); } }
		public static string up_to_master 		{ get{ return LocalizationManager.GetTranslation ("up_to_master"); } }
		public static string up_to_number 		{ get{ return LocalizationManager.GetTranslation ("up_to_number"); } }
		public static string up_to_vice 		{ get{ return LocalizationManager.GetTranslation ("up_to_vice"); } }
		public static string upgrade 		{ get{ return LocalizationManager.GetTranslation ("upgrade"); } }
		public static string user_id 		{ get{ return LocalizationManager.GetTranslation ("user_id"); } }
		public static string victory 		{ get{ return LocalizationManager.GetTranslation ("victory"); } }
		public static string video 		{ get{ return LocalizationManager.GetTranslation ("video"); } }
		public static string video_reward 		{ get{ return LocalizationManager.GetTranslation ("video_reward"); } }
		public static string view 		{ get{ return LocalizationManager.GetTranslation ("view"); } }
		public static string vip_daily_rewards 		{ get{ return LocalizationManager.GetTranslation ("vip_daily_rewards"); } }
		public static string vip_key 		{ get{ return LocalizationManager.GetTranslation ("vip_key"); } }
		public static string wait 		{ get{ return LocalizationManager.GetTranslation ("wait"); } }
		public static string warning 		{ get{ return LocalizationManager.GetTranslation ("warning"); } }
		public static string watch 		{ get{ return LocalizationManager.GetTranslation ("watch"); } }
		public static string win 		{ get{ return LocalizationManager.GetTranslation ("win"); } }
		public static string win_rate 		{ get{ return LocalizationManager.GetTranslation ("win_rate"); } }
		public static string wing1_name 		{ get{ return LocalizationManager.GetTranslation ("wing1_name"); } }
		public static string wing2_name 		{ get{ return LocalizationManager.GetTranslation ("wing2_name"); } }
		public static string wing3_name 		{ get{ return LocalizationManager.GetTranslation ("wing3_name"); } }
		public static string wingman1_name 		{ get{ return LocalizationManager.GetTranslation ("wingman1_name"); } }
		public static string wingman2_name 		{ get{ return LocalizationManager.GetTranslation ("wingman2_name"); } }
		public static string wingman3_name 		{ get{ return LocalizationManager.GetTranslation ("wingman3_name"); } }
		public static string wingman4_name 		{ get{ return LocalizationManager.GetTranslation ("wingman4_name"); } }
		public static string wingman5_name 		{ get{ return LocalizationManager.GetTranslation ("wingman5_name"); } }
		public static string wingman6_name 		{ get{ return LocalizationManager.GetTranslation ("wingman6_name"); } }
		public static string world 		{ get{ return LocalizationManager.GetTranslation ("world"); } }
		public static string x2Gold 		{ get{ return LocalizationManager.GetTranslation ("x2Gold"); } }
		public static string x2_gem_today 		{ get{ return LocalizationManager.GetTranslation ("x2_gem_today"); } }
		public static string x2_gold_today 		{ get{ return LocalizationManager.GetTranslation ("x2_gold_today"); } }
		public static string yes 		{ get{ return LocalizationManager.GetTranslation ("yes"); } }
		public static string you_request 		{ get{ return LocalizationManager.GetTranslation ("you_request"); } }
	}

    public static class ScriptTerms
	{

		public const string ActiveSkill = "ActiveSkill";
		public const string App_Name = "App_Name";
		public const string Font_Localization = "Font Localization";
		public const string MESS_1DAY_1 = "MESS_1DAY_1";
		public const string MESS_1DAY_2 = "MESS_1DAY_2";
		public const string MESS_1DAY_3 = "MESS_1DAY_3";
		public const string MESS_3DAY_1 = "MESS_3DAY_1";
		public const string MESS_3DAY_2 = "MESS_3DAY_2";
		public const string MESS_3DAY_3 = "MESS_3DAY_3";
		public const string MESS_7DAY_1 = "MESS_7DAY_1";
		public const string MESS_7DAY_2 = "MESS_7DAY_2";
		public const string MESS_7DAY_3 = "MESS_7DAY_3";
		public const string Notifi_Achi_PleaseComplete = "Notifi_Achi_PleaseComplete";
		public const string Notifi_DailyQuest_PleaseComplete = "Notifi_DailyQuest_PleaseComplete";
		public const string Notifi_HaveReceived = "Notifi_HaveReceived";
		public const string Notifi_HaveReceived_ChangeKey = "Notifi_HaveReceived_ChangeKey";
		public const string SkillActivated = "SkillActivated";
		public const string accept = "accept";
		public const string accepted = "accepted";
		public const string accumulate = "accumulate";
		public const string accumulated_bell = "accumulated_bell";
		public const string accumulated_candy = "accumulated_candy";
		public const string achi_collect_gold = "achi_collect_gold";
		public const string achi_collect_items = "achi_collect_items";
		public const string achi_defeat_bosses = "achi_defeat_bosses";
		public const string achi_have_upgrades = "achi_have_upgrades";
		public const string achi_kill_enemies = "achi_kill_enemies";
		public const string achi_use_activeskill = "achi_use_activeskill";
		public const string achi_use_emp = "achi_use_emp";
		public const string achievements = "achievements";
		public const string add_friend = "add_friend";
		public const string air_craft = "air_craft";
		public const string aircraft10_name = "aircraft10_name";
		public const string aircraft1_name = "aircraft1_name";
		public const string aircraft2_name = "aircraft2_name";
		public const string aircraft3_name = "aircraft3_name";
		public const string aircraft6_name = "aircraft6_name";
		public const string aircraft7_name = "aircraft7_name";
		public const string aircraft8_name = "aircraft8_name";
		public const string aircraft9_name = "aircraft9_name";
		public const string announcement = "announcement";
		public const string available_exchange = "available_exchange";
		public const string becom_rich = "becom_rich";
		public const string bell_exchange = "bell_exchange";
		public const string betting_amount = "betting_amount";
		public const string bonus = "bonus";
		public const string bonus_medal = "bonus_medal";
		public const string boss = "boss";
		public const string boss_time = "boss_time";
		public const string buy = "buy";
		public const string calling_backup_reduce_life = "calling_backup_reduce_life";
		public const string calling_backup_title = "calling_backup_title";
		public const string campain = "campain";
		public const string cancel = "cancel";
		public const string candy_exchange = "candy_exchange";
		public const string card_from_friend = "card_from_friend";
		public const string charging = "charging";
		public const string chat = "chat";
		public const string claim = "claim";
		public const string claimed = "claimed";
		public const string claimed_reward = "claimed_reward";
		public const string clan = "clan";
		public const string clan_info = "clan_info";
		public const string clan_invitation = "clan_invitation";
		public const string clan_master = "clan_master";
		public const string clan_member = "clan_member";
		public const string clan_name = "clan_name";
		public const string clan_vice = "clan_vice";
		public const string close = "close";
		public const string community = "community";
		public const string community_follow = "community_follow";
		public const string community_friends = "community_friends";
		public const string community_join = "community_join";
		public const string community_like = "community_like";
		public const string community_share = "community_share";
		public const string complete = "complete";
		public const string completed = "completed";
		public const string confirm = "confirm";
		public const string continue_key = "continue_key";
		public const string control_scheme = "control_scheme";
		public const string create = "create";
		public const string current = "current";
		public const string current_candy = "current_candy";
		public const string current_win = "current_win";
		public const string day = "day";
		public const string day_dailyLogin = "day_dailyLogin";
		public const string days = "days";
		public const string deal_end_in = "deal_end_in";
		public const string defeat = "defeat";
		public const string delete = "delete";
		public const string denied = "denied";
		public const string des_card_pack = "des_card_pack";
		public const string des_level_aircraft_sale = "des_level_aircraft_sale";
		public const string des_name_aircraft_sale = "des_name_aircraft_sale";
		public const string des_wing_of_justice = "des_wing_of_justice";
		public const string des_wing_of_redemption = "des_wing_of_redemption";
		public const string des_wing_of_resolution = "des_wing_of_resolution";
		public const string discount_percent = "discount_percent";
		public const string donate = "donate";
		public const string donate_info = "donate_info";
		public const string done = "done";
		public const string dont_save = "dont_save";
		public const string down_to_member = "down_to_member";
		public const string draw = "draw";
		public const string duration = "duration";
		public const string elo_key = "elo_key";
		public const string end_in = "end_in";
		public const string endless = "endless";
		public const string energy = "energy";
		public const string enter_your_giftcode = "enter_your_giftcode";
		public const string enter_your_name = "enter_your_name";
		public const string equip = "equip";
		public const string error = "error";
		public const string event_item_ice_cream = "event_item_ice_cream";
		public const string evolve = "evolve";
		public const string exit = "exit";
		public const string extra = "extra";
		public const string fight = "fight";
		public const string find = "find";
		public const string follow = "follow";
		public const string follow_us = "follow_us";
		public const string following = "following";
		public const string free = "free";
		public const string friend = "friend";
		public const string friend_request = "friend_request";
		public const string friends = "friends";
		public const string game_match = "game_match";
		public const string gem = "gem";
		public const string get_amount_item = "get_amount_item";
		public const string giftcode = "giftcode";
		public const string global = "global";
		public const string glory = "glory";
		public const string glory_chest = "glory_chest";
		public const string glory_point = "glory_point";
		public const string go = "go";
		public const string gold = "gold";
		public const string hangar = "hangar";
		public const string hard = "hard";
		public const string have_been_outrun = "have_been_outrun";
		public const string hell = "hell";
		public const string highest_wave = "highest_wave";
		public const string iap_infinity__forever = "iap_infinity_ forever";
		public const string ice_cream_purchase = "ice_cream_purchase";
		public const string icon = "icon";
		public const string id_key = "id_key";
		public const string info_item_active_skill = "info_item_active_skill";
		public const string info_item_aircraft_general_card = "info_item_aircraft_general_card";
		public const string info_item_aircraft_specific_card = "info_item_aircraft_specific_card";
		public const string info_item_card_box = "info_item_card_box";
		public const string info_item_drone_general_card = "info_item_drone_general_card";
		public const string info_item_drone_specific_card = "info_item_drone_specific_card";
		public const string info_item_energy = "info_item_energy";
		public const string info_item_gem = "info_item_gem";
		public const string info_item_gold = "info_item_gold";
		public const string info_item_life = "info_item_life";
		public const string info_item_power_up = "info_item_power_up";
		public const string info_item_wing_general_card = "info_item_wing_general_card";
		public const string info_item_wing_specific_card = "info_item_wing_specific_card";
		public const string info_season_end = "info_season_end";
		public const string info_season_start = "info_season_start";
		public const string info_skill_10_fire_ball = "info_skill_10_fire_ball";
		public const string info_skill_11_fury_blade = "info_skill_11_fury_blade";
		public const string info_skill_12_phoenix_rise = "info_skill_12_phoenix_rise";
		public const string info_skill_13_x_laser = "info_skill_13_x_laser";
		public const string info_skill_14_double_riffle = "info_skill_14_double_riffle";
		public const string info_skill_15_heavy_missle = "info_skill_15_heavy_missle";
		public const string info_skill_16_laser_strike = "info_skill_16_laser_strike";
		public const string info_skill_17_sonic_wave = "info_skill_17_sonic_wave";
		public const string info_skill_18_x_galting = "info_skill_18_x_galting";
		public const string info_skill_19_rain_of_bomb = "info_skill_19_rain_of_bomb";
		public const string info_skill_1_upgrade_module = "info_skill_1_upgrade_module";
		public const string info_skill_20_splash_shot = "info_skill_20_splash_shot";
		public const string info_skill_21_ice_storm = "info_skill_21_ice_storm";
		public const string info_skill_22_ice_missile = "info_skill_22_ice_missile";
		public const string info_skill_23_judgement = "info_skill_23_judgement";
		public const string info_skill_24_atonement = "info_skill_24_atonement";
		public const string info_skill_25_magic_module = "info_skill_25_magic_module";
		public const string info_skill_26_overgrowth_force = "info_skill_26_overgrowth_force";
		public const string info_skill_27_power_unlock = "info_skill_27_power_unlock";
		public const string info_skill_28_huge_cargo = "info_skill_28_huge_cargo";
		public const string info_skill_29_skywrath_blade = "info_skill_29_skywrath_blade";
		public const string info_skill_2_heavy_shell = "info_skill_2_heavy_shell";
		public const string info_skill_30_ultra_shell = "info_skill_30_ultra_shell";
		public const string info_skill_31_lightning_storm = "info_skill_31_lightning_storm";
		public const string info_skill_32_lightning_strike = "info_skill_32_lightning_strike";
		public const string info_skill_3_extra_power = "info_skill_3_extra_power";
		public const string info_skill_4_fast_bullet = "info_skill_4_fast_bullet";
		public const string info_skill_5_speed_fire = "info_skill_5_speed_fire";
		public const string info_skill_6_mystic_power = "info_skill_6_mystic_power";
		public const string info_skill_7_life_surge = "info_skill_7_life_surge";
		public const string info_skill_8_big_cargo = "info_skill_8_big_cargo";
		public const string info_skill_9_open_fire = "info_skill_9_open_fire";
		public const string invite = "invite";
		public const string invite_friends = "invite_friends";
		public const string invited_to_2vs2 = "invited_to_2vs2";
		public const string item = "item";
		public const string item_name_card_box = "item_name_card_box";
		public const string item_name_card_replace = "item_name_card_replace";
		public const string item_name_uni_aircraft_card = "item_name_uni_aircraft_card";
		public const string item_name_uni_drone_card = "item_name_uni_drone_card";
		public const string item_name_uni_wing_card = "item_name_uni_wing_card";
		public const string join = "join";
		public const string join_group = "join_group";
		public const string join_request = "join_request";
		public const string key_event = "key_event";
		public const string key_pvp = "key_pvp";
		public const string kick = "kick";
		public const string lTry = "lTry";
		public const string label_max = "label_max";
		public const string label_notice = "label_notice";
		public const string label_require = "label_require";
		public const string label_total = "label_total";
		public const string label_you_have = "label_you_have";
		public const string leaderboard = "leaderboard";
		public const string level = "level";
		public const string level_extra_name = "level_extra_name";
		public const string level_upgrade = "level_upgrade";
		public const string life = "life";
		public const string like_us = "like_us";
		public const string live_score = "live_score";
		public const string loading = "loading";
		public const string loading_progress = "loading_progress";
		public const string local = "local";
		public const string login = "login";
		public const string login_facebook = "login_facebook";
		public const string login_gamecenter = "login_gamecenter";
		public const string lose = "lose";
		public const string losses = "losses";
		public const string mail_to_system = "mail_to_system";
		public const string max_reward = "max_reward";
		public const string medal = "medal";
		public const string member_invite = "member_invite";
		public const string member_setting = "member_setting";
		public const string mission_complete_the_map = "mission_complete_the_map";
		public const string mission_defeat_all_enemies = "mission_defeat_all_enemies";
		public const string mission_endless_bonusgold = "mission_endless_bonusgold";
		public const string mission_endless_playtime = "mission_endless_playtime";
		public const string mission_endless_wavereached = "mission_endless_wavereached";
		public const string mission_stay_untouched = "mission_stay_untouched";
		public const string ms_notifi_finish_event = "ms_notifi_finish_event";
		public const string msg_2vs2_invitation = "msg_2vs2_invitation";
		public const string msg_achievements = "msg_achievements";
		public const string msg_aircraft_BataFD01 = "msg_aircraft_BataFD01";
		public const string msg_aircraft_FuryOfAres = "msg_aircraft_FuryOfAres";
		public const string msg_aircraft_SkyWraith = "msg_aircraft_SkyWraith";
		public const string msg_aircraft_ice_shard = "msg_aircraft_ice_shard";
		public const string msg_aircraft_mac_bird = "msg_aircraft_mac_bird";
		public const string msg_aircraft_starbomb = "msg_aircraft_starbomb";
		public const string msg_aircraft_thunder_bolt = "msg_aircraft_thunder_bolt";
		public const string msg_aircraft_twilight_x = "msg_aircraft_twilight_x";
		public const string msg_aircraft_used_time_per_day = "msg_aircraft_used_time_per_day";
		public const string msg_buy_ticket = "msg_buy_ticket";
		public const string msg_buy_ticket_success = "msg_buy_ticket_success";
		public const string msg_can_claim_after = "msg_can_claim_after";
		public const string msg_cant_claim = "msg_cant_claim";
		public const string msg_cant_donate_yourself = "msg_cant_donate_yourself";
		public const string msg_card_double_in_chest = "msg_card_double_in_chest";
		public const string msg_card_purchase_in_shop = "msg_card_purchase_in_shop";
		public const string msg_challenge_friend = "msg_challenge_friend";
		public const string msg_challenge_tournament = "msg_challenge_tournament";
		public const string msg_change_clan = "msg_change_clan";
		public const string msg_choose_any_aircraft = "msg_choose_any_aircraft";
		public const string msg_clan_cant_have_more_vice = "msg_clan_cant_have_more_vice";
		public const string msg_clan_have_full_member = "msg_clan_have_full_member";
		public const string msg_clan_not_exist = "msg_clan_not_exist";
		public const string msg_code_invalid = "msg_code_invalid";
		public const string msg_community_like_and_follow = "msg_community_like_and_follow";
		public const string msg_community_more_friends = "msg_community_more_friends";
		public const string msg_community_total_friends = "msg_community_total_friends";
		public const string msg_confirm_command = "msg_confirm_command";
		public const string msg_congratulation_create_clan = "msg_congratulation_create_clan";
		public const string msg_congratulation_vip = "msg_congratulation_vip";
		public const string msg_content_cant_empty = "msg_content_cant_empty";
		public const string msg_create_and_lead = "msg_create_and_lead";
		public const string msg_daily_login = "msg_daily_login";
		public const string msg_daily_quest = "msg_daily_quest";
		public const string msg_description_glory_chest = "msg_description_glory_chest";
		public const string msg_description_point_glory = "msg_description_point_glory";
		public const string msg_dont_have_clan = "msg_dont_have_clan";
		public const string msg_downgrading_member = "msg_downgrading_member";
		public const string msg_end_season_rank_reward = "msg_end_season_rank_reward";
		public const string msg_endless_objective = "msg_endless_objective";
		public const string msg_enter_id = "msg_enter_id";
		public const string msg_enter_user_id = "msg_enter_user_id";
		public const string msg_event_will_end_in = "msg_event_will_end_in";
		public const string msg_exit_popoup = "msg_exit_popoup";
		public const string msg_extra_level_have_to_use = "msg_extra_level_have_to_use";
		public const string msg_force_update_version = "msg_force_update_version";
		public const string msg_friends_passed_level = "msg_friends_passed_level";
		public const string msg_gain_a_ticket = "msg_gain_a_ticket";
		public const string msg_have_used_reset = "msg_have_used_reset";
		public const string msg_invitation_not_exist = "msg_invitation_not_exist";
		public const string msg_invite_duplicate_invite = "msg_invite_duplicate_invite";
		public const string msg_invite_server_maintain = "msg_invite_server_maintain";
		public const string msg_invite_timeout = "msg_invite_timeout";
		public const string msg_invite_user_busy = "msg_invite_user_busy";
		public const string msg_invite_user_offline = "msg_invite_user_offline";
		public const string msg_item_not_in_shop = "msg_item_not_in_shop";
		public const string msg_item_sold_out = "msg_item_sold_out";
		public const string msg_join_team_to_open = "msg_join_team_to_open";
		public const string msg_kick_member = "msg_kick_member";
		public const string msg_level_halloween_please = "msg_level_halloween_please";
		public const string msg_level_halloween_rules_1 = "msg_level_halloween_rules_1";
		public const string msg_level_halloween_rules_2 = "msg_level_halloween_rules_2";
		public const string msg_level_halloween_rules_3 = "msg_level_halloween_rules_3";
		public const string msg_level_halloween_rules_4 = "msg_level_halloween_rules_4";
		public const string msg_level_xmas_rule_1 = "msg_level_xmas_rule_1";
		public const string msg_level_xmas_rule_2 = "msg_level_xmas_rule_2";
		public const string msg_level_xmas_rule_3 = "msg_level_xmas_rule_3";
		public const string msg_load_chat_history_error = "msg_load_chat_history_error";
		public const string msg_log_in_game_online_to_receive = "msg_log_in_game_online_to_receive";
		public const string msg_login_facebook = "msg_login_facebook";
		public const string msg_lose_3_battle_in_a_row = "msg_lose_3_battle_in_a_row";
		public const string msg_more_gold = "msg_more_gold";
		public const string msg_more_point_to_vip = "msg_more_point_to_vip";
		public const string msg_name_exists_clan = "msg_name_exists_clan";
		public const string msg_name_invalid = "msg_name_invalid";
		public const string msg_need_more_card_number = "msg_need_more_card_number";
		public const string msg_need_more_gem_number = "msg_need_more_gem_number";
		public const string msg_need_set_to_master = "msg_need_set_to_master";
		public const string msg_no_clan = "msg_no_clan";
		public const string msg_no_clan_suggest = "msg_no_clan_suggest";
		public const string msg_no_donate_info = "msg_no_donate_info";
		public const string msg_no_follow = "msg_no_follow";
		public const string msg_no_friends = "msg_no_friends";
		public const string msg_no_request = "msg_no_request";
		public const string msg_not_available_yet = "msg_not_available_yet";
		public const string msg_not_enough_card = "msg_not_enough_card";
		public const string msg_not_enough_level = "msg_not_enough_level";
		public const string msg_not_enough_medal = "msg_not_enough_medal";
		public const string msg_not_enough_reset_quota = "msg_not_enough_reset_quota";
		public const string msg_not_enough_ticket = "msg_not_enough_ticket";
		public const string msg_not_member_in_clan = "msg_not_member_in_clan";
		public const string msg_not_started_yet = "msg_not_started_yet";
		public const string msg_notice_in_trial = "msg_notice_in_trial";
		public const string msg_npc_videoendgame = "msg_npc_videoendgame";
		public const string msg_permission_error = "msg_permission_error";
		public const string msg_play_with_friend = "msg_play_with_friend";
		public const string msg_player_joined_other_clan = "msg_player_joined_other_clan";
		public const string msg_player_not_exist = "msg_player_not_exist";
		public const string msg_player_was_member_your_clan = "msg_player_was_member_your_clan";
		public const string msg_please_wait_from_server = "msg_please_wait_from_server";
		public const string msg_premium_pack = "msg_premium_pack";
		public const string msg_premium_pack_time_again = "msg_premium_pack_time_again";
		public const string msg_premium_pack_time_expired = "msg_premium_pack_time_expired";
		public const string msg_purchase = "msg_purchase";
		public const string msg_purchase_got_extra_discount = "msg_purchase_got_extra_discount";
		public const string msg_purchase_to_get_extra_discount = "msg_purchase_to_get_extra_discount";
		public const string msg_pvp_connect_to_server = "msg_pvp_connect_to_server";
		public const string msg_pvp_empty_mail_box = "msg_pvp_empty_mail_box";
		public const string msg_pvp_finding_opponents = "msg_pvp_finding_opponents";
		public const string msg_pvp_invitation = "msg_pvp_invitation";
		public const string msg_pvp_no_opponents = "msg_pvp_no_opponents";
		public const string msg_pvp_rank_down = "msg_pvp_rank_down";
		public const string msg_pvp_rank_up = "msg_pvp_rank_up";
		public const string msg_pvp_remove_data_client = "msg_pvp_remove_data_client";
		public const string msg_pvp_remove_data_server = "msg_pvp_remove_data_server";
		public const string msg_pvp_sure_sync_data = "msg_pvp_sure_sync_data";
		public const string msg_pvp_sync_data = "msg_pvp_sync_data";
		public const string msg_pvp_sync_data_other = "msg_pvp_sync_data_other";
		public const string msg_quit_clan = "msg_quit_clan";
		public const string msg_rate_enjoy_ques = "msg_rate_enjoy_ques";
		public const string msg_rate_free_updates = "msg_rate_free_updates";
		public const string msg_rate_rate_ques = "msg_rate_rate_ques";
		public const string msg_rate_thanks = "msg_rate_thanks";
		public const string msg_reach_max_vip = "msg_reach_max_vip";
		public const string msg_receive_reward = "msg_receive_reward";
		public const string msg_receive_up_to = "msg_receive_up_to";
		public const string msg_replace_all_current_item_with_random = "msg_replace_all_current_item_with_random";
		public const string msg_request_cooldown_not_over_yet = "msg_request_cooldown_not_over_yet";
		public const string msg_request_duplicate = "msg_request_duplicate";
		public const string msg_request_fully = "msg_request_fully";
		public const string msg_request_no_longer_available = "msg_request_no_longer_available";
		public const string msg_request_not_enough_quota = "msg_request_not_enough_quota";
		public const string msg_reset_more = "msg_reset_more";
		public const string msg_reward_can_claim_after = "msg_reward_can_claim_after";
		public const string msg_reward_can_only_claimed_once = "msg_reward_can_only_claimed_once";
		public const string msg_reward_is_base = "msg_reward_is_base";
		public const string msg_season_score_will_be_reset = "msg_season_score_will_be_reset";
		public const string msg_select_card_type = "msg_select_card_type";
		public const string msg_select_power_up = "msg_select_power_up";
		public const string msg_shop_will_be_reset = "msg_shop_will_be_reset";
		public const string msg_stars_to_unlock = "msg_stars_to_unlock";
		public const string msg_starter_pack = "msg_starter_pack";
		public const string msg_starter_pack_warning = "msg_starter_pack_warning";
		public const string msg_thank_send_mail = "msg_thank_send_mail";
		public const string msg_the_more_win_get = "msg_the_more_win_get";
		public const string msg_to_build_team = "msg_to_build_team";
		public const string msg_tourmanet_not_start_yet = "msg_tourmanet_not_start_yet";
		public const string msg_unlock_aircraft_by_collect_card = "msg_unlock_aircraft_by_collect_card";
		public const string msg_unlock_and_win_battles = "msg_unlock_and_win_battles";
		public const string msg_unlock_bonus_reward_first = "msg_unlock_bonus_reward_first";
		public const string msg_unlock_by_card = "msg_unlock_by_card";
		public const string msg_unlock_by_level = "msg_unlock_by_level";
		public const string msg_unlock_evolve_claim_reward = "msg_unlock_evolve_claim_reward";
		public const string msg_unlock_level_to_purchase = "msg_unlock_level_to_purchase";
		public const string msg_upgrade = "msg_upgrade";
		public const string msg_upgrading_member = "msg_upgrading_member";
		public const string msg_use_another_aircraft = "msg_use_another_aircraft";
		public const string msg_video_reward = "msg_video_reward";
		public const string msg_vip_pack = "msg_vip_pack";
		public const string msg_wait_opponent = "msg_wait_opponent";
		public const string msg_wait_until_battle_end = "msg_wait_until_battle_end";
		public const string msg_wait_until_ready = "msg_wait_until_ready";
		public const string msg_win_battle_first = "msg_win_battle_first";
		public const string msg_win_need_next_reward = "msg_win_need_next_reward";
		public const string msg_win_the_battle_to_get_reward = "msg_win_the_battle_to_get_reward";
		public const string msg_wingman_auto_galting_gun = "msg_wingman_auto_galting_gun";
		public const string msg_wingman_double_galting = "msg_wingman_double_galting";
		public const string msg_wingman_gatling_gun = "msg_wingman_gatling_gun";
		public const string msg_wingman_homing_missile = "msg_wingman_homing_missile";
		public const string msg_wingman_lazer = "msg_wingman_lazer";
		public const string msg_wingman_splasher = "msg_wingman_splasher";
		public const string msg_you_can_request_8_hours = "msg_you_can_request_8_hours";
		public const string msg_you_claimed = "msg_you_claimed";
		public const string msg_you_had_in_clan = "msg_you_had_in_clan";
		public const string msg_you_had_requested = "msg_you_had_requested";
		public const string msg_you_have_invited_to_friend = "msg_you_have_invited_to_friend";
		public const string msg_you_have_to_set_master = "msg_you_have_to_set_master";
		public const string msg_you_not_member_in_clan = "msg_you_not_member_in_clan";
		public const string msg_your_clan_full = "msg_your_clan_full";
		public const string msg_your_rank = "msg_your_rank";
		public const string msg_your_teammate_out = "msg_your_teammate_out";
		public const string music = "music";
		public const string name_request = "name_request";
		public const string next = "next";
		public const string no = "no";
		public const string no_available_exchange = "no_available_exchange";
		public const string no_one_has_donate = "no_one_has_donate";
		public const string normal = "normal";
		public const string notifi_ads_not_available = "notifi_ads_not_available";
		public const string notifi_claimed_reward = "notifi_claimed_reward";
		public const string notifi_comeback_tomorrow = "notifi_comeback_tomorrow";
		public const string notifi_emp_activated = "notifi_emp_activated";
		public const string notifi_enter_giftcode = "notifi_enter_giftcode";
		public const string notifi_failed_to_connect = "notifi_failed_to_connect";
		public const string notifi_get_gold_infinity_pack = "notifi_get_gold_infinity_pack";
		public const string notifi_giftcode_notfound = "notifi_giftcode_notfound";
		public const string notifi_giftcode_used = "notifi_giftcode_used";
		public const string notifi_login_facebook_fail = "notifi_login_facebook_fail";
		public const string notifi_login_facebook_succeeded = "notifi_login_facebook_succeeded";
		public const string notifi_login_gamecenter_fail = "notifi_login_gamecenter_fail";
		public const string notifi_login_gamecenter_succeeded = "notifi_login_gamecenter_succeeded";
		public const string notifi_luckywheel_need_more_ticket = "notifi_luckywheel_need_more_ticket";
		public const string notifi_luckywheel_wait_spin = "notifi_luckywheel_wait_spin";
		public const string notifi_need_evolve_spaceship = "notifi_need_evolve_spaceship";
		public const string notifi_need_evolve_wing = "notifi_need_evolve_wing";
		public const string notifi_need_evolve_wingman = "notifi_need_evolve_wingman";
		public const string notifi_need_more_friend = "notifi_need_more_friend";
		public const string notifi_need_more_loginday = "notifi_need_more_loginday";
		public const string notifi_network_error = "notifi_network_error";
		public const string notifi_network_not_found = "notifi_network_not_found";
		public const string notifi_not_enough_card = "notifi_not_enough_card";
		public const string notifi_not_enough_coin = "notifi_not_enough_coin";
		public const string notifi_not_enough_gem = "notifi_not_enough_gem";
		public const string notifi_not_enough_resources = "notifi_not_enough_resources";
		public const string notifi_purchases_restored_failed = "notifi_purchases_restored_failed";
		public const string notifi_purchases_restored_success = "notifi_purchases_restored_success";
		public const string notifi_pvp_character = "notifi_pvp_character";
		public const string notifi_pvp_coming_soon = "notifi_pvp_coming_soon";
		public const string notifi_pvp_disconnect_server = "notifi_pvp_disconnect_server";
		public const string notifi_pvp_more_gold = "notifi_pvp_more_gold";
		public const string notifi_pvp_room_destroyed = "notifi_pvp_room_destroyed";
		public const string notifi_shop_upgraded = "notifi_shop_upgraded";
		public const string notifi_unlock_aircraft_first = "notifi_unlock_aircraft_first";
		public const string notifi_unlock_device_name = "notifi_unlock_device_name";
		public const string notifi_unlock_wing_first = "notifi_unlock_wing_first";
		public const string notifi_unlock_wingman_first = "notifi_unlock_wingman_first";
		public const string notifi_videoads_tomorrow = "notifi_videoads_tomorrow";
		public const string notifi_videoads_wait = "notifi_videoads_wait";
		public const string notification = "notification";
		public const string npc_popup_tutorial_mess = "npc_popup_tutorial_mess";
		public const string npc_popup_tutorial_title = "npc_popup_tutorial_title";
		public const string objective = "objective";
		public const string offline = "offline";
		public const string old = "old";
		public const string oneTimeOffer = "oneTimeOffer";
		public const string ongoing = "ongoing";
		public const string online = "online";
		public const string only_today = "only_today";
		public const string open = "open";
		public const string open_in = "open_in";
		public const string opponent_disconnect = "opponent_disconnect";
		public const string options = "options";
		public const string or = "or";
		public const string or_reset_now = "or_reset_now";
		public const string out_of_reset = "out_of_reset";
		public const string panel_congrat_new_aircraft = "panel_congrat_new_aircraft";
		public const string panel_congrat_new_drone = "panel_congrat_new_drone";
		public const string panel_congrat_new_rank_aircraft = "panel_congrat_new_rank_aircraft";
		public const string panel_congrat_new_rank_drone = "panel_congrat_new_rank_drone";
		public const string panel_congrat_new_rank_wing = "panel_congrat_new_rank_wing";
		public const string panel_congrat_new_wing = "panel_congrat_new_wing";
		public const string pass_level = "pass_level";
		public const string pause = "pause";
		public const string play = "play";
		public const string player = "player";
		public const string popup_30_days_already_earned = "popup_30_days_already_earned";
		public const string popup_evolve_max_level = "popup_evolve_max_level";
		public const string popup_iap_purchase_remove_ads = "popup_iap_purchase_remove_ads";
		public const string popup_vip_100_percent_drop_gold = "popup_vip_100_percent_drop_gold";
		public const string popup_vip_10_percent_plane_damage = "popup_vip_10_percent_plane_damage";
		public const string popup_vip_1_life_per_day = "popup_vip_1_life_per_day";
		public const string popup_vip_1_power_up_per_day = "popup_vip_1_power_up_per_day";
		public const string popup_vip_1_spin_per_day = "popup_vip_1_spin_per_day";
		public const string popup_vip_20_percent_drone_damage = "popup_vip_20_percent_drone_damage";
		public const string popup_vip_2_percent_plane_damage = "popup_vip_2_percent_plane_damage";
		public const string popup_vip_30_percent_drop_gold = "popup_vip_30_percent_drop_gold";
		public const string popup_vip_50_percent_drop_gold = "popup_vip_50_percent_drop_gold";
		public const string popup_vip_5_percent_plane_damage = "popup_vip_5_percent_plane_damage";
		public const string popup_vip_benefit = "popup_vip_benefit";
		public const string popup_vip_buy_to_get_vip_points = "popup_vip_buy_to_get_vip_points";
		public const string popup_vip_features = "popup_vip_features";
		public const string popup_vip_need_more_points = "popup_vip_need_more_points";
		public const string popup_vip_progress = "popup_vip_progress";
		public const string popup_vip_title = "popup_vip_title";
		public const string popup_vip_x2_reward_daily_task = "popup_vip_x2_reward_daily_task";
		public const string power = "power";
		public const string power_up = "power_up";
		public const string prepare = "prepare";
		public const string promote_event = "promote_event";
		public const string purchase = "purchase";
		public const string purchaseToGetVip = "purchaseToGetVip";
		public const string pvp_rank_reward = "pvp_rank_reward";
		public const string quantity = "quantity";
		public const string quantity_key = "quantity_key";
		public const string quit = "quit";
		public const string rank = "rank";
		public const string rank_1st = "rank_1st";
		public const string rank_2nd = "rank_2nd";
		public const string rank_3rd = "rank_3rd";
		public const string rank_4th = "rank_4th";
		public const string rank_a = "rank_a";
		public const string rank_b = "rank_b";
		public const string rank_c = "rank_c";
		public const string rank_pvp_bronze = "rank_pvp_bronze";
		public const string rank_pvp_challenger = "rank_pvp_challenger";
		public const string rank_pvp_diamond = "rank_pvp_diamond";
		public const string rank_pvp_gold = "rank_pvp_gold";
		public const string rank_pvp_legend = "rank_pvp_legend";
		public const string rank_pvp_master = "rank_pvp_master";
		public const string rank_pvp_platinum = "rank_pvp_platinum";
		public const string rank_pvp_silver = "rank_pvp_silver";
		public const string rank_s = "rank_s";
		public const string rank_ss = "rank_ss";
		public const string rank_sss = "rank_sss";
		public const string read_less = "read_less";
		public const string read_more = "read_more";
		public const string ready = "ready";
		public const string receive_when_tournament_end = "receive_when_tournament_end";
		public const string register = "register";
		public const string reload = "reload";
		public const string replay = "replay";
		public const string request = "request";
		public const string request_sent = "request_sent";
		public const string required_level = "required_level";
		public const string requirements = "requirements";
		public const string reset_in = "reset_in";
		public const string reset_shop = "reset_shop";
		public const string result = "result";
		public const string reward = "reward";
		public const string rewards_are_unlocked = "rewards_are_unlocked";
		public const string royal_pack_extra_aircraft_skill = "royal_pack_extra_aircraft_skill";
		public const string royal_pack_free_golden_chest = "royal_pack_free_golden_chest";
		public const string royal_pack_gem_daily = "royal_pack_gem_daily";
		public const string royal_pack_gem_instantly = "royal_pack_gem_instantly";
		public const string royal_pack_gem_total = "royal_pack_gem_total";
		public const string royal_pack_revive_discount = "royal_pack_revive_discount";
		public const string royalty_pack_title = "royalty_pack_title";
		public const string rule = "rule";
		public const string save = "save";
		public const string save_change = "save_change";
		public const string score = "score";
		public const string search = "search";
		public const string search_for = "search_for";
		public const string season_score = "season_score";
		public const string select = "select";
		public const string selected = "selected";
		public const string send = "send";
		public const string sensitivity = "sensitivity";
		public const string settings = "settings";
		public const string share = "share";
		public const string shop = "shop";
		public const string skill10_name = "skill10_name";
		public const string skill11_name = "skill11_name";
		public const string skill12_name = "skill12_name";
		public const string skill13_name = "skill13_name";
		public const string skill14_name = "skill14_name";
		public const string skill15_name = "skill15_name";
		public const string skill16_name = "skill16_name";
		public const string skill17_name = "skill17_name";
		public const string skill18_name = "skill18_name";
		public const string skill19_name = "skill19_name";
		public const string skill1_name = "skill1_name";
		public const string skill20_name = "skill20_name";
		public const string skill21_name = "skill21_name";
		public const string skill22_name = "skill22_name";
		public const string skill23_name = "skill23_name";
		public const string skill24_name = "skill24_name";
		public const string skill25_name = "skill25_name";
		public const string skill26_name = "skill26_name";
		public const string skill27_name = "skill27_name";
		public const string skill28_name = "skill28_name";
		public const string skill29_name = "skill29_name";
		public const string skill2_name = "skill2_name";
		public const string skill30_name = "skill30_name";
		public const string skill31_name = "skill31_name";
		public const string skill32_name = "skill32_name";
		public const string skill3_name = "skill3_name";
		public const string skill4_name = "skill4_name";
		public const string skill5_name = "skill5_name";
		public const string skill6_name = "skill6_name";
		public const string skill7_name = "skill7_name";
		public const string skill8_name = "skill8_name";
		public const string skill9_name = "skill9_name";
		public const string sold_out = "sold_out";
		public const string sound = "sound";
		public const string speed = "speed";
		public const string star = "star";
		public const string start = "start";
		public const string submit = "submit";
		public const string success = "success";
		public const string suggest_team = "suggest_team";
		public const string summary = "summary";
		public const string summer_holiday_exchange_title_1 = "summer_holiday_exchange_title_1";
		public const string summer_holiday_exchange_title_2 = "summer_holiday_exchange_title_2";
		public const string summer_holiday_exchange_title_3 = "summer_holiday_exchange_title_3";
		public const string summer_holiday_mission_progress_1 = "summer_holiday_mission_progress_1";
		public const string summer_holiday_mission_progress_2 = "summer_holiday_mission_progress_2";
		public const string summer_holiday_mission_progress_3 = "summer_holiday_mission_progress_3";
		public const string summer_holiday_mission_progress_4 = "summer_holiday_mission_progress_4";
		public const string summer_holiday_mission_rule_1 = "summer_holiday_mission_rule_1";
		public const string summer_holiday_mission_rule_2 = "summer_holiday_mission_rule_2";
		public const string summer_holiday_mission_rule_3 = "summer_holiday_mission_rule_3";
		public const string summer_holiday_msg_exchange_1 = "summer_holiday_msg_exchange_1";
		public const string summer_holiday_msg_exchange_2 = "summer_holiday_msg_exchange_2";
		public const string summer_holiday_msg_mission_1 = "summer_holiday_msg_mission_1";
		public const string summer_holiday_msg_mission_2 = "summer_holiday_msg_mission_2";
		public const string system = "system";
		public const string tap_to_close = "tap_to_close";
		public const string team_score = "team_score";
		public const string team_type = "team_type";
		public const string text_double_ticket = "text_double_ticket";
		public const string text_only_purchase_one = "text_only_purchase_one";
		public const string ticket = "ticket";
		public const string time = "time";
		public const string title_2vs2_invitation = "title_2vs2_invitation";
		public const string title_betting = "title_betting";
		public const string title_bonus_reward = "title_bonus_reward";
		public const string title_buy_ticket = "title_buy_ticket";
		public const string title_clan_have_no_member = "title_clan_have_no_member";
		public const string title_daily_login = "title_daily_login";
		public const string title_daily_quest = "title_daily_quest";
		public const string title_daily_quest_collect_gold = "title_daily_quest_collect_gold";
		public const string title_daily_quest_collect_items = "title_daily_quest_collect_items";
		public const string title_daily_quest_complete_any_map = "title_daily_quest_complete_any_map";
		public const string title_daily_quest_defeat_bosses = "title_daily_quest_defeat_bosses";
		public const string title_daily_quest_kill_enemies = "title_daily_quest_kill_enemies";
		public const string title_daily_quest_lucky_wheel = "title_daily_quest_lucky_wheel";
		public const string title_daily_quest_play_endless = "title_daily_quest_play_endless";
		public const string title_daily_quest_play_pvp = "title_daily_quest_play_pvp";
		public const string title_daily_quest_rate_game = "title_daily_quest_rate_game";
		public const string title_daily_quest_share_fb = "title_daily_quest_share_fb";
		public const string title_daily_quest_use_activeskill = "title_daily_quest_use_activeskill";
		public const string title_daily_quest_use_emp = "title_daily_quest_use_emp";
		public const string title_daily_quest_use_power_up = "title_daily_quest_use_power_up";
		public const string title_daily_quest_win_pvp = "title_daily_quest_win_pvp";
		public const string title_donate_card_10_times = "title_donate_card_10_times";
		public const string title_encourage = "title_encourage";
		public const string title_enter_id = "title_enter_id";
		public const string title_evolve_max_upgrade = "title_evolve_max_upgrade";
		public const string title_evolve_new_changes = "title_evolve_new_changes";
		public const string title_evolve_new_passive = "title_evolve_new_passive";
		public const string title_free_reward = "title_free_reward";
		public const string title_friends_info = "title_friends_info";
		public const string title_great_offer = "title_great_offer";
		public const string title_iap_captain = "title_iap_captain";
		public const string title_iap_challenger = "title_iap_challenger";
		public const string title_iap_commander = "title_iap_commander";
		public const string title_iap_conqueror = "title_iap_conqueror";
		public const string title_iap_gem_pack = "title_iap_gem_pack";
		public const string title_iap_infinity_gold = "title_iap_infinity_gold";
		public const string title_iap_remove_ads = "title_iap_remove_ads";
		public const string title_iap_starter = "title_iap_starter";
		public const string title_iap_super_offer = "title_iap_super_offer";
		public const string title_iap_young_pilot = "title_iap_young_pilot";
		public const string title_in_progress = "title_in_progress";
		public const string title_invite_to_team = "title_invite_to_team";
		public const string title_is_friend = "title_is_friend";
		public const string title_lucky_box = "title_lucky_box";
		public const string title_lucky_box_best_rewards = "title_lucky_box_best_rewards";
		public const string title_lucky_box_diamond_chest = "title_lucky_box_diamond_chest";
		public const string title_lucky_box_golden_chest = "title_lucky_box_golden_chest";
		public const string title_lucky_box_your_reward = "title_lucky_box_your_reward";
		public const string title_lucky_wheel = "title_lucky_wheel";
		public const string title_mail_sent = "title_mail_sent";
		public const string title_open_golden_chest = "title_open_golden_chest";
		public const string title_play_with_friend = "title_play_with_friend";
		public const string title_player_already_in_a_team = "title_player_already_in_a_team";
		public const string title_player_found = "title_player_found";
		public const string title_post_war_boost = "title_post_war_boost";
		public const string title_premium_pack = "title_premium_pack";
		public const string title_private = "title_private";
		public const string title_pvp_current_account = "title_pvp_current_account";
		public const string title_pvp_invitation = "title_pvp_invitation";
		public const string title_pvp_mail_box = "title_pvp_mail_box";
		public const string title_pvp_old_account = "title_pvp_old_account";
		public const string title_pvp_player_info = "title_pvp_player_info";
		public const string title_pvp_rank_down = "title_pvp_rank_down";
		public const string title_pvp_rank_up = "title_pvp_rank_up";
		public const string title_quick_match = "title_quick_match";
		public const string title_rank_rewards = "title_rank_rewards";
		public const string title_request_card_2_times = "title_request_card_2_times";
		public const string title_search_friend = "title_search_friend";
		public const string title_select_all = "title_select_all";
		public const string title_select_content = "title_select_content";
		public const string title_select_team_member = "title_select_team_member";
		public const string title_star_chest = "title_star_chest";
		public const string title_star_chest_extra = "title_star_chest_extra";
		public const string title_starter_pack = "title_starter_pack";
		public const string title_summer_holiday = "title_summer_holiday";
		public const string title_team_mail = "title_team_mail";
		public const string title_team_mission = "title_team_mission";
		public const string title_threat = "title_threat";
		public const string title_unfriend = "title_unfriend";
		public const string title_unlock_reward = "title_unlock_reward";
		public const string title_upgrade_Wingman = "title_upgrade_Wingman";
		public const string title_upgrade_drop_chance = "title_upgrade_drop_chance";
		public const string title_upgrade_laser_power = "title_upgrade_laser_power";
		public const string title_upgrade_main_weapon = "title_upgrade_main_weapon";
		public const string title_upgrade_missile_power = "title_upgrade_missile_power";
		public const string title_upgrade_secondary_weapon = "title_upgrade_secondary_weapon";
		public const string title_upgrade_super_weapon = "title_upgrade_super_weapon";
		public const string title_upgrade_wind_slash_power = "title_upgrade_wind_slash_power";
		public const string title_vip_pack = "title_vip_pack";
		public const string title_wait_3_seconds = "title_wait_3_seconds";
		public const string title_war_notify = "title_war_notify";
		public const string title_warning = "title_warning";
		public const string title_win_pvp_5_times = "title_win_pvp_5_times";
		public const string title_wing = "title_wing";
		public const string tooltip_loading_scene_1 = "tooltip_loading_scene_1";
		public const string tooltip_loading_scene_10 = "tooltip_loading_scene_10";
		public const string tooltip_loading_scene_11 = "tooltip_loading_scene_11";
		public const string tooltip_loading_scene_2 = "tooltip_loading_scene_2";
		public const string tooltip_loading_scene_3 = "tooltip_loading_scene_3";
		public const string tooltip_loading_scene_4 = "tooltip_loading_scene_4";
		public const string tooltip_loading_scene_5 = "tooltip_loading_scene_5";
		public const string tooltip_loading_scene_6 = "tooltip_loading_scene_6";
		public const string tooltip_loading_scene_7 = "tooltip_loading_scene_7";
		public const string tooltip_loading_scene_8 = "tooltip_loading_scene_8";
		public const string tooltip_loading_scene_9 = "tooltip_loading_scene_9";
		public const string tooltip_touch_to_change = "tooltip_touch_to_change";
		public const string top_10_ranking_reward = "top_10_ranking_reward";
		public const string tournament = "tournament";
		public const string unequip = "unequip";
		public const string unfollow = "unfollow";
		public const string unlock = "unlock";
		public const string up_to_master = "up_to_master";
		public const string up_to_number = "up_to_number";
		public const string up_to_vice = "up_to_vice";
		public const string upgrade = "upgrade";
		public const string user_id = "user_id";
		public const string victory = "victory";
		public const string video = "video";
		public const string video_reward = "video_reward";
		public const string view = "view";
		public const string vip_daily_rewards = "vip_daily_rewards";
		public const string vip_key = "vip_key";
		public const string wait = "wait";
		public const string warning = "warning";
		public const string watch = "watch";
		public const string win = "win";
		public const string win_rate = "win_rate";
		public const string wing1_name = "wing1_name";
		public const string wing2_name = "wing2_name";
		public const string wing3_name = "wing3_name";
		public const string wingman1_name = "wingman1_name";
		public const string wingman2_name = "wingman2_name";
		public const string wingman3_name = "wingman3_name";
		public const string wingman4_name = "wingman4_name";
		public const string wingman5_name = "wingman5_name";
		public const string wingman6_name = "wingman6_name";
		public const string world = "world";
		public const string x2Gold = "x2Gold";
		public const string x2_gem_today = "x2_gem_today";
		public const string x2_gold_today = "x2_gold_today";
		public const string yes = "yes";
		public const string you_request = "you_request";
	}
}