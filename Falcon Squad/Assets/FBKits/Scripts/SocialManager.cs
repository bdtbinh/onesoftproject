﻿using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System;
using Facebook.MiniJSON;
using System.Collections;
using Mp.Pvp;

public class SocialManager : MonoBehaviour
{
    private static SocialManager _instance;
    public static SocialManager Instance
    {
        get { return _instance; }
    }

    [SerializeField]
    private string android_URL;

    public string facebookFanpageID;

    public string facebookGroupID;

    public string instagramUserName;

    public string twitterUserName;
    public string twitterID;

    [Tooltip("Waiting time for open a browser instead of a social app")]
    [SerializeField]
    private float waitingTime = 1.2f;

    private enum SocialType
    {
        None = -1,
        LikeFB = 0,
        JoinFBGroup,
        FollowInstagram,
        FollowTwitter
    }

    private SocialType rewardSocialType = SocialType.None;

    // Web urls - use this for computers or if the user doesn't have the app installed on phones
    private string[] webUrls = {
    "https://www.facebook.com/",
    "https://www.facebook.com/groups/",
    "https://www.instagram.com/",
    "https://twitter.com/",
    };

    // Use this for android - loads the app - if don't have the app, default to weburls
    private string[] appUrls = {

#if UNITY_ANDROID
    "fb://page/",
    "fb://group/",
#else 
    "fb://page/?id=",
    "fb://group/?id=",
#endif
    "instagram://user?username=",
    "twitter://user?user_id=",
    };


    // Events:
    public Action OnFacebookLoginSuccessed;
    public Action OnFacebookLoginFail;
    public Action OnLikeFBReward;
    public Action OnJoinGroupFBReward;
    public Action OnFollowInstagramReward;
    public Action OnFollowTwitterReward;
    public Action OnFacebookShareSuccessed;

    //Login GameCenter Events
    public Action OnGCLoginSuccessed;
    public Action OnGCLoginFailed;


    #region Init

    private void InitSocialLinks()
    {
        webUrls[0] += facebookFanpageID;
        webUrls[1] += facebookGroupID;
        webUrls[2] += instagramUserName;
        webUrls[3] += twitterUserName;

        appUrls[0] += facebookFanpageID;
        appUrls[1] += facebookGroupID;
        appUrls[2] += instagramUserName;
        appUrls[3] += twitterID;
    }

    void Awake()
    {
        InitSocialLinks();

        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(_instance);
        }
        else
        {
            DestroyImmediate(this);
            return;
        }

        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(FBInitCallback, FBOnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    private void FBInitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void FBOnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again

            Time.timeScale = 1f;

        }
    }

    #endregion

    #region LogIn/LogOut

    private void Start()
    {
        //Debug.LogError("twitterID:" + twitterID);
    }

    public void FBLogIn()
    {
        if (!FB.IsInitialized)
            return;

        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }


    public void SetFriendsFacebookToServer()
    {
        if (GameContext.IS_CHINA_VERSION)
        {
            if (Social.localUser.authenticated && Social.localUser.friends.Length > 0)
            {
                JSONObject jo = "[]".ToJsonObject();
                for (int i = 0; i < Social.localUser.friends.Length; i++)
                {
                    jo.AddItem(Social.localUser.friends[i].id);
                }
                Debug.Log("data friends : " + jo.ToJson());
                new CSAppCenterFriends(jo.ToJson()).Send();
            }
        }
        else
        {
            string query = "/me?fields=friends";
            FB.API(query, HttpMethod.GET, result =>
            {
                if (result.Error == null)
                {
                    FBUser user = JsonFx.Json.JsonReader.Deserialize<FBUser>(result.RawResult);
                    if (user.friends.data.Count > 0)
                    {
                        JSONObject jo = "[]".ToJsonObject();
                        for (int i = 0; i < user.friends.data.Count; i++)
                        {
                            jo.AddItem(user.friends.data[i].id);
                        }
                        Debug.Log("data friends : " + jo.ToJson());
                        new CSFacebookFriends(jo.ToJson()).Send();
                    }
                }
                else
                {
                    Debug.Log("Error SetFriendsFacebookToServer() : " + result.Error);
                }
            });
        }
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log("AuthCallback: " + aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }

            FBSetUserID(aToken.UserId);
            FBSetUserToken(aToken.TokenString);

            if (OnFacebookLoginSuccessed != null)
            {
                OnFacebookLoginSuccessed.Invoke();
            }
        }
        else
        {
            Debug.Log("AuthCallback: " + "login failed!");
            Debug.Log(result.Error);

            if (OnFacebookLoginFail != null)
                OnFacebookLoginFail.Invoke();
        }
    }

    public void FBLogOut()
    {
        FB.LogOut();
    }
    #endregion

    #region Request / Invite

    //public void Request()
    //{
    //    Debug.Log("request twitter");
    //    Application.OpenURL("twitter:///user?screen_name=falconsquadgame");
    //}

    //public void Invite()
    //{
    //    Debug.Log("invite instagram");
    //    Application.OpenURL("https://www.instagram.com/falconsquadgame/");
    //}

    #endregion

    //public Text friendsText;

    //public void GetFriends()
    //{
    //    string query = "/me/friends";
    //    FB.API(query, HttpMethod.GET, result =>
    //    {
    //        var dictionary = Json.Deserialize(result.RawResult) as Dictionary<string, object>;
    //        var friendsList = (List<object>)dictionary["data"];

    //        friendsText.text = string.Empty;

    //        Debug.Log("GetFriends: " + friendsList.Count);
    //        foreach (var dict in friendsList)
    //        {
    //            friendsText.text += ((Dictionary<string, object>)dict)["name"];
    //        }
    //    });
    //}



    #region ShareLink

    public void FBShare()
    {
        FB.ShareLink(new Uri("http://falconsquad.net/falcon-squad.html"), callback: FBShareCallback);

    }

    private void FBShareCallback(IShareResult result)
    {
        if (result.Cancelled || !String.IsNullOrEmpty(result.Error))
        {
            Debug.Log("ShareLink Error: " + result.Error);
        }
        else if (!String.IsNullOrEmpty(result.PostId))
        {
            // Print post identifier of the shared content
            Debug.Log(result.PostId);
        }
        else
        {
            //Reward user ở đây:
            // Share succeeded without postID
            Debug.Log("ShareLink success!");

            if (OnFacebookShareSuccessed != null)
                OnFacebookShareSuccessed.Invoke();
        }
    }

    #endregion

    #region Game Center

    public void LoginGameCenter()
    {
        Social.localUser.Authenticate(success =>
        {
            if (success)
            {
                //string userInfo = "Username: " + Social.localUser.userName +
                //    "\nUser ID: " + Social.localUser.id +
                //    "\nIsUnderage: " + Social.localUser.underage;

                //Debug.Log(userInfo);

                if (OnGCLoginSuccessed != null)
                {
                    OnGCLoginSuccessed.Invoke();
                }
            }
            else
            {
                if (OnGCLoginFailed != null)
                {
                    OnGCLoginFailed.Invoke();
                }
                //Debug.Log("Authentication failed");
            }
        });
    }

    #endregion

    public void FBOnClickBtnLike()
    {
        coCheckForOpenBrowser = CoCheckForOpenBrowser(SocialType.LikeFB);
        StartCoroutine(coCheckForOpenBrowser);
    }


    bool isClickedJoinGroup = false;
    public void FBOnClickBtnJoinGroup()
    {
        if (!isClickedJoinGroup)
        {
            webUrls[1] += CachePvp.sCClientConfig.forumAppId;
            appUrls[1] += CachePvp.sCClientConfig.forumAppId;

            isClickedJoinGroup = true;
        }

        Debug.LogError("vaoOOOOOOOO: " + CachePvp.sCClientConfig.forumAppId);
        coCheckForOpenBrowser = CoCheckForOpenBrowser(SocialType.JoinFBGroup);
        StartCoroutine(coCheckForOpenBrowser);
    }

    public void InstagramOnClickBtnFollow()
    {
        coCheckForOpenBrowser = CoCheckForOpenBrowser(SocialType.FollowInstagram);
        StartCoroutine(coCheckForOpenBrowser);
    }

    public void TwitterOnClickBtnFollow()
    {
        coCheckForOpenBrowser = CoCheckForOpenBrowser(SocialType.FollowTwitter);
        StartCoroutine(coCheckForOpenBrowser);
    }

    public void CheckLoginStatus()
    {
        Debug.Log("check login: " + FB.IsLoggedIn);
    }

    private bool isGamePaused = false;
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            isGamePaused = true;
        }
        else
        {
            if (rewardSocialType == SocialType.None)
                return;

            OnRewardSocial();
        }
    }

    private void OnRewardSocial()
    {
        switch (rewardSocialType)
        {
            case SocialType.LikeFB:
                if (OnLikeFBReward != null)
                    OnLikeFBReward.Invoke();
                break;

            case SocialType.JoinFBGroup:
                if (OnJoinGroupFBReward != null)
                    OnJoinGroupFBReward.Invoke();
                break;

            case SocialType.FollowInstagram:
                if (OnFollowInstagramReward != null)
                    OnFollowInstagramReward.Invoke();
                break;

            case SocialType.FollowTwitter:
                if (OnFollowTwitterReward != null)
                    OnFollowTwitterReward.Invoke();
                break;

            default:
                break;
        }

        rewardSocialType = SocialType.None;
    }

    private void OnDisable()
    {
        if (coCheckForOpenBrowser != null)
            StopCoroutine(coCheckForOpenBrowser);
    }

    //public void FBRequestMe()
    //{
    //    string query = "/me?fields=name,picture";
    //    FB.API(query, HttpMethod.GET, result =>
    //    {
    //        FBUser user = JsonFx.Json.JsonReader.Deserialize<FBUser>(result.RawResult);
    //        //Debug.LogError(user.name);
    //        //Debug.LogError(user.picture.data.height);
    //        //Debug.LogError(user.picture.data.is_silhouette);
    //        //Debug.LogError(user.picture.data.url);
    //        //Debug.LogError(user.picture.data.width);

    //        FBSetNumberOfFriends(user.friends.data.Count);

    //        //user.picture.           //for (int i = 0; i < user.friends.data.Count; i++)
    //        //{
    //        //    Debug.LogError(user.friends.data[i].name + " * " + user.friends.data[i].id);
    //        //}

    //        //Debug.LogError(user.friends.paging.cursors.before);
    //        //Debug.LogError(user.friends.paging.cursors.after);
    //        //Debug.LogError("friend: "+user.friends.summary.total_count);
    //    });
    //}

    public void FBRequestFriendsData(Action OnSuccessed, Action OnFailed)
    {
        string query = "/me?fields=friends";
        FB.API(query, HttpMethod.GET, result =>
        {
            //TextAsset txt = Resources.Load("FacebookTest", typeof(TextAsset)) as TextAsset; // Test load info

            if (result.Error == null)
            {
                FBUser user = JsonFx.Json.JsonReader.Deserialize<FBUser>(result.RawResult);
                FBSetNumberOfFriends(user.friends.data.Count);
                OnSuccessed();
            }
            else
            {
                OnFailed();
            }
        });
    }

    public void FetchAvatar(string userID, int width, int height, Action<Sprite> SetAvatar)
    {
        FB.API(userID + "/picture?type=square&height=" + height + "&width=" + width, HttpMethod.GET, (IGraphResult result) =>
        {
            if (result.Error == null && result.Texture != null)
            {
                SetAvatar(Sprite.Create(result.Texture, new Rect(0, 0, width, height), new Vector2()));
            }
        });
    }

    IEnumerator coCheckForOpenBrowser = null;
    IEnumerator CoCheckForOpenBrowser(SocialType type)
    {
        isGamePaused = false;
        rewardSocialType = type;

        //Debug.LogError(appUrls[(int)type]);
        Application.OpenURL(appUrls[(int)type]);

        yield return new WaitForSeconds(waitingTime);

        if (!isGamePaused)
        {
            Application.OpenURL(webUrls[(int)type]);
        }
    }


    //Facebook logs for analytics
    #region Facebook Logs

    public void FBLogCompleteLevel(string level)
    {
        var p = new Dictionary<string, object>();
        p[Facebook.Unity.AppEventParameterName.Level] = level;
        FB.LogAppEvent(
            Facebook.Unity.AppEventName.AchievedLevel,
            null,
            p
        );
    }

    public void FBLogBuyItem(string package_sku, float priceAmount, string priceCurrency)
    {
        // priceCurrency is a string containing the 3-letter ISO code for the currency
        // that the user spent, and priceAmount is a float containing the quantity spent;
        // packageName is a string containing your SKU code for the thing they bought
        var iapParameters = new Dictionary<string, object>();
        iapParameters["mygame_packagename"] = package_sku;
        FB.LogPurchase(priceAmount, priceCurrency, iapParameters);
    }


    public void FBLogCompleteViewVideo()
    {
        FB.LogAppEvent(
            "CompleteViewVideo",
            1,
            null
        );
    }

    #endregion



    #region Facebook Cache

    public string FBGetUserID()
    {
        return PlayerPrefs.GetString("M_FacebookUserID", "Unknown");
    }

    public void FBSetUserID(string userID)
    {
        PlayerPrefs.SetString("M_FacebookUserID", userID);
    }

    public string FBGetUserToken()
    {
        return PlayerPrefs.GetString("M_FacebookUserToken", "");
    }

    public void FBSetUserToken(string userToken)
    {
        PlayerPrefs.SetString("M_FacebookUserToken", userToken);
    }

    public int FBGetNumberOfFriends()
    {
        return CachePvp.CountFriendsMinh;
    }

    public void FBSetNumberOfFriends(int number)
    {
        PlayerPrefs.SetInt("M_FacebookFriendsNumber", number);
    }

    public bool FBIsAlreadyLike()
    {
        return PlayerPrefs.GetInt("M_AlreadyLikeFacebook", 0) == 1;
    }

    public void FBSetAlreadyLike()
    {
        PlayerPrefs.SetInt("M_AlreadyLikeFacebook", 1);
    }

    public bool FBIsAlreadyJoinGroup()
    {
        return PlayerPrefs.GetInt("M_AlreadyJoinGroupFacebook", 0) == 1;
    }

    public void FBSetAlreadyJoinGroup()
    {
        PlayerPrefs.SetInt("M_AlreadyJoinGroupFacebook", 1);
    }

    public bool FBIsAlreadyClaimFriendsReward(int numberOfFriends)
    {
        return PlayerPrefs.GetInt("M_AlreadyHaveFriendsReward" + numberOfFriends, 0) == 1;
    }

    public void FBSetAlreadyClaimFriendsReward(int numberOfFriends)
    {
        PlayerPrefs.SetInt("M_AlreadyHaveFriendsReward" + numberOfFriends, 1);
    }

    //public bool FBIsAlreadyShare()
    //{
    //    return PlayerPrefs.GetInt("M_AlreadyShareFacebook", 0) == 1;
    //}

    public void FBSetAlreadyShare()
    {
        PlayerPrefs.SetInt("M_AlreadyShareFacebook", 1);
    }

    #endregion


    #region Instagram/Twitter Cache

    public bool InstagramIsAlreadyFollow()
    {
        return PlayerPrefs.GetInt("M_AlreadyFollowInstagram", 0) == 1;
    }

    public void InstagramSetAlreadyFollow()
    {
        PlayerPrefs.SetInt("M_AlreadyFollowInstagram", 1);
    }

    public bool TwitterIsAlreadyFollow()
    {
        return PlayerPrefs.GetInt("M_AlreadyFollowTwitter", 0) == 1;
    }

    public void TwitterSetAlreadyFollow()
    {
        PlayerPrefs.SetInt("M_AlreadyFollowTwitter", 1);
    }

    #endregion

}


