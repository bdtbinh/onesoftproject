#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("DgtdFQYMHgwcI9hhT5x+Afb8Y4VxKGl7e31lbXsoaWtrbXh8aWZrbXhkbShLbXp8YW5ha2l8YWdmKEl9bD0rHUMdURW7nP/+lJbHWLLJUFgXmdMWT1jjDeVWcYwl4z6qX0Rd5L8TtZtKLBoizwcVvkWUVmvAQ4gfQdB+lzscbal/nMElCgsJCAmriglyOIoJfjgGDgtdFQcJCfcMDAsKCYoJCA4BIo5Ajv9rbA0JOIn6OCIONS5vKII7Yv8FisfW46sn8VtiU2wAVjiKCRkOC10VKAyKCQA4igkMODs+UjhqOQM4AQ4LXQwOGwpdWzkbDQgLigkHCDiKCQIKigkJCOyZoQFvhwC8KP/DpCQoZ3i+Nwk4hL9LxyZIrv9PRXcAVjgXDgtdFSsMEDgeOBkOC10MAhsCSXh4ZG0oQWZrJjknOInLDgAjDgkNDQ8KCjiJvhKJu6OreZpPW13JpydJu/Dz63jF7qtEwRF6/VUG3XdXk/otC7Jdh0VVBfks6uPZv3jXB03pL8L5ZXDl770fHwwOGwpdWzkbOBkOC10MAhsCSXh4yGs7f/8yDyRe49IHKQbSsnsRR70XjYuNE5E1Tz/6oZNIhiTcuZga0IgcI9hhT5x+Afb8Y4UmSK7/T0V3TXYXRGNYnkmBzHxqAxiLSY87gonRPnfJj13Rr5GxOkrz0N15lnapWgeVNfsjQSASwPbGvbEG0VYU3sM1emlrfGFrbSh7fGl8bWVtZnx7JjgeOBwOC10MCxsFSXh4ZG0oWmdnfC44LA4LXQwDGxVJeHhkbShLbXp8BQ4BIo5Ajv8FCQkNDQgLigkJCFR8YW5ha2l8bShqcShpZnEoeGl6fD6RRCVwv+WEk9T7f5P6ftp/OEfJKGduKHxgbSh8YG1mKGl4eGRha2koaWZsKGttenxhbmFraXxhZ2YoeGFuYWtpfGFnZihJfXxgZ3phfHE5gxGB1vFDZP0Poyo4CuAQNvBYAdskKGttenxhbmFraXxtKHhnZGFrcQAjDgkNDQ8KCR4WYHx8eHsyJyd/PTo5PDg7PlIfBTs9ODo4MTo5PDgijkCO/wUJCQ0NCDhqOQM4AQ4LXaDUdio9wi3d0QfeY9yqLCsZ/6mkDjgHDgtdFRsJCfcMDTgLCQn3OBVRrw0BdB9IXhkWfNu/gyszT6vdZ1ptZGFpZmttKGdmKHxgYXsoa216OIoMsziKC6uoCwoJCgoJCjgFDgFmbChrZ2ZsYXxhZ2Z7KGduKH17bb0ypfwHBgiaA7kpHiZ83TQF02oetvx7k+babAfDcUc80Ko28XD3Y8CHe4lozhNTASeauvBMQPhoMJYd/Q/kdTGLg1so2zDMubeSRwJj9yP0amRtKHt8aWZsaXpsKHxtemV7KGlkbShBZmsmOS44LA4LXQwDGxVJeHdJoJDx2cJulCxjGdirs+wTIssXeGRtKFpnZ3woS0k4Fh8FOD44PDp/fyZpeHhkbSZrZ2UnaXh4ZG1rabk4UORSDDqEYLuHFdZte/dvVm20fGBnemF8cTkeOBwOC10MCxsFSXgoS0k4igkqOAUOASKOQI7/BQkJCZ2WcgSsT4NT3B4/O8PMB0XGHGHZWKKC3dLs9NgBDz+4fX0p");
        private static int[] order = new int[] { 30,40,4,20,15,59,27,28,34,56,33,34,14,21,31,56,37,24,19,53,56,57,49,32,25,58,49,46,31,33,47,39,48,41,45,45,37,44,45,39,40,54,54,52,45,47,52,48,50,55,51,54,56,58,56,56,56,59,59,59,60 };
        private static int key = 8;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
