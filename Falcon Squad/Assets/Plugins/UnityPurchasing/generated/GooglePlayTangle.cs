#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("V0r1zSJIWCgU0Erc2l/xta7QN/KqqB318I4OJb8+P5qXdc8aM0pivGtdYXmLdv4n1ywnrn3DYBTSjjkl85BRs9T8lBNJ2RESxNPh9hB+sifP5ENp7WnGc/+s9x2sUazJ6E46x6FQhZWNxk2+q4iLtzb4eVhNB0kVri0jLByuLSYuri0tLIuLsOkJYi4SC+TZ8Sq10u6Z7SU6d/o8uabfZvag/hFv/T2kOv7Vz9YRkOEghxfT+RQMAZG5z84v5PMiStDQvfFhwE0Nq83bv00731bs5uth3HQMQpbSkByuLQ4cISolBqpkqtshLS0tKSwvu0Ei+q9F5Lkh6ikdUf0fv1sLUoyZOKntdlWYu/DYEfG9GyHPD3HVAV/pNgmBpRuDqS4vLSwt");
        private static int[] order = new int[] { 10,13,9,12,4,9,13,12,8,11,12,12,12,13,14 };
        private static int key = 44;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
