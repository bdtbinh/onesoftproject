//
//  Copyright (c) 2015 IronSource. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IronSource/ISBaseAdapter+Internal.h"

//System Frameworks For UnityAds Adapter

@import AdSupport;
@import StoreKit;
@import CoreTelephony;

@interface ISUnityAdsAdapter : ISBaseAdapter

@end
